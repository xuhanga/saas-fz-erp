# saas-fz-erp

#### 介绍
专业SAAS服装ERP和门店收银系统，进销存+财务管理+门店收银+配货功能+小程序营销，门店支持自营和加盟，以及货品+费用结算。

#### 软件架构
软件架构说明
JDK8 + JSP+tomcat8原始开发的功能，后续支持分布式spring cloud+spring boot +vue的架构

#### 安装教程

1.  打包到war包
2.  放到tomcat下启动

#### 使用说明

1.  针对连锁店，加盟店总部和门户使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技
![商品信息](%E5%9B%BE%E7%89%87.png)

