package zy.dao;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public interface BaseDao {
	public static String public_table = "fda_public";

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate();

	public Map<String, Object> map(String sql);
}