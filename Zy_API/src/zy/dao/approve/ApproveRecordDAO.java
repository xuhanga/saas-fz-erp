package zy.dao.approve;

import java.util.List;

import zy.entity.approve.T_Approve_Record;

public interface ApproveRecordDAO {
	void save(T_Approve_Record record);
	void save(List<T_Approve_Record> records);
	T_Approve_Record load(String number,Integer companyid);
	List<T_Approve_Record> list(String number,String ar_type,Integer companyid);
}
