package zy.dao.approve.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.approve.ApproveRecordDAO;
import zy.entity.approve.T_Approve_Record;

@Repository
public class ApproveRecordDAOImpl extends BaseDaoImpl implements ApproveRecordDAO{

	@Override
	public void save(T_Approve_Record record) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_approve_record");
		sql.append(" (ar_number,ar_state,ar_describe,ar_sysdate,ar_type,ar_us_name,companyid)");
		sql.append(" VALUES(:ar_number,:ar_state,:ar_describe,:ar_sysdate,:ar_type,:ar_us_name,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(record),holder);
		record.setAr_id(holder.getKey().intValue());
	}
	
	@Override
	public void save(List<T_Approve_Record> records) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_approve_record");
		sql.append(" (ar_number,ar_state,ar_describe,ar_sysdate,ar_type,ar_us_name,companyid)");
		sql.append(" VALUES(:ar_number,:ar_state,:ar_describe,:ar_sysdate,:ar_type,:ar_us_name,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(records.toArray()));
	}

	@Override
	public T_Approve_Record load(String number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ar_id,ar_number,ar_state,ar_describe,ar_sysdate,ar_type,ar_us_name,companyid");
		sql.append(" FROM t_approve_record t");
		sql.append(" WHERE ar_number = :ar_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" ORDER BY ar_id DESC");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource().addValue("ar_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Approve_Record.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<T_Approve_Record> list(String number, String ar_type, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ar_id,ar_number,ar_state,ar_describe,ar_sysdate,ar_type,ar_us_name,companyid");
		sql.append(" FROM t_approve_record t");
		sql.append(" WHERE ar_number = :ar_number");
		sql.append(" AND ar_type = :ar_type");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY ar_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("ar_number", number).addValue("ar_type", ar_type).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Approve_Record.class));	
	}
}
