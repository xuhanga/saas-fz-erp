package zy.dao.base.bra;

import java.util.List;
import java.util.Map;

import zy.entity.base.bra.T_Base_Bra;

public interface BraDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Bra> list(Map<String,Object> param);
	
	Integer queryByName(T_Base_Bra model);
	T_Base_Bra queryByID(Integer id);
	void update(T_Base_Bra model);
	void save(T_Base_Bra model);
	void del(Integer id);
	List<T_Base_Bra> braList(Map<String,Object> param);
	
	List<T_Base_Bra> queryByPdCode(Map<String,Object> param);
}
