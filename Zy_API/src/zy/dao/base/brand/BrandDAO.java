package zy.dao.base.brand;

import java.util.List;
import java.util.Map;

import zy.entity.base.brand.T_Base_Brand;

public interface BrandDAO {
	Integer count(Map<String,Object> param);
	List<T_Base_Brand> list(Map<String,Object> param);
	List<String> brandList(Map<String,Object> param);
	Integer queryByName(T_Base_Brand brand);
	T_Base_Brand queryByID(Integer bd_id);
	void save(T_Base_Brand brand);
	void save(List<T_Base_Brand> brands);
	void update(T_Base_Brand brand);
	void del(Integer bd_id);
}
