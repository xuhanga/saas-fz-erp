package zy.dao.base.company;

import java.util.List;
import java.util.Map;

import zy.entity.sys.company.T_Sys_Company;
import zy.entity.sys.company.T_Zhjr_Unit;

public interface CompanyDAO {
	public List<T_Sys_Company> company_list(Map<String, Object> param);
	public Integer company_count(Map<String, Object> param);
	Integer queryByCode(String code);
	public List<T_Zhjr_Unit> queryUnit();
	public void save(T_Sys_Company company);
}
