package zy.dao.base.company.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.company.CompanyDAO;
import zy.entity.base.depot.T_Base_Depot;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sys.company.T_Sys_Company;
import zy.entity.sys.company.T_Zhjr_Unit;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.set.T_Vip_Setup;
import zy.util.DateUtil;
import zy.util.MD5;
@Repository
public class CompanyDAOImpl extends BaseDaoImpl implements CompanyDAO {

	@Override
	public List<T_Sys_Company> company_list(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object co_code = param.get("co_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT co_id,co_code,co_name,co_state,co_sysdate,co_man,co_tel,co_shops,co_type,co_ver,co_qq");
		sql.append(" FROM t_sys_company t");
		sql.append(" WHERE 1 = 1");
		if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND INSTR(t.co_name,:searchContent)>0");
        }
        if(null != co_code && !"".equals(co_code)){
        	sql.append(" AND t.co_code = :co_code");
        }
		sql.append(" order by co_id DESC");
		sql.append(" limit :start,:end");
		List<T_Sys_Company> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
				new BeanPropertyRowMapper<>(T_Sys_Company.class));
		return list;
	}

	@Override
	public Integer company_count(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object co_code = param.get("co_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT COUNT(1)");
		sql.append(" FROM t_sys_company t");
		sql.append(" WHERE 1 = 1");
        if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND INSTR(t.co_name,:searchContent)>0");
        }
        if(null != co_code && !"".equals(co_code)){
        	sql.append(" AND t.co_code = :co_code");
        }
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public Integer queryByCode(String code) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT co_id");
		sql.append(" FROM t_sys_company t");
		sql.append(" WHERE 1 = 1");
        sql.append(" AND t.co_code = :co_code");
        sql.append(" LIMIT 1");
        Integer id = null;
        try {
        	id = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
        			new MapSqlParameterSource().addValue("co_code", code),Integer.class);
		} catch (Exception e) {
		}
		return id;
	}

	@Override
	public List<T_Zhjr_Unit> queryUnit() {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT un_id,un_name");
		sql.append(" FROM t_zhjr_unit t");
        List<T_Zhjr_Unit> list = namedParameterJdbcTemplate.query(sql.toString(), 
        		new MapSqlParameterSource(),new BeanPropertyRowMapper<>(T_Zhjr_Unit.class));
		return list;
	}
	/**
	 * 新增一商家，要生成许多默认的数据
	 * 1.保存商家信息
	 * 2.保存角色
	 * 3.保存用户
	 * 4.保存角色菜单
	 * 5.保存门店及子表数据
	 * 6.保存仓库
	 * 7.保存系统设置
	 * 8.保存会员设置
	 * @param company公司注册信息
	 * */
	@Override
	public void save(T_Sys_Company company) {
		StringBuffer sql = new StringBuffer("");
		//1.保存商家信息
		sql.append("INSERT INTO t_sys_company(");
		sql.append("co_code,co_name,co_state,co_sysdate,");
		sql.append("co_man,co_tel,co_shops,co_province,");
		sql.append("co_city,co_town,co_addr,co_qq,");
		sql.append("co_type,co_ver,co_users,co_wechat,");
		sql.append("co_salesman,co_unit");
		sql.append(")VALUES(");
		sql.append(":co_code,:co_name,:co_state,:co_sysdate,");
		sql.append(":co_man,:co_tel,:co_shops,:co_province,");
		sql.append(":co_city,:co_town,:co_addr,:co_qq,");
		sql.append(":co_type,:co_ver,:co_users,:co_wechat,");
		sql.append(":co_salesman,:co_unit");
		sql.append(")");
		company.setCo_sysdate(DateUtil.getYearMonthDate());
		company.setCo_state(0);
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(company),holder);
		int id = holder.getKey().intValue();
		company.setCo_id(id);
		sql.setLength(0);
		sql.append("INSERT INTO t_base_shop(");
		sql.append(" sp_code,sp_name,sp_spell,sp_shop_type,sp_state,sp_end,");
		sql.append(" sp_lock,sp_upcode,sp_rate,sp_init,sp_main,companyid");
		sql.append(")VALUES(");
		sql.append(":sp_code,:sp_name,:sp_spell,:sp_shop_type,:sp_state,:sp_end,");
		sql.append(":sp_lock,:sp_upcode,:sp_rate,:sp_init,:sp_main,:companyid");
		sql.append(")");
		T_Base_Shop shop = buildShop(company);
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(shop));
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_role(");
		sql.append("ro_code,ro_name,ro_shop_type,ro_shop_code,ro_date,ro_default,companyid");
		sql.append(")(");
		sql.append("SELECT CONCAT('00',ty_id),CONCAT(ty_name,'角色'),ty_id");
		sql.append(",:sp_code,'"+DateUtil.getYearMonthDate()+"'");
		sql.append(",:sp_shop_type,:companyid");
		sql.append(" FROM common_type");
		sql.append(" WHERE INSTR(ty_ver,:version)>0");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(shop));
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_user(");
		sql.append("us_code,us_account,us_name,us_pass,us_state,us_date,us_ro_code,");
		sql.append("us_shop_code,us_limit,us_lock,us_end,us_default,");
		sql.append("us_pay,companyid");
		sql.append(")VALUES(");
		sql.append(":us_code,:us_account,:us_name,:us_pass,:us_state,:us_date,:us_ro_code,");
		sql.append(":us_shop_code,:us_limit,:us_lock,:us_end,:us_default,");
		sql.append(":us_pay,:companyid");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(buildUser(company)));
		sql.setLength(0);
		sql.append(" INSERT INTO t_sys_rolemenu(");
		sql.append(" rm_ro_code,rm_state,rm_limit,rm_mn_code,companyid)");
		sql.append(" (SELECT CONCAT('00',ty_id),0,mn_limit,mn_code,"+company.getCo_id());
		sql.append(" FROM t_sys_menu t");
		sql.append(" JOIN common_type c");
		sql.append(" ON 1=1");
		sql.append(" AND INSTR(ty_ver,:co_ver)>0");
		sql.append(" WHERE INSTR(mn_version,:co_ver)>0");
		sql.append(" AND mn_state=0)");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(company));
		sql.setLength(0);
		sql.append("INSERT INTO t_base_shop_info(");
		sql.append("spi_man,spi_mobile,spi_addr,");
		sql.append("spi_province,spi_city,spi_town,");
		sql.append("spi_shop_code,companyid");
		sql.append(")VALUES(");
		sql.append(":spi_man,:spi_mobile,:spi_addr,");
		sql.append(":spi_province,:spi_city,:spi_town,");
		sql.append(":sp_code,:companyid");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(shop));
		sql.setLength(0);
		sql.append("INSERT INTO t_base_depot(");
		sql.append("dp_code,dp_name,dp_spell,");
		sql.append("dp_man,dp_state,dp_addr,dp_default,");
		sql.append("dp_mobile,dp_shop_code,companyid");
		sql.append(")VALUES(");
		sql.append(":dp_code,:dp_name,:dp_spell,");
		sql.append(":dp_man,:dp_state,:dp_addr,:dp_default,");
		sql.append(":dp_mobile,:dp_shop_code,:companyid");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(buildDepot(company)));
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_set(");
		sql.append("companyid,st_buy_calc_costprice,st_buy_os,");
		sql.append("st_batch_os,st_batch_showrebates,st_ar_print,");
		sql.append("st_blank,st_subcode_isrule,st_subcode_rule,");
		sql.append("st_allocate_unitprice,st_check_showstock,");
		sql.append("st_check_showdiffer,st_force_checkstock,");
		sql.append("st_useable,st_use_upmoney");
		sql.append(")VALUES(");
		sql.append(":companyid,:st_buy_calc_costprice,:st_buy_os,");
		sql.append(":st_batch_os,:st_batch_showrebates,:st_ar_print,");
		sql.append(":st_blank,:st_subcode_isrule,:st_subcode_rule,");
		sql.append(":st_allocate_unitprice,:st_check_showstock,");
		sql.append(":st_check_showdiffer,:st_force_checkstock,");
		sql.append(":st_useable,:st_use_upmoney");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(buildSet(company)));
		sql.setLength(0);
		sql.append("INSERT INTO t_vip_setup(");
		sql.append("vs_loss_day,vs_consume_day,vs_loyalvip_times,");
		sql.append("vs_richvip_money,vs_activevip_day,vs_shop_code,");
		sql.append("companyid");
		sql.append(")VALUES(");
		sql.append(":vs_loss_day,:vs_consume_day,:vs_loyalvip_times,");
		sql.append(":vs_richvip_money,:vs_activevip_day,:vs_shop_code,");
		sql.append(":companyid");
		sql.append(")");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(buildVipSet(company)));
	}
	private T_Base_Shop buildShop(T_Sys_Company company){
		T_Base_Shop shop = new T_Base_Shop();
		shop.setSp_code("001");
		shop.setSp_name("总公司");
		shop.setSp_spell("ZGS");
		shop.setSp_shop_type("1");
		shop.setSp_state(0);
		shop.setVersion(company.getCo_ver());
		if(company.getCo_state() == 1){
			shop.setSp_end(DateUtil.getDateAddDays(365));
		}else{
			shop.setSp_end(DateUtil.getDateAddDays(15));
		}
		shop.setSp_lock(0);
		shop.setSp_upcode("001");
		shop.setSp_rate(0.0d);
		shop.setSp_init(0);
		shop.setSp_main(0);
		shop.setCompanyid(company.getCo_id());
		shop.setSpi_addr(company.getCo_addr());
		shop.setSpi_city(company.getCo_city());
		shop.setSpi_man(company.getCo_man());
		shop.setSpi_mobile(company.getCo_tel());
		shop.setSpi_province(company.getCo_province());
		shop.setSpi_town(company.getCo_town());
		return shop;
	}
	private T_Base_Depot buildDepot(T_Sys_Company model){
		T_Base_Depot depot = new T_Base_Depot();
		depot.setDp_code("001");
		depot.setDp_addr(model.getCo_addr());
		depot.setDp_default(1);
		depot.setDp_man(model.getCo_man());
		depot.setDp_mobile(model.getCo_tel());
		depot.setDp_name("总公司仓库");
		depot.setDp_spell("ZGSCK");
		depot.setDp_shop_code("001");
		depot.setCompanyid(model.getCo_id());
		depot.setDp_state(0);
		return depot;
	}
	private T_Sys_Set buildSet(T_Sys_Company model){
		T_Sys_Set set = new T_Sys_Set();
		set.setCompanyid(model.getCo_id());
		set.setSt_allocate_unitprice(0);
		set.setSt_ar_print(0);
		set.setSt_batch_os(0);
		set.setSt_batch_showrebates(1);
		set.setSt_blank(3);
		set.setSt_buy_calc_costprice(1);
		set.setSt_buy_os(1);
		set.setSt_check_showdiffer(1);
		set.setSt_check_showstock(0);
		set.setSt_force_checkstock(0);
		set.setSt_subcode_isrule(0);
		set.setSt_subcode_rule("编码+颜色+尺码+杯型");
		set.setSt_use_upmoney(1);
		set.setSt_useable(1);
		return set;
	}
	private T_Vip_Setup buildVipSet(T_Sys_Company model){
		T_Vip_Setup set = new T_Vip_Setup();
		set.setCompanyid(model.getCo_id());
		set.setVs_activevip_day(30);
		set.setVs_consume_day(90);
		set.setVs_loss_day(180);
		set.setVs_loyalvip_times(3);
		set.setVs_richvip_money(1000d);
		set.setVs_shop_code("001");
		return set;
	}
	
	private T_Sys_User buildUser(T_Sys_Company model){
		T_Sys_User user = new T_Sys_User();
		user.setUs_code("001");
		user.setUs_account("admin");
		user.setUs_name("管理员");
		user.setUs_pass(MD5.encryptMd5("123456"));
		user.setUs_state(0);
		user.setUs_date(DateUtil.getCurrentTime());
		user.setUs_ro_code("001");
		user.setUs_shop_code("001");
		user.setUs_limit("CSDVWEP");
		if(model.getCo_state() == 1){
			user.setUs_end(DateUtil.getDateAddDays(365));
		}else{
			user.setUs_end(DateUtil.getDateAddDays(15));
		}
		user.setUs_lock(0);
		user.setUs_default(1);
		user.setUs_pay(0);
		user.setCompanyid(model.getCo_id());
		return user;
	}
}
