package zy.dao.base.department.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.department.DepartmentDAO;
import zy.entity.base.department.T_Base_Department;
import zy.util.CommonUtil;
@Repository
public class DepartmentDAOImpl extends BaseDaoImpl implements DepartmentDAO{
	@Override
	public Integer count(Map<String, Object> param) {
		Object name = param.get("name");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from T_Base_Department t");
		sql.append(" where 1 = 1");
        if(null != name && !"".equals(name)){
        	sql.append(" and instr(t.dm_name,:name)>0");
        }
        sql.append(" and t.dm_shop_code=:shop_code");
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Base_Department> list(Map<String, Object> param) {
		Object name = param.get("name");
		Object shop_type = param.get("shop_type");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dm_id,dm_code,dm_name");
		sql.append(" from T_Base_Department t");
		sql.append(" where 1 = 1");
        if(null != name && !"".equals(name)){
        	sql.append(" and instr(t.dm_name,:name)>0");
        }
		sql.append(" and t.companyid=:companyid");
		
		if(shop_type == null){
			sql.append(" and t.dm_shop_code=:shop_code");
		}else {
			if(CommonUtil.THREE.equals(shop_type)){//自营店看到上级店铺创建的部门
				sql.append(" and t.dm_shop_code=:shop_upcode");
			}else{
				sql.append(" and t.dm_shop_code=:shop_code");
			}
		}
		
		sql.append(" order by dm_id desc");
		List<T_Base_Department> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Department.class));
		return list;
	}

	@Override
	public Integer queryByName(T_Base_Department model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dm_id from T_Base_Department");
		sql.append(" where 1=1");
		if(null != model.getDm_name() && !"".equals(model.getDm_name())){
			sql.append(" and dm_name=:dm_name");
		}
		if(null != model.getDm_id() && model.getDm_id() > 0){
			sql.append(" and dm_id <> :dm_id");
		}
		sql.append(" and companyid=:companyid");
		sql.append(" and dm_shop_code=:dm_shop_code");
		sql.append(" limit 1");
		try{
			Integer id = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(model),Integer.class);
			return id;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public T_Base_Department queryByID(Integer dm_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dm_id,dm_code,dm_name from T_Base_Department");
		sql.append(" where dm_id=:dm_id");
		sql.append(" limit 1");
		try{
			T_Base_Department data = namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("dm_id", dm_id),new BeanPropertyRowMapper<>(T_Base_Department.class));
			return data;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void save(T_Base_Department model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select f_two_code(max(dm_code+0)) from T_Base_Department ");
		sql.append(" where 1=1");
		sql.append(" and companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(model), String.class);
		model.setDm_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO T_Base_Department");
		sql.append(" (dm_code,dm_name,dm_spell,dm_shop_code,companyid)");
		sql.append(" VALUES(:dm_code,:dm_name,:dm_spell,:dm_shop_code,:companyid)");
		
		KeyHolder holder = new GeneratedKeyHolder(); 
		
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(model),holder);
		int id = holder.getKey().intValue();
		model.setDm_id(id);
	}

	@Override
	public void update(T_Base_Department model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" update T_Base_Department");
		sql.append(" set dm_name=:dm_name");
		sql.append(" ,dm_spell=:dm_spell");
		sql.append(" where dm_id=:dm_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(model));
	}

	@Override
	public void del(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM T_Base_Department");
		sql.append(" WHERE dm_id=:dm_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("dm_id", id));
	}
}
