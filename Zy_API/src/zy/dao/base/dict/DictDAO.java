package zy.dao.base.dict;

import java.util.List;
import java.util.Map;

import zy.entity.base.dict.T_Base_Dict;
import zy.entity.base.dict.T_Base_DictList;

public interface DictDAO {
	List<T_Base_Dict> uplist(Map<String,Object> param);
	List<T_Base_DictList> list(Map<String,Object> param);
	Integer queryByName(T_Base_DictList model);
	T_Base_DictList queryByID(Integer id);
	void save(T_Base_DictList model);
	void update(T_Base_DictList model);
	void del(Integer id);
}
