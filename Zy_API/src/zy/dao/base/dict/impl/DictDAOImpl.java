package zy.dao.base.dict.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.dict.DictDAO;
import zy.entity.base.dict.T_Base_Dict;
import zy.entity.base.dict.T_Base_DictList;
@Repository
public class DictDAOImpl extends BaseDaoImpl implements DictDAO {

	@Override
	public List<T_Base_Dict> uplist(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dt_id,dt_code,dt_name");
		sql.append(" from T_Base_Dict t");
		List<T_Base_Dict> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Dict.class));
		return list;
	}

	@Override
	public List<T_Base_DictList> list(Map<String, Object> param) {
		Object name = param.get("name");
		Object up_code = param.get("up_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dtl_id,dtl_code,dtl_upcode,dtl_name");
		sql.append(" from T_Base_DictList t");
		sql.append(" where 1 = 1");
        if(null != name && !"".equals(name)){
        	sql.append(" and instr(t.dtl_name,:name)>0");
        }
        if(null != up_code && !"".equals(up_code)){
        	sql.append(" and t.dtl_upcode=:up_code");
        }
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by dtl_id asc");
		List<T_Base_DictList> list = namedParameterJdbcTemplate.query(sql.toString(), param, 
										new BeanPropertyRowMapper<>(T_Base_DictList.class));
		return list;
	}

	@Override
	public Integer queryByName(T_Base_DictList model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dtl_id from T_Base_DictList");
		sql.append(" where 1=1");
		if(null != model.getDtl_name() && !"".equals(model.getDtl_name())){
			sql.append(" and dtl_name=:dtl_name");
		}
		if(null != model.getDtl_upcode() && !"".equals(model.getDtl_upcode())){
			sql.append(" and dtl_upcode=:dtl_upcode");
		}
		if(null != model.getDtl_id() && model.getDtl_id() > 0){
			sql.append(" and dtl_id<>:dtl_id");
		}
		sql.append(" and companyid=:companyid");
		sql.append(" limit 1");
		try{
			Integer id = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(model),Integer.class);
			return id;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public T_Base_DictList queryByID(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select dtl_id,dtl_code,dtl_name,dtl_upcode from T_Base_DictList");
		sql.append(" where dtl_id=:dtl_id");
		sql.append(" limit 1");
		try{
			T_Base_DictList data = namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("br_id", id)
									,new BeanPropertyRowMapper<>(T_Base_DictList.class));
			return data;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void save(T_Base_DictList model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select f_two_code(max(dtl_code+0)) from T_Base_DictList ");
		sql.append(" where 1=1");
		sql.append(" and dtl_upcode=:dtl_upcode");
		sql.append(" and companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(model), String.class);
		model.setDtl_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO T_Base_DictList");
		sql.append(" (dtl_code,dtl_upcode,dtl_name,companyid)");
		sql.append(" VALUES(:dtl_code,:dtl_upcode,:dtl_name,:companyid)");
		
		KeyHolder holder = new GeneratedKeyHolder(); 
		
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(model),holder);
		int id = holder.getKey().intValue();
		model.setDtl_id(id);
	}

	@Override
	public void update(T_Base_DictList model) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" update T_Base_DictList");
		sql.append(" set dtl_name=:dtl_name");
		sql.append(" where dtl_id=:dtl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(model));
	}

	@Override
	public void del(Integer id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM T_Base_DictList");
		sql.append(" WHERE dtl_id=:dtl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("dtl_id", id));
	}

}
