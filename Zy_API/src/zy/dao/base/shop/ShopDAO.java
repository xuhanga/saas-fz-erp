package zy.dao.base.shop;

import java.util.List;
import java.util.Map;

import zy.dto.base.shop.ShopMoneyDetailsDto;
import zy.entity.base.shop.T_Base_Shop;

public interface ShopDAO {
	List<T_Base_Shop> all_list(Map<String,Object> param);
	List<T_Base_Shop> sub_list(Map<String, Object> param);
	List<T_Base_Shop> sub_list_jmd(Map<String, Object> param);
	List<T_Base_Shop> selectList(Map<String, Object> param);
	List<T_Base_Shop> price_list(Map<String, Object> param);
	List<T_Base_Shop> want_list(Map<String, Object> param);
	List<T_Base_Shop> allot_list(Map<String, Object> param);
	List<T_Base_Shop> money_list(Map<String, Object> param);
	List<T_Base_Shop> sub_tree(Map<String, Object> param);
	List<T_Base_Shop> listByEmp(Map<String, Object> param);
	Integer count(Map<String,Object> param);
	List<T_Base_Shop> list(Map<String,Object> param);
	Integer countZone(Map<String,Object> param);
	List<T_Base_Shop> listZone(Map<String,Object> param);
	T_Base_Shop queryByID(Integer id);
	T_Base_Shop load(String sp_code,Integer companyid);
	void update(T_Base_Shop model);
	void save(T_Base_Shop model);
	void saveSellSet(T_Base_Shop model);
	void saveZone(T_Base_Shop model);
	void del(Integer id);
	void updateState(T_Base_Shop model);
	public List<T_Base_Shop> up_sub_list(Map<String, Object> param);
	
	T_Base_Shop loadShop(String sp_code, Integer companyid);
	void updateReceivable(T_Base_Shop shop);
	void updatePrepay(T_Base_Shop shop);
	void updateSettle(T_Base_Shop shop);
	Map<String, Object> countsumMoneyDetails(Map<String, Object> params);
	List<ShopMoneyDetailsDto> listMoneyDetails(Map<String,Object> params);
	List<String> queryShopCode(Map<String,Object> params);
}
