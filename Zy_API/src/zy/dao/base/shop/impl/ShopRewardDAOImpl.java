package zy.dao.base.shop.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.base.shop.ShopRewardDAO;
import zy.entity.base.shop.T_Base_Shop_Reward;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class ShopRewardDAOImpl extends BaseDaoImpl implements ShopRewardDAO{
	
	@Override
	public Integer count(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_base_shop_reward t");
		sql.append(" JOIN t_base_shop sp ON sp_code = sr_sp_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if (StringUtil.isNotEmpty(params.get("begindate"))) {
			sql.append(" AND sr_sysdate >= :begindate ");
		}
		if (StringUtil.isNotEmpty(params.get("enddate"))) {
			sql.append(" AND sr_sysdate <= :enddate ");
		}
		if (StringUtil.isNotEmpty(params.get("sr_sp_code"))) {
			sql.append(" AND t.sr_sp_code=:sr_sp_code");
		}
		if (StringUtil.isNotEmpty(params.get("sr_rw_code"))) {
			sql.append(" AND t.sr_rw_code=:sr_rw_code");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Base_Shop_Reward> list(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sr_id,sr_sp_code,sr_rw_code,sr_sysdate,sr_state,sr_remark,t.companyid,sp_name AS shop_name,");
		sql.append(" (SELECT rw_name FROM t_sys_reward rw WHERE rw_code = sr_rw_code AND rw.companyid = t.companyid LIMIT 1) AS rw_name");
		sql.append(" FROM t_base_shop_reward t");
		sql.append(" JOIN t_base_shop sp ON sp_code = sr_sp_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if (StringUtil.isNotEmpty(params.get("begindate"))) {
			sql.append(" AND sr_sysdate >= :begindate ");
		}
		if (StringUtil.isNotEmpty(params.get("enddate"))) {
			sql.append(" AND sr_sysdate <= :enddate ");
		}
		if (StringUtil.isNotEmpty(params.get("sr_sp_code"))) {
			sql.append(" AND t.sr_sp_code=:sr_sp_code");
		}
		if (StringUtil.isNotEmpty(params.get("sr_rw_code"))) {
			sql.append(" AND t.sr_rw_code=:sr_rw_code");
		}
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY sr_id DESC");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Base_Shop_Reward.class));
	}
	
	@Override
	public List<T_Base_Shop_Reward> statByShop(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT sr_rw_code,rw_name,rw_style,rw_icon,COUNT(1) AS reward_count");
		sql.append(" FROM t_base_shop_reward t");
		sql.append(" JOIN t_sys_reward rw ON rw_code = sr_rw_code AND rw.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND sr_sp_code = :sr_sp_code");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY sr_rw_code");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Base_Shop_Reward.class));
	}
	
	@Override
	public void save(T_Base_Shop_Reward shopReward) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_shop_reward");
		sql.append(" (sr_sp_code,sr_rw_code,sr_sysdate,sr_state,sr_remark,companyid)");
		sql.append(" VALUES(:sr_sp_code,:sr_rw_code,:sr_sysdate,:sr_state,:sr_remark,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(shopReward),holder);
		shopReward.setSr_id(holder.getKey().intValue());
	}

	@Override
	public void save(List<T_Base_Shop_Reward> shopRewards) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_base_shop_reward");
		sql.append(" (sr_sp_code,sr_rw_code,sr_sysdate,sr_state,sr_remark,companyid)");
		sql.append(" VALUES(:sr_sp_code,:sr_rw_code,:sr_sysdate,:sr_state,:sr_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(shopRewards.toArray()));
	}
}
