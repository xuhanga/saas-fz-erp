package zy.dao.base.size;

import java.util.List;
import java.util.Map;

import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;

public interface SizeDAO {
	Integer count(Map<String,Object> param);
	Integer queryByName(T_Base_Size size);
	List<T_Base_Size> list(Map<String,Object> param);
	public T_Base_Size queryByID(Integer id);
	void save(T_Base_Size model);
	void update(T_Base_Size model);
	void del(Integer id);
	
	//-----组操作
	List<T_Base_Size_Group> listGroup(Map<String, Object> param);
	T_Base_Size_Group queryGroupByID(Integer sz_id);
	void updateGroup(Map<String,Object> map);
	void saveGroup(Map<String,Object> map);
	void delGroup(Integer sz_id);
	
	//-----以下是子表操作方法
	List<T_Base_SizeList> listByID(Map<String,Object> param);
	
	List<T_Base_SizeList> listBySzg(List<String> szgCodes,Integer companyid);
}
