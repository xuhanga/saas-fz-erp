package zy.dao.batch.dealings;

import java.util.List;
import java.util.Map;

import zy.entity.batch.dealings.T_Batch_Dealings;

public interface BatchDealingsDAO {
	Integer count(Map<String,Object> params);
	List<T_Batch_Dealings> list(Map<String,Object> params);
	void save(T_Batch_Dealings dealings);
}
