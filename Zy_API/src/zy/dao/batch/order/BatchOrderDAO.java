package zy.dao.batch.order;

import java.util.List;
import java.util.Map;

import zy.entity.base.product.T_Base_Product;
import zy.entity.batch.order.T_Batch_Import;
import zy.entity.batch.order.T_Batch_Order;
import zy.entity.batch.order.T_Batch_OrderList;

public interface BatchOrderDAO {
	Integer count(Map<String,Object> params);
	List<T_Batch_Order> list(Map<String,Object> params);
	Integer count4Sell(Map<String,Object> params);
	List<T_Batch_Order> list4Sell(Map<String,Object> params);
	T_Batch_Order load(Integer od_id);
	T_Batch_Order load(String number,Integer companyid);
	T_Batch_Order check(String number,Integer companyid);
	List<T_Batch_OrderList> detail_list_forsavetemp(String od_number,Integer companyid);
	List<T_Batch_OrderList> detail_list(Map<String, Object> params);
	List<T_Batch_OrderList> detail_list_print(Map<String, Object> params);
	List<T_Batch_OrderList> detail_sum(Map<String, Object> params);
	List<String> detail_szgcode(Map<String,Object> params);
	List<T_Batch_OrderList> temp_list_forimport(Integer od_type,Integer us_id,Integer companyid);
	List<T_Batch_OrderList> temp_list_forsave(Integer od_type,Integer us_id,Integer companyid);
	List<T_Batch_OrderList> temp_list(Map<String, Object> params);
	List<T_Batch_OrderList> temp_sum(Integer od_type,Integer us_id,Integer companyid);
	List<String> temp_szgcode(Map<String,Object> params);
	Integer count_product(Map<String, Object> param);
	List<T_Base_Product> list_product(Map<String, Object> param);
	T_Base_Product load_product(String pd_code,Integer companyid);
	Double queryLastBatchPrice(String pd_code,String ci_code, Integer companyid);
	Map<String, Object> load_product_size(Map<String,Object> params);
	Double temp_queryUnitPrice(String pd_code,String odl_pi_type, Integer od_type, Integer us_id, Integer companyid);
	T_Batch_OrderList temp_loadBySubCode(String sub_code,String odl_pi_type, Integer od_type, Integer us_id, Integer companyid);
	void temp_save(List<T_Batch_OrderList> temps);
	void temp_save(T_Batch_OrderList temp);
	void temp_update(List<T_Batch_OrderList> temps);
	void temp_updateById(List<T_Batch_OrderList> temps);
	void temp_update(T_Batch_OrderList temp);
	void temp_updateprice(String pd_code,String odl_pi_type, Double unitPrice, Integer od_type, Integer us_id, Integer companyid);
	void temp_updateRemarkById(T_Batch_OrderList temp);
	void temp_updateRemarkByPdCode(T_Batch_OrderList temp);
	void temp_del(List<T_Batch_OrderList> temps);
	void temp_del(Integer odl_id);
	void temp_delByPiCode(T_Batch_OrderList temp);
	void temp_clear(Integer od_type,Integer us_id,Integer companyid);
	List<T_Batch_Import> temp_listByImport(List<String> barCodes,String priceType,Double ci_rate,String batch_price,String ci_code,Integer companyid);
	void save(T_Batch_Order order, List<T_Batch_OrderList> details);
	void update(T_Batch_Order order, List<T_Batch_OrderList> details);
	void updateApprove(T_Batch_Order order);
	void updateStop(T_Batch_Order order);
	void del(String od_number, Integer companyid);
	void deleteList(String od_number, Integer companyid);
}
