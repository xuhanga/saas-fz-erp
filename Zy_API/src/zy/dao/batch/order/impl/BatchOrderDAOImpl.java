package zy.dao.batch.order.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.batch.order.BatchOrderDAO;
import zy.dto.common.ProductDto;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.size.T_Base_Size;
import zy.entity.batch.order.T_Batch_Import;
import zy.entity.batch.order.T_Batch_Order;
import zy.entity.batch.order.T_Batch_OrderList;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class BatchOrderDAOImpl extends BaseDaoImpl implements BatchOrderDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object od_type = params.get("od_type");
		Object od_isdraft = params.get("od_isdraft");
		Object od_ar_state = params.get("od_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object od_client_code = params.get("od_client_code");
		Object od_depot_code = params.get("od_depot_code");
		Object od_manager = params.get("od_manager");
		Object od_number = params.get("od_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_batch_order t");
		sql.append(" JOIN t_batch_client ci ON ci_code = od_client_code AND ci.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ci_sp_code = :shop_code ");//上级店铺编号
		if (StringUtil.isNotEmpty(od_type)) {
			sql.append(" AND od_type = :od_type ");
		}
		if (StringUtil.isNotEmpty(od_isdraft)) {
			sql.append(" AND od_isdraft = :od_isdraft ");
		}
		if (StringUtil.isNotEmpty(od_ar_state)) {
			sql.append(" AND od_ar_state = :od_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND od_make_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND od_make_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(od_client_code)) {
			sql.append(" AND od_client_code = :od_client_code ");
		}
		if (StringUtil.isNotEmpty(od_depot_code)) {
			sql.append(" AND od_depot_code = :od_depot_code ");
		}
		if (StringUtil.isNotEmpty(od_manager)) {
			sql.append(" AND od_manager = :od_manager ");
		}
		if (StringUtil.isNotEmpty(od_number)) {
			sql.append(" AND INSTR(od_number,:od_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Batch_Order> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object od_type = params.get("od_type");
		Object od_isdraft = params.get("od_isdraft");
		Object od_ar_state = params.get("od_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object od_client_code = params.get("od_client_code");
		Object od_depot_code = params.get("od_depot_code");
		Object od_manager = params.get("od_manager");
		Object od_number = params.get("od_number");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT od_id,od_number,od_make_date,od_delivery_date,od_client_code,od_depot_code,od_maker,od_manager,od_state,od_handnumber,");
		sql.append(" od_amount,od_realamount,od_money,od_retailmoney,od_costmoney,od_remark,od_ar_state,od_ar_date,od_isdraft,od_sysdate,od_us_id,od_type,od_isprint,t.companyid,ci_name AS client_name,");
		sql.append(" (SELECT cis_name FROM t_batch_client_shop cis WHERE cis.cis_code = t.od_client_shop_code AND cis.cis_ci_code = t.od_client_code AND cis.companyid = t.companyid LIMIT 1) AS client_shop_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = od_depot_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_batch_order t");
		sql.append(" JOIN t_batch_client ci ON ci_code = od_client_code AND ci.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ci_sp_code = :shop_code ");//上级店铺编号
		if (StringUtil.isNotEmpty(od_type)) {
			sql.append(" AND od_type = :od_type ");
		}
		if (StringUtil.isNotEmpty(od_isdraft)) {
			sql.append(" AND od_isdraft = :od_isdraft ");
		}
		if (StringUtil.isNotEmpty(od_ar_state)) {
			sql.append(" AND od_ar_state = :od_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND od_make_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND od_make_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(od_client_code)) {
			sql.append(" AND od_client_code = :od_client_code ");
		}
		if (StringUtil.isNotEmpty(od_depot_code)) {
			sql.append(" AND od_depot_code = :od_depot_code ");
		}
		if (StringUtil.isNotEmpty(od_manager)) {
			sql.append(" AND od_manager = :od_manager ");
		}
		if (StringUtil.isNotEmpty(od_number)) {
			sql.append(" AND INSTR(od_number,:od_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY od_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_Order.class));
	}
	
	@Override
	public Integer count4Sell(Map<String, Object> params) {
		Object od_type = params.get("od_type");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object od_client_code = params.get("od_client_code");
		Object od_depot_code = params.get("od_depot_code");
		Object od_manager = params.get("od_manager");
		Object od_number = params.get("od_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_batch_order t");
		sql.append(" JOIN t_batch_client ci ON ci_code = od_client_code AND ci.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ci_sp_code = :shop_code ");//上级店铺编号
		sql.append(" AND od_isdraft = 0 ");
		sql.append(" AND od_ar_state = 1 ");
		sql.append(" AND (od_state = 0 OR od_state = 4)");
		if (StringUtil.isNotEmpty(od_type)) {
			sql.append(" AND od_type = :od_type ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND od_make_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND od_make_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(od_client_code)) {
			sql.append(" AND od_client_code = :od_client_code ");
		}
		if (StringUtil.isNotEmpty(od_depot_code)) {
			sql.append(" AND od_depot_code = :od_depot_code ");
		}
		if (StringUtil.isNotEmpty(od_manager)) {
			sql.append(" AND od_manager = :od_manager ");
		}
		if (StringUtil.isNotEmpty(od_number)) {
			sql.append(" AND INSTR(od_number,:od_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}
	
	@Override
	public List<T_Batch_Order> list4Sell(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object od_type = params.get("od_type");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object od_client_code = params.get("od_client_code");
		Object od_depot_code = params.get("od_depot_code");
		Object od_manager = params.get("od_manager");
		Object od_number = params.get("od_number");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT od_id,od_number,od_make_date,od_delivery_date,od_client_code,od_depot_code,od_maker,od_manager,od_state,od_handnumber,");
		sql.append(" od_amount,od_realamount,od_money,od_retailmoney,od_costmoney,od_remark,od_ar_state,od_ar_date,od_isdraft,od_sysdate,od_us_id,od_type,od_isprint,t.companyid,ci_name AS client_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = od_depot_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_batch_order t");
		sql.append(" JOIN t_batch_client ci ON ci_code = od_client_code AND ci.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ci_sp_code = :shop_code ");//上级店铺编号
		sql.append(" AND od_isdraft = 0 ");
		sql.append(" AND od_ar_state = 1 ");
		sql.append(" AND (od_state = 0 OR od_state = 4)");
		if (StringUtil.isNotEmpty(od_type)) {
			sql.append(" AND od_type = :od_type ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND od_make_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND od_make_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(od_client_code)) {
			sql.append(" AND od_client_code = :od_client_code ");
		}
		if (StringUtil.isNotEmpty(od_depot_code)) {
			sql.append(" AND od_depot_code = :od_depot_code ");
		}
		if (StringUtil.isNotEmpty(od_manager)) {
			sql.append(" AND od_manager = :od_manager ");
		}
		if (StringUtil.isNotEmpty(od_number)) {
			sql.append(" AND INSTR(od_number,:od_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY od_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_Order.class));
	}

	@Override
	public T_Batch_Order load(Integer od_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT od_id,od_number,od_make_date,od_delivery_date,od_client_code,od_client_shop_code,od_depot_code,od_maker,od_manager,od_state,od_handnumber,");
		sql.append(" od_amount,od_realamount,od_money,od_retailmoney,od_costmoney,od_remark,od_ar_state,od_ar_date,od_isdraft,od_sysdate,od_us_id,od_type,od_isprint,t.companyid,ci_name AS client_name,");
		sql.append(" (SELECT cis_name FROM t_batch_client_shop cis WHERE cis.cis_code = t.od_client_shop_code AND cis.cis_ci_code = t.od_client_code AND cis.companyid = t.companyid LIMIT 1) AS client_shop_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = od_depot_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_batch_order t");
		sql.append(" JOIN t_batch_client ci ON ci_code = od_client_code AND ci.companyid = t.companyid");
		sql.append(" WHERE od_id = :od_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("od_id", od_id),
					new BeanPropertyRowMapper<>(T_Batch_Order.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Batch_Order load(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT od_id,od_number,od_make_date,od_delivery_date,od_client_code,od_depot_code,od_maker,od_manager,od_state,od_handnumber,");
		sql.append(" od_amount,od_realamount,od_money,od_retailmoney,od_costmoney,od_remark,od_ar_state,od_ar_date,od_isdraft,od_sysdate,od_us_id,od_type,od_isprint,t.companyid,ci_name AS client_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = od_depot_code AND dp.companyid = t.companyid LIMIT 1) AS depot_name");
		sql.append(" FROM t_batch_order t");
		sql.append(" JOIN t_batch_client ci ON ci_code = od_client_code AND ci.companyid = t.companyid");
		sql.append(" WHERE od_number = :od_number AND t.companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("od_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Batch_Order.class));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public T_Batch_Order check(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT od_id,od_number,od_state,od_ar_state,companyid");
		sql.append(" FROM t_batch_order t");
		sql.append(" WHERE od_number = :od_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("od_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Batch_Order.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<T_Batch_OrderList> detail_list_forsavetemp(String od_number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_number,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,odl_unitprice,");
		sql.append(" odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_type,t.companyid ");
		sql.append(" FROM t_batch_orderlist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_number = :odl_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY odl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("odl_number", od_number).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}
	
	@Override
	public List<T_Batch_OrderList> detail_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_number,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,odl_unitprice,");
		sql.append(" odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_type,t.companyid,pd_no,pd_name,pd_unit, ");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = odl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = odl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = odl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_batch_orderlist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.odl_pd_code AND pd.companyid = t.companyid ");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_number = :odl_number");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY odl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}
	
	@Override
	public List<T_Batch_OrderList> detail_list_print(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT odl_id,odl_number,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,");
		sql.append(" odl_unitprice,odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_type,t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = odl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = odl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = odl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_batch_orderlist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.odl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_number = :odl_number");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY odl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}
	
	@Override
	public List<T_Batch_OrderList> detail_sum(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT odl_id,odl_number,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,SUM(odl_amount) AS odl_amount,SUM(odl_realamount) AS odl_realamount,");
		sql.append(" odl_unitprice,odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_type,t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_batch_orderlist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.odl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_number = :odl_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY odl_pd_code,odl_pi_type");
		sql.append(" ORDER BY odl_pd_code,odl_pi_type ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}
	
	@Override
	public List<String> detail_szgcode(Map<String,Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT odl_szg_code");
		sql.append(" FROM t_batch_orderlist t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND odl_number = :odl_number");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}
	
	@Override
	public List<T_Batch_OrderList> temp_list_forimport(Integer od_type,Integer us_id,Integer companyid) {//只查询商品
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,odl_unitprice,");
		sql.append(" odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_us_id,odl_type,t.companyid ");
		sql.append(" FROM t_batch_orderlist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("odl_pi_type", 0).addValue("odl_type", od_type).addValue("odl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}
	
	@Override
	public List<T_Batch_OrderList> temp_list_forsave(Integer od_type,Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,odl_unitprice,");
		sql.append(" odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_us_id,odl_type,t.companyid ");
		sql.append(" FROM t_batch_orderlist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY odl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("odl_type", od_type).addValue("odl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}
	
	@Override
	public List<T_Batch_OrderList> temp_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,odl_unitprice,");
		sql.append(" odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_us_id,odl_type,t.companyid,pd_no,pd_name,pd_unit, ");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = odl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = odl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = odl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_batch_orderlist_temp t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.odl_pd_code AND pd.companyid = t.companyid ");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY odl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}

	@Override
	public List<T_Batch_OrderList> temp_sum(Integer od_type, Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_pd_code,odl_szg_code,SUM(odl_amount) AS odl_amount,SUM(odl_realamount) AS odl_realamount,odl_unitprice, odl_retailprice,odl_costprice,");
		sql.append(" odl_remark,odl_pi_type,odl_us_id,odl_type,t.companyid,pd_no,pd_name,pd_unit,  ");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_batch_orderlist_temp t ");
		sql.append(" JOIN t_base_product pd ON pd_code = t.odl_pd_code AND pd.companyid = t.companyid");  
		sql.append(" WHERE 1=1 ");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY odl_pd_code,odl_pi_type");
		sql.append(" ORDER BY odl_pd_code,odl_pi_type ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("odl_type", od_type).addValue("odl_us_id", us_id).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
	}

	@Override
	public List<String> temp_szgcode(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT odl_szg_code");
		sql.append(" FROM t_batch_orderlist_temp t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}

	@Override
	public Integer count_product(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object alreadyExist = param.get("alreadyExist");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT count(1)");
		sql.append(" FROM (");
		sql.append(" SELECT 1 FROM t_base_product t");
		sql.append(" LEFT JOIN t_batch_orderlist_temp odl ON odl_pd_code = pd_code AND odl.companyid = t.companyid AND odl_type = :od_type AND odl_us_id = :us_id");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0)");
			}
        }
		if("1".equals(alreadyExist)){
			sql.append(" AND odl_id IS NOT NULL ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY pd_code)t");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
	}

	@Override
	public List<T_Base_Product> list_product(Map<String, Object> param) {
		Object sidx = param.get(CommonUtil.SIDX);
		Object sord = param.get(CommonUtil.SORD);
		Object searchContent = param.get("searchContent");
		Object alreadyExist = param.get("alreadyExist");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pd_id,pd_code,pd_no,pd_name,IF(odl_id IS NULL,0,1) AS exist");
		sql.append(" FROM t_base_product t");
		sql.append(" LEFT JOIN t_batch_orderlist_temp odl ON odl_pd_code = pd_code AND odl.companyid = t.companyid AND odl_type = :od_type AND odl_us_id = :us_id");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0)");
			}
        }
		if("1".equals(alreadyExist)){
			sql.append(" AND odl_id IS NOT NULL ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY pd_code");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY pd_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product.class));
	}
	
	@Override
	public T_Base_Product load_product(String pd_code,Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT pd.pd_id,pd.pd_code,pd_no,pd_name,pd_szg_code,pd_unit,pd_year,pd_season,pd_sell_price,pd_cost_price,");
		sql.append(" IFNULL(pd_batch_price,0) AS pd_batch_price,");
		sql.append(" IFNULL(pd_batch_price1,0) AS pd_batch_price1,");
		sql.append(" IFNULL(pd_batch_price2,0) AS pd_batch_price2,");
		sql.append(" IFNULL(pd_batch_price3,0) AS pd_batch_price3,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = pd.companyid LIMIT 1) AS pd_tp_name");
		sql.append(" ,(SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = pd.pd_code AND pdm.companyid = pd.companyid LIMIT 1) AS pdm_img_path");
		sql.append(" FROM t_base_product pd");
		sql.append(" WHERE 1=1");
		sql.append(" AND pd.pd_code = :pd_code");
		sql.append(" AND pd.companyid = :companyid");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("pd_code", pd_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Base_Product.class));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Double queryLastBatchPrice(String pd_code,String ci_code, Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT cp_price");
		sql.append(" FROM t_batch_clientprice");
		sql.append(" WHERE cp_pd_code = :pd_code");
		sql.append(" AND cp_ci_code = :ci_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),new MapSqlParameterSource().addValue("pd_code", pd_code).addValue("ci_code", ci_code).addValue("companyid", companyid), Double.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Map<String, Object> load_product_size(Map<String,Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//1.查询尺码信息
		List<T_Base_Size> sizes = namedParameterJdbcTemplate.query(getSizeSQL(), params, new BeanPropertyRowMapper<>(T_Base_Size.class));
		resultMap.put("sizes",sizes);
		//2.获取颜色杯型信息
		List<ProductDto> inputs = namedParameterJdbcTemplate.query(getColorBraSQL(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("inputs",inputs);
		//3.已录入数量
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT odl_sz_code AS sz_code,odl_cr_code AS cr_code,odl_br_code AS br_code,odl_amount AS amount");
		sql.append(" FROM t_batch_orderlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_pd_code = :pd_code");
		sql.append(" AND odl_us_id = :us_id");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :od_type");
		sql.append(" AND companyid = :companyid");
		List<ProductDto> temps = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("temps",temps);
		//4.库存数量
		List<ProductDto> stocks = namedParameterJdbcTemplate.query(getStockSQL(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("stocks",stocks);
		return resultMap;
	}
	
	@Override
	public Double temp_queryUnitPrice(String pd_code,String odl_pi_type, Integer od_type, Integer us_id, Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT odl_unitprice");
		sql.append(" FROM t_batch_orderlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_pd_code = :odl_pd_code");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource().addValue("odl_pd_code", pd_code)
							.addValue("odl_type", od_type)
							.addValue("odl_pi_type", odl_pi_type)
							.addValue("odl_us_id", us_id)
							.addValue("companyid", companyid), Double.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public T_Batch_OrderList temp_loadBySubCode(String sub_code,String odl_pi_type, Integer od_type, Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT odl_id,odl_pd_code,odl_sub_code,odl_sz_code,odl_szg_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,odl_unitprice,");
		sql.append(" odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_us_id,odl_type,companyid ");
		sql.append(" FROM t_batch_orderlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_sub_code = :odl_sub_code");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource()
							.addValue("odl_sub_code", sub_code)
							.addValue("odl_type", od_type)
							.addValue("odl_pi_type", odl_pi_type)
							.addValue("odl_us_id", us_id)
							.addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Batch_OrderList.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void temp_save(List<T_Batch_OrderList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_batch_orderlist_temp");
		sql.append(" (odl_pd_code,odl_sub_code,odl_szg_code,odl_sz_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,");
		sql.append(" odl_unitprice,odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_us_id,odl_type,companyid)");
		sql.append(" VALUES");
		sql.append(" (:odl_pd_code,:odl_sub_code,:odl_szg_code,:odl_sz_code,:odl_cr_code,:odl_br_code,:odl_amount,:odl_realamount,");
		sql.append(" :odl_unitprice,:odl_retailprice,:odl_costprice,:odl_remark,:odl_pi_type,:odl_us_id,:odl_type,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_save(T_Batch_OrderList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_batch_orderlist_temp");
		sql.append(" (odl_pd_code,odl_sub_code,odl_szg_code,odl_sz_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,");
		sql.append(" odl_unitprice,odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_us_id,odl_type,companyid)");
		sql.append(" VALUES");
		sql.append(" (:odl_pd_code,:odl_sub_code,:odl_szg_code,:odl_sz_code,:odl_cr_code,:odl_br_code,:odl_amount,:odl_realamount,");
		sql.append(" :odl_unitprice,:odl_retailprice,:odl_costprice,:odl_remark,:odl_pi_type,:odl_us_id,:odl_type,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp),holder);
		temp.setOdl_id(holder.getKey().longValue());
	}

	@Override
	public void temp_update(List<T_Batch_OrderList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_batch_orderlist_temp");
		sql.append(" SET odl_amount = :odl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_sub_code = :odl_sub_code");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_updateById(List<T_Batch_OrderList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_batch_orderlist_temp");
		sql.append(" SET odl_amount = :odl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_id = :odl_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_update(T_Batch_OrderList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_batch_orderlist_temp");
		sql.append(" SET odl_amount = :odl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_id = :odl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateprice(String pd_code,String odl_pi_type, Double unitPrice, Integer od_type, Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_batch_orderlist_temp");
		sql.append(" SET odl_unitprice = :odl_unitprice");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_pd_code = :odl_pd_code");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource()
						.addValue("odl_unitprice", unitPrice)
						.addValue("odl_pd_code", pd_code)
						.addValue("odl_pi_type", odl_pi_type)
						.addValue("odl_type", od_type)
						.addValue("odl_us_id", us_id)
						.addValue("companyid", companyid));
	}

	@Override
	public void temp_updateRemarkById(T_Batch_OrderList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_batch_orderlist_temp");
		sql.append(" SET odl_remark = :odl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_id = :odl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateRemarkByPdCode(T_Batch_OrderList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_batch_orderlist_temp");
		sql.append(" SET odl_remark = :odl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_pd_code = :odl_pd_code");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_del(List<T_Batch_OrderList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_batch_orderlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_sub_code = :odl_sub_code");
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}

	@Override
	public void temp_del(Integer odl_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_batch_orderlist_temp");
		sql.append(" WHERE odl_id=:odl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("odl_id", odl_id));
		
	}

	@Override
	public void temp_delByPiCode(T_Batch_OrderList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_batch_orderlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_pd_code = :odl_pd_code");
		if(StringUtil.isNotEmpty(temp.getOdl_cr_code())){
			sql.append(" AND odl_cr_code = :odl_cr_code");
		}
		if(StringUtil.isNotEmpty(temp.getOdl_br_code())){
			sql.append(" AND odl_br_code = :odl_br_code");
		}
		sql.append(" AND odl_pi_type = :odl_pi_type");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_clear(Integer od_type,Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_batch_orderlist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND odl_type = :odl_type");
		sql.append(" AND odl_us_id = :odl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("odl_type", od_type)
						.addValue("odl_us_id", us_id)
						.addValue("companyid", companyid));
	}
	
	@Override
	public List<T_Batch_Import> temp_listByImport(List<String> barCodes,String priceType,Double ci_rate,String batch_price,String ci_code,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT DISTINCT bc_pd_code,bc_subcode,bc_barcode,bc_size,pd_szg_code,bc_color,bc_bra,");
		sql.append(" pd_sell_price AS retail_price,");
		sql.append(" pd_cost_price AS cost_price,");
		if("1".equals(priceType)){//最近批发价
			sql.append(" IFNULL((SELECT cp_price FROM t_batch_clientprice cp");
			sql.append(" WHERE cp_pd_code = bc_pd_code AND cp_ci_code = :ci_code AND cp.companyid = bc.companyid),");
			sql.append(" IFNULL(pd_batch_price,0)) AS unit_price");
		}else if("2".equals(priceType)){//按照折扣率
			sql.append(" :ci_rate*pd_sell_price AS unit_price");
		}else if("3".equals(priceType)){//批发价
			if("0".equals(batch_price)){
				sql.append("pd_batch_price AS unit_price");
			}else if("1".equals(batch_price)){
				sql.append("pd_batch_price AS unit_price1");
			}else if("2".equals(batch_price)){
				sql.append("pd_batch_price AS unit_price2");
			}else if("3".equals(batch_price)){
				sql.append("pd_batch_price AS unit_price3");
			}else {
				sql.append("pd_batch_price AS unit_price");
			}
		}
		sql.append(" FROM t_base_barcode bc");
		sql.append(" JOIN t_base_product pd ON pd.pd_code = bc_pd_code AND pd.companyid = bc.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND bc_barcode IN(:barcode)");
		sql.append(" AND bc.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("barcode", barCodes)
						.addValue("ci_rate", ci_rate)
						.addValue("ci_code", ci_code)
						.addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Batch_Import.class));
	}

	@Override
	public void save(T_Batch_Order order, List<T_Batch_OrderList> details) {
		String prefix = CommonUtil.NUMBER_PREFIX_BATCH_ORDER_OUT + DateUtil.getYearMonthDateYYYYMMDD();
		if(order.getOd_type().equals(1)){
			prefix = CommonUtil.NUMBER_PREFIX_BATCH_ORDER_IN + DateUtil.getYearMonthDateYYYYMMDD();
		}
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(od_number))) AS new_number");
		sql.append(" FROM t_batch_order");
		sql.append(" WHERE 1=1");
		sql.append(" AND INSTR(od_number,:prefix) > 0");
		sql.append(" AND companyid = :companyid");
		String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", order.getCompanyid()), String.class);
		order.setOd_number(new_number);
		sql.setLength(0);
		sql.append(" INSERT INTO t_batch_order");
		sql.append(" (od_number,od_make_date,od_delivery_date,od_client_code,od_client_shop_code,od_depot_code,od_maker,od_manager,od_state,od_handnumber,od_amount,od_realamount,");
		sql.append(" od_money,od_retailmoney,od_costmoney,od_remark,od_ar_state,od_ar_date,od_isdraft,od_sysdate,od_us_id,od_type,od_isprint,companyid)");
		sql.append(" VALUES");
		sql.append(" (:od_number,:od_make_date,:od_delivery_date,:od_client_code,:od_client_shop_code,:od_depot_code,:od_maker,:od_manager,:od_state,:od_handnumber,:od_amount,:od_realamount,");
		sql.append(" :od_money,:od_retailmoney,:od_costmoney,:od_remark,:od_ar_state,:od_ar_date,:od_isdraft,:od_sysdate,:od_us_id,:od_type,:od_isprint,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(order),holder);
		order.setOd_id(holder.getKey().intValue());
		for(T_Batch_OrderList item:details){
			item.setOdl_number(order.getOd_number());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_batch_orderlist");
		sql.append(" (odl_number,odl_pd_code,odl_sub_code,odl_szg_code,odl_sz_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,");
		sql.append(" odl_unitprice,odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_type,companyid)");
		sql.append(" VALUES");
		sql.append(" (:odl_number,:odl_pd_code,:odl_sub_code,:odl_szg_code,:odl_sz_code,:odl_cr_code,:odl_br_code,:odl_amount,:odl_realamount,");
		sql.append(" :odl_unitprice,:odl_retailprice,:odl_costprice,:odl_remark,:odl_pi_type,:odl_type,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void update(T_Batch_Order order, List<T_Batch_OrderList> details) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_batch_order");
		sql.append(" SET od_make_date=:od_make_date");
		sql.append(" ,od_delivery_date=:od_delivery_date");
		sql.append(" ,od_client_code=:od_client_code");
		sql.append(" ,od_client_shop_code=:od_client_shop_code");
		sql.append(" ,od_depot_code=:od_depot_code");
		sql.append(" ,od_maker=:od_maker");
		sql.append(" ,od_manager=:od_manager");
		sql.append(" ,od_handnumber=:od_handnumber");
		sql.append(" ,od_amount=:od_amount");
		sql.append(" ,od_money=:od_money");
		sql.append(" ,od_retailmoney=:od_retailmoney");
		sql.append(" ,od_costmoney=:od_costmoney");
		sql.append(" ,od_remark=:od_remark");
		sql.append(" ,od_ar_state=:od_ar_state");
		sql.append(" ,od_ar_date=:od_ar_date");
		sql.append(" ,od_us_id=:od_us_id");
		sql.append(" ,od_isprint=:od_isprint");
		sql.append(" WHERE od_id=:od_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(order));
		for(T_Batch_OrderList item:details){
			item.setOdl_number(order.getOd_number());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_batch_orderlist");
		sql.append(" (odl_number,odl_pd_code,odl_sub_code,odl_szg_code,odl_sz_code,odl_cr_code,odl_br_code,odl_amount,odl_realamount,");
		sql.append(" odl_unitprice,odl_retailprice,odl_costprice,odl_remark,odl_pi_type,odl_type,companyid)");
		sql.append(" VALUES");
		sql.append(" (:odl_number,:odl_pd_code,:odl_sub_code,:odl_szg_code,:odl_sz_code,:odl_cr_code,:odl_br_code,:odl_amount,:odl_realamount,");
		sql.append(" :odl_unitprice,:odl_retailprice,:odl_costprice,:odl_remark,:odl_pi_type,:odl_type,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void updateApprove(T_Batch_Order order) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_batch_order");
		sql.append(" SET od_ar_state=:od_ar_state");
		sql.append(" ,od_ar_date = :od_ar_date");
		sql.append(" WHERE od_id=:od_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(order));
	}
	
	@Override
	public void updateStop(T_Batch_Order order) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_batch_order");
		sql.append(" SET od_state=:od_state");
		sql.append(" WHERE od_id=:od_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(order));
	}
	
	@Override
	public void del(String od_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_batch_order");
		sql.append(" WHERE od_number=:od_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("od_number", od_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_batch_orderlist");
		sql.append(" WHERE odl_number=:odl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("odl_number", od_number).addValue("companyid", companyid));
	}
	
	@Override
	public void deleteList(String od_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_batch_orderlist");
		sql.append(" WHERE odl_number=:odl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("odl_number", od_number).addValue("companyid", companyid));
	}
	
}
