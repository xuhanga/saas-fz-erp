package zy.dao.batch.prepay;

import java.util.List;
import java.util.Map;

import zy.entity.batch.prepay.T_Batch_Prepay;

public interface BatchPrepayDAO {
	Integer count(Map<String,Object> params);
	List<T_Batch_Prepay> list(Map<String,Object> params);
	T_Batch_Prepay load(Integer pp_id);
	T_Batch_Prepay load(String number,Integer companyid);
	T_Batch_Prepay check(String number,Integer companyid);
	void save(T_Batch_Prepay prepay);
	void update(T_Batch_Prepay prepay);
	void updateApprove(T_Batch_Prepay prepay);
	void del(String pp_number, Integer companyid);
}
