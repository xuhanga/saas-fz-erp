package zy.dao.buy.dealings;

import java.util.List;
import java.util.Map;

import zy.entity.buy.dealings.T_Buy_Dealings;

public interface BuyDealingsDAO {
	Integer count(Map<String,Object> params);
	List<T_Buy_Dealings> list(Map<String,Object> params);
	void save(T_Buy_Dealings dealings);
}
