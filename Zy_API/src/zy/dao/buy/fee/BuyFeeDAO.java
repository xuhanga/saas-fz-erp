package zy.dao.buy.fee;

import java.util.List;
import java.util.Map;

import zy.dto.buy.fee.BuyFeeReportDto;
import zy.entity.buy.fee.T_Buy_Fee;
import zy.entity.buy.fee.T_Buy_FeeList;

public interface BuyFeeDAO {
	Integer count(Map<String,Object> params);
	List<T_Buy_Fee> list(Map<String,Object> params);
	T_Buy_Fee load(Integer fe_id);
	T_Buy_Fee load(String number,Integer companyid);
	T_Buy_Fee check(String number,Integer companyid);
	List<T_Buy_FeeList> temp_list(Map<String, Object> params);
	List<String> temp_check(Integer us_id,Integer companyid);
	void temp_save(List<T_Buy_FeeList> temps);
	void temp_updateMoney(T_Buy_FeeList temp);
	void temp_updateRemark(T_Buy_FeeList temp);
	void temp_del(Integer fel_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Buy_FeeList> detail_list(Map<String, Object> params);
	List<T_Buy_FeeList> detail_list_forsavetemp(String fe_number,Integer companyid);
	void save(T_Buy_Fee fee, List<T_Buy_FeeList> details);
	void update(T_Buy_Fee fee, List<T_Buy_FeeList> details);
	void updateApprove(T_Buy_Fee fee);
	void del(String fe_number, Integer companyid);
	void deleteList(String fe_number, Integer companyid);
	List<BuyFeeReportDto> listReport(Map<String, Object> params);
	List<BuyFeeReportDto> listReportDetail(Map<String, Object> params);
}
