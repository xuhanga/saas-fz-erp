package zy.dao.buy.prepay;

import java.util.List;
import java.util.Map;

import zy.entity.buy.prepay.T_Buy_Prepay;

public interface BuyPrepayDAO {
	Integer count(Map<String,Object> params);
	List<T_Buy_Prepay> list(Map<String,Object> params);
	T_Buy_Prepay load(Integer pp_id);
	T_Buy_Prepay load(String number,Integer companyid);
	T_Buy_Prepay check(String number,Integer companyid);
	void save(T_Buy_Prepay prepay);
	void update(T_Buy_Prepay prepay);
	void updateApprove(T_Buy_Prepay prepay);
	void del(String pp_number, Integer companyid);
}
