package zy.dao.buy.report;

import java.util.List;
import java.util.Map;

import zy.dto.buy.enter.EnterDetailReportDto;
import zy.dto.buy.enter.EnterDetailSizeReportDto;
import zy.dto.buy.enter.EnterDetailsDto;
import zy.dto.buy.enter.EnterRankDto;
import zy.dto.buy.enter.EnterReportCsbDto;
import zy.dto.buy.enter.EnterReportDto;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.type.T_Base_Type;

public interface BuyReportDAO {
	List<T_Base_Type> listTypeByUpCode(String tp_upcode,Integer companyid);
	List<T_Base_Type> listAllType(Integer companyid);
	Integer countEnterReport(Map<String,Object> params);
	Map<String, Object> sumEnterReport(Map<String, Object> params);
	List<EnterReportDto> listEnterReport(Map<String,Object> params);
	List<EnterReportCsbDto> listEnterReport_csb(Map<String, Object> params);
	Integer countEnterDetailReport(Map<String,Object> params);
	Map<String, Object> sumEnterDetailReport(Map<String, Object> params);
	List<EnterDetailReportDto> listEnterDetailReport(Map<String,Object> params);
	List<EnterDetailSizeReportDto> listEnterDetailSizeReport(Map<String, Object> params);
	List<T_Base_Size> loadProductSize(String pd_code,Integer companyid);
	Integer countEnterDetails(Map<String,Object> params);
	Map<String, Object> sumEnterDetails(Map<String, Object> params);
	List<EnterDetailsDto> listEnterDetails(Map<String,Object> params);
	Integer countEnterRank(Map<String,Object> params);
	Map<String, Object> sumEnterRank(Map<String, Object> params);
	List<EnterRankDto> listEnterRank(Map<String,Object> params);
	List<Map<String, Object>> type_level_enter(Map<String,Object> params);
}
