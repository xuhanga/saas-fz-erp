package zy.dao.buy.settle;

import java.util.List;
import java.util.Map;

import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.fee.T_Buy_Fee;
import zy.entity.buy.settle.T_Buy_Settle;
import zy.entity.buy.settle.T_Buy_SettleList;

public interface BuySettleDAO {
	Integer count(Map<String,Object> params);
	List<T_Buy_Settle> list(Map<String,Object> params);
	T_Buy_Settle load(Integer st_id);
	T_Buy_Settle load(String number, Integer companyid);
	T_Buy_Settle check(String number,Integer companyid);
	T_Buy_Settle check_settle(String sp_code, Integer companyid);
	List<T_Buy_SettleList> load_buy_forsavetemp(String sp_code,Integer companyid);
	List<T_Buy_SettleList> temp_list(Map<String, Object> params);
	List<T_Buy_SettleList> temp_list_forsave(Integer us_id,Integer companyid);
	void temp_save(List<T_Buy_SettleList> temps);
	void temp_update(List<T_Buy_SettleList> temps);
	void temp_clear(Integer us_id,Integer companyid);
	void temp_updateDiscountMoney(T_Buy_SettleList temp);
	void temp_updatePrepay(T_Buy_SettleList temp);
	void temp_updateRealMoney(T_Buy_SettleList temp);
	void temp_updateRemark(T_Buy_SettleList temp);
	void temp_updateJoin(String ids,String stl_join);
	List<T_Buy_SettleList> detail_list(String number,Integer companyid);
	void save(T_Buy_Settle settle, List<T_Buy_SettleList> details);
	void update(T_Buy_Settle settle, List<T_Buy_SettleList> details);
	void updateApprove(T_Buy_Settle settle);
	void updatePpNumber(T_Buy_Settle settle);
	List<T_Buy_Enter> listEnterBySettle(String number,Integer companyid);
	List<T_Buy_Enter> listEnterBySettle_Reverse(String number,Integer companyid);
	List<T_Buy_Fee> listFeeBySettle(String number,Integer companyid);
	List<T_Buy_Fee> listFeeBySettle_Reverse(String number,Integer companyid);
	void updateEnterBySettle(List<T_Buy_Enter> enters);
	void updateFeeBySettle(List<T_Buy_Fee> fees);
	void del(String st_number, Integer companyid);
	void deleteList(String st_number, Integer companyid);
	T_Buy_SettleList check_settle_bill(String number, Integer companyid);
}
