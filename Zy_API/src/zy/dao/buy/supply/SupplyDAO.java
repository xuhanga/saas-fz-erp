package zy.dao.buy.supply;

import java.util.List;
import java.util.Map;

import zy.dto.buy.money.SupplyMoneyDetailsDto;
import zy.entity.buy.supply.T_Buy_Supply;

public interface SupplyDAO {
	List<T_Buy_Supply> list(Map<String,Object> param);
	void save(T_Buy_Supply supply);
	void del(Integer sp_id,String sp_code,Integer companyid);
	T_Buy_Supply queryByID(Integer sp_id);
	T_Buy_Supply load(String sp_code,Integer companyid);
	void update(T_Buy_Supply supply);
	
	T_Buy_Supply loadSupply(String sp_code, Integer companyid);
	void updatePayable(T_Buy_Supply supply);
	void updatePrepay(T_Buy_Supply supply);
	void updateSettle(T_Buy_Supply supply);
	
	
	Map<String, Object> countsumMoneyDetails(Map<String, Object> params);
	List<SupplyMoneyDetailsDto> listMoneyDetails(Map<String,Object> params);
	
}
