package zy.dao.common.city;

import java.util.List;

import zy.entity.common.city.Common_City;

public interface CityDAO {
	List<Common_City> queryProvince();
	List<Common_City> queryCity(Integer id);
	List<Common_City> queryTown(Integer id);
}
