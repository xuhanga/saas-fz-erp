package zy.dao.common.kpi;

import java.util.List;
import java.util.Map;

import zy.entity.common.kpi.Common_Kpi;

public interface CommonKpiDAO {
	List<Common_Kpi> list(Map<String, Object> params);
}
