package zy.dao.common.kpi.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.common.kpi.CommonKpiDAO;
import zy.entity.common.kpi.Common_Kpi;
import zy.util.StringUtil;

@Repository
public class CommonKpiDAOImpl extends BaseDaoImpl implements CommonKpiDAO{

	@Override
	public List<Common_Kpi> list(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT t.ki_id,t.ki_code,t.ki_name,t.ki_remark,t.ki_identity,t.ki_state");
		sql.append(" FROM common_kpi t");
		sql.append(" LEFT JOIN t_sys_kpi k ON k.ki_code = t.ki_code AND k.ki_shop_code = :shop_code AND k.companyid = :companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND ki_state = 0");
		sql.append(" AND k.ki_id IS NULL");
		if (StringUtil.isNotEmpty(params.get("searchContent"))) {
			sql.append(" AND (INSTR(t.ki_code,:searchContent)>0 OR INSTR(t.ki_name,:searchContent)>0)");
		}
		return namedParameterJdbcTemplate.query(sql.toString(),params,new BeanPropertyRowMapper<>(Common_Kpi.class));
	}
	
}
