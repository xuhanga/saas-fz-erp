package zy.dao.common.log.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.common.log.CommonLogDAO;
import zy.entity.common.log.Common_Log;

@Repository
public class CommonLogDAOImpl extends BaseDaoImpl implements CommonLogDAO{
	
	@Override
	public Integer count(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1) FROM common_log");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}
	
	@Override
	public List<Common_Log> list(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT lg_id,lg_title,lg_content,lg_user,lg_sysdate");
		sql.append(" FROM common_log");
		sql.append(" ORDER BY lg_id DESC");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(),params,new BeanPropertyRowMapper<>(Common_Log.class));
	}
	
	@Override
	public List<Common_Log> list() {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT lg_id,lg_title,lg_content,lg_user,lg_sysdate");
		sql.append(" FROM common_log");
		sql.append(" ORDER BY lg_id DESC");
		sql.append(" LIMIT 10");
		return namedParameterJdbcTemplate.query(sql.toString(),new HashMap<String, Object>(),new BeanPropertyRowMapper<>(Common_Log.class));
	}
}
