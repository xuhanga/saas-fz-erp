package zy.dao.money.bank;

import java.util.List;
import java.util.Map;

import zy.entity.money.bank.T_Money_BankRun;

public interface BankRunDAO {
	Integer count(Map<String,Object> params);
	List<T_Money_BankRun> list(Map<String,Object> params);
	void save(T_Money_BankRun bankRun);
	void save(List<T_Money_BankRun> bankRuns);
	List<T_Money_BankRun> report_sum(Map<String, Object> params);
}
