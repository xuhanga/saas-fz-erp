package zy.dao.money.income;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dto.money.income.IncomeReportDto;
import zy.entity.money.income.T_Money_Income;
import zy.entity.money.income.T_Money_IncomeList;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class IncomeDAOImpl extends BaseDaoImpl implements IncomeDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object ic_ar_state = params.get("ic_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object ic_shop_code = params.get("ic_shop_code");
		Object ic_ba_code = params.get("ic_ba_code");
		Object ic_manager = params.get("ic_manager");
		Object ic_number = params.get("ic_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_base_shop sp ON sp_code = ic_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if(ic_shop_code != null && !"".equals(ic_shop_code)){
				sql.append(" AND ic_shop_code = :ic_shop_code");
			}
		}else{//加盟店
			sql.append(" JOIN common_type on ty_id=sp_shop_type");
			sql.append(" WHERE 1 = 1");
			sql.append(" AND ic_shop_code = :shop_code");
		}
		if (StringUtil.isNotEmpty(ic_ar_state)) {
			sql.append(" AND ic_ar_state = :ic_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND ic_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND ic_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(ic_ba_code)) {
			sql.append(" AND ic_ba_code = :ic_ba_code ");
		}
		if (StringUtil.isNotEmpty(ic_manager)) {
			sql.append(" AND ic_manager = :ic_manager ");
		}
		if (StringUtil.isNotEmpty(ic_number)) {
			sql.append(" AND INSTR(ic_number,:ic_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Money_Income> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		Object ic_ar_state = params.get("ic_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object ic_shop_code = params.get("ic_shop_code");
		Object ic_ba_code = params.get("ic_ba_code");
		Object ic_manager = params.get("ic_manager");
		Object ic_number = params.get("ic_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ic_id,ic_number,ic_shop_code,ic_date,ic_manager,ic_ba_code,ic_money,ic_us_id,ic_sysdate,ic_ar_state,ic_maker,ic_remark,sp.sp_name as shop_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba.ba_code=t.ic_ba_code AND ba.companyid=t.companyid LIMIT 1) AS ba_name ");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_base_shop sp ON sp_code = ic_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if(ic_shop_code != null && !"".equals(ic_shop_code)){
				sql.append(" AND ic_shop_code = :ic_shop_code");
			}
		}else{//加盟店
			sql.append(" JOIN common_type on ty_id=sp_shop_type");
			sql.append(" WHERE 1 = 1");
			sql.append(" AND ic_shop_code = :shop_code");
		}
		if (StringUtil.isNotEmpty(ic_ar_state)) {
			sql.append(" AND ic_ar_state = :ic_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND ic_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND ic_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(ic_ba_code)) {
			sql.append(" AND ic_ba_code = :ic_ba_code ");
		}
		if (StringUtil.isNotEmpty(ic_manager)) {
			sql.append(" AND ic_manager = :ic_manager ");
		}
		if (StringUtil.isNotEmpty(ic_number)) {
			sql.append(" AND INSTR(ic_number,:ic_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY ic_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Money_Income.class));
	}

	@Override
	public T_Money_Income load(Integer ic_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ic_id,ic_number,ic_shop_code,ic_date,ic_manager,ic_ba_code,ic_money,ic_us_id,ic_sysdate,ic_ar_state,ic_maker,ic_remark,sp.sp_name as shop_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba.ba_code=t.ic_ba_code AND ba.companyid=t.companyid LIMIT 1) AS ba_name ");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_base_shop sp ON sp_code = ic_shop_code AND sp.companyid = t.companyid");
		sql.append(" WHERE ic_id = :ic_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("ic_id", ic_id),
					new BeanPropertyRowMapper<>(T_Money_Income.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Money_Income load(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ic_id,ic_number,ic_shop_code,ic_date,ic_manager,ic_ba_code,ic_money,ic_us_id,ic_sysdate,ic_ar_state,ic_maker,ic_remark,sp.sp_name as shop_name,");
		sql.append(" (SELECT ba_name FROM t_money_bank ba WHERE ba.ba_code=t.ic_ba_code AND ba.companyid=t.companyid LIMIT 1) AS ba_name ");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_base_shop sp ON sp_code = ic_shop_code AND sp.companyid = t.companyid");
		sql.append(" WHERE ic_number = :ic_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("ic_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Money_Income.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Money_Income check(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ic_id,ic_number,ic_shop_code,ic_date,ic_manager,ic_ba_code,ic_money,ic_us_id,ic_sysdate,ic_ar_state,ic_maker,ic_remark");
		sql.append(" FROM t_money_income t");
		sql.append(" WHERE ic_number = :ic_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("ic_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Money_Income.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<T_Money_IncomeList> temp_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT icl_id,icl_mp_code,icl_money,icl_us_id,icl_remark,companyid,");
		sql.append(" (SELECT pp_name FROM t_money_property pp WHERE pp_type = 1 AND pp_code = icl_mp_code AND pp.companyid = t.companyid LIMIT 1) AS mp_name");
		sql.append(" FROM t_money_incomelist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND icl_us_id = :icl_us_id");
		sql.append(" AND companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY icl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Money_IncomeList.class));
	}

	@Override
	public List<String> temp_check(Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT icl_mp_code");
		sql.append(" FROM t_money_incomelist_temp t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND icl_us_id = :icl_us_id");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), new MapSqlParameterSource().addValue("icl_us_id", us_id).addValue("companyid", companyid), String.class);
	}

	@Override
	public void temp_save(List<T_Money_IncomeList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_money_incomelist_temp");
		sql.append(" (icl_mp_code,icl_money,icl_remark,icl_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:icl_mp_code,:icl_money,:icl_remark,:icl_us_id,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}

	@Override
	public void temp_updateMoney(T_Money_IncomeList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_money_incomelist_temp");
		sql.append(" SET icl_money = :icl_money");
		sql.append(" WHERE 1=1");
		sql.append(" AND icl_id = :icl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}

	@Override
	public void temp_updateRemark(T_Money_IncomeList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_money_incomelist_temp");
		sql.append(" SET icl_remark = :icl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND icl_id = :icl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}

	@Override
	public void temp_del(Integer icl_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_money_incomelist_temp");
		sql.append(" WHERE icl_id=:icl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("icl_id", icl_id));
	}

	@Override
	public void temp_clear(Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_money_incomelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND icl_us_id = :icl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("icl_us_id", us_id).addValue("companyid", companyid));
	}

	@Override
	public List<T_Money_IncomeList> detail_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT icl_id,icl_number,icl_mp_code,icl_money,icl_remark,companyid,");
		sql.append(" (SELECT pp_name FROM t_money_property pp WHERE pp_type = 1 AND pp_code = icl_mp_code AND pp.companyid = t.companyid LIMIT 1) AS mp_name");
		sql.append(" FROM t_money_incomelist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND icl_number = :icl_number");
		sql.append(" AND companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY icl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Money_IncomeList.class));
	}
	
	@Override
	public List<T_Money_IncomeList> detail_list_forsavetemp(String ic_number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT icl_id,icl_number,icl_mp_code,icl_money,icl_remark,companyid");
		sql.append(" FROM t_money_incomelist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND icl_number = :icl_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" ORDER BY icl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), 
				new MapSqlParameterSource().addValue("icl_number", ic_number).addValue("companyid", companyid), 
				new BeanPropertyRowMapper<>(T_Money_IncomeList.class));
	}
	
	@Override
	public void save(T_Money_Income income, List<T_Money_IncomeList> details) {
		String prefix = CommonUtil.NUMBER_PREFIX_INCOME + DateUtil.getYearMonthDateYYYYMMDD();
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(ic_number))) AS new_number");
		sql.append(" FROM t_money_income");
		sql.append(" WHERE 1=1");
		sql.append(" AND INSTR(ic_number,:prefix) > 0");
		sql.append(" AND companyid = :companyid");
		String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", income.getCompanyid()), String.class);
		income.setIc_number(new_number);
		sql.setLength(0);
		sql.append(" INSERT INTO t_money_income");
		sql.append(" (ic_number,ic_shop_code,ic_date,ic_manager,ic_ba_code,ic_money,ic_us_id,");
		sql.append(" ic_sysdate,ic_ar_state,ic_maker,ic_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:ic_number,:ic_shop_code,:ic_date,:ic_manager,:ic_ba_code,:ic_money,:ic_us_id,");
		sql.append(" :ic_sysdate,:ic_ar_state,:ic_maker,:ic_remark,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(income),holder);
		income.setIc_id(holder.getKey().intValue());
		for(T_Money_IncomeList item:details){
			item.setIcl_number(income.getIc_number());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_money_incomelist");
		sql.append(" (icl_number,icl_mp_code,icl_money,icl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:icl_number,:icl_mp_code,:icl_money,:icl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void update(T_Money_Income income, List<T_Money_IncomeList> details) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_money_income");
		sql.append(" SET ic_date=:ic_date");
		sql.append(" ,ic_shop_code=:ic_shop_code");
		sql.append(" ,ic_maker=:ic_maker");
		sql.append(" ,ic_manager=:ic_manager");
		sql.append(" ,ic_money=:ic_money");
		sql.append(" ,ic_remark=:ic_remark");
		sql.append(" ,ic_ar_state=:ic_ar_state");
		sql.append(" ,ic_ar_date=:ic_ar_date");
		sql.append(" ,ic_us_id=:ic_us_id");
		sql.append(" WHERE ic_id=:ic_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(income));
		for(T_Money_IncomeList item:details){
			item.setIcl_number(income.getIc_number());
		}
		sql.setLength(0);
		sql.append("INSERT INTO t_money_incomelist");
		sql.append(" (icl_number,icl_mp_code,icl_money,icl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:icl_number,:icl_mp_code,:icl_money,:icl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void updateApprove(T_Money_Income income) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_money_income");
		sql.append(" SET ic_ar_state=:ic_ar_state");
		sql.append(" ,ic_ar_date = :ic_ar_date");
		sql.append(" WHERE ic_id=:ic_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(income));
	}
	
	@Override
	public void del(String ic_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_money_income");
		sql.append(" WHERE ic_number=:ic_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ic_number", ic_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_money_incomelist");
		sql.append(" WHERE icl_number=:icl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("icl_number", ic_number).addValue("companyid", companyid));
	}
	
	@Override
	public void deleteList(String ic_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_money_incomelist");
		sql.append(" WHERE icl_number=:icl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("icl_number", ic_number).addValue("companyid", companyid));
	}

	private String getReportSQL(Map<String, Object> params){
		StringBuffer sql = new StringBuffer();
		if(StringUtil.isNotEmpty(params.get("month"))){
			sql.append(" AND MONTH(ic_date) = :month ");
		}
		if(StringUtil.isNotEmpty(params.get("icl_mp_code"))){
			sql.append(" AND icl_mp_code = :icl_mp_code ");
		}
		if(StringUtil.isNotEmpty(params.get("ic_shop_code"))){
			sql.append(" AND ic_shop_code = :ic_shop_code ");
		}
		if(StringUtil.isNotEmpty(params.get("begindate"))){
			sql.append(" AND ic_date >= :begindate ");
		}
		if(StringUtil.isNotEmpty(params.get("enddate"))){
			sql.append(" AND ic_date <= :enddate ");
		}
		sql.append(" AND ic_ar_state = 1");
		sql.append(" AND t.companyid = :companyid");
		return sql.toString();
	}
	
	@Override
	public List<IncomeReportDto> listReport(Map<String, Object> params) {
		String type = StringUtil.trimString(params.get("type"));
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ");
		if("shop".equals(type)){
			sql.append(" ic_shop_code AS code,sp_name AS name,");
		}else if("property".equals(type)){
			sql.append(" icl_mp_code AS code,");
			sql.append(" (SELECT pp_name FROM t_money_property pp WHERE pp_type = 1 AND pp_code = icl_mp_code AND pp.companyid = t.companyid LIMIT 1) AS name,");
		}else if("month".equals(type)){
			sql.append(" MONTH(ic_date) AS code,CONCAT(MONTH(ic_date),'月份') AS name,");
		}
		sql.append(" SUM(icl_money) AS money");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_money_incomelist icl ON icl_number = ic_number AND icl.companyid = t.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = ic_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
		}else{//加盟店、自营店、合伙店
			sql.append(" WHERE 1 = 1");
			sql.append(" AND ic_shop_code = :shop_code");
		}
		sql.append(getReportSQL(params));
		if("shop".equals(type)){
			sql.append(" GROUP BY ic_shop_code");
		}else if("property".equals(type)){
			sql.append(" GROUP BY icl_mp_code");
		}else if("month".equals(type)){
			sql.append(" GROUP BY MONTH(ic_date)");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(IncomeReportDto.class));
	}

	@Override
	public List<T_Money_IncomeList> listReportDetail(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ");
		sql.append(" icl_id,icl_mp_code,icl_money,icl_remark,ic_date,");
		sql.append(" (SELECT pp_name FROM t_money_property pp WHERE pp_type = 1 AND pp_code = icl_mp_code AND pp.companyid = t.companyid LIMIT 1) AS mp_name");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_money_incomelist icl ON icl_number = ic_number AND icl.companyid = t.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = ic_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
		}else{//加盟店、自营店、合伙店
			sql.append(" WHERE 1 = 1");
			sql.append(" AND ic_shop_code = :shop_code");
		}
		sql.append(getReportSQL(params));
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY icl_id ASC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Money_IncomeList.class));
	}

	@Override
	public List<T_Money_Income> monthMoney(Map<String, Object> paramMap) {
		String shop_type = StringUtil.trimString(paramMap.get(CommonUtil.SHOP_TYPE));
		Object date = paramMap.get("date");
		Object sh_shop_code = paramMap.get("sh_shop_code");
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT DAY(ic_date) ic_date,SUM(ic_money) ic_money");
		sql.append(" FROM t_money_income t");
		sql.append(" JOIN t_base_shop s");
		sql.append(" ON s.sp_code=t.ic_shop_code");
		sql.append(" AND s.companyid=t.companyid");
		if(StringUtil.isNotEmpty(sh_shop_code)){
			paramMap.put("shop_codes", StringUtil.parseList(StringUtil.trimString(sh_shop_code)));
			sql.append(" AND sp_code IN (:shop_codes)");
		}
		if("1".equals(shop_type) || "2".equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 1));
		}else{//自营、加盟、合伙
			sql.append(" AND sp_code=:shop_code");
		}
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(date)){
			sql.append(" AND INSTR(ic_date,:date)>0");
		}
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY DAY(ic_date)");
		return namedParameterJdbcTemplate.query(sql.toString(), paramMap, 
				new BeanPropertyRowMapper<>(T_Money_Income.class));
	}
	
}
