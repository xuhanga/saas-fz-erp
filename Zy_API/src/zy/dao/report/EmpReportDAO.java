package zy.dao.report;

import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpSortDto;

public interface EmpReportDAO {
	Integer queryEmpSort(Map<String, Object> params);
	List<EmpSortDto> listEmpSort(Map<String, Object> params);
	List<EmpSortDto> listEmpDealCount(Map<String, Object> params);
	Map<String, Object> querySellByEmp(Map<String, Object> params);
	Map<String, Object> queryDealCountByEmp(Map<String, Object> params);
	Integer countCome(Map<String, Object> params);
	Integer countReceiveByEmp(Map<String, Object> params);
	Integer countTryByEmp(Map<String, Object> params);
	Integer countVipByEmp(Map<String, Object> params);
}
