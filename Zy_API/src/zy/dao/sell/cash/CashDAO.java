package zy.dao.sell.cash;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cash.T_Sell_Pay_Temp;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.shop.sale.T_Shop_Sale_Present;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.NumberForm;
import zy.form.StringForm;
import zy.form.shop.SaleForm;

public interface CashDAO {
	List<StringForm> putUpList(Map<String,Object> param);
	public List<T_Sell_Shop_Temp> listTemp(Map<String, Object> param);
	public void updateEmp(Map<String, Object> param);
	public void updateArea(Map<String, Object> param);
	
	public NumberForm countSell(Map<String, Object> param);
	public List<T_Sell_Shop_Temp> listSell(Map<String, Object> param);
	public Map<String, Object>	productSize(Map<String, Object> param);
	public T_Sell_Shop_Temp product(Map<String, Object> param);
	
	public NumberForm countBack(Map<String, Object> param);
	public List<T_Sell_Shop_Temp> listBack(Map<String, Object> param);
	
	void saveTemp(List<T_Sell_Shop_Temp> tempList);
	void saveTemp(T_Sell_Shop_Temp temp);
	void updateTemp(List<T_Sell_Shop_Temp> tempList);
	void delTemp(Map<String,Object> param);
	void clearTemp(Map<String,Object> param);
	void delGift(Map<String,Object> param);
	void saveGift(Map<String,Object> param);
	public T_Sell_Shop_Temp tempByID(Map<String, Object> param);
	T_Sell_Shop_Temp subCode(Map<String, Object> param);
	Integer queryNumber(Map<String, Object> param);
	List<T_Sell_Shop_Temp> queryTemp(Map<String, Object> param);
	//查询会员信息
	T_Vip_Member queryVip(Map<String,Object> param);
	//查询会员的商品类别折扣
	List<NumberForm> vipType(Map<String,Object> param);
	//查询会员的商品类别折扣
	List<NumberForm> vipBrand(Map<String,Object> param);
	
	List<SaleForm> querySale(Map<String,Object> param);
	List<T_Shop_Sale_Present> queryPersent(Map<String,Object> param);
	
	T_Sell_Shop_Temp putDown(Map<String,Object> param);
	void putUp(Map<String,Object> param);
	public void to_cash(Map<String, Object> param);
	
	String cashNumber(Map<String,Object> param);
	T_Sell_Shop sumTemp(Map<String,Object> param);
	void save(T_Sell_Shop sell);
	void saveList(Map<String,Object> param);
	void updateDeposit(Map<String,Object> param);
	void updateStock(Map<String,Object> param);
	void savePay(T_Sell_Pay_Temp pay);
	void delPay(Map<String,Object> param);
	List<T_Sell_Pay_Temp> payList(Map<String, Object> param);
}
