package zy.dao.sell.cashier;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.cashier.T_Sell_Cashier_Set;
import zy.form.StringForm;

public interface CashierDAO {
	Integer count(Map<String,Object> param);
	List<T_Sell_Cashier> list(Map<String,Object> param);
	T_Sell_Cashier queryByID(Integer id);
	void save(T_Sell_Cashier model);
	void update(T_Sell_Cashier model);
	void updatePwd(T_Sell_Cashier model);
	void del(Integer id);
	Integer idByCode(T_Sell_Cashier model);
	void saveLimit(T_Sell_Cashier model);
	List<T_Sell_Cashier_Set> limitByCode(Map<String,Object> param);
	void updateLimit(Map<String,Object> param);
	List<StringForm> listCash(Map<String,Object> param);
}
