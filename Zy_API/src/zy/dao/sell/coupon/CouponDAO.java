package zy.dao.sell.coupon;

import java.util.List;
import java.util.Map;

import zy.entity.sell.coupon.T_Sell_Coupon;
import zy.entity.sell.coupon.T_Sell_Coupon_Detail;

public interface CouponDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_Coupon> list(Map<String,Object> params);
	List<T_Sell_Coupon_Detail> listDetail(String sc_number,Integer sc_type,Integer companyid);
	void save(Map<String,Object> params);
	void update(Map<String,Object> params);
	T_Sell_Coupon check(String sc_number,Integer companyid);
	void del(String sc_number,Integer companyid);
	T_Sell_Coupon load(Integer sc_id);
	void updateApprove(T_Sell_Coupon coupon);
}
