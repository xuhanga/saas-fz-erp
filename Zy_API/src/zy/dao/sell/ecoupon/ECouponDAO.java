package zy.dao.sell.ecoupon;

import java.util.List;
import java.util.Map;

import zy.dto.sell.ecoupon.SellECouponDto;
import zy.entity.sell.ecoupon.T_Sell_ECoupon;
import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Brand;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Product;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Shop;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Type;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;

public interface ECouponDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_ECoupon> list(Map<String,Object> params);
	T_Sell_ECoupon load(Integer ec_id);
	List<T_Sell_ECouponList> listDetail(String number,Integer companyid);
	List<T_Sell_ECouponList> temp(Integer us_id,Integer companyid);
	T_Sell_ECouponList temp_load(Integer ecl_id);
	void temp_save(T_Sell_ECouponList temp);
	void temp_save(List<T_Sell_ECouponList> temps);
	void temp_update(T_Sell_ECouponList temp);
	void temp_del(Integer ecl_id);
	void temp_del(Integer us_id,Integer companyid);
	void save(T_Sell_ECoupon eCoupon);
	void update(T_Sell_ECoupon eCoupon);
	void saveList(List<T_Sell_ECouponList> details);
	void saveShop(List<T_Sell_ECoupon_Shop> shops);
	void saveBrand(List<T_Sell_ECoupon_Brand> brands);
	void saveProduct(List<T_Sell_ECoupon_Product> products);
	void saveType(List<T_Sell_ECoupon_Type> types);
	void deleteList(String number,Integer companyid);
	void deleteShop(String number,Integer companyid);
	void deleteBrand(String number,Integer companyid);
	void deleteProduct(String number,Integer companyid);
	void deleteType(String number,Integer companyid);
	T_Sell_ECoupon check(String number,Integer companyid);
	T_Sell_ECoupon check(Integer ec_id);
	void approve(T_Sell_ECoupon eCoupon);
	void del(Integer ec_id);
	Integer user_count(Map<String,Object> paramMap);
	List<T_Sell_Ecoupon_User> user_list(Map<String,Object> paramMap);
	
	//========================前台－－－－－－－－－－－－－－－
	List<T_Sell_Ecoupon_User> queryByCode(Map<String,Object> param);
	void updateEcoupon(Map<String,Object> param);
	List<T_Sell_ECoupon> queryEcoupon(Map<String,Object> param);
	T_Sell_Ecoupon_User allEcoupon(Map<String, Object> param);
	List<T_Sell_ECouponList> typeEcoupon(Map<String, Object> param);
	List<T_Sell_ECouponList> brandEcoupon(Map<String,Object> param);
	List<T_Sell_ECouponList> productEcoupon(Map<String,Object> param);
	void save(T_Sell_Ecoupon_User user);
	
	
	List<SellECouponDto> listEcoupon4Push(Map<String,Object> params);
	T_Sell_ECouponList loadDetail(String ecl_code,Integer companyid);
	Integer countECouponReceived(String ecu_code,Integer companyid);
	
}
