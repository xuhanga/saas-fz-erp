package zy.dao.sell.sell;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;

public interface SellDAO {
	Map<String,Object> countShop(Map<String,Object> param);
	List<T_Sell_Shop> listShop(Map<String,Object> param);
	Integer countShopList(Map<String,Object> param);
	List<T_Sell_ShopList> listShopList(Map<String,Object> param);
	T_Sell_Shop queryByID(Map<String,Object> param);
	List<T_Sell_ShopList> listSell(T_Sell_Shop sell);
}
