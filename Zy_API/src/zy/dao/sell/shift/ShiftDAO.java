package zy.dao.sell.shift;

import java.util.List;
import java.util.Map;

import zy.entity.sell.shift.T_Sell_Shift;

public interface ShiftDAO {
	Integer count(Map<String,Object> params);
	List<T_Sell_Shift> list(Map<String,Object> params);
	public List<T_Sell_Shift> listShop(Map<String, Object> param);
	T_Sell_Shift queryByID(Integer st_id);
	String queryMaxStcode(Integer companyid);
	void save(List<T_Sell_Shift> shifts);
	void update(T_Sell_Shift shift);
	void del(Integer st_id);
}
