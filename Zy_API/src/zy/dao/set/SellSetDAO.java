package zy.dao.set;

import java.util.List;
import java.util.Map;

import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_PrintData;
import zy.entity.sell.set.T_Sell_PrintField;
import zy.entity.sell.set.T_Sell_PrintSet;
import zy.entity.sell.set.T_Sell_Set;

public interface SellSetDAO {
	public List<T_Sell_Set> cashSet(Map<String,Object> param);
	public void updateCash(Map<String, Object> param);
	Map<String,Object> print(Map<String,Object> param);
	Map<String,Object> sysprint(Map<String,Object> param);
	void savePrint(T_Sell_Print print, List<T_Sell_PrintField> printFields, List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets);
	void updatePrint(T_Sell_Print print, List<T_Sell_PrintField> printFields, List<T_Sell_PrintData> printDatas, List<T_Sell_PrintSet> printSets);
}
