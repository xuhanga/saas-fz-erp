package zy.dao.shop.complaint;

import java.util.List;
import java.util.Map;

import zy.entity.shop.complaint.T_Shop_Complaint;

public interface ComplaintDAO {
	Integer count(Map<String,Object> param);
	List<T_Shop_Complaint> list(Map<String,Object> param);
	T_Shop_Complaint load(Integer sc_id);
	void update(T_Shop_Complaint complaint);
}
