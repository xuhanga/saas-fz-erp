package zy.dao.shop.complaint.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.shop.complaint.ComplaintDAO;
import zy.entity.shop.complaint.T_Shop_Complaint;
import zy.util.CommonUtil;

@Repository
public class ComplaintDAOImpl extends BaseDaoImpl implements ComplaintDAO {
    
	@Override
	public Integer count(Map<String, Object> param) {
		Object begindate = param.get("begindate");
		Object enddate = param.get("enddate");
		Object sc_username = param.get("sc_username");
		Object sc_processname = param.get("sc_processname");
		Object sc_shop_code = param.get("sc_shop_code");
		Object sc_satis = param.get("sc_satis");
		Object sc_state = param.get("sc_state");
		Object sc_type = param.get("sc_type");
        Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_shop_complaint t ");
		sql.append(" JOIN t_base_shop sp ON sp.sp_code=t.sc_shop_code AND sp.companyid=t.companyid ");
		sql.append(" where 1 = 1");
		if (null != sc_username && !"".equals(sc_username)) {
			sql.append(" and instr(t.sc_username,:sc_username)>0 ");
		}
		if (null != sc_processname && !"".equals(sc_processname)) {
			sql.append(" and instr(t.sc_processname,:sc_processname)>0 ");
		}
		if (null != sc_shop_code && !"".equals(sc_shop_code)) {
			sql.append(" and t.sc_shop_code = :sc_shop_code ");
		}
		if (null != sc_satis && !"".equals(sc_satis)) {
			sql.append(" and  t.sc_satis = :sc_satis");
		}
		if (null != sc_state && !"".equals(sc_state)) {
			sql.append(" and  t.sc_state = :sc_state");
		}
		if (null != sc_type && !"".equals(sc_type)) {
			sql.append(" and  t.sc_type = :sc_type");
		}
		if (null != begindate && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" and DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') >=:begindate");
			sql.append(" and DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') <=:enddate");
		}
		if (shop_type == CommonUtil.ONE || shop_type == CommonUtil.TWO) {
			sql.append(" AND (sp.sp_upcode=:shop_code and (sp.sp_shop_type='"+CommonUtil.THREE+"' or sp.sp_shop_type='"+CommonUtil.FIVE+"'))");
		}else {
			sql.append(" and  t.sc_shop_code = :shop_code");
		}
		sql.append(" and t.companyid=:companyid");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Shop_Complaint> list(Map<String, Object> param) {
		Object start = param.get("start");
		Object end = param.get("end");
		Object begindate = param.get("begindate");
		Object enddate = param.get("enddate");
		Object sc_username = param.get("sc_username");
		Object sc_processname = param.get("sc_processname");
		Object sc_shop_code = param.get("sc_shop_code");
		Object sc_satis = param.get("sc_satis");
		Object sc_state = param.get("sc_state");
		Object sc_type = param.get("sc_type");
        Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer("");
		sql.append(" select sc_id,sc_username,sc_mobile,sc_sysdate,sc_content,sc_processname,sc_acceptdate,sc_processinfo,sc_state,sc_shop_code,sc_us_id,");
		sql.append(" sc_satis,sc_type,sc_enddate,sc_selman,sp.sp_name as sc_shop_name ");
		sql.append(" from t_shop_complaint t ");
		sql.append(" JOIN t_base_shop sp ON sp.sp_code=t.sc_shop_code AND sp.companyid=t.companyid ");
		sql.append(" where 1 = 1");
		if (null != sc_username && !"".equals(sc_username)) {
			sql.append(" and instr(t.sc_username,:sc_username)>0 ");
		}
		if (null != sc_processname && !"".equals(sc_processname)) {
			sql.append(" and instr(t.sc_processname,:sc_processname)>0 ");
		}
		if (null != sc_shop_code && !"".equals(sc_shop_code)) {
			sql.append(" and t.sc_shop_code = :sc_shop_code ");
		}
		if (null != sc_satis && !"".equals(sc_satis)) {
			sql.append(" and  t.sc_satis = :sc_satis");
		}
		if (null != sc_state && !"".equals(sc_state)) {
			sql.append(" and  t.sc_state = :sc_state");
		}
		if (null != sc_type && !"".equals(sc_type)) {
			sql.append(" and  t.sc_type = :sc_type");
		}
		if (null != begindate && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" and DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') >=:begindate");
			sql.append(" and DATE_FORMAT(t.sc_sysdate,'%Y-%m-%d') <=:enddate");
		}
		if (shop_type == CommonUtil.ONE || shop_type == CommonUtil.TWO) {
			sql.append(" AND (sp.sp_upcode=:shop_code and (sp.sp_shop_type='"+CommonUtil.THREE+"' or sp.sp_shop_type='"+CommonUtil.FIVE+"'))");
		}else {
			sql.append(" and  t.sc_shop_code = :shop_code");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" order by sc_id desc");
		if(null != start && !"".equals(start) && null != end && !"".equals(end)){
			sql.append(" limit :start,:end");
		}
		List<T_Shop_Complaint> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Complaint.class));
		return list;
	}

	@Override
	public T_Shop_Complaint load(Integer sc_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" select sc_id,sc_username,sc_mobile,sc_sysdate,sc_content,sc_processname,sc_acceptdate,sc_processinfo,sc_state,sc_shop_code,sc_us_id,");
		sql.append(" sc_satis,sc_type,sc_enddate,sc_selman ");
		sql.append(" from t_shop_complaint t ");
		sql.append(" where 1 = 1");
		sql.append(" and sc_id = :sc_id");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("sc_id", sc_id),
				new BeanPropertyRowMapper<>(T_Shop_Complaint.class));
	}

	@Override
	public void update(T_Shop_Complaint complaint) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" update t_shop_complaint ");
		sql.append("set sc_processname=:sc_processname,sc_acceptdate=:sc_acceptdate,sc_processinfo=:sc_processinfo,sc_state=:sc_state ");
		sql.append(" where sc_id=:sc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(complaint));
	}

}
