package zy.dao.shop.deposit;

import java.util.List;
import java.util.Map;

import zy.entity.sell.deposit.T_Sell_Deposit;
import zy.entity.sell.deposit.T_Sell_DepositList;

public interface DepositDAO {
	Integer count(Map<String,Object> param);
	List<T_Sell_DepositList> list(Map<String,Object> param);
	public void payDeposit(Map<String, Object> param,List<T_Sell_DepositList> list);
	void queryNumber(Map<String,Object> param);
	List<T_Sell_Deposit> getDeposit(Map<String, Object> param);
	void take(Map<String, Object> param);
	public void back(Map<String, Object> param);
}
