package zy.dao.shop.gift;

import java.util.List;
import java.util.Map;

import zy.entity.base.product.T_Base_Product;
import zy.entity.shop.gift.T_Shop_Gift;
import zy.entity.shop.gift.T_Shop_GiftList;
import zy.entity.shop.gift.T_Shop_Gift_Run;
import zy.entity.shop.gift.T_Shop_Gift_Send;
import zy.entity.sys.user.T_Sys_User;

public interface GiftDAO {
	Integer count(Map<String,Object> param);
	List<T_Shop_Gift> list(Map<String,Object> param);
	void deleteTemp(Map<String,Object> param);
	List<T_Shop_GiftList> temp_list(Map<String,Object> param);
	List<T_Shop_GiftList> temp_sum(String us_code, Integer companyid);
	List<String> temp_szgcode(Map<String,Object> params);
	List<String> queryDpCodeByShop(Map<String,Object> param);
	Integer count_product(Map<String, Object> param);
	List<T_Base_Product> list_product(Map<String, Object> param);
	Map<String,Object> temp_loadproduct(Map<String,Object> param);
	void temp_save(Map<String, Object> params);
	void temp_save_modify(Map<String, Object> params);
	void temp_del(Integer gil_id);
	void temp_delByPiCode(T_Shop_GiftList temp);
	void temp_updateAmountById(Map<String, Object> params);
	void temp_updatePointByPdcode(Map<String, Object> params);
	void save(T_Shop_Gift gift,T_Sys_User user);
	void update(T_Shop_Gift gift,T_Sys_User user);
	Integer count_run(Map<String,Object> param);
	List<T_Shop_Gift_Run> list_run(Map<String,Object> param);
	void show_gift(Map<String, Object> params);
	void del(Map<String, Object> params);
	T_Shop_Gift load(Integer gi_id);
	void initGiftUpdate(Map<String, Object> params);
	void updateGiftListTempAddAmount(Map<String, Object> params);
	Integer getGiftTempSum(Map<String, Object> params);
	Integer report_count(Map<String,Object> param);
	List<T_Shop_Gift_Send> report_list(Map<String,Object> param);
	/***************前台**************/
	Integer sendCount(Map<String,Object> param);
	List<T_Shop_GiftList> sendList(Map<String,Object> param);
	void send(Map<String,Object> param);
}
