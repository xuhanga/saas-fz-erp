package zy.dao.shop.price;

import java.util.List;
import java.util.Map;

import zy.entity.shop.price.T_Shop_Price;
import zy.entity.shop.price.T_Shop_PriceList;

public interface PriceDAO {
	Integer count(Map<String,Object> params);
	List<T_Shop_Price> list(Map<String,Object> params);
	List<T_Shop_PriceList> price_list(Map<String,Object> params);
	List<T_Shop_PriceList> temp_list(Map<String,Object> params);
	List<T_Shop_PriceList> temp_list_forsave(Integer us_id,Integer companyid);
	void save_temp_list(Map<String,Object> params);
	String save_tempList_Enter(Map<String,Object> params);
	void shop_change_templist(Map<String,Object> params);
	void update_templist_byPdCode(Map<String,Object> params);
	void savePrice_templist(Map<String,Object> params);
	void savePoint_templist(Map<String,Object> params);
	void save(T_Shop_Price shop_Price);
	void update(T_Shop_Price shop_Price);
	void saveList(List<T_Shop_PriceList> shop_PriceLists);
	void save(T_Shop_Price shop_Price,List<T_Shop_PriceList> shop_PriceLists);
	void temp_clear(Integer us_id,Integer companyid);
	void temp_del(Integer spl_id);
	T_Shop_Price load(Integer sp_id);
	T_Shop_Price check(String number,Integer companyid);
	void updateApprove(T_Shop_Price price);
	void update_base_shopprice(T_Shop_Price price);
	void del(String number, Integer companyid);
	List<T_Shop_PriceList> detail_list_forsavetemp(String sp_number,Integer companyid);
	void temp_save(List<T_Shop_PriceList> temps);
	void deleteList(String sp_number, Integer companyid);
}
