package zy.dao.shop.program;

import java.util.List;
import java.util.Map;

import zy.entity.shop.program.T_Shop_Program;

public interface ProgramDAO {
	Integer count(Map<String,Object> param);
	List<T_Shop_Program> list(Map<String,Object> param);
	void save(T_Shop_Program program);
	void del(Integer sp_id,Integer sps_id);
	T_Shop_Program load(Map<String,Object> param);
	void update_state(Integer sps_id);
}
