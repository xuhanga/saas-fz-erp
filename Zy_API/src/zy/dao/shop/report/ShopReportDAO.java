package zy.dao.shop.report;

import java.util.List;
import java.util.Map;

import zy.entity.sell.day.T_Sell_Day;

public interface ShopReportDAO {
	List<T_Sell_Day> flow(Map<String, Object> paramMap);
}
