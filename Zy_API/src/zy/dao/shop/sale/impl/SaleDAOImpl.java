package zy.dao.shop.sale.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.shop.sale.SaleDAO;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.shop.sale.T_Shop_Sale;
import zy.entity.shop.sale.T_Shop_Sale_All;
import zy.entity.shop.sale.T_Shop_Sale_Brand;
import zy.entity.shop.sale.T_Shop_Sale_Present;
import zy.entity.shop.sale.T_Shop_Sale_Product;
import zy.entity.shop.sale.T_Shop_Sale_Shop;
import zy.entity.shop.sale.T_Shop_Sale_Type;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class SaleDAOImpl  extends BaseDaoImpl implements SaleDAO{

	@Override
	public Integer count(Map<String, Object> param) {
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		Object begindate = param.get("begindate");
		Object enddate = param.get("enddate");
		Object sss_shop_code = param.get("sss_shop_code");
		Object ss_code = param.get("ss_code");
		Object ss_mt_code = param.get("ss_mt_code");
		Object ss_state = param.get("ss_state");
        
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1) from (");
		sql.append(" select ss_code");
		sql.append(" from t_shop_sale t");
		sql.append(" join t_shop_sale_model ssm on ssm.ssm_code = t.ss_model ");
		sql.append(" join t_shop_sale_shop sss on sss.sss_ss_code = t.ss_code and sss.companyid = t.companyid ");
		sql.append(" join t_base_shop sp on sp.sp_code = sss.sss_shop_code AND sp.companyid = sss.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (sss_shop_code != null && !"".equals(sss_shop_code)) {
				sql.append(" AND sss.sss_shop_code=:sss_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND ss_sysdate >= '" + begindate + " 00:00:00 ' AND ss_sysdate <= '" + enddate + " 23:59:59 '");
		}
		if (ss_code != null && !"".equals(ss_code)) {
			sql.append(" AND t.ss_code=:ss_code ");
		}
		if (ss_mt_code != null && !"".equals(ss_mt_code)) {
			sql.append(" AND t.ss_mt_code=:ss_mt_code ");
		}
		if (ss_state != null && !"".equals(ss_state)) {
			sql.append(" AND t.ss_state=:ss_state ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY t.ss_code ");
		sql.append(") sale_count");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public List<T_Shop_Sale> list(Map<String, Object> param) {
		Object shop_type = param.get(CommonUtil.SHOP_TYPE);
		Object start = param.get("start");
		Object end = param.get("end");
		Object begindate = param.get("begindate");
		Object enddate = param.get("enddate");
		Object sss_shop_code = param.get("sss_shop_code");
		Object ss_code = param.get("ss_code");
		Object ss_mt_code = param.get("ss_mt_code");
		Object ss_state = param.get("ss_state");
		
		StringBuffer sql = new StringBuffer("");
		sql.append(" select ss_id,ss_code,ss_name,ss_begin_date,ss_end_date,ss_week,ss_mt_code,ss_point,ss_manager,");
		sql.append(" ss_sysdate,ss_state,ss_priority,ss_model,GROUP_CONCAT(sp.sp_name) AS ss_sp_name");
		sql.append(" from t_shop_sale t");
		sql.append(" join t_shop_sale_model ssm on ssm.ssm_code = t.ss_model ");
		sql.append(" join t_shop_sale_shop sss on sss.sss_ss_code = t.ss_code and sss.companyid = t.companyid ");
		sql.append(" join t_base_shop sp on sp.sp_code = sss.sss_shop_code AND sp.companyid = sss.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (sss_shop_code != null && !"".equals(sss_shop_code)) {
				sql.append(" AND sss.sss_shop_code=:sss_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND ss_sysdate >= '" + begindate + " 00:00:00 ' AND ss_sysdate <= '" + enddate + " 23:59:59 '");
		}
		if (ss_code != null && !"".equals(ss_code)) {
			sql.append(" AND t.ss_code=:ss_code ");
		}
		if (ss_mt_code != null && !"".equals(ss_mt_code)) {
			sql.append(" AND t.ss_mt_code=:ss_mt_code ");
		}
		if (ss_state != null && !"".equals(ss_state)) {
			sql.append(" AND t.ss_state=:ss_state ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" group by t.ss_code ");
		sql.append(" order by t.ss_id desc ");
		if(null != start && !"".equals(start) && null != end && !"".equals(end)){
			sql.append(" limit :start,:end");
		}
		List<T_Shop_Sale> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale.class));
		return list;
	}

	@Override
	public Integer queryPriority(Map<String, Object> param) {
		String ss_shop_code = (String)param.get("ss_shop_code");
        
		StringBuffer sql = new StringBuffer("");
		sql.append(" select count(1)");
		sql.append(" from t_shop_sale t");
		sql.append(" join t_shop_sale_shop sss on sss.sss_ss_code = t.ss_code and sss.companyid = t.companyid ");
		sql.append(" where 1 = 1");
		if(ss_shop_code != null && !ss_shop_code.equals("")){
			sql.append(" and sss.sss_shop_code IN (");
			String[] si_codes = ss_shop_code.split(",");
			for(String si_code:si_codes){
				sql.append("'"+si_code+"',");
			}
			sql.deleteCharAt(sql.length()-1);
			sql.append(")");
		}
		sql.append(" and t.ss_begin_date <= :curdate");
		sql.append(" and t.ss_end_date >= :curdate");
		sql.append(" and t.ss_priority = :ss_priority");
		sql.append(" and ss_state <> '4'");
		sql.append(" and t.companyid=:companyid");
		sql.append(" LIMIT 1");
		Integer count = namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
		return count;
	}

	@Override
	public void save(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object ifUpdatePriority = param.get("ifUpdatePriority");
		Map<String,Object> productMap = (Map<String,Object>)param.get("productMap");
		Object us_name = param.get("us_name");
		T_Shop_Sale t_shop_sale = (T_Shop_Sale)param.get("t_shop_sale");
		Integer ss_priority = t_shop_sale.getSs_priority();
		String ss_model = t_shop_sale.getSs_model();
		String curdate = DateUtil.getYearMonthDate();
		String time = DateUtil.getCurrentTime();
		
		String ss_shop_codes = (String)param.get("ss_shop_codes");//店铺编号
		String[] shop_codes = null; 
		if(ss_shop_codes !=null && !"".equals(ss_shop_codes)){
			shop_codes = ss_shop_codes.split(",");
		}
		
		StringBuffer sql = new StringBuffer("");
		//更新促销优先级
		if(ifUpdatePriority!=null && "1".equals(ifUpdatePriority)){
			List<T_Shop_Sale> saleList = new ArrayList<T_Shop_Sale>();
			T_Shop_Sale sale = null;
			for(int i=0;i<shop_codes.length;i++){
				sale = new T_Shop_Sale();
				sale.setCurdate(curdate);
				sale.setSs_shop_code(shop_codes[i]);
				sale.setSs_state(0);
				sale.setCompanyid((Integer)companyid);
				sale.setSs_priority(ss_priority);
				saleList.add(sale);
			}
			sql.append(" update t_shop_sale t join t_shop_sale_shop sss on sss.sss_ss_code = t.ss_code and sss.companyid = t.companyid ");
			sql.append(" set ss_priority = ss_priority + 1");
			sql.append(" WHERE 1=1 ");
			sql.append(" AND t.ss_begin_date <= :curdate");
			sql.append(" AND t.ss_end_date >= :curdate");
			sql.append(" AND sss.sss_shop_code=:ss_shop_code ");
//			sql.append(" AND t.ss_state = :ss_state");//2017-11-27去除状态条件
			sql.append(" AND t.companyid= :companyid");
			sql.append(" AND t.ss_priority >= :ss_priority");
			namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(saleList.toArray()));
		}
		
		//获取促销方案编号
		sql.setLength(0);
		String prefix = CommonUtil.NUMBER_PREFIX_SALE + DateUtil.getYearMonthDateYYYYMMDD();
		sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(ss_code))) AS new_number");
		sql.append(" FROM t_shop_sale");
		sql.append(" WHERE 1=1");
		sql.append(" AND INSTR(ss_code,:prefix) > 0");
		sql.append(" AND companyid = :companyid");
		String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", companyid), String.class);
		t_shop_sale.setSs_code(new_number);
		
		//增加促销方案基本信息
		t_shop_sale.setSs_manager((String)us_name);
		t_shop_sale.setSs_sysdate(time);
		t_shop_sale.setSs_state(0);
		t_shop_sale.setCompanyid((Integer)companyid);
		sql.setLength(0);
		sql.append(" INSERT INTO t_shop_sale");
		sql.append(" (ss_code,ss_name,ss_begin_date,ss_end_date,ss_week,ss_mt_code,ss_point,ss_manager,ss_sysdate,ss_state,");
		sql.append(" ss_priority,ss_model,ss_current_spcode,companyid)");
		sql.append(" VALUES ");
		sql.append(" (:ss_code,:ss_name,:ss_begin_date,:ss_end_date,:ss_week,:ss_mt_code,:ss_point,:ss_manager,:ss_sysdate,:ss_state,");
		sql.append(":ss_priority,:ss_model,:ss_current_spcode,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(t_shop_sale),holder);
		t_shop_sale.setSs_id(holder.getKey().intValue());
		
		//添加促销店铺
		List<T_Shop_Sale_Shop> saleShopList = new ArrayList<T_Shop_Sale_Shop>();
		T_Shop_Sale_Shop t_Shop_Sale_Shop = null;
		for(int i=0;i<shop_codes.length;i++){
			t_Shop_Sale_Shop = new T_Shop_Sale_Shop();
			t_Shop_Sale_Shop.setSss_ss_code(t_shop_sale.getSs_code());
			t_Shop_Sale_Shop.setSss_shop_code(shop_codes[i]);
			t_Shop_Sale_Shop.setCompanyid(t_shop_sale.getCompanyid());
			saleShopList.add(t_Shop_Sale_Shop);
		}
		sql.setLength(0);
		sql.append(" insert into t_shop_sale_shop ");
		sql.append(" (sss_ss_code,sss_shop_code,companyid)");
		sql.append(" VALUES ");
		sql.append(" (:sss_ss_code,:sss_shop_code,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(saleShopList.toArray()));
		
		int index = 0;
		if (ss_model != null && !"".equals(ss_model)) {
			List<T_Shop_Sale_All> sale_allList = null;
			T_Shop_Sale_All shop_Sale_All = null;
			List<T_Shop_Sale_Type> sale_typeList = null;
			T_Shop_Sale_Type shop_Sale_Type = null;
			List<T_Shop_Sale_Brand> sale_brandList = null;
			T_Shop_Sale_Brand shop_Sale_Brand = null;
			List<T_Shop_Sale_Product> sale_productList = null;
			T_Shop_Sale_Product shop_Sale_Product = null;
			List<T_Shop_Sale_Present> sale_presentList = null;
			T_Shop_Sale_Present shop_Sale_Present = null;
			if ("111".equals(ss_model)) {//全场直接折扣
				String ssa_discount111 = (String)param.get("ssa_discount111");
				shop_Sale_All = new T_Shop_Sale_All();
				shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
				shop_Sale_All.setSsa_discount(Double.parseDouble(ssa_discount111));
				shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_all ");
				sql.append(" (ssa_ss_code,ssa_discount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssa_ss_code,:ssa_discount,:companyid)");
				namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(shop_Sale_All));
			}
			if ("112".equals(ss_model)) {
				String[] ssa_buy_fulls112 = (String[])param.get("ssa_buy_fulls112");
				String[] ssa_discounts112 = (String[])param.get("ssa_discounts112");
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_fulls112 != null) {
					for (int i = 0,size = ssa_buy_fulls112.length; i < size; i++) {
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_full(Double.parseDouble(ssa_buy_fulls112[i]));
						shop_Sale_All.setSsa_discount(Double.parseDouble(ssa_discounts112[i]));
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_all ");
				sql.append(" (ssa_ss_code,ssa_buy_full,ssa_discount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssa_ss_code,:ssa_buy_full,:ssa_discount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
			}
			if ("113".equals(ss_model)) {
				String[] ssa_buy_number113 = (String[])param.get("ssa_buy_number113");
				String[] ssa_discount113 = (String[])param.get("ssa_discount113");
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_number113 != null) {
					for (int i = 0,size = ssa_buy_number113.length; i < size; i++) {
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_number(Integer.parseInt(ssa_buy_number113[i]));
						shop_Sale_All.setSsa_discount(Double.parseDouble(ssa_discount113[i]));
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_all ");
				sql.append(" (ssa_ss_code,ssa_buy_number,ssa_discount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssa_ss_code,:ssa_buy_number,:ssa_discount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
			}
			if ("121".equals(ss_model)) {
				String[] sst_tp_code121 = (String[])param.get("sst_tp_code121");
				String[] sst_discount121 = (String[])param.get("sst_discount121");
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code121 != null) {
					for (int i = 0,size = sst_tp_code121.length; i < size; i++) {
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code121[i]);
						shop_Sale_Type.setSst_discount(Double.parseDouble(sst_discount121[i]));
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_type ");
				sql.append(" (sst_ss_code,sst_tp_code,sst_discount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:sst_ss_code,:sst_tp_code,:sst_discount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
			}
			if ("122".equals(ss_model)) {
				String[] sst_tp_code122 = (String[])param.get("sst_tp_code122");
				String[] sst_buy_full122 = (String[])param.get("sst_buy_full122");
				String[] sst_discount122 = (String[])param.get("sst_discount122");
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code122 != null) {
					for (int i = 0,size = sst_tp_code122.length; i < size; i++) {
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code122[i]);
						shop_Sale_Type.setSst_buy_full(Double.parseDouble(sst_buy_full122[i]));
						shop_Sale_Type.setSst_discount(Double.parseDouble(sst_discount122[i]));
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_type ");
				sql.append(" (sst_ss_code,sst_tp_code,sst_discount,sst_buy_full,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:sst_ss_code,:sst_tp_code,:sst_discount,:sst_buy_full,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
			}
			if ("123".equals(ss_model)) {
				String[] sst_tp_code123 = (String[])param.get("sst_tp_code123");
				String[] sst_buy_number123 = (String[])param.get("sst_buy_number123");
				String[] sst_discount123 = (String[])param.get("sst_discount123");
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code123 != null) {
					for (int i = 0,size = sst_tp_code123.length; i < size; i++) {
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code123[i]);
						shop_Sale_Type.setSst_buy_number(Integer.parseInt(sst_buy_number123[i]));
						shop_Sale_Type.setSst_discount(Double.parseDouble(sst_discount123[i]));
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_type ");
				sql.append(" (sst_ss_code,sst_tp_code,sst_discount,sst_buy_number,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:sst_ss_code,:sst_tp_code,:sst_discount,:sst_buy_number,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
			}
			if ("131".equals(ss_model)) {
				String[] ssb_bd_code131 = (String[])param.get("ssb_bd_code131");
				String[] ssb_discount131 = (String[])param.get("ssb_discount131");
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code131 != null) {
					for (int i = 0,size = ssb_bd_code131.length; i < size; i++) {
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code131[i]);
						shop_Sale_Brand.setSsb_discount(Double.parseDouble(ssb_discount131[i]));
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_brand ");
				sql.append(" (ssb_ss_code,ssb_bd_code,ssb_discount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_discount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
			}
			if ("132".equals(ss_model)) {
				String[] ssb_bd_code132 = (String[])param.get("ssb_bd_code132");
				String[] ssb_buy_full132 = (String[])param.get("ssb_buy_full132");
				String[] ssb_discount132 = (String[])param.get("ssb_discount132");
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code132 != null) {
					for (int i = 0,size = ssb_bd_code132.length; i < size; i++) {
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code132[i]);
						shop_Sale_Brand.setSsb_buy_full(Double.parseDouble(ssb_buy_full132[i]));
						shop_Sale_Brand.setSsb_discount(Double.parseDouble(ssb_discount132[i]));
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_brand ");
				sql.append(" (ssb_ss_code,ssb_bd_code,ssb_discount,ssb_buy_full,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_discount,:ssb_buy_full,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
			}
			if ("133".equals(ss_model)) {
				String[] ssb_bd_code133 = (String[])param.get("ssb_bd_code133");
				String[] ssb_buy_number133 = (String[])param.get("ssb_buy_number133");
				String[] ssb_discount133 = (String[])param.get("ssb_discount133");
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code133 != null) {
					for (int i = 0,size = ssb_bd_code133.length; i < size; i++) {
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code133[i]);
						shop_Sale_Brand.setSsb_buy_number(Integer.parseInt(ssb_buy_number133[i]));
						shop_Sale_Brand.setSsb_discount(Double.parseDouble(ssb_discount133[i]));
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_brand ");
				sql.append(" (ssb_ss_code,ssb_bd_code,ssb_discount,ssb_buy_number,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_discount,:ssb_buy_number,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
			}
			if ("141".equals(ss_model)) {
				String[] ssp_pd_code141 = (String[])param.get("ssp_pd_code141");
				String[] ssp_discount141 = (String[])param.get("ssp_discount141");
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code141 != null) {
					for (int i = 0,size = ssp_pd_code141.length; i < size; i++) {
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code141[i]);
						shop_Sale_Product.setSsp_discount(Double.parseDouble(ssp_discount141[i]));
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_discount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_discount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("142".equals(ss_model)) {
				String[] ssp_pd_code142 = (String[])param.get("ssp_pd_code142");
				String[] ssp_buy_full142 = (String[])param.get("ssp_buy_full142");
				String[] ssp_discount142 = (String[])param.get("ssp_discount142");
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code142 != null) {
					for (int i = 0,size = ssp_pd_code142.length; i < size; i++) {
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code142[i]);
						shop_Sale_Product.setSsp_buy_full(Double.parseDouble(ssp_buy_full142[i]));
						shop_Sale_Product.setSsp_discount(Double.parseDouble(ssp_discount142[i]));
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_discount,ssp_buy_full,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_discount,:ssp_buy_full,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("143".equals(ss_model)) {
				String[] ssp_pd_code143 = (String[])param.get("ssp_pd_code143");
				String[] ssp_buy_number143 = (String[])param.get("ssp_buy_number143");
				String[] ssp_discount143 = (String[])param.get("ssp_discount143");
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code143 != null) {
					for (int i = 0,size = ssp_pd_code143.length; i < size; i++) {
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code143[i]);
						shop_Sale_Product.setSsp_buy_number(Integer.parseInt(ssp_buy_number143[i]));
						shop_Sale_Product.setSsp_discount(Double.parseDouble(ssp_discount143[i]));
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_discount,ssp_buy_number,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_discount,:ssp_buy_number,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("241".equals(ss_model)) {
				String[] ssp_pd_code241 = (String[])param.get("ssp_pd_code241");
				String[] ssp_special_offer241 = (String[])param.get("ssp_special_offer241");
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code241 != null) {
					for (int i = 0,size = ssp_pd_code241.length; i < size; i++) {
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code241[i]);
						shop_Sale_Product.setSsp_special_offer(Double.parseDouble(ssp_special_offer241[i]));
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_special_offer,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_special_offer,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("242".equals(ss_model)) {
				String[] ssp_pd_code242 = (String[])param.get("ssp_pd_code242");
				String[] ssp_buy_number242 = (String[])param.get("ssp_buy_number242");
				String[] ssp_special_offer242 = (String[])param.get("ssp_special_offer242");
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code242 != null) {
					for (int i = 0,size = ssp_pd_code242.length; i < size; i++) {
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code242[i]);
						shop_Sale_Product.setSsp_buy_number(Integer.parseInt(ssp_buy_number242[i]));
						shop_Sale_Product.setSsp_special_offer(Double.parseDouble(ssp_special_offer242[i]));
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_special_offer,ssp_buy_number,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_special_offer,:ssp_buy_number,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("311".equals(ss_model)) {
				String[] ssa_buy_number311 = (String[])param.get("ssa_buy_number311");
				String[] ssa_donation_number311 = (String[])param.get("ssa_donation_number311");
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_number311 != null) {
					for (int i = 0,size = ssa_buy_number311.length; i < size; i++) {
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_number(Integer.parseInt(ssa_buy_number311[i]));
						shop_Sale_All.setSsa_donation_number(Integer.parseInt(ssa_donation_number311[i]));
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_all ");
				sql.append(" (ssa_ss_code,ssa_buy_number,ssa_donation_number,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssa_ss_code,:ssa_buy_number,:ssa_donation_number,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
			}
			if ("312".equals(ss_model)) {
				String[] ssa_buy_full312 = (String[])param.get("ssa_buy_full312");
				String[] ssa_reduce_amount312 = (String[])param.get("ssa_reduce_amount312");
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_full312 != null) {
					for (int i = 0,size = ssa_buy_full312.length; i < size; i++) {
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_full(Double.parseDouble(ssa_buy_full312[i]));
						shop_Sale_All.setSsa_reduce_amount(Double.parseDouble(ssa_reduce_amount312[i]));
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_all ");
				sql.append(" (ssa_ss_code,ssa_buy_full,ssa_reduce_amount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssa_ss_code,:ssa_buy_full,:ssa_reduce_amount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
			}
			if ("313".equals(ss_model)) {
				String[] ssa_buy_full313 = (String[])param.get("ssa_buy_full313");
				String[] ssa_increased_amount313 = (String[])param.get("ssa_increased_amount313");
				String[] ssp_subcode313 = null,ssp_count313 = null,ssp_pd_code313 = null,ssp_cr_code313 = null,ssp_sz_code313 = null,ssp_br_code313 = null;
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_full313 != null) {
					index = 0;
					for (int i = 0,size = ssa_buy_full313.length; i < size; i++) {
						index ++;
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_full(Double.parseDouble(ssa_buy_full313[i]));
						shop_Sale_All.setSsa_increased_amount(Double.parseDouble(ssa_increased_amount313[i]));
						shop_Sale_All.setSsa_index(index);
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
						
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode313 = (String[])productMap.get("ssp_subcode313_"+index);
							ssp_count313 = (String[])productMap.get("ssp_count313_"+index);
							ssp_pd_code313 = (String[])productMap.get("ssp_pd_code313_"+index);
							ssp_cr_code313 = (String[])productMap.get("ssp_cr_code313_"+index);
							ssp_sz_code313 = (String[])productMap.get("ssp_sz_code313_"+index);
							ssp_br_code313 = (String[])productMap.get("ssp_br_code313_"+index);
							if(ssp_subcode313 != null && ssp_subcode313.length > 0){
								for (int j = 0; j < ssp_subcode313.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode313[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count313[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code313[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code313[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code313[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code313[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_all ");
					sql.append(" (ssa_ss_code,ssa_buy_full,ssa_increased_amount,ssa_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:ssa_ss_code,:ssa_buy_full,:ssa_increased_amount,:ssa_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
				}
			}
			if ("314".equals(ss_model)) {
				String[] ssa_buy_number314 = (String[])param.get("ssa_buy_number314");
				String[] ssp_subcode314 = null,ssp_count314 = null,ssp_pd_code314 = null,ssp_cr_code314 = null,ssp_sz_code314 = null,ssp_br_code314 = null;
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_number314 != null) {
					index = 0;
					for (int i = 0,size = ssa_buy_number314.length; i < size; i++) {
						index ++;
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_number(Integer.parseInt(ssa_buy_number314[i]));
						shop_Sale_All.setSsa_index(index);
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
						
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode314 = (String[])productMap.get("ssp_subcode314_"+index);
							ssp_count314 = (String[])productMap.get("ssp_count314_"+index);
							ssp_pd_code314 = (String[])productMap.get("ssp_pd_code314_"+index);
							ssp_cr_code314 = (String[])productMap.get("ssp_cr_code314_"+index);
							ssp_sz_code314 = (String[])productMap.get("ssp_sz_code314_"+index);
							ssp_br_code314 = (String[])productMap.get("ssp_br_code314_"+index);
							if(ssp_subcode314 != null && ssp_subcode314.length > 0){
								for (int j = 0; j < ssp_subcode314.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode314[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count314[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code314[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code314[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code314[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code314[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_all ");
					sql.append(" (ssa_ss_code,ssa_buy_number,ssa_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:ssa_ss_code,:ssa_buy_number,:ssa_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
				}
			}
			if ("315".equals(ss_model)) {
				String[] ssa_buy_full315 = (String[])param.get("ssa_buy_full315");
				String[] ssp_subcode315 = null,ssp_count315 = null,ssp_pd_code315 = null,ssp_cr_code315 = null,ssp_sz_code315 = null,ssp_br_code315 = null;
				sale_allList = new ArrayList<T_Shop_Sale_All>();
				if (ssa_buy_full315 != null) {
					index = 0;
					for (int i = 0,size = ssa_buy_full315.length; i < size; i++) {
						index ++;
						shop_Sale_All = new T_Shop_Sale_All();
						shop_Sale_All.setSsa_ss_code(t_shop_sale.getSs_code());
						shop_Sale_All.setSsa_buy_full(Double.parseDouble(ssa_buy_full315[i]));
						shop_Sale_All.setSsa_index(index);
						shop_Sale_All.setCompanyid(t_shop_sale.getCompanyid());
						sale_allList.add(shop_Sale_All);
						
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode315 = (String[])productMap.get("ssp_subcode315_"+index);
							ssp_count315 = (String[])productMap.get("ssp_count315_"+index);
							ssp_pd_code315 = (String[])productMap.get("ssp_pd_code315_"+index);
							ssp_cr_code315 = (String[])productMap.get("ssp_cr_code315_"+index);
							ssp_sz_code315 = (String[])productMap.get("ssp_sz_code315_"+index);
							ssp_br_code315 = (String[])productMap.get("ssp_br_code315_"+index);
							if(ssp_subcode315 != null && ssp_subcode315.length > 0){
								for (int j = 0; j < ssp_subcode315.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode315[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count315[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code315[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code315[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code315[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code315[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_all ");
					sql.append(" (ssa_ss_code,ssa_buy_full,ssa_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:ssa_ss_code,:ssa_buy_full,:ssa_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_allList.toArray()));
				}
			}
			if ("322".equals(ss_model)) {
				String[] sst_tp_code322 = (String[])param.get("sst_tp_code322");
				String[] sst_buy_full322 = (String[])param.get("sst_buy_full322");
				String[] sst_reduce_amount322 = (String[])param.get("sst_reduce_amount322");
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code322 != null) {
					for (int i = 0,size = sst_tp_code322.length; i < size; i++) {
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code322[i]);
						shop_Sale_Type.setSst_buy_full(Double.parseDouble(sst_buy_full322[i]));
						shop_Sale_Type.setSst_reduce_amount(Double.parseDouble(sst_reduce_amount322[i]));
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_type ");
				sql.append(" (sst_ss_code,sst_tp_code,sst_buy_full,sst_reduce_amount,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:sst_ss_code,:sst_tp_code,:sst_buy_full,:sst_reduce_amount,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
			}
			if ("323".equals(ss_model)) {
				String[] sst_tp_code323 = (String[])param.get("sst_tp_code323");
				String[] sst_buy_full323 = (String[])param.get("sst_buy_full323");
				String[] sst_increased_amount323 = (String[])param.get("sst_increased_amount323");
				String[] ssp_subcode323 = null,ssp_count323 = null,ssp_pd_code323 = null,ssp_cr_code323 = null,ssp_sz_code323 = null,ssp_br_code323 = null;
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code323 != null) {
					index = 0;
					for (int i = 0,size = sst_tp_code323.length; i < size; i++) {
						index ++;
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code323[i]);
						shop_Sale_Type.setSst_buy_full(Double.parseDouble(sst_buy_full323[i]));
						shop_Sale_Type.setSst_increased_amount(Double.parseDouble(sst_increased_amount323[i]));
						shop_Sale_Type.setSst_index(index);
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode323 = (String[])productMap.get("ssp_subcode323_"+index);
							ssp_count323 = (String[])productMap.get("ssp_count323_"+index);
							ssp_pd_code323 = (String[])productMap.get("ssp_pd_code323_"+index);
							ssp_cr_code323 = (String[])productMap.get("ssp_cr_code323_"+index);
							ssp_sz_code323 = (String[])productMap.get("ssp_sz_code323_"+index);
							ssp_br_code323 = (String[])productMap.get("ssp_br_code323_"+index);
							if(ssp_subcode323 != null && ssp_subcode323.length > 0){
								for (int j = 0; j < ssp_subcode323.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode323[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count323[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code323[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code323[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code323[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code323[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_type ");
					sql.append(" (sst_ss_code,sst_tp_code,sst_buy_full,sst_increased_amount,sst_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:sst_ss_code,:sst_tp_code,:sst_buy_full,:sst_increased_amount,:sst_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
				}
			}
			if ("324".equals(ss_model)) {
				String[] sst_tp_code324 = (String[])param.get("sst_tp_code324");
				String[] sst_buy_number324 = (String[])param.get("sst_buy_number324");
				String[] ssp_subcode324 = null,ssp_count324 = null,ssp_pd_code324 = null,ssp_cr_code324 = null,ssp_sz_code324 = null,ssp_br_code324 = null;
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code324 != null) {
					index = 0;
					for (int i = 0,size = sst_tp_code324.length; i < size; i++) {
						index ++;
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code324[i]);
						shop_Sale_Type.setSst_buy_number(Integer.parseInt(sst_buy_number324[i]));
						shop_Sale_Type.setSst_index(index);
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode324 = (String[])productMap.get("ssp_subcode324_"+index);
							ssp_count324 = (String[])productMap.get("ssp_count324_"+index);
							ssp_pd_code324 = (String[])productMap.get("ssp_pd_code324_"+index);
							ssp_cr_code324 = (String[])productMap.get("ssp_cr_code324_"+index);
							ssp_sz_code324 = (String[])productMap.get("ssp_sz_code324_"+index);
							ssp_br_code324 = (String[])productMap.get("ssp_br_code324_"+index);
							if(ssp_subcode324 != null && ssp_subcode324.length > 0){
								for (int j = 0; j < ssp_subcode324.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode324[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count324[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code324[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code324[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code324[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code324[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_type ");
					sql.append(" (sst_ss_code,sst_tp_code,sst_buy_number,sst_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:sst_ss_code,:sst_tp_code,:sst_buy_number,:sst_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
				}
			}
			if ("325".equals(ss_model)) {
				String[] sst_tp_code325 = (String[])param.get("sst_tp_code325");
				String[] sst_buy_full325 = (String[])param.get("sst_buy_full325");
				String[] ssp_subcode325 = null,ssp_count325 = null,ssp_pd_code325 = null,ssp_cr_code325 = null,ssp_sz_code325 = null,ssp_br_code325 = null;
				sale_typeList = new ArrayList<T_Shop_Sale_Type>();
				if (sst_tp_code325 != null) {
					index = 0;
					for (int i = 0,size = sst_tp_code325.length; i < size; i++) {
						index ++;
						shop_Sale_Type = new T_Shop_Sale_Type();
						shop_Sale_Type.setSst_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Type.setSst_tp_code(sst_tp_code325[i]);
						shop_Sale_Type.setSst_buy_full(Double.parseDouble(sst_buy_full325[i]));
						shop_Sale_Type.setSst_index(index);
						shop_Sale_Type.setCompanyid(t_shop_sale.getCompanyid());
						sale_typeList.add(shop_Sale_Type);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode325 = (String[])productMap.get("ssp_subcode325_"+index);
							ssp_count325 = (String[])productMap.get("ssp_count325_"+index);
							ssp_pd_code325 = (String[])productMap.get("ssp_pd_code325_"+index);
							ssp_cr_code325 = (String[])productMap.get("ssp_cr_code325_"+index);
							ssp_sz_code325 = (String[])productMap.get("ssp_sz_code325_"+index);
							ssp_br_code325 = (String[])productMap.get("ssp_br_code325_"+index);
							if(ssp_subcode325 != null && ssp_subcode325.length > 0){
								for (int j = 0; j < ssp_subcode325.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode325[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count325[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code325[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code325[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code325[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code325[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_type ");
					sql.append(" (sst_ss_code,sst_tp_code,sst_buy_full,sst_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:sst_ss_code,:sst_tp_code,:sst_buy_full,:sst_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_typeList.toArray()));
				}
			}
			if ("332".equals(ss_model)) {
				String[] ssb_bd_code332 = (String[])param.get("ssb_bd_code332");
				String[] ssb_buy_full332 = (String[])param.get("ssb_buy_full332");
				String[] ssb_reduce_amount332 = (String[])param.get("ssb_reduce_amount332");
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code332 != null) {
					for (int i = 0,size = ssb_bd_code332.length; i < size; i++) {
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code332[i]);
						shop_Sale_Brand.setSsb_buy_full(Double.parseDouble(ssb_buy_full332[i]));
						shop_Sale_Brand.setSsb_reduce_amount(Double.parseDouble(ssb_reduce_amount332[i]));
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_brand ");
				sql.append(" (ssb_ss_code,ssb_bd_code,ssb_reduce_amount,ssb_buy_full,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_reduce_amount,:ssb_buy_full,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
			}
			if ("333".equals(ss_model)) {
				String[] ssb_bd_code333 = (String[])param.get("ssb_bd_code333");
				String[] ssb_buy_full333 = (String[])param.get("ssb_buy_full333");
				String[] ssb_increased_amount333 = (String[])param.get("ssb_increased_amount333");
				String[] ssp_subcode333 = null,ssp_count333 = null,ssp_pd_code333 = null,ssp_cr_code333 = null,ssp_sz_code333 = null,ssp_br_code333 = null;
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code333 != null) {
					index = 0;
					for (int i = 0,size = ssb_bd_code333.length; i < size; i++) {
						index ++;
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code333[i]);
						shop_Sale_Brand.setSsb_buy_full(Double.parseDouble(ssb_buy_full333[i]));
						shop_Sale_Brand.setSsb_increased_amount(Double.parseDouble(ssb_increased_amount333[i]));
						shop_Sale_Brand.setSsb_index(index);
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode333 = (String[])productMap.get("ssp_subcode333_"+index);
							ssp_count333 = (String[])productMap.get("ssp_count333_"+index);
							ssp_pd_code333 = (String[])productMap.get("ssp_pd_code333_"+index);
							ssp_cr_code333 = (String[])productMap.get("ssp_cr_code333_"+index);
							ssp_sz_code333 = (String[])productMap.get("ssp_sz_code333_"+index);
							ssp_br_code333 = (String[])productMap.get("ssp_br_code333_"+index);
							if(ssp_subcode333 != null && ssp_subcode333.length > 0){
								for (int j = 0; j < ssp_subcode333.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode333[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count333[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code333[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code333[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code333[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code333[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_brand ");
					sql.append(" (ssb_ss_code,ssb_bd_code,ssb_increased_amount,ssb_buy_full,ssb_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_increased_amount,:ssb_buy_full,:ssb_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
				}
			}
			if ("334".equals(ss_model)) {
				String[] ssb_bd_code334 = (String[])param.get("ssb_bd_code334");
				String[] ssb_buy_number334 = (String[])param.get("ssb_buy_number334");
				String[] ssp_subcode334 = null,ssp_count334 = null,ssp_pd_code334 = null,ssp_cr_code334 = null,ssp_sz_code334 = null,ssp_br_code334 = null;
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code334 != null) {
					index = 0;
					for (int i = 0,size = ssb_bd_code334.length; i < size; i++) {
						index ++;
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code334[i]);
						shop_Sale_Brand.setSsb_buy_number(Integer.parseInt(ssb_buy_number334[i]));
						shop_Sale_Brand.setSsb_index(index);
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode334 = (String[])productMap.get("ssp_subcode334_"+index);
							ssp_count334 = (String[])productMap.get("ssp_count334_"+index);
							ssp_pd_code334 = (String[])productMap.get("ssp_pd_code334_"+index);
							ssp_cr_code334 = (String[])productMap.get("ssp_cr_code334_"+index);
							ssp_sz_code334 = (String[])productMap.get("ssp_sz_code334_"+index);
							ssp_br_code334 = (String[])productMap.get("ssp_br_code334_"+index);
							if(ssp_subcode334 != null && ssp_subcode334.length > 0){
								for (int j = 0; j < ssp_subcode334.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode334[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count334[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code334[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code334[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code334[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code334[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_brand ");
					sql.append(" (ssb_ss_code,ssb_bd_code,ssb_buy_number,ssb_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_buy_number,:ssb_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
				}
			}
			if ("335".equals(ss_model)) {
				String[] ssb_bd_code335 = (String[])param.get("ssb_bd_code335");
				String[] ssb_buy_full335 = (String[])param.get("ssb_buy_full335");
				String[] ssp_subcode335 = null,ssp_count335 = null,ssp_pd_code335 = null,ssp_cr_code335 = null,ssp_sz_code335 = null,ssp_br_code335 = null;
				sale_brandList = new ArrayList<T_Shop_Sale_Brand>();
				if (ssb_bd_code335 != null) {
					index = 0;
					for (int i = 0,size = ssb_bd_code335.length; i < size; i++) {
						index ++;
						shop_Sale_Brand = new T_Shop_Sale_Brand();
						shop_Sale_Brand.setSsb_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Brand.setSsb_bd_code(ssb_bd_code335[i]);
						shop_Sale_Brand.setSsb_buy_full(Double.parseDouble(ssb_buy_full335[i]));
						shop_Sale_Brand.setSsb_index(index);
						shop_Sale_Brand.setCompanyid(t_shop_sale.getCompanyid());
						sale_brandList.add(shop_Sale_Brand);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode335 = (String[])productMap.get("ssp_subcode335_"+index);
							ssp_count335 = (String[])productMap.get("ssp_count335_"+index);
							ssp_pd_code335 = (String[])productMap.get("ssp_pd_code335_"+index);
							ssp_cr_code335 = (String[])productMap.get("ssp_cr_code335_"+index);
							ssp_sz_code335 = (String[])productMap.get("ssp_sz_code335_"+index);
							ssp_br_code335 = (String[])productMap.get("ssp_br_code335_"+index);
							if(ssp_subcode335 != null && ssp_subcode335.length > 0){
								for (int j = 0; j < ssp_subcode335.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode335[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count335[j]));
									shop_Sale_Present.setSsp_pd_code(ssp_pd_code335[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code335[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code335[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code335[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
					sql.setLength(0);
					sql.append(" insert into t_shop_sale_brand ");
					sql.append(" (ssb_ss_code,ssb_bd_code,ssb_buy_full,ssb_index,companyid)");
					sql.append(" VALUES ");
					sql.append(" (:ssb_ss_code,:ssb_bd_code,:ssb_buy_full,:ssb_index,:companyid)");
					namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_brandList.toArray()));
				}
			}
			if ("342".equals(ss_model)) {
				String[] ssp_pd_code342 = (String[])param.get("ssp_pd_code342");
				String[] ssp_buy_full342 = (String[])param.get("ssp_buy_full342");
				String[] ssp_reduce_amount342 = (String[])param.get("ssp_reduce_amount342");
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code342 != null) {
					for (int i = 0,size = ssp_pd_code342.length; i < size; i++) {
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code342[i]);
						shop_Sale_Product.setSsp_buy_full(Double.parseDouble(ssp_buy_full342[i]));
						shop_Sale_Product.setSsp_reduce_amount(Double.parseDouble(ssp_reduce_amount342[i]));
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_reduce_amount,ssp_buy_full,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_reduce_amount,:ssp_buy_full,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("343".equals(ss_model)) {
				String[] ssp_pd_code343 = (String[])param.get("ssp_pd_code343");
				String[] ssp_buy_full343 = (String[])param.get("ssp_buy_full343");
				String[] ssp_increased_amount343 = (String[])param.get("ssp_increased_amount343");
				String[] ssp_subcode343 = null,ssp_count343 = null,sspr_pd_code343 = null,ssp_cr_code343 = null,ssp_sz_code343 = null,ssp_br_code343 = null;
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code343 != null) {
					index = 0;
					for (int i = 0,size = ssp_pd_code343.length; i < size; i++) {
						index ++;
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code343[i]);
						shop_Sale_Product.setSsp_buy_full(Double.parseDouble(ssp_buy_full343[i]));
						shop_Sale_Product.setSsp_increased_amount(Double.parseDouble(ssp_increased_amount343[i]));
						shop_Sale_Product.setSsp_index(index);
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode343 = (String[])productMap.get("ssp_subcode343_"+index);
							ssp_count343 = (String[])productMap.get("ssp_count343_"+index);
							sspr_pd_code343 = (String[])productMap.get("ssp_pd_code343_"+index);
							ssp_cr_code343 = (String[])productMap.get("ssp_cr_code343_"+index);
							ssp_sz_code343 = (String[])productMap.get("ssp_sz_code343_"+index);
							ssp_br_code343 = (String[])productMap.get("ssp_br_code343_"+index);
							if(ssp_subcode343 != null && ssp_subcode343.length > 0){
								for (int j = 0; j < ssp_subcode343.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode343[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count343[j]));
									shop_Sale_Present.setSsp_pd_code(sspr_pd_code343[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code343[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code343[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code343[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_increased_amount,ssp_buy_full,ssp_index,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_increased_amount,:ssp_buy_full,:ssp_index,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("344".equals(ss_model)) {
				String[] ssp_pd_code344 = (String[])param.get("ssp_pd_code344");
				String[] ssp_buy_number344 = (String[])param.get("ssp_buy_number344");
				String[] ssp_subcode344 = null,ssp_count344 = null,sspr_pd_code344 = null,ssp_cr_code344 = null,ssp_sz_code344 = null,ssp_br_code344 = null;
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code344 != null) {
					index = 0;
					for (int i = 0,size = ssp_pd_code344.length; i < size; i++) {
						index ++;
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code344[i]);
						shop_Sale_Product.setSsp_buy_number(Integer.parseInt(ssp_buy_number344[i]));
						shop_Sale_Product.setSsp_index(index);
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode344 = (String[])productMap.get("ssp_subcode344_"+index);
							ssp_count344 = (String[])productMap.get("ssp_count344_"+index);
							sspr_pd_code344 = (String[])productMap.get("ssp_pd_code344_"+index);
							ssp_cr_code344 = (String[])productMap.get("ssp_cr_code344_"+index);
							ssp_sz_code344 = (String[])productMap.get("ssp_sz_code344_"+index);
							ssp_br_code344 = (String[])productMap.get("ssp_br_code344_"+index);
							if(ssp_subcode344 != null && ssp_subcode344.length > 0){
								for (int j = 0; j < ssp_subcode344.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode344[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count344[j]));
									shop_Sale_Present.setSsp_pd_code(sspr_pd_code344[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code344[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code344[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code344[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_buy_number,ssp_index,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_buy_number,:ssp_index,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
			if ("345".equals(ss_model)) {
				String[] ssp_pd_code345 = (String[])param.get("ssp_pd_code345");
				String[] ssp_buy_full345 = (String[])param.get("ssp_buy_full345");
				String[] ssp_subcode345 = null,ssp_count345 = null,sspr_pd_code345 = null,ssp_cr_code345 = null,ssp_sz_code345 = null,ssp_br_code345 = null;
				sale_productList = new ArrayList<T_Shop_Sale_Product>();
				if (ssp_pd_code345 != null) {
					index = 0;
					for (int i = 0,size = ssp_pd_code345.length; i < size; i++) {
						index ++;
						shop_Sale_Product = new T_Shop_Sale_Product();
						shop_Sale_Product.setSsp_ss_code(t_shop_sale.getSs_code());
						shop_Sale_Product.setSsp_pd_code(ssp_pd_code345[i]);
						shop_Sale_Product.setSsp_buy_full(Double.parseDouble(ssp_buy_full345[i]));
						shop_Sale_Product.setSsp_index(index);
						shop_Sale_Product.setCompanyid(t_shop_sale.getCompanyid());
						sale_productList.add(shop_Sale_Product);
						
						sale_presentList = new ArrayList<T_Shop_Sale_Present>();
						if(productMap != null){
							ssp_subcode345 = (String[])productMap.get("ssp_subcode345_"+index);
							ssp_count345 = (String[])productMap.get("ssp_count345_"+index);
							sspr_pd_code345 = (String[])productMap.get("ssp_pd_code345_"+index);
							ssp_cr_code345 = (String[])productMap.get("ssp_cr_code345_"+index);
							ssp_sz_code345 = (String[])productMap.get("ssp_sz_code345_"+index);
							ssp_br_code345 = (String[])productMap.get("ssp_br_code345_"+index);
							if(ssp_subcode345 != null && ssp_subcode345.length > 0){
								for (int j = 0; j < ssp_subcode345.length; j++) {
									shop_Sale_Present = new T_Shop_Sale_Present();
									shop_Sale_Present.setSsp_ss_code(t_shop_sale.getSs_code());
									shop_Sale_Present.setSsp_subcode(ssp_subcode345[j]);
									shop_Sale_Present.setSsp_count(Integer.parseInt(ssp_count345[j]));
									shop_Sale_Present.setSsp_pd_code(sspr_pd_code345[j]);
									shop_Sale_Present.setSsp_cr_code(ssp_cr_code345[j]);
									shop_Sale_Present.setSsp_sz_code(ssp_sz_code345[j]);
									shop_Sale_Present.setSsp_br_code(ssp_br_code345[j]);
									shop_Sale_Present.setSsp_index(index);
									shop_Sale_Present.setCompanyid(t_shop_sale.getCompanyid());
									sale_presentList.add(shop_Sale_Present);
								}
								sql.setLength(0);
								sql.append(" insert into t_shop_sale_present ");
								sql.append(" (ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_cr_code,ssp_sz_code,ssp_br_code,ssp_index,companyid)");
								sql.append(" VALUES ");
								sql.append(" (:ssp_ss_code,:ssp_subcode,:ssp_count,:ssp_pd_code,:ssp_cr_code,:ssp_sz_code,:ssp_br_code,:ssp_index,:companyid)");
								namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_presentList.toArray()));
							}
						}
					}
				}
				sql.setLength(0);
				sql.append(" insert into t_shop_sale_product ");
				sql.append(" (ssp_ss_code,ssp_pd_code,ssp_buy_full,ssp_index,companyid)");
				sql.append(" VALUES ");
				sql.append(" (:ssp_ss_code,:ssp_pd_code,:ssp_buy_full,:ssp_index,:companyid)");
				namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(sale_productList.toArray()));
			}
		}
	}

	@Override
	public Map<String, Object> load(Map<String, Object> param) {
		Map<String,Object> saleMap = null;
		T_Shop_Sale t_Shop_Sale = null;
		List<T_Shop_Sale_All> shop_sale_allList = null;
		List<T_Shop_Sale_Type> shop_sale_typeList = null;
		List<T_Shop_Sale_Brand> shop_sale_brandList = null;
		List<T_Shop_Sale_Product> shop_sale_productList = null;
		List<T_Shop_Sale_Present> shop_sale_presentList = null;
		
		StringBuffer sql = new StringBuffer("");
		sql.append(" select ss_id,ss_code,ss_name,ss_begin_date,ss_end_date,ss_week,ss_mt_code,ss_point,ss_manager,");
		sql.append(" ss_sysdate,ss_state,ss_priority,ss_model,GROUP_CONCAT(sp.sp_name) AS ss_sp_name,ssm_model,ssm_scope,ssm_rule,");
		sql.append(" (select mt_name from t_vip_membertype mt where mt.mt_code=t.ss_mt_code and mt.companyid=t.companyid LIMIT 1) as ss_mt_name");
		sql.append(" from t_shop_sale t");
		sql.append(" join t_shop_sale_model ssm on ssm.ssm_code = t.ss_model ");
		sql.append(" join t_shop_sale_shop sss on sss.sss_ss_code = t.ss_code and sss.companyid = t.companyid ");
		sql.append(" join t_base_shop sp on sp.sp_code = sss.sss_shop_code AND sp.companyid = sss.companyid ");
		sql.append(" where 1 = 1");
		sql.append(" and t.ss_code=:ss_code");
		sql.append(" and t.companyid=:companyid");
		sql.append(" group by t.ss_code ");
		List<T_Shop_Sale> shop_saleList = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale.class));
		
		if(shop_saleList != null && shop_saleList.size()>0){
			t_Shop_Sale = shop_saleList.get(0);
			int ssm_scope = t_Shop_Sale.getSsm_scope();
			// 促销范围为全场
			if (ssm_scope == 1) {
				sql.setLength(0);
				sql.append(" select ssa_id,ssa_ss_code,ssa_discount,ssa_buy_full,ssa_buy_number,ssa_donation_number,");
				sql.append(" ssa_donation_amount,ssa_reduce_amount,ssa_increased_amount,ssa_index");
				sql.append(" from t_shop_sale_all t");
				sql.append(" where 1=1 ");
				sql.append(" and t.ssa_ss_code=:ss_code");
				sql.append(" and t.companyid=:companyid");
				shop_sale_allList = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale_All.class));
			}
			// 促销范围为类别
			if (ssm_scope == 2) {
				sql.setLength(0);
				sql.append(" select sst_id,sst_ss_code,sst_tp_code,sst_discount,sst_buy_full,sst_buy_number,");
				sql.append(" sst_donation_amount,sst_reduce_amount,sst_increased_amount,sst_index,tp_name");
				sql.append(" from t_shop_sale_type t");
				sql.append(" join t_base_type tp on tp.tp_code=t.sst_tp_code and tp.companyid=t.companyid");
				sql.append(" where 1=1 ");
				sql.append(" and t.sst_ss_code=:ss_code");
				sql.append(" and t.companyid=:companyid");
				shop_sale_typeList = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale_Type.class));
			}
			// 促销范围为品牌
			if (ssm_scope == 3) {
				sql.setLength(0);
				sql.append(" select ssb_id,ssb_ss_code,ssb_bd_code,ssb_discount,ssb_buy_full,ssb_buy_number,");
				sql.append(" ssb_donation_amount,ssb_reduce_amount,ssb_increased_amount,ssb_index,bd_name");
				sql.append(" from t_shop_sale_brand t");
				sql.append(" join t_base_brand bd on bd.bd_code=t.ssb_bd_code and bd.companyid=t.companyid");
				sql.append(" where 1=1 ");
				sql.append(" and t.ssb_ss_code=:ss_code");
				sql.append(" and t.companyid=:companyid");
				shop_sale_brandList = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale_Brand.class));
			}
			// 促销范围为商品
			if (ssm_scope == 4) {
				sql.setLength(0);
				sql.append(" select ssp_id,ssp_ss_code,ssp_pd_code,ssp_discount,ssp_buy_full,ssp_buy_number,");
				sql.append(" ssp_donation_amount,ssp_reduce_amount,ssp_increased_amount,ssp_special_offer,ssp_index,pd_name,pd_no,pd_sell_price");
				sql.append(" from t_shop_sale_product t");
				sql.append(" join t_base_product pd on pd.pd_code=t.ssp_pd_code and pd.companyid=t.companyid");
				sql.append(" where 1=1 ");
				sql.append(" and t.ssp_ss_code=:ss_code");
				sql.append(" and t.companyid=:companyid");
				shop_sale_productList = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale_Product.class));
			}
			if (verifyIsPresent(t_Shop_Sale.getSs_model())) {
				sql.setLength(0);
				sql.append("select ssp_id,ssp_ss_code,ssp_subcode,ssp_count,ssp_pd_code,ssp_index,pd_name,pd_no,");
				sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr.cr_code = sd.sd_cr_code AND cr.companyid = sd.companyid LIMIT 1) AS cr_name,");
				sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz.sz_code = sd.sd_sz_code AND sz.companyid = sd.companyid LIMIT 1) AS sz_name,");
				sql.append(" (SELECT br_name FROM t_base_bra br WHERE br.br_code = sd.sd_br_code AND br.companyid = sd.companyid LIMIT 1) AS br_name");
				sql.append(" from t_shop_sale_present t");
				sql.append(" join t_stock_data sd on sd.sd_code=t.ssp_subcode and sd.companyid=t.companyid");
				sql.append(" JOIN t_base_product pd on pd.pd_code=sd.sd_pd_code and pd.companyid=sd.companyid");
				sql.append(" where 1=1");
				sql.append(" and t.ssp_ss_code=:ss_code");
				sql.append(" and t.companyid=:companyid");
				sql.append(" GROUP BY t.ssp_subcode,t.ssp_id");
				shop_sale_presentList = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale_Present.class));
			}
			saleMap = new HashMap<String, Object>(6);
			saleMap.put("t_Shop_Sale", t_Shop_Sale);
			saleMap.put("shop_sale_allList", shop_sale_allList);
			saleMap.put("shop_sale_typeList", shop_sale_typeList);
			saleMap.put("shop_sale_brandList", shop_sale_brandList);
			saleMap.put("shop_sale_productList", shop_sale_productList);
			saleMap.put("shop_sale_presentList", shop_sale_presentList);
			t_Shop_Sale = null;
			shop_sale_allList = null;
			shop_sale_typeList = null;
			shop_sale_brandList = null;
			shop_sale_productList = null;
			shop_sale_presentList = null;
		}
		return saleMap;
	}
	
	public boolean verifyIsPresent(String sm_code){
		boolean flag = false;
		String smCodeArr = "313,314,323,324,333,334,343,344,315,325,335,345,"; 
		if(smCodeArr.indexOf(sm_code+",") > -1){
			flag = true;
		}
		return flag;
	}

	@Override
	public T_Shop_Sale check(String ss_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT ss_id,ss_code,ss_state,companyid");
		sql.append(" FROM t_shop_sale t");
		sql.append(" WHERE ss_code = :ss_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Shop_Sale.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void updateApprove(T_Shop_Sale sale) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_shop_sale");
		sql.append(" SET ss_state=:ss_state");
		sql.append(" WHERE ss_id=:ss_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(sale));
	}

	@Override
	public void del(String ss_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_shop_sale");
		sql.append(" WHERE ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_shop_sale_all");
		sql.append(" WHERE ssa_ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_shop_sale_brand");
		sql.append(" WHERE ssb_ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_shop_sale_present");
		sql.append(" WHERE ssp_ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_shop_sale_product");
		sql.append(" WHERE ssp_ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_shop_sale_shop");
		sql.append(" WHERE sss_ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_shop_sale_type");
		sql.append(" WHERE sst_ss_code=:ss_code");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ss_code", ss_code).addValue("companyid", companyid));
	}

	@Override
	public List<T_Shop_Sale> list(Map<String, Object> param, int limit) {
		param.put("limit", limit);
		Object name = param.get("name");
		StringBuffer sql = new StringBuffer("");
		sql.append(" select ss_id,ss_code,ss_name,ss_begin_date,ss_end_date,ss_mt_code,");
		sql.append(" ss_priority,CONCAT(ssm.ssm_scope_explain,'-',ssm.ssm_model_explain,'-',ssm_rule_explain) ss_model");
		sql.append(" from t_shop_sale t");
		sql.append(" join t_shop_sale_model ssm on ssm.ssm_code = t.ss_model ");
		sql.append(" join t_shop_sale_shop sss on sss.sss_ss_code = t.ss_code and sss.companyid = t.companyid ");
		sql.append(" where 1 = 1");
		sql.append(" AND ss_end_date >= sysdate()");
		sql.append(" AND sss.sss_shop_code=:shop_code");
		if(!StringUtil.isEmpty(name)){
			sql.append(" AND INSTR(t.ss_name,:name)>0");
		}
		sql.append(" AND t.ss_state=1");
		sql.append(" and t.companyid=:companyid");
		sql.append(" group by t.ss_code ");
		sql.append(" order by t.ss_id desc ");
		sql.append(" limit :limit");
		List<T_Shop_Sale> list = namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Shop_Sale.class));
		return list;
	}

	@Override
	public Integer rateList_count(Map<String, Object> paramMap) {
		Object shop_type = paramMap.get(CommonUtil.SHOP_TYPE);
		Object begindate = paramMap.get("begindate");
		Object enddate = paramMap.get("enddate");
		Object shl_shop_code = paramMap.get("shl_shop_code");
		Object tp_code = paramMap.get("tp_code");
		Object bd_code = paramMap.get("bd_code");
		Object pd_no = paramMap.get("pd_no");
		Object pd_name = paramMap.get("pd_name");
		Object ss_code = paramMap.get("ss_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT COUNT(1) ");
		sql.append(" FROM t_sell_shoplist t");
		sql.append(" JOIN t_base_product p ON p.pd_code=t.shl_pd_code AND p.companyid=t.companyid ");
		sql.append(" JOIN t_base_shop sp on sp.sp_code = t.shl_shop_code AND sp.companyid = t.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (shl_shop_code != null && !"".equals(shl_shop_code)) {
				sql.append(" AND t.shl_shop_code=:shl_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if(pd_no!=null && !"".equals(pd_no)){
			sql.append(" AND instr(p.pd_no,:pd_no)>0 ");
		}
		if(pd_name!=null && !"".equals(pd_name)){
			sql.append(" AND instr(p.pd_name,:pd_name)>0 ");
		}
		if(ss_code != null && !"".equals(ss_code)){
			sql.append(" AND instr(shl_sale_code,:ss_code)>0 ");
		}
		if(bd_code != null && !"".equals(bd_code)){
			sql.append(" AND p.pd_bd_code = :bd_code");
		}
		if(tp_code!=null && !"".equals(tp_code)){
			sql.append(" AND (pd_tp_code = :tp_code OR pd_tp_upcode = :tp_code)");
		}
		sql.append(" AND t.shl_sale_code<>''");
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND shl_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), paramMap ,Integer.class);
	}

	@Override
	public List<T_Sell_ShopList> rateList_list(Map<String, Object> paramMap) {
		Object shop_type = paramMap.get(CommonUtil.SHOP_TYPE);
		Object sidx = paramMap.get(CommonUtil.SIDX);
		Object sord = paramMap.get(CommonUtil.SORD);
		Object begindate = paramMap.get("begindate");
		Object enddate = paramMap.get("enddate");
		Object shl_shop_code = paramMap.get("shl_shop_code");
		Object tp_code = paramMap.get("tp_code");
		Object bd_code = paramMap.get("bd_code");
		Object pd_no = paramMap.get("pd_no");
		Object pd_name = paramMap.get("pd_name");
		Object ss_code = paramMap.get("ss_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT shl_number,shl_amount,shl_price,shl_money,shl_sell_price,(shl_sell_price*shl_amount) as shl_sell_money,");
		sql.append(" shl_cost_price,(shl_cost_price*shl_amount) as shl_cost_money,(shl_money-shl_cost_price*shl_amount) as shl_profits,(shl_sell_price*shl_amount-shl_money) as shl_discountmonery,");
		sql.append(" shl_sale_code,shl_pd_code,pd_no,p.pd_name,sp.sp_name as shop_name,shl_date,p.pd_unit,shl_cr_code,shl_sz_code,shl_br_code,shl_vip_code,");
		sql.append("(select bd_name from t_base_brand bd where bd.bd_code=p.pd_bd_code and bd.companyid=p.companyid LIMIT 1) as bd_name,");//品牌
		sql.append("(select tp_name from t_base_type tp where tp.tp_code=p.pd_tp_code and tp.companyid=p.companyid LIMIT 1) as tp_name,");//类别
		sql.append(" (SELECT cr_name FROM t_base_color c WHERE c.cr_code=t.shl_cr_code AND c.companyid=t.companyid LIMIT 1) cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size c WHERE c.sz_code=t.shl_sz_code AND c.companyid=t.companyid LIMIT 1) sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra c WHERE c.br_code=t.shl_br_code AND c.companyid=t.companyid LIMIT 1) br_name,");
		sql.append(" (SELECT em_name FROM t_base_emp c WHERE c.em_code=t.shl_em_code AND c.companyid=t.companyid LIMIT 1) ca_name,");
		sql.append(" (SELECT em_name FROM t_base_emp c WHERE c.em_code=t.shl_main AND c.companyid=t.companyid LIMIT 1) main_name,");
		sql.append(" (SELECT vm_name FROM t_vip_member c WHERE c.vm_code=t.shl_vip_code AND c.companyid=t.companyid LIMIT 1) vip_name");
		sql.append(" FROM t_sell_shoplist t");
		sql.append(" JOIN t_base_product p ON p.pd_code=t.shl_pd_code AND p.companyid=t.companyid ");
		sql.append(" JOIN t_base_shop sp on sp.sp_code = t.shl_shop_code AND sp.companyid = t.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (shl_shop_code != null && !"".equals(shl_shop_code)) {
				sql.append(" AND t.shl_shop_code=:shl_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if(pd_no!=null && !"".equals(pd_no)){
			sql.append(" AND instr(p.pd_no,:pd_no)>0 ");
		}
		if(pd_name!=null && !"".equals(pd_name)){
			sql.append(" AND instr(p.pd_name,:pd_name)>0 ");
		}
		if(ss_code != null && !"".equals(ss_code)){
			sql.append(" AND instr(shl_sale_code,:ss_code)>0 ");
		}
		if(bd_code != null && !"".equals(bd_code)){
			sql.append(" AND p.pd_bd_code = :bd_code");
		}
		if(tp_code!=null && !"".equals(tp_code)){
			sql.append(" AND (pd_tp_code = :tp_code OR pd_tp_upcode = :tp_code)");
		}
		sql.append(" AND t.shl_sale_code<>''");
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND shl_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY shl_id DESC ");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), paramMap, 
				new BeanPropertyRowMapper<>(T_Sell_ShopList.class));
	}

	@Override
	public Map<String, Object> rateList_sum(Map<String, Object> paramMap) {
		Object shop_type = paramMap.get(CommonUtil.SHOP_TYPE);
		Object begindate = paramMap.get("begindate");
		Object enddate = paramMap.get("enddate");
		Object shl_shop_code = paramMap.get("shl_shop_code");
		Object tp_code = paramMap.get("tp_code");
		Object bd_code = paramMap.get("bd_code");
		Object pd_no = paramMap.get("pd_no");
		Object pd_name = paramMap.get("pd_name");
		Object ss_code = paramMap.get("ss_code");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ");
		sql.append(" IFNULL(SUM(shl_amount), 0) AS shl_amount,");
		sql.append(" IFNULL(SUM(shl_sell_price*shl_amount), 0) AS shl_sell_money,");
		sql.append(" IFNULL(SUM(shl_money), 0) AS shl_money,");
		sql.append(" IFNULL(SUM(shl_cost_price*shl_amount), 0) AS shl_cost_money,");
		sql.append(" IFNULL(SUM(shl_money-shl_cost_price*shl_amount), 0) AS shl_profits,");
		sql.append(" IFNULL(SUM(shl_sell_price*shl_amount-shl_money), 0) AS shl_discountmonery ");
		sql.append(" FROM t_sell_shoplist t");
		sql.append(" JOIN t_base_product p ON p.pd_code=t.shl_pd_code AND p.companyid=t.companyid ");
		sql.append(" JOIN t_base_shop sp on sp.sp_code = t.shl_shop_code AND sp.companyid = t.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (shl_shop_code != null && !"".equals(shl_shop_code)) {
				sql.append(" AND t.shl_shop_code=:shl_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if(pd_no!=null && !"".equals(pd_no)){
			sql.append(" AND instr(p.pd_no,:pd_no)>0 ");
		}
		if(pd_name!=null && !"".equals(pd_name)){
			sql.append(" AND instr(p.pd_name,:pd_name)>0 ");
		}
		if(ss_code != null && !"".equals(ss_code)){
			sql.append(" AND instr(shl_sale_code,:ss_code)>0 ");
		}
		if(bd_code != null && !"".equals(bd_code)){
			sql.append(" AND p.pd_bd_code = :bd_code");
		}
		if(tp_code!=null && !"".equals(tp_code)){
			sql.append(" AND (pd_tp_code = :tp_code OR pd_tp_upcode = :tp_code)");
		}
		sql.append(" AND t.shl_sale_code<>''");
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND shl_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForMap(sql.toString(), paramMap);
	}

	@Override
	public List<T_Sell_ShopList> totalList_list(Map<String, Object> paramMap) {
		Object shop_type = paramMap.get(CommonUtil.SHOP_TYPE);
		Object sidx = paramMap.get(CommonUtil.SIDX);
		Object sord = paramMap.get(CommonUtil.SORD);
		Object begindate = paramMap.get("begindate");
		Object enddate = paramMap.get("enddate");
		Object shl_shop_code = paramMap.get("shl_shop_code");
		String tp_code = (String)paramMap.get("tp_code");
		String bd_code = (String)paramMap.get("bd_code");
		Object pd_no = paramMap.get("pd_no");
		Object pd_name = paramMap.get("pd_name");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT SUM(shl_amount) AS shl_amount,SUM(shl_price) AS shl_price,SUM(shl_money) AS shl_money,SUM(shl_sell_price) AS shl_sell_price,SUM(shl_sell_price*shl_amount) as shl_sell_money,");
		sql.append(" SUM(shl_cost_price) AS shl_cost_price,SUM(shl_cost_price*shl_amount) as shl_cost_money,SUM(shl_money-shl_cost_price*shl_amount) as shl_profits,SUM(shl_sell_price*shl_amount-shl_money) as shl_discountmonery,");
		sql.append(" sp.sp_name as shop_name,shl_shop_code");
		sql.append(" FROM t_sell_shoplist t");
		sql.append(" JOIN t_base_product p ON p.pd_code=t.shl_pd_code AND p.companyid=t.companyid ");
		sql.append(" JOIN t_base_shop sp on sp.sp_code = t.shl_shop_code AND sp.companyid = t.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (shl_shop_code != null && !"".equals(shl_shop_code)) {
				sql.append(" AND t.shl_shop_code=:shl_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if(pd_no!=null && !"".equals(pd_no)){
			sql.append(" AND instr(p.pd_no,:pd_no)>0 ");
		}
		if(pd_name!=null && !"".equals(pd_name)){
			sql.append(" AND instr(p.pd_name,:pd_name)>0 ");
		}
		//品牌
		if (bd_code != null && !"".equals(bd_code)) {
			String[] bd_codes = bd_code.split(",");
			for( int i=0; i<bd_codes.length; i++ ){
				if(i==0){
					sql.append(" AND (p.pd_bd_code = '"+bd_codes[i]+"' ");
				}else{
					sql.append(" OR p.pd_bd_code = '"+bd_codes[i]+"'");
				}
				if( i==bd_codes.length-1 ){
					sql.append(" )");
				}
			}
		}
		//类别
		if (tp_code != null && !"".equals(tp_code)) {
			String[] tp_codes = tp_code.split(",");
			for(int i=0;i<tp_codes.length;i++){
				if(i==0){
					sql.append(" AND (pd_tp_code = '"+tp_codes[i]+"' ");
				}else{
					sql.append(" OR pd_tp_code = '"+tp_codes[i]+"'");
				}
				if( i==tp_codes.length-1 ){
					sql.append(" )");
				}
			}
		}
		sql.append(" AND t.shl_sale_code<>''");
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND shl_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" AND t.companyid=:companyid");
		sql.append(" GROUP BY shl_shop_code ");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY shl_shop_code DESC ");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), paramMap, 
				new BeanPropertyRowMapper<>(T_Sell_ShopList.class));
	}

	@Override
	public Map<String, Object> totalList_sum(Map<String, Object> paramMap) {
		Object shop_type = paramMap.get(CommonUtil.SHOP_TYPE);
		Object begindate = paramMap.get("begindate");
		Object enddate = paramMap.get("enddate");
		Object shl_shop_code = paramMap.get("shl_shop_code");
		String tp_code = (String)paramMap.get("tp_code");
		String bd_code = (String)paramMap.get("bd_code");
		Object pd_no = paramMap.get("pd_no");
		Object pd_name = paramMap.get("pd_name");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT SUM(shl_amount) AS shl_amount,SUM(shl_price) AS shl_price,SUM(shl_money) AS shl_money,SUM(shl_sell_price) AS shl_sell_price,SUM(shl_sell_price*shl_amount) as shl_sell_money,");
		sql.append(" SUM(shl_cost_price) AS shl_cost_price,SUM(shl_cost_price*shl_amount) as shl_cost_money,SUM(shl_money-shl_cost_price*shl_amount) as shl_profits,SUM(shl_sell_price*shl_amount-shl_money) as shl_discountmonery ");
		sql.append(" FROM t_sell_shoplist t");
		sql.append(" JOIN t_base_product p ON p.pd_code=t.shl_pd_code AND p.companyid=t.companyid ");
		sql.append(" JOIN t_base_shop sp on sp.sp_code = t.shl_shop_code AND sp.companyid = t.companyid ");
		if(CommonUtil.ONE.equals(shop_type) || CommonUtil.TWO.equals(shop_type)){//总公司、分公司
			sql.append(getShopSQL(shop_type, 0));
			sql.append(" WHERE 1 = 1");
			if (shl_shop_code != null && !"".equals(shl_shop_code)) {
				sql.append(" AND t.shl_shop_code=:shl_shop_code ");
			}
		}else{//自营、加盟、合伙
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}
		if(pd_no!=null && !"".equals(pd_no)){
			sql.append(" AND instr(p.pd_no,:pd_no)>0 ");
		}
		if(pd_name!=null && !"".equals(pd_name)){
			sql.append(" AND instr(p.pd_name,:pd_name)>0 ");
		}
		//品牌
		if (bd_code != null && !"".equals(bd_code)) {
			String[] bd_codes = bd_code.split(",");
			for( int i=0; i<bd_codes.length; i++ ){
				if(i==0){
					sql.append(" AND (p.pd_bd_code = '"+bd_codes[i]+"' ");
				}else{
					sql.append(" OR p.pd_bd_code = '"+bd_codes[i]+"'");
				}
				if( i==bd_codes.length-1 ){
					sql.append(" )");
				}
			}
		}
		//类别
		if (tp_code != null && !"".equals(tp_code)) {
			String[] tp_codes = tp_code.split(",");
			for(int i=0;i<tp_codes.length;i++){
				if(i==0){
					sql.append(" AND (pd_tp_code = '"+tp_codes[i]+"' ");
				}else{
					sql.append(" OR pd_tp_code = '"+tp_codes[i]+"'");
				}
				if( i==tp_codes.length-1 ){
					sql.append(" )");
				}
			}
		}
		sql.append(" AND t.shl_sale_code<>''");
		if (begindate != null && !"".equals(begindate) && enddate != null && !"".equals(enddate)) {
			sql.append(" AND shl_date BETWEEN :begindate AND :enddate");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForMap(sql.toString(), paramMap);
	}
	
}
