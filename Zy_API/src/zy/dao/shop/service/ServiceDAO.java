package zy.dao.shop.service;

import java.util.List;
import java.util.Map;

import zy.entity.shop.service.T_Shop_Service;
import zy.entity.shop.service.T_Shop_Service_Item;

public interface ServiceDAO {
	Integer count(Map<String,Object> param);
	List<T_Shop_Service> list(Map<String,Object> param);
	void save(T_Shop_Service service);
	void update(T_Shop_Service service);
	void del(Integer ss_id);
	T_Shop_Service load(Integer ss_id);
	Integer list_serviceitemcount(Map<String,Object> param);
	List<T_Shop_Service_Item> list_serviceitem(Map<String,Object> param);
	Integer queryItemByName(T_Shop_Service_Item service_Item);
	void save_serviceitem(T_Shop_Service_Item service_Item);
	void update_serviceitem(T_Shop_Service_Item service_Item);
	T_Shop_Service_Item queryServiceItemByID(Integer ssi_id);
	void del_serviceitem(Integer ssi_id);
}
