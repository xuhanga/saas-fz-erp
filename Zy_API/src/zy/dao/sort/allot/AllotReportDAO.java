package zy.dao.sort.allot;

import java.util.List;
import java.util.Map;

import zy.dto.sort.allot.AllotDetailReportDto;
import zy.dto.sort.allot.AllotReportDto;

public interface AllotReportDAO {
	Integer countAllotReport(Map<String,Object> params);
	Map<String, Object> sumAllotReport(Map<String, Object> params);
	List<AllotReportDto> listAllotReport(Map<String,Object> params);
	Integer countAllotDetailReport(Map<String,Object> params);
	Map<String, Object> sumAllotDetailReport(Map<String, Object> params);
	List<AllotDetailReportDto> listAllotDetailReport(Map<String,Object> params);
}
