package zy.dao.sort.allot.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sort.allot.AllotReportDAO;
import zy.dto.sort.allot.AllotDetailReportDto;
import zy.dto.sort.allot.AllotReportDto;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class AllotReportDAOImpl extends BaseDaoImpl implements AllotReportDAO{

	private String getAllotReportSQL(Map<String, Object> params){
		StringBuffer sql = new StringBuffer();
		String bd_code = StringUtil.trimString(params.get("bd_code"));
		String tp_code = StringUtil.trimString(params.get("tp_code"));
		String at_shop_code = StringUtil.trimString(params.get("at_shop_code"));
		if(StringUtil.isNotEmpty(bd_code)){
			params.put("bdCodes", Arrays.asList(bd_code.split(",")));
			sql.append(" AND pd_bd_code IN(:bdCodes)");
		}
		if(StringUtil.isNotEmpty(tp_code)){
			params.put("tpCodes", Arrays.asList(tp_code.split(",")));
			sql.append(" AND pd_tp_code IN(:tpCodes)");
		}
		if(StringUtil.isNotEmpty(at_shop_code)){
			params.put("shop_codes", Arrays.asList(at_shop_code.split(",")));
			sql.append(" AND at_shop_code IN (:shop_codes) ");
		}
		if(StringUtil.isNotEmpty(params.get("at_outdp_code"))){
			sql.append(" AND at_outdp_code = :at_outdp_code ");
		}
		if(StringUtil.isNotEmpty(params.get("at_indp_code"))){
			sql.append(" AND at_indp_code = :at_indp_code ");
		}
		if(StringUtil.isNotEmpty(params.get("pd_season"))){
			sql.append(" AND pd_season = :pd_season ");
		}
		if(StringUtil.isNotEmpty(params.get("pd_year"))){
			sql.append(" AND pd_year = :pd_year ");
		}
		if(StringUtil.isNotEmpty(params.get("at_manager"))){
			sql.append(" AND at_manager = :at_manager ");
		}
		if(StringUtil.isNotEmpty(params.get("pd_code"))){
			sql.append(" AND pd_code = :pd_code ");
		}
		if(StringUtil.isNotEmpty(params.get("pd_no"))){
			sql.append(" AND INSTR(pd_no,:pd_no) > 0 ");
		}
		if(StringUtil.isNotEmpty(params.get("begindate"))){
			sql.append(" AND at_ar_date >= :begindate ");
		}
		if(StringUtil.isNotEmpty(params.get("enddate"))){
			sql.append(" AND at_ar_date <= :enddate ");
		}
		if(StringUtil.isNotEmpty(params.get("at_type"))){
			sql.append(" AND at_type = :at_type ");
		}
		if(StringUtil.isNotEmpty(params.get("at_ar_state"))){
			sql.append(" AND at_ar_state = :at_ar_state ");
		}else {
			sql.append(" AND at_ar_state IN (3,4,5)");
		}
		sql.append(" AND t.companyid = :companyid");
		return sql.toString();
	}
	
	@Override
	public Integer countAllotReport(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		String type = StringUtil.trimString(params.get("type"));
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(1) FROM");
		sql.append(" (SELECT 1 FROM t_sort_allot t");
		sql.append(" JOIN t_sort_allotlist atl ON atl_number = at_number AND atl.companyid = t.companyid");
		sql.append(" JOIN t_base_product pd ON pd_code = atl_pd_code AND pd.companyid = atl.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = at_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type)){//总公司：配货发货单、退货确认单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_shop_type = "+CommonUtil.TWO);
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司：配货发货单、退货确认单、配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_code = :shop_code");
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//加盟店、合伙店：配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}else {
			sql.append(" WHERE 1 = 2");
		}
		sql.append(getAllotReportSQL(params));
		if("brand".equals(type)){
			sql.append(" GROUP BY pd_bd_code");
		}else if("type".equals(type)){
			sql.append(" GROUP BY pd_tp_code");
		}else if("shop".equals(type)){
			sql.append(" GROUP BY at_shop_code");
		}else if("product".equals(type)){
			sql.append(" GROUP BY atl_pd_code");
		}else if("season".equals(type)){
			sql.append(" GROUP BY pd_season");
		}else if("depot".equals(type)){
			sql.append(" GROUP BY at_outdp_code,at_indp_code");
		}else if("manager".equals(type)){
			sql.append(" GROUP BY at_manager");
		}
		sql.append(")temp");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public Map<String, Object> sumAllotReport(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ");
		sql.append(" SUM(IF(at_type = 0,atl_applyamount,0)) AS apply_amount,");
		sql.append(" SUM(IF(at_type = 0,atl_sendamount,0)) AS send_amount,");
		sql.append(" SUM(IF(at_type = 1,-atl_applyamount,0)) AS apply_amount_th,");
		sql.append(" SUM(IF(at_type = 1,-atl_sendamount,0)) AS send_amount_th,");
		sql.append(" SUM(IF(at_type = 0,atl_applyamount*atl_unitprice,0)) AS apply_money,");
		sql.append(" SUM(IF(at_type = 0,atl_sendamount*atl_unitprice,0)) AS send_money,");
		sql.append(" SUM(IF(at_type = 0,atl_sendamount*atl_costprice,0)) AS send_cost,");
		sql.append(" SUM(IF(at_type = 1,-atl_applyamount*atl_unitprice,0)) AS apply_money_th,");
		sql.append(" SUM(IF(at_type = 1,-atl_sendamount*atl_unitprice,0)) AS send_money_th,");
		sql.append(" SUM(IF(at_type = 1,-atl_sendamount*atl_costprice,0)) AS send_cost_th");
		sql.append(" FROM t_sort_allot t");
		sql.append(" JOIN t_sort_allotlist atl ON atl_number = at_number AND atl.companyid = t.companyid");
		sql.append(" JOIN t_base_product pd ON pd_code = atl_pd_code AND pd.companyid = atl.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = at_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type)){//总公司：配货发货单、退货确认单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_shop_type = "+CommonUtil.TWO);
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司：配货发货单、退货确认单、配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_code = :shop_code");
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//加盟店、合伙店：配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}else {
			sql.append(" WHERE 1 = 2");
		}
		sql.append(getAllotReportSQL(params));
		return namedParameterJdbcTemplate.queryForMap(sql.toString(), params);
	}

	@Override
	public List<AllotReportDto> listAllotReport(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		String type = StringUtil.trimString(params.get("type"));
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT atl_id AS id,");
		sql.append(" SUM(IF(at_type = 0,atl_applyamount,0)) AS apply_amount,");
		sql.append(" SUM(IF(at_type = 0,atl_sendamount,0)) AS send_amount,");
		sql.append(" SUM(IF(at_type = 1,-atl_applyamount,0)) AS apply_amount_th,");
		sql.append(" SUM(IF(at_type = 1,-atl_sendamount,0)) AS send_amount_th,");
		sql.append(" SUM(IF(at_type = 0,atl_applyamount*atl_unitprice,0)) AS apply_money,");
		sql.append(" SUM(IF(at_type = 0,atl_sendamount*atl_unitprice,0)) AS send_money,");
		sql.append(" SUM(IF(at_type = 0,atl_sendamount*atl_costprice,0)) AS send_cost,");
		sql.append(" SUM(IF(at_type = 1,-atl_applyamount*atl_unitprice,0)) AS apply_money_th,");
		sql.append(" SUM(IF(at_type = 1,-atl_sendamount*atl_unitprice,0)) AS send_money_th,");
		sql.append(" SUM(IF(at_type = 1,-atl_sendamount*atl_costprice,0)) AS send_cost_th,");
		if("brand".equals(type)){
			sql.append(" pd_bd_code AS code,");
			sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS name");
		}else if("type".equals(type)){
			sql.append(" pd_tp_code AS code,");
			sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS name");
		}else if("shop".equals(type)){
			sql.append(" at_shop_code AS code,sp_name AS name");
		}else if("product".equals(type)){
			sql.append(" atl_pd_code AS code,pd_name AS name,pd_no");
		}else if("season".equals(type)){
			sql.append(" pd_season AS code,pd_season AS name");
		}else if("depot".equals(type)){
			sql.append(" at_outdp_code AS code,at_indp_code AS indp_code,");
			sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = at_outdp_code AND dp.companyid = t.companyid LIMIT 1) AS name,");
			sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = at_indp_code AND dp.companyid = t.companyid LIMIT 1) AS indp_name");
		}else if("manager".equals(type)){
			sql.append(" at_manager AS code,at_manager AS name");
		}
		sql.append(" FROM t_sort_allot t");
		sql.append(" JOIN t_sort_allotlist atl ON atl_number = at_number AND atl.companyid = t.companyid");
		sql.append(" JOIN t_base_product pd ON pd_code = atl_pd_code AND pd.companyid = atl.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = at_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type)){//总公司：配货发货单、退货确认单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_shop_type = "+CommonUtil.TWO);
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司：配货发货单、退货确认单、配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_code = :shop_code");
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//加盟店、合伙店：配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}else {
			sql.append(" WHERE 1 = 2");
		}
		sql.append(getAllotReportSQL(params));
		if("brand".equals(type)){
			sql.append(" GROUP BY pd_bd_code");
		}else if("type".equals(type)){
			sql.append(" GROUP BY pd_tp_code");
		}else if("shop".equals(type)){
			sql.append(" GROUP BY at_shop_code");
		}else if("product".equals(type)){
			sql.append(" GROUP BY atl_pd_code");
		}else if("season".equals(type)){
			sql.append(" GROUP BY pd_season");
		}else if("depot".equals(type)){
			sql.append(" GROUP BY at_outdp_code,at_indp_code");
		}else if("manager".equals(type)){
			sql.append(" GROUP BY at_manager");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(AllotReportDto.class));
	}

	@Override
	public Integer countAllotDetailReport(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sort_allot t");
		sql.append(" JOIN t_sort_allotlist atl ON atl_number = at_number AND atl.companyid = t.companyid");
		sql.append(" JOIN t_base_product pd ON pd_code = atl_pd_code AND pd.companyid = atl.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = at_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type)){//总公司：配货发货单、退货确认单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_shop_type = "+CommonUtil.TWO);
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司：配货发货单、退货确认单、配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_code = :shop_code");
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//加盟店、合伙店：配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}else {
			sql.append(" WHERE 1 = 2");
		}
		sql.append(getAllotReportSQL(params));
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}
	
	@Override
	public Map<String, Object> sumAllotDetailReport(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT ");
		sql.append(" SUM(atl_applyamount) AS atl_applyamount,");
		sql.append(" SUM(atl_sendamount) AS atl_sendamount,");
		sql.append(" SUM(atl_applyamount*atl_unitprice) AS atl_applymoney,");
		sql.append(" SUM(atl_sendamount*atl_unitprice) AS atl_sendmoney,");
		sql.append(" SUM(atl_sendamount*atl_costprice) AS atl_sendcost");
		sql.append(" FROM t_sort_allot t");
		sql.append(" JOIN t_sort_allotlist atl ON atl_number = at_number AND atl.companyid = t.companyid");
		sql.append(" JOIN t_base_product pd ON pd_code = atl_pd_code AND pd.companyid = atl.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = at_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type)){//总公司：配货发货单、退货确认单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_shop_type = "+CommonUtil.TWO);
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司：配货发货单、退货确认单、配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_code = :shop_code");
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//加盟店、合伙店：配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}else {
			sql.append(" WHERE 1 = 2");
		}
		sql.append(getAllotReportSQL(params));
		return namedParameterJdbcTemplate.queryForMap(sql.toString(), params);
	}
	
	@Override
	public List<AllotDetailReportDto> listAllotDetailReport(Map<String, Object> params) {
		Object shop_type = params.get(CommonUtil.SHOP_TYPE);
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT atl_id,at_date,atl_number,atl_pd_code,atl_sub_code,atl_sz_code,atl_szg_code,atl_cr_code,atl_br_code,atl_applyamount,atl_sendamount,");
		sql.append(" atl_unitprice,atl_costprice,atl_remark,atl_type,t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" sp_name AS shop_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = at_outdp_code AND dp.companyid = t.companyid LIMIT 1) AS outdp_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = at_indp_code AND dp.companyid = t.companyid LIMIT 1) AS indp_name,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = atl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = atl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = atl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_sort_allot t");
		sql.append(" JOIN t_sort_allotlist atl ON atl_number = at_number AND atl.companyid = t.companyid");
		sql.append(" JOIN t_base_product pd ON pd_code = atl_pd_code AND pd.companyid = atl.companyid");
		sql.append(" JOIN t_base_shop sp ON sp_code = at_shop_code AND sp.companyid = t.companyid");
		if(CommonUtil.ONE.equals(shop_type)){//总公司：配货发货单、退货确认单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_shop_type = "+CommonUtil.TWO);
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.TWO.equals(shop_type)){//分公司：配货发货单、退货确认单、配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND (sp_code = :shop_code");
			sql.append(" OR (sp_upcode = :shop_code AND sp_shop_type IN("+CommonUtil.FOUR+","+CommonUtil.FIVE+"))");
			sql.append(")");
		}else if(CommonUtil.FOUR.equals(shop_type) || CommonUtil.FIVE.equals(shop_type)){//加盟店、合伙店：配货申请单、退货申请单
			sql.append(" WHERE 1 = 1");
			sql.append(" AND sp_code = :shop_code");
		}else {
			sql.append(" WHERE 1 = 2");
		}
		sql.append(getAllotReportSQL(params));
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(AllotDetailReportDto.class));
	}
	
}
