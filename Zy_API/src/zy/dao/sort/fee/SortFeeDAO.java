package zy.dao.sort.fee;

import java.util.List;
import java.util.Map;

import zy.entity.sort.fee.T_Sort_Fee;
import zy.entity.sort.fee.T_Sort_FeeList;

public interface SortFeeDAO {
	Integer count(Map<String,Object> params);
	List<T_Sort_Fee> list(Map<String,Object> params);
	T_Sort_Fee load(Integer fe_id);
	T_Sort_Fee load(String number, Integer companyid);
	T_Sort_Fee check(String number,Integer companyid);
	List<T_Sort_FeeList> temp_list(Map<String, Object> params);
	List<String> temp_check(Integer us_id,Integer companyid);
	void temp_save(List<T_Sort_FeeList> temps);
	void temp_updateMoney(T_Sort_FeeList temp);
	void temp_updateRemark(T_Sort_FeeList temp);
	void temp_del(Integer fel_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Sort_FeeList> detail_list(Map<String, Object> params);
	List<T_Sort_FeeList> detail_list_forsavetemp(String fe_number,Integer companyid);;
	void save(T_Sort_Fee fee, List<T_Sort_FeeList> details);
	void update(T_Sort_Fee fee, List<T_Sort_FeeList> details);
	void updateApprove(T_Sort_Fee fee);
	void del(String fe_number, Integer companyid);
	void deleteList(String fe_number, Integer companyid);
}
