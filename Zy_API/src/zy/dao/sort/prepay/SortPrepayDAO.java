package zy.dao.sort.prepay;

import java.util.List;
import java.util.Map;

import zy.entity.sort.prepay.T_Sort_Prepay;

public interface SortPrepayDAO {
	Integer count(Map<String,Object> params);
	List<T_Sort_Prepay> list(Map<String,Object> params);
	T_Sort_Prepay load(Integer pp_id);
	T_Sort_Prepay load(String number,Integer companyid);
	T_Sort_Prepay check(String number,Integer companyid);
	void save(T_Sort_Prepay prepay);
	void update(T_Sort_Prepay prepay);
	void updateApprove(T_Sort_Prepay prepay);
	void del(String pp_number, Integer companyid);
}
