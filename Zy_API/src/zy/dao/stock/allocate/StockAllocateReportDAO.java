package zy.dao.stock.allocate;

import java.util.List;
import java.util.Map;

import zy.dto.stock.allocate.StockAllocateDetailReportDto;
import zy.dto.stock.allocate.StockAllocateReportDto;

public interface StockAllocateReportDAO {
	Map<String, Object> countsumAllocateReport(Map<String, Object> params);
	List<StockAllocateReportDto> listAllocateReport(Map<String,Object> params);
	Map<String, Object> countsumAllocateDetailReport(Map<String, Object> params);
	List<StockAllocateDetailReportDto> listAllocateDetailReport(Map<String,Object> params);
}
