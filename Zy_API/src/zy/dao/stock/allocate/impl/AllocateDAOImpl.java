package zy.dao.stock.allocate.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.stock.allocate.AllocateDAO;
import zy.dto.common.ProductDto;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.size.T_Base_Size;
import zy.entity.stock.T_Stock_Import;
import zy.entity.stock.allocate.T_Stock_Allocate;
import zy.entity.stock.allocate.T_Stock_AllocateList;
import zy.entity.stock.data.T_Stock_DataBill;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Repository
public class AllocateDAOImpl extends BaseDaoImpl implements AllocateDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object ac_isdraft = params.get("ac_isdraft");
		Object ac_ar_state = params.get("ac_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object ac_outdp_code = params.get("ac_outdp_code");
		Object ac_indp_code = params.get("ac_indp_code");
		Object ac_manager = params.get("ac_manager");
		Object ac_number = params.get("ac_number");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_stock_allocate t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(ac_isdraft)) {
			sql.append(" AND ac_isdraft = :ac_isdraft ");
		}
		if (StringUtil.isNotEmpty(ac_ar_state)) {
			sql.append(" AND ac_ar_state = :ac_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND ac_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND ac_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(ac_outdp_code)) {
			sql.append(" AND ac_outdp_code = :ac_outdp_code ");
		}
		if (StringUtil.isNotEmpty(ac_indp_code)) {
			sql.append(" AND ac_indp_code = :ac_indp_code ");
		}
		if (StringUtil.isNotEmpty(ac_manager)) {
			sql.append(" AND ac_manager = :ac_manager ");
		}
		if (StringUtil.isNotEmpty(ac_number)) {
			sql.append(" AND INSTR(ac_number,:ac_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Stock_Allocate> list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		Object ac_isdraft = params.get("ac_isdraft");
		Object ac_ar_state = params.get("ac_ar_state");
		Object begindate = params.get("begindate");
		Object enddate = params.get("enddate");
		Object ac_outdp_code = params.get("ac_outdp_code");
		Object ac_indp_code = params.get("ac_indp_code");
		Object ac_manager = params.get("ac_manager");
		Object ac_number = params.get("ac_number");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ac_id,ac_number,ac_date,ac_outdp_code,ac_indp_code,ac_manager,ac_amount,ac_money,ac_remark,ac_isdraft,ac_ar_date,");
		sql.append(" ac_ar_state,ac_us_id,ac_sysdate,companyid,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = ac_outdp_code AND dp.companyid = t.companyid LIMIT 1) AS outdepot_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = ac_indp_code AND dp.companyid = t.companyid LIMIT 1) AS indepot_name");
		sql.append(" FROM t_stock_allocate t");
		sql.append(" WHERE 1=1");
		if (StringUtil.isNotEmpty(ac_isdraft)) {
			sql.append(" AND ac_isdraft = :ac_isdraft ");
		}
		if (StringUtil.isNotEmpty(ac_ar_state)) {
			sql.append(" AND ac_ar_state = :ac_ar_state ");
		}
		if (StringUtil.isNotEmpty(begindate)) {
			sql.append(" AND ac_date >= :begindate ");
		}
		if (StringUtil.isNotEmpty(enddate)) {
			sql.append(" AND ac_date <= :enddate ");
		}
		if (StringUtil.isNotEmpty(ac_outdp_code)) {
			sql.append(" AND ac_outdp_code = :ac_outdp_code ");
		}
		if (StringUtil.isNotEmpty(ac_indp_code)) {
			sql.append(" AND ac_indp_code = :ac_indp_code ");
		}
		if (StringUtil.isNotEmpty(ac_manager)) {
			sql.append(" AND ac_manager = :ac_manager ");
		}
		if (StringUtil.isNotEmpty(ac_number)) {
			sql.append(" AND INSTR(ac_number,:ac_number) > 0 ");
		}
		sql.append(" AND t.companyid=:companyid");
		if(sidx != null && !"".equals(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY ac_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_Allocate.class));
	}

	@Override
	public T_Stock_Allocate load(Integer ac_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ac_id,ac_number,ac_date,ac_outdp_code,ac_indp_code,ac_manager,ac_amount,ac_money,ac_remark,ac_isdraft,ac_ar_date,");
		sql.append(" ac_ar_state,ac_us_id,ac_sysdate,companyid,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = ac_outdp_code AND dp.companyid = t.companyid LIMIT 1) AS outdepot_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = ac_indp_code AND dp.companyid = t.companyid LIMIT 1) AS indepot_name");
		sql.append(" FROM t_stock_allocate t");
		sql.append(" WHERE ac_id = :ac_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("ac_id", ac_id),
					new BeanPropertyRowMapper<>(T_Stock_Allocate.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Stock_Allocate load(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ac_id,ac_number,ac_date,ac_outdp_code,ac_indp_code,ac_manager,ac_amount,ac_money,ac_remark,ac_isdraft,ac_ar_date,");
		sql.append(" ac_ar_state,ac_us_id,ac_sysdate,companyid,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = ac_outdp_code AND dp.companyid = t.companyid LIMIT 1) AS outdepot_name,");
		sql.append(" (SELECT dp_name FROM t_base_depot dp WHERE dp_code = ac_indp_code AND dp.companyid = t.companyid LIMIT 1) AS indepot_name");
		sql.append(" FROM t_stock_allocate t");
		sql.append(" WHERE ac_number = :ac_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("ac_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Stock_Allocate.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public T_Stock_Allocate check(String number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ac_id,ac_number,ac_outdp_code,ac_indp_code,ac_ar_state,ac_remark,companyid");
		sql.append(" FROM t_stock_allocate t");
		sql.append(" WHERE ac_number = :ac_number");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("ac_number", number).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Stock_Allocate.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<T_Stock_AllocateList> detail_list_forsavetemp(String ac_number,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,companyid");
		sql.append(" FROM t_stock_allocatelist t");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_number = :acl_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY acl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("acl_number", ac_number).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}
	
	@Override
	public List<T_Stock_AllocateList> detail_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_number,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,");
		sql.append(" t.companyid,pd_no,pd_name,pd_unit,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = acl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = acl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = acl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_stock_allocatelist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.acl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_number = :acl_number");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY acl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}
	
	@Override
	public List<T_Stock_AllocateList> detail_list_print(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_number,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,");
		sql.append(" t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = acl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = acl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = acl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_stock_allocatelist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.acl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_number = :acl_number");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY acl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}
	
	@Override
	public List<T_Stock_AllocateList> detail_sum(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_number,acl_pd_code,acl_szg_code,SUM(ABS(acl_amount)) AS acl_amount,acl_unitprice,acl_remark,");
		sql.append(" t.companyid,pd_no,pd_name,pd_unit,pd_season,pd_year,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_stock_allocatelist t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.acl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_number = :acl_number");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY acl_pd_code");
		sql.append(" ORDER BY acl_pd_code ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}
	
	@Override
	public List<String> detail_szgcode(Map<String,Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT acl_szg_code");
		sql.append(" FROM t_stock_allocatelist t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND acl_number = :acl_number");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}
	
	@Override
	public List<T_Stock_AllocateList> temp_list(Map<String, Object> params) {
		Object sidx = params.get(CommonUtil.SIDX);
		Object sord = params.get(CommonUtil.SORD);
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,");
		sql.append(" acl_us_id,t.companyid,pd_no,pd_name,pd_unit,");
		sql.append(" (SELECT cr_name FROM t_base_color cr WHERE cr_code = acl_cr_code AND cr.companyid = t.companyid LIMIT 1) AS cr_name,");
		sql.append(" (SELECT sz_name FROM t_base_size sz WHERE sz_code = acl_sz_code AND sz.companyid = t.companyid LIMIT 1) AS sz_name,");
		sql.append(" (SELECT br_name FROM t_base_bra br WHERE br_code = acl_br_code AND br.companyid = t.companyid LIMIT 1) AS br_name");
		sql.append(" FROM t_stock_allocatelist_temp t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.acl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND t.companyid = :companyid");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY acl_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}

	@Override
	public List<T_Stock_AllocateList> temp_list_forimport(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,");
		sql.append(" acl_us_id,t.companyid");
		sql.append(" FROM t_stock_allocatelist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("acl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}
	
	@Override
	public List<T_Stock_AllocateList> temp_list_forsave(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,");
		sql.append(" acl_us_id,t.companyid");
		sql.append(" FROM t_stock_allocatelist_temp t");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" ORDER BY acl_id ASC");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("acl_us_id", us_id).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}
	
	@Override
	public List<T_Stock_AllocateList> temp_sum(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_pd_code,acl_szg_code,SUM(acl_amount) AS acl_amount,acl_unitprice,acl_remark,");
		sql.append(" acl_us_id,t.companyid,pd_no,pd_name,pd_unit,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = t.companyid LIMIT 1) AS bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = t.companyid LIMIT 1) AS tp_name");
		sql.append(" FROM t_stock_allocatelist_temp t");
		sql.append(" JOIN t_base_product pd ON pd_code = t.acl_pd_code AND pd.companyid = t.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND t.companyid = :companyid");
		sql.append(" GROUP BY acl_pd_code");
		sql.append(" ORDER BY acl_pd_code ASC");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
	}

	@Override
	public List<String> temp_szgcode(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT acl_szg_code");
		sql.append(" FROM t_stock_allocatelist_temp t ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND t.companyid = :companyid");
		return namedParameterJdbcTemplate.queryForList(sql.toString(), params, String.class);
	}
	
	@Override
	public T_Stock_AllocateList temp_loadBySubCode(String sub_code, Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_id,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,");
		sql.append(" acl_us_id,companyid");
		sql.append(" FROM t_stock_allocatelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_sub_code = :acl_sub_code");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource().addValue("acl_sub_code", sub_code).addValue("acl_us_id", us_id).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Stock_AllocateList.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public Integer count_product(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		Object alreadyExist = param.get("alreadyExist");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT count(1)");
		sql.append(" FROM (");
		sql.append(" SELECT 1 FROM t_base_product t");
		sql.append(" LEFT JOIN t_stock_allocatelist_temp acl ON acl_pd_code = pd_code AND acl.companyid = t.companyid AND acl_us_id = :us_id");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0)");
			}
        }
		if("1".equals(alreadyExist)){
			sql.append(" AND acl_id IS NOT NULL ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY pd_code)t");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), param, Integer.class);
	}

	@Override
	public List<T_Base_Product> list_product(Map<String, Object> param) {
		Object sidx = param.get(CommonUtil.SIDX);
		Object sord = param.get(CommonUtil.SORD);
		Object searchContent = param.get("searchContent");
		Object alreadyExist = param.get("alreadyExist");
		Object exactQuery = param.get("exactQuery");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pd_id,pd_code,pd_no,pd_name,IF(acl_id IS NULL,0,1) AS exist");
		sql.append(" FROM t_base_product t");
		sql.append(" LEFT JOIN t_stock_allocatelist_temp acl ON acl_pd_code = pd_code AND acl.companyid = t.companyid AND acl_us_id = :us_id");
		sql.append(" where 1 = 1");
		if(StringUtil.isNotEmpty(searchContent)){
			if("1".equals(exactQuery)){
				sql.append(" AND (t.pd_name = :searchContent OR t.pd_no = :searchContent)");
			}else {
				sql.append(" AND (instr(t.pd_name,:searchContent)>0 OR instr(t.pd_spell,:searchContent)>0 OR instr(t.pd_no,:searchContent)>0)");
			}
        }
		if("1".equals(alreadyExist)){
			sql.append(" AND acl_id IS NOT NULL ");
		}
		sql.append(" and t.companyid=:companyid");
		sql.append(" GROUP BY pd_code");
		if(StringUtil.isNotEmpty(sidx)){
			sql.append(" ORDER BY ").append(sidx).append(" ").append(sord);
		}else {
			sql.append(" ORDER BY pd_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Base_Product.class));
	}
	
	@Override
	public T_Base_Product load_product(String pd_code,Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT pd.pd_id,pd.pd_code,pd_no,pd_name,pd_szg_code,pd_unit,pd_year,pd_season,pd_sell_price,pd_buy_price,pd_cost_price,");
		sql.append(" (SELECT bd_name FROM t_base_brand bd WHERE bd_code = pd_bd_code AND bd.companyid = pd.companyid LIMIT 1) AS pd_bd_name,");
		sql.append(" (SELECT tp_name FROM t_base_type tp WHERE tp_code = pd_tp_code AND tp.companyid = pd.companyid LIMIT 1) AS pd_tp_name");
		sql.append(" ,(SELECT pdm_img_path FROM t_base_product_img pdm WHERE pdm_pd_code = pd.pd_code AND pdm.companyid = pd.companyid LIMIT 1) AS pdm_img_path");
		sql.append(" FROM t_base_product pd");
		sql.append(" WHERE 1=1");
		sql.append(" AND pd.pd_code = :pd_code");
		sql.append(" AND pd.companyid = :companyid");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("pd_code", pd_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Base_Product.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public Map<String, Object> load_product_size(Map<String,Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//1.查询尺码信息
		List<T_Base_Size> sizes = namedParameterJdbcTemplate.query(getSizeSQL(), params, new BeanPropertyRowMapper<>(T_Base_Size.class));
		resultMap.put("sizes",sizes);
		//2.获取颜色杯型信息
		List<ProductDto> inputs = namedParameterJdbcTemplate.query(getColorBraSQL(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("inputs",inputs);
		//3.已录入数量
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT acl_sz_code AS sz_code,acl_cr_code AS cr_code,acl_br_code AS br_code,acl_amount AS amount");
		sql.append(" FROM t_stock_allocatelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_pd_code = :pd_code");
		sql.append(" AND acl_us_id = :us_id");
		sql.append(" AND companyid = :companyid");
		List<ProductDto> temps = namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("temps",temps);
		//4.库存数量
		List<ProductDto> stocks = namedParameterJdbcTemplate.query(getStockSQL(), params, new BeanPropertyRowMapper<>(ProductDto.class));
		resultMap.put("stocks",stocks);
		return resultMap;
	}
	
	@Override
	public Double temp_queryUnitPrice(String pd_code, Integer us_id, Integer companyid){
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT acl_unitprice");
		sql.append(" FROM t_stock_allocatelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_pd_code = :acl_pd_code");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),
					new MapSqlParameterSource().addValue("acl_pd_code", pd_code)
							.addValue("acl_us_id", us_id)
							.addValue("companyid", companyid), Double.class);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void temp_save(List<T_Stock_AllocateList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_stock_allocatelist_temp");
		sql.append(" (acl_pd_code,acl_sub_code,acl_sz_code,acl_szg_code,acl_cr_code,acl_br_code,acl_amount,");
		sql.append(" acl_unitprice,acl_remark,acl_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:acl_pd_code,:acl_sub_code,:acl_sz_code,:acl_szg_code,:acl_cr_code,:acl_br_code,:acl_amount,");
		sql.append(" :acl_unitprice,:acl_remark,:acl_us_id,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_save(T_Stock_AllocateList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_stock_allocatelist_temp");
		sql.append(" (acl_pd_code,acl_sub_code,acl_sz_code,acl_szg_code,acl_cr_code,acl_br_code,acl_amount,");
		sql.append(" acl_unitprice,acl_remark,acl_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:acl_pd_code,:acl_sub_code,:acl_sz_code,:acl_szg_code,:acl_cr_code,:acl_br_code,:acl_amount,");
		sql.append(" :acl_unitprice,:acl_remark,:acl_us_id,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp),holder);
		temp.setAcl_id(holder.getKey().intValue());
	}
	
	@Override
	public void temp_update(List<T_Stock_AllocateList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_allocatelist_temp");
		sql.append(" SET acl_amount = :acl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_sub_code = :acl_sub_code");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_updateById(List<T_Stock_AllocateList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_allocatelist_temp");
		sql.append(" SET acl_amount = :acl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_id = :acl_id");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_update(T_Stock_AllocateList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_allocatelist_temp");
		sql.append(" SET acl_amount = :acl_amount");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_id = :acl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateRemarkById(T_Stock_AllocateList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_allocatelist_temp");
		sql.append(" SET acl_remark = :acl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_id = :acl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateRemarkByPdCode(T_Stock_AllocateList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_allocatelist_temp");
		sql.append(" SET acl_remark = :acl_remark");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_pd_code = :acl_pd_code");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_updateprice(String pd_code, Double unitPrice, Integer us_id, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_stock_allocatelist_temp");
		sql.append(" SET acl_unitprice = :acl_unitprice");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_pd_code = :acl_pd_code");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource()
						.addValue("acl_unitprice", unitPrice)
						.addValue("acl_pd_code", pd_code)
						.addValue("acl_us_id", us_id)
						.addValue("companyid", companyid));
	}
	
	@Override
	public void temp_del(List<T_Stock_AllocateList> temps) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_stock_allocatelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_sub_code = :acl_sub_code");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(temps.toArray()));
	}
	
	@Override
	public void temp_del(Integer acl_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_stock_allocatelist_temp");
		sql.append(" WHERE acl_id=:acl_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("acl_id", acl_id));
	}
	
	@Override
	public void temp_delByPiCode(T_Stock_AllocateList temp) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_stock_allocatelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_pd_code = :acl_pd_code");
		if(StringUtil.isNotEmpty(temp.getAcl_cr_code())){
			sql.append(" AND acl_cr_code = :acl_cr_code");
		}
		if(StringUtil.isNotEmpty(temp.getAcl_br_code())){
			sql.append(" AND acl_br_code = :acl_br_code");
		}
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(temp));
	}
	
	@Override
	public void temp_clear(Integer us_id,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append("DELETE FROM t_stock_allocatelist_temp");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_us_id = :acl_us_id");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(),
				new MapSqlParameterSource().addValue("acl_us_id", us_id).addValue("companyid", companyid));
	}
	
	@Override
	public List<T_Stock_Import> temp_listByImport(List<String> barCodes,Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT DISTINCT bc_pd_code,bc_subcode,bc_barcode,bc_size,pd_szg_code,bc_color,bc_bra,pd_cost_price AS unit_price");
		sql.append(" FROM t_base_barcode bc");
		sql.append(" JOIN t_base_product pd ON pd.pd_code = bc_pd_code AND pd.companyid = bc.companyid");
		sql.append(" WHERE 1=1");
		sql.append(" AND bc_barcode IN(:barcode)");
		sql.append(" AND bc.companyid = :companyid");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("barcode", barCodes).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_Import.class));
	}
	
	@Override
	public void save(T_Stock_Allocate allocate, List<T_Stock_AllocateList> details) {
		String prefix = CommonUtil.NUMBER_PREFIX_STOCK_ALLOCATE + DateUtil.getYearMonthDateYYYYMMDD();
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT CONCAT(:prefix,f_addnumber(MAX(ac_number))) AS new_number");
		sql.append(" FROM t_stock_allocate");
		sql.append(" WHERE 1=1");
		sql.append(" AND INSTR(ac_number,:prefix) > 0");
		sql.append(" AND companyid = :companyid");
		String new_number = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
				new MapSqlParameterSource().addValue("prefix", prefix).addValue("companyid", allocate.getCompanyid()), String.class);
		allocate.setAc_number(new_number);
		sql.setLength(0);
		sql.append(" INSERT INTO t_stock_allocate");
		sql.append(" (ac_number,ac_date,ac_outdp_code,ac_indp_code,ac_manager,ac_amount,ac_money,");
		sql.append(" ac_remark,ac_ar_state,ac_isdraft,ac_sysdate,ac_us_id,companyid)");
		sql.append(" VALUES");
		sql.append(" (:ac_number,:ac_date,:ac_outdp_code,:ac_indp_code,:ac_manager,:ac_amount,:ac_money,");
		sql.append(" :ac_remark,:ac_ar_state,:ac_isdraft,:ac_sysdate,:ac_us_id,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(allocate),holder);
		allocate.setAc_id(holder.getKey().intValue());
		for(T_Stock_AllocateList item:details){
			item.setAcl_number(allocate.getAc_number());
		}
		sql.setLength(0);
		sql.append(" INSERT INTO t_stock_allocatelist");
		sql.append(" (acl_number,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:acl_number,:acl_pd_code,:acl_sub_code,:acl_szg_code,:acl_sz_code,:acl_cr_code,:acl_br_code,:acl_amount,:acl_unitprice,:acl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	
	@Override
	public void update(T_Stock_Allocate allocate, List<T_Stock_AllocateList> details) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_stock_allocate");
		sql.append(" SET ac_date=:ac_date");
		sql.append(" ,ac_outdp_code=:ac_outdp_code");
		sql.append(" ,ac_indp_code=:ac_indp_code");
		sql.append(" ,ac_manager=:ac_manager");
		sql.append(" ,ac_amount=:ac_amount");
		sql.append(" ,ac_money=:ac_money");
		sql.append(" ,ac_remark=:ac_remark");
		sql.append(" ,ac_ar_state=:ac_ar_state");
		sql.append(" ,ac_us_id=:ac_us_id");
		sql.append(" WHERE ac_id=:ac_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(allocate));
		for(T_Stock_AllocateList item:details){
			item.setAcl_number(allocate.getAc_number());
		}
		sql.setLength(0);
		sql.append(" INSERT INTO t_stock_allocatelist");
		sql.append(" (acl_number,acl_pd_code,acl_sub_code,acl_szg_code,acl_sz_code,acl_cr_code,acl_br_code,acl_amount,acl_unitprice,acl_remark,companyid)");
		sql.append(" VALUES");
		sql.append(" (:acl_number,:acl_pd_code,:acl_sub_code,:acl_szg_code,:acl_sz_code,:acl_cr_code,:acl_br_code,:acl_amount,:acl_unitprice,:acl_remark,:companyid)");
		namedParameterJdbcTemplate.batchUpdate(sql.toString(), SqlParameterSourceUtils.createBatch(details.toArray()));
	}
	

	@Override
	public void updateApprove(T_Stock_Allocate allocate) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_stock_allocate");
		sql.append(" SET ac_ar_state=:ac_ar_state");
		sql.append(" ,ac_ar_date = :ac_ar_date");
		sql.append(" WHERE ac_id=:ac_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(allocate));
	}
	
	@Override
	public List<T_Stock_DataBill> listStock(String number,String dp_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT acl_pd_code AS sd_pd_code,acl_sub_code AS sd_code,acl_cr_code AS sd_cr_code,acl_sz_code AS sd_sz_code,");
		sql.append(" acl_br_code AS sd_br_code,SUM(ABS(acl_amount)) AS bill_amount,sd_id,sd_amount");
		sql.append(" FROM t_stock_allocatelist acl");
		sql.append(" LEFT JOIN t_stock_data sd ON acl_sub_code = sd_code AND acl.companyid = sd.companyid AND sd.sd_dp_code = :dp_code");
		sql.append(" WHERE 1=1");
		sql.append(" AND acl_number = :acl_number");
		sql.append(" AND acl.companyid = :companyid");
		sql.append(" GROUP BY acl_sub_code");
		return namedParameterJdbcTemplate.query(sql.toString(),
				new MapSqlParameterSource().addValue("acl_number", number).addValue("dp_code", dp_code).addValue("companyid", companyid),
				new BeanPropertyRowMapper<>(T_Stock_DataBill.class));
	}
	
	@Override
	public void del(String ac_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_stock_allocate");
		sql.append(" WHERE ac_number=:ac_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("ac_number", ac_number).addValue("companyid", companyid));
		sql.setLength(0);
		sql.append(" DELETE FROM t_stock_allocatelist");
		sql.append(" WHERE acl_number=:acl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("acl_number", ac_number).addValue("companyid", companyid));
	}
	
	@Override
	public void deleteList(String ac_number, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_stock_allocatelist");
		sql.append(" WHERE acl_number=:acl_number");
		sql.append(" AND companyid = :companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("acl_number", ac_number).addValue("companyid", companyid));
	}
	
	
}
