package zy.dao.stock.useable;

import java.util.List;
import java.util.Map;

import zy.entity.stock.useable.T_Stock_Useable;
import zy.entity.sys.user.T_Sys_User;

public interface UseableDAO {
	List<T_Stock_Useable> list(T_Sys_User user);
	void save(List<T_Stock_Useable> datas);
	void update(List<T_Stock_Useable> datas);
	Map<String, Object> loadUseableStock(String pd_code,String dp_code,Integer companyid);
}
