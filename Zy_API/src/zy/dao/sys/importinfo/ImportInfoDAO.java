package zy.dao.sys.importinfo;

import java.util.Map;

import zy.entity.sys.improtinfo.T_Import_Info;

public interface ImportInfoDAO {
	T_Import_Info queryImportInfo(Map<String,Object> param);
	void updateImportInfo(T_Import_Info t_Import_ColorInfo);
	void saveImportInfo(T_Import_Info t_Import_ColorInfo);
}
