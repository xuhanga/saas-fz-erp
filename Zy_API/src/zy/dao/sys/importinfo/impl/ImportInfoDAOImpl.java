package zy.dao.sys.importinfo.impl;

import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.importinfo.ImportInfoDAO;
import zy.entity.sys.improtinfo.T_Import_Info;

@Repository
public class ImportInfoDAOImpl extends BaseDaoImpl implements ImportInfoDAO {

	@Override
	public T_Import_Info queryImportInfo(Map<String, Object> param) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ii_id,ii_filename,ii_total,ii_success,ii_fail,ii_error_filename,ii_sysdate ");
		sql.append(" FROM t_import_info t");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND ii_type = :ii_type");
		sql.append(" AND ii_us_id = :us_id");
		sql.append(" AND companyid = :companyid");
		try{
			T_Import_Info data = namedParameterJdbcTemplate.queryForObject(sql.toString(), param,new BeanPropertyRowMapper<>(T_Import_Info.class));
			return data;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public void updateImportInfo(T_Import_Info t_Import_Info) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" update t_import_info");
		sql.append(" set ii_filename=:ii_filename");
		sql.append(" ,ii_total=:ii_total");
		sql.append(" ,ii_success=:ii_success");
		sql.append(" ,ii_fail=:ii_fail");
		sql.append(" ,ii_error_filename=:ii_error_filename");
		sql.append(" ,ii_sysdate=:ii_sysdate");
		sql.append(" where ii_us_id=:ii_us_id");
		sql.append(" AND ii_type=:ii_type");
		sql.append(" AND companyid=:companyid");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(t_Import_Info));
	}

	@Override
	public void saveImportInfo(T_Import_Info t_Import_ColorInfo) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_import_info");
		sql.append(" (ii_filename,ii_total,ii_success,ii_fail,ii_error_filename,ii_us_id,ii_sysdate,ii_type,companyid)");
		sql.append(" VALUES ");
		sql.append(" (:ii_filename,:ii_total,:ii_success,:ii_fail,:ii_error_filename,:ii_us_id,:ii_sysdate,:ii_type,:companyid)");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(t_Import_ColorInfo));
	}

}
