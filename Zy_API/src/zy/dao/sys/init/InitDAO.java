package zy.dao.sys.init;

import java.util.List;

import zy.entity.base.shop.T_Base_Shop;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.sort.allot.T_Sort_Allot;

public interface InitDAO {
	T_Base_Shop checkShop(String shop_code,Integer companyid);
	void updateShopInit(String shop_code,String shop_type, Integer companyid);
	List<T_Batch_Client> listClient4Init(String shop_code,Integer companyid);
	List<T_Buy_Supply> listSupply4Init(Integer companyid);
	List<T_Base_Shop> listShop4Init(String shop_code,String shop_type, Integer companyid);
	List<T_Money_Bank> listBank4Init(String shop_code,Integer companyid);
	void saveQcClient(List<T_Batch_Sell> sells);
	void saveQcSupply(List<T_Buy_Enter> enters);
	void saveQcShop(List<T_Sort_Allot> allots);
}
