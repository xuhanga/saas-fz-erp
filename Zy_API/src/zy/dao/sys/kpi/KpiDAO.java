package zy.dao.sys.kpi;

import java.util.List;
import java.util.Map;

import zy.entity.sys.kpi.T_Sys_Kpi;
import zy.entity.sys.kpi.T_Sys_KpiScore;

public interface KpiDAO {
	List<T_Sys_Kpi> list(Map<String, Object> params);
	List<T_Sys_KpiScore> listScores(Map<String, Object> params);
	T_Sys_Kpi check(String ki_code, String shop_code, Integer companyid);
	void save(T_Sys_Kpi kpi, List<T_Sys_KpiScore> kpiScores);
	void delete(String ki_code, String shop_code, Integer companyid);
	void deleteScores(String ki_code, String shop_code, Integer companyid);
	void saveScores(List<T_Sys_KpiScore> kpiScores);
}
