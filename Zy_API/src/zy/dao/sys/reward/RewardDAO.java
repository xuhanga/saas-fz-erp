package zy.dao.sys.reward;

import java.util.List;
import java.util.Map;

import zy.entity.sys.reward.T_Sys_Reward;

public interface RewardDAO {
	Integer count(Map<String, Object> params);
	List<T_Sys_Reward> list(Map<String, Object> params);
	T_Sys_Reward load(Integer rw_id);
	T_Sys_Reward check(T_Sys_Reward reward);
	void save(T_Sys_Reward reward);
	void update(T_Sys_Reward reward);
	void del(Integer rw_id);
}
