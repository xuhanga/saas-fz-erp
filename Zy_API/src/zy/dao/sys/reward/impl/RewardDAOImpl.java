package zy.dao.sys.reward.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.sys.reward.RewardDAO;
import zy.entity.sys.reward.T_Sys_Reward;
import zy.util.StringUtil;

@Repository
public class RewardDAOImpl extends BaseDaoImpl implements RewardDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		Object searchContent = params.get("searchContent");
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT COUNT(1)");
		sql.append(" FROM t_sys_reward t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND rw_shop_code = :shop_code ");
		if(StringUtil.isNotEmpty(searchContent)){
        	sql.append(" AND (INSTR(rw_code,:searchContent)>0 OR INSTR(rw_name,:searchContent)>0)");
        }
		sql.append(" AND t.companyid=:companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}
	
	@Override
	public List<T_Sys_Reward> list(Map<String, Object> params) {
		Object searchContent = params.get("searchContent");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT rw_id,rw_code,rw_name,rw_score,rw_icon,rw_style,rw_remark,rw_shop_code,companyid");
		sql.append(" FROM t_sys_reward t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND rw_shop_code = :shop_code ");
		if(StringUtil.isNotEmpty(searchContent)){
        	sql.append(" AND (INSTR(rw_code,:searchContent)>0 OR INSTR(rw_name,:searchContent)>0)");
        }
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY rw_id ASC");
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Sys_Reward.class));
	}
	
	@Override
	public T_Sys_Reward load(Integer rw_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT rw_id,rw_code,rw_name,rw_score,rw_icon,rw_style,rw_remark,rw_shop_code,companyid");
		sql.append(" FROM t_sys_reward t");
		sql.append(" WHERE rw_id = :rw_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("rw_id", rw_id),
					new BeanPropertyRowMapper<>(T_Sys_Reward.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public T_Sys_Reward check(T_Sys_Reward reward) {
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT rw_id,rw_code,companyid,rw_name");
		sql.append(" FROM t_sys_reward t");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND rw_shop_code = :rw_shop_code ");//上级店铺编号
		sql.append(" AND rw_name = :rw_name");
		sql.append(" AND t.companyid = :companyid");
		if(reward.getRw_id() != null && reward.getRw_id()>0 ){
			sql.append(" AND rw_id <> :rw_id");
		}
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(),new BeanPropertySqlParameterSource(reward),
					new BeanPropertyRowMapper<>(T_Sys_Reward.class));
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public void save(T_Sys_Reward reward) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_three_code(max(rw_code+0)) FROM t_sys_reward ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(reward), String.class);
		reward.setRw_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO t_sys_reward");
		sql.append(" (rw_code,rw_name,rw_score,rw_icon,rw_style,rw_remark,rw_shop_code,companyid)");
		sql.append(" VALUES");
		sql.append(" (:rw_code,:rw_name,:rw_score,:rw_icon,:rw_style,:rw_remark,:rw_shop_code,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(reward),holder);
		reward.setRw_id(holder.getKey().intValue());
	}
	
	@Override
	public void update(T_Sys_Reward reward) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_sys_reward");
		sql.append(" SET rw_name=:rw_name");
		sql.append(" ,rw_score=:rw_score");
		sql.append(" ,rw_icon=:rw_icon");
		sql.append(" ,rw_style=:rw_style");
		sql.append(" ,rw_remark=:rw_remark");
		sql.append(" WHERE rw_id=:rw_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(reward));
	}
	
	@Override
	public void del(Integer rw_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_sys_reward");
		sql.append(" WHERE rw_id=:rw_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("rw_id", rw_id));
	}
	
}
