package zy.dao.sys.role;

import java.util.List;
import java.util.Map;

import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.role.T_Sys_Role;
import zy.entity.sys.role.T_Sys_RoleMenu;

public interface RoleDAO {
	List<T_Sys_Role> list(Map<String,Object> param);
	List<T_Sys_Role> dia_list(Map<String,Object> param);
	Integer queryByName(T_Sys_Role model);
	T_Sys_Role queryByID(Integer id);
	void update(T_Sys_Role model);
	void save(T_Sys_Role model);
	void del(Integer id);
	
	List<T_Sys_Menu> listMenu(Map<String,Object> param);
	void updateMenu(T_Sys_Menu menu);
	void updateMenus(List<T_Sys_Menu> menuList);
	List<T_Sys_Menu> listAllMenu(String mn_version,Integer shop_type);
	List<T_Sys_RoleMenu> listRoleMenu(String ro_code,Integer companyid);
	void saveRoleMenu(List<T_Sys_RoleMenu> roleMenus);
	void delRoleMenu(List<Integer> rm_ids);
}
