package zy.dao.sys.sqb;

import zy.entity.sys.sqb.T_Sys_Sqb;

public interface SqbDAO {
	T_Sys_Sqb load(Integer sqb_id);
	T_Sys_Sqb check(String device_id);
	void save(T_Sys_Sqb sqb);
	void update_activate(T_Sys_Sqb sqb);
	void update_checkin(T_Sys_Sqb sqb);
}
