package zy.dao.sys.user;

import java.util.List;
import java.util.Map;

import zy.entity.sys.user.T_Sys_User;

public interface UserDAO {
	Integer count(Map<String,Object> param);
	List<T_Sys_User> list(Map<String,Object> param);
	Integer queryByName(T_Sys_User model);
	T_Sys_User queryByID(Integer id);
	void update(T_Sys_User model);
	void updateState(T_Sys_User model);
	void save(T_Sys_User model);
	void del(Integer id);
	void initPwd(Integer id);
	String getPwd(Integer id);
	void changePassword(Integer id, String us_pwd);
}
