package zy.dao.sys.workbench;

import java.util.List;
import java.util.Map;

import zy.entity.common.todo.Common_ToDo;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.workbench.T_Sys_WorkBench;

public interface WorkBenchDAO {
	List<T_Sys_Menu> listUpMenu(Map<String, Object> params);
	List<T_Sys_Menu> listSubMenu(Map<String, Object> params);
	List<Common_ToDo> listToDo(Map<String, Object> params);
	Integer count(Map<String,Object> params);
	List<T_Sys_WorkBench> list(Map<String,Object> params);
	List<T_Sys_WorkBench> listByUser(Map<String, Object> params);
	List<T_Sys_WorkBench> listToDoByUser(Map<String, Object> params);
	Integer check(List<Integer> mnIds,Integer wb_type, Integer us_id, Integer companyid);
	void save(List<T_Sys_WorkBench> workBenchs);
	void delete(Integer wb_id);
	void delete(List<Integer> ids);
	Map<String, Object> statToDoWarn(Map<String, Object> params);
}
