package zy.dao.vip.clear;

import java.util.List;
import java.util.Map;

import zy.entity.vip.member.T_Vip_Member;

public interface VipPointsClearDAO {
	Integer count(Map<String,Object> params);
	List<T_Vip_Member> list(Map<String,Object> params);
	Map<String, Object> sum(Map<String, Object> params);
	List<T_Vip_Member> getMemberByIds(Map<String, Object> params);
	void clear_points(Map<String,Object> params);
}
