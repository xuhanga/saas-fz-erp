package zy.dao.vip.membertype.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.vip.membertype.MemberTypeDAO;
import zy.entity.vip.membertype.T_Vip_MemberType;

@Repository
public class MemberTypeDAOImpl extends BaseDaoImpl implements MemberTypeDAO{

	@Override
	public List<T_Vip_MemberType> list(Map<String, Object> param) {
		Object searchContent = param.get("searchContent");
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT mt_id,mt_code,mt_name,mt_discount,mt_isauto_up,mt_min_point,mt_money,mt_point,mt_point_to_money,mt_mark,companyid");
		sql.append(" FROM t_vip_membertype t");
		sql.append(" WHERE 1 = 1");
        if(null != searchContent && !"".equals(searchContent)){
        	sql.append(" AND (INSTR(t.mt_code,:searchContent)>0 OR INSTR(t.mt_name,:searchContent)>0)");
        }
        sql.append(" AND mt_shop_upcode=:shop_code");
		sql.append(" AND t.companyid=:companyid");
		sql.append(" ORDER BY mt_id DESC");
		return namedParameterJdbcTemplate.query(sql.toString(), param, new BeanPropertyRowMapper<>(T_Vip_MemberType.class));
	}

	@Override
	public Integer queryByName(T_Vip_MemberType memberType) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT mt_id FROM t_vip_membertype");
		sql.append(" WHERE 1=1");
		if(null != memberType.getMt_name() && !"".equals(memberType.getMt_name())){
			sql.append(" AND mt_name=:mt_name");
		}
		if(null != memberType.getMt_id() && memberType.getMt_id() > 0){
			sql.append(" AND mt_id <> :mt_id");
		}
		sql.append(" AND companyid=:companyid");
		sql.append(" LIMIT 1");
		try{
			Integer id = namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new BeanPropertySqlParameterSource(memberType),Integer.class);
			return id;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public T_Vip_MemberType queryByID(Integer mt_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT mt_id,mt_code,mt_name,mt_discount,mt_isauto_up,mt_min_point,mt_money,mt_point,mt_point_to_money,mt_mark,companyid ");
		sql.append(" FROM t_vip_membertype");
		sql.append(" WHERE mt_id=:mt_id");
		sql.append(" LIMIT 1");
		try{
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("mt_id", mt_id),new BeanPropertyRowMapper<>(T_Vip_MemberType.class));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public void save(T_Vip_MemberType memberType) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_three_code(max(mt_code+0)) from t_vip_membertype ");
		sql.append(" WHERE 1=1");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(memberType), String.class);
		memberType.setMt_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO t_vip_membertype");
		sql.append(" (mt_code,mt_name,mt_discount,mt_isauto_up,mt_min_point,mt_money,mt_point,mt_point_to_money,mt_shop_upcode,mt_mark,companyid)");
		sql.append(" VALUES(:mt_code,:mt_name,:mt_discount,:mt_isauto_up,:mt_min_point,:mt_money,:mt_point,:mt_point_to_money,:mt_shop_upcode,:mt_mark,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(memberType),holder);
		int id = holder.getKey().intValue();
		memberType.setMt_id(id);
	}

	@Override
	public void update(T_Vip_MemberType memberType) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_vip_membertype");
		sql.append(" SET mt_name=:mt_name");
		sql.append(" ,mt_discount=:mt_discount");
		sql.append(" ,mt_isauto_up=:mt_isauto_up");
		sql.append(" ,mt_min_point=:mt_min_point");
		sql.append(" ,mt_money=:mt_money");
		sql.append(" ,mt_point=:mt_point");
		sql.append(" ,mt_point_to_money=:mt_point_to_money");
		sql.append(" ,mt_mark=:mt_mark");
		sql.append(" WHERE mt_id=:mt_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(memberType));
	}

	@Override
	public void del(Integer mt_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_vip_membertype");
		sql.append(" WHERE mt_id=:mt_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("mt_id", mt_id));
	}

}
