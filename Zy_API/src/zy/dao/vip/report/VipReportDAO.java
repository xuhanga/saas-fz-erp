package zy.dao.vip.report;

import java.util.List;
import java.util.Map;

import zy.entity.vip.member.T_Vip_ActivevipAnalysis;
import zy.entity.vip.member.T_Vip_BrandAnalysis;
import zy.entity.vip.member.T_Vip_ConsumeDetailList;
import zy.entity.vip.member.T_Vip_ConsumeMonthCompare;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_NoShopAnalysis;
import zy.entity.vip.member.T_Vip_QuotaAnalysis;
import zy.entity.vip.member.T_Vip_TypeAnalysis;
import zy.entity.vip.set.T_Vip_AgeGroupSetUp;

public interface VipReportDAO {
	Integer point_count(Map<String,Object> params);
	List<T_Vip_Member> point_list(Map<String,Object> params);
	List<T_Vip_TypeAnalysis> type_analysis_list(Map<String,Object> params);
	Integer consume_detail_count(Map<String,Object> params);
	List<T_Vip_ConsumeDetailList> consume_detail_list(Map<String,Object> params);
	Map<String, Object> consume_detail_sum(Map<String, Object> params);
	Integer activevip_analysis_count(Map<String,Object> params);
	List<T_Vip_ActivevipAnalysis> activevip_analysis_list(Map<String,Object> params);
	Map<String, Object> activevip_analysis_sum(Map<String, Object> params);
	Integer noshopvip_analysis_count(Map<String,Object> params);
	List<T_Vip_NoShopAnalysis> noshopvip_analysis_list(Map<String,Object> params);
	Map<String, Object> noshopvip_analysis_sum(Map<String, Object> params);
	Map<String, Object> member_lose_data(Map<String, Object> params);
	Integer vipquota_analysis_count(Map<String,Object> params);
	List<T_Vip_QuotaAnalysis> vipquota_analysis_list(Map<String,Object> params);
	List<T_Vip_BrandAnalysis> brand_analysis_list(Map<String,Object> params);
	List<T_Vip_ConsumeMonthCompare> vipconsumeMonthCompareList(Map<String,Object> params);
	List<T_Vip_AgeGroupSetUp> vipage_analysis_column(Map<String,Object> params);
	List<T_Vip_AgeGroupSetUp> vipage_analysis_sell_column(Map<String,Object> params);
	Integer vipage_analysis_count(Map<String,Object> params);
	List<T_Vip_Member> vipage_analysis_list(Map<String,Object> params);
	Map<String, Object> vipage_analysis_sum(Map<String, Object> params);
}
