package zy.dao.vip.set;

import java.util.List;
import java.util.Map;

import zy.entity.vip.set.T_Vip_AgeGroupSetUp;
import zy.entity.vip.set.T_Vip_BirthdaySms;
import zy.entity.vip.set.T_Vip_Grade;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.entity.vip.set.T_Vip_Setup;

public interface VipSetDAO {
	T_Vip_BirthdaySms loadBirthdaySms(Map<String, Object> params);
	List<T_Vip_ReturnSetUp> loadReturnSetUp(Map<String, Object> params);
	List<T_Vip_AgeGroupSetUp> loadAgeGroupSetUp(Map<String, Object> params);
	T_Vip_AgeGroupSetUp loadAgeGroupSetUp(Integer ags_id);
	List<T_Vip_ReturnSetUp> loadReturnSetUp(String shop_code,Integer companyid);
	List<T_Vip_Grade> loadGrade(Map<String, Object> params);
	T_Vip_Setup loadSetUp(Map<String, Object> params);
	void updateBirthdaySms(T_Vip_BirthdaySms birthdaySms);
	void updateReturnSet(List<T_Vip_ReturnSetUp> returnSetUps);
	void updateGrade(List<T_Vip_Grade> grades);
	void updateSetup(Map<String, Object> params);
	void delReturnSet(Integer rts_id);
	void delAgeGroupSet(Integer ags_id);
	void saveReturnSet(Map<String, Object> params);
	void saveAgeGroupSet(Map<String, Object> params);
	void updateAgeGroupSet(Map<String, Object> params);
}
