package zy.dao.vip.trylist;

import java.util.List;
import java.util.Map;

import zy.entity.vip.trylist.T_Vip_TryList;

public interface TryListDAO {
	List<T_Vip_TryList> list(Map<String, Object> params);
	void save(T_Vip_TryList tryList);
}
