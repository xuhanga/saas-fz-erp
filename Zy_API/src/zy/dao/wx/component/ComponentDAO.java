package zy.dao.wx.component;

import zy.entity.wx.component.T_Wx_Component;

public interface ComponentDAO {
	T_Wx_Component load(String wc_appid);
	void update(T_Wx_Component component);
	void updateAccessToken(T_Wx_Component component);
	void updatePreAuth(T_Wx_Component component);
}
