package zy.dao.wx.component.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.component.ComponentDAO;
import zy.entity.wx.component.T_Wx_Component;

@Repository
public class ComponentDAOImpl extends BaseDaoImpl implements ComponentDAO{

	@Override
	public T_Wx_Component load(String wc_appid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT wc_id,wc_appid,wc_verify_ticket,wc_createtime,wc_access_token,wc_expire_time,wc_pre_auth_code,wc_expire_time_pre_auth");
		sql.append(" FROM t_wx_component t");
		sql.append(" WHERE wc_appid = :wc_appid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("wc_appid", wc_appid),
					new BeanPropertyRowMapper<>(T_Wx_Component.class));
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public void update(T_Wx_Component component) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_wx_component");
		sql.append(" SET wc_createtime=:wc_createtime");
		sql.append(" ,wc_verify_ticket=:wc_verify_ticket");
		sql.append(" WHERE wc_id=:wc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(component));
	}
	
	@Override
	public void updateAccessToken(T_Wx_Component component) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_wx_component");
		sql.append(" SET wc_access_token=:wc_access_token");
		sql.append(" ,wc_expire_time=:wc_expire_time");
		sql.append(" WHERE wc_id=:wc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(component));
	}
	
	@Override
	public void updatePreAuth(T_Wx_Component component) {
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE t_wx_component");
		sql.append(" SET wc_pre_auth_code=:wc_pre_auth_code");
		sql.append(" ,wc_expire_time_pre_auth=:wc_expire_time_pre_auth");
		sql.append(" WHERE wc_id=:wc_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(component));
	}
}
