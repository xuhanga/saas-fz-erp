package zy.dao.wx.product;

import java.util.List;
import java.util.Map;

import zy.entity.wx.my.T_Wx_Comment;

public interface WxCommentDAO {
	Integer count(Map<String,Object> params);
	List<T_Wx_Comment> list(Map<String,Object> params);
	T_Wx_Comment saveComment(T_Wx_Comment t_Wx_Comment);
}
