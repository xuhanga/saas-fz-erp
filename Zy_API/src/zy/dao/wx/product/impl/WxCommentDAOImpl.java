package zy.dao.wx.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.product.WxCommentDAO;
import zy.entity.wx.my.T_Wx_Comment;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class WxCommentDAOImpl extends BaseDaoImpl implements WxCommentDAO{

	@Override
	public Integer count(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT COUNT(1)");
		sql.append(" FROM T_Wx_Comment");
		sql.append(" WHERE 1=1");
		sql.append(" AND type = 'product'");
		sql.append(" AND pd_code = :pd_code");
		sql.append(" AND shop_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		return namedParameterJdbcTemplate.queryForObject(sql.toString(), params, Integer.class);
	}

	@Override
	public List<T_Wx_Comment> list(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT * ");
		sql.append(" FROM T_Wx_Comment");
		sql.append(" WHERE 1=1");
		sql.append(" AND type = 'product'");
		sql.append(" AND pd_code = :pd_code");
		sql.append(" AND shop_code = :shop_code");
		sql.append(" AND companyid = :companyid");
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_Comment.class));
	}

	@Override
	public T_Wx_Comment saveComment(T_Wx_Comment t_Wx_Comment) {
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO t_wx_comment");
		sql.append(" (content,createdate,star,pic1,pic2,pic3,pic4,nikname,avatarurl,wu_code,pd_code,shop_code,companyid,type)");
		sql.append(" VALUES ");
		sql.append("(:content,:createdate,:star,:pic1,:pic2,:pic3,:pic4,:nikname,:avatarurl,:wu_code,:pd_code,:shop_code,:companyid,:type)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(t_Wx_Comment),holder);
		t_Wx_Comment.setId(holder.getKey().intValue());
		return t_Wx_Comment;
	}
	
}
