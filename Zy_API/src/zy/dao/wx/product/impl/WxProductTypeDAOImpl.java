package zy.dao.wx.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.product.WxProductTypeDAO;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductType;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Repository
public class WxProductTypeDAOImpl extends BaseDaoImpl implements WxProductTypeDAO{

	@Override
	public List<T_Wx_ProductType> list(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pt_id,pt_code,pt_upcode,pt_name,pt_weight,pt_state,pt_img_path,pt_shop_code,companyid");
		sql.append(" FROM t_wx_producttype");
		sql.append(" WHERE 1=1");
		if(StringUtil.isNotEmpty(params.get("searchContent"))){
			sql.append(" AND (INSTR(pt_code,:searchContent) > 0 OR INSTR(pt_name,:searchContent) > 0)");
		}
		if(StringUtil.isNotEmpty(params.get("pt_upcode"))){
			sql.append(" AND pt_upcode = :pt_upcode ");
		}
		sql.append(" AND pt_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY pt_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_ProductType.class));
	}

	@Override
	public T_Wx_ProductType check(T_Wx_ProductType productType) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pt_id,pt_code,pt_upcode,pt_name,pt_weight,pt_state,pt_img_path,pt_shop_code,companyid");
		sql.append(" FROM t_wx_producttype");
		sql.append(" WHERE 1=1");
		if(null != productType.getPt_name() && !"".equals(productType.getPt_name())){
			sql.append(" AND pt_name=:pt_name");
		}
		if(null != productType.getPt_id() && productType.getPt_id() > 0){
			sql.append(" AND pt_id <> :pt_id");
		}
		sql.append(" AND companyid=:companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(productType),
					new BeanPropertyRowMapper<>(T_Wx_ProductType.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public T_Wx_ProductType load(Integer pt_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pt_id,pt_code,pt_upcode,pt_name,pt_weight,pt_state,pt_img_path,pt_shop_code,companyid");
		sql.append(" FROM t_wx_producttype");
		sql.append(" WHERE pt_id = :pt_id");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), new MapSqlParameterSource().addValue("pt_id", pt_id),
					new BeanPropertyRowMapper<>(T_Wx_ProductType.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void save(T_Wx_ProductType productType) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT f_three_code(max(pt_code+0)) FROM t_wx_producttype ");
		sql.append(" WHERE 1=1");
		sql.append(" AND pt_shop_code=:pt_shop_code");
		sql.append(" AND companyid=:companyid");
		String code = namedParameterJdbcTemplate.queryForObject(sql.toString(), new BeanPropertySqlParameterSource(productType), String.class);
		productType.setPt_code(code);
		sql.setLength(0);
		sql.append("INSERT INTO t_wx_producttype");
		sql.append(" (pt_code,pt_upcode,pt_name,pt_weight,pt_state,pt_img_path,pt_shop_code,companyid)");
		sql.append(" VALUES(:pt_code,:pt_upcode,:pt_name,:pt_weight,:pt_state,:pt_img_path,:pt_shop_code,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(productType),holder);
		productType.setPt_id(holder.getKey().intValue());
	}

	@Override
	public void update(T_Wx_ProductType productType) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_wx_producttype");
		sql.append(" SET pt_name=:pt_name");
		sql.append(" ,pt_weight=:pt_weight");
		sql.append(" ,pt_state=:pt_state");
		if(StringUtil.isNotEmpty(productType.getPt_img_path())){
			sql.append(" ,pt_img_path=:pt_img_path");
		}
		sql.append(" WHERE pt_id=:pt_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(productType));
	}

	@Override
	public void del(Integer pt_id) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" DELETE FROM t_wx_producttype");
		sql.append(" WHERE pt_id=:pt_id");
		namedParameterJdbcTemplate.update(sql.toString(), new MapSqlParameterSource().addValue("pt_id", pt_id));
	}
	
	@Override
	public List<T_Wx_ProductType> getProductType(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT pt_id,pt_code,pt_upcode,pt_name,pt_weight,pt_state,pt_img_path,pt_shop_code,companyid");
		sql.append(" FROM t_wx_producttype");
		sql.append(" WHERE 1=1");
		sql.append(" AND pt_shop_code=:shop_code");
		sql.append(" AND companyid=:companyid");
		sql.append(" AND pt_code not in (select pt_upcode from t_wx_producttype where companyid=:companyid and pt_shop_code=:shop_code)");
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY pt_id DESC");
		}
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_ProductType.class));
	}
	
	@Override
	public List<T_Wx_Product> getProductByType(Map<String, Object> params) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ");
		sql.append(" 	p.wp_id,p.wp_number ,p.wp_shop_code,p.wp_pd_code,p.wp_pd_name,p.wp_sell_price,p.wp_rate_price,p.wp_type,p.wp_state,p.wp_pt_code,p.wp_istry,p.wp_try_way,p.wp_isbuy,p.wp_buy_way,p.wp_postfree,p.wp_subtitle,p.wp_weight,p.wp_sell_amount,p.wp_virtual_amount,p.wp_buy_limit,p.wp_browse_count,p.wp_likecount,p.wp_sysdate,p.companyid, ");
		sql.append(" 	pdm_img_path ");
		sql.append(" FROM ");
		sql.append(" 	t_wx_product p ");
		sql.append(" 	INNER JOIN t_base_product_img pdm on pdm_pd_code = p.wp_pd_code AND pdm.companyid = p.companyid ");
		sql.append(" INNER JOIN t_wx_producttype pt ON pt.pt_code = p.wp_pt_code ");
		sql.append(" AND pt.companyid = p.companyid ");
		sql.append(" AND pt.pt_shop_code = p.wp_shop_code ");
		sql.append(" where p.wp_pt_code='004' ");//:pt_code
		sql.append(" AND p.wp_shop_code=:shop_code");
		sql.append(" AND p.companyid=:companyid");
		if(StringUtil.isNotEmpty(params.get(CommonUtil.SIDX))){
			sql.append(" ORDER BY ").append(params.get(CommonUtil.SIDX)).append(" ").append(params.get(CommonUtil.SORD));
		}else {
			sql.append(" ORDER BY wp_id DESC");
		}
		sql.append(" LIMIT :start,:end");
		return namedParameterJdbcTemplate.query(sql.toString(), params, new BeanPropertyRowMapper<>(T_Wx_Product.class));
	}

}
