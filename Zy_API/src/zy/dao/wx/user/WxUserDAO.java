package zy.dao.wx.user;

import java.util.List;
import java.util.Map;

import zy.entity.wx.my.T_Wx_Address;
import zy.entity.wx.user.T_Wx_User;
import zy.entity.wx.user.T_Wx_User_Account_Detail;

public interface WxUserDAO {
	T_Wx_User load(Integer wu_id);
	T_Wx_User loadByMobile(String wu_mobile);
	T_Wx_User login(String wu_mobile,String wu_password,String nickname,String avatarurl);
	T_Wx_User login(Map<String, Object> params);
	T_Wx_User update(Map<String, Object> params);
	
	T_Wx_User save(T_Wx_User wxUser);
	T_Wx_User updatePassword(String old_password, String new_password,String confirm_password);
	
	T_Wx_Address getAddress(String wu_code);
	List<T_Wx_User_Account_Detail> getAccoutDetails(Map<String, Object> paramMap);
	
	T_Wx_User saveUserInfo(Map<String, Object> params);
	T_Wx_User initUser(Map<String, Object> params);
	
}
