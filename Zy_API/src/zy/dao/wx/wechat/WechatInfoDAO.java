package zy.dao.wx.wechat;

import zy.entity.wx.wechat.T_Wx_WechatInfo;

public interface WechatInfoDAO {
	T_Wx_WechatInfo load(String shop_code,Integer companyid);
	void save(T_Wx_WechatInfo wechatInfo);
	void update(T_Wx_WechatInfo wechatInfo);
}
