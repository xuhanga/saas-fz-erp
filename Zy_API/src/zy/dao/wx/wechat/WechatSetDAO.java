package zy.dao.wx.wechat;

import zy.entity.wx.wechat.T_Wx_WechatSet;

public interface WechatSetDAO {
	T_Wx_WechatSet load(String shop_code, Integer companyid);
	void save(T_Wx_WechatSet wechatSet);
	void update(T_Wx_WechatSet wechatSet);
}
