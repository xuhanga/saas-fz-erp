package zy.dao.wx.wechat.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.wx.wechat.WechatSetDAO;
import zy.entity.wx.wechat.T_Wx_WechatSet;
import zy.util.StringUtil;

@Repository
public class WechatSetDAOImpl extends BaseDaoImpl implements WechatSetDAO{
	@Override
	public T_Wx_WechatSet load(String shop_code, Integer companyid) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" SELECT ws_id,ws_title,ws_bgimg_path,ws_useable_balance,ws_sysdate,ws_shop_code,companyid");
		sql.append(" FROM t_wx_wechatset");
		sql.append(" WHERE ws_shop_code = :ws_shop_code");
		sql.append(" AND companyid = :companyid");
		sql.append(" LIMIT 1");
		try {
			return namedParameterJdbcTemplate.queryForObject(sql.toString(), 
					new MapSqlParameterSource().addValue("ws_shop_code", shop_code).addValue("companyid", companyid),
					new BeanPropertyRowMapper<>(T_Wx_WechatSet.class));
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void save(T_Wx_WechatSet wechatSet) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" INSERT INTO t_wx_wechatset");
		sql.append(" (ws_title,ws_bgimg_path,ws_useable_balance,ws_sysdate,ws_shop_code,companyid)");
		sql.append(" VALUES(:ws_title,:ws_bgimg_path,:ws_useable_balance,:ws_sysdate,:ws_shop_code,:companyid)");
		KeyHolder holder = new GeneratedKeyHolder(); 
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(wechatSet),holder);
		wechatSet.setWs_id(holder.getKey().intValue());
	}

	@Override
	public void update(T_Wx_WechatSet wechatSet) {
		StringBuffer sql = new StringBuffer("");
		sql.append(" UPDATE t_wx_wechatset");
		sql.append(" SET ws_title = :ws_title");
		if(StringUtil.isNotEmpty(wechatSet.getWs_bgimg_path())){
			sql.append(" ,ws_bgimg_path = :ws_bgimg_path");
		}
		sql.append(" WHERE ws_id=:ws_id");
		namedParameterJdbcTemplate.update(sql.toString(), new BeanPropertySqlParameterSource(wechatSet));
	}
}
