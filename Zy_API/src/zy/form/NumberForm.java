package zy.form;

import java.io.Serializable;

public class NumberForm implements Serializable{
	private static final long serialVersionUID = 1L;
	private String code;
	private Double number;
	private Integer count;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getNumber() {
		return number;
	}
	public void setNumber(Double number) {
		this.number = number;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
}
