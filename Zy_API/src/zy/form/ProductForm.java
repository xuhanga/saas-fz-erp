package zy.form;

public class ProductForm extends PageForm{
	private String bd_code;
	private String tp_code;
	private String pd_season;
	private String pd_year;
	private String pd_code;
	private String cr_code;
	private String sz_code;
	private String br_code;
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public String getPd_year() {
		return pd_year;
	}
	public void setPd_year(String pd_year) {
		this.pd_year = pd_year;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getSz_code() {
		return sz_code;
	}
	public void setSz_code(String sz_code) {
		this.sz_code = sz_code;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
}
