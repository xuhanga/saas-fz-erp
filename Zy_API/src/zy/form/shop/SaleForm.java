package zy.form.shop;

import java.io.Serializable;

public class SaleForm implements Serializable{
	private static final long serialVersionUID = 1L;
	private String ss_code;
	private String ss_mt_code;
	private Integer ss_point;
	private Integer ss_priority;
	private String ss_model;
	private Integer ssm_model;
	private Integer ssm_rule;
	private Integer ssm_scope;
	private Integer ssa_index;
	private Integer ssb_index;
	private Integer sst_index;
	private Integer ssp_index;
	private Double ssa_discount;
	private Double ssa_buy_full;
	private Double ssa_buy_number;
	private Integer ssa_donation_number;
	private Double ssa_donation_amount;
	private Double ssa_reduce_amount;
	private Double ssa_increased_amount;
	private String ssb_bd_code;
	private Double ssb_discount;
	private Double ssb_buy_full;
	private Double ssb_buy_number;
	private Double ssb_donation_amount;
	private Double ssb_reduce_amount;
	private Double ssb_increased_amount;
	private String sst_tp_code;
	private Double sst_discount;
	private Double sst_buy_full;
	private Double sst_buy_number;
	private Double sst_donation_amount;
	private Double sst_reduce_amount;
	private Double sst_increased_amount;
	private String ssp_pd_code;
	private Double ssp_discount;
	private Double ssp_buy_full;
	private Double ssp_buy_number;
	private Double ssp_donation_amount;
	private Double ssp_reduce_amount;
	private Double ssp_increased_amount;
	private Double ssp_special_offer;
	public String getSs_code() {
		return ss_code;
	}
	public void setSs_code(String ss_code) {
		this.ss_code = ss_code;
	}
	public String getSs_mt_code() {
		return ss_mt_code;
	}
	public void setSs_mt_code(String ss_mt_code) {
		this.ss_mt_code = ss_mt_code;
	}
	public Integer getSs_point() {
		return ss_point;
	}
	public void setSs_point(Integer ss_point) {
		this.ss_point = ss_point;
	}
	public Integer getSs_priority() {
		return ss_priority;
	}
	public void setSs_priority(Integer ss_priority) {
		this.ss_priority = ss_priority;
	}
	public String getSs_model() {
		return ss_model;
	}
	public void setSs_model(String ss_model) {
		this.ss_model = ss_model;
	}
	public Integer getSsm_model() {
		return ssm_model;
	}
	public void setSsm_model(Integer ssm_model) {
		this.ssm_model = ssm_model;
	}
	public Integer getSsm_rule() {
		return ssm_rule;
	}
	public void setSsm_rule(Integer ssm_rule) {
		this.ssm_rule = ssm_rule;
	}
	public Integer getSsm_scope() {
		return ssm_scope;
	}
	public void setSsm_scope(Integer ssm_scope) {
		this.ssm_scope = ssm_scope;
	}
	public Double getSsa_discount() {
		return ssa_discount;
	}
	public void setSsa_discount(Double ssa_discount) {
		this.ssa_discount = ssa_discount;
	}
	public Double getSsa_buy_full() {
		return ssa_buy_full;
	}
	public void setSsa_buy_full(Double ssa_buy_full) {
		this.ssa_buy_full = ssa_buy_full;
	}
	public Double getSsa_buy_number() {
		return ssa_buy_number;
	}
	public void setSsa_buy_number(Double ssa_buy_number) {
		this.ssa_buy_number = ssa_buy_number;
	}
	public Double getSsa_donation_amount() {
		return ssa_donation_amount;
	}
	public void setSsa_donation_amount(Double ssa_donation_amount) {
		this.ssa_donation_amount = ssa_donation_amount;
	}
	public Double getSsa_reduce_amount() {
		return ssa_reduce_amount;
	}
	public void setSsa_reduce_amount(Double ssa_reduce_amount) {
		this.ssa_reduce_amount = ssa_reduce_amount;
	}
	public Double getSsa_increased_amount() {
		return ssa_increased_amount;
	}
	public void setSsa_increased_amount(Double ssa_increased_amount) {
		this.ssa_increased_amount = ssa_increased_amount;
	}
	public String getSsb_bd_code() {
		return ssb_bd_code;
	}
	public void setSsb_bd_code(String ssb_bd_code) {
		this.ssb_bd_code = ssb_bd_code;
	}
	public Double getSsb_discount() {
		return ssb_discount;
	}
	public void setSsb_discount(Double ssb_discount) {
		this.ssb_discount = ssb_discount;
	}
	public Double getSsb_buy_full() {
		return ssb_buy_full;
	}
	public void setSsb_buy_full(Double ssb_buy_full) {
		this.ssb_buy_full = ssb_buy_full;
	}
	public Double getSsb_buy_number() {
		return ssb_buy_number;
	}
	public void setSsb_buy_number(Double ssb_buy_number) {
		this.ssb_buy_number = ssb_buy_number;
	}
	public Double getSsb_donation_amount() {
		return ssb_donation_amount;
	}
	public void setSsb_donation_amount(Double ssb_donation_amount) {
		this.ssb_donation_amount = ssb_donation_amount;
	}
	public Double getSsb_reduce_amount() {
		return ssb_reduce_amount;
	}
	public void setSsb_reduce_amount(Double ssb_reduce_amount) {
		this.ssb_reduce_amount = ssb_reduce_amount;
	}
	public Double getSsb_increased_amount() {
		return ssb_increased_amount;
	}
	public void setSsb_increased_amount(Double ssb_increased_amount) {
		this.ssb_increased_amount = ssb_increased_amount;
	}
	public String getSst_tp_code() {
		return sst_tp_code;
	}
	public void setSst_tp_code(String sst_tp_code) {
		this.sst_tp_code = sst_tp_code;
	}
	public Double getSst_discount() {
		return sst_discount;
	}
	public void setSst_discount(Double sst_discount) {
		this.sst_discount = sst_discount;
	}
	public Double getSst_buy_full() {
		return sst_buy_full;
	}
	public void setSst_buy_full(Double sst_buy_full) {
		this.sst_buy_full = sst_buy_full;
	}
	public Double getSst_buy_number() {
		return sst_buy_number;
	}
	public void setSst_buy_number(Double sst_buy_number) {
		this.sst_buy_number = sst_buy_number;
	}
	public Double getSst_donation_amount() {
		return sst_donation_amount;
	}
	public void setSst_donation_amount(Double sst_donation_amount) {
		this.sst_donation_amount = sst_donation_amount;
	}
	public Double getSst_reduce_amount() {
		return sst_reduce_amount;
	}
	public void setSst_reduce_amount(Double sst_reduce_amount) {
		this.sst_reduce_amount = sst_reduce_amount;
	}
	public Double getSst_increased_amount() {
		return sst_increased_amount;
	}
	public void setSst_increased_amount(Double sst_increased_amount) {
		this.sst_increased_amount = sst_increased_amount;
	}
	public String getSsp_pd_code() {
		return ssp_pd_code;
	}
	public void setSsp_pd_code(String ssp_pd_code) {
		this.ssp_pd_code = ssp_pd_code;
	}
	public Double getSsp_discount() {
		return ssp_discount;
	}
	public void setSsp_discount(Double ssp_discount) {
		this.ssp_discount = ssp_discount;
	}
	public Double getSsp_buy_full() {
		return ssp_buy_full;
	}
	public void setSsp_buy_full(Double ssp_buy_full) {
		this.ssp_buy_full = ssp_buy_full;
	}
	public Double getSsp_buy_number() {
		return ssp_buy_number;
	}
	public void setSsp_buy_number(Double ssp_buy_number) {
		this.ssp_buy_number = ssp_buy_number;
	}
	public Double getSsp_donation_amount() {
		return ssp_donation_amount;
	}
	public void setSsp_donation_amount(Double ssp_donation_amount) {
		this.ssp_donation_amount = ssp_donation_amount;
	}
	public Double getSsp_reduce_amount() {
		return ssp_reduce_amount;
	}
	public void setSsp_reduce_amount(Double ssp_reduce_amount) {
		this.ssp_reduce_amount = ssp_reduce_amount;
	}
	public Double getSsp_increased_amount() {
		return ssp_increased_amount;
	}
	public void setSsp_increased_amount(Double ssp_increased_amount) {
		this.ssp_increased_amount = ssp_increased_amount;
	}
	public Integer getSsa_index() {
		return ssa_index;
	}
	public void setSsa_index(Integer ssa_index) {
		this.ssa_index = ssa_index;
	}
	public Integer getSsb_index() {
		return ssb_index;
	}
	public void setSsb_index(Integer ssb_index) {
		this.ssb_index = ssb_index;
	}
	public Integer getSst_index() {
		return sst_index;
	}
	public void setSst_index(Integer sst_index) {
		this.sst_index = sst_index;
	}
	public Integer getSsp_index() {
		return ssp_index;
	}
	public void setSsp_index(Integer ssp_index) {
		this.ssp_index = ssp_index;
	}
	public Double getSsp_special_offer() {
		return ssp_special_offer;
	}
	public void setSsp_special_offer(Double ssp_special_offer) {
		this.ssp_special_offer = ssp_special_offer;
	}
	public Integer getSsa_donation_number() {
		return ssa_donation_number;
	}
	public void setSsa_donation_number(Integer ssa_donation_number) {
		this.ssa_donation_number = ssa_donation_number;
	}
	
}
