package zy.service.approve;

import java.util.List;

import zy.entity.approve.T_Approve_Record;

public interface ApproveRecordService {
	List<T_Approve_Record> list(String number,String ar_type,Integer companyid);
}
