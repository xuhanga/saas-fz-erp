package zy.service.base.affiliate.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.affiliate.AffiliateDAO;
import zy.entity.base.affiliate.T_Base_Affiliate;
import zy.service.base.affiliate.AffiliateService;
import zy.util.CommonUtil;

@Service
public class AffiliateServiceImpl implements AffiliateService{
	
	@Resource
	private AffiliateDAO affiliateDAO;

	@Override
	public List<T_Base_Affiliate> list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return affiliateDAO.list(param);
	}

	@Override
	public Integer queryByName(T_Base_Affiliate affiliate) {
		return affiliateDAO.queryByName(affiliate);
	}

	@Override
	public T_Base_Affiliate queryByID(Integer af_id) {
		return affiliateDAO.queryByID(af_id);
	}

	@Override
	public void save(T_Base_Affiliate affiliate) {
		affiliateDAO.save(affiliate);
	}

	@Override
	public void update(T_Base_Affiliate affiliate) {
		affiliateDAO.update(affiliate);
	}

	@Override
	public void del(Integer af_id) {
		affiliateDAO.del(af_id);
	}
}
