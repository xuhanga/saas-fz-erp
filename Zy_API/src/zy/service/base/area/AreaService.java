package zy.service.base.area;

import java.util.List;
import java.util.Map;

import zy.entity.base.area.T_Base_Area;

public interface AreaService {
	List<T_Base_Area> list(Map<String,Object> param);
	Integer queryByName(T_Base_Area area);
	T_Base_Area queryByID(Integer ar_id);
	void save(T_Base_Area area);
	void update(T_Base_Area area);
	void del(Integer ar_id);
}
