package zy.service.base.bra;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.bra.T_Base_Bra;

public interface BraService {
	PageData<T_Base_Bra> page(Map<String,Object> param);
	Integer queryByName(T_Base_Bra model);
	T_Base_Bra queryByID(Integer id);
	void save(T_Base_Bra model);
	void update(T_Base_Bra model);
	void del(Integer id);
	List<T_Base_Bra> braList(Map<String,Object> param);
}
