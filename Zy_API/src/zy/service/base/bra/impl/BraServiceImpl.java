package zy.service.base.bra.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.bra.BraDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.bra.T_Base_Bra;
import zy.service.base.bra.BraService;
import zy.util.CommonUtil;
@Service
public class BraServiceImpl implements BraService{
	@Resource
	private BraDAO braDAO;

	@Override
	public PageData<T_Base_Bra> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = braDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Bra> list = braDAO.list(param);
		PageData<T_Base_Bra> pageData = new PageData<T_Base_Bra>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Transactional
	@Override
	public void save(T_Base_Bra model) {
		braDAO.save(model);
	}
	@Override
	public void update(T_Base_Bra model) {
		braDAO.update(model);
	}
	@Override
	public Integer queryByName(T_Base_Bra model) {
		return braDAO.queryByName(model);
	}
	
	@Override
	public T_Base_Bra queryByID(Integer id) {
		return braDAO.queryByID(id);
	}
	@Override
	public void del(Integer id) {
		braDAO.del(id);
	}
	@Override
	public List<T_Base_Bra> braList(Map<String, Object> param) {
		return braDAO.braList(param);
	}
}
