package zy.service.base.color;

import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import zy.entity.PageData;
import zy.entity.base.color.T_Base_Color;

public interface ColorService {
	PageData<T_Base_Color> page(Map<String,Object> param);
	Integer queryByName(T_Base_Color color);
	T_Base_Color queryByID(Integer cr_id);
	void save(T_Base_Color color);
	void save(Map<String,Object> param);
	void update(T_Base_Color color);
	void del(Integer cr_id);
	List<T_Base_Color> colorList(Map<String,Object> param);
	Map<String, Object> parseSavingCardInfoExcel(Workbook soureWorkBook, WritableWorkbook errsheet, int companyid) throws Exception;
	void save_to_product(T_Base_Color color,String pd_code);
}
