package zy.service.base.department.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.department.DepartmentDAO;
import zy.entity.base.department.T_Base_Department;
import zy.service.base.department.DepartmentService;
@Service
public class DepartmentServiceImpl implements DepartmentService{
	@Resource
	private DepartmentDAO departmentDAO;
	@Override
	public List<T_Base_Department> list(Map<String, Object> param) {
		return departmentDAO.list(param);
	}

	@Override
	public Integer queryByName(T_Base_Department model) {
		return departmentDAO.queryByName(model);
	}

	@Override
	public T_Base_Department queryByID(Integer id) {
		return departmentDAO.queryByID(id);
	}

	@Override
	public void save(T_Base_Department model) {
		departmentDAO.save(model);
	}

	@Override
	public void update(T_Base_Department model) {
		departmentDAO.update(model);
	}

	@Override
	public void del(Integer id) {
		departmentDAO.del(id);
	}

}
