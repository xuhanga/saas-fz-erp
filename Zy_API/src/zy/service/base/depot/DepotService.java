package zy.service.base.depot;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.depot.T_Base_Depot;

public interface DepotService {
	PageData<T_Base_Depot> page(Map<String,Object> param);
	List<T_Base_Depot> list4buy(Map<String,Object> param);
	List<T_Base_Depot> list4batch(Map<String, Object> param);
	List<T_Base_Depot> list4stock(Map<String, Object> param);
	List<T_Base_Depot> list4want(Map<String, Object> param);
	List<T_Base_Depot> list4allot(Map<String, Object> param);
	List<T_Base_Depot> list4sellallocate(Map<String, Object> param);
	List<T_Base_Depot> listByShop(String shop_code,Integer companyid);
	T_Base_Depot queryByID(Integer id);
	void update(T_Base_Depot model);
	void save(T_Base_Depot model);
	void updateDefault(T_Base_Depot model);
	void del(T_Base_Depot model);
	
	List<T_Base_Depot> ownDepot(Map<String, Object> param);
}
