package zy.service.base.depot.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.depot.DepotDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.depot.T_Base_Depot;
import zy.service.base.depot.DepotService;
import zy.util.CommonUtil;

@Service
public class DepotServiceImpl implements DepotService{
	@Resource
	private DepotDAO depotDAO;

	@Override
	public PageData<T_Base_Depot> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = depotDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Depot> list = depotDAO.list(param);
		PageData<T_Base_Depot> pageData = new PageData<T_Base_Depot>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public List<T_Base_Depot> list4buy(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return depotDAO.list4buy(param);
	}
	
	@Override
	public List<T_Base_Depot> list4batch(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return depotDAO.list4batch(param);
	}
	
	@Override
	public List<T_Base_Depot> list4stock(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return depotDAO.list4stock(param);
	}
	
	@Override
	public List<T_Base_Depot> list4want(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return depotDAO.list4want(param);
	}
	
	@Override
	public List<T_Base_Depot> list4allot(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return depotDAO.list4allot(param);
	}
	
	@Override
	public List<T_Base_Depot> list4sellallocate(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return depotDAO.list4sellallocate(param);
	}
	
	@Override
	public List<T_Base_Depot> listByShop(String shop_code,Integer companyid) {
		return depotDAO.listByShop(shop_code,companyid);
	}

	@Override
	public List<T_Base_Depot> ownDepot(Map<String, Object> param) {
		return depotDAO.ownDepot(param);
	}

	@Override
	public T_Base_Depot queryByID(Integer id) {
		return depotDAO.queryByID(id);
	}

	@Override
	public void update(T_Base_Depot model) {
		depotDAO.update(model);
	}

	@Override
	public void updateDefault(T_Base_Depot model) {
		depotDAO.updateDefault(model);
	}

	@Override
	public void save(T_Base_Depot model) {
		try {
			depotDAO.save(model);
		} catch (Exception e) {
			throw new RuntimeException("保存失败!");
		}
	}

	@Override
	public void del(T_Base_Depot model) {
		Integer id =  depotDAO.useByCode(model); 
		if(null != id && id > 0){
			throw new RuntimeException("业务关联，无法删除!");
		}
		depotDAO.del(model);
	}

}
