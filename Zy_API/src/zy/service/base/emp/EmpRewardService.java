package zy.service.base.emp;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.emp.T_Base_Emp_Reward;

public interface EmpRewardService {
	List<T_Base_Emp_Reward> statByEmp(Map<String, Object> params);
	PageData<T_Base_Emp_Reward> page(Map<String, Object> params);
}
