package zy.service.base.emp;

import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.entity.PageData;
import zy.entity.base.emp.T_Base_Emp;
import zy.form.StringForm;

public interface EmpService {
	PageData<T_Base_Emp> page(Map<String,Object> param);
	void save(T_Base_Emp emp);
	T_Base_Emp queryByID(Integer em_id);
	void update(T_Base_Emp emp);
	void del(Integer em_id);
	
	List<T_Base_Emp> listCombo(Map<String,Object> param);
	//-------------------前台---------------------------------
	PageData<T_Base_Emp> pageShop(Map<String,Object> param);
	List<StringForm> listShop(Map<String,Object> param);
	//-------------------接口---------------------------------
	EmpLoginDto login(String em_code, String em_pass, String co_code);
	void updatePwd(Integer em_id,String new_pass,String old_pass);
}
