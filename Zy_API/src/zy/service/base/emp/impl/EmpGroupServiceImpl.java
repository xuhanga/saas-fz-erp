package zy.service.base.emp.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.emp.EmpGroupDAO;
import zy.entity.base.emp.T_Base_Emp;
import zy.entity.base.emp.T_Base_EmpGroup;
import zy.entity.base.emp.T_Base_EmpGroupList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.emp.EmpGroupService;
import zy.util.StringUtil;

@Service
public class EmpGroupServiceImpl implements EmpGroupService{
	@Resource
	private EmpGroupDAO empGroupDAO;
	
	@Override
	public List<T_Base_EmpGroup> list(Map<String, Object> params) {
		return empGroupDAO.list(params);
	}
	
	@Override
	public T_Base_EmpGroup load(Integer eg_id) {
		return empGroupDAO.load(eg_id);
	}
	
	@Override
	public List<T_Base_EmpGroupList> listDetails(Map<String, Object> params) {
		return empGroupDAO.listDetails(params);
	}
	
	@Override
	public List<T_Base_Emp> listEmps(Map<String, Object> params) {
		return empGroupDAO.listEmps(params);
	}
	
	@Override
	@Transactional
	public void save(T_Base_EmpGroup empGroup,List<T_Base_EmpGroupList> groupLists, T_Sys_User user) {
		if(StringUtil.isEmpty(empGroup.getEg_name())){
			throw new IllegalArgumentException("员工组名称不能为空");
		}
		if (groupLists == null || groupLists.size() == 0) {
			throw new IllegalArgumentException("请选择员工");
		}
		empGroup.setEg_shop_code(user.getUs_shop_code());
		empGroup.setCompanyid(user.getCompanyid());
		empGroupDAO.save(empGroup, groupLists);
	}
	
	@Override
	@Transactional
	public void update(T_Base_EmpGroup empGroup,List<T_Base_EmpGroupList> groupLists, T_Sys_User user) {
		if(StringUtil.isEmpty(empGroup.getEg_id())){
			throw new IllegalArgumentException("eg_id不能为空");
		}
		if(StringUtil.isEmpty(empGroup.getEg_code())){
			throw new IllegalArgumentException("eg_code不能为空");
		}
		if(StringUtil.isEmpty(empGroup.getEg_name())){
			throw new IllegalArgumentException("员工组名称不能为空");
		}
		if (groupLists == null || groupLists.size() == 0) {
			throw new IllegalArgumentException("请选择员工");
		}
		empGroup.setEg_shop_code(user.getUs_shop_code());
		empGroup.setCompanyid(user.getCompanyid());
		empGroupDAO.update(empGroup, groupLists);
	}
	
	@Override
	@Transactional
	public void del(Integer eg_id) {
		T_Base_EmpGroup empGroup = empGroupDAO.load(eg_id);
		if(empGroup == null){
			throw new IllegalArgumentException("员工组信息不存在");
		}
		empGroupDAO.del(eg_id, empGroup.getEg_code(), empGroup.getCompanyid());
	}
	
}
