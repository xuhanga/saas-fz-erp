package zy.service.base.emp.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.emp.EmpDAO;
import zy.dto.base.emp.EmpLoginDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.emp.T_Base_Emp;
import zy.form.StringForm;
import zy.service.base.emp.EmpService;
import zy.util.CommonUtil;
import zy.util.MD5;
@Service
public class EmpServiceImpl implements EmpService {
	@Resource
	private EmpDAO empDAO;
	
	@Override
	public PageData<T_Base_Emp> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = empDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Emp> list = empDAO.list(param);
		PageData<T_Base_Emp> pageData = new PageData<T_Base_Emp>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Base_Emp> pageShop(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = empDAO.countShop(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Emp> list = empDAO.listShop(param);
		PageData<T_Base_Emp> pageData = new PageData<T_Base_Emp>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<StringForm> listShop(Map<String, Object> param) {
		return empDAO._listShop(param);
	}

	@Override
	public void save(T_Base_Emp emp) {
		empDAO.save(emp);
	}

	@Override
	public T_Base_Emp queryByID(Integer em_id) {
		return empDAO.queryByID(em_id);
	}

	@Override
	public void update(T_Base_Emp emp) {
		empDAO.update(emp);
	}

	@Override
	public void del(Integer em_id) {
		empDAO.del(em_id);
	}

	@Override
	public List<T_Base_Emp> listCombo(Map<String, Object> param) {
		Object shop_code = param.get(CommonUtil.SHOP_CODE);
		if(null == shop_code || "".equals(shop_code)){
			throw new IllegalArgumentException("店铺未选择!");
		}
		return empDAO.listCombo(param);
	}

	@Override
	public EmpLoginDto login(String em_code, String em_pass, String co_code) {
		return empDAO.login(em_code, em_pass, co_code);
	}
	
	@Override
	public void updatePwd(Integer em_id,String new_pass,String old_pass) {
		T_Base_Emp emp = empDAO.queryByID(em_id);
		if(emp == null){
			throw new RuntimeException("用户不存在!");
		}
		if(!MD5.encryptMd5(old_pass).equals(emp.getEm_pass())){
			throw new RuntimeException("原密码输入错误!");
		}
		emp.setEm_pass(MD5.encryptMd5(new_pass));
		empDAO.updatePwd(emp);
	}
	

}
