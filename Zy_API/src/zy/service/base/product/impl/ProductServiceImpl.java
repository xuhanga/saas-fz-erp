package zy.service.base.product.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.product.ProductDAO;
import zy.dao.base.size.SizeDAO;
import zy.dao.sys.importinfo.ImportInfoDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.bra.T_Base_Bra;
import zy.entity.base.brand.T_Base_Brand;
import zy.entity.base.color.T_Base_Color;
import zy.entity.base.dict.T_Base_DictList;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Barcode_Set;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.product.T_Base_Product_Assist;
import zy.entity.base.product.T_Base_Product_Br;
import zy.entity.base.product.T_Base_Product_Color;
import zy.entity.base.product.T_Base_Product_Img;
import zy.entity.base.product.T_Base_Product_Shop_Price;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;
import zy.entity.base.type.T_Base_Type;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.sys.improtinfo.T_Import_Info;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.product.ProductService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.ExcelImportUtil;
import zy.util.StringUtil;
@Service
public class ProductServiceImpl implements ProductService {

	@Resource
	private ProductDAO productDAO;
	@Resource
	private SizeDAO sizeDAO;
	@Resource
	private ImportInfoDAO importInfoDAO;
	
	@Override
	public PageData<T_Base_Product> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = productDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Product> list = productDAO.list(param);
		PageData<T_Base_Product> pageData = new PageData<T_Base_Product>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Base_Barcode> barcodelist(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (pd_code == null) {
			throw new IllegalArgumentException("参数pd_code不能为null");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = productDAO.barcodeCount(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Barcode> list = productDAO.barcodeList(param);
		PageData<T_Base_Barcode> pageData = new PageData<T_Base_Barcode>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Base_Product_Shop_Price> shop_price_list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (pd_code == null) {
			throw new IllegalArgumentException("参数pd_code不能为null");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = productDAO.shop_price_listCount(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Product_Shop_Price> list = productDAO.shop_price_list(param);
		PageData<T_Base_Product_Shop_Price> pageData = new PageData<T_Base_Product_Shop_Price>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public PageData<T_Base_Barcode> barcodepage(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = productDAO.barcodePageCount(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Barcode> list = productDAO.barcodePageList(param);
		PageData<T_Base_Barcode> pageData = new PageData<T_Base_Barcode>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<T_Base_Barcode_Set> barcodeplan(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return productDAO.barcodeplan(param);
	}
	
	@Override
	public T_Base_Barcode_Set getPrintSetByBsid(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Integer bs_id = (Integer)param.get("bs_id");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		T_Base_Barcode_Set barcode_Set = null;
		if(bs_id != null && bs_id > 0){
			barcode_Set = productDAO.getPrintSetByBsid(param);
		}
		if(barcode_Set == null){
			barcode_Set = new T_Base_Barcode_Set();
			barcode_Set.setBs_id(0);
			barcode_Set.setBs_line(1);
			barcode_Set.setBs_bartype("128Auto");
			barcode_Set.setBs_hight_spacing((double)2.9);
			barcode_Set.setBs_width_spacing((double)0);
			barcode_Set.setBs_width((double)35);
			barcode_Set.setBs_height((double)15);
			barcode_Set.setBs_cr_ifshow(0);
			barcode_Set.setBs_pdno_ifshow(0);
			barcode_Set.setBs_sz_ifshow(0);
			barcode_Set.setBs_bs_ifshow(0);
			barcode_Set.setBs_bd_ifshow(0);
			barcode_Set.setBs_price_ifshow(0);
			barcode_Set.setBs_vipprice_ifshow(0);
			barcode_Set.setBs_signprice_ifshow(0);
			barcode_Set.setBs_cr_row(0);
			barcode_Set.setBs_pdno_row(0);
			barcode_Set.setBs_sz_row(0);
			barcode_Set.setBs_bs_row(0);
			barcode_Set.setBs_bd_row(0);
			barcode_Set.setBs_price_row(0);
			barcode_Set.setBs_vipprice_row(0);
			barcode_Set.setBs_signprice_row(0);
			
			barcode_Set.setBs_paper_width((double)40);
			barcode_Set.setBs_paper_height((double)30);
			barcode_Set.setBs_top_backgauge((double)1.5);
			barcode_Set.setBs_left_backgauge((double)1.5);
			barcode_Set.setBs_show_fiename("1");
			barcode_Set.setBs_font_name("宋体");
			barcode_Set.setBs_font_size(9);
			barcode_Set.setBs_bold("0");
			barcode_Set.setBs_pdname_ifshow(0);
			barcode_Set.setBs_pdname_row(0);
			barcode_Set.setBs_class_ifshow(0);
			barcode_Set.setBs_class_row(0);
			barcode_Set.setBs_safestandard_ifshow(0);
			barcode_Set.setBs_safestandard_row(0);
			
			barcode_Set.setBs_executionstandard_ifshow(0);
			barcode_Set.setBs_executionstandard_row(0);
			
			barcode_Set.setBs_filling_ifshow(0);
			barcode_Set.setBs_filling_row(0);
			barcode_Set.setBs_p_space((double)0);
			barcode_Set.setBs_ps_space((double)0);
			barcode_Set.setBs_plan_name("");
			barcode_Set.setBs_subcode_showup(0);
			barcode_Set.setBs_price_showdown(0);
			barcode_Set.setBs_language(0);
		}
		return barcode_Set;
	}
	
	@Override
	public T_Base_Barcode_Set getPrintSetByState(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		T_Base_Barcode_Set barcode_Set = productDAO.getPrintSetByState(param);
		if(barcode_Set == null){
			barcode_Set = new T_Base_Barcode_Set();
			barcode_Set.setBs_id(0);
			barcode_Set.setBs_line(1);
			barcode_Set.setBs_bartype("128Auto");
			barcode_Set.setBs_hight_spacing((double)2.9);
			barcode_Set.setBs_width_spacing((double)0);
			barcode_Set.setBs_width((double)35);
			barcode_Set.setBs_height((double)15);
			barcode_Set.setBs_cr_ifshow(0);
			barcode_Set.setBs_pdno_ifshow(0);
			barcode_Set.setBs_sz_ifshow(0);
			barcode_Set.setBs_bs_ifshow(0);
			barcode_Set.setBs_bd_ifshow(0);
			barcode_Set.setBs_price_ifshow(0);
			barcode_Set.setBs_vipprice_ifshow(0);
			barcode_Set.setBs_signprice_ifshow(0);
			barcode_Set.setBs_cr_row(0);
			barcode_Set.setBs_pdno_row(0);
			barcode_Set.setBs_sz_row(0);
			barcode_Set.setBs_bs_row(0);
			barcode_Set.setBs_bd_row(0);
			barcode_Set.setBs_price_row(0);
			barcode_Set.setBs_vipprice_row(0);
			barcode_Set.setBs_signprice_row(0);
			
			barcode_Set.setBs_paper_width((double)40);
			barcode_Set.setBs_paper_height((double)30);
			barcode_Set.setBs_top_backgauge((double)1.5);
			barcode_Set.setBs_left_backgauge((double)1.5);
			barcode_Set.setBs_show_fiename("1");
			barcode_Set.setBs_font_name("宋体");
			barcode_Set.setBs_font_size(9);
			barcode_Set.setBs_bold("0");
			barcode_Set.setBs_pdname_ifshow(0);
			barcode_Set.setBs_pdname_row(0);
			barcode_Set.setBs_class_ifshow(0);
			barcode_Set.setBs_class_row(0);
			barcode_Set.setBs_safestandard_ifshow(0);
			barcode_Set.setBs_safestandard_row(0);
			
			barcode_Set.setBs_executionstandard_ifshow(0);
			barcode_Set.setBs_executionstandard_row(0);
			barcode_Set.setBs_infabric_ifshow(0);
			barcode_Set.setBs_infabric_row(0);
			barcode_Set.setBs_type_ifshow(0);
			barcode_Set.setBs_type_row(0);
			barcode_Set.setBs_place_ifshow(0);
			barcode_Set.setBs_place_row(0);
			barcode_Set.setBs_fabric_ifshow(0);
			barcode_Set.setBs_fabric_row(0);
			barcode_Set.setBs_washexplain_ifshow(0);
			barcode_Set.setBs_washexplain_row(0);
			
			barcode_Set.setBs_filling_ifshow(0);
			barcode_Set.setBs_filling_row(0);
			barcode_Set.setBs_p_space((double)0);
			barcode_Set.setBs_ps_space((double)0);
			barcode_Set.setBs_plan_name("");
			barcode_Set.setBs_subcode_showup(0);
			barcode_Set.setBs_price_showdown(0);
			barcode_Set.setBs_language(0);
		}
		return barcode_Set;
	}

	@Override
	@Transactional
	public void enable_plan(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object bs_id = param.get("bs_id");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(bs_id == null || "".equals(bs_id)){
			throw new IllegalArgumentException("条码设置id不能为空！");
		}
		productDAO.enable_plan(param);
	}

	@Override
	@Transactional
	public void del_bar_plan(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object bs_id = param.get("bs_id");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(bs_id == null || "".equals(bs_id)){
			throw new IllegalArgumentException("条码设置id不能为空！");
		}
		productDAO.del_bar_plan(param);
	}

	@Override
	@Transactional
	public void save_barcode_printset(T_Base_Barcode_Set barcode_Set) {
		if(barcode_Set == null || barcode_Set == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		barcode_Set.setBs_sysdate(DateUtil.getCurrentTime());
		if(barcode_Set.getBs_id() != null && barcode_Set.getBs_id()>0){
			productDAO.update_barcode_printset(barcode_Set);
		}else {
			productDAO.save_barcode_printset(barcode_Set);
		}
	}

	@Override
	public List<T_Base_Product> list(Map<String, Object> param) {
		return productDAO.list(param);
	}

	@Override
	public List<T_Base_Product> productReport(Map<String, Object> param) {
		return productDAO.list(param);
	}

	@Override
	public Integer pdNoIfExit(T_Base_Product product) {
		if(product.getPd_no() == null || "".equals(product.getPd_no())){
			throw new IllegalArgumentException("商品货号不能为空");
		}
		Integer id = productDAO.queryByPdNo(product);
		return id;
	}

	@Override
	@Transactional
	public void save(T_Base_Product product) {
		Integer id = productDAO.queryByPdNo(product);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("商品货号["+product.getPd_no()+"]已经存在");
		}
		if(product == null || product == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(product.getPd_no() == null || "".equals(product.getPd_no())){
			throw new IllegalArgumentException("商品货号不能为空");
		}
		if(product.getPd_name() == null || "".equals(product.getPd_name())){
			throw new IllegalArgumentException("商品名称不能为空");
		}
		if(product.getPd_bd_code() == null || "".equals(product.getPd_bd_code())){
			throw new IllegalArgumentException("商品品牌不能为空");
		}
		if(product.getPd_tp_code() == null || "".equals(product.getPd_tp_code())){
			throw new IllegalArgumentException("商品分类不能为空");
		}
		if(product.getPd_style() == null || "".equals(product.getPd_style())){
			throw new IllegalArgumentException("商品款式不能为空");
		}
		if(product.getPd_bra() != null && product.getPd_bra() == 1){
			if(product.getPdb_br_codes() == null || "".equals(product.getPdb_br_codes())){
				throw new IllegalArgumentException("杯型不能为空");
			}
		}
		if(product.getPd_szg_code() == null || "".equals(product.getPd_szg_code())){
			throw new IllegalArgumentException("尺码组不能为空");
		}
		if(product.getPdc_cr_codes() == null || "".equals(product.getPdc_cr_codes())){
			throw new IllegalArgumentException("颜色不能为空");
		}
		product.setPd_spell(StringUtil.getSpell(product.getPd_name()));//简码
		product.setPd_sysdate(DateUtil.getCurrentTime());
		product.setPd_add_date(DateUtil.getYearMonthDate());
		productDAO.save(product);
		
	}

	@Override
	@Transactional
	public void save(Map<String, Object> param) {//商品资料导入保存
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		List<T_Base_Product> productList = (List<T_Base_Product>)param.get("productList");//导入成功商品资料
		Map<String, List<T_Base_Product_Color>> colorMap = (Map<String, List<T_Base_Product_Color>>)param.get("colorMap");//导入成功商品资料对应的颜色
		Map<String, List<T_Base_Product_Br>> brMap = (Map<String, List<T_Base_Product_Br>>)param.get("brMap");//导入成功商品资料对应的杯型
		
		List<T_Base_Product_Color> allProductColorList = new ArrayList<T_Base_Product_Color>();//存放所有需要保存的商品资料对应的颜色信息
		List<T_Base_Product_Br> allProductBrList = new ArrayList<T_Base_Product_Br>();//存放所有需要保存的商品资料对应的杯型信息
		
		T_Import_Info t_Import_Info = (T_Import_Info)param.get("t_Import_Info");
		if(productList != null && productList.size()>0 && colorMap!=null){
			//获取商品资料最大code
			String max_pd_code = productDAO.queryMaxPdcode(companyid);
			Integer int_pd_code = Integer.parseInt(max_pd_code);
			for(int i=0;i<productList.size();i++){
				String pd_no = productList.get(i).getPd_no();
				String pd_code = "";
				pd_code = StringUtil.getTwoCode(int_pd_code + i);//获取code
				//把code放到集合中
				productList.get(i).setPd_code(pd_code);
				List<T_Base_Product_Color> colorList = (List<T_Base_Product_Color>)colorMap.get(pd_no);//商品资料对应的颜色
				if(colorList!=null && colorList.size()>0){
					for(int j=0;j<colorList.size();j++){
						colorList.get(j).setPdc_pd_code(pd_code);
					}
					allProductColorList.addAll(colorList);
				}
				List<T_Base_Product_Br> brList = (List<T_Base_Product_Br>)brMap.get(pd_no);//商品资料对应的杯型
				if(brList!=null && brList.size()>0){
					for(int j=0;j<brList.size();j++){
						brList.get(j).setPdb_pd_code(pd_code);
					}
					allProductBrList.addAll(brList);
				}
			}
			
			productDAO.saveProductInfo(productList);
			productDAO.saveProductColor(allProductColorList);
			productDAO.saveProductBr(allProductBrList);
		}
		// 查询当前用户是否导入过数据
		T_Import_Info info = importInfoDAO.queryImportInfo(param);
		if(info != null){
			String temp_uploaddir = CommonUtil.CHECK_BASE + CommonUtil.EXCEL_PATH;
			String last_error_filename = temp_uploaddir + File.separator + info.getIi_error_filename();
			File last_error_file = new File(last_error_filename);
			if (last_error_file.exists()) {
				last_error_file.delete();
			}
			ExcelImportUtil.deleteFile(last_error_file);
			importInfoDAO.updateImportInfo(t_Import_Info);
		}else {
			importInfoDAO.saveImportInfo(t_Import_Info);
		}
		
	}

	@Override
	@Transactional
	public void update(T_Base_Product product) {
		Integer id = productDAO.queryByPdNo(product);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("商品编号["+product.getPd_no()+"]已经存在");
		}
		if(product == null || product == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(product.getPd_no() == null || "".equals(product.getPd_no())){
			throw new IllegalArgumentException("商品货号不能为空");
		}
		if(product.getPd_name() == null || "".equals(product.getPd_name())){
			throw new IllegalArgumentException("商品名称不能为空");
		}
		if(product.getPd_bd_code() == null || "".equals(product.getPd_bd_code())){
			throw new IllegalArgumentException("商品品牌不能为空");
		}
		if(product.getPd_tp_code() == null || "".equals(product.getPd_tp_code())){
			throw new IllegalArgumentException("商品分类不能为空");
		}
		if(product.getPd_style() == null || "".equals(product.getPd_style())){
			throw new IllegalArgumentException("商品款式不能为空");
		}
		if(product.getPd_bra() != null && product.getPd_bra() == 1){
			if(product.getPdb_br_codes() == null || "".equals(product.getPdb_br_codes())){
				throw new IllegalArgumentException("杯型不能为空");
			}
		}
		if(product.getPd_szg_code() == null || "".equals(product.getPd_szg_code())){
			throw new IllegalArgumentException("尺码组不能为空");
		}
		if(product.getPdc_cr_codes() == null || "".equals(product.getPdc_cr_codes())){
			throw new IllegalArgumentException("颜色不能为空");
		}
		product.setPd_spell(StringUtil.getSpell(product.getPd_name()));//简码
		product.setPd_mod_date(DateUtil.getYearMonthDate());
		productDAO.update(product);
	}

	@Override
	@Transactional
	public void del(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pd_id = param.get("pd_id");
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(pd_id == null || "".equals(pd_id)){
			throw new IllegalArgumentException("商品id不能为空！");
		}
		if(pd_code == null || "".equals(pd_code)){
			throw new IllegalArgumentException("商品编号不能为空！");
		}
		productDAO.del(param);
	}

	@Override
	public Boolean queryColorIfEmploy(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object codes = param.get("codes");
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(codes == null || "".equals(codes)){
			throw new IllegalArgumentException("颜色编号不能为空！");
		}
		if(pd_code == null || "".equals(pd_code)){
			throw new IllegalArgumentException("商品编号不能为空！");
		}
		return productDAO.queryColorIfEmploy(param);
	}

	@Override
	public Boolean querySizeIfEmploy(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object codes = param.get("codes");
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(codes == null || "".equals(codes)){
			throw new IllegalArgumentException("尺码组编号不能为空！");
		}
		if(pd_code == null || "".equals(pd_code)){
			throw new IllegalArgumentException("商品编号不能为空！");
		}
		
		List<T_Base_SizeList> sizeList = sizeDAO.listByID(param);
		param.put("sizeList", sizeList);
		return productDAO.querySizeIfEmploy(param);
	}
	
	@Override
	public Boolean queryBraIfEmploy(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object codes = param.get("codes");
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(codes == null || "".equals(codes)){
			throw new IllegalArgumentException("杯型编号不能为空！");
		}
		if(pd_code == null || "".equals(pd_code)){
			throw new IllegalArgumentException("商品编号不能为空！");
		}
		return productDAO.queryBraIfEmploy(param);
	}
	
	@Override
	public Boolean queryProductIfEmploy(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pd_code = param.get("pd_code");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(pd_code == null || "".equals(pd_code)){
			throw new IllegalArgumentException("商品编号不能为空！");
		}
		return productDAO.queryProductIfEmploy(param);
	}

	@Override
	public T_Base_Product queryByID(Integer pd_id) {
		if(pd_id == null || "".equals(pd_id)){
			throw new IllegalArgumentException("参数pd_id不能为空 !");
		}
		return productDAO.queryByID(pd_id);
	}

	@Override
	public List<T_Base_Color> colorByProduct(Map<String, Object> param) {
		return productDAO.colorByProduct(param);
	}

	@Override
	public List<T_Base_Bra> braByProduct(Map<String, Object> param) {
		return productDAO.braByProduct(param);
	}

	@Override
	public List<String> queryBarCodeIfExit(Map<String, Object> param) {
		return productDAO.queryBarCodeIfExit(param);
	}

	@Override
	public List<T_Base_Product_Img> productimglist(Map<String, Object> param) {
		return productDAO.productimglist(param);
	}

	@Override
	public void saveOneBarcode(Map<String, Object> param) {
		productDAO.saveOneBarcode(param);
	}

	@Override
	@Transactional
	public void saveAllBarcode(Map<String, Object> param) {
		productDAO.saveAllBarcode(param);
	}

	@Override
	public void updateOneBarcode(Map<String, Object> param) {
		productDAO.updateOneBarcode(param);
	}
	
	@Override
	@Transactional
	public void updateAllBarcode(Map<String, Object> param) {
		productDAO.updateAllBarcode(param);
	}

	@Override
	public void deleteBarcode(Map<String, Object> param) {
		productDAO.deleteBarcode(param);
	}

	@Override
	@Transactional
	public void delBulkBarcode(Map<String, Object> param) {
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		if (StringUtil.isEmpty(companyid)) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Base_Barcode> t_Base_Barcodes = new ArrayList<T_Base_Barcode>();
		String bc_id = (String)param.get("bc_ids");
		String []bc_ids = bc_id.split(",");
		if(bc_id == null || bc_ids.length == 0){
			throw new IllegalArgumentException("条码id不能为空！");
		}
		T_Base_Barcode t_Base_Barcode = null;
		for(int i=0;i<bc_ids.length;i++){
			t_Base_Barcode = new T_Base_Barcode();
			t_Base_Barcode.setBc_id(Integer.parseInt(bc_ids[i]));
			t_Base_Barcode.setCompanyid(companyid);
			t_Base_Barcodes.add(t_Base_Barcode);
		}
		productDAO.delBulkBarcode(t_Base_Barcodes);
	}

	@Override
	public void delAllBarcode(Map<String, Object> param) {
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		if (StringUtil.isEmpty(companyid)) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		productDAO.delAllBarcode(companyid);
	}

	@Override
	public void saveimg(Map<String, Object> param) {
		productDAO.saveimg(param);
	}

	@Override
	public void deleteimg(Map<String, Object> param) {
		productDAO.deleteimg(param);
	}

	@Override
	public List<T_Base_Product> list_server(Map<String, Object> params) {
		if (StringUtil.isEmpty(params.get(CommonUtil.COMPANYID))) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return productDAO.list_server(params);
	}
	
	@Override
	public T_Base_Product loadByBarcode(String barcode,Integer companyid) {
		return productDAO.loadByBarcode(barcode, companyid);
	}

	@Override
	public Map<String, Object> parseSavingProductExcel(Workbook soureWorkBook,
			WritableWorkbook errsheet, T_Sys_User user) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			Integer companyid= user.getCompanyid();
			Sheet sheet = soureWorkBook.getSheet(0);
			WritableSheet errorSheet = errsheet.getSheet(0);
			// 商品资料导入列数为50，再多的列数不允许导入
			int columns = 50;
			int rows = sheet.getRows(); // 获取导入行数
			System.out.println("商品资料导入数共计" + (rows - 1) + "条!");
			
			if(rows <= 1){
				resultMap.put("fail", 0);// 错误条数
				resultMap.put("success", 0);// 成功条数
				return resultMap;
			}

			//获取导入商品资料需要的数据
			Map<String, List> checkImportProduct = productDAO.getCheckImportProduct(user);
			
			if(checkImportProduct == null){
				resultMap.put("fail", 0);// 错误条数
				resultMap.put("success", 0);// 成功条数
				return resultMap;
			}
			List<T_Base_Brand> brandList = (List<T_Base_Brand>)checkImportProduct.get("brandList");//品牌
			List<T_Buy_Supply> supplieList = (List<T_Buy_Supply>)checkImportProduct.get("supplieList");//供应商
			List<T_Base_Type> typeList = (List<T_Base_Type>)checkImportProduct.get("typeList");//商品分类
			List<T_Base_DictList> seasonList = (List<T_Base_DictList>)checkImportProduct.get("seasonList");//商品季节
			List<T_Base_DictList> unitList  = (List<T_Base_DictList>)checkImportProduct.get("unitList");//商品单位
			List<T_Base_DictList> stuffList = (List<T_Base_DictList>)checkImportProduct.get("stuffList");//面料
			List<T_Base_DictList> styleList = (List<T_Base_DictList>)checkImportProduct.get("styleList");//款式
			List<T_Base_DictList> priceList = (List<T_Base_DictList>)checkImportProduct.get("priceList");//价格特性
			List<T_Base_Size_Group> sizeGroupList = (List<T_Base_Size_Group>)checkImportProduct.get("sizeGroupList");//尺码组
			List<T_Base_Color> colorList = (List<T_Base_Color>)checkImportProduct.get("colorList");//颜色
			List<T_Base_Bra> braList = (List<T_Base_Bra>)checkImportProduct.get("braList");//杯型
			List<String> pdNoList = (List<String>)checkImportProduct.get("pdNoList");//已有商品货号
			
			T_Base_Product product = null;
			String errorCause = "";//错误原因
			int errorRow = 1;
			int successRow = 0;//导入成功行数
			List<T_Base_Product> productList = new ArrayList<T_Base_Product>();//存放验证成功的商品资料
			Map<String, List<T_Base_Product_Color>> colorMap = new HashMap<String, List<T_Base_Product_Color>>();//商品资料对应的颜色
			Map<String, List<T_Base_Product_Br>> brMap = new HashMap<String, List<T_Base_Product_Br>>();//商品资料对应的杯型
			// 第一行为属性，从第二行开始
			for (int i = 1; i < rows; i++) {
				//执行每次100条
				if(i%100 == 0){
					Thread.sleep(1000);
				}
				product = new T_Base_Product();
				
				if (ExcelImportUtil.isBlankRow(sheet, i, columns - 1)) {//判断是否为空行
					continue;
				}
				
				boolean tag = false;
				
				Cell cell0 = sheet.getCell(0, i);// 商品货号
				String pd_no = cell0.getContents().trim();
				if("".equals(pd_no)){//商品货号不能为空
					errorCause = "源文件第"+(i+1)+"行， 商品货号不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(ExcelImportUtil.checkStrRegex("^[\\dA-Za-z-._]+$", pd_no)){//货号只能输入英文、数字、.、_、-
					errorCause = "源文件第"+(i+1)+"行， 商品货号只能输入英文、数字和(. - _)符号";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(pd_no.length()>15){//货号长度不能超过15
					errorCause = "源文件第"+(i+1)+"行， 商品货号长度不能超过15位";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<pdNoList.size();k++){
					if(pd_no.equalsIgnoreCase(pdNoList.get(k))){
						tag = true;
						break;
					}
				}
				if(tag){//商品货号不能和数据库中重复
					errorCause = "源文件第"+(i+1)+"行， 商品货号已存在，请重新输入";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_no(pd_no);
				
				Cell cell1 = sheet.getCell(1, i);// 商品款号
				String pd_number = cell1.getContents().trim();
				if(!"".equals(pd_number)){
					if(pd_number.length()>30){//款号长度不能超过30
						errorCause = "源文件第"+(i+1)+"行， 商品款号长度不能超过30位";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				product.setPd_number(pd_number);
				
				Cell cell2 = sheet.getCell(2, i);// 商品名称
				String pd_name = cell2.getContents().trim();
				if("".equals(pd_name)){//商品名称不能为空
					errorCause = "源文件第"+(i+1)+"行， 商品名称不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(pd_name.length()>30){//名称长度不能超过30
					errorCause = "源文件第"+(i+1)+"行， 商品名称长度不能超过30位";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_spell(StringUtil.Hanzi2SimpleSpelling(pd_name));//拼音简码
				product.setPd_name(pd_name);
				
				Cell cell3 = sheet.getCell(3, i);// 商品品牌
				String pd_bd_name = cell3.getContents().trim();
				String pd_bd_code = "";
				if("".equals(pd_bd_name)){//商品名称不能为空
					errorCause = "源文件第"+(i+1)+"行， 商品品牌不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<brandList.size();k++){
					if(pd_bd_name.equals(brandList.get(k).getBd_name())){
						pd_bd_code = brandList.get(k).getBd_code();
						break;
					}
				}
				if("".equals(pd_bd_code)){//如果数据库中没有品牌信息，则返回错误
					errorCause = "源文件第"+(i+1)+"行， 商品品牌不存在，请确认！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_bd_code(pd_bd_code);
				
				Cell cell4 = sheet.getCell(4, i);//商品分类
				String pd_tp_name = cell4.getContents().trim();
				String pd_tp_code = "";
				String pd_tp_upcode = "";
				if("".equals(pd_tp_name)){
					errorCause = "源文件第"+(i+1)+"行， 商品分类不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<typeList.size();k++){
					if(pd_tp_name.equals(typeList.get(k).getTp_name())){
						pd_tp_code = typeList.get(k).getTp_code();
						pd_tp_upcode = typeList.get(k).getTp_upcode();
						break;
					}
				}
				if("".equals(pd_tp_code)){
					errorCause = "源文件第"+(i+1)+"行， 您输入的商品分类不存在，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_tp_code(pd_tp_code);
				product.setPd_tp_upcode(pd_tp_upcode);
				
				
				Cell cell5 = sheet.getCell(5, i);//商品款式
				String pd_style = cell5.getContents().trim();
				String pd_style_code = "";
				if("".equals(pd_style)){
					errorCause = "源文件第"+(i+1)+"行， 商品款式不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<styleList.size();k++){
					if(pd_style.equals(styleList.get(k).getDtl_name())){
						pd_style_code = styleList.get(k).getDtl_code();
						break;
					}
				}
				if("".equals(pd_style_code)){
					errorCause = "源文件第"+(i+1)+"行， 您输入的商品款式不存在，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_style(pd_style);
				
				Cell cell6 = sheet.getCell(6, i);//商品单位
				String pd_unit = cell6.getContents().trim();
				String pd_unit_code = "";
				if("".equals(pd_unit)){
					errorCause = "源文件第"+(i+1)+"行， 商品单位不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<unitList.size();k++){
					if(pd_unit.equals(unitList.get(k).getDtl_name())){
						pd_unit_code = unitList.get(k).getDtl_code();
						break;
					}
				}
				if("".equals(pd_unit_code)){
					errorCause = "源文件第"+(i+1)+"行， 您输入的商品单位不存在，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_unit(pd_unit);
				
				Cell cell7 = sheet.getCell(7, i);//尺码组
				String pd_szg_name = cell7.getContents().trim();
				String pd_szg_code = "";
				if("".equals(pd_szg_name)){//尺码组不能为空
					errorCause = "源文件第"+(i+1)+"行， 尺码组不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<sizeGroupList.size();k++){
					if(pd_szg_name.equals(sizeGroupList.get(k).getSzg_name())){
						pd_szg_code = sizeGroupList.get(k).getSzg_code();
						break;
					}
				}
				if("".equals(pd_szg_code)){
					errorCause = "源文件第"+(i+1)+"行， 尺码组不存在，请确认！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_szg_code(pd_szg_code);
				
				Cell cell8 = sheet.getCell(8, i);//颜色
				String cr_names = cell8.getContents().trim();
				if("".equals(cr_names)){//颜色不能为空
					errorCause = "源文件第"+(i+1)+"行， 颜色不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				cr_names = cr_names.replace("，", ",");
				String[] crNameList = cr_names.split(",");//逗号分隔颜色名称
				List<T_Base_Product_Color> productColorList = new ArrayList<T_Base_Product_Color>();//匹配成功颜色code
				for(int k=0;k<crNameList.length;k++){
					for(int j=0;j<colorList.size();j++){
						if(crNameList[k].equals(colorList.get(j).getCr_name())){
							T_Base_Product_Color t_Base_Product_Color = new T_Base_Product_Color();
							t_Base_Product_Color.setPdc_cr_code(colorList.get(j).getCr_code());
							t_Base_Product_Color.setCompanyid(companyid);
							productColorList.add(t_Base_Product_Color);
							break;
						}
					}
				}
				if(productColorList.size()<1){//输入的颜色在数据库中不存在
					errorCause = "源文件第"+(i+1)+"行， 您输入的颜色不存在，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				
				Cell cell9 = sheet.getCell(9, i);//杯型
				String br_names = cell9.getContents().trim();
				List<T_Base_Product_Br> productBrList = new ArrayList<T_Base_Product_Br>();//匹配成功杯型code
				if("".equals(br_names)){
					product.setPd_bra(0);//不是内衣
				}else{
					br_names = br_names.replace("，", ",");
					String[] brNameList = br_names.split(",");//逗号分隔尺码名称
					for(int k=0;k<brNameList.length;k++){
						for(int j=0;j<braList.size();j++){
							if(brNameList[k].equals(braList.get(j).getBr_name())){
								T_Base_Product_Br t_Base_Product_Br = new T_Base_Product_Br();
								t_Base_Product_Br.setPdb_br_code(braList.get(j).getBr_code());
								t_Base_Product_Br.setCompanyid(companyid);
								productBrList.add(t_Base_Product_Br);
								break;
							}
						}
					}
					if(productBrList.size() < 1){//输入的尺码在数据库中不存在
						errorCause = "源文件第"+(i+1)+"行， 您输入的杯型不存在，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_bra(1);
				}
				
				Cell cell10 = sheet.getCell(10, i);// 供货单位
				String pd_sp_name = cell10.getContents().trim();
				String pd_sp_code = "";
				if(!"".equals(pd_sp_name)){
					for(int k=0;k<supplieList.size();k++){
						if(pd_sp_name.equals(supplieList.get(k).getSp_name())){
							pd_sp_code = supplieList.get(k).getSp_code();
						}
					}
				}
				product.setPd_sp_code(pd_sp_code);
				
				Cell cell11 = sheet.getCell(11, i);//上市日期
				String pd_date = cell11.getContents().trim();
				if("".equals(pd_date)){
					product.setPd_date(DateUtil.getCurrentTime("yyyy-MM-dd"));
				}else{
					String beListeddate = ExcelImportUtil.checkCellIsDate(cell11);
					if("".equals(beListeddate)){
						errorCause = "源文件第"+(i+1)+"行， 您输入的上市日期格式不正确，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_date(beListeddate);
					}
				}
				
				Cell cell12 = sheet.getCell(12, i);//退换货日期
				String pd_returndate = cell12.getContents().trim();
				if("".equals(pd_returndate)){
					product.setPd_returndate(DateUtil.getCurrentTime("yyyy-MM-dd"));
				}else{
					String returnDate = ExcelImportUtil.checkCellIsDate(cell12);
					if("".equals(returnDate)){
						errorCause = "源文件第"+(i+1)+"行， 您输入的退换货日期格式不正确，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_date(returnDate);
					}
				}
				
				Cell cell13 = sheet.getCell(13, i);//进货周期
				String pd_buy_cycle = cell13.getContents().trim();
				if(!"".equals(pd_buy_cycle)){
					if(ExcelImportUtil.checkStrRegex("^[1-9]\\d*$", pd_buy_cycle)){//只能输入正整数
						errorCause = "源文件第"+(i+1)+"行， 进货周期只能输入正整数！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_buy_cycle(Integer.parseInt(pd_buy_cycle));
				}else {
					product.setPd_buy_cycle(10);
				}
				
				Cell cell14 = sheet.getCell(14, i);//零售周期
				String pd_sell_cycle = cell14.getContents().trim();
				if("".equals(pd_sell_cycle)){
					errorCause = "源文件第"+(i+1)+"行， 零售周期不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(ExcelImportUtil.checkStrRegex("^[1-9]\\d*$", pd_sell_cycle)){//只能输入正整数
					errorCause = "源文件第"+(i+1)+"行，  零售周期只能输入正整数！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_sell_cycle(Integer.parseInt(pd_sell_cycle));
				
				Cell cell15 = sheet.getCell(15, i);//商品季节
				String pd_season = cell15.getContents().trim();
				String pd_season_code = "";
				if("".equals(pd_season)){
					errorCause = "源文件第"+(i+1)+"行， 商品季节不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<seasonList.size();k++){
					if(pd_season.equals(seasonList.get(k).getDtl_name())){
						pd_season_code = seasonList.get(k).getDtl_code();
						break;
					}
				}
				if("".equals(pd_season_code)){
					errorCause = "源文件第"+(i+1)+"行， 您输入的商品季节不存在，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_season(pd_season);
				
				Cell cell16 = sheet.getCell(16, i);//商品年份
				String pd_year = cell16.getContents().trim();
				if("".equals(pd_year)){
					product.setPd_year(Integer.parseInt(DateUtil.getCurrentTime("yyyy")));//获取当前年份
				}else if(ExcelImportUtil.checkStrRegex("^[1-9]\\d*$", pd_year) || pd_year.length()!=4){//只能为数字 并且只能为4位
					product.setPd_year(Integer.parseInt(DateUtil.getCurrentTime("yyyy")));//获取当前年份
				}else{
					product.setPd_year(Integer.parseInt(pd_year));
				}
				
				Cell cell17 = sheet.getCell(17, i);//面料
				String pd_fabric = cell17.getContents().trim();
				String pd_fabric_name = "";
				for(int k=0;k<stuffList.size();k++){
					if(pd_fabric.equals(stuffList.get(k).getDtl_name())){
						pd_fabric_name = stuffList.get(k).getDtl_name();
						break;
					}
				}
				product.setPd_fabric(pd_fabric_name);
				
				Cell cell18 = sheet.getCell(18, i);//里料
				String pd_in_fabric = cell18.getContents().trim();
				if(pd_in_fabric.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的里料太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_in_fabric(pd_in_fabric);
				
				Cell cell19 = sheet.getCell(19, i);//标牌价
				String pd_sign_price = cell19.getContents().trim();
				if("".equals(pd_sign_price)){
					product.setPd_sign_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_sign_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 标牌价格不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_sign_price(Double.parseDouble(pd_sign_price));
					}
				}
				
				Cell cell20 = sheet.getCell(20, i);//会员价
				String pd_vip_price = cell20.getContents().trim();
				if("".equals(pd_vip_price)){
					product.setPd_vip_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_vip_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 会员价格不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_vip_price(Double.parseDouble(pd_vip_price));
					}
				}
				
				Cell cell21 = sheet.getCell(21, i);//零售价
				String pd_sell_price = cell21.getContents().trim();
				if("".equals(pd_sell_price)){
					product.setPd_sell_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_sell_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 零售价格不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_sell_price(Double.parseDouble(pd_sell_price));
					}
				}
				
				Cell cell22 = sheet.getCell(22, i);//成本价
				String pd_cost_price = cell22.getContents().trim();
				if("".equals(pd_cost_price)){
					product.setPd_cost_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_cost_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 成本价格不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_cost_price(Double.parseDouble(pd_cost_price));
					}
				}
				
				Cell cell23 = sheet.getCell(23, i);//进价
				String pd_buy_price = cell23.getContents().trim();
				if("".equals(pd_buy_price)){
					product.setPd_buy_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_buy_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 进价不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_buy_price(Double.parseDouble(pd_buy_price));
					}
				}
				
				Cell cell24 = sheet.getCell(24, i);//配送价
				String pd_sort_price = cell24.getContents().trim();
				if("".equals(pd_sort_price)){
					product.setPd_sort_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_sort_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 配送价不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_sort_price(Double.parseDouble(pd_sort_price));
					}
				}
				
				Cell cell25 = sheet.getCell(25, i);//导购员提成金额
				String pd_commission = cell25.getContents().trim();
				if("".equals(pd_commission)){
					product.setPd_commission((double)0.0);
				}else{
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_commission)){
						errorCause = "源文件第"+(i+1)+"行， 导购员提成金额只能输入数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_commission(Double.parseDouble(pd_commission));
				}
				
				Cell cell26 = sheet.getCell(26, i);//导购员提成率
				String pd_commission_rate = cell26.getContents().trim();
				if("".equals(pd_commission_rate)){
					product.setPd_commission_rate((double)0.0);
				}else{
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_commission_rate)){
						errorCause = "源文件第"+(i+1)+"行， 导购员提成率只能输入0~1之间数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					if(Double.parseDouble(pd_commission_rate)<0 || Double.parseDouble(pd_commission_rate)>1){
						errorCause = "源文件第"+(i+1)+"行， 导购员提成率只能输入0~1之间数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_commission_rate(Double.parseDouble(pd_commission_rate));
				}
				
				Cell cell27 = sheet.getCell(27, i);//批发价
				String pd_batch_price = cell27.getContents().trim();
				if("".equals(pd_batch_price)){
					product.setPd_batch_price((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_batch_price)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 批发价不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_batch_price(Double.parseDouble(pd_batch_price));
					}
				}
				
				Cell cell28 = sheet.getCell(28, i);//积分兑换
				String pd_score = cell28.getContents().trim();
				if("".equals(pd_score)){
					product.setPd_score(0);
				}else{
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_score)){
						errorCause = "源文件第"+(i+1)+"行， 积分兑换只能输入数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_score(Integer.parseInt(pd_score));
				}
				
				Cell cell29 = sheet.getCell(29, i);//规格
				String pd_size = cell29.getContents().trim();
				if(pd_size.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的规格太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_size(pd_size);
				
				Cell cell30 = sheet.getCell(30, i);//安全标准
				String pd_safe = cell30.getContents().trim();
				if(pd_safe.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的安全标准太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_safe(pd_safe);
				
				Cell cell31 = sheet.getCell(31, i);//产地
				String pd_place = cell31.getContents().trim();
				if(pd_place.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的产地太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_place(pd_place);
				
				Cell cell32 = sheet.getCell(32, i);//填充物
				String pd_fill = cell32.getContents().trim();
				if(pd_fill.length() > 30){
					errorCause = "源文件第"+(i+1)+"行， 您输入的填充物太长，请不要超过30个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_fill(pd_fill);
				
				Cell cell33 = sheet.getCell(33, i);//执行标准
				String pd_execute = cell33.getContents().trim();
				if(pd_execute.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的执行标准太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_execute(pd_execute);
				
				Cell cell34 = sheet.getCell(34, i);//等级
				String pd_grade = cell34.getContents().trim();
				if(pd_grade.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的等级太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_grade(pd_grade);
				
				Cell cell35 = sheet.getCell(35, i);//型号
				String pd_model = cell35.getContents().trim();
				if(pd_model.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的型号太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_model(pd_model);
				
				Cell cell36 = sheet.getCell(36, i);//相色
				String pd_color = cell36.getContents().trim();
				if(pd_color.length() > 20){
					errorCause = "源文件第"+(i+1)+"行， 您输入的相色太长，请不要超过20个字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				product.setPd_color(pd_color);
				
				Cell cell37 = sheet.getCell(37, i);//价格特性
				String pd_price_name = cell37.getContents().trim();
				if("".equals(pd_price_name)){
					product.setPd_price_name(pd_price_name);
				}else{
					String price_name = "";
					for(int k=0;k<priceList.size();k++){
						if(pd_price_name.equals(priceList.get(k).getDtl_name())){
							price_name = priceList.get(k).getDtl_name();
						}
					}
					if("".equals(price_name)){
						errorCause = "源文件第"+(i+1)+"行， 您输入的价格特性不存在，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_price_name(price_name);
				}
				
				Cell cell38 = sheet.getCell(38, i);//业务员提成金额
				String pd_salesman_comm = cell38.getContents().trim();
				if("".equals(pd_salesman_comm)){
					product.setPd_salesman_comm((double)0.0);
				}else{
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_salesman_comm)){
						errorCause = "源文件第"+(i+1)+"行， 业务员提成金额只能输入数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_salesman_comm(Double.parseDouble(pd_salesman_comm));
				}
				
				Cell cell39 = sheet.getCell(39, i);//业务员提成率
				String pd_salesman_commrate = cell39.getContents().trim();
				if("".equals(pd_salesman_commrate)){
					product.setPd_salesman_commrate((double)0.0);
				}else{
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_salesman_commrate)){
						errorCause = "源文件第"+(i+1)+"行， 业务员提成率只能输入0~1之间数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					if(Double.parseDouble(pd_salesman_commrate)<0 || Double.parseDouble(pd_salesman_commrate)>1){
						errorCause = "源文件第"+(i+1)+"行， 业务员提成率只能输入0~1之间数字，请修改！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					product.setPd_salesman_commrate(Double.parseDouble(pd_salesman_commrate));
				}
				
				Cell cell40 = sheet.getCell(40, i);//手动打折
				String pd_sale = cell40.getContents().trim();
				if("".equals(pd_sale)){
					product.setPd_sale(1);
				}else{
					if("是".equals(pd_sale)){
						product.setPd_sale(1);
					}else if("否".equals(pd_sale)){
						product.setPd_sale(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 手动打折只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell41 = sheet.getCell(41, i);//分店变价
				String pd_change = cell41.getContents().trim();
				if("".equals(pd_change)){
					product.setPd_change(1);
				}else{
					if("是".equals(pd_change)){
						product.setPd_change(1);
					}else if("否".equals(pd_change)){
						product.setPd_change(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 分店变价只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell42 = sheet.getCell(42, i);//是否赠品
				String pd_gift = cell42.getContents().trim();
				if("".equals(pd_gift)){
					product.setPd_gift(0);
				}else{
					if("是".equals(pd_gift)){
						product.setPd_gift(1);
					}else if("否".equals(pd_gift)){
						product.setPd_gift(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 是否赠品只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell43 = sheet.getCell(43, i);//会员折扣
				String pd_vip_sale = cell43.getContents().trim();
				if("".equals(pd_vip_sale)){
					product.setPd_vip_sale(1);
				}else{
					if("是".equals(pd_vip_sale)){
						product.setPd_vip_sale(1);
					}else if("否".equals(pd_vip_sale)){
						product.setPd_vip_sale(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行，会员折扣只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell44 = sheet.getCell(44, i);//是否礼品
				String pd_present = cell44.getContents().trim();
				if("".equals(pd_present)){
					product.setPd_present(0);
				}else{
					if("是".equals(pd_present)){
						product.setPd_present(1);
					}else if("否".equals(pd_present)){
						product.setPd_present(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 是否礼品只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell45 = sheet.getCell(45, i);//允许采购
				String pd_buy = cell45.getContents().trim();
				if("".equals(pd_buy)){
					product.setPd_buy(1);
				}else{
					if("是".equals(pd_buy)){
						product.setPd_buy(1);
					}else if("否".equals(pd_buy)){
						product.setPd_buy(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 允许采购只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell46 = sheet.getCell(46, i);//是否积分
				String pd_point = cell46.getContents().trim();
				if("".equals(pd_point)){
					product.setPd_point(1);
				}else{
					if("是".equals(pd_point)){
						product.setPd_point(1);
					}else if("否".equals(pd_point)){
						product.setPd_point(0);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 是否积分只允许输入'是'和'否'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell47 = sheet.getCell(47, i);//批发价1
				String pd_batch_price1 = cell47.getContents().trim();
				if("".equals(pd_batch_price1)){
					product.setPd_batch_price1((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_batch_price1)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 批发价1不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_batch_price1(Double.parseDouble(pd_batch_price1));
					}
				}
				
				Cell cell48 = sheet.getCell(48, i);//批发价2
				String pd_batch_price2 = cell48.getContents().trim();
				if("".equals(pd_batch_price2)){
					product.setPd_batch_price2((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_batch_price2)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 批发价2不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_batch_price2(Double.parseDouble(pd_batch_price2));
					}
				}
				
				Cell cell49 = sheet.getCell(49, i);//批发价3
				String pd_batch_price3 = cell49.getContents().trim();
				if("".equals(pd_batch_price3)){
					product.setPd_batch_price3((double)0.0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", pd_batch_price3)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 批发价3不正确，请输入正确的价格！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						product.setPd_batch_price3(Double.parseDouble(pd_batch_price3));
					}
				}
				
				pdNoList.add(pd_no);//如果添加成功，把该条货号放入已存在的货号中
				
				product.setPd_sysdate(DateUtil.getCurrentTime());
				product.setPd_add_manager(user.getUs_name());//创建人
				product.setPd_add_date(DateUtil.getYearMonthDate());//创建时间
				product.setCompanyid(companyid);
				product.setPd_state(0);//状态
				
				productList.add(product);
				colorMap.put(pd_no, productColorList);//货号对应的颜色
				brMap.put(pd_no, productBrList);//货号对应的杯型
				successRow++;
			}
			
			resultMap.put("productList", productList);//商品资料
			resultMap.put("colorMap", colorMap);//商品资料对应的颜色
			resultMap.put("brMap", brMap);//商品资料对应的杯型
			resultMap.put("fail", errorRow-1);// 错误条数
			resultMap.put("success", successRow);// 成功条数
		} catch (InterruptedException e) {
			 e.printStackTrace(); 
		}catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}

	@Override
	public Map<String, Object> parseSavingBarcodeExcel(Workbook soureWorkBook,
			WritableWorkbook errsheet, T_Sys_User user, T_Sys_Set set) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		try {
			Integer companyid= user.getCompanyid();
			// 得到工作簿中的第一个表索引即为excel下的sheet1,sheet2,sheet3...
			Sheet sheet = soureWorkBook.getSheet(0);
			WritableSheet errorSheet = errsheet.getSheet(0);
			// 条形码导入列数为5，再多的列数不允许导入
			int columns = 5;
			int rows = sheet.getRows(); // 获取导入行数
			System.out.println("条形码导入数共计" + (rows - 1) + "条!");
			if(rows <= 1){
				resultMap.put("fail", 0);// 错误条数
				resultMap.put("success", 0);// 成功条数
				return resultMap;
			}

			//获取导入条形码需要的数据
			Map<String, Object> checkImportBarcode = productDAO.getCheckImportBarcode(user);
			
			if(checkImportBarcode == null){
				resultMap.put("fail", 0);// 错误条数
				resultMap.put("success", 0);// 成功条数
				return resultMap;
			}
			
			Map<String,Map<String,Object>> pd_nos = (Map<String,Map<String,Object>>)checkImportBarcode.get("productMap");//已有商品货号
			List<Map<String, String>> barcodes = (List<Map<String, String>>)checkImportBarcode.get("barcodeList");//条形码
			
			List<T_Base_Barcode> barcodeList = new ArrayList<T_Base_Barcode>();//存放验证成功的商品资料
			T_Base_Barcode t_Base_Barcode = null;
			String errorCause = "";//错误原因
			int errorRow = 1;
			int successRow = 0;//导入成功行数
			// 第一行为属性，从第二行开始
			for (int i = 1; i < rows; i++) {
				if(i%100 == 0){
					Thread.sleep(1000);
				}
				t_Base_Barcode = new T_Base_Barcode();
				
				if (ExcelImportUtil.isBlankRow(sheet, i, columns - 1)) {//判断是否为空行
					continue;
				}
				
				boolean tag = false;
				
				Cell cell0 = sheet.getCell(0, i);// 商品货号
				String pd_no = cell0.getContents().trim().toLowerCase();
				if("".equals(pd_no)){//商品货号不能为空
					errorCause = "源文件第"+(i+1)+"行， 商品货号不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(ExcelImportUtil.checkStrRegex("^[\\dA-Za-z-._]+$", pd_no)){//货号只能输入英文、数字、.、_、-
					errorCause = "源文件第"+(i+1)+"行， 商品货号只能输入英文、数字和(. - _)符号";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if (pd_nos.get(pd_no)==null){
					errorCause = "源文件第"+(i+1)+"行， 商品货号不存在，请重新输入";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				Map<String,Object> infoMap = pd_nos.get(pd_no);//获取货号对应的尺码、颜色、杯型、code等信息
				t_Base_Barcode.setBc_pd_no(pd_no);
				String pd_code = (String)infoMap.get("pdCode");
				t_Base_Barcode.setBc_pd_code(pd_code);
				
				Cell cell1 = sheet.getCell(1, i);//颜色
				String cr_name = cell1.getContents().trim();
				String cr_code = "";
				Map<String,String> colorMap=(Map<String,String>)infoMap.get("colorMap");//获取货号颜色
				if(!"".equals(cr_name)){
					cr_code=StringUtil.trimString(colorMap.get(cr_name));
					if (cr_code.length()==0){//颜色不存在
						errorCause = "源文件第"+(i+1)+"行，颜色不存在，请重新输入";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					t_Base_Barcode.setBc_color(cr_code);
				}else{
					errorCause = "源文件第"+(i+1)+"行， 请输入颜色";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				
				Cell cell2 = sheet.getCell(2, i);//尺码
				String sz_name = cell2.getContents().trim();
				String sz_code = "";
				if(!"".equals(sz_name)){
					Map<String,String> sizeMap=(Map<String,String>)infoMap.get("sizeMap");//获取货号尺码
					sz_code=StringUtil.trimString(sizeMap.get(sz_name));
					if (sz_code.length()==0){//尺码不存在
						errorCause = "源文件第"+(i+1)+"行，尺码不存在，请重新输入";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					t_Base_Barcode.setBc_size(sz_code);
				}else{
					errorCause = "源文件第"+(i+1)+"行， 请输入尺码";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				
				Cell cell3 = sheet.getCell(3, i);//杯型
				String br_name = cell3.getContents().trim();
				String br_code = "";
				Map<String,String> brMap=(Map<String,String>)infoMap.get("brMap");//获取货号杯型
				if(brMap.size()==0){//没有杯型
					if(br_name.length()>0){//商品中不不存在该杯型
						errorCause = "源文件第"+(i+1)+"行， 商品没有杯型，请重新输入";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}else{//有杯型
					if(!"".equals(br_name)){
						tag = false;
						br_code=StringUtil.trimString(brMap.get(br_name));
						if (br_code.length()==0){//杯型不存在
							errorCause = "源文件第"+(i+1)+"行，杯型不存在，请重新输入";
							ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
							errorRow++;
							continue;
						}
						t_Base_Barcode.setBc_bra(br_code);
					}else{
						errorCause = "源文件第"+(i+1)+"行， 请输入杯型";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				
				Cell cell4 = sheet.getCell(4, i);//条形码
				String barcode = cell4.getContents().trim();
				if("".equals(barcode)){
					errorCause = "源文件第"+(i+1)+"行， 条形码不能为空，请修改！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				
				if(ExcelImportUtil.checkStrRegex("^[\\dA-Za-z-._]+$", barcode)){//只能输入数字和英文 -._
					errorCause = "源文件第"+(i+1)+"行， 条形码只能输入英文和汉字！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				
				Map<String, String> barcodeMap = barcodes.get(0);
				if(barcodeMap == null){
					errorCause = "源文件第"+(i+1)+"行， 数据异常，请联系管理员！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(barcodeMap.get(barcode.toUpperCase()) != null){
					errorCause = "源文件第"+(i+1)+"行， 条形码已存在，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}else{
					barcodeMap.put(barcode.toUpperCase(), barcode.toUpperCase());
					t_Base_Barcode.setBc_barcode(barcode.toUpperCase());
				}
				
				String subcode = pd_code+cr_code+sz_code+br_code;
				String sys_barcode = "";
				Integer ST_SubCodeIsRule = set.getSt_subcode_isrule();
				String ST_SubCodeRule = set.getSt_subcode_rule();
				String []subcodeRules = ST_SubCodeRule.split("\\+");//分割
				if(ST_SubCodeIsRule == 0){
					sys_barcode = pd_code+cr_code+sz_code+br_code;
				}else{
					for(int j=0;j<subcodeRules.length;j++){
						if("货号"==subcodeRules[j]){
							sys_barcode += pd_no;
							continue;
						}
						if("编码"==subcodeRules[j]){
							sys_barcode += pd_code;
							continue;
						}
						if("颜色"==subcodeRules[j]){
							sys_barcode += cr_code;
							continue;
						}
						if("尺码"==subcodeRules[j]){
							sys_barcode += sz_code;
							continue;
						}
						if("杯型"==subcodeRules[j]){
							sys_barcode += br_code;
							continue;
						}
					}
				}
				t_Base_Barcode.setBc_subcode(subcode);
				t_Base_Barcode.setBc_sys_barcode(sys_barcode);
				t_Base_Barcode.setCompanyid(companyid);
				barcodeList.add(t_Base_Barcode);
				successRow++;
			}
			resultMap.put("barcodeList", barcodeList);//验证成功的条形码
			resultMap.put("fail", errorRow-1);// 错误条数
			resultMap.put("success", successRow);// 成功条数
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return resultMap;
	}

	@Override
	@Transactional
	public void save_import_barcode(Map<String, Object> param) {
		List<T_Base_Barcode> barcodeList = (List<T_Base_Barcode>)param.get("barcodeList");//导入成功条形码
		T_Import_Info t_Import_Info = (T_Import_Info)param.get("t_Import_Info");
		if(barcodeList != null && barcodeList.size()>0){
			productDAO.saveBarcode(barcodeList);
		}
		// 查询当前用户是否导入过数据
		T_Import_Info info = importInfoDAO.queryImportInfo(param);
		if(info != null){
			String temp_uploaddir = CommonUtil.CHECK_BASE + CommonUtil.EXCEL_PATH;
			String last_error_filename = temp_uploaddir + File.separator + info.getIi_error_filename();
			File last_error_file = new File(last_error_filename);
			if (last_error_file.exists()) {
				last_error_file.delete();
			}
			ExcelImportUtil.deleteFile(last_error_file);
			importInfoDAO.updateImportInfo(t_Import_Info);
		}else {
			importInfoDAO.saveImportInfo(t_Import_Info);
		}
	}

	@Override
	public T_Base_Product_Assist get_assist_byid(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return productDAO.get_assist_byid(param);
	}

	@Override
	public List<T_Base_Product_Assist> assist_template(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return productDAO.assist_template(param);
	}

	@Override
	public void save_product_assist(T_Base_Product_Assist product_Assist) {
		if(product_Assist == null || product_Assist == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(product_Assist.getPda_name() == null || "".equals(product_Assist.getPda_name())){
			throw new IllegalArgumentException("模板名称不能为空");
		}
		productDAO.save_product_assist(product_Assist);
	}

	@Override
	public void update_product_assist(T_Base_Product_Assist product_Assist) {
		if(product_Assist == null || product_Assist == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(product_Assist.getPda_name() == null || "".equals(product_Assist.getPda_name())){
			throw new IllegalArgumentException("模板名称不能为空");
		}
		productDAO.update_product_assist(product_Assist);
	}

	@Override
	public void del_product_assist(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pda_id = param.get("pda_id");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(pda_id == null || "".equals(pda_id)){
			throw new IllegalArgumentException("id不能为空！");
		}
		productDAO.del_product_assist(param);
	}
}
