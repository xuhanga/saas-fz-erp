package zy.service.base.shop;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.shop.T_Base_Shop_Reward;

public interface ShopRewardService {
	List<T_Base_Shop_Reward> statByShop(Map<String, Object> params);
	PageData<T_Base_Shop_Reward> page(Map<String, Object> params);
}
