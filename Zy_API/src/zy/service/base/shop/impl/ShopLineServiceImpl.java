package zy.service.base.shop.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.shop.ShopLineDAO;
import zy.entity.base.shop.T_Base_Shop_Line;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.shop.ShopLineService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Service
public class ShopLineServiceImpl implements ShopLineService{
	@Resource
	private ShopLineDAO shopLineDAO;
	
	@Override
	public T_Base_Shop_Line load(String shop_code, Integer companyid) {
		return shopLineDAO.load(shop_code, companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Base_Shop_Line shopLine, T_Sys_User user) {
		if (shopLine.getSpl_id() != null && shopLine.getSpl_id() > 0) {
			shopLineDAO.update(shopLine);
		}else {
			if (CommonUtil.ONE.equals(user.getShoptype())
					|| CommonUtil.TWO.equals(user.getShoptype())
					|| CommonUtil.FOUR.equals(user.getShoptype())
					|| CommonUtil.FIVE.equals(user.getShoptype())) {// 总公司、分公司、加盟、合伙店
				shopLine.setSpl_shop_code(user.getUs_shop_code());
			}else{//自营
				shopLine.setSpl_shop_code(user.getShop_upcode());
			}
			if (CommonUtil.FOUR.equals(user.getShoptype()) || CommonUtil.FIVE.equals(user.getShoptype())) {// 加盟、合伙店
				shopLine.setSpl_ordershop_code(user.getUs_shop_code());
			}
			shopLine.setSpl_state(0);
			shopLine.setSpl_sysdate(DateUtil.getCurrentTime());
			shopLine.setCompanyid(user.getCompanyid());
			shopLineDAO.save(shopLine);
		}
	}
	
	@Override
	public List<T_Base_Shop_Line> listByMobile(String mobile) {
		return shopLineDAO.listByMobile(mobile);
	}
	
}
