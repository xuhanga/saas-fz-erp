package zy.service.base.shop.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.shop.ShopRewardDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.shop.T_Base_Shop_Reward;
import zy.service.base.shop.ShopRewardService;
import zy.util.CommonUtil;

@Service
public class ShopRewardServiceImpl implements ShopRewardService{
	@Resource
	private ShopRewardDAO shopRewardDAO;
	
	@Override
	public List<T_Base_Shop_Reward> statByShop(Map<String, Object> params) {
		return shopRewardDAO.statByShop(params);
	}
	
	@Override
	public PageData<T_Base_Shop_Reward> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = shopRewardDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Shop_Reward> list = shopRewardDAO.list(params);
		PageData<T_Base_Shop_Reward> pageData = new PageData<T_Base_Shop_Reward>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
