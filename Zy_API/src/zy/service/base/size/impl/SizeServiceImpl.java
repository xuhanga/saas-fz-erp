package zy.service.base.size.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.size.SizeDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.base.size.T_Base_Size_Group;
import zy.service.base.size.SizeService;
import zy.util.CommonUtil;
@Service
public class SizeServiceImpl implements SizeService{
	@Resource
	private SizeDAO sizeDAO;
	@Override
	public PageData<T_Base_Size> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = sizeDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Size> list = sizeDAO.list(param);
		PageData<T_Base_Size> pageData = new PageData<T_Base_Size>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	
	@Override
	public List<T_Base_Size> list(Map<String, Object> param) {
		return sizeDAO.list(param);
	}



	@Override
	public List<T_Base_Size_Group> listGroup(Map<String, Object> param) {
		return sizeDAO.listGroup(param);
	}

	@Override
	public List<T_Base_SizeList> listByID(Map<String, Object> param) {
		return sizeDAO.listByID(param);
	}

	@Override
	public Integer queryByName(T_Base_Size model) {
		return sizeDAO.queryByName(model);
	}
	@Transactional
	@Override
	public void save(T_Base_Size model) {
		sizeDAO.save(model);
	}
	@Transactional
	@Override
	public void update(T_Base_Size model) {
		sizeDAO.update(model);
	}
	@Transactional
	@Override
	public void del(Integer id) {
		sizeDAO.del(id);
	}

	@Override
	public T_Base_Size_Group queryGroupByID(Integer sz_id) {
		return sizeDAO.queryGroupByID(sz_id);
	}

	@Override
	public T_Base_Size queryByID(Integer id) {
		return sizeDAO.queryByID(id);
	}
	@Transactional
	@Override
	public void saveGroup(Map<String,Object> map) {
		sizeDAO.saveGroup(map);
	}
	@Transactional
	@Override
	public void updateGroup(Map<String,Object> map) {
		sizeDAO.updateGroup(map);
	}
	@Transactional
	@Override
	public void delGroup(Integer sz_id) {
		sizeDAO.delGroup(sz_id);
	}

}
