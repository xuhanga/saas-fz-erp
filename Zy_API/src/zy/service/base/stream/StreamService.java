package zy.service.base.stream;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.base.stream.T_Base_Stream;

public interface StreamService {
	PageData<T_Base_Stream> page(Map<String,Object> param);
	void save(T_Base_Stream model);
	T_Base_Stream queryByID(Integer id);
	void update(T_Base_Stream model);
	void del(Integer id);
}
