package zy.service.base.type;

import java.util.List;
import java.util.Map;

import zy.entity.base.type.T_Base_Type;

public interface TypeService {
	public List<T_Base_Type> list(Map<String, Object> param);
	Integer queryByName(T_Base_Type t_Base_Type);
	public void save(T_Base_Type t_Base_Type);
	void del(Integer tp_id);
	T_Base_Type queryByID(Integer tp_id);
	void update(T_Base_Type type);
}
