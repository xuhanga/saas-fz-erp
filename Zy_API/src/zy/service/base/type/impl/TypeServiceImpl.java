package zy.service.base.type.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.base.type.TypeDAO;
import zy.entity.base.type.T_Base_Type;
import zy.service.base.type.TypeService;
import zy.util.CommonUtil;

@Service
public class TypeServiceImpl implements TypeService{
	@Resource
	private TypeDAO typeDAO;
	@Override
	public List<T_Base_Type> list(Map<String, Object> param) {
		if (param == null) {
			throw new IllegalArgumentException("查询参数不能为null");
		}
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object sidx = param.get(CommonUtil.SIDX);
		Object sord = param.get(CommonUtil.SORD); // desc or asc
		Object searchContent = param.get("searchContent");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		if (sidx != null) {
			if (!(sidx instanceof String)) {
				throw new IllegalArgumentException("排序参数需为String类型");
			} else {
				String _sidx = (String) sidx;
				if (!_sidx.equals("tp_id") && !_sidx.equals("tp_name")) {
					throw new IllegalArgumentException("排序参数非法");
				}

				if (sord != null) {
					if (!(sord instanceof String)) {
						throw new IllegalArgumentException("排序类型参数需为String类型");
					} else {
						String _sord = (String) sord;
						if (!_sord.endsWith("asc") && !_sord.endsWith("desc")) {
							throw new IllegalArgumentException("排序类型参数需为'asc'或'desc'");
						}
					}
				} else {
					param.put(CommonUtil.SORD, "asc");
				}
			}
		} else {
			param.put(CommonUtil.SIDX, "tp_id");
			param.put(CommonUtil.SORD, "asc");
		}
		
		if (searchContent != null && !"".equals(searchContent)) {
			if (searchContent instanceof String) {
				param.put("tp_name", searchContent);
			} else {
				throw new IllegalArgumentException("模糊查询参数需为String类型");
			}
		}
		return typeDAO.list(param);
	}
	
	@Override
	public Integer queryByName(T_Base_Type t_Base_Type) {
		return typeDAO.queryByName(t_Base_Type);
	}

	@Override
	public void save(T_Base_Type t_Base_Type) {
		typeDAO.save(t_Base_Type);
	}

	@Override
	public void del(Integer tp_id) {
		typeDAO.del(tp_id);
	}

	@Override
	public T_Base_Type queryByID(Integer tp_id) {
		return typeDAO.queryByID(tp_id);
	}

	@Override
	public void update(T_Base_Type type) {
		typeDAO.update(type);
	}
	
}
