package zy.service.batch.client.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.batch.client.ClientDAO;
import zy.dto.batch.money.ClientBackAnalysisDto;
import zy.dto.batch.money.ClientMoneyDetailsDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.client.T_Batch_Client_Shop;
import zy.entity.sys.user.T_Sys_User;
import zy.service.batch.client.ClientService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Service
public class ClientServiceImpl implements ClientService{
	@Resource
	private ClientDAO clientDAO;
	
	@Override
	public PageData<T_Batch_Client> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = clientDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Batch_Client> list = clientDAO.list(params);
		PageData<T_Batch_Client> pageData = new PageData<T_Batch_Client>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public PageData<T_Batch_Client> page4dialog(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = clientDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Batch_Client> list = clientDAO.list4dialog(params);
		PageData<T_Batch_Client> pageData = new PageData<T_Batch_Client>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Batch_Client load(Integer ci_id) {
		return clientDAO.load(ci_id);
	}
	
	@Override
	@Transactional
	public void save(T_Batch_Client client, T_Sys_User user) {
		if(StringUtil.isEmpty(client.getCi_name())){
			throw new IllegalArgumentException("客户名称不能为空");
		}
		if(StringUtil.isEmpty(client.getCi_area())){
			throw new IllegalArgumentException("客户所在区域不能为空");
		}
		client.setCompanyid(user.getCompanyid());
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
			client.setCi_sp_code(user.getUs_shop_code());
		}else{//自营、加盟、合伙
			client.setCi_sp_code(user.getShop_upcode());
		}
		T_Batch_Client existClient = clientDAO.check(client);
		if (existClient != null) {
			throw new RuntimeException("客户已存在!");
		}
		client.setCi_spell(StringUtil.getSpell(client.getCi_name()));
		if(client.getCi_rate() == null){
			client.setCi_rate(1d);
		}
		if(client.getCi_init_debt() == null){
			client.setCi_init_debt(0d);
		}
		client.setCi_receivable(0d);
		client.setCi_received(0d);
		client.setCi_prepay(0d);
		if(client.getCi_batch_cycle() == null){
			client.setCi_batch_cycle(7);
		}
		if(client.getCi_settle_cycle() == null){
			client.setCi_settle_cycle(15);
		}
		if(client.getCi_earnest() == null){
			client.setCi_earnest(0d);
		}
		if(client.getCi_deposit() == null){
			client.setCi_deposit(0d);
		}
		if(client.getCi_credit_limit() == null){
			client.setCi_credit_limit(0d);
		}
		clientDAO.save(client);
	}
	
	@Override
	@Transactional
	public void save_client_shop(T_Batch_Client_Shop client_Shop,
			T_Sys_User user) {
		if(StringUtil.isEmpty(client_Shop.getCis_name())){
			throw new IllegalArgumentException("店铺名称不能为空");
		}
		client_Shop.setCompanyid(user.getCompanyid());
		T_Batch_Client_Shop existClientShop = clientDAO.check_client_shop(client_Shop);
		if (existClientShop != null) {
			throw new RuntimeException("店铺已存在!");
		}
		client_Shop.setCis_name(StringUtil.decodeString(client_Shop.getCis_name()));
		client_Shop.setCis_link_adr(StringUtil.decodeString(client_Shop.getCis_link_adr()));
		client_Shop.setCis_link_mobile(StringUtil.decodeString(client_Shop.getCis_link_mobile()));
		client_Shop.setCis_link_tel(StringUtil.decodeString(client_Shop.getCis_link_tel()));
		client_Shop.setCis_linkman(StringUtil.decodeString(client_Shop.getCis_linkman()));
		client_Shop.setCis_spell(StringUtil.getSpell(client_Shop.getCis_name()));
		clientDAO.save_client_shop(client_Shop);
	}

	@Override
	@Transactional
	public void update(T_Batch_Client client, T_Sys_User user) {
		if(StringUtil.isEmpty(client.getCi_name())){
			throw new IllegalArgumentException("客户名称不能为空");
		}
		if(StringUtil.isEmpty(client.getCi_area())){
			throw new IllegalArgumentException("客户所在区域不能为空");
		}
		client.setCompanyid(user.getCompanyid());
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
			client.setCi_sp_code(user.getUs_shop_code());
		}else{//自营、加盟、合伙
			client.setCi_sp_code(user.getShop_upcode());
		}
		T_Batch_Client existClient = clientDAO.check(client);
		if (existClient != null) {
			throw new RuntimeException("客户已存在!");
		}
		client.setCi_spell(StringUtil.getSpell(client.getCi_name()));
		if(client.getCi_rate() == null){
			client.setCi_rate(1d);
		}
		if(client.getCi_init_debt() == null){
			client.setCi_init_debt(0d);
		}
		if(client.getCi_batch_cycle() == null){
			client.setCi_batch_cycle(7);
		}
		if(client.getCi_settle_cycle() == null){
			client.setCi_settle_cycle(15);
		}
		if(client.getCi_earnest() == null){
			client.setCi_earnest(0d);
		}
		if(client.getCi_deposit() == null){
			client.setCi_deposit(0d);
		}
		if(client.getCi_credit_limit() == null){
			client.setCi_credit_limit(0d);
		}
		if(user.getSp_init() == 1){//启用账套，则客户期初欠款不能修改
			client.setCi_init_debt(null);
		}
		clientDAO.update(client);
	}
	
	@Override
	@Transactional
	public void del(Integer ci_id, String ci_code,Integer companyid) {
		//TODO 验证是否有批发数据
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (StringUtil.isEmpty(ci_code)) {
			throw new IllegalArgumentException("参数ci_code不能为null");
		}
		clientDAO.del(ci_id,ci_code,companyid);
	}
	
	@Override
	public PageData<ClientMoneyDetailsDto> pageMoneyDetails(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = clientDAO.countsumMoneyDetails(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<ClientMoneyDetailsDto> list = clientDAO.listMoneyDetails(params);
		PageData<ClientMoneyDetailsDto> pageData = new PageData<ClientMoneyDetailsDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
	@Override
	public List<ClientBackAnalysisDto> listBackAnalysis(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return clientDAO.listBackAnalysis(params);
	}

	@Override
	public List<T_Batch_Client_Shop> shop_list(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		List<T_Batch_Client_Shop> list = clientDAO.shop_list(params);
		return list;
	}

	@Override
	public void del_client_shop(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		clientDAO.del_client_shop(params);
	}

	@Override
	public T_Batch_Client_Shop load_shop(Integer cis_id) {
		return clientDAO.load_shop(cis_id);
	}

	@Override
	public void update_client_shop(T_Batch_Client_Shop client_Shop,
			T_Sys_User user) {
		if(StringUtil.isEmpty(client_Shop.getCis_name())){
			throw new IllegalArgumentException("店铺名称不能为空");
		}
		client_Shop.setCompanyid(user.getCompanyid());
		T_Batch_Client_Shop existClientShop = clientDAO.check_client_shop(client_Shop);
		if (existClientShop != null) {
			throw new RuntimeException("店铺已存在!");
		}
		client_Shop.setCis_name(StringUtil.decodeString(client_Shop.getCis_name()));
		client_Shop.setCis_link_adr(StringUtil.decodeString(client_Shop.getCis_link_adr()));
		client_Shop.setCis_link_mobile(StringUtil.decodeString(client_Shop.getCis_link_mobile()));
		client_Shop.setCis_link_tel(StringUtil.decodeString(client_Shop.getCis_link_tel()));
		client_Shop.setCis_linkman(StringUtil.decodeString(client_Shop.getCis_linkman()));
		client_Shop.setCis_spell(StringUtil.getSpell(client_Shop.getCis_name()));
		clientDAO.update_client_shop(client_Shop);
	}
}
