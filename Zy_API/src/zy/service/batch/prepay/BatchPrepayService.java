package zy.service.batch.prepay;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.prepay.T_Batch_Prepay;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.sys.user.T_Sys_User;

public interface BatchPrepayService {
	PageData<T_Batch_Prepay> page(Map<String, Object> params);
	T_Batch_Prepay load(Integer pp_id);
	T_Batch_Prepay load(String number,Integer companyid);
	T_Batch_Client loadClient(String ci_code,Integer companyid);
	T_Money_Bank loadBank(String ba_code,Integer companyid);
	void save(T_Batch_Prepay prepay, T_Sys_User user);
	void update(T_Batch_Prepay prepay, T_Sys_User user);
	T_Batch_Prepay approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Batch_Prepay reverse(String number, T_Sys_User user);
	void del(String number, Integer companyid);
}
