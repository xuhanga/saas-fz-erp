package zy.service.batch.sell;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Product;
import zy.entity.batch.sell.T_Batch_Sell;
import zy.entity.batch.sell.T_Batch_SellList;
import zy.entity.sys.user.T_Sys_User;

public interface BatchSellService {
	PageData<T_Batch_Sell> page(Map<String,Object> params);
	List<T_Batch_Sell> listExport(Map<String, Object> params);
	T_Batch_Sell load(Integer se_id);
	T_Batch_Sell load(String number,Integer companyid);
	List<T_Batch_SellList> detail_list(Map<String, Object> params);
	List<T_Batch_SellList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Batch_SellList> temp_list(Map<String, Object> params);
	List<T_Batch_SellList> temp_sum(Integer se_type,Integer us_id,Integer companyid);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	void temp_save(Map<String, Object> params);
	void temp_import(Map<String, Object> params);
	void temp_import_order(Map<String, Object> params);
	void temp_updateAmount(T_Batch_SellList temp);
	void temp_updatePrice(T_Batch_SellList temp);
	void temp_updateRebatePrice(T_Batch_SellList temp);
	void temp_updateRemarkById(T_Batch_SellList temp);
	void temp_updateRemarkByPdCode(T_Batch_SellList temp);
	void temp_del(Integer sel_id);
	void temp_delByPiCode(T_Batch_SellList temp);
	void temp_clear(Integer se_type,Integer us_id,Integer companyid);
	void save(T_Batch_Sell sell, T_Sys_User user);
	void update(T_Batch_Sell sell, T_Sys_User user);
	T_Batch_Sell approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Batch_Sell reverse(String number, T_Sys_User user);
	void initUpdate(String number,Integer se_type, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode,T_Sys_User user);
}
