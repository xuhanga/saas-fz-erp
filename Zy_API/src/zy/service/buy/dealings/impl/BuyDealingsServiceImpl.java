package zy.service.buy.dealings.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.buy.dealings.BuyDealingsDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.buy.dealings.T_Buy_Dealings;
import zy.service.buy.dealings.BuyDealingsService;
import zy.util.CommonUtil;

@Service
public class BuyDealingsServiceImpl implements BuyDealingsService{
	@Resource
	private BuyDealingsDAO buyDealingsDAO;
	
	@Override
	public PageData<T_Buy_Dealings> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buyDealingsDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Buy_Dealings> list = buyDealingsDAO.list(params);
		PageData<T_Buy_Dealings> pageData = new PageData<T_Buy_Dealings>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
