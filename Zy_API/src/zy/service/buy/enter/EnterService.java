package zy.service.buy.enter;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Product;
import zy.entity.buy.enter.T_Buy_Enter;
import zy.entity.buy.enter.T_Buy_EnterList;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;

public interface EnterService {
	PageData<T_Buy_Enter> page(Map<String,Object> params);
	List<T_Buy_Enter> listExport(Map<String, Object> params);
	T_Buy_Enter load(Integer et_id);
	T_Buy_Enter load(String number,Integer companyid);
	List<T_Buy_EnterList> detail_list(Map<String, Object> params);
	List<T_Buy_EnterList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Buy_EnterList> temp_list(Map<String, Object> params);
	List<T_Buy_EnterList> temp_sum(Map<String, Object> params);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	void temp_save(Map<String, Object> params);
	void temp_import(Map<String, Object> params);
	void temp_import_draft(Map<String, Object> params);
	void temp_import_order(Map<String, Object> params);
	void temp_updateAmount(T_Buy_EnterList temp);
	void temp_updatePrice(T_Buy_EnterList temp);
	void temp_updateRemarkById(T_Buy_EnterList temp);
	void temp_updateRemarkByPdCode(T_Buy_EnterList temp);
	void temp_del(Integer etl_id);
	void temp_delByPiCode(T_Buy_EnterList temp);
	void temp_clear(Integer et_type,Integer us_id,Integer companyid);
	void save(T_Buy_Enter enter, T_Sys_User user);
	void update(T_Buy_Enter enter, T_Sys_User user);
	T_Buy_Enter approve(String number, T_Approve_Record record, T_Sys_User user,T_Sys_Set set);
	T_Buy_Enter reverse(String number,T_Sys_User user,T_Sys_Set set);
	void initUpdate(String number,Integer et_type, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number,Integer sp_id,Integer displayMode,T_Sys_User user);
	List<T_Base_Barcode> print_barcode(Map<String, Object> params);
}
