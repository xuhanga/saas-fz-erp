package zy.service.buy.fee;

import java.util.List;
import java.util.Map;

import zy.dto.buy.fee.BuyFeeReportDto;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.fee.T_Buy_Fee;
import zy.entity.buy.fee.T_Buy_FeeList;
import zy.entity.sys.user.T_Sys_User;

public interface BuyFeeService {
	PageData<T_Buy_Fee> page(Map<String, Object> params);
	T_Buy_Fee load(Integer fe_id);
	T_Buy_Fee load(String number,Integer companyid);
	List<T_Buy_FeeList> temp_list(Map<String, Object> params);
	void temp_save(List<T_Buy_FeeList> temps,T_Sys_User user);
	void temp_updateMoney(T_Buy_FeeList temp);
	void temp_updateRemark(T_Buy_FeeList temp);
	void temp_del(Integer fel_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Buy_FeeList> detail_list(Map<String, Object> params);
	void save(T_Buy_Fee fee, T_Sys_User user);
	void update(T_Buy_Fee fee, T_Sys_User user);
	T_Buy_Fee approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Buy_Fee reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
	List<BuyFeeReportDto> listReport(Map<String, Object> params);
	List<BuyFeeReportDto> listReportDetail(Map<String, Object> params);
}
