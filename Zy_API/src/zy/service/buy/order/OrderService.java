package zy.service.buy.order;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Product;
import zy.entity.buy.order.T_Buy_Order;
import zy.entity.buy.order.T_Buy_OrderList;
import zy.entity.sys.user.T_Sys_User;

public interface OrderService {
	PageData<T_Buy_Order> page(Map<String,Object> params);
	PageData<T_Buy_Order> page4Enter(Map<String, Object> params);
	T_Buy_Order load(Integer od_id);
	List<T_Buy_OrderList> detail_list(Map<String, Object> params);
	List<T_Buy_OrderList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Buy_OrderList> temp_list(Map<String, Object> params);
	List<T_Buy_OrderList> temp_sum(Integer od_type,Integer us_id,Integer companyid);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	void temp_save(Map<String, Object> params);
	void temp_import(Map<String, Object> params);
	void temp_import_draft(Map<String, Object> params);
	void temp_updateAmount(T_Buy_OrderList temp);
	void temp_updatePrice(T_Buy_OrderList temp);
	void temp_updateRemarkById(T_Buy_OrderList temp);
	void temp_updateRemarkByPdCode(T_Buy_OrderList temp);
	void temp_del(Integer odl_id);
	void temp_delByPiCode(T_Buy_OrderList temp);
	void temp_clear(Integer od_type,Integer us_id,Integer companyid);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	void save(T_Buy_Order order,T_Sys_User user);
	void update(T_Buy_Order order,T_Sys_User user);
	T_Buy_Order approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Buy_Order stop(String number,T_Sys_User user);
	void initUpdate(String number,Integer od_type, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number,Integer sp_id,Integer displayMode,T_Sys_User user);
}
