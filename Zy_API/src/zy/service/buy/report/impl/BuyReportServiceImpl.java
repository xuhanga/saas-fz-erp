package zy.service.buy.report.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import zy.dao.buy.report.BuyReportDAO;
import zy.dto.buy.enter.EnterDetailReportDto;
import zy.dto.buy.enter.EnterDetailSizeReportDto;
import zy.dto.buy.enter.EnterDetailsDto;
import zy.dto.buy.enter.EnterRankDto;
import zy.dto.buy.enter.EnterReportCsbDto;
import zy.dto.buy.enter.EnterReportDto;
import zy.dto.common.TypeLevelDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.type.T_Base_Type;
import zy.service.buy.report.BuyReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.base.TypeVO;

@Service
public class BuyReportServiceImpl implements BuyReportService{
	@Resource
	private BuyReportDAO buyReportDAO;
	
	@Override
	public PageData<EnterReportDto> pageEnterReport(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buyReportDAO.countEnterReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<EnterReportDto> list = buyReportDAO.listEnterReport(params);
		PageData<EnterReportDto> pageData = new PageData<EnterReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(buyReportDAO.sumEnterReport(params));
		return pageData;
	}
	
	@Override
	public List<EnterReportCsbDto> listEnterReport_csb(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		return buyReportDAO.listEnterReport_csb(params);
	}
	
	@Override
	public PageData<EnterDetailReportDto> pageEnterDetailReport(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buyReportDAO.countEnterDetailReport(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<EnterDetailReportDto> list = buyReportDAO.listEnterDetailReport(params);
		PageData<EnterDetailReportDto> pageData = new PageData<EnterDetailReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(buyReportDAO.sumEnterDetailReport(params));
		return pageData;
	}
	
	@Override
	public Map<String, Object> loadEnterDetailSizeReport(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		String pd_code = StringUtil.trimString(params.get("pd_code"));
		List<T_Base_Size> sizes = buyReportDAO.loadProductSize(pd_code, companyid);
		List<EnterDetailSizeReportDto> datas = buyReportDAO.listEnterDetailSizeReport(params);
		for (EnterDetailSizeReportDto dto : datas) {
			String size_amount = dto.getSize_amount();
			if(StringUtil.isEmpty(size_amount)){
				continue;
			}
			String[] sizeAmounts = size_amount.split(",");
			for (String sizeAmount : sizeAmounts) {
				String[] temps = sizeAmount.split(":");
				if(temps.length != 2){
					continue;
				}
				if(dto.getAmountMap().containsKey(temps[0])){
					dto.getAmountMap().put(temps[0], dto.getAmountMap().get(temps[0]) + Integer.parseInt(temps[1]));
				}else {
					dto.getAmountMap().put(temps[0], Integer.parseInt(temps[1]));
				}
			}
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("sizes", sizes);
		resultMap.put("datas", datas);
		return resultMap;
	}
	
	@Override
	public PageData<EnterDetailsDto> pageEnterDetails(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buyReportDAO.countEnterDetails(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<EnterDetailsDto> list = buyReportDAO.listEnterDetails(params);
		PageData<EnterDetailsDto> pageData = new PageData<EnterDetailsDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(buyReportDAO.sumEnterDetails(params));
		return pageData;
	}
	
	@Override
	public PageData<EnterRankDto> pageEnterRank(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = buyReportDAO.countEnterRank(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<EnterRankDto> list = buyReportDAO.listEnterRank(params);
		PageData<EnterRankDto> pageData = new PageData<EnterRankDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(buyReportDAO.sumEnterRank(params));
		return pageData;
	}
	
	@Override
	public List<Map<String, Object>> type_level_enter(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID),"连接超时，请重新登录!");
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		String tp_upcode = StringUtil.trimString(params.get("tp_upcode"));
		List<T_Base_Type> childTypes = buyReportDAO.listTypeByUpCode(tp_upcode, companyid);
		Map<String, String> childTypeMap = new HashMap<String, String>();
		for (T_Base_Type type : childTypes) {
			childTypeMap.put(type.getTp_code(), type.getTp_name());
		}
		List<T_Base_Type> allTypes = buyReportDAO.listAllType(companyid);
		//根据父类编号的直接子类和所有类别，生成直接子类和其所有子类的映射关系
		Map<String, String> tpCode_RootCode = TypeVO.Convert(allTypes, Arrays.asList(childTypeMap.keySet().toArray()));
		List<Map<String, Object>> datas = buyReportDAO.type_level_enter(params);
		Map<String, TypeLevelDto> dtosMap = new HashMap<String, TypeLevelDto>();
		for (T_Base_Type type : childTypes) {
			TypeLevelDto dto = new TypeLevelDto();
			dto.setTp_code(type.getTp_code());
			dto.setTp_name(type.getTp_name());
			dtosMap.put(type.getTp_code(), dto);
		}
		for (Map<String, Object> item : datas) {
			String tp_code = StringUtil.trimString(item.get("tp_code"));
			String month = StringUtil.trimString(item.get("month"));
			Integer amount = Integer.parseInt(item.get("amount").toString());
			Double money = Double.parseDouble(item.get("money").toString());
			if (!childTypeMap.containsKey(tp_code) && tpCode_RootCode.containsKey(tp_code)) {
				tp_code = tpCode_RootCode.get(tp_code);
			}
			TypeLevelDto dto = dtosMap.get(tp_code);
			if (dto == null) {
				continue;
			}
			if(dto.getAmountMap().containsKey(month)){
				dto.getAmountMap().put(month, dto.getAmountMap().get(month)+amount);
			}else {
				dto.getAmountMap().put(month, amount);
			}
			if(dto.getMoneyMap().containsKey(month)){
				dto.getMoneyMap().put(month, dto.getMoneyMap().get(month)+money);
			}else {
				dto.getMoneyMap().put(month, money);
			}
			if(!dto.getChild_type().contains(StringUtil.trimString(item.get("tp_code")))){
				dto.getChild_type().add(StringUtil.trimString(item.get("tp_code")));
			}
		}
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		Map<String, Object> item = null;
		for (T_Base_Type type : childTypes) {
			TypeLevelDto dto = dtosMap.get(type.getTp_code());
			//数量
			item = new HashMap<String, Object>();
			item.put("id", dto.getTp_code()+"_0");
			item.put("tp_code", dto.getTp_code());
			item.put("tp_code_hid", dto.getTp_code());
			item.put("tp_name", dto.getTp_name());
			item.put("project", "数量");
			item.putAll(dto.getAmountMap());
			item.put("total", dto.getTotalAmount());
			item.put("child_type", join(dto.getChild_type()));
			resultList.add(item);
			//金额
			item = new HashMap<String, Object>();
			item.put("id", dto.getTp_code()+"_1");
			item.put("tp_code", dto.getTp_code());
			item.put("tp_code_hid", dto.getTp_code());
			item.put("tp_name", dto.getTp_name());
			item.put("project", "金额");
			item.putAll(dto.getMoneyMap());
			item.put("total", dto.getTotalMoney());
			item.put("child_type", join(dto.getChild_type()));
			resultList.add(item);
		}
		return resultList;
	}
	private String join(List<String> types){
		StringBuffer result = new StringBuffer();
		for (String item : types) {
			result.append(item).append(",");
		}
		if(result.length() > 0){
			result.deleteCharAt(result.length() - 1);
		}
		return result.toString();
	}
	
}
