package zy.service.buy.settle;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.settle.T_Buy_Settle;
import zy.entity.buy.settle.T_Buy_SettleList;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.entity.sys.user.T_Sys_User;

public interface BuySettleService {
	PageData<T_Buy_Settle> page(Map<String, Object> params);
	T_Buy_Settle load(Integer st_id);
	T_Buy_Settle load(String number, Integer companyid);
	T_Buy_Supply loadSupply(String sp_code,Integer companyid);
	List<T_Buy_SettleList> temp_list(Map<String, Object> params);
	void temp_save(String sp_code,T_Sys_User user);
	void temp_updateDiscountMoney(T_Buy_SettleList temp);
	void temp_updatePrepay(T_Buy_SettleList temp);
	void temp_updateRealMoney(T_Buy_SettleList temp);
	void temp_updateRemark(T_Buy_SettleList temp);
	void temp_updateJoin(String ids,String stl_join);
	Map<String, Object> temp_entire(Map<String, Object> params);
	List<T_Buy_SettleList> detail_list(String number,Integer companyid);
	void save(T_Buy_Settle settle, T_Sys_User user);
	void update(T_Buy_Settle settle, T_Sys_User user);
	T_Buy_Settle approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Buy_Settle reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
}
