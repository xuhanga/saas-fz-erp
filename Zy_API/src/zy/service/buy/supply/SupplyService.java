package zy.service.buy.supply;

import java.util.List;
import java.util.Map;

import zy.dto.buy.money.SupplyMoneyDetailsDto;
import zy.entity.PageData;
import zy.entity.buy.supply.T_Buy_Supply;

public interface SupplyService {
	List<T_Buy_Supply> list(Map<String,Object> param);
	void save(T_Buy_Supply supply);
	void del(Integer sp_id,String sp_code,Integer companyid);
	void update(T_Buy_Supply supply);
	T_Buy_Supply queryByID(Integer sp_id);
	PageData<SupplyMoneyDetailsDto> pageMoneyDetails(Map<String, Object> params);
}
