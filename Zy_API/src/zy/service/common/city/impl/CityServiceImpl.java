package zy.service.common.city.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.common.city.CityDAO;
import zy.entity.common.city.Common_City;
import zy.service.common.city.CityService;
@Service
public class CityServiceImpl implements CityService{
	@Resource
	private CityDAO cityDAO;
	@Override
	public List<Common_City> queryProvince() {
		return cityDAO.queryProvince();
	}

	@Override
	public List<Common_City> queryCity(Integer id) {
		return cityDAO.queryCity(id);
	}

	@Override
	public List<Common_City> queryTown(Integer id) {
		return cityDAO.queryTown(id);
	}

}
