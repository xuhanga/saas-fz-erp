package zy.service.common.dict.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.common.dict.CommonDictDAO;
import zy.entity.common.dict.Common_Dict;
import zy.service.common.dict.CommonDictService;

@Service
public class CommonDictServiceImpl implements CommonDictService{
	@Resource
	private CommonDictDAO commonDictDAO;

	@Override
	public List<Common_Dict> list(String dt_type) {
		return commonDictDAO.list(dt_type);
	}
}
