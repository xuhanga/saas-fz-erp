package zy.service.common.kpi.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.common.kpi.CommonKpiDAO;
import zy.entity.common.kpi.Common_Kpi;
import zy.service.common.kpi.CommonKpiService;

@Service
public class CommonKpiServiceImpl implements CommonKpiService{
	@Resource
	private CommonKpiDAO commonKpiDAO;
	
	@Override
	public List<Common_Kpi> list(Map<String, Object> params) {
		return commonKpiDAO.list(params);
	}
	
}
