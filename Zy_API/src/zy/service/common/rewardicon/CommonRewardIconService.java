package zy.service.common.rewardicon;

import java.util.List;

import zy.entity.common.rewardicon.Common_RewardIcon;

public interface CommonRewardIconService {
	List<Common_RewardIcon> list();
}
