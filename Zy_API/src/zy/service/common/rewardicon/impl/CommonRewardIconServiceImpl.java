package zy.service.common.rewardicon.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.common.rewardicon.CommonRewardIconDAO;
import zy.entity.common.rewardicon.Common_RewardIcon;
import zy.service.common.rewardicon.CommonRewardIconService;

@Service
public class CommonRewardIconServiceImpl implements CommonRewardIconService{
	@Resource
	private CommonRewardIconDAO commonRewardIconDAO;

	@Override
	public List<Common_RewardIcon> list() {
		return commonRewardIconDAO.list();
	}
}
