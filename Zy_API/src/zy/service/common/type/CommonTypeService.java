package zy.service.common.type;

import java.util.List;
import java.util.Map;

import zy.entity.common.type.Common_Type;

public interface CommonTypeService {
	List<Common_Type> list(Map<String,Object> param);
	List<Common_Type> sub_list(Map<String,Object> param);
}
