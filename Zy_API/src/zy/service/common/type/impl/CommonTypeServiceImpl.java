package zy.service.common.type.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.common.type.CommonTypeDAO;
import zy.entity.common.type.Common_Type;
import zy.service.common.type.CommonTypeService;
@Service
public class CommonTypeServiceImpl implements CommonTypeService{
	@Resource
	private CommonTypeDAO commonTypeDAO;

	@Override
	public List<Common_Type> list(Map<String, Object> param) {
		return commonTypeDAO.list(param);
	}

	@Override
	public List<Common_Type> sub_list(Map<String, Object> param) {
		return commonTypeDAO.sub_list(param);
	}
	
}
