package zy.service.home;

import java.util.List;
import java.util.Map;

import zy.entity.sell.day.T_Sell_Day;

public interface HomeService {
	Map<String, Object> loadHomeInfo(Map<String, Object> params);
	Map<String, Object> statSellByDay(Map<String, Object> params);
	Map<String, Object> statDayKpi(Map<String, Object> params);
	List<T_Sell_Day> statByHour(Map<String, Object> params);
	Map<String, Object> loadWeather(Map<String, Object> params);
}
