package zy.service.money.expense;

import java.util.List;
import java.util.Map;

import zy.dto.money.expense.ExpenseReportDepartmentDto;
import zy.dto.money.expense.ExpenseReportDto;
import zy.dto.money.expense.ExpenseReportMonthDto;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.expense.T_Money_Expense;
import zy.entity.money.expense.T_Money_ExpenseList;
import zy.entity.sys.user.T_Sys_User;

public interface ExpenseService {
	PageData<T_Money_Expense> page(Map<String, Object> params);
	T_Money_Expense load(Integer ep_id);
	T_Money_Expense load(String number,Integer companyid);
	List<T_Money_ExpenseList> temp_list(Map<String, Object> params);
	void temp_save(List<T_Money_ExpenseList> temps,T_Sys_User user);
	void temp_updateMoney(T_Money_ExpenseList temp);
	void temp_updateSharemonth(T_Money_ExpenseList temp);
	void temp_updateRemark(T_Money_ExpenseList temp);
	void temp_del(Integer epl_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Money_ExpenseList> detail_list(Map<String, Object> params);
	void save(T_Money_Expense expense, T_Sys_User user);
	void update(T_Money_Expense expense, T_Sys_User user);
	T_Money_Expense approve(String number, T_Approve_Record record, T_Sys_User user);
	void approveBatch(String numbers, T_Approve_Record record, T_Sys_User user);
	T_Money_Expense reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String numbers, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
	List<ExpenseReportDto> listReport(Map<String, Object> params);
	List<T_Money_ExpenseList> listReportDetail(Map<String, Object> params);
	List<ExpenseReportMonthDto> expense_head(Map<String, Object> params);
	List<ExpenseReportMonthDto> expense_shop(Map<String, Object> params);
	List<ExpenseReportDepartmentDto> expense_department(Map<String, Object> params);
}
