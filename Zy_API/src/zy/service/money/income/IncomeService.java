package zy.service.money.income;

import java.util.List;
import java.util.Map;

import zy.dto.money.income.IncomeReportDto;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.income.T_Money_Income;
import zy.entity.money.income.T_Money_IncomeList;
import zy.entity.sys.user.T_Sys_User;

public interface IncomeService {
	PageData<T_Money_Income> page(Map<String, Object> params);
	T_Money_Income load(Integer ic_id);
	T_Money_Income load(String number,Integer companyid);
	List<T_Money_IncomeList> temp_list(Map<String, Object> params);
	void temp_save(List<T_Money_IncomeList> temps,T_Sys_User user);
	void temp_updateMoney(T_Money_IncomeList temp);
	void temp_updateRemark(T_Money_IncomeList temp);
	void temp_del(Integer icl_id);
	void temp_clear(Integer us_id,Integer companyid);
	List<T_Money_IncomeList> detail_list(Map<String, Object> params);
	void save(T_Money_Income income, T_Sys_User user);
	void update(T_Money_Income income, T_Sys_User user);
	T_Money_Income approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Money_Income reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user);
	List<IncomeReportDto> listReport(Map<String, Object> params);
	List<T_Money_IncomeList> listReportDetail(Map<String, Object> params);
}
