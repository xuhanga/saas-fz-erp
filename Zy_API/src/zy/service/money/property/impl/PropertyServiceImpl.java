package zy.service.money.property.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.money.property.PropertyDAO;
import zy.entity.money.property.T_Money_Property;
import zy.service.money.property.PropertyService;
import zy.util.CommonUtil;

@Service
public class PropertyServiceImpl implements PropertyService{
	
	@Resource
	private PropertyDAO propertyDAO;

	@Override
	public List<T_Money_Property> list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return propertyDAO.list(param);
	}

	@Override
	public T_Money_Property queryByID(Integer pp_id) {
		return propertyDAO.queryByID(pp_id);
	}

	@Override
	public void save(T_Money_Property property) {
		Integer id = propertyDAO.queryByName(property);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("科目名称["+property.getPp_name()+"]已经存在");
		}
		propertyDAO.save(property);
	}

	@Override
	public void update(T_Money_Property property) {
		if(property == null || property.getPp_id() == null){
			throw new IllegalArgumentException("参数pp_id不能为空");
		}
		Integer id = propertyDAO.queryByName(property);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("科目名称["+property.getPp_name()+"]已经存在");
		}
		propertyDAO.update(property);
	}

	@Override
	public void del(Integer pp_id) {
		T_Money_Property property = propertyDAO.queryByID(pp_id);
		if(property == null){
			throw new IllegalArgumentException("该数据已经删除或不存在");
		}
		if(property.getPp_type().equals(0)){
			Integer useCount = propertyDAO.checkUse_Expense(property.getPp_code(), property.getCompanyid());
			if (useCount > 0) {
				throw new RuntimeException("该费用类型已存在业务关联，不能删除");
			}
		}else if(property.getPp_type().equals(1)){
			Integer useCount = propertyDAO.checkUse_Income(property.getPp_code(), property.getCompanyid());
			if (useCount > 0) {
				throw new RuntimeException("该其他收入已存在业务关联，不能删除");
			}
		}
		propertyDAO.del(pp_id);
	}

}
