package zy.service.report.excel;

public interface ReportDataModelInterface<T> {
	public T getDataModel();
}