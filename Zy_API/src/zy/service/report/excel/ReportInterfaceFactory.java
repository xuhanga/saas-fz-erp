package zy.service.report.excel;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings("rawtypes")
public class ReportInterfaceFactory {
	@Resource
	private ApplicationContext applicationContext;

	@SuppressWarnings("unchecked")
	public <T> T newInstance(Class<T> classType, Map map) {
		AbstractExcelReport fileInterface = (AbstractExcelReport) applicationContext.getBean(classType);
		fileInterface.constructor(map);
		return (T) fileInterface;
	}
}