package zy.service.report.excel.config;

import zy.service.report.excel.AbstractExcelReport;

public abstract class AbstractConfigReport<T> extends AbstractExcelReport<T> {
	protected ReportConfigInterface<T> reportConfigInterface = getReportConfigInterface();

	public abstract ReportConfigInterface<T> getReportConfigInterface();

	@Override
	protected void write() {
		reportConfigInterface.write(workbook);
	}

	@Override
	public T getDataModel() {
		return reportConfigInterface.getDatas();
	}
}