package zy.service.report.excel.config;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFormatWriter<T> implements FormatWriter<T> {
	protected List<RowCallback<T>> rowCallbacks = new ArrayList<RowCallback<T>>();
	protected List<ColumnCallback<T>> columnCallbacks = new ArrayList<ColumnCallback<T>>();

	@Override
	public FormatWriter<T> addRowCallback(RowCallback<T> rowCallback) {
		rowCallbacks.add(rowCallback);
		return this;
	}

	@Override
	public FormatWriter<T> addColumnCallback(ColumnCallback<T> columnCallback) {
		columnCallbacks.add(columnCallback);
		return this;
	}
}