package zy.service.report.excel.config;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public abstract class AbstractMuiltSheetPageDataReportConfig<T> extends AbstractReportConfigAdapter<List<T>> {
	@Override
	public List<T> getDatas() {
		return getData();
	}

	@Override
	public void write(Workbook workbook) {
		String[] sheets = getSheet();
		String[] titles = getTitle();
		String[][] labels = getLabel();
		String[][] names = getName();
		List<T> datas = getData();

		for (int i = 0; i < sheets.length; i++) {
			String sheet = sheets[i];
			String title = titles[i];
			String[] label = labels[i];
			String[] name = names[i];
			T data = datas.get(i);

			Sheet _sheet = workbook.createSheet(sheet != null && !sheet.isEmpty() ? sheet : title);

			writeTitle(_sheet, title, label, name, data);
			writeHead(_sheet, title, label, name, data);
			writeData(_sheet, title, label, name, data);
		}
	}

	public abstract String[] getSheet();

	public abstract String[] getTitle();

	public abstract String[][] getLabel();

	public abstract String[][] getName();

	public abstract List<T> getData();

	public abstract void writeTitle(Sheet sheet, String title, String[] label, String[] name, T data);

	public abstract void writeHead(Sheet sheet, String title, String[] label, String[] name, T data);

	public abstract void writeData(Sheet sheet, String title, String[] label, String[] name, T data);
}