package zy.service.report.excel.config;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public abstract class AbstractSingleSheetPageDataReportConfig<T> extends AbstractReportConfigAdapter<T> {

	@Override
	public T getDatas() {
		return getData();
	}

	@Override
	public void write(Workbook workbook) {
		String sheet = getSheet();
		String title = getTitle();
		String[] label = getLabel();
		String[] name = getName();
		T data = getData();

		Sheet _sheet = workbook.createSheet(sheet != null && !sheet.isEmpty() ? sheet : title);

		writeTitle(_sheet, title, label, name, data);
		writeHead(_sheet, title, label, name, data);
		writeData(_sheet, title, label, name, data);
	}

	public abstract String getSheet();

	public abstract String getTitle();

	public abstract String[] getLabel();

	public abstract String[] getName();

	public abstract T getData();

	public abstract void writeTitle(Sheet sheet, String title, String[] label, String[] name, T data);

	public abstract void writeHead(Sheet sheet, String title, String[] label, String[] name, T data);

	public abstract void writeData(Sheet sheet, String title, String[] label, String[] name, T data);
}