package zy.service.report.excel.config;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;

public abstract class MuiltSheetBeanReportConfig<T> extends AbstractMuiltSheetPageDataReportConfig<List<T>> {
	protected BeanWriter<T> beanWriter;
	
	public MuiltSheetBeanReportConfig(BeanWriter<T> beanWriter) {
		this.beanWriter = beanWriter;
	}

	public void writeTitle(Sheet sheet, String title, String[] label, String[] name, List<T> data) {
		beanWriter.writeTitle(sheet, title, label, name, data);
	}

	public void writeHead(Sheet sheet, String title, String[] label, String[] name, List<T> data) {
		beanWriter.writeHead(sheet, title, label, name, data);
	}

	public void writeData(Sheet sheet, String title, String[] label, String[] name, List<T> data) {
		beanWriter.writeData(sheet, title, label, name, data);
	}
}