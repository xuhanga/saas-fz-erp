package zy.service.report.excel.config;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;

public abstract class SingleSheetBeanReportConfig<T> extends AbstractSingleSheetPageDataReportConfig<List<T>> {
	protected BeanWriter<T> beanWriter;

	public SingleSheetBeanReportConfig(BeanWriter<T> beanWriter) {
		this.beanWriter = beanWriter;
	}

	public void writeTitle(Sheet sheet, String title, String[] label, String[] name, List<T> data) {
		beanWriter.writeTitle(sheet, title, label, name, data);
	}

	public void writeHead(Sheet sheet, String title, String[] label, String[] name, List<T> data) {
		beanWriter.writeHead(sheet, title, label, name, data);
	}

	public void writeData(Sheet sheet, String title, String[] label, String[] name, List<T> data) {
		beanWriter.writeData(sheet, title, label, name, data);
	}
}