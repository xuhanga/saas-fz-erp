package zy.service.report.excel.config;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;

public abstract class SingleSheetMapReportConfig extends AbstractSingleSheetPageDataReportConfig<List<Map<String, Object>>> {
	protected MapWriter mapWriter = new MapWriter();

	public void writeTitle(Sheet sheet, String title, String[] label, String[] name, List<Map<String, Object>> data) {
		mapWriter.writeTitle(sheet, title, label, name, data);
	}

	public void writeHead(Sheet sheet, String title, String[] label, String[] name, List<Map<String, Object>> data) {
		mapWriter.writeHead(sheet, title, label, name, data);
	}

	public void writeData(Sheet sheet, String title, String[] label, String[] name, List<Map<String, Object>> data) {
		mapWriter.writeData(sheet, title, label, name, data);
	}
}