package zy.service.report.excel.reporter;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import zy.entity.base.product.T_Base_Product;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.service.base.product.ProductService;
import zy.service.report.excel.config.BeanWriter;
import zy.service.report.excel.config.ColumnTextFormatter;
import zy.service.report.excel.config.ReportConfigInterface;
import zy.service.report.excel.config.SingleSheetBeanReportConfig;
import zy.service.sort.allot.AllotService;

/**
 * @author: miaosai
 * @date:2018年4月25日 下午9:18:52
 * 
 */

@Component
@SuppressWarnings("unchecked")
@Scope("prototype")
public class AllotReporter extends AbstractFDAReport<List<T_Sort_Allot>>{
	@Resource
	private AllotService allotService;

	@Override
	protected String setFileName() {
		return "配货单" + time;
	}

	@Override
	public ReportConfigInterface<List<T_Sort_Allot>> getReportConfigInterface() {
		BeanWriter<T_Sort_Allot> beanWriter = new BeanWriter<T_Sort_Allot>(T_Sort_Allot.class);
		
		beanWriter.addColumnCallback(new ColumnTextFormatter<T_Sort_Allot>() {
			@Override
			public String column() {
				return "at_ar_state";
			}
			@Override
			public Object value(Object value) {
				if ("0".equals(value.toString())) {
					return "未审核";
				} else if ("1".equals(value.toString())) {
					return "已审核";
				} else if ("2".equals(value.toString())) {
					return "已退回";
				} else if ("3".equals(value.toString())) {
					return "在途";
				} else if ("4".equals(value.toString())) {
					return "完成";
				} else if ("5".equals(value.toString())) {
					return "拒收";
				} else {
					return "非法状态值";
				}
			}
		});

		return new SingleSheetBeanReportConfig<T_Sort_Allot>(beanWriter) {
			@Override
			public String getSheet() {
				return "配货单";
			}

			@Override
			public String getTitle() {
				return "配货单";
			}

			@Override
			public String[] getLabel() {
				return new String[] { "单据编号", "日期", "店铺", "发货仓库", "收货仓库", "状态", "经办人", "申请", "实发", "相差", "申请金额", "实发金额", 
						"实发零售金额", "审核日期", "制单人"};
			}
			
			
			@Override
			public String[] getName() {
				return new String[] { "at_number", "at_date", "shop_name", "outdepot_name", "indepot_name", "at_ar_state", "at_manager", 
						"at_applyamount", "at_sendamount", "at_sendamount", "at_applymoney", "at_sendmoney", "at_sendsellmoney", "at_ar_date", "at_maker"};
			}

			@Override
			public List<T_Sort_Allot> getData() {
				return allotService.listExport(map);
			}
		};
	}
}
