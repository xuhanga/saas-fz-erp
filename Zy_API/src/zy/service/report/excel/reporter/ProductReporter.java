package zy.service.report.excel.reporter;

import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import zy.entity.base.product.T_Base_Product;
import zy.service.base.product.ProductService;
import zy.service.report.excel.config.BeanWriter;
import zy.service.report.excel.config.ColumnTextFormatter;
import zy.service.report.excel.config.ReportConfigInterface;
import zy.service.report.excel.config.SingleSheetBeanReportConfig;

@Component
@SuppressWarnings("unchecked")
@Scope("prototype")
public class ProductReporter extends AbstractFDAReport<List<T_Base_Product>> {
	@Resource
	private ProductService productService;

	@Override
	protected String setFileName() {
		return "商品资料" + time;
	}

	@Override
	public ReportConfigInterface<List<T_Base_Product>> getReportConfigInterface() {
		BeanWriter<T_Base_Product> beanWriter = new BeanWriter<T_Base_Product>(T_Base_Product.class);
		/*beanWriter.addColumnCallback(new ColumnTextFormatter<T_Base_Product>() {
			@Override
			public String column() {
				return "bc_status";
			}
			@Override
			public Object value(Object value) {
				if ("1".equals(value)) {
					return "正常营业";
				} else if ("2".equals(value)) {
					return "停业";
				} else if ("3".equals(value)) {
					return "歇业";
				} else if ("4".equals(value)) {
					return "注销";
				} else if ("5".equals(value)) {
					return "吊证";
				} else {
					return "非法状态值";
				}
			}
		}).addColumnCallback(new ColumnTextFormatter<T_Base_Product>() {
			@Override
			public String column() {
				return "check_times";
			}
			@Override
			public Object value(Object value) {
				return value;
			}
			@Override
			public void setValue(int row, T_Base_Product t, Row dataRow, int column, Object value, String name, Cell dataCell) {
				super.setValue(row, t, dataRow, column, t.getShouldcheck_times() + "/" + t.getCheck_times(), name, dataCell);
			}
		});*/

		return new SingleSheetBeanReportConfig<T_Base_Product>(beanWriter) {
			@Override
			public String getSheet() {
				return "商品资料";
			}

			@Override
			public String getTitle() {
				return "商品资料";
			}

			@Override
			public String[] getLabel() {
				return new String[] { "商品货号", "商品名称", "品牌", "类型", "尺码组", "成本价", "批发价", "进货价", "配送价", "会员价", "零售价", "标牌价", 
						"款式", "单位", "季节", "年份","上市时间","面料","商品款号","创建人","创建日期","修改人","修改日期" };
			}
			
			@Override
			public String[] getName() {
				return new String[] { "pd_no", "pd_name", "pd_bd_name", "pd_tp_name", "pd_szg_name", "pd_cost_price", "pd_batch_price", "pd_buy_price", 
						"pd_sort_price", "pd_vip_price", "pd_sell_price", "pd_sign_price", "pd_style", "pd_unit", "pd_season", "pd_year","pd_date","pd_fabric",
						"pd_number","pd_add_manager","pd_add_date","pd_mod_manager","pd_mod_date"};
			}

			@Override
			public List<T_Base_Product> getData() {
				return productService.productReport(map);
			}
		};
	}
}