package zy.service.sell.allocate;

import java.util.List;
import java.util.Map;

import zy.dto.sell.allocate.SellAllocateReportDto;
import zy.entity.PageData;
import zy.entity.base.product.T_Base_Product;
import zy.entity.sell.allocate.T_Sell_Allocate;
import zy.entity.sell.allocate.T_Sell_AllocateList;
import zy.entity.sell.cashier.T_Sell_Cashier;

public interface SellAllocateService {
	PageData<T_Sell_Allocate> page(Map<String,Object> params);
	T_Sell_Allocate load(Integer ac_id);
	List<T_Sell_AllocateList> detail_list(Map<String, Object> params);
	List<T_Sell_AllocateList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Sell_AllocateList> temp_list(Map<String, Object> params);
	List<T_Sell_AllocateList> temp_sum(Map<String, Object> params);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	void temp_save(List<T_Sell_AllocateList> temps);
	void temp_import(Map<String, Object> params);
	void temp_copy(Map<String, Object> params);
	void temp_updateAmount(T_Sell_AllocateList temp);
	void temp_updateRemarkById(T_Sell_AllocateList temp);
	void temp_updateRemarkByPdCode(T_Sell_AllocateList temp);
	void temp_del(Integer acl_id);
	void temp_delByPiCode(T_Sell_AllocateList temp);
	void temp_clear(Integer us_id,Integer companyid);
	void save(T_Sell_Allocate allocate, T_Sell_Cashier cashier);
	void update(T_Sell_Allocate allocate, T_Sell_Cashier cashier);
	T_Sell_Allocate send(String number,String ac_out_dp, T_Sell_Cashier cashier);
	T_Sell_Allocate receive(String number,String ac_in_dp, T_Sell_Cashier cashier);
	T_Sell_Allocate reject(String number,T_Sell_Cashier cashier);
	T_Sell_Allocate rejectconfirm(String number,T_Sell_Cashier cashier);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode, T_Sell_Cashier cashier);
	PageData<SellAllocateReportDto> pageReport(Map<String, Object> params);
}
