package zy.service.sell.card;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.card.T_Sell_Card;
import zy.entity.sell.card.T_Sell_CardList;

public interface CardService {
	PageData<T_Sell_Card> page(Map<String,Object> params);
	PageData<T_Sell_CardList> pageDetail(Map<String,Object> params);
	T_Sell_Card queryByID(Integer cd_id);
	List<T_Sell_CardList> listDetail(String cd_code,Integer companyid);
	void save(Map<String, Object> params);
	void update(T_Sell_Card card);
	T_Sell_Card recharge(T_Sell_CardList cardList,Integer cd_id);
	void updateState(T_Sell_Card card);
	void del(Integer cd_id);
	
	
	//-----------前台-----------------
	List<T_Sell_Card> listShop(Map<String,Object> param);
	void saveShop(Map<String,Object> param);
	void editPass(Map<String,Object> param);
	T_Sell_Card queryByCode(Map<String,Object> param);
	T_Sell_Card cardByMobile(Map<String,Object> param);
}
