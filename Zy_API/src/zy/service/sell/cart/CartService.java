package zy.service.sell.cart;

import java.util.List;
import java.util.Map;

import zy.entity.sell.cart.T_Sell_Cart;
import zy.entity.sell.cart.T_Sell_CartList;

public interface CartService {
	Map<String, Object> loadCart(Map<String, Object> params);
	List<T_Sell_CartList> listCartList(Map<String, Object> params);
	void updateAmount(Integer scl_amount,Long scl_id);
	void saveCartList(T_Sell_CartList cartList);
	void delCartList(Long scl_id);
	void clearCartList(String em_code,String shop_code,Integer companyid);
	void updateCartListVip(String vm_code,String em_code,String shop_code,Integer companyid);
	void saveCart(T_Sell_Cart cart);
}
