package zy.service.sell.cart.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sell.cart.CartDAO;
import zy.dao.vip.member.MemberDAO;
import zy.entity.sell.cart.T_Sell_Cart;
import zy.entity.sell.cart.T_Sell_CartList;
import zy.entity.vip.member.T_Vip_Member;
import zy.service.sell.cart.CartService;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class CartServiceImpl implements CartService{
	@Resource
	private CartDAO cartDAO;
	@Resource
	private MemberDAO memberDAO;

	@Override
	public Map<String, Object> loadCart(Map<String, Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		String scl_em_code = StringUtil.trimString(params.get("scl_em_code"));
		String scl_shop_code = StringUtil.trimString(params.get("scl_shop_code"));
		Integer companyid = (Integer)params.get("companyid");
		List<T_Sell_CartList> cartLists = cartDAO.temp_list(scl_em_code, scl_shop_code, companyid);
		String vm_code = null;
		for (T_Sell_CartList item : cartLists) {
			if(StringUtil.isNotEmpty(item.getScl_vm_code())){
				vm_code = item.getScl_vm_code();
				break;
			}
		}
		T_Vip_Member member = null;
		if(vm_code != null){
			member = memberDAO.loadByCode(vm_code, companyid);
		}
		resultMap.put("cartLists", cartLists);
		resultMap.put("member", member);
		return resultMap;
	}
	
	@Override
	public List<T_Sell_CartList> listCartList(Map<String, Object> params) {
		String scl_em_code = StringUtil.trimString(params.get("scl_em_code"));
		String scl_shop_code = StringUtil.trimString(params.get("scl_shop_code"));
		Integer companyid = (Integer)params.get("companyid");
		return cartDAO.temp_list(scl_em_code, scl_shop_code, companyid);
	}

	@Override
	@Transactional
	public void updateAmount(Integer scl_amount, Long scl_id) {
		cartDAO.updateAmount(scl_amount, scl_id);
	}
	
	@Override
	@Transactional
	public void saveCartList(T_Sell_CartList cartList) {
		T_Sell_CartList exist = cartDAO.loadCartList(cartList.getScl_sub_code(), cartList.getScl_em_code(), cartList.getScl_shop_code(), cartList.getCompanyid());
		if (exist != null) {
			cartDAO.updateAmount(exist.getScl_amount() + cartList.getScl_amount(), exist.getScl_id());
		}else {
			cartDAO.saveCartList(cartList);
		}
	}
	
	@Override
	@Transactional
	public void delCartList(Long scl_id) {
		cartDAO.delCartList(scl_id);
	}
	
	@Override
	@Transactional
	public void clearCartList(String em_code,String shop_code,Integer companyid) {
		cartDAO.clearCartList(em_code, shop_code, companyid);
	}
	
	@Override
	@Transactional
	public void updateCartListVip(String vm_code,String em_code,String shop_code,Integer companyid) {
		cartDAO.updateCartListVip(vm_code,em_code, shop_code, companyid);
	}
	
	@Override
	@Transactional
	public void saveCart(T_Sell_Cart cart) {
		List<T_Sell_CartList> cartLists = cartDAO.temp_list(cart.getSc_em_code(), cart.getSc_shop_code(), cart.getCompanyid());
		if (cartLists == null || cartLists.size() == 0) {
			throw new RuntimeException("购物车为空");
		}
		String vm_code = null;
		int totalAmount = 0;
		double totalMoney = 0d;
		for (T_Sell_CartList item : cartLists) {
			totalAmount += item.getScl_amount();
			totalMoney += item.getScl_amount() * item.getScl_sell_price();
			if(StringUtil.isNotEmpty(item.getScl_vm_code())){
				vm_code = item.getScl_vm_code();
			}
		}
		cart.setSc_amount(totalAmount);
		cart.setSc_money(totalMoney);
		cart.setSc_vm_code(vm_code);
		cart.setSc_sysdate(DateUtil.getCurrentTime());
		cartDAO.saveCart(cart, cartLists);
	}
	
	
}
