package zy.service.sell.cash.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sell.card.CardDAO;
import zy.dao.sell.cash.CashDAO;
import zy.dao.sell.ecoupon.ECouponDAO;
import zy.dao.sell.voucher.VoucherDAO;
import zy.dao.vip.member.MemberDAO;
import zy.dto.sqb.PayReq;
import zy.dto.sqb.PayResp;
import zy.dto.sqb.QueryReq;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sell.cash.T_Sell_Pay_Temp;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.shop.sale.T_Shop_Sale_Present;
import zy.entity.sys.sqb.T_Sys_Sqb;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.NumberForm;
import zy.form.StringForm;
import zy.form.shop.SaleForm;
import zy.service.sell.cash.CashService;
import zy.service.sqb.SqbHttpProxy;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
import zy.vo.sell.SaleVO;
import zy.vo.sell.SellVO;
@Service
public class CashServiceImpl implements CashService{
	@Resource
	private CashDAO cashDAO;
	@Resource
	private VoucherDAO voucherDAO;
	@Resource
	private CardDAO cardDAO;
	@Resource
	private ECouponDAO couponDAO;
	@Resource
	private MemberDAO memberDAO;
	@Override
	public List<StringForm> putUpList(Map<String, Object> param) {
		return cashDAO.putUpList(param);
	}
	@Override
	public List<T_Sell_Shop_Temp> listTemp(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时，或其他用户登录!");
		}
		return cashDAO.listTemp(param);
	}
	@Transactional
	@Override
	public void updateEmp(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时，或其他用户登录!");
		}
		cashDAO.updateEmp(param);
	}
	@Transactional
	@Override
	public void updateArea(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时，或其他用户登录!");
		}
		cashDAO.updateArea(param);
	}
	@Override
	public T_Sell_Shop_Temp product(Map<String, Object> param){
		T_Sell_Shop_Temp product = cashDAO.product(param);
		if(null != product){
			param.clear();
			param.put("product", product);
		}
		return product;
	}
	/**
	 * 根据货号查询尺码横排数据
	 * 1.查询尺码数据
	 * 2.查询库存数据
	 * 
	 * */
	@Override
	public Map<String, Object> productSize(Map<String, Object> param) {
		Map<String,Object> map = cashDAO.productSize(param);
		if(null == map || map.size() == 0){
			throw new IllegalArgumentException("货号不正确!");
		}
		return SellVO.buildProductInput(map);
	}
	
	@Override
	public PageData<T_Sell_Shop_Temp> pageBack(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		NumberForm map = cashDAO.countBack(param);
		PageInfo pageInfo = new PageInfo(map.getCount(), _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_Shop_Temp> list = cashDAO.listBack(param);
		PageData<T_Sell_Shop_Temp> pageData = new PageData<T_Sell_Shop_Temp>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		Map<String,Object> data = new HashMap<String, Object>(1);
		data.put("sht_amount", map.getNumber());
		pageData.setData(data);
		return pageData;
	}
	@Override
	public PageData<T_Sell_Shop_Temp> pageSell(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		NumberForm map = cashDAO.countSell(param);
		PageInfo pageInfo = new PageInfo(map.getCount(), _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_Shop_Temp> list = cashDAO.listSell(param);
		PageData<T_Sell_Shop_Temp> pageData = new PageData<T_Sell_Shop_Temp>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		Map<String,Object> data = new HashMap<String, Object>(1);
		data.put("sht_amount", map.getNumber());
		pageData.setData(data);
		return pageData;
	}
	
	@Override
	public int subCode(Map<String, Object> param) {
		T_Sell_Shop_Temp temp = cashDAO.subCode(param);
		String sell_type = (String)param.get(CommonUtil.SELL_TYPE);
		if(null != temp){
			temp.setSht_price(temp.getSht_sell_price());
			temp.setSht_money(temp.getSht_amount()*temp.getSht_sell_price());
			if("0".equals(sell_type)){
				saveTemp(temp,param);
			}else if("1".equals(sell_type)){
				saveBackTemp(temp,param);
			}else if("2".equals(sell_type)){
			}
			return 1;
		}else{
			return 0;
		}
	}
	@Override
	public int queryNumber(Map<String, Object> param) {
		Integer id = cashDAO.queryNumber(param);
		if(null != id && id > 0){//是零售单
			return 2;
		}else{
			return subCode(param);
		}
	}
	@Transactional
	@SuppressWarnings("unchecked")
	private void saveBackTemp(T_Sell_Shop_Temp temp,Map<String, Object> param){
		String companyid = StringUtil.trimString(param.get(CommonUtil.COMPANYID));
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		int index = 0;
		if(StringUtil.isEmpty(companyid)){
			throw new IllegalArgumentException("操作超时，请重新登录!");
		}
		//保存新增数据
		cashDAO.saveTemp(temp);
		List<T_Sell_Shop_Temp> list = cashDAO.queryTemp(param);
		if(null != vip_state && "1".equals(vip_state)){
			if(null != vip){//在会员的情况
				index += buildVipRate(list, param);
			}
		}
		if(index > 0){//有打折处理，则更新数据库
			cashDAO.updateTemp(list);
			cashDAO.saveGift(param);
		}
	}
	
	/**
	 * 1.保存新增的货号，可以重复
	 * 2.把保存的数据查出来，进入折扣流程
	 * 4.非促销处理
	 * 5.无会员处理
	 * 6.有会员处理
	 * 7.折上折处理
	 * 8.更新处理
	 * */
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public void saveBackTemp(Map<String, Object> param) {
		String companyid = StringUtil.trimString(param.get(CommonUtil.COMPANYID));
		int index = 0;
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		
		if(StringUtil.isEmpty(companyid)){
			throw new IllegalArgumentException("操作超时，请重新登录!");
		}
		SellVO.buildShopTemp(param);
		List<T_Sell_Shop_Temp> tempList = (List<T_Sell_Shop_Temp>)param.get("tempList");
		if(null != tempList && tempList.size() > 0){
			if(vip == null && !StringUtil.isEmpty(tempList.get(0).getSht_vip_code())){
				param.put("vip_code", tempList.get(0).getSht_vip_code());//key不能变
				vip = queryVip(param);
				param.put(CommonUtil.KEY_VIP, vip);
			}
			//保存新增数据
			cashDAO.saveTemp(tempList);
			//取出所有数据，进入打折流程
			List<T_Sell_Shop_Temp> list = cashDAO.queryTemp(param);
			if(null != vip_state && "1".equals(vip_state)){
				if(null != vip){//在会员的情况
					index += buildVipRate(list, param);
				}
			}
			if(index > 0){//有打折处理，则更新数据库
				cashDAO.updateTemp(list);
			}
		}
	}
	@Transactional
	@SuppressWarnings("unchecked")
	private void saveTemp(T_Sell_Shop_Temp temp,Map<String, Object> param){
		int index = 0;
		String companyid = StringUtil.trimString(param.get(CommonUtil.COMPANYID));
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String issale = (String)setMap.get("KEY_SALE");
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		
		if(StringUtil.isEmpty(companyid)){
			throw new IllegalArgumentException("操作超时，请重新登录!");
		}
		//保存新增数据
		cashDAO.saveTemp(temp);
		//先删除促销的赠品重新计算
		cashDAO.delGift(param);
		//取出所有数据，进入打折流程
		List<T_Sell_Shop_Temp> list = cashDAO.queryTemp(param);
		if(!StringUtil.isEmpty(issale) && "1".equals(issale)){
			//进入促销流程
			index += saleRate(list, param);
		}
		if(null != vip_state && "1".equals(vip_state)){
			if(null != vip){//在会员的情况
				index += buildVipRate(list, param);
			}
		}
		if(index > 0){//有打折处理，则更新数据库
			cashDAO.updateTemp(list);
			cashDAO.saveGift(param);
		}
	}
	/**
	 * 1.保存新增的货号，可以重复
	 * 2.把保存的数据查出来，进入折扣流程
	 * 3.促销处理---还有折上折处理
	 * 4.非促销处理
	 * 5.无会员处理
	 * 6.有会员处理
	 * 7.折上折处理
	 * 8.更新处理
	 * */
	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public void saveTemp(Map<String, Object> param) {
		String companyid = StringUtil.trimString(param.get(CommonUtil.COMPANYID));
		int index = 0;
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String issale = (String)setMap.get("KEY_SALE");
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		
		if(StringUtil.isEmpty(companyid)){
			throw new IllegalArgumentException("操作超时，请重新登录!");
		}
		SellVO.buildShopTemp(param);
		List<T_Sell_Shop_Temp> tempList = (List<T_Sell_Shop_Temp>)param.get("tempList");
		if(null != tempList && tempList.size() > 0){
			//保存新增数据
			cashDAO.saveTemp(tempList);
			//先删除促销的赠品重新计算
			cashDAO.delGift(param);
			//取出所有数据，进入打折流程
			List<T_Sell_Shop_Temp> list = cashDAO.queryTemp(param);
			if(!StringUtil.isEmpty(issale) && "1".equals(issale)){
				//进入促销流程
				index += saleRate(list, param);
			}
			if(null != vip_state && "1".equals(vip_state)){
				if(null != vip){//在会员的情况
					index += buildVipRate(list, param);
				}
			}
			if(index > 0){//有打折处理，则更新数据库
				cashDAO.updateTemp(list);
				cashDAO.saveGift(param);
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public void updateTemp(Map<String, Object> param) {
		Object sht_id = param.get("sht_id");
		Integer sht_amount = NumberUtil.toInteger(param.get("sht_amount"));
		//先删除数据，则进行折扣处理
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		T_Vip_Member member = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		String issale = (String)setMap.get("KEY_SALE");//是否启用促销功能
		int index = 0;
		//先删除促销的赠品重新计算
		cashDAO.delGift(param);
		List<T_Sell_Shop_Temp> tempList = cashDAO.queryTemp(param);
		for(T_Sell_Shop_Temp temp:tempList){
			if(temp.getSht_id() == Long.parseLong(StringUtil.trimString(sht_id))){
				if(1 != temp.getSht_isgift()){
					temp.setSht_amount(sht_amount);
					temp.setSht_money(temp.getSht_price()*sht_amount);
				}else{
					temp.setSht_amount(sht_amount);
				}
				index ++;
			}
		}
		//促销处理,针对该会员的促销处理，如果没有，则进入无促销处理
		if(!StringUtil.isEmpty(issale) && "1".equals(issale)){
			index += saleRate(tempList, param);
		}
		
		/*****无促销流程*****/
		if(null != vip_state && "1".equals(vip_state)){
			if(null != member){//在会员的情况
				index += buildVipRate(tempList, param);
			}
		}
		if(index > 0){
			cashDAO.updateTemp(tempList);
			cashDAO.saveGift(param);
		}
	}
	/**
	 * 促销处理
	 * 1.把当前门店所有促销查出来生成集合
	 * 2.循环临时表进行促销匹配
	 * 3.匹配到的进行相应的模式里打折处理
	 * 
	 * */
	private int saleRate(List<T_Sell_Shop_Temp> list,Map<String,Object> param){
		param.put("today", DateUtil.getYearMonthDate());
		param.put("week", DateUtil.getWeek(DateUtil.getYearMonthDate()));
		List<SaleForm> saleList = cashDAO.querySale(param);
		List<T_Shop_Sale_Present> giftList = null;
		int index = 0;
		if(null != saleList && saleList.size() > 0){//如果有促销方案存在
			List<String> codes = new ArrayList<String>();
			for(SaleForm sale:saleList){
				int scope = SaleVO.buildRule(sale.getSs_model());//获取有赠品的类型
				if(scope == 3 || scope == 4 || scope == 5){//有赠品的促销存在
					codes.add(sale.getSs_code());
				}
			}
			if(codes.size() > 0){//则有赠品，则要查询赠品表
				param.put("codes", codes);
				giftList = cashDAO.queryPersent(param);
			}
			index = SaleVO.buildSale(list,saleList,giftList,param);
		}
		return index;
	}
	public static void main(String[] args) {
		System.out.println(DateUtil.getYearMonthDate());
		try {
			System.out.println(DateUtil.getWeek(DateUtil.getYearMonthDate()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据ID删除临时表
	 * 1.从数据库中把数据删除
	 * 2.查询临时表数据
	 * 3.若是零售，则走促销流程，更改促销数据
	 * 4.会员打折：把促销中更改为不促销的商品进行打折(还是把所有的可打折的重新打折一边)
	 * */
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public void delTemp(Map<String, Object> param) {
		//先删除数据，则进行折扣处理
		cashDAO.delTemp(param);
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		T_Vip_Member member = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		String issale = (String)setMap.get("KEY_SALE");//是否启用促销功能
		String isgift = StringUtil.trimString(param.get("isgift"));
		int index = 0;
		//促销处理,针对该会员的促销处理，如果没有，则进入无促销处理
		if(!StringUtil.isEmpty(issale) && "1".equals(issale) && !"1".equals(isgift)){
			//1.先删除促销的赠品重新计算
			cashDAO.delGift(param);
			//删除赠品后再重新查询，避免重复送赠品
			List<T_Sell_Shop_Temp> tempList = cashDAO.queryTemp(param);
			
			//2.重新计算促销
			index += saleRate(tempList, param);
			if(index > 0){
				/*****会员折扣处理，无促销情况就不再会员打折处理了*****/
				if(null != vip_state && "1".equals(vip_state)){
					if(null != member){//在会员的情况
						index += buildVipRate(tempList, param);
					}
				}
			}
			if(index > 0){
				cashDAO.updateTemp(tempList);
				cashDAO.saveGift(param);
			}
		}
	}
	@Override
	public void clearTemp(Map<String, Object> param) {
		cashDAO.clearTemp(param);
	}
	/**
	 * 添加商品后的会员打折，不计算有过手动打折的商品
	 * 会员打折处理
	 * */
	@Transactional
	@SuppressWarnings("unchecked")
	private int buildVipRate(List<T_Sell_Shop_Temp> list ,Map<String,Object> param){
		int index = 0;//判断是否发生数据变化
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		queryVipRate(param);//取会员的折扣信息
		String vip_rate_type = (String)setMap.get("KEY_VIP_RATE_TYPE");//会员的打折类别1会员类别2会员商品类别3会员商品品牌4商品会员价
		String money_decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
		String money_type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
		String  double_rate = (String)setMap.get("KEY_VIP_DOUBLE_RATE");
		for(T_Sell_Shop_Temp temp:list){
			if(null != temp.getSht_isgift() && 1 == temp.getSht_isgift()){//是赠品，则不用考虑打折情况
				continue;
			}
			double sell_money = temp.getSht_sell_price()*temp.getSht_amount();//零售金额
			if(null != temp.getSht_isvip() && 1 == temp.getSht_isvip()){//商品资料里设置会员是否打折0否1是
				if(!StringUtil.isEmpty(temp.getSht_sale_model())){//有促销情况下，只考虑促销+会员折上折
					if(!StringUtil.isEmpty(double_rate) && "1".equals(double_rate)){
						//特价的情况，不考虑折上折的情况
						if(!"241".equals(temp.getSht_sale_model()) && !"242".equals(temp.getSht_sale_model())){
							if(!"4".equals(vip_rate_type)){
								double vip_rate = SellVO.getVipRate(vip_rate_type, temp, param);
								if(vip_rate > -1){
									double money = NumberUtil.buildMoney(money_decimal, money_type, 
											(sell_money-temp.getSht_sale_money())*vip_rate);
									double vip_money = sell_money-money-temp.getSht_sale_money();
									temp.setSht_price(NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount()));
									temp.setSht_money(money);
									temp.setSht_vip_money(NumberUtil.buildMoney(money_decimal, money_type, vip_money));//会员让利金额
									temp.setSht_vipvalue(vip_rate);
									temp.setSht_viptype(NumberUtil.toInteger(vip_rate_type));
									temp.setSht_hand_money(0d);//未启用折上折，会员打折时，手动让利金额为0
									temp.setSht_handtype(0);//把手动折扣的记录去掉
									temp.setSht_handvalue(0d);
								}
							}
						}
					}
				}else{//无促销情况下
					//考虑之前已经打过手动折和会员折上折的情况,则不处理
					if(null != double_rate && "1".equals(double_rate) && Math.abs(temp.getSht_hand_money()) > 0){//已经手动打折的商品进行折上折处理
						continue;
					}else{//无促销，无手动（折上折），只是会员打折
						if("4".equals(vip_rate_type)){//会员商品价格
							double price = NumberUtil.buildMoney(money_decimal, money_type, temp.getSht_vip_price()) ;//折后价
							double money = NumberUtil.buildMoney(money_decimal, money_type, price*temp.getSht_amount());//折后金额
							temp.setSht_price(price);
							temp.setSht_money(money);
							temp.setSht_vip_money(NumberUtil.buildMoney(money_decimal, money_type, sell_money-money));//会员让利金额
							temp.setSht_sale_money(0d);//未启用折上折，会员打折时，促销金额为0
							temp.setSht_hand_money(0d);//未启用折上折，会员打折时，手动让利金额为0
						}else{
							double vip_rate = SellVO.getVipRate(vip_rate_type, temp, param);
							if(vip_rate > -1){
								temp.setSht_viptype(NumberUtil.toInteger(vip_rate_type));
								SellVO.buildVipRate(temp, money_decimal, money_type, vip_rate);
							}else{
								SellVO.buildNoVipRate(temp, money_decimal, money_type);//
							}
						}
						index ++;
					}
				}
			}
		}
		return index;
	}
	/**
	 * 根据会员类别编号，查出临时表货号对应的折扣
	 * 只能取到会员的折扣，如果设置是商品会员价，则直接取临时表会员价
	 * */
	@SuppressWarnings("unchecked")
	private void queryVipRate(Map<String,Object> param){
		double viprate = 0d;
		boolean isbirthday = false;
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		String vip_rate_type = (String)setMap.get("KEY_VIP_RATE_TYPE");//会员的打折类别1会员类别2会员商品类别3会员商品品牌4商品会员价
		if(null != setMap.get("KEY_VIP_BIRTHDAY") && "1".equals(setMap.get("KEY_VIP_BIRTHDAY"))){//启用了会员打折
			if(null != vip.getIsbirthday() && "1".equals(vip.getIsbirthday())){//如果为0的情况，则不在会员生日折扣范围内
				isbirthday = true;
			}
		}
		if(isbirthday){//
			viprate = Double.parseDouble((String)setMap.get("KEY_VIP_RATE"));
		}else{
			if("1".equals(vip_rate_type)){//会员类别折扣
				viprate = vip.getMt_discount();
			}
			param.put("mt_code", vip.getVm_mt_code());
			if("2".equals(vip_rate_type)){//会员商品类别折扣
				//查询是一个集合，因为要把所有商品对应的类别都查出来
				List<NumberForm> typeList = cashDAO.vipType(param);
				param.put("typeList", typeList);
			}
			if("3".equals(vip_rate_type)){//会员品牌折扣
				//查询是一个集合，因为要把所有商品对应的品牌折扣都查出来
				List<NumberForm> brandList = cashDAO.vipBrand(param);
				param.put("brandList", brandList);
			}
		}
		param.put("vip_rate", viprate);
	}
	/**
	 * 输入会员时，查询会员的信息
	 * */
	@SuppressWarnings("unchecked")
	public T_Vip_Member queryVip(Map<String,Object> param){
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		Integer day = Integer.parseInt((String)setMap.get("KEY_VIP_DAYS"));
		T_Vip_Member member = cashDAO.queryVip(param);
		if(null != member){
			String date = "";
			if(!"1".equals(member.getVm_birthday_type())){
				date = member.getVm_birthday();
			}else{
				date = member.getVm_lunar_birth();
			}
			boolean isBirthday = DateUtil.compareDate(member.getVm_birthday_type(), date, day);
			if(isBirthday){
				member.setIsbirthday("1");
			}else{
				member.setIsbirthday("0");
			}
		}
		return member;
	}
	/**
	 * 会员打折功能
	 * 1.是否有促销存在-有--->促销流程
	 * 2.设置里是否启用会员打折
	 * 3.查询会员的折扣信息
	 * 4.查询临时表
	 * 5.打折处理
	 * 6.更新临时表
	 * */
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public void vipRate(Map<String, Object> param) {
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		T_Vip_Member member = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		String vip_state = (String)setMap.get("KEY_VIP_STATE");//会员打折是否启用
		String issale = (String)setMap.get("KEY_SALE");//是否启用促销功能
		//先删除促销的赠品重新计算
		cashDAO.delGift(param);
		List<T_Sell_Shop_Temp> tempList = cashDAO.queryTemp(param);
		//促销处理,针对该会员的促销处理，如果没有，则进入无促销处理
		if(!StringUtil.isEmpty(issale) && "1".equals(issale)){
			saleRate(tempList, param);
		}
		
		if(!StringUtil.isEmpty(vip_state) && "1".equals(vip_state)){//启用会员打折
			queryVipRate(param);//查询会员的折扣
			String vip_rate_type = (String)setMap.get("KEY_VIP_RATE_TYPE");//会员的打折类别1会员类别2会员商品类别3会员商品品牌4商品会员价
			String money_decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
			String money_type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
			String double_rate = (String)setMap.get("KEY_VIP_DOUBLE_RATE");
			for(T_Sell_Shop_Temp temp:tempList){
				temp.setSht_vip_code(member.getVm_code());
				if(null != member.getVip_other() && "1".equals(member.getVip_other())){
					temp.setSht_ispoint(0);
				}
				if(null != temp.getSht_isgift() && 1 == temp.getSht_isgift()){//是赠品，则不用考虑打折情况
					continue;
				}
				if(null != temp.getSht_isvip() && 1 == temp.getSht_isvip()){//商品资料里设置会员是否打折0否1是
					temp.setSht_viptype(Integer.parseInt(vip_rate_type));
					double vip_rate = SellVO.getVipRate(vip_rate_type, temp, param);
					//折上折处理有两种情况：1.促销+会员2.会员+手动折
					if(!StringUtil.isEmpty(temp.getSht_sale_code())){//存在促销的情况下
						if("1".equals(double_rate)){//会员折上折才会走下流程
							if(!temp.getSht_sale_model().equals("241") && !temp.getSht_sale_model().equals("241")){
								if(!"4".equals(vip_rate_type)){
									SellVO.bulidSaleAndVipRate(temp, money_decimal, money_type, vip_rate);
								}
							}
						}
					}else{
						if("4".equals(vip_rate_type)){//会员商品价格,会员打折类别是商品会员价时，则不进行折上折操作
							double sell_money = temp.getSht_sell_price()*temp.getSht_amount();//零售价
							double price = NumberUtil.buildMoney(money_decimal, money_type, temp.getSht_vip_price()) ;//折后价
							double money = NumberUtil.buildMoney(money_decimal, money_type, price*temp.getSht_amount());//折后金额
							temp.setSht_price(price);
							temp.setSht_money(money);
							temp.setSht_vip_money(NumberUtil.buildMoney(money_decimal, money_type, sell_money-money));//会员让利金额
							temp.setSht_sale_money(0d);//未启用折上折，会员打折时，促销金额为0
							temp.setSht_hand_money(0d);//未启用折上折，会员打折时，手动让利金额为0
							temp.setSht_vipvalue(temp.getSht_vip_price());
							temp.setSht_handtype(0);//把手动折扣的记录去掉
							temp.setSht_handvalue(0d);
						}else{
							if(vip_rate > -1){
								/**
								 * 无促销的情况下，折上折：手动折+会员折，按操作顺序来
								 * */
								if(!StringUtil.isEmpty(double_rate) && "1".equals(double_rate) && !StringUtil.isEmpty(temp.getSht_handtype()) && temp.getSht_handtype() != 0){//折上折
									SellVO.buildVipDoubleRate(temp, money_decimal, money_type, vip_rate);
								}else{
									SellVO.buildVipRate(temp, money_decimal, money_type, vip_rate);
								}
							}else{
								SellVO.buildNoVipRate(temp, money_decimal, money_type);//
							}
						}
					}
				}
			}
		}else{
			for(T_Sell_Shop_Temp temp:tempList){
				temp.setSht_vip_code(member.getVm_code());
				temp.setSht_isvip(1);
				if(null != member.getVip_other() && "1".equals(member.getVip_other())){
					temp.setSht_ispoint(0);
				}
			}
		}
		cashDAO.updateTemp(tempList);
		cashDAO.saveGift(param);
	}
	
	/**
	 * 手动打折
	 * @param param 参数集合
	 * */
	@Transactional
	@Override
	public void handRate(Map<String, Object> param) {
		Object many_rate = param.get("many_rate");
		List<T_Sell_Shop_Temp> tempList = new ArrayList<T_Sell_Shop_Temp>();
		if("1".equals(many_rate.toString())){//所有打折
			List<T_Sell_Shop_Temp> list = cashDAO.queryTemp(param);
			for(T_Sell_Shop_Temp temp:list){
				if(1 == temp.getSht_isgift() || !StringUtil.isEmpty(temp.getSht_sale_code()) || 1 != temp.getSht_ishand()){
					//赠品，有促销，商品不打折不能进行打折处理
					continue;
				}else{
					buildHandRate(temp,param);
					tempList.add(temp);
				}
			}
		}else{//单条打折
			T_Sell_Shop_Temp temp = cashDAO.tempByID(param);
			if(1 == temp.getSht_isgift() || !StringUtil.isEmpty(temp.getSht_sale_code()) || 1 != temp.getSht_ishand()){
				//赠品，有促销，商品不打折不能进行打折处理
				throw new IllegalArgumentException("商品无法打折");
			}else{
				buildHandRate(temp,param);
				tempList.add(temp);
			}
		}
		cashDAO.updateTemp(tempList);
	}
	
	/**
	 * 整单打折。
	 * 1.把需要打折的数据查询出来，除掉赠品
	 * 2.去掉促销和会员折
	 * 3.进行整单折扣计算:让利和实际金额：先计算除了最后一条的让利，最后一条另外计算，保存数据的准备性
	 * 4.更新临时表
	 * */
	@SuppressWarnings("unchecked")
	@Override
	public void allRate(Map<String, Object> param) {
		String hand_type = (String)param.get("hand_type");
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
		String type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
		cashDAO.delGift(param);
		List<T_Sell_Shop_Temp> list = cashDAO.queryTemp(param);
		if(null != list && list.size() > 0){
			int size = list.size(),index = 0;
			double rate_money = 0d;
			for(T_Sell_Shop_Temp temp:list){
				++ index;
				delSale(decimal, type, temp);
				delVipRate(temp);
				delHandRate(temp);
				if(!"1".equals(hand_type)){
					if(index == size){
						buildLast(temp,param,rate_money);
					}else{
						rate_money += buildAllRate(temp, param);
					}
				}else{
					buildAllRate(temp, param);
				}
			}
			cashDAO.updateTemp(list);
		}
	}
	private static void delSale(String decimal,String type,T_Sell_Shop_Temp temp){
		temp.setSht_price(temp.getSht_sell_price());
		temp.setSht_money((NumberUtil.buildMoney(decimal, type,temp.getSht_sell_price()*temp.getSht_amount())));
		temp.setSht_sale_money(0d);
		temp.setSht_sale_code("");
		temp.setSht_sale_model("");
		temp.setSht_remark("");
		temp.setSht_sale_send_money(0d);
	}
	private static void delHandRate(T_Sell_Shop_Temp temp){
		temp.setSht_hand_money(0d);
		temp.setSht_handtype(0);
		temp.setSht_handvalue(0d);
	}
	private static void delVipRate(T_Sell_Shop_Temp temp){
		temp.setSht_vip_money(0d);
		temp.setSht_viptype(0);
		temp.setSht_vipvalue(0d);
	}
	/**
	 * 处理最后一条折扣，保存整单打折，最后结果正确
	 * */
	private void buildLast(T_Sell_Shop_Temp temp,Map<String, Object> param,double rate_money){
		String hand_type = (String)param.get("hand_type");
		double hand_value = Double.parseDouble((String)param.get("hand_value"));
		double total_money = NumberUtil.toDouble(param.get("total_money"));
		double sell_money = NumberUtil.toDouble(temp.getSht_sell_price()*temp.getSht_amount());
		double temp_money = 0d;
		if("2".equals(hand_type.toString())){//让利
			temp_money = hand_value-rate_money;
		}else{
			temp_money = total_money-hand_value-rate_money;
		}
		double money = sell_money-temp_money;
		temp.setSht_handtype(Integer.parseInt(hand_type));
		temp.setSht_handvalue(temp_money);
		if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
			money =	-Math.abs(money);
		}else{
			money = Math.abs(money);
		}
		temp.setSht_vip_money(0d);
		temp.setSht_viptype(0);
		temp.setSht_vipvalue(0d);
		double price = money/temp.getSht_amount();
		temp.setSht_money(money);
		temp.setSht_hand_money(temp_money);
		temp.setSht_price(price);
	}
	/**
	 * 分三种折扣情况
	 * 1.折扣2.让利3.实际
	 * */
	@SuppressWarnings("unchecked")
	private double buildAllRate(T_Sell_Shop_Temp temp,Map<String, Object> param){
		String hand_type = (String)param.get("hand_type");
		double hand_value = Double.parseDouble((String)param.get("hand_value"));
		double total_money = NumberUtil.toDouble(param.get("total_money"));
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String money_decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
		String money_type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
		double sell_money = NumberUtil.toDouble(temp.getSht_sell_price()*temp.getSht_amount());
		double temp_money = 0d;
		if("1".equals(hand_type)){//折扣，只有折扣情况下才会有折上折
			temp.setSht_handtype(Integer.parseInt(hand_type));
			temp.setSht_handvalue(hand_value);
			double money = 0d;
			temp.setSht_viptype(0);
			temp.setSht_vipvalue(0d);
			money = NumberUtil.buildMoney(money_decimal,money_type,
					(sell_money*hand_value));
			double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount());
			temp.setSht_money(money);
			temp.setSht_hand_money(sell_money-money);
			temp.setSht_price(price);
		}else{//非折扣类型
			temp.setSht_handtype(Integer.parseInt(hand_type));
			double money = 0d;
			if("2".equals(hand_type.toString())){//让利
				money = StringUtil.double2Two(((total_money-hand_value)/total_money)*sell_money);
				temp_money = StringUtil.double2Two(sell_money-money);
				temp.setSht_handvalue(temp_money);
				if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
					money =	-Math.abs(money);
				}else{
					money = Math.abs(money);
				}
				temp.setSht_vip_money(0d);
				temp.setSht_viptype(0);
				temp.setSht_vipvalue(0d);
				double price = StringUtil.double2Two(money/temp.getSht_amount());
				temp.setSht_money(money);
				temp.setSht_hand_money(temp_money);
				temp.setSht_price(price);
			}
			if("3".equals(hand_type.toString())){//实际
				money = StringUtil.double2Two(sell_money*hand_value/total_money);
				temp_money = StringUtil.double2Two(sell_money-money);
				temp.setSht_handvalue(temp_money);
				if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
					money = -Math.abs(money);
				}else{
					money = Math.abs(money);
				}
				double price = StringUtil.double2Two(money/temp.getSht_amount());
				temp.setSht_money(money);
				temp.setSht_hand_money(temp_money);
				temp.setSht_price(price);
				temp.setSht_vip_money(0d);
				temp.setSht_viptype(0);
				temp.setSht_vipvalue(0d);
			}
		}
		return temp_money;
	}
	/**
	 * 会员是不是折上折
	 * 分三种折扣情况
	 * 1.折扣2.让利3.实际
	 * */
	@SuppressWarnings("unchecked")
	private void buildHandRate(T_Sell_Shop_Temp temp,Map<String, Object> param){
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		String hand_type = (String)param.get("hand_type");
		double hand_value = Double.parseDouble((String)param.get("hand_value"));
		Integer no_point = Integer.parseInt((String)param.get("no_point"));
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String money_decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
		String money_type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
		String double_rate = (String)setMap.get("KEY_VIP_DOUBLE_RATE");//折上折
		if(null != no_point && 1 == no_point){
			temp.setSht_ispoint(no_point);
		}
		double sell_money = NumberUtil.buildMoney(money_decimal,money_type,
				(temp.getSht_sell_price()*temp.getSht_amount()));
		if("1".equals(hand_type)){//折扣，只有折扣情况下才会有折上折
			temp.setSht_handtype(Integer.parseInt(hand_type));
			temp.setSht_handvalue(hand_value);
			double money = 0d;
			if(null != vip && null != double_rate && "1".equals(double_rate) && 1 == temp.getSht_isvip()){
				if(null != temp.getSht_viptype() && temp.getSht_viptype() != 4){//不是商品会员价打折
					double temp_money = 0d;
					if(temp.getSht_vipvalue() > 0){
						temp_money = sell_money*temp.getSht_vipvalue()*hand_value;
					}else{//会员打折选择为品牌类别折扣或商品类别会员折扣时，没有设置会员对应的类别的折扣，会为0，如果为0则不考虑会员的折扣
						temp_money = sell_money*hand_value;
					}
					money = NumberUtil.buildMoney(money_decimal,money_type,temp_money);
				}else{
					money = NumberUtil.buildMoney(money_decimal,money_type,
							(sell_money-temp.getSht_vipvalue())*hand_value);
				}
			}else{
				temp.setSht_vip_money(0d);
				temp.setSht_viptype(0);
				temp.setSht_vipvalue(0d);
				money = NumberUtil.buildMoney(money_decimal,money_type,
						(sell_money*hand_value));
			}
			double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount());
			temp.setSht_money(money);
			temp.setSht_hand_money(sell_money-money);
			temp.setSht_price(price);
		}else{//非折扣类型
			temp.setSht_handtype(Integer.parseInt(hand_type));
			temp.setSht_handvalue(hand_value);
			if("2".equals(hand_type.toString())){//让利
				double money = 0d;
				if(null != vip && null != double_rate && "1".equals(double_rate) && 1 == temp.getSht_isvip()){
					if(null != temp.getSht_viptype() && temp.getSht_viptype() != 4){//不是商品会员价打折
						if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
							money = NumberUtil.buildMoney(money_decimal,money_type,
									(sell_money*temp.getSht_vipvalue())+hand_value);
						}else{
							money = NumberUtil.buildMoney(money_decimal,money_type,
									(sell_money*temp.getSht_vipvalue())-hand_value);
						}
					}else{
						if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
							money = NumberUtil.buildMoney(money_decimal,money_type,
									(sell_money+hand_value));
						}else{
							money = NumberUtil.buildMoney(money_decimal,money_type,
									(sell_money-hand_value));
						}
					}
				}else{
					if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
						money = NumberUtil.buildMoney(money_decimal,money_type,
								(sell_money+hand_value));
					}else{
						money = NumberUtil.buildMoney(money_decimal,money_type,
							(sell_money-hand_value));
					}
					temp.setSht_vip_money(0d);
					temp.setSht_viptype(0);
					temp.setSht_vipvalue(0d);
				}
				double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount());
				temp.setSht_money(money);
				temp.setSht_hand_money(sell_money-money);
				temp.setSht_price(price);
			}
			if("3".equals(hand_type.toString())){//实际
				double money = NumberUtil.buildMoney(money_decimal,money_type,
						(hand_value));
				if(temp.getSht_amount() < 0){//数量小于0，则是退货，让利应该是加值
					money = -Math.abs(money);
				}else{
					money = Math.abs(money);
				}
				double price = NumberUtil.buildMoney(money_decimal, money_type, money/temp.getSht_amount());
				temp.setSht_money(money);
				temp.setSht_hand_money(sell_money-money);
				temp.setSht_price(price);
				temp.setSht_vip_money(0d);
				temp.setSht_viptype(0);
				temp.setSht_vipvalue(0d);
			}
			if("4".equals(hand_type.toString())){//赠品
				temp.setSht_handtype(0);
				temp.setSht_handvalue(0d);
				temp.setSht_money(0d);
				temp.setSht_hand_money(0d);
				temp.setSht_price(0d);
				temp.setSht_isgift(1);
				temp.setSht_vip_money(0d);
				temp.setSht_viptype(0);
				temp.setSht_vipvalue(0d);
			}
		}
	}
	/**
	 * 提取挂单
	 * 1.清空现有的数据
	 * 2.查询提取的数据
	 * 3.更新挂单的状态
	 * 4.若有会员，则查询会员的信息
	 * */
	@Override
	public T_Vip_Member putDown(Map<String, Object> param) {
		cashDAO.clearTemp(param);
		T_Sell_Shop_Temp temp = cashDAO.putDown(param);
		if(null != temp){
			param.put("vip_code", temp.getSht_vip_code());
			param.put("sell_type", temp.getSht_state());
			return queryVip(param);
		}
		return null;
	}
	@Override
	public void putUp(Map<String, Object> param) {
		cashDAO.putUp(param);
	}
	@Override
	public void to_cash(Map<String, Object> param) {
		cashDAO.to_cash(param);
	}
	
	/**
	 * 1.查询编号
	 * 2.统计主表的金额
	 * 3.保存主表数据和子表数据
	 * 4.各种券的使用处理
	 * 5.库存处理
	 * 6.会员的处理
	 * 7.删除临时表数据
	 * @return map 返回处理的结果
	 * */
	@Transactional
	@Override
	public Map<String, Object> sellCashOut(Map<String, Object> param) {
		String number = cashDAO.cashNumber(param);
		T_Sell_Shop sell_shop = cashDAO.sumTemp(param);
		Map<String,Object> map = new HashMap<String, Object>(2);
		if(null != sell_shop){
			//初始化零售主表
			sell_shop.setSh_number(number);
			param.put("number", number);
			buildSell(sell_shop, param);
			cashDAO.save(sell_shop);
			cashDAO.saveList(param);
			handCards(param);
			handStock(param);
			handVip(param);
			cashDAO.clearTemp(param);
			map.put("sh_id", sell_shop.getSh_id());
		}
		return map;
	}
	/**
	 * 调用支付接口支付
	 * 保存到临时表
	 * 订单编号生成规则：商家编号+店铺编号+时间戳+两位随机数
	 * */
	@Override
	public PayResp doPay(Map<String, Object> param) {
		T_Sys_Sqb sqb = (T_Sys_Sqb)param.get("sqb");
		if(null == sqb){
			throw new IllegalArgumentException("未启用收钱吧!");
		}
		T_Sell_Pay_Temp pay = (T_Sell_Pay_Temp)param.get("pay");
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		String companycode = (String)param.get("companycode");
		String shop_code = (String)param.get(CommonUtil.SHOP_CODE);
		String em_code = (String)param.get(CommonUtil.EMP_CODE);
		String em_name = (String)param.get(CommonUtil.EMP_NAME);
		PayReq payReq = new PayReq();
		pay.setPt_em_code(em_code);
		pay.setPt_number(companycode+shop_code+DateUtil.getCurrentTime("YYMMddHHmmss")+RandomUtils.nextInt(99));
		pay.setCompanyid(companyid);
		pay.setPt_shop_code(shop_code);
		payReq.setTerminal_sn(sqb.getSqb_terminal_sn());
		payReq.setTerminal_key(sqb.getSqb_terminal_key());
		payReq.setClient_sn(pay.getPt_number());
		payReq.setMoney(pay.getPt_money());
		payReq.setSubject(companycode+"-"+shop_code+"-"+em_code);
		payReq.setDynamic_id(pay.getPt_paycode());
		payReq.setOperator(em_name);
		PayResp result = new SqbHttpProxy(CommonUtil.SQB_API_DOMAIN).pay(payReq);
		if(!result.getResult_code().equals("PAY_SUCCESS")){
			cashDAO.savePay(pay);
		}
		return result;
	}
	
	
	@Override
	public List<T_Sell_Pay_Temp> payList(Map<String, Object> param) {
		return cashDAO.payList(param);
	}
	@Override
	public PayResp payTemp(Map<String, Object> param) {
		T_Sys_Sqb sqb = (T_Sys_Sqb)param.get("sqb");
		String pt_number = (String)param.get("pt_number");
		if(null == sqb){
			throw new IllegalArgumentException("未启用收钱吧!");
		}
		QueryReq payReq = new QueryReq();
		payReq.setTerminal_sn(sqb.getSqb_terminal_sn());
		payReq.setClient_sn(pt_number);
		payReq.setTerminal_key(sqb.getSqb_terminal_key());
		payReq.setTerminal_sn(sqb.getSqb_terminal_sn());
		PayResp result = new SqbHttpProxy(CommonUtil.SQB_API_DOMAIN).query(payReq);
		if(result.getResult_code().equals("PAY_SUCCESS")){
			cashDAO.delPay(param);
		}
		return result;
	}
	/**
	 * 1.储值卡处理
	 * 2.代金券处理
	 * 3.优惠券处理
	 * 4.
	 * */
	private void handCards(Map<String, Object> param){
		String sell_type = StringUtil.trimString(param.get(CommonUtil.SELL_TYPE));
		Double ec_money = NumberUtil.toDouble(StringUtil.trimString(param.get("sh_ec_money")));
		String ecu_id = StringUtil.trimString(param.get("ecu_id"));
		String[] cd_ids = (String[])param.get("cd_ids");
		String[] cd_money = (String[])param.get("cd_money");
		String[] vc_ids = (String[])param.get("vc_ids");
		String[] vc_money = (String[])param.get("vc_money");
		//零售下，才会处理优惠券、代金券、订金、
		if(!StringUtil.isEmpty(sell_type) && "0".equals(sell_type)){
			String sd_number = (String)param.get("sd_number");
			if(!StringUtil.isEmpty(sd_number)){//订金
				cashDAO.updateDeposit(param);
			}
			if(null != vc_ids && null != vc_money && vc_ids.length > 0){
				voucherDAO.updateVoucher(param);//代金券
			}
			if(!StringUtil.isEmpty(ecu_id) && null != ec_money && ec_money > 0){
				couponDAO.updateEcoupon(param);
			}
		}
		if(null != cd_ids && null != cd_money && cd_ids.length > 0){
			cardDAO.updateCard(param);
		}
		
	}
	@Transactional
	private void handStock(Map<String, Object> param){
		cashDAO.updateStock(param);
	}
	/**
	 * 1.会员有抵用金额处理
	 * 2.会员的积分处理
	 * 3.会员升级的处理
	 * 4.会员分销处理
	 * */
	@Transactional
	private void handVip(Map<String, Object> param){
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		if(vip != null){
			memberDAO.updateBySell(param);
		}
	}
	
	private void buildSell(T_Sell_Shop sell_shop,Map<String, Object> param){
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		sell_shop.setCompanyid(Integer.parseInt(StringUtil.trimString(param.get(CommonUtil.COMPANYID))));
		sell_shop.setSh_shop_code(StringUtil.trimString(param.get(CommonUtil.SHOP_CODE)));
		sell_shop.setSh_em_code(StringUtil.trimString(param.get(CommonUtil.EMP_CODE)));
		sell_shop.setSh_state(Integer.parseInt(StringUtil.trimString(param.get(CommonUtil.SELL_TYPE))));
		sell_shop.setSh_sysdate(DateUtil.getCurrentTime());
		sell_shop.setSh_date(DateUtil.getYearMonthDate());
		sell_shop.setSh_source(1);
		sell_shop.setSh_st_code(StringUtil.trimString(param.get("st_code")));
		sell_shop.setSh_remark(StringUtil.trimString(param.get("sh_remark")));
		
		sell_shop.setSh_cash(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_cash"))));//现金
		sell_shop.setSh_ali_money(NumberUtil.toDouble(param.get("sh_ali_money")));
		sell_shop.setSh_wx_money(NumberUtil.toDouble(param.get("sh_wx_money")));
		sell_shop.setSh_bank_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_bank_money"))));//银行卡金额
		sell_shop.setSh_vc_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_vc_money"))));//代金券金额
		sell_shop.setSh_vc_rate(NumberUtil.toDouble(StringUtil.trimString(param.get("vc_rate"))));//储值卡金额
		sell_shop.setSh_ec_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_ec_money"))));//优惠券金额
		sell_shop.setSh_point_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_point_money"))));//会员积分抵用金额
		sell_shop.setSh_sd_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_sd_money"))));//订金金额
		sell_shop.setSh_lost_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_lost_money"))));//找零金额
		sell_shop.setSh_mall_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_mall_money"))));//商场卡金额
		sell_shop.setSh_cd_money(NumberUtil.toDouble(StringUtil.trimString(param.get("sh_cd_money"))));//储值卡金额
		sell_shop.setSh_cd_rate(NumberUtil.toDouble(StringUtil.trimString(param.get("cd_rate"))));//储值卡金额
		if(null != vip){
			sell_shop.setSh_vip_code(vip.getVm_code());
			sell_shop.setSh_vip_other(StringUtil.isEmpty(vip.getVip_other())?0:vip.getVip_other());
		}else{
			sell_shop.setSh_vip_code("");
			sell_shop.setSh_vip_other(0);
		}
	}
}
