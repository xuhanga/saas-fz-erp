package zy.service.sell.coupon.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.sell.coupon.CouponDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.coupon.T_Sell_Coupon;
import zy.entity.sell.coupon.T_Sell_Coupon_Detail;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sell.coupon.CouponService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class CouponServiceImpl implements CouponService {
	@Resource
	private CouponDAO couponDAO;
	
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Override
	public PageData<T_Sell_Coupon> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = couponDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_Coupon> list = couponDAO.list(params);
		PageData<T_Sell_Coupon> pageData = new PageData<T_Sell_Coupon>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<T_Sell_Coupon_Detail> listDetail(String sc_number,
			Integer sc_type, Integer companyid) {
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (sc_number == null || "".equals(sc_number)) {
			throw new IllegalArgumentException("参数sc_number不能为null");
		}
		if (sc_type == null || "".equals(sc_type)) {
			throw new IllegalArgumentException("参数sc_type不能为null");
		}
		return couponDAO.listDetail(sc_number,sc_type,companyid);
	}

	@Override
	@Transactional
	public void save(Map<String, Object> params) {
		T_Sell_Coupon coupon = (T_Sell_Coupon)params.get("coupon");
		if(coupon == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		String sc_shop_code = coupon.getSc_shop_code();
		if(StringUtil.isEmpty(sc_shop_code)){
			throw new IllegalArgumentException("请选择活动门店");
		}
		String sc_name = StringUtil.decodeString(coupon.getSc_name());
		if(StringUtil.isEmpty(sc_name)){
			throw new IllegalArgumentException("方案名称不能为空");
		}
		Integer sc_amount = coupon.getSc_amount();
		if(sc_amount == null || sc_amount<=0){
			throw new IllegalArgumentException("请输入发放数量");
		}
		Double sc_minus_money = coupon.getSc_minus_money();
		if(sc_minus_money == null || sc_minus_money<=0){
			throw new IllegalArgumentException("请输入面值多少元");
		}
		Double sc_full_money = coupon.getSc_full_money();
		if(sc_full_money == null || sc_full_money<=0){
			throw new IllegalArgumentException("请输入使用条件，满多少元");
		}
		Integer sc_type = coupon.getSc_type();
		if(sc_type == null){
			throw new IllegalArgumentException("请选择优惠券范围");
		}
		coupon.setSc_name(sc_name);
		coupon.setSc_remark(StringUtil.decodeString(coupon.getSc_remark()));
		params.put("coupon", coupon);
		couponDAO.save(params);
	}

	@Override
	@Transactional
	public void update(Map<String, Object> params) {
		T_Sell_Coupon coupon = (T_Sell_Coupon)params.get("coupon");
		if(coupon == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		String sc_shop_code = coupon.getSc_shop_code();
		if(StringUtil.isEmpty(sc_shop_code)){
			throw new IllegalArgumentException("请选择活动门店");
		}
		String sc_name = StringUtil.decodeString(coupon.getSc_name());
		if(StringUtil.isEmpty(sc_name)){
			throw new IllegalArgumentException("方案名称不能为空");
		}
		Integer sc_amount = coupon.getSc_amount();
		if(sc_amount == null || sc_amount<=0){
			throw new IllegalArgumentException("请输入发放数量");
		}
		Double sc_minus_money = coupon.getSc_minus_money();
		if(sc_minus_money == null || sc_minus_money<=0){
			throw new IllegalArgumentException("请输入面值多少元");
		}
		Double sc_full_money = coupon.getSc_full_money();
		if(sc_full_money == null || sc_full_money<=0){
			throw new IllegalArgumentException("请输入使用条件，满多少元");
		}
		Integer sc_type = coupon.getSc_type();
		if(sc_type == null){
			throw new IllegalArgumentException("请选择优惠券范围");
		}
		coupon.setSc_name(sc_name);
		coupon.setSc_remark(StringUtil.decodeString(coupon.getSc_remark()));
		params.put("coupon", coupon);
		couponDAO.update(params);
	}

	@Override
	@Transactional
	public void del(String sc_number, Integer companyid) {
		if (sc_number == null) {
			throw new IllegalArgumentException("参数sc_number不能为null");
		}
		T_Sell_Coupon coupon = couponDAO.check(sc_number, companyid);
		if(coupon == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(coupon.getSc_state()) && !CommonUtil.AR_STATE_FAIL.equals(coupon.getSc_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		couponDAO.del(sc_number, companyid);
	}

	@Override
	public T_Sell_Coupon load(Integer sc_id) {
		return couponDAO.load(sc_id);
	}

	@Override
	@Transactional
	public T_Sell_Coupon approve(String number, T_Approve_Record record,
			T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Coupon coupon = couponDAO.check(number, user.getCompanyid());
		if(coupon == null){
			throw new RuntimeException("优惠券方案已经不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(coupon.getSc_state())){
			throw new RuntimeException("优惠券方案已经审核");
		}
		//更新优惠券方案审核状态
		coupon.setSc_state(record.getAr_state());
		couponDAO.updateApprove(coupon);
		//保存审核记录表
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sell_coupon");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return coupon;
	}

	@Override
	public T_Sell_Coupon stop(String number, T_Approve_Record record,
			T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_Coupon coupon = couponDAO.check(number, user.getCompanyid());
		if(coupon == null){
			throw new RuntimeException("优惠券方案已经不存在");
		}
		if(!(CommonUtil.AR_STATE_APPROVED.equals(coupon.getSc_state()))){
			throw new RuntimeException("优惠券方案未审核通过或已终止");
		}
		//更新优惠券方案审核状态
		coupon.setSc_state(CommonUtil.AR_STATE_STOP);
		couponDAO.updateApprove(coupon);
		//保存审核记录表
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		record.setAr_state(CommonUtil.AR_STATE_STOP);
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sell_coupon");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return coupon;
	}
}
