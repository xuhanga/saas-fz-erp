package zy.service.sell.day;

import java.util.Map;

import zy.entity.sell.day.T_Sell_Day;
import zy.entity.sell.day.T_Sell_Weather;

public interface DayService {
	T_Sell_Weather weather(Map<String,Object> paramMap);
	T_Sell_Day queryDay(Map<String,Object> paramMap);
	void come(Map<String,Object> paramMap);
	void receive(Map<String,Object> paramMap);
	void comereceive(Map<String, Object> paramMap);
	void doTry(Map<String,Object> paramMap);
	Integer countreceive(Map<String, Object> paramMap);
}
