package zy.service.sell.dayend;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.dayend.T_Sell_DayEnd;

public interface DayEndService {
	void save(Map<String,Object> param);
	T_Sell_DayEnd query(Map<String,Object> param);
	void update(T_Sell_DayEnd dayend,Map<String,Object> param);
	PageData<T_Sell_DayEnd> list(Map<String,Object> params);
	List<T_Sell_DayEnd> listShop(Map<String,Object> paramMap);
	public T_Sell_DayEnd endByID(Integer de_id);
}
