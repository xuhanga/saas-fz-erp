package zy.service.sell.dayend.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.dao.sell.dayend.DayEndDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sell.dayend.T_Sell_DayEnd;
import zy.service.sell.dayend.DayEndService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;
@Service
public class DayEndServiceImpl implements DayEndService{
	@Resource
	private DayEndDAO dayEndDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	@Transactional
	@Override
	public void save(Map<String, Object> param) {
		dayEndDAO.save(param);
	}
	@Override
	public T_Sell_DayEnd query(Map<String, Object> param) {
		return dayEndDAO.query(param);
	}
	@Override
	public void update(T_Sell_DayEnd dayend,Map<String, Object> param) {
		Integer companyid = NumberUtil.toInteger(param.get(CommonUtil.COMPANYID));
		String shop_code = StringUtil.trimString(param.get(CommonUtil.SHOP_CODE));
		if(null != companyid && companyid > 0){
			dayEndDAO.update(dayend);
			if(dayend.getTotal_bank() > 0){
				T_Money_Bank bank = bankDAO.queryByCode(dayend.getDe_in_bank(), companyid);
				Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
				bank.setBa_balance(beginBalance + dayend.getTotal_bank());
				bankDAO.updateBalanceById(bank);
				T_Money_BankRun run = new T_Money_BankRun();
				run.setBr_ba_code(dayend.getDe_in_bank());
				run.setBr_balance(beginBalance + dayend.getTotal_bank());
				run.setBr_bt_code(CommonUtil.BANKRUN_DENEND);
				run.setBr_date(DateUtil.getYearMonthDate());
				run.setBr_enter(dayend.getTotal_bank());
				run.setBr_manager(StringUtil.trimString(param.get(CommonUtil.EMP_NAME)));
				run.setBr_number("");
				run.setBr_shop_code(shop_code);
				run.setBr_sysdate(DateUtil.getCurrentTime());
				run.setCompanyid(companyid);
				bankRunDAO.save(run);
			}
			
			if(dayend.getTotal_cash() > 0){
				T_Money_Bank bank = bankDAO.queryByCode(dayend.getDe_in_cash(), companyid);
				Double beginBalance = bank.getBa_balance() == null ? 0d : bank.getBa_balance();
				bank.setBa_balance(beginBalance + dayend.getTotal_cash());
				bankDAO.updateBalanceById(bank);
				T_Money_BankRun run = new T_Money_BankRun();
				run.setBr_ba_code(dayend.getDe_in_cash());
				run.setBr_balance(beginBalance + dayend.getTotal_cash());
				run.setBr_bt_code(CommonUtil.BANKRUN_DENEND);
				run.setBr_date(DateUtil.getYearMonthDate());
				run.setBr_enter(dayend.getTotal_cash());
				run.setBr_manager(StringUtil.trimString(param.get(CommonUtil.EMP_NAME)));
				run.setBr_number("");
				run.setBr_shop_code(shop_code);
				run.setBr_sysdate(DateUtil.getCurrentTime());
				run.setCompanyid(companyid);
				bankRunDAO.save(run);
			}
		}
	}
	@Override
	public PageData<T_Sell_DayEnd> list(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("时间太长没操作，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = dayEndDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Sell_DayEnd> list = dayEndDAO.list(params);
		PageData<T_Sell_DayEnd> pageData = new PageData<T_Sell_DayEnd>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Override
	public List<T_Sell_DayEnd> listShop(Map<String, Object> paramMap) {
		return dayEndDAO.listShop(paramMap);
	}
	@Override
	public T_Sell_DayEnd endByID(Integer de_id) {
		return dayEndDAO.endByID(de_id);
	}
	
}
