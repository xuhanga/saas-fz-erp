package zy.service.sell.displayarea;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.displayarea.T_Sell_DisplayArea;
import zy.form.StringForm;

public interface DisplayAreaService {
	PageData<T_Sell_DisplayArea> page(Map<String,Object> params);
	T_Sell_DisplayArea queryByID(Integer da_id);
	void save(T_Sell_DisplayArea displayArea);
	void update(T_Sell_DisplayArea displayArea);
	void del(Integer da_id);
	List<StringForm> listShop(Map<String,Object> param);
}
