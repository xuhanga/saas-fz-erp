package zy.service.sell.displayarea.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.sell.displayarea.DisplayAreaDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sell.displayarea.T_Sell_DisplayArea;
import zy.form.StringForm;
import zy.service.sell.displayarea.DisplayAreaService;
import zy.util.CommonUtil;

@Service
public class DisplayAreaServiceImpl implements DisplayAreaService{
	@Resource
	private DisplayAreaDAO displayAreaDAO;

	@Override
	public PageData<T_Sell_DisplayArea> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = displayAreaDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_DisplayArea> list = displayAreaDAO.list(params);
		PageData<T_Sell_DisplayArea> pageData = new PageData<T_Sell_DisplayArea>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Sell_DisplayArea queryByID(Integer da_id) {
		return displayAreaDAO.queryByID(da_id);
	}

	@Override
	public void save(T_Sell_DisplayArea displayArea) {
		displayAreaDAO.save(displayArea);
	}

	@Override
	public void update(T_Sell_DisplayArea displayArea) {
		displayAreaDAO.update(displayArea);
	}

	@Override
	public void del(Integer da_id) {
		displayAreaDAO.del(da_id);
	}

	@Override
	public List<StringForm> listShop(Map<String, Object> param) {
		return displayAreaDAO.listShop(param);
	}
	
}
