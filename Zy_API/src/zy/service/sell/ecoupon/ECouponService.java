package zy.service.sell.ecoupon;

import java.util.List;
import java.util.Map;

import zy.dto.sell.ecoupon.SellECouponDto;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.ecoupon.T_Sell_ECoupon;
import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.sys.user.T_Sys_User;

public interface ECouponService {
	PageData<T_Sell_ECoupon> page(Map<String,Object> params);
	T_Sell_ECoupon load(Integer ec_id);
	List<T_Sell_ECouponList> listDetail(String number,Integer companyid);
	List<T_Sell_ECouponList> temp(Integer us_id,Integer companyid);
	T_Sell_ECouponList temp_load(Integer ecl_id);
	void temp_save(T_Sell_ECouponList temp);
	void temp_update(T_Sell_ECouponList temp);
	void temp_del(Integer ecl_id);
	void save(T_Sell_ECoupon eCoupon,Map<String, Object> params);
	void update(T_Sell_ECoupon eCoupon,Map<String, Object> params);
	T_Sell_ECoupon approve(String number,T_Approve_Record record,T_Sys_User user);
	T_Sell_ECoupon stop(String number,String ec_stop_cause,T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(Integer ec_id);
	PageData<T_Sell_Ecoupon_User> report(Map<String,Object> paramMap);
	
	//前台－－－－－－－－－－－－－－－－－－
	List<T_Sell_Ecoupon_User> queryByCode(Map<String,Object> param);
	void sendCoupon(Map<String,Object> param,T_Sell_Ecoupon_User user);
	
	List<SellECouponDto> listEcoupon4Push(Map<String, Object> params);
	T_Sell_Ecoupon_User checkEcoupon(Map<String,Object> param);
}
