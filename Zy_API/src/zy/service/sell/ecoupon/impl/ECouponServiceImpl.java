package zy.service.sell.ecoupon.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.sell.cash.CashDAO;
import zy.dao.sell.ecoupon.ECouponDAO;
import zy.dto.sell.ecoupon.SellECouponDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.sell.ecoupon.T_Sell_ECoupon;
import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Brand;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Product;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Shop;
import zy.entity.sell.ecoupon.T_Sell_ECoupon_Type;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.service.sell.ecoupon.ECouponService;
import zy.util.CheckCodeUtil;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;

@Service
public class ECouponServiceImpl implements ECouponService{
	@Resource
	private ECouponDAO eCouponDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	@Resource
	private CashDAO cashDAO;

	@Override
	public PageData<T_Sell_ECoupon> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = eCouponDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Sell_ECoupon> list = eCouponDAO.list(params);
		PageData<T_Sell_ECoupon> pageData = new PageData<T_Sell_ECoupon>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Sell_ECoupon load(Integer ec_id) {
		return eCouponDAO.load(ec_id);
	}

	@Override
	public List<T_Sell_ECouponList> listDetail(String number, Integer companyid) {
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		return eCouponDAO.listDetail(number, companyid);
	}

	@Override
	public List<T_Sell_ECouponList> temp(Integer us_id, Integer companyid) {
		return eCouponDAO.temp(us_id, companyid);
	}
	
	@Override
	public T_Sell_ECouponList temp_load(Integer ecl_id) {
		if (ecl_id == null) {
			throw new IllegalArgumentException("参数ecl_id不能为null");
		}
		return eCouponDAO.temp_load(ecl_id);
	}

	@Override
	public void temp_save(T_Sell_ECouponList temp) {
		eCouponDAO.temp_save(temp);
	}
	
	@Override
	public void temp_update(T_Sell_ECouponList temp) {
		if (temp == null || temp.getEcl_id() == null) {
			throw new IllegalArgumentException("参数ecl_id不能为null");
		}
		eCouponDAO.temp_update(temp);
	}

	@Override
	public void temp_del(Integer ecl_id) {
		if (ecl_id == null) {
			throw new IllegalArgumentException("参数ecl_id不能为null");
		}
		eCouponDAO.temp_del(ecl_id);
	}
	
	@Override
	@Transactional
	public void save(T_Sell_ECoupon eCoupon,Map<String, Object> params) {
		T_Sys_User user = (T_Sys_User)params.get("user");
		if(eCoupon == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		String shop_codes = eCoupon.getShop_codes();
		String mode_codes = eCoupon.getMode_codes();
		String use_mode_codes = eCoupon.getUse_mode_codes();
		if(StringUtil.isEmpty(eCoupon.getEc_name())){
			throw new IllegalArgumentException("方案名称不能为空");
		}
		if(StringUtil.isEmpty(shop_codes)){
			throw new IllegalArgumentException("请选择活动门店");
		}
		if(eCoupon.getEc_mode() == null){
			throw new IllegalArgumentException("请选择发放条件");
		}
		if (!eCoupon.getEc_mode().equals(1) && StringUtil.isEmpty(mode_codes)) {
			throw new IllegalArgumentException("请选择具体发放条件");
		}
		if(eCoupon.getEc_use_mode() == null){
			throw new IllegalArgumentException("请选择使用范围");
		}
		if (!eCoupon.getEc_use_mode().equals(1) && StringUtil.isEmpty(use_mode_codes)) {
			throw new IllegalArgumentException("请选择具体使用范围");
		}
		
		List<T_Sell_ECouponList> temps = eCouponDAO.temp(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("电子券方案已保存，请勿重复提交");
		}
		//1.保存主表
		eCoupon.setEc_state(0);
		eCoupon.setEc_sysdate(DateUtil.getYearMonthDate());
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
			eCoupon.setEc_shop_code(user.getUs_shop_code());
		}else if(CommonUtil.THREE.equals(user.getShoptype()) || CommonUtil.FIVE.equals(user.getShoptype())){//自营店、合伙店
			eCoupon.setEc_shop_code(user.getShop_upcode());
		}else if(CommonUtil.FOUR.equals(user.getShoptype())){//加盟店
			eCoupon.setEc_shop_code(user.getUs_shop_code());
		}
		eCouponDAO.save(eCoupon);
		//2.保存明细表
		for (int i = 0; i < temps.size(); i++) {
			temps.get(i).setEcl_number(eCoupon.getEc_number());
			temps.get(i).setEcl_code(eCoupon.getEc_number()+String.format("%03d",(i+1)));
		}
		eCouponDAO.saveList(temps);
		//3.保存活动门店子表
		List<T_Sell_ECoupon_Shop> shops = new ArrayList<T_Sell_ECoupon_Shop>();
		String[] shopcodes = shop_codes.split(",");
		for (String shopcode : shopcodes) {
			T_Sell_ECoupon_Shop itemShop = new T_Sell_ECoupon_Shop();
			itemShop.setCompanyid(user.getCompanyid());
			itemShop.setEcs_ec_number(eCoupon.getEc_number());
			itemShop.setEcs_shop_code(shopcode);
			shops.add(itemShop);
		}
		eCouponDAO.saveShop(shops);
		//4.保存发放条件子表--1.全场2.品牌3.类别4.商品
		if(eCoupon.getEc_type() == 1){//只是前台收银发放的时候才保存发放条件
			if(eCoupon.getEc_mode().equals(2)){
				String[] modecodes = mode_codes.split(",");
				List<T_Sell_ECoupon_Brand> brands = new ArrayList<T_Sell_ECoupon_Brand>();
				for (String modecode : modecodes) {
					T_Sell_ECoupon_Brand itemBrand = new T_Sell_ECoupon_Brand();
					itemBrand.setCompanyid(user.getCompanyid());
					itemBrand.setEcb_ec_number(eCoupon.getEc_number());
					itemBrand.setEcb_bd_code(modecode);
					itemBrand.setEcb_type(1);
					brands.add(itemBrand);
				}
				eCouponDAO.saveBrand(brands);
			}else if(eCoupon.getEc_mode().equals(3)){
				String[] modecodes = mode_codes.split(",");
				List<T_Sell_ECoupon_Type> types = new ArrayList<T_Sell_ECoupon_Type>();
				for (String modecode : modecodes) {
					T_Sell_ECoupon_Type itemType = new T_Sell_ECoupon_Type();
					itemType.setCompanyid(user.getCompanyid());
					itemType.setEct_ec_number(eCoupon.getEc_number());
					itemType.setEct_tp_code(modecode);
					itemType.setEct_type(1);
					types.add(itemType);
				}
				eCouponDAO.saveType(types);
			}else if(eCoupon.getEc_mode().equals(4)){
				String[] modecodes = mode_codes.split(",");
				List<T_Sell_ECoupon_Product> products = new ArrayList<T_Sell_ECoupon_Product>();
				for (String modecode : modecodes) {
					T_Sell_ECoupon_Product itemProduct = new T_Sell_ECoupon_Product();
					itemProduct.setCompanyid(user.getCompanyid());
					itemProduct.setEcp_ec_number(eCoupon.getEc_number());
					itemProduct.setEcp_pd_code(modecode);
					itemProduct.setEcp_type(1);
					products.add(itemProduct);
				}
				eCouponDAO.saveProduct(products);
		}
		}
		
		//5.保存使用范围子表--1.全场2.品牌3.类别4.商品
		if(eCoupon.getEc_use_mode().equals(2)){
			String[] usemodecodes = use_mode_codes.split(",");
			List<T_Sell_ECoupon_Brand> brands = new ArrayList<T_Sell_ECoupon_Brand>();
			for (String modecode : usemodecodes) {
				T_Sell_ECoupon_Brand itemBrand = new T_Sell_ECoupon_Brand();
				itemBrand.setCompanyid(user.getCompanyid());
				itemBrand.setEcb_ec_number(eCoupon.getEc_number());
				itemBrand.setEcb_bd_code(modecode);
				itemBrand.setEcb_type(2);
				brands.add(itemBrand);
			}
			eCouponDAO.saveBrand(brands);
		}else if(eCoupon.getEc_use_mode().equals(3)){
			String[] usemodecodes = use_mode_codes.split(",");
			List<T_Sell_ECoupon_Type> types = new ArrayList<T_Sell_ECoupon_Type>();
			for (String modecode : usemodecodes) {
				T_Sell_ECoupon_Type itemType = new T_Sell_ECoupon_Type();
				itemType.setCompanyid(user.getCompanyid());
				itemType.setEct_ec_number(eCoupon.getEc_number());
				itemType.setEct_tp_code(modecode);
				itemType.setEct_type(2);
				types.add(itemType);
			}
			eCouponDAO.saveType(types);
		}else if(eCoupon.getEc_use_mode().equals(4)){
			String[] usemodecodes = use_mode_codes.split(",");
			List<T_Sell_ECoupon_Product> products = new ArrayList<T_Sell_ECoupon_Product>();
			for (String modecode : usemodecodes) {
				T_Sell_ECoupon_Product itemProduct = new T_Sell_ECoupon_Product();
				itemProduct.setCompanyid(user.getCompanyid());
				itemProduct.setEcp_ec_number(eCoupon.getEc_number());
				itemProduct.setEcp_pd_code(modecode);
				itemProduct.setEcp_type(2);
				products.add(itemProduct);
			}
			eCouponDAO.saveProduct(products);
		}
		//6.删除临时表
		eCouponDAO.temp_del(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Sell_ECoupon eCoupon,Map<String, Object> params) {
		T_Sys_User user = (T_Sys_User)params.get("user");
		if(eCoupon == null || eCoupon.getEc_id() == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		String shop_codes = eCoupon.getShop_codes();
		String mode_codes = eCoupon.getMode_codes();
		String use_mode_codes = eCoupon.getUse_mode_codes();
		if(StringUtil.isEmpty(eCoupon.getEc_name())){
			throw new IllegalArgumentException("方案名称不能为空");
		}
		if(StringUtil.isEmpty(shop_codes)){
			throw new IllegalArgumentException("请选择活动门店");
		}
		if(eCoupon.getEc_mode() == null){
			throw new IllegalArgumentException("请选择发放条件");
		}
		if (!eCoupon.getEc_mode().equals(1) && StringUtil.isEmpty(mode_codes)) {
			throw new IllegalArgumentException("请选择具体发放条件");
		}
		if(eCoupon.getEc_use_mode() == null){
			throw new IllegalArgumentException("请选择使用范围");
		}
		if (!eCoupon.getEc_use_mode().equals(1) && StringUtil.isEmpty(use_mode_codes)) {
			throw new IllegalArgumentException("请选择具体使用范围");
		}
		
		List<T_Sell_ECouponList> temps = eCouponDAO.temp(user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("电子券方案已保存，请勿重复提交");
		}
		T_Sell_ECoupon oldCoupon = eCouponDAO.check(eCoupon.getEc_number(), user.getCompanyid());
		if(oldCoupon == null){
			throw new RuntimeException("电子券方案已经不存在");
		}
		//1.保存主表
		eCoupon.setEc_state(0);
		eCoupon.setEc_sysdate(DateUtil.getYearMonthDate());
		eCouponDAO.update(eCoupon);
		//1.1删除旧数据
		eCouponDAO.deleteList(oldCoupon.getEc_number(), user.getCompanyid());
		eCouponDAO.deleteShop(oldCoupon.getEc_number(), user.getCompanyid());
		if(eCoupon.getEc_mode().equals(2)){
			eCouponDAO.deleteBrand(oldCoupon.getEc_number(), user.getCompanyid());
		}else if(eCoupon.getEc_mode().equals(3)){
			eCouponDAO.deleteType(oldCoupon.getEc_number(), user.getCompanyid());
		}else if(eCoupon.getEc_mode().equals(4)){
			eCouponDAO.deleteProduct(oldCoupon.getEc_number(), user.getCompanyid());
		}
		if(!eCoupon.getEc_use_mode().equals(eCoupon.getEc_mode())){
			if(eCoupon.getEc_use_mode().equals(2)){
				eCouponDAO.deleteBrand(oldCoupon.getEc_number(), user.getCompanyid());
			}else if(eCoupon.getEc_use_mode().equals(3)){
				eCouponDAO.deleteType(oldCoupon.getEc_number(), user.getCompanyid());
			}else if(eCoupon.getEc_use_mode().equals(4)){
				eCouponDAO.deleteProduct(oldCoupon.getEc_number(), user.getCompanyid());
			}
		}
		//2.保存明细表
		for (int i = 0; i < temps.size(); i++) {
			temps.get(i).setEcl_number(eCoupon.getEc_number());
			temps.get(i).setEcl_code(eCoupon.getEc_number()+String.format("%03d",(i+1)));
		}
		eCouponDAO.saveList(temps);
		//3.保存活动门店子表
		List<T_Sell_ECoupon_Shop> shops = new ArrayList<T_Sell_ECoupon_Shop>();
		String[] shopcodes = shop_codes.split(",");
		for (String shopcode : shopcodes) {
			T_Sell_ECoupon_Shop itemShop = new T_Sell_ECoupon_Shop();
			itemShop.setCompanyid(user.getCompanyid());
			itemShop.setEcs_ec_number(eCoupon.getEc_number());
			itemShop.setEcs_shop_code(shopcode);
			shops.add(itemShop);
		}
		eCouponDAO.saveShop(shops);
		//4.保存发放条件子表--1.全场2.品牌3.类别4.商品
		if(eCoupon.getEc_mode().equals(2)){
			String[] modecodes = mode_codes.split(",");
			List<T_Sell_ECoupon_Brand> brands = new ArrayList<T_Sell_ECoupon_Brand>();
			for (String modecode : modecodes) {
				T_Sell_ECoupon_Brand itemBrand = new T_Sell_ECoupon_Brand();
				itemBrand.setCompanyid(user.getCompanyid());
				itemBrand.setEcb_ec_number(eCoupon.getEc_number());
				itemBrand.setEcb_bd_code(modecode);
				itemBrand.setEcb_type(1);
				brands.add(itemBrand);
			}
			eCouponDAO.saveBrand(brands);
		}else if(eCoupon.getEc_mode().equals(3)){
			String[] modecodes = mode_codes.split(",");
			List<T_Sell_ECoupon_Type> types = new ArrayList<T_Sell_ECoupon_Type>();
			for (String modecode : modecodes) {
				T_Sell_ECoupon_Type itemType = new T_Sell_ECoupon_Type();
				itemType.setCompanyid(user.getCompanyid());
				itemType.setEct_ec_number(eCoupon.getEc_number());
				itemType.setEct_tp_code(modecode);
				itemType.setEct_type(1);
				types.add(itemType);
			}
			eCouponDAO.saveType(types);
		}else if(eCoupon.getEc_mode().equals(4)){
			String[] modecodes = mode_codes.split(",");
			List<T_Sell_ECoupon_Product> products = new ArrayList<T_Sell_ECoupon_Product>();
			for (String modecode : modecodes) {
				T_Sell_ECoupon_Product itemProduct = new T_Sell_ECoupon_Product();
				itemProduct.setCompanyid(user.getCompanyid());
				itemProduct.setEcp_ec_number(eCoupon.getEc_number());
				itemProduct.setEcp_pd_code(modecode);
				itemProduct.setEcp_type(1);
				products.add(itemProduct);
			}
			eCouponDAO.saveProduct(products);
		}
		
		//5.保存使用范围子表--1.全场2.品牌3.类别4.商品
		if(eCoupon.getEc_use_mode().equals(2)){
			String[] usemodecodes = use_mode_codes.split(",");
			List<T_Sell_ECoupon_Brand> brands = new ArrayList<T_Sell_ECoupon_Brand>();
			for (String modecode : usemodecodes) {
				T_Sell_ECoupon_Brand itemBrand = new T_Sell_ECoupon_Brand();
				itemBrand.setCompanyid(user.getCompanyid());
				itemBrand.setEcb_ec_number(eCoupon.getEc_number());
				itemBrand.setEcb_bd_code(modecode);
				itemBrand.setEcb_type(2);
				brands.add(itemBrand);
			}
			eCouponDAO.saveBrand(brands);
		}else if(eCoupon.getEc_use_mode().equals(3)){
			String[] usemodecodes = use_mode_codes.split(",");
			List<T_Sell_ECoupon_Type> types = new ArrayList<T_Sell_ECoupon_Type>();
			for (String modecode : usemodecodes) {
				T_Sell_ECoupon_Type itemType = new T_Sell_ECoupon_Type();
				itemType.setCompanyid(user.getCompanyid());
				itemType.setEct_ec_number(eCoupon.getEc_number());
				itemType.setEct_tp_code(modecode);
				itemType.setEct_type(2);
				types.add(itemType);
			}
			eCouponDAO.saveType(types);
		}else if(eCoupon.getEc_use_mode().equals(4)){
			String[] usemodecodes = use_mode_codes.split(",");
			List<T_Sell_ECoupon_Product> products = new ArrayList<T_Sell_ECoupon_Product>();
			for (String modecode : usemodecodes) {
				T_Sell_ECoupon_Product itemProduct = new T_Sell_ECoupon_Product();
				itemProduct.setCompanyid(user.getCompanyid());
				itemProduct.setEcp_ec_number(eCoupon.getEc_number());
				itemProduct.setEcp_pd_code(modecode);
				itemProduct.setEcp_type(2);
				products.add(itemProduct);
			}
			eCouponDAO.saveProduct(products);
		}
		//6.删除临时表
		eCouponDAO.temp_del(user.getUs_id(), user.getCompanyid());
	}
	@Override
	@Transactional
	public T_Sell_ECoupon approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_ECoupon eCoupon = eCouponDAO.check(number, user.getCompanyid());
		if(eCoupon == null){
			throw new RuntimeException("电子券方案已经不存在");
		}
		if(CommonUtil.AR_STATE_APPROVED == eCoupon.getEc_state()){
			throw new RuntimeException("电子券方案已经审核");
		}
		eCoupon.setEc_state(record.getAr_state());
		//更新电子券方案审核状态
		eCouponDAO.approve(eCoupon);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sell_ecoupon");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return eCoupon;
	}
	
	@Override
	@Transactional
	public T_Sell_ECoupon stop(String number,String stop_cause,T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sell_ECoupon eCoupon = eCouponDAO.check(number, user.getCompanyid());
		if(eCoupon == null){
			throw new RuntimeException("电子券方案已经不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(eCoupon.getEc_state())){
			throw new RuntimeException("电子券方案未审核通过或已终止");
		}
		eCoupon.setEc_state(CommonUtil.AR_STATE_STOP);
		//更新电子券方案审核状态
		eCouponDAO.approve(eCoupon);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_STOP);
		record.setAr_describe(stop_cause);
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sell_ecoupon");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return eCoupon;
	}

	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		eCouponDAO.temp_del(us_id, companyid);
		List<T_Sell_ECouponList> details = eCouponDAO.listDetail(number, companyid);
		for(T_Sell_ECouponList item:details){
			item.setEcl_us_id(us_id);
		}
		eCouponDAO.temp_save(details);
	}

	@Override
	@Transactional
	public void del(Integer ec_id) {
		if (ec_id == null) {
			throw new IllegalArgumentException("参数ec_id不能为null");
		}
		T_Sell_ECoupon eCoupon = eCouponDAO.check(ec_id);
		if (eCoupon == null) {
			throw new RuntimeException("电子券方案不存在或已删除！");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(eCoupon.getEc_state()) && !CommonUtil.AR_STATE_FAIL.equals(eCoupon.getEc_state())){
			throw new RuntimeException("电子券方案已审核通过不能删除！");
		}
		eCouponDAO.del(ec_id);
		eCouponDAO.deleteList(eCoupon.getEc_number(), eCoupon.getCompanyid());
		eCouponDAO.deleteShop(eCoupon.getEc_number(), eCoupon.getCompanyid());
		if(eCoupon.getEc_mode().equals(2)){
			eCouponDAO.deleteBrand(eCoupon.getEc_number(), eCoupon.getCompanyid());
		}else if(eCoupon.getEc_mode().equals(3)){
			eCouponDAO.deleteType(eCoupon.getEc_number(), eCoupon.getCompanyid());
		}else if(eCoupon.getEc_mode().equals(4)){
			eCouponDAO.deleteProduct(eCoupon.getEc_number(), eCoupon.getCompanyid());
		}
		if(!eCoupon.getEc_use_mode().equals(eCoupon.getEc_mode())){
			if(eCoupon.getEc_use_mode().equals(2)){
				eCouponDAO.deleteBrand(eCoupon.getEc_number(), eCoupon.getCompanyid());
			}else if(eCoupon.getEc_use_mode().equals(3)){
				eCouponDAO.deleteType(eCoupon.getEc_number(), eCoupon.getCompanyid());
			}else if(eCoupon.getEc_use_mode().equals(4)){
				eCouponDAO.deleteProduct(eCoupon.getEc_number(), eCoupon.getCompanyid());
			}
		}
	}
	

	@Override
	public PageData<T_Sell_Ecoupon_User> report(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("时间太长没操作，请重新登录!");
		}
		Integer pageSize = (Integer)paramMap.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)paramMap.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = eCouponDAO.user_count(paramMap);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		paramMap.put(CommonUtil.START, (pageIndex-1)*pageSize);
		paramMap.put(CommonUtil.END, pageSize);
		
		List<T_Sell_Ecoupon_User> list = eCouponDAO.user_list(paramMap);
		PageData<T_Sell_Ecoupon_User> pageData = new PageData<T_Sell_Ecoupon_User>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<T_Sell_Ecoupon_User> queryByCode(Map<String, Object> param) {
		List<T_Sell_Ecoupon_User> list = eCouponDAO.queryByCode(param);
		if(null != list && list.size() > 0){
			List<T_Sell_Shop_Temp> tempList = cashDAO.queryTemp(param);
			handList(list, tempList,param);
		}
		return list;
	}
	private void handList(List<T_Sell_Ecoupon_User> list,List<T_Sell_Shop_Temp> tempList,Map<String, Object> param){
		for(T_Sell_Ecoupon_User ecoupon:list){
			double money = 0;
			if(ecoupon.getUse_model().equals("1")){//全场
				money = buildMoney(tempList);
			}
			if(ecoupon.getUse_model().equals("2")){//品牌
				money = buildBdMoney(ecoupon.getBd_code(),tempList);
			}
			if(ecoupon.getUse_model().equals("3")){//类别
				money = buildTpMoney(ecoupon.getTp_code(),tempList);
			}
			if(ecoupon.getUse_model().equals("4")){//商品
				money = buildPdMoney(ecoupon.getPd_code(),tempList);
			}
			if(money >= ecoupon.getEcu_limitmoney()){
				ecoupon.setUse_state(1);
				money = 0;
			}
		}
	}
	private Double buildMoney(List<T_Sell_Shop_Temp> tempList){
		double money = 0;
		for(T_Sell_Shop_Temp temp:tempList){
			money += temp.getSht_money();
		}
		return money;
	}
	private Double buildBdMoney(String code,List<T_Sell_Shop_Temp> tempList){
		double money = 0;
		for(T_Sell_Shop_Temp temp:tempList){
			if((","+code+",").indexOf(","+temp.getSht_bd_code()+",") > -1){
				money += temp.getSht_money();
			}
		}
		return money;
	}
	private Double buildTpMoney(String code,List<T_Sell_Shop_Temp> tempList){
		double money = 0;
		for(T_Sell_Shop_Temp temp:tempList){
			if((","+code+",").indexOf(","+temp.getSht_tp_code()+",") > -1){
				money += temp.getSht_money();
			}
		}
		return money;
	}
	private Double buildPdMoney(String code,List<T_Sell_Shop_Temp> tempList){
		double money = 0;
		for(T_Sell_Shop_Temp temp:tempList){
			if((","+code+",").indexOf(","+temp.getSht_pd_code()+",") > -1){
				money += temp.getSht_money();
			}
		}
		return money;
	}
	@Transactional
	@Override
	public void sendCoupon(Map<String, Object> param,T_Sell_Ecoupon_User user) {
		user.setCompanyid(NumberUtil.toInteger(param.get(CommonUtil.COMPANYID)));
		user.setEcu_date(DateUtil.getYearMonthDate());
		user.setEcu_shop_code(StringUtil.trimString(param.get(CommonUtil.SHOP_CODE)));
		user.setEcu_state(0);
		user.setEcu_check_no(CheckCodeUtil.buildCheckCode(4));
		eCouponDAO.save(user);
	}
	@Override
	public T_Sell_Ecoupon_User checkEcoupon(Map<String, Object> param) {
		List<T_Sell_ECoupon> list = eCouponDAO.queryEcoupon(param);
		T_Sell_Ecoupon_User user = null;
		if(null != list && list.size() > 0){
			//此处循环查询，是因为同一店铺优惠券方案内容少，则循环查询，若数据量大，则禁用
			for(T_Sell_ECoupon item:list){
				param.put("ec_number", item.getEc_number());
				if(item.getEc_mode() == 1){//全场
					user = eCouponDAO.allEcoupon(param);
					if(null != user){
						initParam(user, param, item);
						break;
					}
				}
				if(item.getEc_mode() == 2){
					user = handleEcoupon(item.getEc_mode(),param, eCouponDAO.brandEcoupon(param));
					if(null != user){
						initParam(user, param, item);
						break;
					}
				}
				if(item.getEc_mode() == 3){
					user = handleEcoupon(item.getEc_mode(),param, eCouponDAO.typeEcoupon(param));
					if(null != user){
						initParam(user, param, item);
						break;
					}
				}
				if(item.getEc_mode() == 4){
					user = handleEcoupon(item.getEc_mode(),param, eCouponDAO.productEcoupon(param));
					if(null != user){
						initParam(user, param, item);
						break;
					}
				}
			}
		}
		return user;
	}
	private void initParam(T_Sell_Ecoupon_User user,Map<String,Object> param,T_Sell_ECoupon item){
		user.setEcu_sh_number(StringUtil.trimString(param.get("number")));
		user.setEcu_begindate(DateUtil.getYearMonthDate());
		user.setEcu_enddate(StringUtil.trimString(DateUtil.addDate(DateUtil.getCurrentDate() ,item.getEc_deadline())));
		T_Vip_Member vip = (T_Vip_Member)param.get(CommonUtil.KEY_VIP);
		if(null != vip){
			user.setEcu_name(vip.getVm_name());
			user.setEcu_tel(vip.getVm_mobile());
			user.setEcu_vip_code(vip.getVm_code());
		}
	}
	private boolean hadCode(String main,String code){
		if((","+main+",").indexOf(","+code+",") > 0){
			return true;
		}
		return false;
	}
	/**
	 * 1.把优惠券的品牌关联的区间查出来
	 * 2.把收银的表中所有的品牌的金额累计起来
	 * 3.用累计的金额去品牌的优惠方案去识别属于哪个阶段子表
	 * 4.如果有满足条件的，则生成用户领取的优惠券表
	 * */
	@SuppressWarnings("unchecked")
	private T_Sell_Ecoupon_User handleEcoupon(Integer mode,Map<String,Object> param,List<T_Sell_ECouponList> list){
		T_Sell_Ecoupon_User user = null;
		String tmepCode = "";
		if(null != list && list.size() > 0){
			double sell_money = 0d;
			List<T_Sell_ShopList> sellList = (List<T_Sell_ShopList>)param.get("sell_list");
			if(null != sellList && sellList.size() > 0){
				for(T_Sell_ShopList sell:sellList){
					if(2 == mode){//品牌
						tmepCode = sell.getBd_code();
					}
					if(3 == mode){
						tmepCode = sell.getTp_code();
					}
					if(4 == mode){
						tmepCode = sell.getShl_pd_code();
					}
					if(hadCode(list.get(0).getTemp_code(),tmepCode)){
						sell_money += sell.getShl_money();
					}
				}
				if(sell_money > 0){
					double temp = 0d,money = 0d,limit = 0d;
					String code = "";
					for(T_Sell_ECouponList item:list){
						if(item.getEcl_usedmoney() < sell_money){
							if(temp < item.getEcl_usedmoney()){
								temp = item.getEcl_usedmoney();
								money = item.getEcl_money();
								code = item.getEcl_code();
								limit = item.getEcl_limitmoney();
							}
						}
					}
					if(money != 0){
						user = new T_Sell_Ecoupon_User();
						user.setEcu_limitmoney(limit);
						user.setEcu_money(money);
						user.setEcu_code(code);
						user.setEcu_ec_number(list.get(0).getEcl_number());
					}
				}
			}
		}
		return user;
	}
	@Override
	public List<SellECouponDto> listEcoupon4Push(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get(CommonUtil.SHOP_CODE), "参数shop_code不能为null");
		Assert.notNull(params.get(CommonUtil.SHOP_TYPE), "参数shop_type不能为null");
		Assert.notNull(params.get(CommonUtil.PAGESIZE), "参数pageSize不能为null");
		Assert.notNull(params.get(CommonUtil.PAGEINDEX), "参数pageIndex不能为null");
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return eCouponDAO.listEcoupon4Push(params);
	}
	
	
}
