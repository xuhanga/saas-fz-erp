package zy.service.sell.sell;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;

public interface SellService {
	PageData<T_Sell_Shop> pageShop(Map<String,Object> param);
	PageData<T_Sell_ShopList> pageShopList(Map<String,Object> param);
	Map<String,Object> print(Map<String,Object> param);
}
