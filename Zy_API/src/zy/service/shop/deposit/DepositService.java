package zy.service.shop.deposit;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.sell.deposit.T_Sell_Deposit;
import zy.entity.sell.deposit.T_Sell_DepositList;

public interface DepositService {
	PageData<T_Sell_DepositList> page(Map<String,Object> param);
	void payDeposit(Map<String,Object> param);
	List<T_Sell_Deposit> getDeposit(Map<String,Object> param);
	void take(Map<String,Object> param);
	void back(Map<String,Object> param);
}
