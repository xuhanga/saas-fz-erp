package zy.service.shop.plan;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.plan.T_Shop_MonthPlan;
import zy.entity.shop.plan.T_Shop_MonthPlan_Day;
import zy.entity.sys.user.T_Sys_User;

public interface MonthPlanService {
	PageData<T_Shop_MonthPlan> page(Map<String, Object> params);
	T_Shop_MonthPlan load(Integer mp_id);
	T_Shop_MonthPlan check(String shop_code, Integer year,Integer month, Integer companyid);
	void save(T_Shop_MonthPlan plan,List<T_Shop_MonthPlan_Day> days, T_Sys_User user);
	List<T_Shop_MonthPlan_Day> listDay(String number,Integer companyid);
	T_Shop_MonthPlan approve(String number, T_Approve_Record record, T_Sys_User user);
	void del(String number, Integer companyid);
}
