package zy.service.shop.plan;

import java.util.List;
import java.util.Map;

import zy.dto.shop.plan.PlanChartDto;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.plan.T_Shop_Plan;
import zy.entity.shop.plan.T_Shop_Plan_Expense;
import zy.entity.shop.plan.T_Shop_Plan_Month;
import zy.entity.sys.user.T_Sys_User;

public interface PlanService {
	PageData<T_Shop_Plan> page(Map<String, Object> params);
	List<T_Shop_Plan_Expense> listExpense(String pe_number,Integer companyid);
	List<T_Shop_Plan_Month> listMonth(String pm_number,Integer companyid);
	T_Shop_Plan load(Integer pl_id);
	T_Shop_Plan check(String shop_code,Integer year,Integer companyid);
	T_Shop_Plan loadPreData(String shop_code,Integer year,Integer companyid);
	List<Map<String, Object>> loadPreDataMonth(String shop_code, Integer year, Integer companyid);
	List<Map<String, Object>> loadPreDataDay(String shop_code,Integer year,Integer month, Integer companyid);
	void save(T_Shop_Plan plan,List<T_Shop_Plan_Month> planMonths,List<T_Shop_Plan_Expense> planExpenses, T_Sys_User user);
	T_Shop_Plan approve(String number, T_Approve_Record record, T_Sys_User user);
	void del(String number, Integer companyid);
	List<PlanChartDto> chartByShop(Map<String, Object> params);
	List<PlanChartDto> chartByMonth(Map<String, Object> params);
	Map<String, Object> statByYearMonth(Map<String, Object> params);
	T_Shop_Plan_Month loadPlanMonth(String shop_code,Integer year,Integer month,Integer companyid);
}
