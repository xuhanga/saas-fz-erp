package zy.service.shop.plan.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.shop.plan.MonthPlanDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.plan.T_Shop_MonthPlan;
import zy.entity.shop.plan.T_Shop_MonthPlan_Day;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.plan.MonthPlanService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class MonthPlanServiceImpl implements MonthPlanService{
	@Resource
	private MonthPlanDAO monthPlanDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Override
	public PageData<T_Shop_MonthPlan> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = monthPlanDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Shop_MonthPlan> list = monthPlanDAO.list(params);
		PageData<T_Shop_MonthPlan> pageData = new PageData<T_Shop_MonthPlan>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Shop_MonthPlan load(Integer mp_id) {
		return monthPlanDAO.load(mp_id);
	}
	
	@Override
	public T_Shop_MonthPlan check(String shop_code, Integer year,Integer month, Integer companyid) {
		return monthPlanDAO.check(shop_code, year, month, companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Shop_MonthPlan plan,List<T_Shop_MonthPlan_Day> days, T_Sys_User user) {
		if(StringUtil.isEmpty(plan.getMp_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if (days == null || days.size() == 0) {
			throw new IllegalArgumentException("请将计划分配到天");
		}
		plan.setMp_number(CommonUtil.NUMBER_PREFIX_SHOP_MONTHPLAN+plan.getMp_shop_code()+plan.getMp_year()+String.format("%02d",plan.getMp_month()));
		plan.setMp_sysdate(DateUtil.getCurrentTime());
		plan.setMp_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		plan.setMp_maker(user.getUs_name());
		plan.setMp_us_id(user.getUs_id());
		plan.setCompanyid(user.getCompanyid());
		T_Shop_MonthPlan existPlan = monthPlanDAO.check(plan.getMp_number(), plan.getCompanyid());
		if(existPlan != null){
			throw new IllegalArgumentException("该店铺月计划已制作，请勿重复提交");
		}
		monthPlanDAO.save(plan, days);
	}
	
	@Override
	public List<T_Shop_MonthPlan_Day> listDay(String number,Integer companyid) {
		return monthPlanDAO.listDay(number, companyid);
	}

	@Override
	@Transactional
	public T_Shop_MonthPlan approve(String number, T_Approve_Record record, T_Sys_User user) {
		T_Shop_MonthPlan plan = monthPlanDAO.check(number, user.getCompanyid());
		if(plan == null){
			throw new RuntimeException("计划不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(plan.getMp_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//更新单据审核状态
		plan.setMp_ar_state(record.getAr_state());
		plan.setMp_ar_date(DateUtil.getCurrentTime());
		monthPlanDAO.updateApprove(plan);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_monthplan");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return plan;
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		T_Shop_MonthPlan plan = monthPlanDAO.check(number, companyid);
		if(plan == null){
			throw new RuntimeException("计划不存在");
		}
		
		if(CommonUtil.AR_STATE_APPROVED.equals(plan.getMp_ar_state())){
			throw new RuntimeException("计划已审核通过不能删除！");
		}
		monthPlanDAO.del(number, companyid);
	}
	
}
