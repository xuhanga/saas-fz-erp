package zy.service.shop.plan.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.shop.plan.PlanDAO;
import zy.dto.shop.plan.PlanChartDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.plan.T_Shop_Plan;
import zy.entity.shop.plan.T_Shop_Plan_Expense;
import zy.entity.shop.plan.T_Shop_Plan_Month;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.plan.PlanService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class PlanServiceImpl implements PlanService{
	@Resource
	private PlanDAO planDAO;
	
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Override
	public PageData<T_Shop_Plan> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = planDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Shop_Plan> list = planDAO.list(params);
		PageData<T_Shop_Plan> pageData = new PageData<T_Shop_Plan>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public List<T_Shop_Plan_Expense> listExpense(String pe_number,Integer companyid) {
		return planDAO.listExpense(pe_number, companyid);
	}
	
	@Override
	public List<T_Shop_Plan_Month> listMonth(String pm_number,Integer companyid) {
		return planDAO.listMonth(pm_number, companyid);
	}
	
	@Override
	public T_Shop_Plan load(Integer pl_id) {
		return planDAO.load(pl_id);
	}

	@Override
	public T_Shop_Plan check(String shop_code, Integer year, Integer companyid) {
		return planDAO.check(shop_code, year, companyid);
	}
	
	@Override
	public T_Shop_Plan loadPreData(String shop_code, Integer year, Integer companyid) {
		return planDAO.loadPreData(shop_code, year, companyid);
	}

	@Override
	public List<Map<String, Object>> loadPreDataMonth(String shop_code,Integer year, Integer companyid) {
		return planDAO.loadPreDataMonth(shop_code, year, companyid);
	}
	
	@Override
	public List<Map<String, Object>> loadPreDataDay(String shop_code,Integer year,Integer month, Integer companyid) {
		return planDAO.loadPreDataDay(shop_code, year,month, companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Shop_Plan plan,List<T_Shop_Plan_Month> planMonths,List<T_Shop_Plan_Expense> planExpenses, T_Sys_User user) {
		if(StringUtil.isEmpty(plan.getPl_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(plan.getPl_name())){
			throw new IllegalArgumentException("计划名称不能为空");
		}
		if (planMonths == null || planMonths.size() == 0) {
			throw new IllegalArgumentException("请将计划分配到月");
		}
		if (plan.getPl_type().equals(1)) {
			if (planExpenses == null || planExpenses.size() == 0) {
				throw new IllegalArgumentException("请输入费用金额");
			}
		}
		if (plan.getPl_sell_money() == null || plan.getPl_sell_money().equals(0d)) {
			throw new IllegalArgumentException("请设置目标金额");
		}
		plan.setPl_sysdate(DateUtil.getCurrentTime());
		plan.setPl_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		plan.setPl_maker(user.getUs_name());
		plan.setPl_us_id(user.getUs_id());
		plan.setCompanyid(user.getCompanyid());
		planDAO.save(plan, planMonths, planExpenses);
	}
	
	@Override
	@Transactional
	public T_Shop_Plan approve(String number, T_Approve_Record record, T_Sys_User user) {
		T_Shop_Plan plan = planDAO.check(number, user.getCompanyid());
		if(plan == null){
			throw new RuntimeException("计划不存在");
		}
		if(!CommonUtil.WANT_AR_STATE_NOTAPPROVE.equals(plan.getPl_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		
		//更新单据审核状态
		plan.setPl_ar_state(record.getAr_state());
		plan.setPl_ar_date(DateUtil.getCurrentTime());
		planDAO.updateApprove(plan);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_plan");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		return plan;
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		T_Shop_Plan plan = planDAO.check(number, companyid);
		if(plan == null){
			throw new RuntimeException("计划不存在");
		}
		
		if(CommonUtil.AR_STATE_APPROVED.equals(plan.getPl_ar_state())){
			throw new RuntimeException("计划已审核通过不能删除！");
		}
		planDAO.del(number, companyid);
	}
	
	@Override
	public List<PlanChartDto> chartByShop(Map<String, Object> params) {
		return planDAO.chartByShop(params);
	}
	
	@Override
	public List<PlanChartDto> chartByMonth(Map<String, Object> params) {
		return planDAO.chartByMonth(params);
	}
	
	@Override
	public Map<String, Object> statByYearMonth(Map<String, Object> params) {
		return planDAO.statByYearMonth(params);
	}
	
	@Override
	public T_Shop_Plan_Month loadPlanMonth(String shop_code,Integer year,Integer month,Integer companyid) {
		return planDAO.loadPlanMonth(shop_code, year, month, companyid);
	}
	
}
