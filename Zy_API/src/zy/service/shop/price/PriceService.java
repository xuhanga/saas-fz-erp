package zy.service.shop.price;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.price.T_Shop_Price;
import zy.entity.shop.price.T_Shop_PriceList;
import zy.entity.sys.user.T_Sys_User;

public interface PriceService {
	PageData<T_Shop_Price> page(Map<String,Object> param);
	List<T_Shop_PriceList> price_list(Map<String, Object> param);
	List<T_Shop_PriceList> temp_list(Map<String, Object> param);
	void save_temp_list(Map<String, Object> params);
	String save_tempList_Enter(Map<String, Object> params);
	void shop_change_templist(Map<String, Object> params);
	void update_templist_byPdCode(Map<String, Object> params);
	void savePrice_templist(Map<String, Object> params);
	void savePoint_templist(Map<String, Object> params);
	void save(T_Shop_Price shop_Price,T_Sys_User user);
	void update(T_Shop_Price shop_Price,T_Sys_User user);
	void temp_clear(T_Sys_User user);
	void temp_del(Integer spl_id);
	T_Shop_Price load(Integer sp_id);
	T_Shop_Price approve(String number, T_Approve_Record record, T_Sys_User user);
	void del(String number, Integer companyid);
	void initUpdate(String number,Integer us_id, Integer companyid);
}
