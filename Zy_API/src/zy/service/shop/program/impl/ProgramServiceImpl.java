package zy.service.shop.program.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.shop.program.ProgramDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.shop.program.T_Shop_Program;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.program.ProgramService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Service
public class ProgramServiceImpl implements ProgramService{
	
	@Resource
	private ProgramDAO programDAO;
	
	@Override
	public PageData<T_Shop_Program> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = programDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Program> list = programDAO.list(param);
		PageData<T_Shop_Program> pageData = new PageData<T_Shop_Program>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	@Transactional
	public void save(T_Shop_Program program, T_Sys_User user) {
		if(user == null){
			throw new IllegalArgumentException("请重新登录");
		}
		if (program == null) {
			throw new IllegalArgumentException("参数不能为null");
		}
		String sp_shop_code = program.getSp_shop_code();
		String sp_title = program.getSp_title();
		String sp_info = program.getSp_info();
		String sp_imgpath = program.getSp_imgpath();
		if(sp_shop_code == null || "".equals(sp_shop_code)){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(sp_title == null || "".equals(sp_title)){
			throw new IllegalArgumentException("方案标题不能为空");
		}
		if(sp_info == null || "".equals(sp_info)){
			throw new IllegalArgumentException("方案说明不能为空");
		}
		if(sp_imgpath == null || "".equals(sp_imgpath)){
			throw new IllegalArgumentException("图片路径不能为空");
		}
		
		program.setCompanyid(user.getCompanyid());
		program.setSp_us_id(user.getUs_id());
		program.setSp_us_name(user.getUs_name());
		program.setSp_sysdate(DateUtil.getCurrentTime());
		
		programDAO.save(program); 
	}

	@Override
	@Transactional
	public void del(Integer sp_id,Integer sps_id) {
		programDAO.del(sp_id,sps_id);
	}

	@Override
	public T_Shop_Program load(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object sp_id = param.get("sp_id");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (sp_id == null) {
			throw new IllegalArgumentException("参数sp_id不能为null");
		}
		return programDAO.load(param);
	}

	@Override
	@Transactional
	public T_Shop_Program load_cash(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Integer sps_id = (Integer)param.get("sps_id");
		Object sp_id = param.get("sp_id");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (sps_id == null) {
			throw new IllegalArgumentException("参数sps_id不能为null");
		}
		if (sp_id == null) {
			throw new IllegalArgumentException("参数sp_id不能为null");
		}
		T_Shop_Program t_Shop_Program = programDAO.load(param);
		if(t_Shop_Program!=null){
			programDAO.update_state(sps_id);
		}
		return t_Shop_Program;
	}
}
