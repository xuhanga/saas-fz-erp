package zy.service.shop.sale;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.shop.sale.T_Shop_Sale;
import zy.entity.sys.user.T_Sys_User;

public interface SaleService {
	PageData<T_Shop_Sale> page(Map<String,Object> param);
	Integer queryPriority(Map<String,Object> param);
	void save(Map<String,Object> param);
	Map<String,Object> load(Map<String,Object> param);
	T_Shop_Sale approve(String code,T_Approve_Record record,T_Sys_User user);
	T_Shop_Sale stop(String code,String ec_stop_cause,T_Sys_User user);
	void del(String ss_code, Integer companyid);
	List<T_Shop_Sale> list(Map<String,Object> param);
	PageData<T_Sell_ShopList> rateList(Map<String,Object> paramMap);
	PageData<T_Sell_ShopList> totalList(Map<String,Object> paramMap);
}
