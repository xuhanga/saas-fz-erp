package zy.service.shop.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.shop.service.ServiceDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.service.T_Shop_Service;
import zy.entity.shop.service.T_Shop_Service_Item;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.service.ServiceService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class ServiceServiceImpl implements ServiceService {
	@Resource
	private ServiceDAO serviceDAO;
	
	@Override
	public PageData<T_Shop_Service> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = serviceDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Service> list = serviceDAO.list(param);
		PageData<T_Shop_Service> pageData = new PageData<T_Shop_Service>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public void save(T_Shop_Service service, T_Sys_User user) {
		if(service == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(service.getSs_tel())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_name())){
			throw new IllegalArgumentException("顾客姓名不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_manager())){
			throw new IllegalArgumentException("受理人员不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_startdate())){
			throw new IllegalArgumentException("接受日期不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_pd_no())){
			throw new IllegalArgumentException("商品货号不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_shop_code())){
			throw new IllegalArgumentException("维修店铺不能为空");
		}
		service.setCompanyid(user.getCompanyid());
		service.setSs_state(0);
		service.setSs_sms_count(0);
		service.setSs_ca_emcode(user.getUs_code());
		service.setSs_sysdate(DateUtil.getCurrentTime());
		service.setSs_tel(StringUtil.decodeString(service.getSs_tel()));
		service.setSs_name(StringUtil.decodeString(service.getSs_name()));
		service.setSs_manager(StringUtil.decodeString(service.getSs_manager()));
		service.setSs_pd_no(StringUtil.decodeString(service.getSs_pd_no()));
		service.setSs_question(StringUtil.decodeString(service.getSs_question()));
		serviceDAO.save(service);
	}

	@Override
	public void save(T_Shop_Service service, T_Sell_Cashier cashier) {
		if(service == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(service.getSs_tel())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_name())){
			throw new IllegalArgumentException("顾客姓名不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_manager())){
			throw new IllegalArgumentException("受理人员不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_startdate())){
			throw new IllegalArgumentException("接受日期不能为空");
		}
		if(StringUtil.isEmpty(service.getSs_pd_no())){
			throw new IllegalArgumentException("商品货号不能为空");
		}
		service.setSs_shop_code(cashier.getCa_shop_code());
		service.setCompanyid(cashier.getCompanyid());
		service.setSs_state(0);
		service.setSs_sms_count(0);
		service.setSs_ca_emcode(cashier.getCa_em_code());
		service.setSs_sysdate(DateUtil.getCurrentTime());
		service.setSs_tel(StringUtil.decodeString(service.getSs_tel()));
		service.setSs_name(StringUtil.decodeString(service.getSs_name()));
		service.setSs_manager(StringUtil.decodeString(service.getSs_manager()));
		service.setSs_pd_no(StringUtil.decodeString(service.getSs_pd_no()));
		service.setSs_question(StringUtil.decodeString(service.getSs_question()));
		serviceDAO.save(service);
	}

	@Override
	public void update(T_Shop_Service service, T_Sys_User user) {
		if(service == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(service.getSs_state())){
			throw new IllegalArgumentException("状态不能为空!");
		}
		if(StringUtil.isEmpty(service.getSs_id())){
			throw new IllegalArgumentException("ss_id不能为空!");
		}
		service.setSs_startdate(DateUtil.getYearMonthDate());
		service.setSs_manager(user.getUs_name());
		service.setSs_result(StringUtil.decodeString(service.getSs_result()));
		service.setSs_servicedate(DateUtil.getYearMonthDate());
		service.setSs_satis(StringUtil.decodeString(service.getSs_satis()));
		service.setSs_enddate(DateUtil.getYearMonthDate());
		service.setSs_pickupname(user.getUs_name());
		serviceDAO.update(service);
	}

	@Override
	public void update(T_Shop_Service service, T_Sell_Cashier cashier) {
		if(service == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(service.getSs_state())){
			throw new IllegalArgumentException("状态不能为空!");
		}
		if(StringUtil.isEmpty(service.getSs_id())){
			throw new IllegalArgumentException("ss_id不能为空!");
		}
		service.setSs_startdate(DateUtil.getYearMonthDate());
		service.setSs_manager(cashier.getEm_name());
		service.setSs_result(StringUtil.decodeString(service.getSs_result()));
		service.setSs_servicedate(DateUtil.getYearMonthDate());
		service.setSs_satis(StringUtil.decodeString(service.getSs_satis()));
		service.setSs_enddate(DateUtil.getYearMonthDate());
		service.setSs_pickupname(cashier.getEm_name());
		serviceDAO.update(service);
	}

	@Override
	public void del(Integer ss_id) {
		serviceDAO.del(ss_id);
	}

	@Override
	public T_Shop_Service load(Integer ss_id) {
		return serviceDAO.load(ss_id);
	}

	@Override
	public PageData<T_Shop_Service_Item> list_serviceitem(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = serviceDAO.list_serviceitemcount(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Service_Item> list = serviceDAO.list_serviceitem(param);
		PageData<T_Shop_Service_Item> pageData = new PageData<T_Shop_Service_Item>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public List<T_Shop_Service_Item> list_serviceitem_combo(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return serviceDAO.list_serviceitem(param);
	}

	@Override
	public Integer queryItemByName(T_Shop_Service_Item service_Item) {
		return serviceDAO.queryItemByName(service_Item);
	}

	@Override
	@Transactional
	public void save_serviceitem(T_Shop_Service_Item service_Item) {
		serviceDAO.save_serviceitem(service_Item);
	}

	@Override
	public T_Shop_Service_Item queryServiceItemByID(Integer ssi_id) {
		return serviceDAO.queryServiceItemByID(ssi_id);
	}

	@Override
	public void update_serviceitem(T_Shop_Service_Item service_Item) {
		serviceDAO.update_serviceitem(service_Item);
	}

	@Override
	public void del_serviceitem(Integer ssi_id) {
		serviceDAO.del_serviceitem(ssi_id);
	}

}
