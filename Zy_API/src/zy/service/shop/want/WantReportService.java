package zy.service.shop.want;

import java.util.Map;

import zy.dto.shop.want.WantDetailReportDto;
import zy.dto.shop.want.WantReportDto;
import zy.entity.PageData;

public interface WantReportService {
	PageData<WantReportDto> pageWantReport(Map<String, Object> params);
	PageData<WantDetailReportDto> pageWantDetailReport(Map<String, Object> params);
}
