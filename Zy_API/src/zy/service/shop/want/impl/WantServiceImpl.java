package zy.service.shop.want.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.base.product.ProductDAO;
import zy.dao.base.shop.ShopDAO;
import zy.dao.base.size.SizeDAO;
import zy.dao.shop.price.PriceDAO;
import zy.dao.shop.want.WantDAO;
import zy.dao.stock.data.DataDAO;
import zy.dao.stock.useable.UseableDAO;
import zy.dao.sys.print.PrintDAO;
import zy.dto.common.BarcodeImportDto;
import zy.dto.common.ProductDto;
import zy.dto.shop.want.ProductStorePriceDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.product.T_Base_Product_Shop_Price;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.shop.price.T_Shop_Price;
import zy.entity.shop.price.T_Shop_PriceList;
import zy.entity.shop.want.T_Shop_Want;
import zy.entity.shop.want.T_Shop_WantList;
import zy.entity.stock.data.T_Stock_DataBill;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.want.WantService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.common.SizeHorizontalVO;
import zy.vo.shop.WantVO;

@Service
public class WantServiceImpl implements WantService{
	@Resource
	private WantDAO wantDAO;
	
	@Resource
	private SizeDAO sizeDAO;
	
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	
	@Resource
	private DataDAO dataDAO;
	
	@Resource
	private PrintDAO printDAO;
	
	@Resource
	private ShopDAO shopDAO;
	
	@Resource
	private PriceDAO priceDAO;
	
	@Resource
	private UseableDAO useableDAO;
	
	@Resource
	private ProductDAO productDAO;
	
	@Override
	public Integer count(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return wantDAO.count(params);
	}
	
	@Override
	public PageData<T_Shop_Want> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = wantDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Shop_Want> list = wantDAO.list(params);
		PageData<T_Shop_Want> pageData = new PageData<T_Shop_Want>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Shop_Want load(Integer wt_id) {
		T_Shop_Want want = wantDAO.load(wt_id);
		if(want != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(want.getWt_number(), want.getCompanyid());
			if(approve_Record != null){
				want.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return want;
	}

	@Override
	public List<T_Shop_WantList> detail_list(Map<String, Object> params) {
		return wantDAO.detail_list(params);
	}
	
	@Override
	public List<T_Shop_WantList> detail_sum(Map<String, Object> params) {
		return wantDAO.detail_sum(params);
	}
	
	@Override
	public Map<String, Object> detail_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = wantDAO.detail_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}
	
	@Override
	public Map<String, Object> detail_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		Integer wt_ar_state = (Integer)params.get("wt_ar_state");
		Integer isFHD = (Integer)params.get("isFHD");
		Integer isSend = (Integer)params.get("isSend");
		List<String> szgCodes = wantDAO.detail_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "wtl_pd_code,wtl_cr_code,wtl_br_code");
		params.put(CommonUtil.SORD, "ASC");
		boolean sendAmount = false;//尺码横排是否使用发货数量
		if(isSend.intValue() == 1){
			sendAmount = true;
		}else if(isFHD.intValue() == 1){
			sendAmount = true;
		}else if(wt_ar_state.intValue() == 3 || wt_ar_state.intValue() == 4 || wt_ar_state.intValue() == 5){
			sendAmount = true;
		}
		
		List<T_Shop_WantList> temps = wantDAO.detail_list(params);
		return WantVO.getJsonSizeData(sizeGroupList, temps,sendAmount);
	}
	
	@Override
	public List<T_Shop_WantList> temp_list(Map<String, Object> params) {
		return wantDAO.temp_list(params);
	}

	@Override
	public List<T_Shop_WantList> temp_sum(Integer wt_type, Integer us_id,Integer companyid) {
		return wantDAO.temp_sum(wt_type, us_id, companyid);
	}

	@Override
	public Map<String, Object> temp_size_title(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = wantDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("titles", new ArrayList<List<String>>());
			return resultMap;
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		return SizeHorizontalVO.getJsonSizeTitles(sizeGroupList);
	}

	@Override
	public Map<String, Object> temp_size(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		List<String> szgCodes = wantDAO.temp_szgcode(params);
		if(szgCodes==null||szgCodes.size()==0){
			return new HashMap<String, Object>();
		}
		List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, companyid);
		params.put(CommonUtil.SIDX, "wtl_pd_code,wtl_cr_code,wtl_br_code");
		params.put(CommonUtil.SORD, "ASC");
		List<T_Shop_WantList> temps = wantDAO.temp_list(params);
		return WantVO.getJsonSizeData(sizeGroupList, temps,false);
	}
	
	@Override
	public PageData<T_Base_Product> page_product(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = wantDAO.count_product(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Base_Product> list = wantDAO.list_product(param);
		PageData<T_Base_Product> pageData = new PageData<T_Base_Product>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> temp_loadproduct(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		T_Sys_Set sysSet = (T_Sys_Set)params.get(CommonUtil.KEY_SYSSET);
		Integer us_id = (Integer)params.get("us_id");
		String pd_code = (String)params.get("pd_code");
		String dp_code = (String)params.get("dp_code");
		String sp_code = (String)params.get("sp_code");
		Integer wt_type = (Integer)params.get("wt_type");
		String exist = (String)params.get("exist");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		T_Base_Product base_Product = wantDAO.load_product(pd_code,sp_code, companyid);
		
		Map<String, Object> product = new HashMap<String, Object>();
		product.put("pd_id", base_Product.getPd_id());
		product.put("pd_code", base_Product.getPd_code());
		product.put("pd_no", base_Product.getPd_no());
		product.put("pd_name", base_Product.getPd_name());
		product.put("pd_szg_code", base_Product.getPd_szg_code());
		product.put("pd_unit", base_Product.getPd_unit());
		product.put("pd_year", base_Product.getPd_year());
		product.put("pd_season", base_Product.getPd_season());
		product.put("pd_sell_price", base_Product.getPd_sell_price());
		product.put("pd_cost_price", base_Product.getPd_cost_price());
		product.put("pd_bd_name", base_Product.getPd_bd_name());
		product.put("pd_tp_name", base_Product.getPd_tp_name());
		product.put("pdm_img_path", StringUtil.trimString(base_Product.getPdm_img_path()));
		Double unitPrice = null;
		if("1".equals(exist)){//已经录入从临时表中查询单价
			T_Shop_WantList temp_price = wantDAO.temp_queryUnitPrice(pd_code, wt_type, us_id, companyid);
			if (temp_price != null) {
				unitPrice = temp_price.getWtl_unitprice();
				product.put("temp_unitPrice", StringUtil.trimString(unitPrice));
			}
		}
		if(unitPrice == null){
			unitPrice = base_Product.getPd_sell_price();
		}
		product.put("unitPrice", unitPrice);
		resultMap.put("product", product);
		params.put("szg_code", base_Product.getPd_szg_code());
		Map<String, Object> productMap = wantDAO.load_product_size(params);
		List<T_Base_Size> sizes=(List<T_Base_Size>)productMap.get("sizes");
		List<ProductDto> inputs=(List<ProductDto>)productMap.get("inputs");
		List<ProductDto> temps=(List<ProductDto>)productMap.get("temps");
		List<ProductDto> stocks=(List<ProductDto>)productMap.get("stocks");
		Map<String,Object> usableStockMap = null;
		if (sysSet.getSt_useable() != null && sysSet.getSt_useable().intValue() == 1) {
			usableStockMap = useableDAO.loadUseableStock(pd_code, dp_code, companyid);
		}
		resultMap.putAll(SizeHorizontalVO.buildJsonProductInput(pd_code, sizes, inputs, stocks, temps, usableStockMap));
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> send_loadproduct(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		T_Sys_Set sysSet = (T_Sys_Set)params.get(CommonUtil.KEY_SYSSET);
		String pd_code = (String)params.get("pd_code");
		String dp_code = (String)params.get("dp_code");
		String wt_number = (String)params.get("wt_number");
		String sp_code = (String)params.get("sp_code");
		String exist = (String)params.get("exist");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		T_Base_Product base_Product = wantDAO.load_product(pd_code,sp_code, companyid);
		
		Map<String, Object> product = new HashMap<String, Object>();
		product.put("pd_id", base_Product.getPd_id());
		product.put("pd_code", base_Product.getPd_code());
		product.put("pd_no", base_Product.getPd_no());
		product.put("pd_name", base_Product.getPd_name());
		product.put("pd_szg_code", base_Product.getPd_szg_code());
		product.put("pd_unit", base_Product.getPd_unit());
		product.put("pd_year", base_Product.getPd_year());
		product.put("pd_season", base_Product.getPd_season());
		product.put("pd_sell_price", base_Product.getPd_sell_price());
		product.put("pd_cost_price", base_Product.getPd_cost_price());
		product.put("pd_bd_name", base_Product.getPd_bd_name());
		product.put("pd_tp_name", base_Product.getPd_tp_name());
		product.put("pdm_img_path", StringUtil.trimString(base_Product.getPdm_img_path()));
		Double unitPrice = null;
		if("1".equals(exist)){//已经录入从临时表中查询单价
			T_Shop_WantList temp_price = wantDAO.detail_queryUnitPrice(pd_code, wt_number, companyid);
			if (temp_price != null) {
				unitPrice = temp_price.getWtl_unitprice();
				product.put("temp_unitPrice", StringUtil.trimString(unitPrice));
			}
		}
		if(unitPrice == null){
			unitPrice = base_Product.getPd_sell_price();
		}
		product.put("unitPrice", unitPrice);
		resultMap.put("product", product);
		params.put("szg_code", base_Product.getPd_szg_code());
		Map<String, Object> productMap = wantDAO.load_product_size_send(params);
		List<T_Base_Size> sizes=(List<T_Base_Size>)productMap.get("sizes");
		List<ProductDto> inputs=(List<ProductDto>)productMap.get("inputs");
		List<ProductDto> temps=(List<ProductDto>)productMap.get("temps");
		List<ProductDto> stocks=(List<ProductDto>)productMap.get("stocks");
		Map<String,Object> usableStockMap = null;
		if (sysSet.getSt_useable() != null && sysSet.getSt_useable().intValue() == 1) {
			usableStockMap = useableDAO.loadUseableStock(pd_code, dp_code, companyid);
		}
		resultMap.putAll(SizeHorizontalVO.buildJsonProductInput(pd_code, sizes, inputs, stocks, temps, usableStockMap));
		return resultMap;
	}
	
	@Override
	@Transactional
	public Map<String, Object> temp_save_bybarcode(Map<String, Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Integer wt_type = (Integer)params.get("wt_type");
		String barcode = (String)params.get("barcode");
		Integer amount = (Integer)params.get("amount");
		String sp_code = (String)params.get("sp_code");
		T_Sys_User user = (T_Sys_User)params.get("user");
		T_Base_Barcode base_Barcode = productDAO.loadBarcode(barcode, user.getCompanyid());
		if(base_Barcode == null){
			resultMap.put("result", 3);//条码不存在
			return resultMap;
		}
		T_Shop_WantList temp = wantDAO.temp_loadBySubCode(base_Barcode.getBc_subcode(), wt_type, user.getUs_id(), user.getCompanyid());
		if (temp != null) {//临时表存在则直接更新数量
			temp.setWtl_applyamount(temp.getWtl_applyamount()+amount);
			wantDAO.temp_update(temp);
			resultMap.put("result", 1);//update
			resultMap.put("temp", temp);
			return resultMap;
		}
		T_Base_Product base_Product = wantDAO.load_product(base_Barcode.getBc_pd_code(),sp_code, user.getCompanyid());
		T_Shop_WantList temp_price= wantDAO.temp_queryUnitPrice(base_Barcode.getBc_pd_code(),wt_type, user.getUs_id(), user.getCompanyid());;
		Double unitPrice = null;
		if(temp_price != null){
			unitPrice = temp_price.getWtl_unitprice();
		}else {
			unitPrice = base_Product.getPd_sell_price();
		}
		
		temp = new T_Shop_WantList();
		temp.setWtl_pd_code(base_Barcode.getBc_pd_code());
		temp.setWtl_cr_code(base_Barcode.getBc_color());
		temp.setWtl_sz_code(base_Barcode.getBc_size());
		temp.setWtl_szg_code(base_Product.getPd_szg_code());
		temp.setWtl_br_code(base_Barcode.getBc_bra());
		temp.setWtl_sub_code(temp.getWtl_pd_code()+temp.getWtl_cr_code()+temp.getWtl_sz_code()+temp.getWtl_br_code());
		temp.setWtl_applyamount(amount);
		temp.setWtl_unitprice(unitPrice);
		temp.setWtl_costprice(base_Product.getPd_cost_price());
		temp.setWtl_remark("");
		temp.setWtl_type(wt_type);
		temp.setWtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		wantDAO.temp_save(temp);
		temp.setPd_no(base_Product.getPd_no());
		temp.setPd_name(base_Product.getPd_name());
		temp.setPd_unit(base_Product.getPd_unit());
		temp.setBd_name(base_Product.getPd_bd_name());
		temp.setTp_name(base_Product.getPd_tp_name());
		temp.setCr_name(base_Barcode.getBc_colorname());
		temp.setSz_name(base_Barcode.getBc_sizename());
		temp.setBr_name(base_Barcode.getBc_braname());
		resultMap.put("result", 2);//add
		resultMap.put("temp", temp);
		return resultMap;
	}
	
	@Override
	@Transactional
	public Map<String, Object> detail_save_bybarcode(Map<String, Object> params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Integer wt_type = (Integer)params.get("wt_type");
		String wt_number = (String)params.get("wt_number");
		String barcode = (String)params.get("barcode");
		Integer amount = (Integer)params.get("amount");
		String sp_code = (String)params.get("sp_code");
		T_Sys_User user = (T_Sys_User)params.get("user");
		T_Base_Barcode base_Barcode = productDAO.loadBarcode(barcode, user.getCompanyid());
		if(base_Barcode == null){
			resultMap.put("result", 3);//条码不存在
			return resultMap;
		}
		T_Shop_WantList temp = wantDAO.detail_loadBySubCode(base_Barcode.getBc_subcode(), wt_number, user.getCompanyid());
		if (temp != null) {//临时表存在则直接更新数量
			if (wt_type.intValue() == 0) {
				temp.setWtl_sendamount(temp.getWtl_sendamount() + amount);
			} else {
				temp.setWtl_sendamount(temp.getWtl_sendamount() - amount);
			}
			wantDAO.detail_update(temp);
			resultMap.put("result", 1);//update
			resultMap.put("temp", temp);
			return resultMap;
		}
		T_Base_Product base_Product = wantDAO.load_product(base_Barcode.getBc_pd_code(),sp_code, user.getCompanyid());
		T_Shop_WantList temp_price= wantDAO.detail_queryUnitPrice(base_Barcode.getBc_pd_code(),wt_number, user.getCompanyid());;
		Double unitPrice = null;
		if(temp_price != null){
			unitPrice = temp_price.getWtl_unitprice();
		}else {
			unitPrice = base_Product.getPd_sell_price();
		}
		
		temp = new T_Shop_WantList();
		temp.setWtl_number(wt_number);
		temp.setWtl_pd_code(base_Barcode.getBc_pd_code());
		temp.setWtl_cr_code(base_Barcode.getBc_color());
		temp.setWtl_sz_code(base_Barcode.getBc_size());
		temp.setWtl_szg_code(base_Product.getPd_szg_code());
		temp.setWtl_br_code(base_Barcode.getBc_bra());
		temp.setWtl_sub_code(temp.getWtl_pd_code()+temp.getWtl_cr_code()+temp.getWtl_sz_code()+temp.getWtl_br_code());
		temp.setWtl_applyamount(0);
		if (wt_type.intValue() == 0) {
			temp.setWtl_sendamount(amount);
		} else {
			temp.setWtl_sendamount(-amount);
		}
		temp.setWtl_unitprice(unitPrice);
		temp.setWtl_costprice(base_Product.getPd_cost_price());
		temp.setWtl_remark("");
		temp.setWtl_type(wt_type);
		temp.setWtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		wantDAO.detail_save(temp);
		temp.setPd_no(base_Product.getPd_no());
		temp.setPd_name(base_Product.getPd_name());
		temp.setPd_unit(base_Product.getPd_unit());
		temp.setBd_name(base_Product.getPd_bd_name());
		temp.setTp_name(base_Product.getPd_tp_name());
		temp.setCr_name(base_Barcode.getBc_colorname());
		temp.setSz_name(base_Barcode.getBc_sizename());
		temp.setBr_name(base_Barcode.getBc_braname());
		resultMap.put("result", 2);//add
		resultMap.put("temp", temp);
		return resultMap;
	}
	
	@Override
	@Transactional
	public void temp_save(Map<String, Object> params) {
		List<T_Shop_WantList> temps = (List<T_Shop_WantList>)params.get("temps");
		Integer wt_type = (Integer)params.get("wt_type");
		T_Sys_User user = (T_Sys_User)params.get("user");
		Object pd_code = params.get("pd_code");
		Object unitPrice = params.get("unitPrice");
		if (temps != null && temps.size() > 0) {
			List<T_Shop_WantList> temps_add = new ArrayList<T_Shop_WantList>();
			List<T_Shop_WantList> temps_update = new ArrayList<T_Shop_WantList>();
			List<T_Shop_WantList> temps_del = new ArrayList<T_Shop_WantList>();
			for (T_Shop_WantList item : temps) {
				if ("add".equals(item.getOperate_type())){
					if (item.getWtl_applyamount() > 0) {
						temps_add.add(item);
					}
				}else if("update".equals(item.getOperate_type())){
					if(!item.getWtl_applyamount().equals(0)){
						temps_update.add(item);
					}else {
						temps_del.add(item);
					}
				}
			}
			if (temps_add.size() > 0) {
				wantDAO.temp_save(temps_add);
			}
			if (temps_update.size() > 0) {
				wantDAO.temp_update(temps_update);
			}
			if (temps_del.size() > 0) {
				wantDAO.temp_del(temps_del);
			}
		}
		if (StringUtil.isNotEmpty(pd_code) && StringUtil.isNotEmpty(unitPrice)) {
			wantDAO.temp_updateprice(pd_code.toString(),Double.parseDouble(unitPrice.toString()), wt_type, user.getUs_id(), user.getCompanyid());
		}
	}
	
	@Override
	@Transactional
	public void send_save_detail(Map<String, Object> params) {
		List<T_Shop_WantList> temps = (List<T_Shop_WantList>)params.get("temps");
		String wt_number = (String)params.get("wt_number");
		T_Sys_User user = (T_Sys_User)params.get("user");
		Object pd_code = params.get("pd_code");
		Object unitPrice = params.get("unitPrice");
		if (temps != null && temps.size() > 0) {
			List<T_Shop_WantList> temps_add = new ArrayList<T_Shop_WantList>();
			List<T_Shop_WantList> temps_update = new ArrayList<T_Shop_WantList>();
			List<T_Shop_WantList> temps_del = new ArrayList<T_Shop_WantList>();
			for (T_Shop_WantList item : temps) {
				if ("add".equals(item.getOperate_type())){
					if (item.getWtl_sendamount() > 0) {
						temps_add.add(item);
					}
				}else if("update".equals(item.getOperate_type())){
					if(!item.getWtl_sendamount().equals(0)){
						temps_update.add(item);
					}else {
						temps_del.add(item);
					}
				}
			}
			if (temps_add.size() > 0) {
				wantDAO.detail_save(temps_add);
			}
			if (temps_update.size() > 0) {
				wantDAO.detail_update(temps_update);
			}
			if (temps_del.size() > 0) {
				wantDAO.detail_del(temps_del);
			}
		}
		if (StringUtil.isNotEmpty(pd_code) && StringUtil.isNotEmpty(unitPrice)) {
			wantDAO.detail_updateprice(pd_code.toString(),Double.parseDouble(unitPrice.toString()), wt_number, user.getCompanyid());
		}
	}
	
	@Override
	@Transactional
	public void temp_import(Map<String, Object> params) {
		List<String[]> datas = (List<String[]>)params.get("datas");
		Integer wt_type = (Integer)params.get("wt_type");
		T_Sys_User user = (T_Sys_User)params.get("user");
		String sp_code = (String)params.get("sp_code");
		List<String> barcodes = new ArrayList<String>();
		Map<String, Integer> data_amount = new HashMap<String, Integer>();
		for (String[] data : datas) {
			barcodes.add(data[0]);
			data_amount.put(data[0], Integer.parseInt(data[1]));
		}
		List<BarcodeImportDto> imports = wantDAO.temp_listByImport(barcodes, sp_code, user.getCompanyid());
		if (imports == null || imports.size() == 0) {
			throw new RuntimeException("无数据可导入");
		}
		Map<String, Double> unitPriceMap = new HashMap<String, Double>();
		Map<String, T_Shop_WantList> tempsMap = new HashMap<String, T_Shop_WantList>();
		List<T_Shop_WantList> temps = wantDAO.temp_list_forimport(wt_type, user.getUs_id(), user.getCompanyid());
		for (T_Shop_WantList temp : temps) {
			if(!unitPriceMap.containsKey(temp.getWtl_pd_code())){
				unitPriceMap.put(temp.getWtl_pd_code(), temp.getWtl_unitprice());
			}
			tempsMap.put(temp.getWtl_sub_code(), temp);
		}
		List<T_Shop_WantList> temps_add = new ArrayList<T_Shop_WantList>();
		List<T_Shop_WantList> temps_update = new ArrayList<T_Shop_WantList>();
		for (BarcodeImportDto item : imports) {
			if(tempsMap.containsKey(item.getBc_subcode())){//临时表已存在，更新数量
				T_Shop_WantList temp = tempsMap.get(item.getBc_subcode());
				temp.setWtl_applyamount(temp.getWtl_applyamount()+data_amount.get(item.getBc_barcode()));
				temps_update.add(temp);
			}else {//临时表不存在，新增数据
				T_Shop_WantList temp = new T_Shop_WantList();
				temp.setWtl_pd_code(item.getBc_pd_code());
				temp.setWtl_sub_code(item.getBc_subcode());
				temp.setWtl_sz_code(item.getBc_size());
				temp.setWtl_szg_code(item.getPd_szg_code());
				temp.setWtl_cr_code(item.getBc_color());
				temp.setWtl_br_code(item.getBc_bra());
				temp.setWtl_applyamount(data_amount.get(item.getBc_barcode()));
				if(unitPriceMap.containsKey(item.getBc_pd_code())){//临时表已存在此货号，则使用临时表价格
					temp.setWtl_unitprice(unitPriceMap.get(item.getBc_pd_code()));
				}else {
					temp.setWtl_unitprice(item.getUnit_price());
				}
				temp.setWtl_costprice(item.getCost_price());
				temp.setWtl_remark("");
				temp.setWtl_us_id(user.getUs_id());
				temp.setWtl_type(wt_type);
				temp.setCompanyid(user.getCompanyid());
				temps_add.add(temp);
			}
		}
		if (temps_add.size() > 0) {
			wantDAO.temp_save(temps_add);
		}
		if (temps_update.size() > 0) {
			wantDAO.temp_updateById(temps_update);
		}
	}
	
	@Override
	@Transactional
	public void temp_copy(Map<String, Object> params) {
		String ids = (String)params.get("ids");
		Integer wt_type = (Integer)params.get("wt_type");
		T_Sys_User user = (T_Sys_User)params.get("user");
		if(StringUtil.isEmpty(ids)){
			throw new IllegalArgumentException("参数ids不能为null");
		}
		List<Long> wtl_ids = new ArrayList<Long>();
		String[] idArr = ids.split(",");
		for (String id : idArr) {
			wtl_ids.add(Long.parseLong(id));
		}
		List<T_Shop_WantList> details = wantDAO.detail_list_forcopy(wtl_ids);
		if (details == null || details.size() == 0) {
			throw new RuntimeException("单据明细不存在");
		}
		for(T_Shop_WantList item:details){
			item.setWtl_us_id(user.getUs_id());
			item.setWtl_type(wt_type);
			if(item.getWtl_sendamount().intValue() != 0){
				item.setWtl_applyamount(item.getWtl_sendamount());
			}
		}
		wantDAO.temp_clear(wt_type, user.getUs_id(), user.getCompanyid());
		wantDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void temp_updateAmount(T_Shop_WantList temp) {
		wantDAO.temp_update(temp);
	}
	
	@Override
	@Transactional
	public void temp_updatePrice(T_Shop_WantList temp) {
		wantDAO.temp_updateprice(temp.getWtl_pd_code(),temp.getWtl_unitprice(), temp.getWtl_type(),temp.getWtl_us_id(), temp.getCompanyid());
	}
	
	@Override
	@Transactional
	public void temp_updateRemarkById(T_Shop_WantList temp) {
		wantDAO.temp_updateRemarkById(temp);
	}
	
	@Override
	@Transactional
	public void temp_updateRemarkByPdCode(T_Shop_WantList temp) {
		wantDAO.temp_updateRemarkByPdCode(temp);
	}
	
	@Override
	@Transactional
	public void temp_del(Integer wtl_id) {
		wantDAO.temp_del(wtl_id);
	}

	@Override
	@Transactional
	public void temp_delByPiCode(T_Shop_WantList temp) {
		if (temp.getCompanyid() == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if (temp.getWtl_us_id() == null) {
			throw new IllegalArgumentException("参数us_id不能为null");
		}
		if (temp.getWtl_type() == null) {
			throw new IllegalArgumentException("参数wtl_type不能为null");
		}
		wantDAO.temp_delByPiCode(temp);
	}
	
	@Override
	@Transactional
	public void temp_clear(Integer wt_type,Integer us_id,Integer companyid) {
		wantDAO.temp_clear(wt_type, us_id, companyid);;
	}

	@Override
	@Transactional
	public void detail_updateAmount(T_Shop_WantList temp) {
		wantDAO.detail_update(temp);
	}
	
	@Override
	@Transactional
	public void detail_updatePrice(T_Shop_WantList temp) {
		wantDAO.detail_updateprice(temp);
	}
	
	@Override
	@Transactional
	public void detail_automatch(String number,Integer companyid) {
		wantDAO.detail_automatch(number, companyid);
	}
	
	@Override
	@Transactional
	public void detail_del(Integer wtl_id) {
		wantDAO.detail_del(wtl_id);
	}
	
	@Override
	@Transactional
	public void save(T_Shop_Want want, T_Sys_User user) {
		if(StringUtil.isEmpty(want.getWt_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(want.getWt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		if((want.getWt_type().intValue() == 0 && (user.getShoptype().equals("1") || user.getShoptype().equals("2")))
				|| (want.getWt_type().intValue() == 1 && user.getShoptype().equals("3"))){
			if(StringUtil.isEmpty(want.getWt_outdp_code())){
				throw new IllegalArgumentException("发货仓库不能为空");
			}
		}else if((want.getWt_type().intValue() == 1 && (user.getShoptype().equals("1") || user.getShoptype().equals("2")))
				||(want.getWt_type().intValue() == 0 && user.getShoptype().equals("3"))){
			if(StringUtil.isEmpty(want.getWt_indp_code())){
				throw new IllegalArgumentException("收货仓库不能为空");
			}
		}
		want.setCompanyid(user.getCompanyid());
		want.setWt_us_id(user.getUs_id());
		want.setWt_maker(user.getUs_name());
		want.setWt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		want.setWt_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Shop_WantList> temps = wantDAO.temp_list_forsave(want.getWt_type(), user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		if(want.getWt_type().equals(1)){//退货
			for (T_Shop_WantList temp : temps) {
				temp.setWtl_applyamount(-Math.abs(temp.getWtl_applyamount()));
				temp.setWtl_sendamount(0);
			}
		}else{
			if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//店铺发货单(总公司、分公司)
				for (T_Shop_WantList temp : temps) {
					temp.setWtl_sendamount(temp.getWtl_applyamount());
					temp.setWtl_applyamount(0);
				}
			}else {//补货申请单
				for (T_Shop_WantList temp : temps) {
					temp.setWtl_sendamount(0);
				}
			}
		}
		int wt_applyamount = 0;
		int wt_sendamount = 0;
		double wt_applymoney = 0d;
		double wt_sendmoney = 0d;
		double wt_sendcostmoney = 0d;
		for (T_Shop_WantList temp : temps) {
			wt_applyamount += temp.getWtl_applyamount();
			wt_sendamount += temp.getWtl_sendamount();
			wt_applymoney += temp.getWtl_applyamount() * temp.getWtl_unitprice();
			wt_sendmoney += temp.getWtl_sendamount() * temp.getWtl_unitprice();
			wt_sendcostmoney += temp.getWtl_sendamount() * temp.getWtl_costprice();
		}
		want.setWt_applyamount(wt_applyamount);
		want.setWt_sendamount(wt_sendamount);
		want.setWt_applymoney(wt_applymoney);
		want.setWt_sendmoney(wt_sendmoney);
		want.setWt_sendcostmoney(wt_sendcostmoney);
		wantDAO.save(want, temps, user);
		//3.删除临时表
		wantDAO.temp_clear(want.getWt_type(), user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Shop_Want want, T_Sys_User user) {
		if(StringUtil.isEmpty(want.getWt_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(want.getWt_outdp_code())){
			throw new IllegalArgumentException("仓库不能为空");
		}
		if(StringUtil.isEmpty(want.getWt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		want.setCompanyid(user.getCompanyid());
		want.setWt_us_id(user.getUs_id());
		want.setWt_maker(user.getUs_name());
		want.setWt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		want.setWt_sysdate(DateUtil.getCurrentTime());
		//1.1查临时表
		List<T_Shop_WantList> temps = wantDAO.temp_list_forsave(want.getWt_type(), user.getUs_id(), user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//1.2验证单据
		T_Shop_Want oldWant = wantDAO.check(want.getWt_number(), user.getCompanyid());
		if (oldWant == null || !CommonUtil.AR_STATE_FAIL.equals(oldWant.getWt_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//1.3删除子表
		wantDAO.deleteList(want.getWt_number(), user.getCompanyid());
		//2.保存单据
		if(want.getWt_type().equals(1)){//退货
			for (T_Shop_WantList temp : temps) {
				temp.setWtl_applyamount(-Math.abs(temp.getWtl_applyamount()));
				temp.setWtl_sendamount(0);
			}
		}else{
			if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//店铺发货单(总公司、分公司)
				for (T_Shop_WantList temp : temps) {
					temp.setWtl_sendamount(temp.getWtl_applyamount());
					temp.setWtl_applyamount(0);
				}
			}else {//补货申请单
				for (T_Shop_WantList temp : temps) {
					temp.setWtl_sendamount(0);
				}
			}
		}
		int wt_applyamount = 0;
		int wt_sendamount = 0;
		double wt_applymoney = 0d;
		double wt_sendmoney = 0d;
		double wt_sendcostmoney = 0d;
		for (T_Shop_WantList temp : temps) {
			wt_applyamount += temp.getWtl_applyamount();
			wt_sendamount += temp.getWtl_sendamount();
			wt_applymoney += temp.getWtl_applyamount() * temp.getWtl_unitprice();
			wt_sendmoney += temp.getWtl_sendamount() * temp.getWtl_unitprice();
			wt_sendcostmoney += temp.getWtl_sendamount() * temp.getWtl_costprice();
		}
		want.setWt_applyamount(wt_applyamount);
		want.setWt_sendamount(wt_sendamount);
		want.setWt_applymoney(wt_applymoney);
		want.setWt_sendmoney(wt_sendmoney);
		want.setWt_sendcostmoney(wt_sendcostmoney);
		wantDAO.update(want, temps);
		//3.删除临时表
		wantDAO.temp_clear(want.getWt_type(), user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Shop_Want approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Shop_Want want = wantDAO.check(number, user.getCompanyid());
		if(want == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.WANT_AR_STATE_NOTAPPROVE.equals(want.getWt_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		if(CommonUtil.WANT_AR_STATE_APPROVED.equals(record.getAr_state()) && want.getWt_applyamount().equals(0)){//店铺发货单审核通过
			if(StringUtil.isEmpty(want.getWt_outdp_code())){
				throw new RuntimeException("发货仓库不能为空");
			}
		}
		//指定审核人检查
		if(StringUtil.isNotEmpty(want.getWt_ar_usid()) && !user.getUs_id().equals(want.getWt_ar_usid())){
			throw new RuntimeException("该单据指定审核人，与当前用户不一致");
		}
		//更新单据审核状态
		if(CommonUtil.WANT_AR_STATE_FAIL.equals(record.getAr_state())){
			want.setWt_ar_state(record.getAr_state());
		}else if(want.getWt_applyamount().equals(0)){//店铺发货单
			want.setWt_ar_state(CommonUtil.WANT_AR_STATE_SEND);
		}else {
			want.setWt_ar_state(record.getAr_state());
		}
		want.setWt_ar_date(DateUtil.getCurrentTime());
		wantDAO.updateApprove(want);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_want");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(CommonUtil.WANT_AR_STATE_FAIL.equals(record.getAr_state())){//审核不通过，则直接返回
			return want;
		}
		if(want.getWt_applyamount() != 0){
			return want;
		}
		//3.审核通过(店铺发货单直接发货)
		List<T_Stock_DataBill> stocks = wantDAO.listStock(number, want.getWt_outdp_code(), user.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(want.getWt_outdp_code());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(user.getCompanyid());
				stock.setSd_amount(-stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() - stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return want;
	}
	
	@Override
	@Transactional
	public T_Shop_Want send(String number,String wt_outdp_code, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Shop_Want want = wantDAO.check(number, user.getCompanyid());
		if(want == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.WANT_AR_STATE_APPROVED.equals(want.getWt_ar_state())){
			throw new RuntimeException("单据未审核或已发货");
		}
		want.setWt_outdp_code(wt_outdp_code);
		want.setWt_ar_state(CommonUtil.WANT_AR_STATE_SEND);
		if(StringUtil.isEmpty(want.getWt_outdp_code())){
			throw new RuntimeException("发货仓库不能为空");
		}
		wantDAO.updateSend(want);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_SEND);
		record.setAr_number(number);
		record.setAr_describe("");
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_want");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		
		//3.发货
		List<T_Stock_DataBill> stocks = wantDAO.listStock(number, want.getWt_outdp_code(), user.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(want.getWt_outdp_code());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(user.getCompanyid());
				stock.setSd_amount(-stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() - stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		return want;
	}
	
	@Override
	@Transactional
	public T_Shop_Want receive(Map<String, Object> params) {
		String number = StringUtil.trimString(params.get("number"));
		String wt_indp_code = StringUtil.trimString(params.get("wt_indp_code"));
		String us_name = StringUtil.trimString(params.get("us_name"));
		Integer us_id = (Integer)params.get("us_id");
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		
		T_Shop_Want want = wantDAO.check(number, companyid);
		if(want == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.WANT_AR_STATE_SEND.equals(want.getWt_ar_state())){
			throw new RuntimeException("单据未发货或已完成");
		}
		want.setWt_indp_code(wt_indp_code);
		want.setWt_ar_state(CommonUtil.WANT_AR_STATE_FINISH);
		if(StringUtil.isEmpty(want.getWt_indp_code())){
			throw new RuntimeException("收货仓库不能为空");
		}
		wantDAO.updateReceive(want);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_FINISH);
		record.setAr_number(number);
		record.setAr_describe("");
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(us_name);
		record.setAr_type("t_shop_want");
		record.setCompanyid(companyid);
		approveRecordDAO.save(record);
		
		//3.收货
		List<T_Stock_DataBill> stocks = wantDAO.listStock(number, want.getWt_indp_code(), companyid);
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(want.getWt_indp_code());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(companyid);
				stock.setSd_amount(stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() + stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		//4.更新店铺分店价格，生成调价单
		if(want.getWt_type().intValue() != 0){
			return want;
		}
		List<ProductStorePriceDto> priceDtos = wantDAO.load_want_product_price(number, want.getWt_shop_code(), companyid);
		List<T_Base_Product_Shop_Price> shopPrices_add = new ArrayList<T_Base_Product_Shop_Price>();
		List<T_Base_Product_Shop_Price> shopPrices_update = new ArrayList<T_Base_Product_Shop_Price>();
		List<T_Shop_PriceList> priceLists = new ArrayList<T_Shop_PriceList>();
		for (ProductStorePriceDto dto : priceDtos) {
			if (dto.getPdp_id() != null && dto.getPdp_id().intValue() != 0) {//分店价格已存在
				if(!dto.getUnitprice().equals(dto.getPdp_sell_price())){
					T_Base_Product_Shop_Price shop_Price = new T_Base_Product_Shop_Price();
					shop_Price.setPdp_id(dto.getPdp_id());
					shop_Price.setPdp_sell_price(dto.getUnitprice());
					shopPrices_update.add(shop_Price);
					//调价明细
					T_Shop_PriceList priceItem = new T_Shop_PriceList();
					priceItem.setSpl_pd_code(dto.getPd_code());
					priceItem.setSpl_old_sellprice(dto.getPdp_sell_price());
					priceItem.setSpl_new_sellprice(dto.getUnitprice());
					priceItem.setSpl_shop_code(want.getWt_shop_code());
					priceItem.setSpl_us_id(us_id);
					priceItem.setCompanyid(companyid);
					priceLists.add(priceItem);
				}
			}else {//分店价格不存在
				if(!dto.getUnitprice().equals(dto.getPd_sell_price())){
					T_Base_Product_Shop_Price shop_Price = new T_Base_Product_Shop_Price();
					shop_Price.setPdp_pd_code(dto.getPd_code());
					shop_Price.setPdp_shop_code(want.getWt_shop_code());
					shop_Price.setCompanyid(companyid);
					shop_Price.setPdp_sell_price(dto.getUnitprice());
					shop_Price.setPdp_vip_price(dto.getPd_vip_price());
					shop_Price.setPdp_cost_price(dto.getPd_cost_price());
					shop_Price.setPdp_sort_price(dto.getPd_sort_price());
					shopPrices_add.add(shop_Price);
					//调价明细
					T_Shop_PriceList priceItem = new T_Shop_PriceList();
					priceItem.setSpl_pd_code(dto.getPd_code());
					priceItem.setSpl_old_sellprice(dto.getPd_sell_price());
					priceItem.setSpl_new_sellprice(dto.getUnitprice());
					priceItem.setSpl_shop_code(want.getWt_shop_code());
					priceItem.setSpl_us_id(us_id);
					priceItem.setCompanyid(companyid);
					priceLists.add(priceItem);
				}
			}
		}
		if (shopPrices_add.size() > 0 || shopPrices_update.size() > 0) {
			wantDAO.update_base_shopprice(shopPrices_add, shopPrices_update);
		}
		if (priceLists.size() > 0) {
			T_Shop_Price shopPrice = new T_Shop_Price();
			shopPrice.setSp_maker(us_name);
			shopPrice.setSp_makerdate(DateUtil.getCurrentTime());
			shopPrice.setSp_sysdate(DateUtil.getCurrentTime());
			shopPrice.setSp_manager(us_name);
			shopPrice.setSp_is_sellprice(1);
			shopPrice.setSp_is_vipprice(0);
			shopPrice.setSp_is_costprice(0);
			shopPrice.setSp_is_sortprice(0);
			shopPrice.setSp_state(CommonUtil.AR_STATE_APPROVED);
			shopPrice.setSp_shop_code(want.getWt_shop_code());
			shopPrice.setSp_shop_type(3);
			shopPrice.setSp_remark("店铺发货单【"+number+"】修改价格生成。");
			shopPrice.setSp_us_id(us_id);
			shopPrice.setCompanyid(companyid);
			priceDAO.save(shopPrice, priceLists);
		}
		
		return want;
	}
	
	@Override
	@Transactional
	public T_Shop_Want reject(Map<String, Object> params) {
		String number = StringUtil.trimString(params.get("number"));
		String us_name = StringUtil.trimString(params.get("us_name"));
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		T_Shop_Want want = wantDAO.check(number, companyid);
		if(want == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.WANT_AR_STATE_SEND.equals(want.getWt_ar_state())){
			throw new RuntimeException("单据未发货或已完成");
		}
		want.setWt_ar_state(CommonUtil.WANT_AR_STATE_REJECT);
		wantDAO.updateReject(want);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REJECT);
		record.setAr_number(number);
		record.setAr_describe("");
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(us_name);
		record.setAr_type("t_shop_want");
		record.setCompanyid(companyid);
		approveRecordDAO.save(record);
		return want;
	}
	
	@Override
	@Transactional
	public T_Shop_Want rejectconfirm(String number,T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Shop_Want want = wantDAO.check(number, user.getCompanyid());
		if(want == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.WANT_AR_STATE_REJECT.equals(want.getWt_ar_state())){
			throw new RuntimeException("单据未被拒收，不能确认！");
		}
		want.setWt_ar_state(CommonUtil.WANT_AR_STATE_APPROVED);
		want.setWt_sendamount(0);
		want.setWt_sendmoney(0d);
		want.setWt_sendcostmoney(0d);
		wantDAO.updateRejectConfirm(want);
		//保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REJECTCONFIRM);
		record.setAr_number(number);
		record.setAr_describe("");
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_shop_want");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		
		//3.恢复库存
		List<T_Stock_DataBill> stocks = wantDAO.listStock(number, want.getWt_outdp_code(), user.getCompanyid());
		List<T_Stock_DataBill> add_stocks = new ArrayList<T_Stock_DataBill>();
		List<T_Stock_DataBill> update_stocks = new ArrayList<T_Stock_DataBill>();
		for (T_Stock_DataBill stock : stocks) {
			if(stock.getSd_id() == null){
				stock.setSd_dp_code(want.getWt_outdp_code());
				stock.setSd_init(0);
				stock.setSd_date(DateUtil.getCurrentTime());
				stock.setCompanyid(user.getCompanyid());
				stock.setSd_amount(stock.getBill_amount());
				add_stocks.add(stock);
			}else {
				stock.setSd_amount(stock.getSd_amount() + stock.getBill_amount());
				update_stocks.add(stock);
			}
		}
		if (add_stocks.size() > 0) {
			dataDAO.save(add_stocks);
		}
		if (update_stocks.size() > 0) {
			dataDAO.update(update_stocks);
		}
		
		return want;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number,Integer wt_type, Integer us_id, Integer companyid) {
		List<T_Shop_WantList> details = wantDAO.detail_list_forsavetemp(number, companyid);;
		for(T_Shop_WantList item:details){
			item.setWtl_us_id(us_id);
			if(item.getWtl_applyamount().intValue() == 0){
				item.setWtl_applyamount(item.getWtl_sendamount());
			}
		}
		wantDAO.temp_clear(wt_type, us_id, companyid);
		wantDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Shop_Want want = wantDAO.check(number, companyid);
		if(want == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(want.getWt_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(want.getWt_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		wantDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode,T_Sys_User user) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Shop_Want want = wantDAO.load(number, user.getCompanyid());
		List<T_Shop_WantList> wantList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("wtl_number", number);
		params.put("companyid", user.getCompanyid());
		if(displayMode.intValue() == 2){//汇总模式
			wantList = wantDAO.detail_sum(params);
		}else if(displayMode.intValue() == 1){//尺码模式 
			params.put(CommonUtil.SIDX, "wtl_pd_code,wtl_cr_code,wtl_br_code");
			params.put(CommonUtil.SORD, "ASC");
			wantList = wantDAO.detail_list_print(params);
		}else {
			wantList = wantDAO.detail_list_print(params);
		}
		resultMap.put("want", want);
		resultMap.put("wantList", wantList);
		resultMap.put("shop", shopDAO.load(want.getWt_shop_code(), user.getCompanyid()));
		resultMap.put("approveRecords", approveRecordDAO.list(number, "t_shop_want", user.getCompanyid()));
		if(displayMode.intValue() == 1){//尺码模式查询
			List<String> szgCodes = new ArrayList<String>();
			for (T_Shop_WantList item : wantList) {
				if(!szgCodes.contains(item.getWtl_szg_code())){
					szgCodes.add(item.getWtl_szg_code());
				}
			}
			List<T_Base_SizeList> sizeGroupList = sizeDAO.listBySzg(szgCodes, user.getCompanyid());
			resultMap.put("sizeGroupList", sizeGroupList);
		}
		return resultMap;
	}
	
}
