package zy.service.sort.prepay;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.sort.prepay.T_Sort_Prepay;
import zy.entity.sys.user.T_Sys_User;

public interface SortPrepayService {
	PageData<T_Sort_Prepay> page(Map<String, Object> params);
	T_Sort_Prepay load(Integer pp_id);
	T_Sort_Prepay load(String number,Integer companyid);
	T_Base_Shop loadShop(String sp_code,Integer companyid);
	T_Money_Bank loadBank(String ba_code,Integer companyid);
	void save(T_Sort_Prepay prepay, T_Sys_User user);
	void update(T_Sort_Prepay prepay, T_Sys_User user);
	T_Sort_Prepay approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Sort_Prepay reverse(String number, T_Sys_User user);
	void del(String number, Integer companyid);
}
