package zy.service.sort.settle.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.approve.ApproveRecordDAO;
import zy.dao.base.shop.ShopDAO;
import zy.dao.money.bank.BankDAO;
import zy.dao.money.bank.BankRunDAO;
import zy.dao.sort.dealings.SortDealingsDAO;
import zy.dao.sort.prepay.SortPrepayDAO;
import zy.dao.sort.settle.SortSettleDAO;
import zy.dao.sys.print.PrintDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.money.bank.T_Money_BankRun;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.entity.sort.dealings.T_Sort_Dealings;
import zy.entity.sort.fee.T_Sort_Fee;
import zy.entity.sort.prepay.T_Sort_Prepay;
import zy.entity.sort.settle.T_Sort_Settle;
import zy.entity.sort.settle.T_Sort_SettleList;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sort.settle.SortSettleService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Service
public class SortSettleServiceImpl implements SortSettleService{
	@Resource
	private SortSettleDAO sortSettleDAO;
	@Resource
	private ApproveRecordDAO approveRecordDAO;
	@Resource
	private SortDealingsDAO sortDealingsDAO;
	@Resource
	private SortPrepayDAO sortPrepayDAO;
	@Resource
	private ShopDAO shopDAO;
	@Resource
	private BankDAO bankDAO;
	@Resource
	private BankRunDAO bankRunDAO;
	@Resource
	private PrintDAO printDAO;
	
	@Override
	public PageData<T_Sort_Settle> page(Map<String, Object> params) {
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Integer totalCount = sortSettleDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<T_Sort_Settle> list = sortSettleDAO.list(params);
		PageData<T_Sort_Settle> pageData = new PageData<T_Sort_Settle>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public T_Sort_Settle load(Integer st_id) {
		T_Sort_Settle settle = sortSettleDAO.load(st_id);
		if(settle != null){
			T_Approve_Record approve_Record = approveRecordDAO.load(settle.getSt_number(), settle.getCompanyid());
			if(approve_Record != null){
				settle.setAr_describe(approve_Record.getAr_describe());
			}
		}
		return settle;
	}
	
	@Override
	public T_Sort_Settle load(String number,Integer companyid) {
		return sortSettleDAO.load(number, companyid);
	}
	
	@Override
	public T_Base_Shop loadShop(String sp_code,Integer companyid) {
		return shopDAO.loadShop(sp_code, companyid);
	}
	
	@Override
	public List<T_Sort_SettleList> temp_list(Map<String, Object> params) {
		return sortSettleDAO.temp_list(params);
	}

	@Override
	@Transactional
	public void temp_save(String sp_code, T_Sys_User user) {
		if (StringUtil.isEmpty(sp_code)) {
			throw new RuntimeException("请选择店铺");
		}
		T_Sort_Settle existSettle = sortSettleDAO.check_settle(sp_code, user.getCompanyid());
		if(existSettle != null){
			throw new RuntimeException("该店铺有单据未审核，不能再结算!");
		}
		sortSettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
		List<T_Sort_SettleList> temps = sortSettleDAO.load_sort_forsavetemp(sp_code, user.getCompanyid());
		for (T_Sort_SettleList temp : temps) {
			if(temp.getStl_ar_state().intValue() == 3 || temp.getStl_ar_state().intValue() == 5){
				temp.setStl_join(0);
			}else {
				temp.setStl_join(1);
			}
			temp.setStl_us_id(user.getUs_id());
			temp.setCompanyid(user.getCompanyid());
		}
		sortSettleDAO.temp_save(temps);
	}
	
	@Override
	@Transactional
	public void temp_updateDiscountMoney(T_Sort_SettleList temp) {
		sortSettleDAO.temp_updateDiscountMoney(temp);
	}

	@Override
	@Transactional
	public void temp_updatePrepay(T_Sort_SettleList temp) {
		sortSettleDAO.temp_updatePrepay(temp);
	}

	@Override
	@Transactional
	public void temp_updateRealMoney(T_Sort_SettleList temp) {
		sortSettleDAO.temp_updateRealMoney(temp);
	}

	@Override
	@Transactional
	public void temp_updateRemark(T_Sort_SettleList temp) {
		sortSettleDAO.temp_updateRemark(temp);
	}

	@Override
	@Transactional
	public void temp_updateJoin(String ids, String stl_join) {
		sortSettleDAO.temp_updateJoin(ids, stl_join);
	}
	
	@Override
	@Transactional
	public Map<String, Object> temp_entire(Map<String, Object> params) {
		Double discount_money = (Double)params.get("discount_money");
		Double prepay = (Double)params.get("prepay");
		Double received = (Double)params.get("received");
		T_Sys_User user = (T_Sys_User)params.get("user");
		if(discount_money == null || prepay == null || received == null){
			throw new RuntimeException("整单结算金额不能为空");
		}
		if (discount_money.doubleValue() == 0d && prepay.doubleValue() == 0d
				&& received.doubleValue() == 0d) {
			throw new RuntimeException("整单结算金额不能为0");
		}
		List<T_Sort_SettleList> temps = sortSettleDAO.temp_list_forentire(user.getUs_id(), user.getCompanyid());
		
		double remained_discount_money = discount_money;
		double remained_prepay = prepay;
		double remained_received = received;
		for (T_Sort_SettleList temp : temps) {
			if (CommonUtil.SORTDEALINGS_TYPE_TH.equals(temp.getStl_type())//退货单
					|| (CommonUtil.SORTDEALINGS_TYPE_QC.equals(temp.getStl_type()) && temp.getStl_unreceivable() < 0)//负的期初单
					|| (CommonUtil.SORTDEALINGS_TYPE_FY.equals(temp.getStl_type()) && temp.getStl_unreceivable() < 0)) {//负的费用单
				double tempMoney = temp.getStl_unreceivable();
				temp.setStl_discount_money(0d);
				temp.setStl_prepay(0d);
				temp.setStl_real_received(tempMoney);
				temp.setStl_join(1);
				remained_received += Math.abs(tempMoney);
				continue;
			}
			
			Double temp_discount_money = null;
			Double temp_prepay = null;
			Double temp_received = null;
			
			// 使用优惠金额
			if (remained_discount_money > 0) {
				temp_discount_money = Math.min(remained_discount_money,temp.getStl_unreceivable());
				remained_discount_money -= temp_discount_money;
			}else{
				temp_discount_money = 0d;
			}
			temp.setStl_discount_money(temp_discount_money);//设置本次使用优惠金额
			//使用预付款金额
			if(remained_prepay > 0){
				temp_prepay = Math.min(remained_prepay, temp.getStl_unreceivable()-temp.getStl_discount_money());
				remained_prepay -= temp_prepay;
			}else{
				temp_prepay = 0d;
			}
			temp.setStl_prepay(temp_prepay);//设置本次使用预收款金额
			
			//使用实收金额
			if(remained_received > 0){
				temp_received = Math.min(remained_received, temp.getStl_unreceivable()-temp.getStl_discount_money()-temp.getStl_prepay());
				remained_received -= temp_received;
			}else{
				temp_received = 0d;
			}
			temp.setStl_real_received(temp_received);
			if (temp_discount_money != 0 || temp_prepay != 0 || temp_received != 0) {
				temp.setStl_join(1);
			}else {
				if (temp.getStl_unreceivable() == 0) {
					temp.setStl_join(1);
				}else {
					temp.setStl_join(0);
				}
			}
		}
		sortSettleDAO.temp_update(temps);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("st_receivedmore", remained_discount_money+remained_prepay+remained_received);
		return resultMap;
	}
	
	@Override
	public List<T_Sort_SettleList> detail_list(String number,Integer companyid) {
		return sortSettleDAO.detail_list(number,companyid);
	}
	
	@Override
	@Transactional
	public void save(T_Sort_Settle settle, T_Sys_User user) {
		if(settle == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(settle.getSt_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		settle.setCompanyid(user.getCompanyid());
		settle.setSt_us_id(user.getUs_id());
		settle.setSt_maker(user.getUs_name());
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_sysdate(DateUtil.getCurrentTime());
		//1.查临时表
		List<T_Sort_SettleList> temps = sortSettleDAO.temp_list_forsave(user.getUs_id(),user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//2.保存单据
		double st_discount_money = 0d;
		double st_prepay = 0d;
		double st_received = 0d;
		for (T_Sort_SettleList temp : temps) {
			st_discount_money += temp.getStl_discount_money();
			st_prepay += temp.getStl_prepay();
			st_received += temp.getStl_real_received();
		}
		settle.setSt_discount_money(st_discount_money);
		settle.setSt_prepay(st_prepay);
		settle.setSt_received(st_received);
		if (settle.getSt_entire().intValue() == 1) {
			if (settle.getSt_receivedmore() == null || settle.getSt_receivedmore() < 0) {
				settle.setSt_receivedmore(0d);
			}
		} else {
			settle.setSt_receivedmore(0d);
		}
		sortSettleDAO.save(settle, temps);
		//3.删除临时表
		sortSettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public void update(T_Sort_Settle settle, T_Sys_User user) {
		if(settle == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(settle.getSt_shop_code())){
			throw new IllegalArgumentException("店铺不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_ba_code())){
			throw new IllegalArgumentException("银行账户不能为空");
		}
		if(StringUtil.isEmpty(settle.getSt_manager())){
			throw new IllegalArgumentException("经办人不能为空");
		}
		
		settle.setCompanyid(user.getCompanyid());
		settle.setSt_us_id(user.getUs_id());
		settle.setSt_maker(user.getUs_name());
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_ar_date(null);
		settle.setSt_sysdate(DateUtil.getCurrentTime());
		//1.1查临时表
		List<T_Sort_SettleList> temps = sortSettleDAO.temp_list_forsave(user.getUs_id(),user.getCompanyid());
		if(temps == null || temps.size() == 0){
			throw new RuntimeException("单据已保存，请勿重复提交");
		}
		//1.2验证单据
		T_Sort_Settle oldSettle = sortSettleDAO.check(settle.getSt_number(), user.getCompanyid());
		if (oldSettle == null || !CommonUtil.AR_STATE_FAIL.equals(oldSettle.getSt_ar_state())) {
			throw new RuntimeException("单据已修改，请勿重复提交");
		}
		//2.保存单据
		double st_discount_money = 0d;
		double st_prepay = 0d;
		double st_received = 0d;
		for (T_Sort_SettleList temp : temps) {
			st_discount_money += temp.getStl_discount_money();
			st_prepay += temp.getStl_prepay();
			st_received += temp.getStl_real_received();
		}
		settle.setSt_discount_money(st_discount_money);
		settle.setSt_prepay(st_prepay);
		settle.setSt_received(st_received);
		if (settle.getSt_entire().intValue() == 1) {
			if (settle.getSt_receivedmore() == null || settle.getSt_receivedmore() < 0) {
				settle.setSt_receivedmore(0d);
			}
		} else {
			settle.setSt_receivedmore(0d);
		}
		sortSettleDAO.deleteList(settle.getSt_number(), user.getCompanyid());
		sortSettleDAO.update(settle, temps);
		//3.删除临时表
		sortSettleDAO.temp_clear(user.getUs_id(), user.getCompanyid());
	}
	
	@Override
	@Transactional
	public T_Sort_Settle approve(String number, T_Approve_Record record, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sort_Settle settle = sortSettleDAO.check(number, user.getCompanyid());
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据已经审核");
		}
		//更新单据审核状态
		settle.setSt_ar_state(record.getAr_state());
		settle.setSt_ar_date(DateUtil.getCurrentTime());
		sortSettleDAO.updateApprove(settle);
		//保存审核记录表
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sort_settle");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		if(!CommonUtil.AR_STATE_APPROVED.equals(settle.getSt_ar_state())){//审核不通过，则直接返回
			return settle;
		}
		//3.审核通过
		//3.1更新批发客户财务&生成往来明细账
		T_Base_Shop shop = shopDAO.loadShop(settle.getSt_shop_code(), user.getCompanyid());
		shop.setSp_receivable(shop.getSp_receivable()-settle.getSt_discount_money());
		shop.setSp_received(shop.getSp_received()+settle.getSt_prepay()+settle.getSt_received());
		shop.setSp_prepay(shop.getSp_prepay()-settle.getSt_prepay()+settle.getSt_receivedmore());
		shopDAO.updateSettle(shop);
		if (settle.getSt_receivedmore() > 0) {//生成预收款单
			T_Sort_Prepay prepay = new T_Sort_Prepay();
			prepay.setPp_date(DateUtil.getCurrentTime());
			prepay.setPp_shop_code(settle.getSt_shop_code());
			prepay.setPp_type(0);
			prepay.setPp_money(settle.getSt_receivedmore());
			prepay.setPp_maker(user.getUs_name());
			prepay.setPp_manager(settle.getSt_manager());
			prepay.setPp_ba_code(settle.getSt_ba_code());
			prepay.setPp_remark("店铺结算单["+number+"]整单结算剩余金额转入本单据。");
			prepay.setPp_ar_state(CommonUtil.AR_STATE_APPROVED);
			prepay.setPp_ar_date(DateUtil.getCurrentTime());
			prepay.setPp_sysdate(DateUtil.getCurrentTime());
			prepay.setPp_us_id(user.getUs_id());
			prepay.setCompanyid(user.getCompanyid());
			sortPrepayDAO.save(prepay);;
			settle.setSt_pp_number(prepay.getPp_number());
			sortSettleDAO.updatePpNumber(settle);
		}
		T_Sort_Dealings dealings = new T_Sort_Dealings();
		dealings.setDl_shop_code(settle.getSt_shop_code());
		dealings.setDl_number(settle.getSt_number());
		dealings.setDl_type(CommonUtil.SORTDEALINGS_TYPE_JS);
		dealings.setDl_discount_money(settle.getSt_discount_money());
		dealings.setDl_receivable(0d);
		dealings.setDl_received(settle.getSt_received());
		dealings.setDl_debt(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay()+settle.getSt_receivedmore());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		String dl_remark = settle.getSt_remark();
		if(settle.getSt_prepay() != 0){
			dl_remark = "(使用预收款金额："+String.format("%.2f", settle.getSt_prepay())+")";
		}
		dealings.setDl_remark(dl_remark);
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		sortDealingsDAO.save(dealings);
		if(StringUtil.isNotEmpty(settle.getSt_pp_number())){//生成预收款单明细账
			dealings = new T_Sort_Dealings();
			dealings.setDl_shop_code(settle.getSt_shop_code());
			dealings.setDl_number(settle.getSt_pp_number());
			dealings.setDl_type(CommonUtil.SORTDEALINGS_TYPE_YSK);
			dealings.setDl_discount_money(0d);
			dealings.setDl_receivable(0d);
			dealings.setDl_received(settle.getSt_receivedmore());
			dealings.setDl_debt(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			sortDealingsDAO.save(dealings);
		}
		//更改批发销售单金额数据
		List<T_Sort_Allot> allots = sortSettleDAO.listAllotBySettle(number, user.getCompanyid());
		if (allots.size() > 0) {
			for (T_Sort_Allot allot : allots) {
				double sum = Math.abs(allot.getAt_discount_money()+allot.getAt_prepay()+allot.getAt_received());
				if (sum == 0) {
					allot.setAt_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(allot.getAt_sendmoney())) {
					allot.setAt_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					allot.setAt_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			sortSettleDAO.updateAllotBySettle(allots);
		}
		//更新客户费用单金额数据
		List<T_Sort_Fee> fees = sortSettleDAO.listFeeBySettle(number, user.getCompanyid());
		if (fees.size() > 0) {
			for (T_Sort_Fee fee : fees) {
				double sum = Math.abs(fee.getFe_discount_money()+fee.getFe_prepay()+fee.getFe_received());
				if (sum == 0) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(fee.getFe_money())) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			sortSettleDAO.updateFeeBySettle(fees);
		}
		if(settle.getSt_received() != 0 || settle.getSt_receivedmore() > 0){//更改银行帐户余额
			T_Money_Bank bank = bankDAO.queryByCode(settle.getSt_ba_code(), user.getCompanyid());
			bank.setBa_balance(bank.getBa_balance()+settle.getSt_received()+settle.getSt_receivedmore());
			bankDAO.updateBalanceById(bank);
			if(settle.getSt_received() != 0){//店铺结算单银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance()-settle.getSt_receivedmore());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_SORTSETTLE);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				if(settle.getSt_received()>0){
					bankRun.setBr_enter(Math.abs(settle.getSt_received()));
				}else {
					bankRun.setBr_out(Math.abs(settle.getSt_received()));
				}
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(number);
				bankRun.setBr_remark("店铺结算单");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
			if(settle.getSt_receivedmore() > 0){//预收款银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_SORTPREPAY);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				bankRun.setBr_enter(settle.getSt_receivedmore());
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(settle.getSt_pp_number());
				bankRun.setBr_remark("店铺预收款");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
		}
		return settle;
	}
	
	@Override
	@Transactional
	public T_Sort_Settle reverse(String number, T_Sys_User user) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sort_Settle settle = sortSettleDAO.check(number, user.getCompanyid());
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_APPROVED.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据未审核或审核未通过");
		}
		
		//1.更新单据审核状态
		settle.setSt_ar_state(CommonUtil.AR_STATE_NOTAPPROVE);
		settle.setSt_ar_date(DateUtil.getCurrentTime());
		sortSettleDAO.updateApprove(settle);
		//2.保存审核记录表
		T_Approve_Record record = new T_Approve_Record();
		record.setAr_state(CommonUtil.AR_STATE_REVERSE_APPROVE);
		record.setAr_describe(user.getUs_name()+"反审核单据");
		record.setAr_number(number);
		record.setAr_sysdate(DateUtil.getCurrentTime());
		record.setAr_us_name(user.getUs_name());
		record.setAr_type("t_sort_settle");
		record.setCompanyid(user.getCompanyid());
		approveRecordDAO.save(record);
		
		//3更新批发客户财务&删除往来明细账
		T_Base_Shop shop = shopDAO.loadShop(settle.getSt_shop_code(), user.getCompanyid());
		shop.setSp_receivable(shop.getSp_receivable()+settle.getSt_discount_money());
		shop.setSp_received(shop.getSp_received()-settle.getSt_prepay()-settle.getSt_received());
		shop.setSp_prepay(shop.getSp_prepay()+settle.getSt_prepay()-settle.getSt_receivedmore());
		shopDAO.updateSettle(shop);
		T_Sort_Dealings dealings = new T_Sort_Dealings();
		dealings.setDl_shop_code(settle.getSt_shop_code());
		dealings.setDl_number(settle.getSt_number());
		dealings.setDl_type(CommonUtil.SORTDEALINGS_TYPE_JS);
		dealings.setDl_discount_money(-settle.getSt_discount_money());
		dealings.setDl_receivable(0d);
		dealings.setDl_received(-settle.getSt_received());
		dealings.setDl_debt(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay()-settle.getSt_receivedmore());
		dealings.setDl_date(DateUtil.getCurrentTime());
		dealings.setDl_manager(user.getUs_name());
		dealings.setDl_remark("反审核恢复账目往来明细");
		dealings.setDl_sysdate(DateUtil.getCurrentTime());
		dealings.setCompanyid(user.getCompanyid());
		sortDealingsDAO.save(dealings);
		if(StringUtil.isNotEmpty(settle.getSt_pp_number())){//删除预收款单及其往来明细
			sortPrepayDAO.del(settle.getSt_pp_number(), user.getCompanyid());
			dealings = new T_Sort_Dealings();
			dealings.setDl_shop_code(settle.getSt_shop_code());
			dealings.setDl_number(settle.getSt_pp_number());
			dealings.setDl_type(CommonUtil.SORTDEALINGS_TYPE_YSK);
			dealings.setDl_discount_money(0d);
			dealings.setDl_receivable(0d);
			dealings.setDl_received(-settle.getSt_receivedmore());
			dealings.setDl_debt(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay());
			dealings.setDl_date(DateUtil.getCurrentTime());
			dealings.setDl_manager(user.getUs_name());
			dealings.setDl_remark("反审核恢复账目往来明细");
			dealings.setDl_sysdate(DateUtil.getCurrentTime());
			dealings.setCompanyid(user.getCompanyid());
			sortDealingsDAO.save(dealings);
		}
		
		//更改批发销售单金额数据
		List<T_Sort_Allot> allots = sortSettleDAO.listAllotBySettle_Reverse(number, user.getCompanyid());
		if (allots.size() > 0) {
			for (T_Sort_Allot allot : allots) {
				double sum = Math.abs(allot.getAt_discount_money()+allot.getAt_prepay()+allot.getAt_received());
				if (sum == 0) {
					allot.setAt_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(allot.getAt_sendmoney())) {
					allot.setAt_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					allot.setAt_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			sortSettleDAO.updateAllotBySettle(allots);
		}
		//更新客户费用单金额数据
		List<T_Sort_Fee> fees = sortSettleDAO.listFeeBySettle_Reverse(number, user.getCompanyid());
		if (fees.size() > 0) {
			for (T_Sort_Fee fee : fees) {
				double sum = Math.abs(fee.getFe_discount_money()+fee.getFe_prepay()+fee.getFe_received());
				if (sum == 0) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_UNPAY);
				} else if (sum < Math.abs(fee.getFe_money())) {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_PART);
				} else {
					fee.setFe_pay_state(CommonUtil.PAY_STATE_FINISH);
				}
			}
			sortSettleDAO.updateFeeBySettle(fees);
		}
		if(settle.getSt_received() != 0 || settle.getSt_receivedmore() > 0){//更改银行帐户余额
			T_Money_Bank bank = bankDAO.queryByCode(settle.getSt_ba_code(), user.getCompanyid());
			bank.setBa_balance(bank.getBa_balance()-settle.getSt_received()-settle.getSt_receivedmore());
			bankDAO.updateBalanceById(bank);
			if(settle.getSt_received() != 0){//店铺结算单银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance()+settle.getSt_receivedmore());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_SORTSETTLE);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				if(settle.getSt_received()>0){
					bankRun.setBr_out(Math.abs(settle.getSt_received()));
				}else {
					bankRun.setBr_enter(Math.abs(settle.getSt_received()));
				}
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(number);
				bankRun.setBr_remark("反审核恢复账目");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
			if(settle.getSt_receivedmore() > 0){//预收款银行流水账
				T_Money_BankRun bankRun = new T_Money_BankRun();
				bankRun.setBr_ba_code(settle.getSt_ba_code());
				bankRun.setBr_balance(bank.getBa_balance());
				bankRun.setBr_bt_code(CommonUtil.BANKRUN_SORTPREPAY);
				bankRun.setBr_date(DateUtil.getCurrentTime());
				bankRun.setBr_out(settle.getSt_receivedmore());
				bankRun.setBr_manager(settle.getSt_manager());
				bankRun.setBr_number(settle.getSt_pp_number());
				bankRun.setBr_remark("反审核恢复账目");
				bankRun.setBr_shop_code(bank.getBa_shop_code());
				bankRun.setBr_sysdate(DateUtil.getCurrentTime());
				bankRun.setCompanyid(user.getCompanyid());
				bankRunDAO.save(bankRun);
			}
		}
		return settle;
	}
	
	@Override
	@Transactional
	public void initUpdate(String number, Integer us_id, Integer companyid) {
		List<T_Sort_SettleList> details = sortSettleDAO.detail_list(number, companyid);
		for(T_Sort_SettleList item:details){
			item.setStl_us_id(us_id);
			item.setStl_join(1);
		}
		sortSettleDAO.temp_clear(us_id, companyid);
		sortSettleDAO.temp_save(details);
	}
	
	@Override
	@Transactional
	public void del(String number, Integer companyid) {
		if (number == null) {
			throw new IllegalArgumentException("参数number不能为null");
		}
		T_Sort_Settle settle = sortSettleDAO.check(number, companyid);
		if(settle == null){
			throw new RuntimeException("单据不存在");
		}
		if(!CommonUtil.AR_STATE_NOTAPPROVE.equals(settle.getSt_ar_state()) && !CommonUtil.AR_STATE_FAIL.equals(settle.getSt_ar_state())){
			throw new RuntimeException("单据已审核通过不能删除！");
		}
		sortSettleDAO.del(number, companyid);
	}
	
	@Override
	public Map<String, Object> loadPrintData(String number, Integer sp_id, T_Sys_User user) {
		Map<String, Object> resultMap = printDAO.loadPrint4Bill(sp_id);
		T_Sort_Settle settle = sortSettleDAO.load(number, user.getCompanyid());
		List<T_Sort_SettleList> settleList = sortSettleDAO.detail_list(number, user.getCompanyid());
		resultMap.put("settle", settle);
		resultMap.put("settleList", settleList);
		resultMap.put("shop", shopDAO.load(settle.getSt_shop_code(), user.getCompanyid()));
		return resultMap;
	}
	
}
