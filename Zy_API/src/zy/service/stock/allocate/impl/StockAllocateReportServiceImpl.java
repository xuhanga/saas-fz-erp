package zy.service.stock.allocate.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.stock.allocate.StockAllocateReportDAO;
import zy.dto.stock.allocate.StockAllocateDetailReportDto;
import zy.dto.stock.allocate.StockAllocateReportDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.service.stock.allocate.StockAllocateReportService;
import zy.util.CommonUtil;

@Service
public class StockAllocateReportServiceImpl implements StockAllocateReportService{
	@Resource
	private StockAllocateReportDAO stockAllocateReportDAO;
	
	@Override
	public PageData<StockAllocateReportDto> pageAllocateReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = stockAllocateReportDAO.countsumAllocateReport(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<StockAllocateReportDto> list = stockAllocateReportDAO.listAllocateReport(params);
		PageData<StockAllocateReportDto> pageData = new PageData<StockAllocateReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
	
	@Override
	public PageData<StockAllocateDetailReportDto> pageAllocateDetailReport(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		
		Map<String, Object> sumMap = stockAllocateReportDAO.countsumAllocateDetailReport(params);
		int totalCount = Integer.parseInt(sumMap.get("totalCount").toString());
		PageInfo pageInfo = new PageInfo(totalCount, pageSize, pageIndex);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		
		List<StockAllocateDetailReportDto> list = stockAllocateReportDAO.listAllocateDetailReport(params);
		PageData<StockAllocateDetailReportDto> pageData = new PageData<StockAllocateDetailReportDto>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(sumMap);
		return pageData;
	}
}
