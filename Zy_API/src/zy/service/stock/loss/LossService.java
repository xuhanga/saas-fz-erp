package zy.service.stock.loss;

import java.util.List;
import java.util.Map;

import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.base.product.T_Base_Product;
import zy.entity.stock.loss.T_Stock_Loss;
import zy.entity.stock.loss.T_Stock_LossList;
import zy.entity.sys.user.T_Sys_User;

public interface LossService {
	PageData<T_Stock_Loss> page(Map<String,Object> params);
	T_Stock_Loss load(Integer lo_id);
	List<T_Stock_LossList> detail_list(Map<String, Object> params);
	List<T_Stock_LossList> detail_sum(Map<String, Object> params);
	Map<String, Object> detail_size_title(Map<String, Object> params);
	Map<String, Object> detail_size(Map<String, Object> params);
	List<T_Stock_LossList> temp_list(Map<String, Object> params);
	List<T_Stock_LossList> temp_sum(Map<String, Object> params);
	Map<String, Object> temp_size_title(Map<String, Object> params);
	Map<String, Object> temp_size(Map<String, Object> params);
	Map<String, Object> temp_save_bybarcode(Map<String, Object> params);
	PageData<T_Base_Product> page_product(Map<String, Object> param);
	Map<String, Object> temp_loadproduct(Map<String, Object> params);
	void temp_save(Map<String, Object> params);
	void temp_import(Map<String, Object> params);
	void temp_import_draft(Map<String, Object> params);
	void temp_updateAmount(T_Stock_LossList temp);
	void temp_updateRemarkById(T_Stock_LossList temp);
	void temp_updateRemarkByPdCode(T_Stock_LossList temp);
	void temp_del(Integer lol_id);
	void temp_delByPiCode(T_Stock_LossList temp);
	void temp_clear(Integer us_id,Integer companyid);
	void save(T_Stock_Loss loss, T_Sys_User user);
	void update(T_Stock_Loss loss, T_Sys_User user);
	T_Stock_Loss approve(String number, T_Approve_Record record, T_Sys_User user);
	T_Stock_Loss reverse(String number, T_Sys_User user);
	void initUpdate(String number, Integer us_id, Integer companyid);
	void del(String number, Integer companyid);
	Map<String, Object> loadPrintData(String number, Integer sp_id, Integer displayMode, T_Sys_User user);
}
