package zy.service.stock.useable;

import java.util.List;

import zy.entity.stock.useable.T_Stock_Useable;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;

public interface UseableService {
	List<T_Stock_Useable> list(T_Sys_User user);
	void update(List<T_Stock_Useable> datas,T_Sys_Set set);
}
