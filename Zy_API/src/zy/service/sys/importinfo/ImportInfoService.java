package zy.service.sys.importinfo;

import java.util.Map;

import zy.entity.sys.improtinfo.T_Import_Info;

public interface ImportInfoService {
	T_Import_Info queryImportInfo(Map<String,Object> param);
}
