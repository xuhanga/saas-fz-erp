package zy.service.sys.importinfo.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.sys.importinfo.ImportInfoDAO;
import zy.entity.sys.improtinfo.T_Import_Info;
import zy.service.sys.importinfo.ImportInfoService;

@Service
public class ImportInfoServiceImpl implements ImportInfoService{
	@Resource
	private ImportInfoDAO importInfoDAO;
	
	@Override
	public T_Import_Info queryImportInfo(Map<String, Object> param) {
		return importInfoDAO.queryImportInfo(param);
	}
}
