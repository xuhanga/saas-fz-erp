package zy.service.sys.init;

import zy.entity.sys.user.T_Sys_User;

public interface InitService {
	void enableAccount(T_Sys_User user);
}
