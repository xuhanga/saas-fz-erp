package zy.service.sys.print;

import java.util.List;
import java.util.Map;

import zy.entity.common.print.Common_Print;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.print.T_Sys_PrintSet;
import zy.entity.sys.user.T_Sys_User;

public interface PrintService {
	List<Common_Print> listCommonPrint(String pt_type);
	List<T_Sys_Print> list(Integer sp_type,String sp_shop_code,Integer companyid);
	Map<String, Object> loadPrint(Integer sp_id);
	Map<String, Object> loadDefaultPrint(Integer pt_type);
	void save(T_Sys_Print print, List<T_Sys_PrintField> printFields, List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets,T_Sys_User user);
	void save(T_Sys_Print print, List<T_Sys_PrintField> printFields, List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets,T_Sell_Cashier cashier);
	void update(T_Sys_Print print, List<T_Sys_PrintField> printFields, List<T_Sys_PrintData> printDatas, List<T_Sys_PrintSet> printSets);
	void del(Integer sp_id);
}
