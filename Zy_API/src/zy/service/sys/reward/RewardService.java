package zy.service.sys.reward;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.sys.reward.T_Sys_Reward;
import zy.entity.sys.user.T_Sys_User;

public interface RewardService {
	PageData<T_Sys_Reward> page(Map<String, Object> params);
	T_Sys_Reward load(Integer rw_id);
	void save(T_Sys_Reward reward, T_Sys_User user);
	void update(T_Sys_Reward reward, T_Sys_User user);
	void del(Integer rw_id);
}
