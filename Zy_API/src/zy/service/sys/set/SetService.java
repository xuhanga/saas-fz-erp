package zy.service.sys.set;

import zy.entity.sys.set.T_Sys_Set;

public interface SetService {
	T_Sys_Set load(Integer companyid);
	void update(T_Sys_Set set);
}
