package zy.service.sys.set.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sys.set.SetDAO;
import zy.entity.sys.set.T_Sys_Set;
import zy.service.sys.set.SetService;

@Service
public class SetServiceImpl implements SetService{
	@Resource
	private SetDAO setDAO;

	@Override
	public T_Sys_Set load(Integer companyid) {
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return setDAO.load(companyid);
	}

	@Override
	@Transactional
	public void update(T_Sys_Set set) {
		if (set.getSt_id() == null) {
			setDAO.save(set);
		} else {
			setDAO.update(set);
		}
	}
	
}
