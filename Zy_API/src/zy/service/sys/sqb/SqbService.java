package zy.service.sys.sqb;

import zy.entity.sys.sqb.T_Sys_Sqb;

public interface SqbService {
	T_Sys_Sqb load(Integer sqb_id);
	void save(T_Sys_Sqb sqb);
}
