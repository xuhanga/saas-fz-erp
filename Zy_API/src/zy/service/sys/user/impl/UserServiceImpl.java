package zy.service.sys.user.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.sys.user.UserDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.user.UserService;
import zy.util.CommonUtil;
import zy.util.MD5;
import zy.util.StringUtil;
@Service
public class UserServiceImpl implements UserService{
	@Resource
	private UserDAO userDAO;
	@Override
	public PageData<T_Sys_User> page(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		Object pageSize = param.get(CommonUtil.PAGESIZE);
		Object pageIndex = param.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = userDAO.count(param);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		param.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		param.put(CommonUtil.END, _pageSize);
		
		List<T_Sys_User> list = userDAO.list(param);
		PageData<T_Sys_User> pageData = new PageData<T_Sys_User>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public Integer queryByName(T_Sys_User model) {
		return userDAO.queryByName(model);
	}

	@Override
	public T_Sys_User queryByID(Integer id) {
		return userDAO.queryByID(id);
	}

	@Override
	public void update(T_Sys_User model) {
		userDAO.update(model);
	}

	@Override
	public void updateState(T_Sys_User model) {
		userDAO.updateState(model);
	}

	@Override
	public void save(T_Sys_User model) {
		userDAO.save(model);
	}

	@Override
	public void del(Integer id) {
		userDAO.del(id);
	}

	@Override
	public void initPwd(Integer id) {
		userDAO.initPwd(id);
	}

	@Override
	public void changePassword(Integer us_id, String old_pwd, String us_pwd) {
		if (StringUtil.isEmpty(us_id)) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String now_pwd = userDAO.getPwd(us_id);
		old_pwd = MD5.encryptMd5(old_pwd);
		us_pwd = MD5.encryptMd5(us_pwd);
		if(now_pwd != null && !now_pwd.equals(old_pwd)){
			throw new IllegalArgumentException("当前密码不正确，请从新输入!");
		}
		userDAO.changePassword(us_id,us_pwd);
	}

}
