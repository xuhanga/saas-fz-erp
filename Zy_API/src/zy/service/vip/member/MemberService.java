package zy.service.vip.member;

import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import zy.dto.base.emp.EmpLoginDto;
import zy.entity.PageData;
import zy.entity.base.product.T_Base_Product;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.member.T_Vip_ConsumeDetailList;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.entity.vip.member.T_Vip_PonitList;
import zy.entity.vip.member.T_Vip_Tag;
import zy.entity.vip.trylist.T_Vip_TryList;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.form.NumberForm;

public interface MemberService {
	PageData<T_Vip_Member> page(Map<String,Object> params);
	T_Vip_Member queryByID(Integer vm_id);
	T_Vip_Member_Info queryInfoByCode(String vmi_code, Integer companyid);
	void save(T_Vip_Member member, T_Vip_Member_Info memberInfo,T_Sys_User user);
	void update(T_Vip_Member member, T_Vip_Member_Info memberInfo, T_Sys_User user);
	void save(T_Vip_Member member,T_Vip_Member_Info memberInfo);
	void update(T_Vip_Member member, T_Vip_Member_Info memberInfo);
	void del(Map<String, Object> params);
	PageData<T_Vip_Member> assign_page(Map<String,Object> params);
	void update_assign_emp(Map<String,Object> params);
	void save_visit(T_Vip_Visit visit);
	PageData<T_Vip_ConsumeDetailList> sellList(Map<String,Object> param);
	public List<T_Sell_Shop> shopList(Map<String, Object> paramMap);
	List<T_Vip_TryList> tryList(Map<String,Object> paramMap);
	List<T_Vip_Visit> backList(Map<String,Object> paramMap);
	List<NumberForm> timeList(Map<String,Object> paramMap);
	List<T_Base_Product> pushList(Map<String,Object> paramMap);
	List<T_Sell_Ecoupon_User> ecouponList(Map<String,Object> paramMap);
	Map<String, Object> parseSavingCardInfoExcel(Workbook soureWorkBook, WritableWorkbook errsheet, Map<String,Object> param) throws Exception;
	void saveImport(Map<String,Object> param);
	List<T_Vip_Tag> query_vip_tag(Map<String,Object> param);
	void del_vip_tag(Map<String, Object> params);
	void set_member_tag(Map<String, Object> params);
	void save_vip_tag(Map<String, Object> params);
	PageData<T_Vip_Member> birthday_list(Map<String,Object> params);
	//------------------------前台---------------------------
	PageData<T_Vip_Member> pageByShop(Map<String,Object> params);
	T_Vip_Member vipByCode(Map<String,Object> param);
	T_Vip_Member vipByMobile(Map<String,Object> param);
	T_Vip_Member vipById(Integer vm_id);
	void updatePoint(Map<String,Object> param);
	List<T_Sell_ShopList> listSell(Map<String,Object> param);
	List<T_Vip_PonitList> listPoint(Map<String,Object> param);
	//------------------------接口---------------------------
	List<T_Vip_Member> list_server(Map<String,Object> params);
	void save(T_Vip_Member member, T_Vip_Member_Info memberInfo,EmpLoginDto empLoginDto);
	void update(T_Vip_Member member, T_Vip_Member_Info memberInfo,EmpLoginDto empLoginDto);
	void updateImg(T_Vip_Member member);
	T_Vip_Member search(Map<String, Object> params);
}
