package zy.service.vip.member.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.depot.DepotDAO;
import zy.dao.sys.importinfo.ImportInfoDAO;
import zy.dao.vip.member.MemberDAO;
import zy.dto.base.emp.EmpLoginDto;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.base.brand.T_Base_Brand;
import zy.entity.base.color.T_Base_Color;
import zy.entity.base.depot.T_Base_Depot;
import zy.entity.base.emp.T_Base_Emp;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.base.type.T_Base_Type;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.sys.improtinfo.T_Import_Info;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.member.T_Vip_ConsumeDetailList;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.entity.vip.member.T_Vip_PonitList;
import zy.entity.vip.member.T_Vip_Tag;
import zy.entity.vip.membertype.T_Vip_MemberType;
import zy.entity.vip.trylist.T_Vip_TryList;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.form.NumberForm;
import zy.service.vip.member.MemberService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.ExcelImportUtil;
import zy.util.LunarCalendar;
import zy.util.MD5;
import zy.util.NumberUtil;
import zy.util.StringUtil;

@Service
public class MemberServiceImpl implements MemberService{
	@Resource
	private MemberDAO memberDAO;
	
	@Resource
	private ImportInfoDAO importInfoDAO;
	
	@Resource
	private DepotDAO depotDAO;

	@Override
	public PageData<T_Vip_Member> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String lossDay = (String)params.get("lossDay");
		String consumeDay = (String)params.get("consumeDay");
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -Integer.parseInt(lossDay));//流失
		lossDay = DateUtil.format(cal, "yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -Integer.parseInt(consumeDay));//活跃日期
		consumeDay = DateUtil.format(calendar, "yyyy-MM-dd");
		params.put("lossDay", lossDay);
		params.put("consumeDay", consumeDay);
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = memberDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = memberDAO.list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
	@Override
	public PageData<T_Vip_Member> birthday_list(Map<String, Object> params) {
		int smsDays = 0;//生日短信提前通知天数
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String agowarnDay = (String)params.get("agowarnDay");
		String consumeDay = (String)params.get("consumeDay");
		String CurrentMode = (String)params.get("CurrentMode");;//0:今日  1:最近 2:本月
		String begindate = (String)params.get("begindate");
		String enddate = (String)params.get("enddate");
		
		
		if( StringUtil.isNotEmpty(agowarnDay) ){
			smsDays = Integer.parseInt(agowarnDay);
		}
		if( StringUtil.isEmpty(consumeDay) ){
			consumeDay = "90";
		}
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -Integer.parseInt(consumeDay));//活跃日期
		String consumeDate = DateUtil.format(calendar, "yyyy-MM-dd");
		params.put("consumeDate", consumeDate);
		
		if( "0".equalsIgnoreCase(CurrentMode) ){
			begindate = DateUtil.getYearMonthDate();
			enddate = DateUtil.getYearMonthDate();
		}else if("1".equalsIgnoreCase(CurrentMode) && smsDays>0){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 1);
			begindate = DateUtil.format(cal, "yyyy-MM-dd");
			cal.add(Calendar.DATE, smsDays-1);
			enddate = DateUtil.format(cal, "yyyy-MM-dd");
		}
		else if("2".equalsIgnoreCase(CurrentMode)){
			begindate = DateUtil.monthStartDay(DateUtil.getYearMonthDate());
			enddate = DateUtil.monthEndDay(begindate);
		}
		if(StringUtil.isEmpty(begindate)||StringUtil.isEmpty(enddate)){
			begindate = enddate = DateUtil.getYearMonthDate();
		}
		params.put("begindate", begindate);
		params.put("enddate", enddate);
		
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = memberDAO.birthday_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = memberDAO.birthday_list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(memberDAO.birthday_sum(params));
		return pageData;
	}


	@Override
	public PageData<T_Vip_Member> pageByShop(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = memberDAO.countByShop(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = memberDAO.listByShop(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}

	@Override
	public T_Vip_Member vipByCode(Map<String, Object> param) {
		return memberDAO.vipByCode(param);
	}

	
	@Override
	public T_Vip_Member vipByMobile(Map<String, Object> param) {
		return memberDAO.vipByMobile(param);
	}

	@Override
	public T_Vip_Member queryByID(Integer vm_id) {
		return memberDAO.queryByID(vm_id);
	}

	@Override
	public T_Vip_Member vipById(Integer vm_id) {
		return memberDAO.vipById(vm_id);
	}

	@Override
	public T_Vip_Member_Info queryInfoByCode(String vmi_code, Integer companyid) {
		return memberDAO.queryInfoByCode(vmi_code, companyid);
	}

	@Override
	@Transactional
	public void save(T_Vip_Member member, T_Vip_Member_Info memberInfo, T_Sys_User user) {
		if(member == null || memberInfo == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(member.getVm_cardcode())){
			throw new IllegalArgumentException("会员卡号不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_mobile())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		Integer id = memberDAO.queryByCardCode(member, user);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		Integer _id = memberDAO.queryByMobile(member, user);
		if (null != _id && _id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		if(StringUtil.isNotEmpty(member.getVm_password())){
			member.setVm_password(MD5.encryptMd5(member.getVm_password()));
		}else {
			member.setVm_password(MD5.encryptMd5(CommonUtil.INIT_PWD));
		}
		member.setVm_points(member.getVm_init_points());
		member.setVm_used_points(0d);
		member.setVm_times(0);
		member.setVm_total_money(0d);
		memberDAO.save(member, memberInfo);
	}
	
	@Override
	@Transactional
	public void update(T_Vip_Member member, T_Vip_Member_Info memberInfo, T_Sys_User user) {
		if(member == null || memberInfo == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(member.getVm_id() == null){
			throw new IllegalArgumentException("参数vm_id不能为空");
		}
		if(memberInfo.getVmi_id() == null){
			throw new IllegalArgumentException("参数vmi_id不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_cardcode())){
			throw new IllegalArgumentException("会员卡号不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_mobile())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		Integer id = memberDAO.queryByCardCode(member, user);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("会员卡号["+member.getVm_cardcode()+"]已经存在");
		}
		Integer _id = memberDAO.queryByMobile(member, user);
		if (null != _id && _id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		memberDAO.update(member, memberInfo);
	}
	
	@Override
	@Transactional
	public void save(T_Vip_Member member, T_Vip_Member_Info memberInfo) {
		if(member == null || memberInfo == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(member.getVm_cardcode() == null || "".equals(member.getVm_cardcode())){
			throw new IllegalArgumentException("会员卡号不能为空");
		}
		if(member.getVm_mobile() == null || "".equals(member.getVm_mobile())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		Integer id = memberDAO.queryByCardCode(member);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		Integer _id = memberDAO.queryByMobile(member);
		if (null != _id && _id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		
		/*if(member.getVm_password() != null && !"".equals(member.getVm_password())){
			member.setVm_password(MD5.encryptMd5(member.getVm_password()));
		}*/
		member.setVm_points(member.getVm_init_points());
		member.setVm_used_points(0d);
		member.setVm_times(0);
		member.setVm_total_money(0d);
		memberDAO.save(member, memberInfo);
	}
	
	@Override
	@Transactional
	public void update(T_Vip_Member member, T_Vip_Member_Info memberInfo) {
		try {
		if(member == null || memberInfo == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(member.getVm_id() == null){
			throw new IllegalArgumentException("参数vm_id不能为空");
		}
		if(memberInfo.getVmi_id() == null){
			throw new IllegalArgumentException("参数vmi_id不能为空");
		}
		if(member.getVm_cardcode() == null || "".equals(member.getVm_cardcode())){
			throw new IllegalArgumentException("会员卡号不能为空");
		}
		if(member.getVm_mobile() == null || "".equals(member.getVm_mobile())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		Integer id = memberDAO.queryByCardCode(member);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("会员卡号["+member.getVm_cardcode()+"]已经存在");
		}
		Integer _id = memberDAO.queryByMobile(member);
		if (null != _id && _id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
/*		if(member.getVm_password() != null && !"".equals(member.getVm_password())){
			member.setVm_password(MD5.encryptMd5(member.getVm_password()));
		}*/
			memberDAO.update(member, memberInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	@Transactional
	public void del(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		Integer vm_id = (Integer)params.get("vm_id");
		String vm_code = (String)params.get("vm_code");
		memberDAO.del(vm_id);
		Integer vmi_id = memberDAO.queryInfoIdByCode(vm_code, companyid);
		if(vmi_id != null){
			memberDAO.delInfo(vmi_id);
		}
	}
	
	@Override
	public PageData<T_Vip_Member> assign_page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = memberDAO.assign_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = memberDAO.assign_list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(memberDAO.assign_sum(params));
		return pageData;
	}

	@Override
	@Transactional
	public void update_assign_emp(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		String rowId = (String)params.get("rowIds");
		String code = (String)params.get("codes");
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(StringUtil.isEmpty(code)||StringUtil.isEmpty(rowId)){
			throw new IllegalArgumentException("数据异常，分配失败！");
		}
		String[] codes = code.split(",");
		String[] rowIds = rowId.split(","); 
		if( codes.length>rowIds.length ){
			throw new IllegalArgumentException("选择办卡人员太多，无法平均分配");
		}
		memberDAO.update_assign_emp(params);
	}

	@Override
	@Transactional
	public void save_visit(T_Vip_Visit visit) {
		if(visit == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(visit.getVi_manager())){
			throw new IllegalArgumentException("回访人员不能为空");
		}
		if(StringUtil.isEmpty(visit.getVi_manager_code())){
			throw new IllegalArgumentException("回访人员不能为空");
		}
		if(StringUtil.isEmpty(visit.getVi_content())){
			throw new IllegalArgumentException("回访内容不能为空");
		}
		memberDAO.save_visit(visit);
		if(visit.getVi_is_arrive() == 1){
			memberDAO.save_specialfocus(visit);//特别关注
		}
		if(visit.getVi_is_arrive() == 2){
			memberDAO.save_reachshop(visit);//意向到店
		}
	}
	
	@Override
	public PageData<T_Vip_ConsumeDetailList> sellList(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Map<String,Object> map = memberDAO.sell_count(params);
		PageInfo pageInfo = new PageInfo(NumberUtil.toInteger(map.get("conut")), _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_ConsumeDetailList> list = memberDAO.sell_list(params);
		PageData<T_Vip_ConsumeDetailList> pageData = new PageData<T_Vip_ConsumeDetailList>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(map);
		return pageData;
	}

	@Override
	public List<T_Sell_Shop> shopList(Map<String, Object> paramMap) {
		return memberDAO.shopList(paramMap);
	}

	@Override
	public List<T_Vip_TryList> tryList(Map<String, Object> paramMap) {
		return memberDAO.tryList(paramMap);
	}

	@Override
	public List<T_Sell_Ecoupon_User> ecouponList(Map<String, Object> paramMap) {
		return memberDAO.ecouponList(paramMap);
	}

	@Override
	public List<T_Vip_Visit> backList(Map<String, Object> paramMap) {
		return memberDAO.backList(paramMap);
	}
	
	@Override
	public List<NumberForm> timeList(Map<String, Object> paramMap) {
		return memberDAO.timeList(paramMap);
	}

	@Override
	public List<T_Base_Product> pushList(Map<String, Object> paramMap) {
		Object companyid = paramMap.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		//获取仓库信息
		List<T_Base_Depot> deportList = depotDAO.list4stock(paramMap);
		//获取会员经常购买的品牌
		List<T_Base_Brand> often_brand = memberDAO.queryVipOftenBrand(paramMap);
		//获取会员经常购买的类别
		List<T_Base_Type> often_type = memberDAO.queryVipOftenType(paramMap);
		//获取会员经常购买的颜色
		List<T_Base_Color> often_color = memberDAO.queryVipOftenColor(paramMap);
		//查询会员消费能力
		Map<String, Object> often_price = memberDAO.queryVipOftenPrice(paramMap);
		
		if(often_brand == null && often_type == null && often_color == null){
			return null;
		}
		
		if(deportList != null && deportList.size()>0){//拼接仓库查询条件
			StringBuffer deport = new StringBuffer();
			for(int i=0;i<deportList.size();i++){
				deport.append("'").append(deportList.get(i).getDp_code()).append("',");
			}
			String deportStr = deport.substring(0,deport.length()-1);
			paramMap.put("deport", deportStr);
		}
		
		if(often_brand != null && often_brand.size()>0){//拼接品牌查询条件
			StringBuffer oftenBrand = new StringBuffer();
			for(int i=0;i<often_brand.size();i++){
				oftenBrand.append("'").append(often_brand.get(i).getBd_code()).append("',");
			}
			String oftenBrandStr = oftenBrand.substring(0,oftenBrand.length()-1);
			paramMap.put("oftenBrand", oftenBrandStr);
		}
		
		if(often_type != null && often_type.size()>0){//拼接类别查询条件
			StringBuffer oftenType = new StringBuffer();
			for(int i=0;i<often_type.size();i++){
				oftenType.append("'").append(often_type.get(i).getTp_code()).append("',");
			}
			String oftenTypeStr = oftenType.substring(0,oftenType.length()-1);
			paramMap.put("oftenType", oftenTypeStr);
		}
		
		if(often_color != null && often_color.size()>0){//拼接颜色查询条件
			StringBuffer oftenColor = new StringBuffer();
			for(int i=0;i<often_color.size();i++){
				oftenColor.append("'").append(often_color.get(i).getCr_code()).append("',");
			}
			String oftenColorStr = oftenColor.substring(0,oftenColor.length()-1);
			paramMap.put("oftenColor", oftenColorStr);
		}
		
		paramMap.put("maxPrice", often_price.get("maxPrice"));//最大消费金额
		paramMap.put("minPrice", often_price.get("minPrice"));//最低消费金额
		return memberDAO.pushList(paramMap);
	}

	@Override
	public Map<String, Object> parseSavingCardInfoExcel(Workbook soureWorkBook,
			WritableWorkbook errsheet, Map<String,Object> param) throws Exception {
		WritableSheet errorSheet = errsheet.getSheet(0);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Integer companyid = (Integer)param.get("companyid");
		try {
			Sheet sheet = soureWorkBook.getSheet(0);
			int columns = 47;//
			int rsRows = sheet.getRows(); // 行数
			System.out.println("共计: " + (rsRows-1) + "条");
			
			//获取导入商品资料需要的数据
			Map<String, List> checkImportMember = memberDAO.getCheckImportMember(param);
			if(checkImportMember == null){
				resultMap.put("fail", 0);// 错误条数
				resultMap.put("success", 0);// 成功条数
				return resultMap;
			}
			List<T_Vip_MemberType> memberTypes = (List<T_Vip_MemberType>)checkImportMember.get("memberTypes");//会员类别
			List<T_Base_Shop> shops = (List<T_Base_Shop>)checkImportMember.get("shops");//商家店铺
			List<T_Vip_Member> members = (List<T_Vip_Member>)checkImportMember.get("members");//已存在的会员编号及手机号码
			List<T_Base_Emp> emps = (List<T_Base_Emp>)checkImportMember.get("emps");//经办人
			
			T_Vip_Member t_Vip_Member = null;
			T_Vip_Member_Info t_Vip_Member_Info = null;
			String errorCause = "";//错误原因
			int errorRow = 1;
			int successRow = 0;//导入成功行数
			List<T_Vip_Member> memberList = new ArrayList<T_Vip_Member>();//存放验证成功的会员信息
			Map<String, T_Vip_Member_Info> memberInfoMap = new HashMap<String, T_Vip_Member_Info>();//会员信息
			for (int i = 1; i < rsRows; i++) { // 如第一行为属性项则从第二行开始取数据
				t_Vip_Member = new T_Vip_Member();
				t_Vip_Member_Info = new T_Vip_Member_Info();
				if (ExcelImportUtil.isBlankRow(sheet, i, columns-1)) { // 空行则跳过
					continue;
				}
				boolean tag = false;
				
				Cell cell0 = sheet.getCell(0, i);// 手机号码
				String vm_mobile = cell0.getContents().trim();
				if("".equals(vm_mobile)){//手机号码不能为空
					errorCause = "源文件第"+(i+1)+"行， 手机号码不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(ExcelImportUtil.checkStrRegex("^1[3|4|5|7|8][0-9]{9}$", vm_mobile)){
					errorCause = "源文件第"+(i+1)+"行， 请输入正确的手机号！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<members.size();k++){
					if(vm_mobile.equals(members.get(k).getVm_mobile())){
						tag = true;
						break;
					}
				}
				if(tag){//手机号码不能和数据库中重复
					errorCause = "源文件第"+(i+1)+"行，手机号码已存在，请重新输入";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member.setVm_mobile(vm_mobile);
				
				Cell cell1 = sheet.getCell(1, i);// 会员卡号
				String vm_cardcode = cell1.getContents().trim();
				if("".equals(vm_cardcode)){//会员卡号不能为空
					errorCause = "源文件第"+(i+1)+"行， 会员卡号不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(vm_cardcode.length()>20){
					errorCause = "源文件第"+(i+1)+"行， 会员卡号最多输入20字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				tag = false;
				for(int k=0;k<members.size();k++){
					if(vm_cardcode.equals(members.get(k).getVm_cardcode())){
						tag = true;
						break;
					}
				}
				if(tag){//会员卡号不能和数据库中重复
					errorCause = "源文件第"+(i+1)+"行，会员卡号已存在，请重新输入";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member.setVm_cardcode(vm_cardcode);
				
				Cell cell2 = sheet.getCell(2, i);// 会员类别
				String vm_mt_name = cell2.getContents().trim();
				String vm_mt_code = "";
				if("".equals(vm_mt_name)){//会员类别不能为空
					errorCause = "源文件第"+(i+1)+"行， 会员类别不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				for(int k=0;k<memberTypes.size();k++){
					if(vm_mt_name.equals(memberTypes.get(k).getMt_name())){
						vm_mt_code = memberTypes.get(k).getMt_code();
						break;
					}
				}
				if("".equals(vm_mt_code)){//如果数据库中没有会员类别，则返回错误
					errorCause = "源文件第"+(i+1)+"行，会员类别不存在，请确认！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member.setVm_mt_code(vm_mt_code);
				
				Cell cell3 = sheet.getCell(3, i);// 会员姓名
				String vm_name = cell3.getContents().trim();
				if("".equals(vm_name)){//会员姓名不能为空
					errorCause = "源文件第"+(i+1)+"行， 姓名不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(vm_name.length()>15){
					errorCause = "源文件第"+(i+1)+"行， 姓名长度不能超过15个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member.setVm_name(vm_name);
				
				Cell cell4 = sheet.getCell(4, i);// 密码
				String vm_password = cell4.getContents().trim();
				if(StringUtil.isNotEmpty(vm_password)){
					if(ExcelImportUtil.checkStrRegex("^(\\w){6,16}$", vm_password)){
						errorCause = "源文件第"+(i+1)+"行， 请输入正确的密码，密码只能输入字母、数字，密码长度必须在6到16之间！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					t_Vip_Member.setVm_password(MD5.encryptMd5(vm_password));
				}else {
					t_Vip_Member.setVm_password(MD5.encryptMd5(CommonUtil.INIT_PWD));
				}
				
				Cell cell5 = sheet.getCell(5, i);//会员生日
				String vm_birthday = cell5.getContents().trim();
				if("".equals(vm_birthday)){
					t_Vip_Member.setVm_birthday(null);
					t_Vip_Member.setVm_lunar_birth(null);
				}else{
					if(!ExcelImportUtil.checkDate(vm_birthday)){
						String checkVmBirthday = ExcelImportUtil.checkCellIsDate(cell5);
						if("".equals(checkVmBirthday)){
							errorCause = "源文件第"+(i+1)+"行， 您输入的会员生日格式不正确，请重新输入！";
							ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
							errorRow++;
							continue;
						}else{
							vm_birthday = checkVmBirthday;
						}
					}
				}
				
				Cell cell6 = sheet.getCell(6, i);//生日方式 公历或农历
				String vm_birthday_type_name = cell6.getContents().trim();
				Integer vm_birthday_type = 0;
				if("".equals(vm_birthday_type_name)){
					vm_birthday_type = 0;
					if(!"".equals(vm_birthday)){
						String vm_lunar_birth = LunarCalendar.solarToLunar(vm_birthday);
						if("".equals(vm_lunar_birth)){
							errorCause = "源文件第"+(i+1)+"行， 您输入的会员生日格式不正确，请重新输入！";
							ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
							errorRow++;
							continue;
						}
						t_Vip_Member.setVm_birthday(vm_birthday);
						//获取农历日期
					    t_Vip_Member.setVm_lunar_birth(vm_lunar_birth);
					    //获取生肖
					    t_Vip_Member_Info.setVmi_anima(LunarCalendar.animalsYear(vm_birthday));
					}
				}else if("公历".equals(vm_birthday_type_name)){
					vm_birthday_type = 0;
					if(!"".equals(vm_birthday)){
						String vm_lunar_birth = LunarCalendar.solarToLunar(vm_birthday);
						if("".equals(vm_lunar_birth)){
							errorCause = "源文件第"+(i+1)+"行， 您输入的会员生日格式不正确，请重新输入！";
							ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
							errorRow++;
							continue;
						}
						t_Vip_Member.setVm_birthday(vm_birthday);
						//获取农历日期
					    t_Vip_Member.setVm_lunar_birth(vm_lunar_birth);
					    //获取生肖
					    t_Vip_Member_Info.setVmi_anima(LunarCalendar.animalsYear(vm_birthday));
					}
					
				}else if("农历".equals(vm_birthday_type_name)){
					vm_birthday_type = 1;
					if(!"".equals(vm_birthday)){
						String vm_birthdayStr = LunarCalendar.lunarToSolar(vm_birthday);
						if("".equals(vm_birthdayStr)){
							errorCause = "源文件第"+(i+1)+"行， 您输入的会员生日格式不正确，请重新输入！";
							ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
							errorRow++;
							continue;
						}
						t_Vip_Member.setVm_lunar_birth(vm_birthday);
						//获取生肖
						t_Vip_Member_Info.setVmi_anima(LunarCalendar.animalsYear(vm_birthday));
					    //获取公历生日
					    t_Vip_Member.setVm_birthday(vm_birthdayStr);
					}
				}else{
					errorCause = "源文件第"+(i+1)+"行， 您输入的生日方式格式不正确，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member.setVm_birthday_type(vm_birthday_type);
				
				Cell cell7 = sheet.getCell(7, i);//办卡日期
				String vm_date = cell7.getContents().trim();
				if("".equals(vm_date)){//办卡日期不能为空
					errorCause = "源文件第"+(i+1)+"行，办卡日期不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(!ExcelImportUtil.checkDate(vm_date)){
					String checkVmDate = ExcelImportUtil.checkCellIsDate(cell7);
					if("".equals(checkVmDate)){
						errorCause = "源文件第"+(i+1)+"行， 您输入的办卡日期格式不正确，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_date(checkVmDate);
					}
				}else {
					t_Vip_Member.setVm_date(vm_date);
				}
				
				Cell cell8 = sheet.getCell(8, i);//有效日期
				String vm_enddate = cell8.getContents().trim();
				if("".equals(vm_enddate)){//有效日期不能为空
					errorCause = "源文件第"+(i+1)+"行，有效日期不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if(!ExcelImportUtil.checkDate(vm_enddate)){
					String checkVmEndDate = ExcelImportUtil.checkCellIsDate(cell8);
					if("".equals(checkVmEndDate)){
						errorCause = "源文件第"+(i+1)+"行， 您输入的有效日期格式不正确，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_enddate(checkVmEndDate);
					}
				}else {
					t_Vip_Member.setVm_enddate(vm_enddate);
				}
				
				Cell cell9 = sheet.getCell(9, i);//性别
				String vm_sex = cell9.getContents().trim();
				if("".equals(vm_sex)){//性别不能为空
					errorCause = "源文件第"+(i+1)+"行，性别不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				if("男".equals(vm_sex) || "女".equals(vm_sex)){
					t_Vip_Member.setVm_sex(vm_sex);
				}else {
					errorCause = "源文件第"+(i+1)+"行，性别只能输入男或女！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				
				Cell cell10 = sheet.getCell(10, i);//婚姻状况
				String vmi_marriage_state_name = cell10.getContents().trim();
				Integer vmi_marriage_state = 0;
				if("".equals(vmi_marriage_state_name) || "未婚".equals(vmi_marriage_state_name)){
					vmi_marriage_state = 0;
				}else if("已婚".equals(vmi_marriage_state_name)){
					vmi_marriage_state = 1;
				}else if("曾婚".equals(vmi_marriage_state_name)){
					vmi_marriage_state = 2;
				}else {
					errorCause = "源文件第"+(i+1)+"行，婚姻状况只能输入未婚、已婚或曾婚！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_marriage_state(vmi_marriage_state);
				
				Cell cell11 = sheet.getCell(11, i);//民族
				String vmi_nation = cell11.getContents().trim();
				if("".equals(vmi_nation)){
					t_Vip_Member_Info.setVmi_nation("汉族");
				}else {
					t_Vip_Member_Info.setVmi_nation(vmi_nation);
				}
				
				Cell cell12 = sheet.getCell(12, i);//血型
				String vmi_bloodtype = cell12.getContents().trim();
				if(!"".equals(vmi_bloodtype)){
					vmi_bloodtype = vmi_bloodtype.toUpperCase();
					if(!"A".equals(vmi_bloodtype) && !"B".equals(vmi_bloodtype) 
							&& !"AB".equals(vmi_bloodtype) && !"O".equals(vmi_bloodtype)){
						errorCause = "源文件第"+(i+1)+"行，血型输入有误，请重新输入！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				t_Vip_Member_Info.setVmi_bloodtype(vmi_bloodtype);
				
				Cell cell13 = sheet.getCell(13, i);//证件类型
				String vmi_idcard_type_name = cell13.getContents().trim();
				Integer vmi_idcard_type = 0;
				if("".equals(vmi_idcard_type_name) || "身份证".equals(vmi_idcard_type_name)){
					vmi_idcard_type = 0;
				}else if("军官证".equals(vmi_idcard_type_name)){
					vmi_idcard_type = 1;
				}else if("驾照".equals(vmi_idcard_type_name)){
					vmi_idcard_type = 2;
				}else if("护照".equals(vmi_idcard_type_name)){
					vmi_idcard_type = 3;
				}else if("学生证".equals(vmi_idcard_type_name)){
					vmi_idcard_type = 4;
				}else if("其他".equals(vmi_idcard_type_name)){
					vmi_idcard_type = 5;
				}else {
					errorCause = "源文件第"+(i+1)+"行，证件类型有误，请重新输入！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_idcard_type(vmi_idcard_type);
				
				Cell cell14 = sheet.getCell(14, i);//证件号
				String vmi_idcard = cell14.getContents().trim();
				if(!"".equals(vmi_idcard) && vmi_idcard.length()>30){
					errorCause = "源文件第"+(i+1)+"行，证件号长度不能大于30个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_idcard(vmi_idcard);
				
				Cell cell15 = sheet.getCell(15, i);//工作单位
				String vmi_work_unit = cell15.getContents().trim();
				if(!"".equals(vmi_work_unit) && vmi_work_unit.length()>50){
					errorCause = "源文件第"+(i+1)+"行，工作单位长度不能大于50个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_work_unit(vmi_work_unit);
				
				Cell cell16 = sheet.getCell(16, i);//qq
				String vmi_qq = cell16.getContents().trim();
				if(!"".equals(vmi_qq) && vmi_qq.length()>20){
					errorCause = "源文件第"+(i+1)+"行，QQ长度不能大于20个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_qq(vmi_qq);
				
				Cell cell17 = sheet.getCell(17, i);//微信
				String vmi_wechat = cell17.getContents().trim();
				if(!"".equals(vmi_wechat) && vmi_wechat.length()>30){
					errorCause = "源文件第"+(i+1)+"行，微信长度不能大于30个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_wechat(vmi_wechat);
				
				Cell cell18 = sheet.getCell(18, i);//E-Mail
				String vmi_email = cell18.getContents().trim();
				if(!"".equals(vmi_email) && vmi_email.length()>50){
					errorCause = "源文件第"+(i+1)+"行，E-Mail长度不能大于50个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_email(vmi_email);
				
				Cell cell19 = sheet.getCell(19, i);//职业
				String vmi_job = cell19.getContents().trim();
				if(!"".equals(vmi_job)){
					String strJob = "老板|企业管理者|公务员|教师|职员|军人|学生|医生|其他|";
					if(!strJob.contains(vmi_job+"|")){
						errorCause = "源文件第"+(i+1)+"行，职业只能输入'老板、企业管理者、公务员、教师、职员、军人、学生、医生、其他'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				t_Vip_Member_Info.setVmi_job(vmi_job);
				
				Cell cell20 = sheet.getCell(20, i);//宗教信仰
				String vmi_faith = cell20.getContents().trim();
				if(!"".equals(vmi_faith) && vmi_faith.length()>50){
					errorCause = "源文件第"+(i+1)+"行，宗教信仰长度不能大于50个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_faith(vmi_faith);
				
				Cell cell21 = sheet.getCell(21, i);//家庭节日
				String vmi_family_festival = cell21.getContents().trim();
				if(!"".equals(vmi_family_festival) && vmi_family_festival.length()>50){
					errorCause = "源文件第"+(i+1)+"行，家庭节日长度不能大于50个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_family_festival(vmi_family_festival);
				
				Cell cell22 = sheet.getCell(22, i);//地址
				String vmi_address = cell22.getContents().trim();
				if(!"".equals(vmi_address) && vmi_address.length()>70){
					errorCause = "源文件第"+(i+1)+"行，地址长度不能大于70个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_address(vmi_address);
				
				Cell cell23 = sheet.getCell(23, i);//初始积分
				String vm_init_points = cell23.getContents().trim();
				if("".equals(vm_init_points)){
					t_Vip_Member.setVm_init_points(0.0d);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", vm_init_points)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 初始积分不正确，请输入正确的初始积分！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_init_points(Double.parseDouble(vm_init_points));
					}
				}
				
				Cell cell24 = sheet.getCell(24, i);//会员积分
				String vm_total_point = cell24.getContents().trim();
				if("".equals(vm_total_point)){
					t_Vip_Member.setVm_total_point(0.0d);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", vm_total_point)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 会员积分不正确，请输入正确的会员积分！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_total_point(Double.parseDouble(vm_total_point));
					}
				}
				
				Cell cell25 = sheet.getCell(25, i);//已用积分
				String vm_used_points = cell25.getContents().trim();
				if("".equals(vm_used_points)){
					t_Vip_Member.setVm_used_points(0.0d);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", vm_used_points)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行，已用积分不正确，请输入正确的已用积分！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_used_points(Double.parseDouble(vm_used_points));
					}
				}
				
				Cell cell26 = sheet.getCell(26, i);//剩余积分
				String vm_points = cell26.getContents().trim();
				if("".equals(vm_points)){
					t_Vip_Member.setVm_points(0.0d);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", vm_points)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行， 剩余积分不正确，请输入正确的剩余积分！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_points(Double.parseDouble(vm_points));
					}
				}
				
				Cell cell27 = sheet.getCell(27, i);//消费次数
				String vm_times = cell27.getContents().trim();
				if("".equals(vm_times)){
					t_Vip_Member.setVm_times(0);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[0-9]\\d*$", vm_times)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行，消费次数不正确，请输入正确的消费次数！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_times(Integer.parseInt(vm_times));
					}
				}
				
				Cell cell28 = sheet.getCell(28, i);//累计消费额
				String vm_total_money = cell28.getContents().trim();
				if("".equals(vm_total_money)){
					t_Vip_Member.setVm_total_money(0.0d);
				}else {
					if(ExcelImportUtil.checkStrRegex("^[\\d]+\\.{0,1}[\\d]*$", vm_total_money)){//只能输入数字
						errorCause = "源文件第"+(i+1)+"行，累计消费额不正确，请输入正确的累计消费额！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}else{
						t_Vip_Member.setVm_total_money(Double.parseDouble(vm_total_money));
					}
				}
				
				Cell cell29 = sheet.getCell(29, i);//状态
				String vm_state_str = cell29.getContents().trim();
				Integer vm_state = 0;
				if("".equals(vm_state_str)){
					errorCause = "源文件第"+(i+1)+"行，状态不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}else {
					if("正常".equals(vm_state_str)){
						vm_state = 0;
					}else if("无效".equals(vm_state_str)){
						vm_state = 1;
					}else if("挂失".equals(vm_state_str)){
						vm_state = 2;
					}else {
						errorCause = "源文件第"+(i+1)+"行，状态只能输入“正常、无效、挂失”！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					t_Vip_Member.setVm_state(vm_state);
				}
				
				Cell cell30 = sheet.getCell(30, i);//经办人
				String vm_manager_name = cell30.getContents().trim();
				String vm_manager_code = "";
				if("".equals(vm_manager_name)){
					t_Vip_Member.setVm_manager_code(vm_manager_code);
				}else {
					tag = false;
					for(int k=0;k<emps.size();k++){
						if(vm_manager_name.equals(emps.get(k).getEm_name())){
							vm_manager_code = emps.get(k).getEm_code();
							tag = true;
							break;
						}
					}
					if(!tag){//经办人不存在
						errorCause = "源文件第"+(i+1)+"行，经办人不存在，请重新输入";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					t_Vip_Member.setVm_manager_code(vm_manager_code);
				}
				
				Cell cell31 = sheet.getCell(31, i);//发卡店铺
				String vm_shop_name = cell31.getContents().trim();
				String vm_shop_code = "";
				if("".equals(vm_shop_name)){
					errorCause = "源文件第"+(i+1)+"行，发卡店铺不能为空！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}else {
					tag = false;
					for(int k=0;k<shops.size();k++){
						if(vm_shop_name.equals(shops.get(k).getSp_name())){
							vm_shop_code = shops.get(k).getSp_code();
							tag = true;
							break;
						}
					}
					if(!tag){//发卡店铺不存在
						errorCause = "源文件第"+(i+1)+"行，发卡店铺不存在，请重新输入";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
					t_Vip_Member.setVm_shop_code(vm_shop_code);
				}
				
				Cell cell32 = sheet.getCell(32, i);//身高
				String vmi_height = cell32.getContents().trim();
				if(!"".equals(vmi_height) && vmi_height.length()>8){
					errorCause = "源文件第"+(i+1)+"行，身高不能大于8个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_height(vmi_height);
				
				Cell cell33 = sheet.getCell(33, i);//体重
				String vmi_weight = cell33.getContents().trim();
				if(!"".equals(vmi_weight) && vmi_weight.length()>8){
					errorCause = "源文件第"+(i+1)+"行，体重不能大于8个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_weight(vmi_weight);
				
				Cell cell34 = sheet.getCell(34, i);//肤色
				String vmi_skin = cell34.getContents().trim();
				if(!"".equals(vmi_skin) && vmi_skin.length()>8){
					errorCause = "源文件第"+(i+1)+"行，肤色不能大于8个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_skin(vmi_skin);
				
				Cell cell35 = sheet.getCell(35, i);//胸围
				String vmi_bust = cell35.getContents().trim();
				if(!"".equals(vmi_bust) && vmi_bust.length()>8){
					errorCause = "源文件第"+(i+1)+"行，胸围不能大于8个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_bust(vmi_bust);
				
				Cell cell36 = sheet.getCell(36, i);//腰围
				String vmi_waist = cell36.getContents().trim();
				if(!"".equals(vmi_waist) && vmi_waist.length()>8){
					errorCause = "源文件第"+(i+1)+"行，腰围不能大于8个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_waist(vmi_waist);
				
				Cell cell37 = sheet.getCell(37, i);//臀围
				String vmi_hips = cell37.getContents().trim();
				if(!"".equals(vmi_hips) && vmi_hips.length()>8){
					errorCause = "源文件第"+(i+1)+"行，臀围不能大于8个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_hips(vmi_hips);
				
				Cell cell38 = sheet.getCell(38, i);//上装尺码
				String vmi_size_top = cell38.getContents().trim();
				if(!"".equals(vmi_size_top) && vmi_size_top.length()>10){
					errorCause = "源文件第"+(i+1)+"行，上装尺码不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_size_top(vmi_size_top);
				
				Cell cell39 = sheet.getCell(39, i);//下装尺码
				String vmi_size_lower = cell39.getContents().trim();
				if(!"".equals(vmi_size_lower) && vmi_size_lower.length()>10){
					errorCause = "源文件第"+(i+1)+"行，下装尺码不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_size_lower(vmi_size_lower);
				
				Cell cell40 = sheet.getCell(40, i);//鞋子尺码
				String vmi_size_shoes = cell40.getContents().trim();
				if(!"".equals(vmi_size_shoes) && vmi_size_shoes.length()>10){
					errorCause = "源文件第"+(i+1)+"行，鞋子尺码不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_size_shoes(vmi_size_shoes);
				
				Cell cell41 = sheet.getCell(41, i);//内衣尺寸
				String vmi_size_bra = cell41.getContents().trim();
				if(!"".equals(vmi_size_bra) && vmi_size_bra.length()>10){
					errorCause = "源文件第"+(i+1)+"行，内衣尺寸不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_size_bra(vmi_size_bra);
				
				Cell cell42 = sheet.getCell(42, i);//裤长
				String vmi_trousers_length = cell42.getContents().trim();
				if(!"".equals(vmi_trousers_length) && vmi_trousers_length.length()>10){
					errorCause = "源文件第"+(i+1)+"行，裤长不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_trousers_length(vmi_trousers_length);
				
				Cell cell43 = sheet.getCell(43, i);//脚型
				String vmi_foottype = cell43.getContents().trim();
				if(!"".equals(vmi_foottype)){
					String strFoottype = "正常|偏胖|偏瘦|脚面扁平|脚面偏高|";
					if(!strFoottype.contains(vmi_foottype+"|")){
						errorCause = "源文件第"+(i+1)+"行，职业只能输入'正常、偏胖、偏瘦、脚面扁平、脚面偏高'！";
						ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
						errorRow++;
						continue;
					}
				}
				t_Vip_Member_Info.setVmi_foottype(vmi_foottype);
				
				Cell cell44 = sheet.getCell(44, i);//中意颜色
				String vmi_like_color = cell44.getContents().trim();
				if(!"".equals(vmi_like_color) && vmi_like_color.length()>10){
					errorCause = "源文件第"+(i+1)+"行，中意颜色不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_like_color(vmi_like_color);
				
				Cell cell45 = sheet.getCell(45, i);//常穿品牌
				String vmi_like_brand = cell45.getContents().trim();
				if(!"".equals(vmi_like_brand) && vmi_like_brand.length()>10){
					errorCause = "源文件第"+(i+1)+"行，常穿品牌不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_like_brand(vmi_like_brand);
				
				Cell cell46 = sheet.getCell(46, i);//穿着偏好
				String vmi_wear_prefer = cell46.getContents().trim();
				if(!"".equals(vmi_wear_prefer) && vmi_wear_prefer.length()>10){
					errorCause = "源文件第"+(i+1)+"行，穿着偏好不能大于10个字符！";
					ExcelImportUtil.copySheetRow(errorSheet, sheet, i, errorRow, columns, errorCause);
					errorRow++;
					continue;
				}
				t_Vip_Member_Info.setVmi_wear_prefer(vmi_wear_prefer);
				
				t_Vip_Member.setVm_sysdate(DateUtil.getCurrentTime());
				t_Vip_Member.setCompanyid(companyid);
				t_Vip_Member_Info.setCompanyid(companyid);
				
				memberList.add(t_Vip_Member);
				memberInfoMap.put(vm_cardcode, t_Vip_Member_Info);
				
				members.add(t_Vip_Member);
				successRow++;
			}
			resultMap.put("memberList", memberList);//会员信息
			resultMap.put("memberInfoMap", memberInfoMap);//会员其他信息
			resultMap.put("fail", errorRow-1);// 错误条数
			resultMap.put("success", successRow);// 成功条数
		}catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		return resultMap;
	}

	@Override
	@Transactional
	public void saveImport(Map<String, Object> param) {
		Integer companyid = (Integer)param.get(CommonUtil.COMPANYID);
		List<T_Vip_Member> memberList = (List<T_Vip_Member>)param.get("memberList");//导入成功会员信息
		Map<String, T_Vip_Member_Info> memberInfoMap = (Map<String, T_Vip_Member_Info>)param.get("memberInfoMap");//导入成功会员详细信息
		
		List<T_Vip_Member_Info> memberInfoList = new ArrayList<T_Vip_Member_Info>();//存放需要保存的会员详细信息
		
		T_Import_Info t_Import_Info = (T_Import_Info)param.get("t_Import_Info");
		if(memberList != null && memberList.size()>0 && memberInfoMap!=null){
			//获取会员最大code
			String max_vm_code = memberDAO.queryMaxVmcode(companyid);
			Integer int_vm_code = Integer.parseInt(max_vm_code);
			for(int i=0;i<memberList.size();i++){
				String vm_cardcode = memberList.get(i).getVm_cardcode();
				String vm_code = "";
				vm_code = (int_vm_code + i) + "";//获取code
				//把code放到集合中
				memberList.get(i).setVm_code(vm_code);
				
				T_Vip_Member_Info member_Info = (T_Vip_Member_Info)memberInfoMap.get(vm_cardcode);//获取会员对应的会员详细信息
				member_Info.setVmi_code(vm_code);
				memberInfoList.add(member_Info);
			}
			memberDAO.saveMemberImport(memberList);
			memberDAO.saveMemberInfoImport(memberInfoList);
		}
		
		// 查询当前用户是否导入过数据
		T_Import_Info info = importInfoDAO.queryImportInfo(param);
		if(info != null){
			String temp_uploaddir = CommonUtil.CHECK_BASE + CommonUtil.EXCEL_PATH;
			String last_error_filename = temp_uploaddir + File.separator + info.getIi_error_filename();
			File last_error_file = new File(last_error_filename);
			if (last_error_file.exists()) {
				last_error_file.delete();
			}
			ExcelImportUtil.deleteFile(last_error_file);
			importInfoDAO.updateImportInfo(t_Import_Info);
		}else {
			importInfoDAO.saveImportInfo(t_Import_Info);
		}
	}

	@Override
	public List<T_Vip_Tag> query_vip_tag(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return memberDAO.query_vip_tag(param);
	}

	@Override
	@Transactional
	public void del_vip_tag(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String del_type = (String)params.get("del_type");
		if(del_type != null && !"".equals(del_type) && "vmt".equals(del_type)){//删除当前会员绑定标签数据
			memberDAO.del_member_tag(params);
		}else if(del_type != null && !"".equals(del_type) && "vt".equals(del_type)){
			memberDAO.del_vip_tag(params);
		}else {
			throw new IllegalArgumentException("数据错误，删除失败!");
		}
	}

	@Override
	@Transactional
	public void set_member_tag(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		memberDAO.set_member_tag(params);
	}

	@Override
	@Transactional
	public void save_vip_tag(Map<String, Object> params) {
		Integer companyid = (Integer)params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String vt_name = (String)params.get("vt_name");
		String vt_manager_code = (String)params.get("us_code");
		String vt_shop_code = (String)params.get("shop_code");
		String vt_sysdate = DateUtil.getCurrentTime();
		String vm_code = (String)params.get("vm_code");
		T_Vip_Tag t_Vip_Tag = new T_Vip_Tag();
		t_Vip_Tag.setVt_name(vt_name);
		t_Vip_Tag.setVt_manager_code(vt_manager_code);
		t_Vip_Tag.setVt_shop_code(vt_shop_code);
		t_Vip_Tag.setVt_sysdate(vt_sysdate);
		t_Vip_Tag.setCompanyid(companyid);
		t_Vip_Tag.setVm_code(vm_code);
		memberDAO.save_vip_tag(t_Vip_Tag);
	}

	//------------------------前台--------------------------------------
	@Transactional
	public void updatePoint(Map<String,Object> param){
		memberDAO.updatePoint(param);
	}

	@Override
	public List<T_Sell_ShopList> listSell(Map<String, Object> param) {
		return memberDAO.listSell(param);
	}

	@Override
	public List<T_Vip_PonitList> listPoint(Map<String, Object> param) {
		return memberDAO.listPoint(param);
	}

	//------------------------接口---------------------------
	@Override
	public List<T_Vip_Member> list_server(Map<String, Object> params) {
		if (StringUtil.isEmpty(params.get(CommonUtil.COMPANYID))) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(StringUtil.isEmpty(params.get(CommonUtil.SHOP_CODE))){
			throw new IllegalArgumentException("参数shop_code不能为null");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return memberDAO.list_server(params);
	}
	
	@Override
	@Transactional
	public void save(T_Vip_Member member, T_Vip_Member_Info memberInfo,EmpLoginDto empLoginDto) {
		if(member == null || memberInfo == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(member.getVm_name())){
			throw new IllegalArgumentException("会员姓名不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_cardcode())){
			throw new IllegalArgumentException("会员卡号不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_mobile())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		T_Sys_User user = new T_Sys_User();
		user.setUs_shop_code(empLoginDto.getEm_shop_code());
		user.setShoptype(""+empLoginDto.getShoptype());
		user.setShop_upcode(empLoginDto.getShop_upcode());
		user.setShop_uptype(""+empLoginDto.getShop_uptype());
		Integer id = memberDAO.queryByCardCode(member, user);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		Integer _id = memberDAO.queryByMobile(member, user);
		if (null != _id && _id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		if(StringUtil.isEmpty(member.getVm_birthday())){
			member.setVm_birthday(null);
		}
		if(StringUtil.isEmpty(member.getVm_lunar_birth())){
			member.setVm_lunar_birth(null);
		}
		memberDAO.save(member, memberInfo);
	}
	
	@Override
	@Transactional
	public void update(T_Vip_Member member, T_Vip_Member_Info memberInfo,EmpLoginDto empLoginDto) {
		if(member == null || memberInfo == null){
			throw new IllegalArgumentException("参数不能为null");
		}
		if(StringUtil.isEmpty(member.getVm_name())){
			throw new IllegalArgumentException("会员姓名不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_cardcode())){
			throw new IllegalArgumentException("会员卡号不能为空");
		}
		if(StringUtil.isEmpty(member.getVm_mobile())){
			throw new IllegalArgumentException("手机号码不能为空");
		}
		T_Sys_User user = new T_Sys_User();
		user.setUs_shop_code(empLoginDto.getEm_shop_code());
		user.setShoptype(""+empLoginDto.getShoptype());
		user.setShop_upcode(empLoginDto.getShop_upcode());
		user.setShop_uptype(""+empLoginDto.getShop_uptype());
		Integer id = memberDAO.queryByCardCode(member, user);
		if (null != id && id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		Integer _id = memberDAO.queryByMobile(member, user);
		if (null != _id && _id > 0) {
			throw new IllegalArgumentException("卡号或手机已存在!");
		}
		if(StringUtil.isEmpty(member.getVm_birthday())){
			member.setVm_birthday(null);
		}
		if(StringUtil.isEmpty(member.getVm_lunar_birth())){
			member.setVm_lunar_birth(null);
		}
		memberDAO.update(member, memberInfo);
	}
	
	@Override
	@Transactional
	public void updateImg(T_Vip_Member member) {
		memberDAO.updateImg(member);
	}
	
	@Override
	public T_Vip_Member search(Map<String, Object> params) {
		if (StringUtil.isEmpty(params.get(CommonUtil.COMPANYID))) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		if(StringUtil.isEmpty(params.get(CommonUtil.SHOP_CODE))){
			throw new IllegalArgumentException("参数shop_code不能为null");
		}
		return memberDAO.search(params);
	}
	
}
