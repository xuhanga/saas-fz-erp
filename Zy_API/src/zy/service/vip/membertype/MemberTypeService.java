package zy.service.vip.membertype;

import java.util.List;
import java.util.Map;

import zy.entity.vip.membertype.T_Vip_MemberType;

public interface MemberTypeService {
	List<T_Vip_MemberType> list(Map<String,Object> param);
	Integer queryByName(T_Vip_MemberType memberType);
	T_Vip_MemberType queryByID(Integer mt_id);
	void save(T_Vip_MemberType memberType);
	void update(T_Vip_MemberType memberType);
	void del(Integer mt_id);
}
