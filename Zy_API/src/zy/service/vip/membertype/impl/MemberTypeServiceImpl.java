package zy.service.vip.membertype.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.vip.membertype.MemberTypeDAO;
import zy.entity.vip.membertype.T_Vip_MemberType;
import zy.service.vip.membertype.MemberTypeService;
import zy.util.CommonUtil;

@Service
public class MemberTypeServiceImpl implements MemberTypeService{
	@Resource
	private MemberTypeDAO memberTypeDAO;

	@Override
	public List<T_Vip_MemberType> list(Map<String, Object> param) {
		Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return memberTypeDAO.list(param);
	}

	@Override
	public Integer queryByName(T_Vip_MemberType memberType) {
		return memberTypeDAO.queryByName(memberType);
	}

	@Override
	public T_Vip_MemberType queryByID(Integer mt_id) {
		return memberTypeDAO.queryByID(mt_id);
	}

	@Override
	public void save(T_Vip_MemberType memberType) {
		memberTypeDAO.save(memberType);
	}

	@Override
	public void update(T_Vip_MemberType memberType) {
		if (memberType == null || memberType.getMt_id() == null) {
			throw new IllegalArgumentException("参数mt_id不能为null");
		}
		memberTypeDAO.update(memberType);
	}

	@Override
	public void del(Integer mt_id) {
		memberTypeDAO.del(mt_id);
	}
}
