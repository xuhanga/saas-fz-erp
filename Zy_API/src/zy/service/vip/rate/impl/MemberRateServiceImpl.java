package zy.service.vip.rate.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.vip.rate.MemberRateDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.vip.rate.T_Vip_Brand_Rate;
import zy.entity.vip.rate.T_Vip_Type_Rate;
import zy.service.vip.rate.MemberRateService;
import zy.util.CommonUtil;
import zy.vo.vip.VipVO;
@Service
public class MemberRateServiceImpl implements MemberRateService{
	@Resource
	private MemberRateDAO memberRateDAO;
	@Override
	public PageData<T_Vip_Brand_Rate> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = memberRateDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Brand_Rate> list = memberRateDAO.list(params);
		PageData<T_Vip_Brand_Rate> pageData = new PageData<T_Vip_Brand_Rate>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Override
	public PageData<T_Vip_Type_Rate> pageType(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = memberRateDAO.countType(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Type_Rate> list = memberRateDAO.listType(params);
		PageData<T_Vip_Type_Rate> pageData = new PageData<T_Vip_Type_Rate>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Override
	public void saveBrand(Map<String, Object> param) {
		VipVO.buildBrand(param);
		memberRateDAO.saveBrand(param);
	}
	@Override
	public void saveType(Map<String, Object> param) {
		VipVO.buildType(param);
		memberRateDAO.saveType(param);		
	}
	@Override
	public void delType(Map<String, Object> param) {
		memberRateDAO.delType(param);
	}
	@Override
	public void delBrand(Map<String, Object> param) {
		memberRateDAO.delBrand(param);
	}
	@Override
	public T_Vip_Brand_Rate brandByID(Integer br_id) {
		return memberRateDAO.brandByID(br_id);
	}
	@Override
	public T_Vip_Type_Rate typeByID(Integer tr_id) {
		return memberRateDAO.typeByID(tr_id);
	}
	@Override
	public void updateBrand(Map<String, Object> param) {
		memberRateDAO.updateBrand(param);
	}
	@Override
	public void updateType(Map<String, Object> param) {
		memberRateDAO.updateType(param);
	}

}
