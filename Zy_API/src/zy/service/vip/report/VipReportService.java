package zy.service.vip.report;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.vip.member.T_Vip_ActivevipAnalysis;
import zy.entity.vip.member.T_Vip_BrandAnalysis;
import zy.entity.vip.member.T_Vip_ConsumeDetailList;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_NoShopAnalysis;
import zy.entity.vip.member.T_Vip_QuotaAnalysis;
import zy.entity.vip.member.T_Vip_TypeAnalysis;

public interface VipReportService {
	PageData<T_Vip_Member> point_list(Map<String,Object> params);
	PageData<T_Vip_TypeAnalysis> type_analysis_list(Map<String,Object> params);
	PageData<T_Vip_ConsumeDetailList> consume_detail_list(Map<String,Object> params);
	PageData<T_Vip_ActivevipAnalysis> activevip_analysis_list(Map<String,Object> params);
	PageData<T_Vip_NoShopAnalysis> noshopvip_analysis_list(Map<String,Object> params);
	Map<String, Object> member_lose_data(Map<String,Object> params); 
	PageData<T_Vip_QuotaAnalysis> vipquota_analysis_list(Map<String,Object> params);
	PageData<T_Vip_BrandAnalysis> brand_analysis_list(Map<String,Object> params);
	PageData<Map<String, Object>> vipconsume_month_compare(Map<String,Object> params);
	String vipconsume_month_compare_column(Map<String,Object> params);
	String vipage_analysis_column(Map<String,Object> params);
	PageData<T_Vip_Member> vipage_analysis_list(Map<String,Object> params);
}
