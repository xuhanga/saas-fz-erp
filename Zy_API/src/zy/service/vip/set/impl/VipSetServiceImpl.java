package zy.service.vip.set.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.vip.set.VipSetDAO;
import zy.entity.vip.set.T_Vip_AgeGroupSetUp;
import zy.entity.vip.set.T_Vip_BirthdaySms;
import zy.entity.vip.set.T_Vip_Grade;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.entity.vip.set.T_Vip_Setup;
import zy.service.vip.set.VipSetService;
import zy.util.CommonUtil;

@Service
public class VipSetServiceImpl implements VipSetService{

	@Resource
	private VipSetDAO vipSetDAO;
	
	@Override
	public T_Vip_BirthdaySms loadBirthdaySms(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return vipSetDAO.loadBirthdaySms(params);
	}

	@Override
	public List<T_Vip_ReturnSetUp> loadReturnSetUp(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return vipSetDAO.loadReturnSetUp(params);
	}
	@Override
	public List<T_Vip_ReturnSetUp> loadReturnSetUp(String shop_code,Integer companyid) {
		return vipSetDAO.loadReturnSetUp(shop_code, companyid);
	}

	@Override
	public List<T_Vip_AgeGroupSetUp> loadAgeGroupSetUp(
			Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return vipSetDAO.loadAgeGroupSetUp(params);
	}

	@Override
	public T_Vip_AgeGroupSetUp loadAgeGroupSetUp(Integer ags_id) {
		if (ags_id == null) {
			throw new IllegalArgumentException("数据出错，请联系管理员!");
		}
		return vipSetDAO.loadAgeGroupSetUp(ags_id);
	}

	@Override
	public List<T_Vip_Grade> loadGrade(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return vipSetDAO.loadGrade(params);
	}

	@Override
	public T_Vip_Setup loadSetUp(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		return vipSetDAO.loadSetUp(params);
	}

	@Override
	@Transactional
	public void update(Map<String, Object> params) {
		try{
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时！！！");
		}
		
		//会员生日短信设置
		T_Vip_BirthdaySms birthdaySms = (T_Vip_BirthdaySms)params.get("birthdaySms");
		if(birthdaySms == null){
			throw new IllegalArgumentException("生日提醒设置参数为空！！！");
		}
		vipSetDAO.updateBirthdaySms(birthdaySms);
		//消费回访设置
		List<T_Vip_ReturnSetUp> returnSetUps = (List<T_Vip_ReturnSetUp>)params.get("returnSetUps");
		if(returnSetUps == null){
			throw new IllegalArgumentException("消费者回访设置为空！！！");
		}
		vipSetDAO.updateReturnSet(returnSetUps);
		//会员等级设置
		List<T_Vip_Grade> grades = (List<T_Vip_Grade>)params.get("grades");
		if(grades == null){
			throw new IllegalArgumentException("会员等级设置为空！！！");
		}
		vipSetDAO.updateGrade(grades);
		//会员系统参数设置
		T_Vip_Setup setup = (T_Vip_Setup)params.get("setup");
		if(setup == null){
			throw new IllegalArgumentException("会员等级设置为空！！！");
		}
		vipSetDAO.updateSetup(params);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	@Transactional
	public void delReturnSet(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时！！！");
		}
		
		Integer rts_id = (Integer)params.get("rts_id");
		vipSetDAO.delReturnSet(rts_id);//删除消费回访设置
		
		List<T_Vip_ReturnSetUp> returnSetUps = vipSetDAO.loadReturnSetUp(params);//更新回访设置
		if(returnSetUps != null && returnSetUps.size()>0){
			List<T_Vip_ReturnSetUp> returnSetUpsNew = new ArrayList<T_Vip_ReturnSetUp>();
			T_Vip_ReturnSetUp t_Vip_ReturnSetUp = null;
			for(int i=0;i<returnSetUps.size();i++){
				t_Vip_ReturnSetUp = returnSetUps.get(i);
				t_Vip_ReturnSetUp.setRts_second(i+1);
				returnSetUpsNew.add(t_Vip_ReturnSetUp);
			}
			vipSetDAO.updateReturnSet(returnSetUps);
		}
	}

	@Override
	public void delAgeGroupSet(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时！！！");
		}
		
		Integer ags_id = (Integer)params.get("ags_id");
		vipSetDAO.delAgeGroupSet(ags_id);//删除消费回访设置
	}

	@Override
	public void saveReturnSet(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时！！！");
		}
		vipSetDAO.saveReturnSet(params);
	}

	@Override
	public void saveAgeGroupSet(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时！！！");
		}
		vipSetDAO.saveAgeGroupSet(params);
	}

	@Override
	public void updateAgeGroupSet(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("登录超时！！！");
		}
		vipSetDAO.updateAgeGroupSet(params);
	}
}
