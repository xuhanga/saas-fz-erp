package zy.service.vip.visit.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.sell.ecoupon.ECouponDAO;
import zy.dao.vip.member.MemberDAO;
import zy.dao.vip.visit.VisitDAO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.service.vip.visit.VisitService;
import zy.util.CheckCodeUtil;
import zy.util.CommonUtil;
import zy.util.LunarCalendar;
import zy.util.StringUtil;

@Service
public class VisitServiceImpl implements VisitService {
	
	@Resource
	private VisitDAO visitDAO;
	@Resource
	private ECouponDAO eCouponDAO;
	@Resource
	private MemberDAO memberDAO;
	
	
	@Override
	public PageData<T_Vip_Member> page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = visitDAO.count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Member> list = visitDAO.list(params);
		PageData<T_Vip_Member> pageData = new PageData<T_Vip_Member>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		pageData.setData(visitDAO.sum(params));
		return pageData;
	}
	
	@Override
	public List<T_Vip_Member> list(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return visitDAO.list(params);
	}
	
	@Override
	public List<T_Vip_Member> list_birthday_visit(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		String begindate = (String)params.get("begindate");
		String enddate = (String)params.get("enddate");
		if(StringUtil.isEmpty(begindate)){
			throw new IllegalArgumentException("参数begindate不能为null");
		}
		if(StringUtil.isEmpty(enddate)){
			throw new IllegalArgumentException("参数enddate不能为null");
		}
		String beginLunarDate = LunarCalendar.solarToLunar(begindate);
		String endLunarDate = LunarCalendar.solarToLunar(enddate);
		begindate = begindate.substring(5);
		enddate = enddate.substring(5);
		beginLunarDate = beginLunarDate.substring(5);
		endLunarDate = endLunarDate.substring(5);
		params.put("begindate", begindate);
		params.put("enddate", enddate);
		params.put("beginLunarDate", beginLunarDate);
		params.put("endLunarDate", endLunarDate);
		int crossYear = 0;
		if (enddate.compareTo(begindate) < 0) {//公历跨年份
			crossYear = 1;
		}
		int crossYearLunar = 0;
		if (endLunarDate.compareTo(beginLunarDate) < 0) {//农历跨年份
			crossYearLunar = 1;
		}
		params.put("crossYear", crossYear);
		params.put("crossYearLunar", crossYearLunar);
		Integer pageSize = (Integer)params.get(CommonUtil.PAGESIZE);
		Integer pageIndex = (Integer)params.get(CommonUtil.PAGEINDEX);
		params.put(CommonUtil.START, (pageIndex-1)*pageSize);
		params.put(CommonUtil.END, pageSize);
		return visitDAO.list_birthday_visit(params);
	}

	@Override
	@Transactional
	public void save_visit_ecoupon(T_Vip_Visit visit,T_Sell_Ecoupon_User ecouponUser) {
		T_Sell_ECouponList eCouponList = eCouponDAO.loadDetail(ecouponUser.getEcu_code(), ecouponUser.getCompanyid());
		if (eCouponList == null) {
			throw new RuntimeException("优惠券不存在");
		}
		int receivedCount = eCouponDAO.countECouponReceived(ecouponUser.getEcu_code(), ecouponUser.getCompanyid());
		if (receivedCount >= eCouponList.getEcl_amount()) {
			throw new RuntimeException("优惠券已被领完");
		}
		memberDAO.save_visit(visit);
		ecouponUser.setEcu_check_no(CheckCodeUtil.buildCheckCode(4));
		visitDAO.save_ecoupon_user(ecouponUser);
	}

	@Override
	public PageData<T_Vip_Visit> detail_page(Map<String, Object> params) {
		Object companyid = params.get(CommonUtil.COMPANYID);
		Object pageSize = params.get(CommonUtil.PAGESIZE);
		Object pageIndex = params.get(CommonUtil.PAGEINDEX);
		if (companyid == null) {
			throw new IllegalArgumentException("连接超时，请重新登录!");
		}
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = visitDAO.detail_count(params);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		params.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		params.put(CommonUtil.END, _pageSize);
		
		List<T_Vip_Visit> list = visitDAO.detail_list(params);
		PageData<T_Vip_Visit> pageData = new PageData<T_Vip_Visit>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	
}
