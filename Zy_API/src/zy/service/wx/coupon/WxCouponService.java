package zy.service.wx.coupon;

import java.util.List;
import java.util.Map;

import zy.entity.sell.ecoupon.T_Sell_ECouponList;

public interface WxCouponService {
	/**
	 * 获取领券中心优惠券列表
	 * @param params
	 * @return
	 */
	List<T_Sell_ECouponList> getCouponCenterList(Map<String, Object> params);
	
	/**
	 * 领取优惠券
	 * @param params
	 */
	void drawCoupon(Map<String, Object> params);
}
