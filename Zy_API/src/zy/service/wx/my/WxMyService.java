package zy.service.wx.my;

import java.util.List;
import java.util.Map;

import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.wx.my.T_Wx_Address;

public interface WxMyService {
	Map<String, Object> getMyValue(String mobile,Map<String, Object> params);
	void updatePassword(Map<String, Object> params);
	List<T_Wx_Address> getAddrListByUserCode(Map<String, Object> params);
	T_Wx_Address saveAddress(T_Wx_Address t_Wx_Address);
	void updateAddressStateById(Map<String, Object> params);
	T_Wx_Address getAddressInfoById(Map<String, Object> params);
	T_Wx_Address getDefaultAddressInfoByCode(Map<String, Object> params);
	void deleteAddressById(Integer wd_id);
	void updateAddressById(T_Wx_Address t_Wx_Address);
	List<T_Sell_Ecoupon_User> getMyCouponList(Map<String, Object> params);
}
