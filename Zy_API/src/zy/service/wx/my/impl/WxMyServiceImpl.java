package zy.service.wx.my.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import zy.dao.wx.my.WxMyDAO;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.wx.my.T_Wx_Address;
import zy.service.wx.my.WxMyService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;

@Service
public class WxMyServiceImpl implements WxMyService {

	@Resource
	private WxMyDAO wxMyDAO;
	
	@Override
	public Map<String, Object> getMyValue(String mobile,Map<String, Object> params) {
		Assert.notNull(mobile, "连接超时，请重新登录!");
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get(CommonUtil.SHOP_TYPE), "参数shop_type不能为null");
		Assert.notNull(params.get(CommonUtil.SHOP_CODE), "参数shop_code不能为null");
		Assert.notNull(params.get(CommonUtil.SHOP_UPCODE), "参数shop_upcode不能为空!");
		params.put("mobile", mobile);
		return wxMyDAO.getMyValue(params);
	}

	@Override
	public void updatePassword(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get("wu_code"), "连接超时，请重新登录!");
//		Assert.notNull(params.get("old_password"), "旧密码不能为空!");
		Assert.notNull(params.get("new_password"), "新密码不能为空!");
		
//		String old_password = (String)params.get("old_password");
		String new_password = (String)params.get("new_password");
//		String confirm_password = (String)params.get("confirm_password");
		String wu_code = (String)params.get("wu_code");
		
//		if(!new_password.equals(confirm_password)){
//			throw new IllegalArgumentException("新密码和确认新密码不一致!");
//		}
		
//		old_password = MD5.encryptMd5(old_password).toUpperCase();
		new_password = MD5.encryptMd5(new_password).toUpperCase();
//		String now_password = wxMyDAO.getNowPasswordByWuCode(wu_code);
//		if(now_password != null && !now_password.equals(old_password)){
//			throw new IllegalArgumentException("当前密码不正确，请从新输入!");
//		}
		wxMyDAO.updatePassword(wu_code, new_password);
	}

	@Override
	public List<T_Wx_Address> getAddrListByUserCode(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get("wu_code"), "连接超时，请重新登录!");
		
		String wu_code = (String)params.get("wu_code");
		
		return wxMyDAO.getAddrListByUserCode(wu_code);
	}

	@Override
	@Transactional
	public T_Wx_Address saveAddress(T_Wx_Address t_Wx_Address) {
		Assert.notNull(t_Wx_Address.getWd_user_code(),"用户编号不能为空!");
		Assert.notNull(t_Wx_Address.getWd_name(),"收货人名称不能为空!");
		Assert.notNull(t_Wx_Address.getWd_mobile(),"手机号码不能为空!");
		Assert.notNull(t_Wx_Address.getWd_province(),"省不能为空!");
		Assert.notNull(t_Wx_Address.getWd_city(),"市不能为空!");
		Assert.notNull(t_Wx_Address.getWd_area(),"区县不能为空!");
		Assert.notNull(t_Wx_Address.getWd_town(),"乡镇街道不能为空!");
		Assert.notNull(t_Wx_Address.getWd_addr(),"详细地址不能为空!");
		Integer wd_state = t_Wx_Address.getWd_state();
		String wd_user_code = t_Wx_Address.getWd_user_code();
		if(null == wd_state){
			t_Wx_Address.setWd_state(0);
			wd_state = 0;
		}
		t_Wx_Address.setWd_sysdate(DateUtil.getCurrentTime());
		
		//如果wd_state 为1默认， 更新改用户的其他地址为0非默认
		if(wd_state == 1){
			wxMyDAO.updateAddressStateByUsCode(wd_user_code);
		}
		return wxMyDAO.saveAddress(t_Wx_Address);
	}

	@Override
	@Transactional
	public void updateAddressStateById(Map<String, Object> params) {
		Assert.notNull(params.get("wd_id"),"地址ID不能为空!");
		Assert.notNull(params.get("wd_user_code"),"用户编号不能为空!");
		String wd_user_code = (String)params.get("wd_user_code");
		Integer wd_id = (Integer)params.get("wd_id");
		wxMyDAO.updateAddressStateByUsCode(wd_user_code);
		wxMyDAO.updateAddressStateById(wd_id);
	}

	@Override
	public T_Wx_Address getAddressInfoById(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get("wd_id"), "地址ID不能为空!");
		Integer wd_id = (Integer)params.get("wd_id");
		return wxMyDAO.getAddressInfoById(wd_id);
	}

	@Override
	public T_Wx_Address getDefaultAddressInfoByCode(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get("wd_code"), "连接超时，请重新登录!");
		String wd_code = (String)params.get("wd_code");
		return wxMyDAO.getDefaultAddressInfoByCode(wd_code);
	}
	
	@Override
	public void deleteAddressById(Integer wd_id) {
		Assert.notNull(wd_id, "地址ID不能为空!");
		wxMyDAO.deleteAddressById(wd_id);
	}

	@Override
	@Transactional
	public void updateAddressById(T_Wx_Address t_Wx_Address) {
		Assert.notNull(t_Wx_Address.getWd_id(),"地址ID不能为空!");
		Assert.notNull(t_Wx_Address.getWd_user_code(),"用户编号不能为空!");
		Assert.notNull(t_Wx_Address.getWd_name(),"收货人名称不能为空!");
		Assert.notNull(t_Wx_Address.getWd_mobile(),"手机号码不能为空!");
		Assert.notNull(t_Wx_Address.getWd_province(),"省不能为空!");
		Assert.notNull(t_Wx_Address.getWd_city(),"市不能为空!");
		Assert.notNull(t_Wx_Address.getWd_area(),"区县不能为空!");
		Assert.notNull(t_Wx_Address.getWd_town(),"乡镇街道不能为空!");
		Assert.notNull(t_Wx_Address.getWd_addr(),"详细地址不能为空!");
		Integer wd_state = t_Wx_Address.getWd_state();
		String wd_user_code = t_Wx_Address.getWd_user_code();
		if(null == wd_state){
			t_Wx_Address.setWd_state(0);
			wd_state = 0;
		}
		
		//如果wd_state 为1默认， 更新改用户的其他地址为0非默认
		if(wd_state == 1){
			wxMyDAO.updateAddressStateByUsCode(wd_user_code);
		}
		wxMyDAO.updateAddressById(t_Wx_Address);
	}

	@Override
	public List<T_Sell_Ecoupon_User> getMyCouponList(Map<String, Object> params) {
		Assert.notNull(params.get(CommonUtil.COMPANYID), "连接超时，请重新登录!");
		Assert.notNull(params.get("wu_code"), "连接超时，请重新登录!");
		Assert.notNull(params.get(CommonUtil.SHOP_CODE), "参数shop_code不能为null");
		Assert.notNull(params.get("wu_mobile"), "手机号码不能为空!");
		return wxMyDAO.getMyCouponList(params);
	}
	
}
