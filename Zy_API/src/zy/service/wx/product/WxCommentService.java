package zy.service.wx.product;

import java.util.Map;

import zy.entity.PageData;
import zy.entity.wx.my.T_Wx_Comment;

public interface WxCommentService {
	PageData<T_Wx_Comment> page(Map<String, Object> params);
	T_Wx_Comment saveComment(T_Wx_Comment t_Wx_Comment);
}
