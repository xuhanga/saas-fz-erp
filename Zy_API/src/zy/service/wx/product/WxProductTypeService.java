package zy.service.wx.product;

import java.util.List;
import java.util.Map;

import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductType;

public interface WxProductTypeService {
	List<T_Wx_ProductType> list(Map<String,Object> params);
	T_Wx_ProductType load(Integer pt_id);
	void save(T_Wx_ProductType productType);
	void update(T_Wx_ProductType productType);
	void del(Integer pt_id);
	List<T_Wx_ProductType> getProductType(Map<String,Object> params);
	List<T_Wx_Product> getProductByType(Map<String,Object> params);
}
