package zy.service.wx.wechat;

import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.wechat.T_Wx_WechatSet;

public interface WechatSetService {
	T_Wx_WechatSet load(String shop_code, Integer companyid);
	void update(T_Wx_WechatSet wechatSet, T_Sys_User user);
}
