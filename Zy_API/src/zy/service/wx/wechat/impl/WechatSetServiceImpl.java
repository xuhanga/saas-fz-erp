package zy.service.wx.wechat.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.wx.wechat.WechatSetDAO;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.wechat.T_Wx_WechatSet;
import zy.service.wx.wechat.WechatSetService;
import zy.util.CommonUtil;
import zy.util.DateUtil;

@Service
public class WechatSetServiceImpl implements WechatSetService{
	@Resource
	private WechatSetDAO wechatSetDAO;
	
	@Override
	public T_Wx_WechatSet load(String shop_code, Integer companyid) {
		return wechatSetDAO.load(shop_code, companyid);
	}
	
	@Override
	@Transactional
	public void update(T_Wx_WechatSet wechatSet, T_Sys_User user) {
		if (wechatSet.getWs_id() != null && wechatSet.getWs_id() > 0) {
			wechatSetDAO.update(wechatSet);
		}else {
			if (CommonUtil.ONE.equals(user.getShoptype())
					|| CommonUtil.TWO.equals(user.getShoptype())
					|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
				wechatSet.setWs_shop_code(user.getUs_shop_code());
			}else{//自营、合伙
				wechatSet.setWs_shop_code(user.getShop_upcode());
			}
			wechatSet.setWs_useable_balance(0d);
			wechatSet.setWs_sysdate(DateUtil.getCurrentTime());
			wechatSet.setCompanyid(user.getCompanyid());
			wechatSetDAO.save(wechatSet);
		}
	}
	
	
}
