package zy.vo.base;

import java.util.ArrayList;
import java.util.List;

import zy.entity.base.affiliate.T_Base_Affiliate;

public class AffiliateVO {
	public static String buildTree(List<T_Base_Affiliate> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有区域',open:true},");
		String nodes="";
		if(list != null && list.size() >0){
			nodes = (initToTree(list,"0"));
		}
		treeHtml.append(nodes);
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
	public static String initToTree(List<T_Base_Affiliate> list, String rootCode){
		StringBuffer json = new StringBuffer(""); 
		List<T_Base_Affiliate> nodeList = hasChild(list, rootCode); // 获得集合中的父节点下的子集list  mn_IsHaveChildMenuItem
		list.remove(nodeList);
		// 循环子集
		//json.append("{");
		for (T_Base_Affiliate node : nodeList) {
			List<T_Base_Affiliate> Nodes = hasChild(list, node.getAf_code());
			json.append("{id:'" + node.getAf_code() + "',pId:'"+node.getAf_upcode()+"',name:'"+node.getAf_name()+"'");
			if (Nodes.size() > 0) {
				json.append(",open:true");
			}
			json.append("},");
			// 查询全部集合下，是否存在该子集OID的子子集
			if (Nodes.size() > 0) {
				json.append(initToTree(list, node.getAf_code())); // 递归树，传入子子集，子集父节点
			}
		}
		return json.toString();
	}
	public static List<T_Base_Affiliate> hasChild(List<T_Base_Affiliate> list, String upid) {
		  List<T_Base_Affiliate> nodeList = new ArrayList<T_Base_Affiliate>();
		  for (T_Base_Affiliate comm : list) {
		   // 如果数据的父节点==Parent的值，则该数据为子节点，存入list<SysRole>
			   if (comm.getAf_upcode().equals(upid)) {
				   nodeList.add(comm);
			   }
		  }
		  return nodeList;
	 }
}
