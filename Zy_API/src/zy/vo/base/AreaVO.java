package zy.vo.base;

import java.util.ArrayList;
import java.util.List;

import zy.entity.base.area.T_Base_Area;

public class AreaVO {
	public static String buildTreeAll(List<T_Base_Area> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有地区',open:true},");
		for (T_Base_Area node : list) {
			treeHtml.append("{id:'" + node.getAr_code() + "',pId:'"+node.getAr_upcode()+"',name:'"+node.getAr_name()+"'");
			treeHtml.append(",open:true");
			treeHtml.append("},");
		}
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
	public static String buildTree(List<T_Base_Area> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有地区',open:false},");
		String nodes="";
		if(list != null && list.size() >0){
			nodes = (initToTree(list,"0"));
		}
		treeHtml.append(nodes);
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
	public static String initToTree(List<T_Base_Area> list, String rootCode){
		StringBuffer json = new StringBuffer(""); 
		List<T_Base_Area> nodeList = hasChild(list, rootCode); // 获得集合中的父节点下的子集list  mn_IsHaveChildMenuItem
		list.remove(nodeList);
		// 循环子集
		//json.append("{");
		for (T_Base_Area node : nodeList) {
			List<T_Base_Area> Nodes = hasChild(list, node.getAr_code());
			json.append("{id:'" + node.getAr_code() + "',pId:'"+node.getAr_upcode()+"',name:'"+node.getAr_name()+"'");
			if (Nodes.size() > 0) {
				json.append(",open:false");
			}
			json.append("},");
			// 查询全部集合下，是否存在该子集OID的子子集
			if (Nodes.size() > 0) {
				json.append(initToTree(list, node.getAr_code())); // 递归树，传入子子集，子集父节点
			}
		}
		return json.toString();
	}
	public static List<T_Base_Area> hasChild(List<T_Base_Area> list, String upid) {
		  List<T_Base_Area> nodeList = new ArrayList<T_Base_Area>();
		  for (T_Base_Area comm : list) {
		   // 如果数据的父节点==Parent的值，则该数据为子节点，存入list<SysRole>
			   if (comm.getAr_upcode().equals(upid)) {
				   nodeList.add(comm);
			   }
		  }
		  return nodeList;
	 }
}
