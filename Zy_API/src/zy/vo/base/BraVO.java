package zy.vo.base;

import java.util.List;

import zy.entity.base.bra.T_Base_Bra;
import zy.util.StringUtil;

public class BraVO {
	/**
	 * 查询杯型转换成HTML
	 * */
	public static String toBraHtmlByList(List<T_Base_Bra> list) throws Exception{
		StringBuffer html = new StringBuffer("");
		for(T_Base_Bra  braList:list){
			html.append("<option value=\""+StringUtil.trimString(braList.getBr_code())+"\">");
			html.append("("+StringUtil.trimString(braList.getBr_code())+")");
			html.append(StringUtil.trimString(braList.getBr_name()));
			html.append("</option>");
		}
		return html.toString();
	}
}
