package zy.vo.base;

import java.util.List;

import zy.entity.base.color.T_Base_Color;
import zy.util.StringUtil;

public class ColorVO {
	/**
	 * 查询y颜色转换成HTML
	 * */
	public static String toColorHtmlByList(List<T_Base_Color> list) throws Exception{
		StringBuffer html = new StringBuffer("");
		for(T_Base_Color  colorList:list){
			html.append("<option value=\""+StringUtil.trimString(colorList.getCr_code())+"\">");
			html.append("("+StringUtil.trimString(colorList.getCr_code())+")");
			html.append(StringUtil.trimString(colorList.getCr_name()));
			html.append("</option>");
		}
		return html.toString();
	}
}
