package zy.vo.base;

import java.util.ArrayList;
import java.util.List;

import zy.entity.base.department.T_Base_Department;

public class EmpVO {
	public static String buildTree(List<T_Base_Department> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有部门',open:true},");
		String nodes="";
		if(list != null && list.size() >0){
			nodes = (initToTree(list,"0"));
		}
		treeHtml.append(nodes);
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
	public static String initToTree(List<T_Base_Department> list, String rootCode){
		StringBuffer json = new StringBuffer(""); 
		List<T_Base_Department> nodeList = hasChild(list, rootCode); // 获得集合中的父节点下的子集list  mn_IsHaveChildMenuItem
		list.remove(nodeList);
		// 循环子集
		//json.append("{");
		for (T_Base_Department node : nodeList) {
			List<T_Base_Department> Nodes = hasChild(list, node.getDm_code());
			json.append("{id:'" + node.getDm_code() + "',pId:'0',name:'"+node.getDm_name()+"'");
			if (Nodes.size() > 0) {
				json.append(",open:true");
			}
			json.append("},");
			// 查询全部集合下，是否存在该子集OID的子子集
			if (Nodes.size() > 0) {
				json.append(initToTree(list, node.getDm_code())); // 递归树，传入子子集，子集父节点
			}
		}
		return json.toString();
	}
	public static List<T_Base_Department> hasChild(List<T_Base_Department> list, String upid) {
		  List<T_Base_Department> nodeList = new ArrayList<T_Base_Department>();
		  for (T_Base_Department comm : list) {
		   // 如果数据的父节点==Parent的值，则该数据为子节点，存入list<SysRole>
			   if ("0".equals(upid)) {
				   nodeList.add(comm);
			   }
		  }
		  return nodeList;
	 }
}
