package zy.vo.base;

import java.util.List;

import zy.entity.base.shop.T_Base_Shop;

public class ShopVO {
	public static String buildTreeAll(List<T_Base_Shop> list){
		StringBuffer treeHtml = new StringBuffer();
		treeHtml.append("[{id:0,name:'所有店铺',open:true},");
		for (T_Base_Shop node : list) {
			treeHtml.append("{id:'" + node.getSp_code() + "',pId:'0',name:'"+node.getSp_name()+"'");
			treeHtml.append(",open:true");
			treeHtml.append("},");
		}
		treeHtml.deleteCharAt(treeHtml.length()-1);
		treeHtml.append("]");
		return treeHtml.toString();
	}
}
