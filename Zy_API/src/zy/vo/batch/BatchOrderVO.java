package zy.vo.batch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.base.size.T_Base_SizeList;
import zy.entity.batch.client.T_Batch_Client;
import zy.entity.batch.order.T_Batch_Order;
import zy.entity.batch.order.T_Batch_OrderList;
import zy.entity.batch.order.T_Batch_SizeCommon;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.PriceLimitUtil;
import zy.util.PrintUtil;
import zy.util.StringUtil;
import zy.vo.common.PrintVO;
import zy.vo.common.SizeHorizontalVO;

public class BatchOrderVO {
	
	private static List<T_Batch_SizeCommon> getSizeModeList(List<T_Batch_OrderList> list,int maxColNumArray, Map<String, List<String>> map) {
		// 尺码横排结果
		List<T_Batch_SizeCommon> lstRes = new ArrayList<T_Batch_SizeCommon>();
		int[] subArray = null;
		T_Batch_SizeCommon temp = null;
		int Amount_Total = 0;
		double RetailMoney_Total = 0d;
		double UnitMoney_Total = 0d;
		double CostMoney_Total = 0d;
		String pd_code = "";
		String cr_code = "";
		String br_code = "";
		String pd_type = "";
		List<String> sizeListWithSzgIdList = null;
		for (T_Batch_OrderList item : list) {
			if (!pd_code.equals(item.getOdl_pd_code())
					|| !cr_code.equals(item.getOdl_cr_code())
					|| !br_code.equals(item.getOdl_br_code())
					|| !pd_type.equals(StringUtil.trimString(item.getOdl_pi_type()))
					) {
				pd_code = StringUtil.trimString(item.getOdl_pd_code());
				cr_code = StringUtil.trimString(item.getOdl_cr_code());
				br_code = StringUtil.trimString(item.getOdl_br_code());
				pd_type = StringUtil.trimString(item.getOdl_pi_type());
				Amount_Total = 0;
				RetailMoney_Total = 0d;
				UnitMoney_Total = 0d;
				CostMoney_Total = 0d;
				temp = new T_Batch_SizeCommon();
				subArray = new int[maxColNumArray];
				temp.setSingle_amount(subArray);
				lstRes.add(temp);
			}
			temp.setId(item.getOdl_id());
			temp.setPd_code(item.getOdl_pd_code());
			temp.setPd_no(item.getPd_no());
			temp.setPd_name(item.getPd_name());
			temp.setPd_unit(item.getPd_unit());
			temp.setPd_type(pd_type);
			temp.setBd_name(item.getBd_name());
			temp.setTp_name(item.getTp_name());
			temp.setPd_year(item.getPd_year());
			temp.setPd_season(item.getPd_season());
			temp.setSzg_code(item.getOdl_szg_code());
			temp.setCr_code(item.getOdl_cr_code());
			temp.setCr_name(item.getCr_name());
			temp.setBr_code(item.getOdl_br_code());
			temp.setBr_name(item.getBr_name());
			temp.setUnitprice(item.getOdl_unitprice());
			temp.setRetailprice(item.getOdl_retailprice());
			temp.setCostprice(item.getOdl_costprice());
			Amount_Total += item.getOdl_amount();
			RetailMoney_Total += item.getOdl_amount()*item.getOdl_retailprice();
			UnitMoney_Total += item.getOdl_amount()*item.getOdl_unitprice();
			CostMoney_Total += item.getOdl_amount()*item.getOdl_costprice();
			temp.setTotalamount(Amount_Total);
			temp.setUnitmoney(UnitMoney_Total);
			temp.setRetailmoney(RetailMoney_Total);
			temp.setCostmoney(CostMoney_Total);
			
			//获得尺码组编号对应的尺码编号信息
			sizeListWithSzgIdList = (List<String>)map.get(item.getOdl_szg_code());
			//然后根据对应的下标确定其位置
			int szIdIndex = sizeListWithSzgIdList.indexOf(item.getOdl_sz_code());
			//根据下标位置得到，其对应的尺码数量，然后进行累加
			subArray[szIdIndex] += item.getOdl_amount();
		}
		return lstRes;
	}
	
	public static Map<String, Object> getJsonSizeData(List<T_Base_SizeList> sizeGroupList,List<T_Batch_OrderList> dataList){
		if (sizeGroupList != null && sizeGroupList.size() > 0) {
			int[] maxColNumArray = new int[1];
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            List<List<String>> lstRes = SizeHorizontalVO.getSizeModeTitleInfoList(map, maxColNumArray, sizeGroupList);
            List<T_Batch_SizeCommon> list = getSizeModeList(dataList,maxColNumArray[0],map);
            int sizeMaxSize = maxColNumArray[0];
			int[] totalSizeBySingle = new int[sizeMaxSize];
			Collections.sort(lstRes, new Comparator<List<String>>() {
				@Override
				public int compare(List<String> o1, List<String> o2) {
					return o1.size() > o2.size() ? -1 : 0;
				}
			});
			int Amount_Total = 0;
			double RetailMoney_Total = 0d;
			double UnitMoney_Total = 0d;
			double CostMoney_Total = 0d;
			HashMap<String, Object> sizeGroupMap = new HashMap<String, Object>();
			List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
			HashMap<String, Object> entry = null;
			if (list == null || list.isEmpty()) {
			} else {
				for (T_Batch_SizeCommon item : list) {
					entry = new HashMap<String, Object>();
					entry.put("id", item.getId());
					entry.put("pd_code", item.getPd_code());
					entry.put("pd_no", item.getPd_no());
					entry.put("pd_name", item.getPd_name());
					entry.put("pd_unit", item.getPd_unit());
					entry.put("cr_code", item.getCr_code());
					entry.put("cr_name", item.getCr_name());
					entry.put("br_code", item.getBr_code());
					entry.put("br_name", item.getBr_name());
					entry.put("odl_pi_type", item.getPd_type());
					int[] amounts = item.getSingle_amount();
					if (lstRes == null || lstRes.size() <= 0) {
					} else {
						String[] header = null;
						int amountSize = 0;
						if (map.containsKey(item.getSzg_code())) {
							amountSize = map.get(item.getSzg_code()).size();
						}
						List<String> headers = lstRes.get(0);
						for (int i = 0; i < headers.size(); i++) {
							header = headers.get(i).split("_AND_");
							if (i >= amountSize) {
								entry.put(header[0], "");
							} else {
								entry.put(header[0],amounts[i] == 0 ? "": amounts[i] + "");
								totalSizeBySingle[i] = totalSizeBySingle[i] + amounts[i];
							}
						}
					}
					entry.put("Amount_Total", item.getTotalamount());
					entry.put("unit_price", item.getUnitprice());
					entry.put("unit_money", item.getUnitmoney());
					entry.put("retail_price", item.getRetailprice());
					entry.put("retail_money", item.getRetailmoney());
					entry.put("cost_price", item.getCostprice());
					entry.put("cost_money", item.getCostmoney());
					entry.put("sizeGroupCode", item.getSzg_code());
					Amount_Total += item.getTotalamount();
					UnitMoney_Total += item.getUnitmoney();
					RetailMoney_Total += item.getRetailmoney();
					CostMoney_Total += item.getCostmoney();
					values.add(entry);
				}
			}
			if (lstRes == null || lstRes.size() <= 0) {
			} else {
				String[] header = null;
                List<String> headers = lstRes.get(0);
                for (int i = 0; i < headers.size(); i++) {
                    header = headers.get(i).split("_AND_");
                    sizeGroupMap.put(header[0], totalSizeBySingle[i] == 0 ? "" : totalSizeBySingle[i]);
                }
			}
			HashMap<String, Object> userData = new HashMap<String, Object>();
			userData.put("pd_no", "合计：");
			userData.put("Amount_Total", Amount_Total);
			userData.put("unit_money", UnitMoney_Total);
			userData.put("retail_money", RetailMoney_Total);
			userData.put("cost_money", CostMoney_Total);
			userData.putAll(sizeGroupMap);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("data", values);
			resultMap.put("userData", userData);
			return resultMap;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<T_Batch_OrderList> convertMap2Model(Map<String, Object> data,Integer od_type,T_Sys_User user){
		List<T_Batch_OrderList> resultList = new ArrayList<T_Batch_OrderList>();
		List products = (List)data.get("products");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		T_Batch_OrderList model = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			Integer odl_pi_type = Integer.parseInt(StringUtil.trimString(product.get("odl_pi_type")));
			model = new T_Batch_OrderList();
			model.setOdl_pd_code(StringUtil.trimString(product.get("pd_code")));
			model.setOdl_cr_code(StringUtil.trimString(product.get("cr_code")));
			model.setOdl_sz_code(StringUtil.trimString(product.get("sz_code")));
			model.setOdl_szg_code(StringUtil.trimString(product.get("pd_szg_code")));
			model.setOdl_br_code(StringUtil.trimString(product.get("br_code")));
			model.setOdl_sub_code(model.getOdl_pd_code()+model.getOdl_cr_code()+model.getOdl_sz_code()+model.getOdl_br_code());
			model.setOdl_amount(Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			model.setOdl_realamount(0);
			if(odl_pi_type.intValue() == 1){//赠品
				model.setOdl_unitprice(0d);
			}else {
				model.setOdl_unitprice(Double.parseDouble(StringUtil.trimString(product.get("unitPrice"))));
			}
			model.setOdl_retailprice(Double.parseDouble(StringUtil.trimString(product.get("retailPrice"))));
			model.setOdl_costprice(Double.parseDouble(StringUtil.trimString(product.get("costPrice"))));
			model.setOdl_remark("");
			model.setOdl_pi_type(odl_pi_type);
			model.setOdl_type(od_type);
			model.setOdl_us_id(user.getUs_id());
			model.setCompanyid(user.getCompanyid());
			model.setOperate_type(StringUtil.trimString(product.get("operate_type")));
			resultList.add(model);
		}
		return resultList;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<String[]> convertMap2Model_import(Map<String, Object> data,T_Sys_User user){
		List<String[]> resultList = new ArrayList<String[]>();
		List products = (List)data.get("datas");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		String[] item = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			item = new String[2];
			item[0] = StringUtil.trimString(product.get("barCode"));
			item[1] = StringUtil.trimString(product.get("amount"));
			resultList.add(item);
		}
		return resultList;
	}
	
	public static Map<String,Object> buildPrintJson(Map<String,Object> paramMap,Integer printMode){
		Map<String,Object> htmlMap = null;
		PrintVO.List2Map(paramMap);
		if(printMode != null){
			if(printMode.intValue() == 0){
				htmlMap = getPrintListHTML(paramMap);
			}
			if(printMode.intValue() == 1){
				htmlMap = getPrintSizeHTML(paramMap);
			}
			if(printMode.intValue() == 2){
				htmlMap = getPrintSumHTML(paramMap);
			}
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] getHeadFieldHTML(Map<String,Object> paramMap){
		StringBuffer headHtml = new StringBuffer(""),footHtml = new StringBuffer("");
		List<T_Sys_PrintField> printFields = (List<T_Sys_PrintField>)paramMap.get("fields");
		Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
		T_Batch_Order order = (T_Batch_Order)paramMap.get("order");
		T_Batch_Client client = (T_Batch_Client)paramMap.get("client");
		T_Sys_User user = (T_Sys_User)paramMap.get("user");
		headHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		headHtml.append("<tbody>");
		headHtml.append("<tr>");
		headHtml.append("<td colspan=\"3\" class=\"head-title\"><b class=\"head-font\">"+printSetMap.get(PrintUtil.PrintSet.PAGE_TITLE)+"&nbsp;</b></td>");
		headHtml.append("</tr>");
		int index = -1;
		double i = 1;
		for(T_Sys_PrintField printField:printFields){
			String colspan = "";
			if(printField.getSpf_position().intValue() == PrintUtil.FALSE){
				if(index != printField.getSpf_line()){
					if(index == -1){
						headHtml.append("<tr>");
					}else{
						i ++;
						headHtml.append("</tr><tr>");
					}
				}
				if(printField.getSpf_colspan() > 1){
					colspan = "colspan='"+printField.getSpf_colspan()+"'";
				}
				
				if(PrintUtil.PrintField.MAKEDATE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_make_date()).append("</td>");
				}
				if(PrintUtil.PrintField.MANAGER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_manager()).append("</td>");
				}
				if(PrintUtil.PrintField.NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_number()).append("</td>");
				}
				if(PrintUtil.PrintField.CLIENT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getClient_name()).append("</td>");
				}
				if(PrintUtil.PrintField.TEL.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_tel()).append("</td>");
				}
				if(PrintUtil.PrintField.MOBILE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_mobile()).append("</td>");
				}
				if(PrintUtil.PrintField.DEPOT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getDepot_name()).append("</td>");
				}
				if(PrintUtil.PrintField.LINKMAN.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(client.getCi_man()).append("</td>");
				}
				if(PrintUtil.PrintField.HAND_NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_handnumber()).append("</td>");
				}
				if(PrintUtil.PrintField.AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_amount()).append("</td>");
				}
				if(PrintUtil.PrintField.REAL_AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_realamount()).append("</td>");
				}
				if(PrintUtil.PrintField.GAP_AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_amount()-order.getOd_realamount()).append("</td>");
				}
				if(PrintUtil.PrintField.MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(order.getOd_money(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.ADDRESS.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(StringUtil.trimString(client.getCi_addr())).append("</td>");
				}
				if(PrintUtil.PrintField.REMARK.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(order.getOd_remark()).append("</td>");
				}
				index = printField.getSpf_line();
			}else if(printField.getSpf_position().intValue() == PrintUtil.TRUE){
				if(PrintUtil.PrintField.END_MAKER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString(order.getOd_maker())).append("</span>");
				}
				if(PrintUtil.PrintField.END_APPROVER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString("TODO")).append("</span>");
				}
			}
		}
		headHtml.append("</tr>");
		//标题高度
		double titleheight = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TITLE_HEIGHT));
		//表头每行高度
		double head_height = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.HEAD_HEIGHT));
		//表格数据与表头间距
		double table_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOP));
		double page_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.PAGE_TOP));
		//总高度
		double sum_height = page_top + table_top + titleheight + (head_height * i);
		headHtml.append(" </tbody>");
		headHtml.append("</table>");
		String[] html = new String[3];
		html[0] = headHtml.toString();
		html[1] = footHtml.toString();
		html[2] = sum_height + "";
		return html;
	}
	
	public static boolean IsExistBras(List<T_Batch_OrderList> orderLists){
		boolean flag = false;
		for(T_Batch_OrderList item :orderLists){
			if(StringUtil.isNotEmpty(item.getOdl_br_code())){
				flag = true;
				break;
			}
		}
		return flag;
	}
	private static String formatOdl_pi_type(Integer odl_pi_type){
		if(odl_pi_type == null){
			return "";
		}
		if(odl_pi_type.intValue() == 1){
			return "赠品";
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintListHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Batch_OrderList> orderList = (List<T_Batch_OrderList>)paramMap.get("orderList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			boolean isHas = IsExistBras(orderList);
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th>");
						html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
						html.append(printData.getSpd_name());
						html.append("</div>");
						html.append("</th>");
					}
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,amount = 0,realamount=0;
			double unitMoney = 0d,retailMoney = 0d,costMoney = 0d;
			html.append("<tbody>");			
			for(T_Batch_OrderList item: orderList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SZ_NAME)).append(">");
						html.append(item.getSz_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getOdl_amount()+"</td>");
						amount += item.getOdl_amount();
					}
					if(PrintUtil.PrintData.REAL_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REAL_AMOUNT)).append(">");
						html.append(item.getOdl_realamount()+"</td>");
						realamount += item.getOdl_realamount();
					}
					if(PrintUtil.PrintData.GAP_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GAP_AMOUNT)).append(">");
						html.append((item.getOdl_amount()-item.getOdl_realamount())+"</td>");
					}
					
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_unitprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_unitmoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
						unitMoney += item.getOdl_unitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_retailprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_retailmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
						retailMoney += item.getOdl_retailmoney();
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_retailprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_costmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
						costMoney += item.getOdl_costmoney();
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_unitprice()/item.getOdl_retailprice(), CommonUtil.PRICE_LIMIT_BATCH, CommonUtil.PRICE_LIMIT_RETAIL,user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GIFT)).append(">");
						html.append(formatOdl_pi_type(item.getOdl_pi_type())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getOdl_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.REAL_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.GAP_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+amount+"</td>");
					}
					if(PrintUtil.PrintData.REAL_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+realamount+"</td>");
					}
					if(PrintUtil.PrintData.GAP_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+(amount-realamount)+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(unitMoney, CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(retailMoney, CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(costMoney, CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			if(isHas){
				colspan = dataSize+1;
			}else{
				colspan = dataSize;
			}
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSizeHTML(Map<String,Object> paramMap){
		StringBuffer html = new StringBuffer("");
		Map<String,Object> htmlMap = null;
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Batch_OrderList> orderList = (List<T_Batch_OrderList>)paramMap.get("orderList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			List<T_Base_SizeList> sizeGroupList = (List<T_Base_SizeList>)paramMap.get("sizeGroupList");
			int[] maxColNumArray = new int[1];
			Map<String, List<String>> sizeMap = new HashMap<String, List<String>>();
			List<List<String>> listResult = SizeHorizontalVO.getSizeModeTitleInfoList(sizeMap, maxColNumArray, sizeGroupList);
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			List<T_Batch_SizeCommon> sizeCommonList = getSizeModeList(orderList, maxColNumArray[0], sizeMap);
			if(listResult.size()==0 || sizeCommonList.size()==0) {
				return null;
			}
			int[] totalSizeBySingle = new int[maxColNumArray[0]];
			boolean isHas = IsExistBras(orderList);
			
			//根据商家ID和货号查询出对应的尺码组信息在title输出
		    int rowSize = listResult.size()+1;
		    html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
		    html.append("<thead>");
		    html.append("<tr>");
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th rowspan=\""+rowSize+"\">");
						html.append(printData.getSpd_name());
						html.append("</th>");
					}
		    	}else if(PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){
		    		html.append("<th colspan=\""+maxColNumArray[0]+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}else if(!PrintUtil.PrintData.REMARK.equals(printData.getSpd_code())
						&&!PrintUtil.PrintData.REAL_AMOUNT.equals(printData.getSpd_code())
						&&!PrintUtil.PrintData.GAP_AMOUNT.equals(printData.getSpd_code())
						){
					html.append("<th rowspan=\""+(rowSize)+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}
			}

		    html.append("</tr>"); 
		    if(printDataMap.containsKey(PrintUtil.PrintData.SZ_NAME)){
			    for(int i=0;i<listResult.size();i++){  
			    	html.append("<tr>");
			    	List<String> sizeList = (List<String>)listResult.get(i);
			    	for (int k = 0;k<maxColNumArray[0];k++){
			    		html.append("<th>");//入库单详细信息
			    		if (k < sizeList.size()){
			    			html.append(sizeList.get(k).split("_AND_")[1]);
			    		}else{
			    			html.append("-");
			    		}
			    		html.append("</th>");
			    	}
			    	html.append("</tr>");
			    }
		    }
		    html.append("</thead>");
		    int i = 0,amount = 0;
		    double unitMoney = 0d,retailMoney = 0d,costMoney = 0d;
			for (T_Batch_SizeCommon item : sizeCommonList) {
				i++;
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td nowrap").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						
						int[] arr = item.getSingle_amount();
						for(int k = 0;k< maxColNumArray[0];k++){	
							html.append("<td align='center'>");
							if (k<arr.length){
								html.append(arr[k] == 0 ? "" : arr[k]);//单个尺码数量
								totalSizeBySingle[k] = totalSizeBySingle[k]+ arr[k];
							}
							html.append("</td>");
						}
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getTotalamount()+"</td>");
						amount += item.getTotalamount();
					}
					
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitmoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getUnitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getRetailprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getRetailmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
						retailMoney += item.getRetailmoney();
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getCostprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getCostmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
						costMoney += item.getCostmoney();
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitprice()/item.getRetailprice(),CommonUtil.PRICE_LIMIT_BATCH,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GIFT)).append(">");
						html.append(formatOdl_pi_type(Integer.parseInt(item.getPd_type()))+"</td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tfoot>");
			boolean hasMaxSize = false;
			html.append("<tr>");
			for (int j = 0; j < printDatas.size(); j++) {
				if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
					html.append("<td>总计</td>");
				}
				if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
					if (isHas){
						html.append("<td></td>");
					}
				}
				if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
					for(int k = 0;k< maxColNumArray[0];k++){	
						hasMaxSize = true;
						html.append("<td style=\"text-align:center;\">");
						if(totalSizeBySingle[k] != 0){
							html.append(totalSizeBySingle[k]);
						}
						html.append("</td>");
					}	
					if (!hasMaxSize){
						html.append("<td>");html.append("</td>");
					} 
				}
				if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
					html.append("<td style=\"text-align:right;\">");
					html.append(amount+"</td>");
				}
				if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</span></td>");
				}
				if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(retailMoney,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</span></td>");
				}
				if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(costMoney,CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</span></td>");
				}
				if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
			}
			html.append("</tr>");
			
			int dataSize = printDatas.size()-1;
			if(printDataMap.containsKey(PrintUtil.PrintData.REMARK)){
				dataSize -= 1;
			}
			int colspan = 0;
			if(isHas){
				colspan = dataSize;
			}else if(printDataMap.containsKey(PrintUtil.PrintData.BR_NAME)){
				colspan = dataSize-1;
			}else{
				colspan = dataSize;
			}
			colspan += maxColNumArray[0];
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tr>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSumHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Batch_OrderList> orderList = (List<T_Batch_OrderList>)paramMap.get("orderList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.CR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){//汇总模式不显示尺码杯型颜色
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,amount = 0,realamount=0;
			double unitMoney = 0d,retailMoney = 0d,costMoney = 0d;
			html.append("<tbody>");			
			for(T_Batch_OrderList item: orderList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getOdl_amount()+"</td>");
						amount += item.getOdl_amount();
					}
					if(PrintUtil.PrintData.REAL_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REAL_AMOUNT)).append(">");
						html.append(item.getOdl_realamount()+"</td>");
						realamount += item.getOdl_realamount();
					}
					if(PrintUtil.PrintData.GAP_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GAP_AMOUNT)).append(">");
						html.append((item.getOdl_amount()-item.getOdl_realamount())+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_unitprice(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_unitmoney(),CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getOdl_unitmoney();
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_retailprice(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RETAIL_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_retailmoney(),CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
						retailMoney += item.getOdl_retailmoney();
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_costprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.COST_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_costmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
						costMoney += item.getOdl_costmoney();
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REBATE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getOdl_unitprice()/item.getOdl_retailprice(),CommonUtil.PRICE_LIMIT_BATCH,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.GIFT)).append(">");
						html.append(formatOdl_pi_type(item.getOdl_pi_type())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getOdl_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.REAL_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.GAP_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+amount+"</td>");
					}
					if(PrintUtil.PrintData.REAL_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+realamount+"</td>");
					}
					if(PrintUtil.PrintData.GAP_AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+(amount-realamount)+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_BATCH, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.RETAIL_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RETAIL_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(retailMoney,CommonUtil.PRICE_LIMIT_RETAIL, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.COST_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.COST_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(costMoney,CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REBATE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.GIFT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			colspan = dataSize-2;
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
}
