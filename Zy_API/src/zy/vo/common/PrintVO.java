package zy.vo.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintSet;
import zy.util.PrintUtil;

public class PrintVO {
	
	@SuppressWarnings("unchecked")
	public static void List2Map(Map<String,Object> paramMap){
		Map<String,Object> printSetMap = null,printDataMap = null;
		List<T_Sys_PrintSet> printSets = (List<T_Sys_PrintSet>)(paramMap.get("sets"));
		List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
		try {
			printSetMap = new HashMap<String, Object>(printSets.size());
			for(T_Sys_PrintSet printSet:printSets){
				printSetMap.put(printSet.getSps_code(), printSet.getSps_name());
			}
			printDataMap = new HashMap<String, Object>(printDatas.size());
			Map<String,Object> subDataMap = null;
			for(T_Sys_PrintData printData:printDatas){
				subDataMap = new HashMap<String, Object>(2);
				subDataMap.put(PrintUtil.WIDTH, printData.getSpd_width());
				subDataMap.put(PrintUtil.ALIGN, printData.getSpd_align());
				printDataMap.put(printData.getSpd_code(), subDataMap);
			}
			paramMap.put("printSetMap", printSetMap);
			paramMap.put("printDataMap", printDataMap);
		} catch (Exception e) {
		}
	}
	
	@SuppressWarnings("unchecked")
	public static String getFieldStyleValue(Map<String,Object> printDataMap,String key){
		StringBuffer html = new StringBuffer();
		Map<String,Object> subDataMap = (Map<String,Object>)printDataMap.get(key);
		int width = (Integer)subDataMap.get(PrintUtil.WIDTH);
		width -= 2;
		html.append(" style='text-align:"+PrintUtil.doAlign(""+subDataMap.get(PrintUtil.ALIGN))+";min-width:"+width+"mm;'");
		return html.toString();
	}
	
	@SuppressWarnings("unchecked")
	public static String getPrintStyleHTML(Map<String,Object> paramMap){
		StringBuffer styleHtml = new StringBuffer();
		Map<String,Object> setMap = (Map<String,Object>)paramMap.get("printSetMap");
		try {
			double page_left = Double.parseDouble((String)setMap.get(PrintUtil.PrintSet.PAGE_LEFT));
			double page_right = Double.parseDouble((String)setMap.get(PrintUtil.PrintSet.PAGE_RIGHT));
			double page_top = Double.parseDouble((String)setMap.get(PrintUtil.PrintSet.PAGE_TOP));
			double page_bottom = Double.parseDouble((String)setMap.get(PrintUtil.PrintSet.PAGE_BOTTOM));
			styleHtml.append("<style>");
			styleHtml.append(" body{padding-left:"+page_left+"mm;padding-right:"+page_right+"mm;padding-top:"+page_top+"mm;padding-bottom:"+page_bottom+"mm;}");
			styleHtml.append(" table{border-collapse:collapse;}");
			styleHtml.append(" p{margin:0 auto;line-height:18px;}");
			styleHtml.append(" .head-title{text-align:center;height:"+setMap.get(PrintUtil.PrintSet.TITLE_HEIGHT)+"mm;}");
			styleHtml.append(" .head-font{font-size:").append(setMap.get(PrintUtil.PrintSet.TITLE_SIZE)).append("px;");
			styleHtml.append(" font-family:\"").append(setMap.get(PrintUtil.PrintSet.TITLE_FONT)).append("\",Arial,Helvetica,sans-serif;}");
			styleHtml.append(" .head-print{border:0;width:100%;}");
			styleHtml.append(" .head-print tr td{width:33%;height:").append(setMap.get(PrintUtil.PrintSet.HEAD_HEIGHT)).append("mm;");
			styleHtml.append(" font-size:").append(setMap.get(PrintUtil.PrintSet.HEAD_SIZE)).append("px;");
			styleHtml.append(" font-family:\"").append(setMap.get(PrintUtil.PrintSet.HEAD_FONT)).append("\",Arial,Helvetica,sans-serif;}");
			styleHtml.append(" .data-print{width:100%;font-size:").append(setMap.get(PrintUtil.PrintSet.TABLE_SIZE)).append("px;border:#000 solid 1px;border-width:1px 0 0 0px;}");//margin-top:"+setMap.get(PrintParam.TABLE_TOP)+"mm;
			String table_blod = (String)setMap.get(PrintUtil.PrintSet.TABLE_BOLD);
			int blod_size = 400;
			if(table_blod != null && (""+PrintUtil.TRUE).equals(table_blod)){
				blod_size = 700;
			}
			styleHtml.append(" .data-print th{height:5mm;font-weight:"+blod_size+";border:#000 solid 1px;border-width:0 1px 1px 1px;}");
			styleHtml.append(" .data-print td{line-height:"+setMap.get(PrintUtil.PrintSet.TABLE_DATAHEIGHT)+"mm;border:#000 solid 1px;border-width:0 1px 1px 1px;font-size:").append(setMap.get(PrintUtil.PrintSet.TABLE_SIZE)).append("px;}");
			styleHtml.append(" .data-print td.table-remark{border:0px;}");
			styleHtml.append("</style>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return styleHtml.toString();
	}
}
