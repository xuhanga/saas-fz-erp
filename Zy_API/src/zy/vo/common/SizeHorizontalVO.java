package zy.vo.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import zy.dto.common.ProductDto;
import zy.entity.base.size.T_Base_Size;
import zy.entity.base.size.T_Base_SizeList;
import zy.util.StringUtil;

/**
 * 尺码横排通用VO
 */
public class SizeHorizontalVO {
	
	public static Map<String, Object> getJsonSizeTitles(List<T_Base_SizeList> sizeGroupList){
		List<List<String>> titles = new ArrayList<List<String>>();
        Map<String, Object> sizeCodeMap = new HashMap<String, Object>();
		if (sizeGroupList != null && sizeGroupList.size() > 0) {
			int[] maxColNumArray = new int[1];
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            List<List<String>> lstRes = getSizeModeTitleInfoList(map, maxColNumArray, sizeGroupList);
            Collections.sort(lstRes, new Comparator<List<String>>() {
                @Override
                public int compare(List<String> o1, List<String> o2) {
                    return o1.size() > o2.size() ? -1 : 0;
                }
            });
            int sizeMaxSize = maxColNumArray[0];
            String title = null;
            List<String> subTitles = null;
            for (int i = 0; i < lstRes.size(); i++) {
                subTitles = new ArrayList<String>();
                ArrayList<String> sizeList = (ArrayList<String>) lstRes.get(i);
                for (int k = 0; k < sizeMaxSize; k++) {
                    if (k < sizeList.size()) {
                        title = sizeList.get(k).toString();
                        subTitles.add(title);
                    } else {
                        subTitles.add("-");
                    }
                }
                titles.add(subTitles);
            }
            for (Map.Entry<String, List<String>> et : map.entrySet()) {
                if (StringUtils.endsWith(et.getKey(), "_name")) {
                    sizeCodeMap.put(StringUtils.remove(et.getKey(), "_name"), et.getValue());
                }
            }
		}else {
			titles.add(new ArrayList<String>());
		}
        if (titles == null || titles.size() == 0) {
            titles.add(new ArrayList<String>());
        }
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("titles", titles);
        resultMap.put("sizeCodeMap", sizeCodeMap);
        return resultMap;
	}
	
	public static List<List<String>> getSizeModeTitleInfoList(Map<String, List<String>> map, int[] maxColNumArray,
			List<T_Base_SizeList> sizeModeInfoList) {
		List<List<String>> lstRes = new ArrayList<List<String>>();
		try {
			if (sizeModeInfoList != null) {
				String szg_Code = "";
				int maxColNum = 0;
				int colNum = 0;
				List<String> subSzCodeLst = null;
				List<String> subSzNameLst = null;
				List<String> subSzNameLst1 = null;
				for (T_Base_SizeList bs : sizeModeInfoList) {
					if (!bs.getSzl_szg_code().equals(szg_Code)) {
						if (colNum > maxColNum) {
							maxColNum = colNum;
						}
						szg_Code = bs.getSzl_szg_code();
						colNum = 0;
						subSzCodeLst = new ArrayList<String>();
						subSzNameLst = new ArrayList<String>();
						subSzNameLst1 = new ArrayList<String>();
						map.put(bs.getSzl_szg_code(), subSzCodeLst);
						map.put(bs.getSzl_szg_code() + "_name", subSzNameLst);
						lstRes.add(subSzNameLst1);
					}

					subSzCodeLst.add(bs.getSzl_sz_code());
					subSzNameLst.add(bs.getSz_name());
					subSzNameLst1.add(String.valueOf(bs.getSzl_sz_code()) + "_AND_" + bs.getSz_name());
					colNum++;
				}
				if (colNum > maxColNum) {
					maxColNum = colNum;
				}
				maxColNumArray[0] = maxColNum;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstRes;
	}

	public static Map<String, Object> buildJsonProductInput(String pd_code,List<T_Base_Size> sizes,List<ProductDto> inputs,
			List<ProductDto> stocks,List<ProductDto> temps,
			Map<String,Object> usableStockMap){
		List<Map<String, Object>> columns = new ArrayList<Map<String,Object>>();
		Map<String, Object> item = null;
		for (T_Base_Size size : sizes) {
			item = new HashMap<String, Object>();
			item.put("code", size.getSz_code());
			item.put("name", size.getSz_name());
			columns.add(item);
		}
		List<Map<String, Object>> rows = new ArrayList<Map<String,Object>>();
		int rowIndex=1;
		for (ProductDto brColor : inputs) {
			item = new HashMap<String, Object>();
			item.put("id", String.valueOf(rowIndex));rowIndex++;
			item.put("cr_code", brColor.getCr_code());
			item.put("cr_name", brColor.getCr_name());
			item.put("br_code", brColor.getBr_code());
			item.put("br_name", brColor.getBr_name());
			int amountStock=0;
			Integer amountInput = null;
			for (T_Base_Size size : sizes) {
				for (int i = 0; i <stocks.size(); i++) {
					ProductDto stock=stocks.get(i);
					if (brColor.getCr_code().equals(stock.getCr_code())
							&& size.getSz_code().equals(stock.getSz_code())
							&& brColor.getBr_code().equals(stock.getBr_code())) {
						amountStock = stock.getAmount();
						stocks.remove(stock);
						break;
					}
				}
				for (int i = 0; i <temps.size(); i++) {
					ProductDto temp=temps.get(i);
					if (brColor.getCr_code().equals(temp.getCr_code())
							&& size.getSz_code().equals(temp.getSz_code())
							&& brColor.getBr_code().equals(temp.getBr_code())) {
						amountInput = temp.getAmount();
						temps.remove(temp);
						break;
					}
				}
				//货号+颜色+尺码+杯型
				String sub_code = StringUtil.trimString(pd_code).toUpperCase()
						+ StringUtil.trimString(brColor.getCr_code())
						+ StringUtil.trimString(size.getSz_code())
						+ StringUtil.trimString(brColor.getBr_code());
				if (usableStockMap != null){//启用可用库存
					Object obj=usableStockMap.get(sub_code);
					int usableStockAmount =0;
					if (obj!=null){
						usableStockAmount = (Integer)obj;
					}
					if (amountInput != null || usableStockAmount != 0 || amountStock != 0) {
						item.put(size.getSz_code(), amountStock+"/"+(amountStock+usableStockAmount)+"/"+(amountInput==null?"":amountInput));//(amountInput == 0 ? "" : amountInput)
					}
				}else {
					if (amountInput != null || amountStock != 0) {
						item.put(size.getSz_code(), amountStock + "/" + (amountInput==null?"":amountInput));
					}
				}
				amountInput=null;amountStock=0;
			}
			rows.add(item);
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("columns", columns);
		resultMap.put("rows", rows);
		return resultMap;
	}
	
}
