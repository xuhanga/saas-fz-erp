package zy.vo.sell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.sell.cash.T_Sell_Shop_Temp;
import zy.entity.shop.sale.T_Shop_Sale_Present;
import zy.entity.vip.member.T_Vip_Member;
import zy.form.shop.SaleForm;
import zy.util.CommonUtil;
import zy.util.NumberUtil;
import zy.util.StringUtil;

public class SaleVO {
	
	public static int buildScope(String model){
		if(StringUtil.isNotEmpty(model)){
			return NumberUtil.toInteger(model.substring(1,2));
		}else{
			return 0;
		}
	}
	
	public static int buildRule(String model){
		if(StringUtil.isNotEmpty(model)){
			return NumberUtil.toInteger(model.substring(2,3));
		}else{
			return 0;
		}
	}
	
	public static Map<String,Object> getGift(List<T_Shop_Sale_Present> list){
		List<String> codes = new ArrayList<String>();
		Map<String,Object> map = new HashMap<String, Object>();;
		for(T_Shop_Sale_Present p:list){
			codes.add(p.getSsp_subcode());
			if(p.getSsp_addMoney() > 0){
				if(map.containsKey(p.getSsp_ss_code())){
					map.put(p.getSsp_ss_code(), p.getSsp_count()+Integer.parseInt(StringUtil.trimString(map.get(p.getSsp_ss_code()))));
				}else{
					map.put(p.getSsp_ss_code(), p.getSsp_count());
				}
				map.put("_"+p.getSsp_ss_code(), p.getSsp_addMoney());
			}
		}
		Map<String,Object> result = new HashMap<String, Object>();
		result.put("subcodes", codes);
		result.put("giftmoney", map);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static void buildGifts(List<T_Sell_Shop_Temp> list,Map<String, Object> param){
		Map<String,Object> giftMap = (Map<String,Object>)param.get("giftmoney");
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String money_decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
		String money_type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
		for(T_Sell_Shop_Temp temp:list){
			if(giftMap.containsKey(temp.getSht_sale_code())){
				double money = Double.parseDouble(String.valueOf(giftMap.get("_"+temp.getSht_sale_code())));
				Integer amount = Integer.parseInt(String.valueOf(giftMap.get(temp.getSht_sale_code())));
				temp.setSht_price(NumberUtil.buildMoney(money_decimal, money_type, money/amount));
			}else{
				temp.setSht_price(0d);
			}
		}
	}
	/**
	 * 促销的处理
	 * 把所有商品与促销以及赠品都取出来，进行业务比对
	 * 更新临时表集合1
	 * 1.对每个商品进行促匹配，找出对当前商品的最高等级的促销
	 * 2.把找到的临时表集合进行促销处理
	 * */
	@SuppressWarnings("unchecked")
	public static int buildSale(List<T_Sell_Shop_Temp> list,
			List<SaleForm> saleList, List<T_Shop_Sale_Present> giftList,
			Map<String, Object> param) {
		int index = 0;
		T_Vip_Member vip = (T_Vip_Member) param.get(CommonUtil.KEY_VIP);
		Map<String,Object> saleMap = new HashMap<String, Object>();
		
		//把是促销的数据找出来
		for (SaleForm sale : saleList) {
			for (T_Sell_Shop_Temp temp : list) {
				if(1 == temp.getSht_isgift()){//如果是赠品，不直接下一条
					continue;
				}
				if(2 == temp.getSht_isgift()){//如果是送的商品，则先把金额还原
					temp.setSht_money(temp.getSht_price()*temp.getSht_amount());
					temp.setSht_isgift(0);
				}
				if (null != vip) {//会员存在情况的下促销
					if ("N".equals(sale.getSs_mt_code())) {//不是会员的促销
						continue;
					}
					if ("Y".equals(sale.getSs_mt_code())//促销只针对是会员
							|| "ALL".equals(sale.getSs_mt_code())//促销针对是全场
							|| vip.getVm_mt_code().equals(sale.getSs_mt_code())) {//促销针对是具体的某个会员类别
						setSale(temp, sale);			
						index ++;
						continue;
					}
				}else{//无会员存在下的促销流程
					if("ALL".equals(sale.getSs_mt_code()) || "N".equals(sale.getSs_mt_code())){
						setSale(temp, sale);
						index ++;
					}
				}
			}
			if(saleMap.containsKey(sale.getSs_code())){//促销集合里存在该促销了，就从集合里获取该促销
				List<SaleForm> subList = (List<SaleForm>)saleMap.get(sale.getSs_code());
				subList.add(sale);
			}else{//如果集合里没有该促销，就从把当前促销放在集合里
				List<SaleForm> subList = new ArrayList<SaleForm>(); 
				subList.add(sale);
				saleMap.put(sale.getSs_code(), subList);
			}
		}
		index += setSaleRate(list,saleMap,giftList,param);//进行促销打折
		return index;
	}
	@SuppressWarnings("unchecked")
	private static int setSaleRate(List<T_Sell_Shop_Temp> list,
			Map<String,Object> saleMap, List<T_Shop_Sale_Present> giftList,
			Map<String, Object> param){
		Map<String,Object> setMap = (Map<String,Object>)param.get(CommonUtil.KEY_CASHIER_SET);
		String money_decimal = (String)setMap.get("KEY_MONEY_DECIMAL");//金额位数：1整数2一位小数3两位小数
		String money_type = (String)setMap.get("KEY_MONEY_TYPE");//金额取舍1四舍五入2只入不舍3只舍不入
		int index = 0;
		for (T_Sell_Shop_Temp temp : list) {
			String model = temp.getSht_sale_model();
			String code = temp.getSht_sale_code();
			if(1 != temp.getSht_isgift()){//当前数据不是赠品
				if(!StringUtil.isEmpty(model) && !StringUtil.isEmpty(code)){//有促销
					if(index > 0 && "311".equals(model)){
						//针对是全场的买几送几，只能计算一次，还没有找到更好的办法
					}else{
						SaleFactory.model(list,giftList,model,temp,(List<SaleForm>)saleMap.get(code),money_decimal,money_type,param);
						index ++;
					}
				}
			}
		}
		return index;
	}
	private static void setSale(T_Sell_Shop_Temp temp,SaleForm sale){
		int scope = buildScope(sale.getSs_model());
		if(1 == scope){//全场
			if(!StringUtil.isEmpty(temp.getSht_sale_code())){
				if(null != temp.getSht_sale_priority()){
					if(temp.getSht_sale_priority() > sale.getSs_priority()){
						setSaleType(temp, sale);
					}
				}
			}else{
				setSaleType(temp, sale);
			}
		}
		if(2 == scope){//类别
			if(!StringUtil.isEmpty(temp.getSht_sale_code())){
				if(null != temp.getSht_sale_priority()){
					if(temp.getSht_sale_priority() > sale.getSs_priority()){
						if(temp.getSht_tp_code().equals(sale.getSst_tp_code())){
							setSaleType(temp, sale);
						}
					}
				}
			}else{
				if(temp.getSht_tp_code().equals(sale.getSst_tp_code())){
					setSaleType(temp, sale);
				}
			}
		}
		if(3 == scope){//品牌
			if(!StringUtil.isEmpty(temp.getSht_sale_code())){
				if(null != temp.getSht_sale_priority()){
					if(temp.getSht_sale_priority() > sale.getSs_priority()){
						if(temp.getSht_bd_code().equals(sale.getSsb_bd_code())){
							setSaleType(temp, sale);
						}
					}
				}
			}else{
				if(temp.getSht_bd_code().equals(sale.getSsb_bd_code())){
					setSaleType(temp, sale);
				}
			}
		}
		if(4 == scope){//商品
			if(!StringUtil.isEmpty(temp.getSht_sale_code())){
				if(null != temp.getSht_sale_priority()){
					if(temp.getSht_sale_priority() > sale.getSs_priority()){
						if(temp.getSht_pd_code().equals(sale.getSsp_pd_code())){
							setSaleType(temp, sale);
						}
					}
				}
			}else{
				if(temp.getSht_pd_code().equals(sale.getSsp_pd_code())){
					setSaleType(temp, sale);
				}
			}
		}
	}
	
	private static void setSaleType(T_Sell_Shop_Temp temp,SaleForm sale){
		temp.setSht_sale_code(sale.getSs_code());
		temp.setSht_sale_model(sale.getSs_model());
		temp.setSht_sale_priority(sale.getSs_priority());
		if(1 == sale.getSs_point()){//只有本身是积分的，则可以积分，可设为不积分f
			if(1 == temp.getSht_ispoint()){
				temp.setSht_ispoint(sale.getSs_point());
			}
		}
	}
	static class SaleFactory{
		//全场金额
		private static double getAllMoney(List<T_Sell_Shop_Temp> list,String ss_code){
			double total_money = 0d;
			for(T_Sell_Shop_Temp temp:list){
				if(1 != temp.getSht_isgift()){
					if(ss_code.equals(temp.getSht_sale_code())){
						total_money += temp.getSht_amount()*temp.getSht_sell_price();
					}
				}
			}
			return total_money;
		}
		private static double getTypeMoney(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp){
			double total_money = 0d;
			for(T_Sell_Shop_Temp _temp:list){
				if(1 != _temp.getSht_isgift()){
					if(_temp.getSht_tp_code().equals(temp.getSht_tp_code())){
						if(temp.getSht_sale_code().equals(_temp.getSht_sale_code())){
							total_money += _temp.getSht_amount()*_temp.getSht_sell_price();
						}
					}
				}
			}
			return total_money;
		}
		private static double getBrandMoney(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp _temp){
			double total_money = 0d;
			for(T_Sell_Shop_Temp temp:list){
				if(1 != temp.getSht_isgift()){
					if(_temp.getSht_bd_code().equals(temp.getSht_bd_code())){
						if(_temp.getSht_sale_code().equals(temp.getSht_sale_code())){
							total_money += temp.getSht_amount()*temp.getSht_sell_price();
						}
					}
				}
			}
			return total_money;
		}
		private static double getProductMoney(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp _temp){
			double total_money = 0d;
			for(T_Sell_Shop_Temp temp:list){
				if(1 != temp.getSht_isgift()){
					if(temp.getSht_pd_code().equals(_temp.getSht_pd_code())){
						if(_temp.getSht_sale_code().equals(temp.getSht_sale_code())){
							total_money += temp.getSht_amount()*temp.getSht_sell_price();
						}
					}
				}
			}
			return total_money;
		}
		//相同的促销，则算总数据，
		private static double getAllAmount(List<T_Sell_Shop_Temp> list,String ss_code){
			double total_amount = 0d;
			for(T_Sell_Shop_Temp temp:list){
				if(1 != temp.getSht_isgift()){
					if(ss_code.equals(temp.getSht_sale_code())){
						total_amount += temp.getSht_amount();
					}
				}
			}
			return total_amount;
		}
		private static double getTypeAmount(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp){
			double total_amount = 0d;
			for(T_Sell_Shop_Temp _temp:list){
				if(1 != _temp.getSht_isgift()){
					if(_temp.getSht_tp_code().equals(temp.getSht_tp_code())){
						if(temp.getSht_sale_code().equals(_temp.getSht_sale_code())){
							total_amount += _temp.getSht_amount();
						}
					}
				}
			}
			return total_amount;
		}
		private static double getBrandAmount(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp _temp){
			double total_amount = 0d;
			for(T_Sell_Shop_Temp temp:list){
				if(1 != temp.getSht_isgift()){
					if(_temp.getSht_bd_code().equals(temp.getSht_bd_code())){
						if(_temp.getSht_sale_code().equals(temp.getSht_sale_code())){
							total_amount += temp.getSht_amount();
						}
					}
				}
			}
			return total_amount;
		}
		private static double getProductAmount(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp){
			double total_amount = 0d;
			for(T_Sell_Shop_Temp _temp:list){
				if(1 != _temp.getSht_isgift()){
					if(temp.getSht_pd_code().equals(_temp.getSht_pd_code())){
						if(temp.getSht_sale_code().equals(_temp.getSht_sale_code())){
							total_amount += _temp.getSht_amount();
						}
					}
				}
			}
			return total_amount;
		}
		
		//折扣---全场---直接折扣
		private static void mode111(T_Sell_Shop_Temp temp,List<SaleForm> sale,String decimal,String type){
			double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
			double money = NumberUtil.buildMoney(decimal, type, sell_money*sale.get(0).getSsa_discount());
			updateSale(temp, money, sell_money, decimal, type);
		}
		//折扣---全场---买满金额
		private static void mode112(List<T_Sell_Shop_Temp> list, T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getAllMoney(list, temp.getSht_sale_code());
			double full = 0d,rate = 0d;
			for(SaleForm sale:saleList){
				if(full < sale.getSsa_buy_full()){
					if(total_money > sale.getSsa_buy_full()){
						full = sale.getSsa_buy_full();
						rate = sale.getSsa_discount();
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣--全场--件数
		private static void mode113(List<T_Sell_Shop_Temp> list, T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double amount = 0d,rate = 0d;
			double total_amount = getAllAmount(list, temp.getSht_sale_code());
			for(SaleForm sale:saleList){
				if(amount < sale.getSsa_buy_number()){
					if(total_amount >= sale.getSsa_buy_number()){
						amount = sale.getSsa_buy_number();
						rate = sale.getSsa_discount();
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣---类别---直接折扣
		private static void mode121(T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double rate = 0d;
			for(SaleForm sale:saleList){
				if(temp.getSht_tp_code().equals(sale.getSst_tp_code())){
					rate = sale.getSst_discount();
					break;
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣---类别---买满金额折扣
		private static void mode122(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double full = 0d,rate = 0d;
			double total_money = getTypeMoney(list, temp);
			for(SaleForm sale:saleList){
				if(temp.getSht_tp_code().equals(sale.getSst_tp_code())){
					if(full < sale.getSst_buy_full()){
						if(total_money >= sale.getSst_buy_full()){
							full = sale.getSst_buy_full();
							rate = sale.getSst_discount();
						}
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣--类别--件数
		private static void mode123(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double amount = 0d,rate = 0d;
			double total_amount = getTypeAmount(list, temp);
			for(SaleForm sale:saleList){
				if(temp.getSht_tp_code().equals(sale.getSst_tp_code())){
					if(amount < sale.getSst_buy_number()){
						if(total_amount >= sale.getSst_buy_number()){
							amount = sale.getSst_buy_number();
							rate = sale.getSst_discount();
						}
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣---品牌---直接折扣
		private static void mode131(T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double rate = 0d;
			for(SaleForm sale:saleList){
				if(temp.getSht_bd_code().equals(sale.getSsb_bd_code())){
					rate = sale.getSsb_discount();
					break;
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣---品牌---买满金额折扣
		private static void mode132(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double full = 0d,rate = 0d;
			double total_money = getBrandMoney(list, temp);
			for(SaleForm sale:saleList){
				if(temp.getSht_bd_code().equals(sale.getSsb_bd_code())){
					if(full < sale.getSsb_buy_full()){
						if(total_money >= sale.getSsb_buy_full()){
							full = sale.getSsb_buy_full();
							rate = sale.getSsb_discount();
						}
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣--品牌--件数
		private static void mode133(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double amount = 0d,rate = 0d;
			double total_amount = getBrandAmount(list, temp);
			for(SaleForm sale:saleList){
				if(temp.getSht_bd_code().equals(sale.getSsb_bd_code())){
					if(amount < sale.getSsb_buy_number()){
						if(total_amount >= sale.getSsb_buy_number()){
							amount = sale.getSsb_buy_number();
							rate = sale.getSsb_discount();
						}
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣---商品---直接折扣
		private static void mode141(T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double rate = 0d;
			for(SaleForm sale:saleList){
				if(temp.getSht_pd_code().equals(sale.getSsp_pd_code())){
					rate = sale.getSsp_discount();
					break;
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣---商品---买满金额折扣
		private static void mode142(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double full = 0d,rate = 0d;
			double total_money = getProductMoney(list, temp);
			for(SaleForm sale:saleList){
				if(sale.getSsp_pd_code().equals(temp.getSht_pd_code())){
					if(full < sale.getSsp_buy_full()){
						if(total_money >= sale.getSsp_buy_full()){
							full = sale.getSsp_buy_full();
							rate = sale.getSsp_discount();
						}
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//折扣--商品--件数
		private static void mode143(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double amount = 0d,rate = 0d;
			double total_amount = getProductAmount(list, temp);
			for(SaleForm sale:saleList){
				if(temp.getSht_pd_code().equals(sale.getSsp_pd_code())){
					if(amount < sale.getSsp_buy_number()){
						if(total_amount >= sale.getSsp_buy_number()){
							amount = sale.getSsp_buy_number();
							rate = sale.getSsp_discount();
						}
					}
				}
			}
			if(rate > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, sell_money*rate);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//商品---特价
		private static void mode241(T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double price = 0d;
			for(SaleForm sale:saleList){
				if(temp.getSht_pd_code().equals(sale.getSsp_pd_code())){
					price = sale.getSsp_special_offer();
					break;
				}
			}
			if(price >= 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, price*temp.getSht_amount());
				delVipRate(temp);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//件数特价，不会有折上折情况
		private static void mode242(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double amount = 0d,price = 0d;
			double total_amount = getProductAmount(list, temp);
			for(SaleForm sale:saleList){
				if(temp.getSht_pd_code().equals(sale.getSsp_pd_code())){
					if(amount < sale.getSsp_buy_number()){
						if(total_amount >= sale.getSsp_buy_number()){
							amount = sale.getSsp_buy_number();
							price = sale.getSsp_special_offer();
							break;
						}
					}
				}
			}
			if(price >= 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double money = NumberUtil.buildMoney(decimal, type, price*temp.getSht_amount());
				delVipRate(temp);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//买满几件送发件最低价格商品
		private static void mode311(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_amount = getAllAmount(list, temp.getSht_sale_code());
			double amount = 0d;
			int _amount = 0;
			for(SaleForm sale:saleList){
				if(amount < sale.getSsa_buy_number()){
					if(total_amount >= sale.getSsa_buy_number()){
						amount = sale.getSsa_buy_number();
						_amount = sale.getSsa_donation_number();
					}
				}
			}
			sortList(list);
			/**
			 * 1  2  3
			 * 2  1  3
			 * 1  1  2
			 * 送2 
			 * */
			if(_amount > 0){
				for(T_Sell_Shop_Temp item:list){
					if(_amount > 0){//数量大于0时，则表明赠品的数量还没有送完，小于或等于0时，则证明赠品数量送够了
						if(_amount == item.getSht_amount()){//如果赠品的数量正好和零售单条数量一样，则直接改为赠品
							setGift(item);
						}
						if(_amount > item.getSht_amount()){//如果零售单条数据小于送的赠品数量，则单条零售数量不够，则要再往下循环比对
							setGift(item);
						}
						if(_amount < item.getSht_amount()){//如果零售单条数据大于送的赠品数量，则一条数量就够设为赠品了
							setLastGift(item, _amount);
						}
						_amount = _amount-item.getSht_amount();
					}else{
						break;
					}
				}
			}else{
				for(T_Sell_Shop_Temp item:list){
					delSale(decimal, type, item);
				}
			}
		}
		private static void setGift(T_Sell_Shop_Temp temp){
			temp.setSht_isgift(2);
			temp.setSht_money(0d);
			delHandRate(temp);
			delVipRate(temp);
		}
		
		private static void setLastGift(T_Sell_Shop_Temp temp,int amount){
			temp.setSht_money(temp.getSht_price() * (temp.getSht_amount()-amount));
			delHandRate(temp);
			delVipRate(temp);
		}
		private static void sortList(List<T_Sell_Shop_Temp> list){
			Collections.sort(list, new Comparator<T_Sell_Shop_Temp>() {
	            @Override
	            public int compare(T_Sell_Shop_Temp o1, T_Sell_Shop_Temp o2) {
	                int c = 0;
	                c = o1.getSht_price() <= o2.getSht_price() ? 1 : -1;
	                return  -c;
	            }
	        });
		}
		public static void main(String[] args) {
			/*List<T_Sell_Shop_Temp> list = new ArrayList<T_Sell_Shop_Temp>();
			T_Sell_Shop_Temp temp = new T_Sell_Shop_Temp();
			temp.setSht_price(100d);
			list.add(temp);
			T_Sell_Shop_Temp temp1 = new T_Sell_Shop_Temp();
			temp1.setSht_price(101d);
			list.add(temp1);
			T_Sell_Shop_Temp temp2 = new T_Sell_Shop_Temp();
			temp2.setSht_price(102d);
			list.add(temp2);
			T_Sell_Shop_Temp temp3 = new T_Sell_Shop_Temp();
			temp3.setSht_price(99d);
			list.add(temp3);
			sortList(list);
			for(T_Sell_Shop_Temp item:list){
				System.out.println(item.getSht_price());
			}*/
			
		}
		private static void mode312(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getAllMoney(list, temp.getSht_sale_code());
			double full = 0d,plusMoney = 0d;
			for(SaleForm sale:saleList){
				if(full < sale.getSsa_buy_full()){//上条促销金额小于本条买满金额，则进行再计算
					if(total_money >= sale.getSsa_buy_full()){//金额大于促销买满的金额
						full = sale.getSsa_buy_full();
						plusMoney = sale.getSsa_reduce_amount();
					}
				}
			}
			if(plusMoney > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double sale_money = NumberUtil.buildMoney(decimal, type, (sell_money/total_money)*plusMoney);
				double money = sell_money-sale_money;
				temp.setSht_remark("满"+full+"减"+plusMoney);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		private static List<T_Shop_Sale_Present> mode313(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getAllMoney(list, temp.getSht_sale_code());
			double full = 0d,addMoney = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(full < sale.getSsa_buy_full()){
					if(total_money >= sale.getSsa_buy_full()){
						full = sale.getSsa_buy_full();
						addMoney = sale.getSsa_increased_amount();
						_sale = sale;
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSsa_index()){
							present.setSsp_addMoney(addMoney);
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"元加"+addMoney+"元送商品");
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode314(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_amount = getAllAmount(list, temp.getSht_sale_code());
			double amount = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(amount < sale.getSsa_buy_number()){
					if(total_amount >= sale.getSsa_buy_number()){
						amount = sale.getSsa_buy_number();
						_sale = sale;
					}
				}
			}
			if(null != _sale && amount > 0){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_index() == _sale.getSsa_index()){
						gift.add(present);
					}
				}
				temp.setSht_remark("满"+amount+"个送商品");
			}
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode315(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getAllMoney(list, temp.getSht_sale_code());
			double full = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(full < sale.getSsa_buy_full()){
					if(total_money >= sale.getSsa_buy_full()){
						full = sale.getSsa_buy_full();
						_sale = sale;
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSsa_index()){
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"元送赠品");
			return gift;
		}
		//买满送---类别----满减
		private static void mode322(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getTypeMoney(list, temp);
			double full = 0d,plusMoney = 0d;
			for(SaleForm sale:saleList){
				if(sale.getSst_tp_code().equals(temp.getSht_tp_code())){
					if(full < sale.getSst_buy_full()){
						if(total_money >= sale.getSst_buy_full()){
							full = sale.getSst_buy_full();
							plusMoney = sale.getSst_reduce_amount();
						}
					}
				}
			}
			if(plusMoney > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double sale_money = NumberUtil.buildMoney(decimal, type, (sell_money/total_money)*plusMoney);
				double money = sell_money-sale_money;
				temp.setSht_remark("满"+full+"减"+plusMoney);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//类别----满多少加多少送商品
		private static List<T_Shop_Sale_Present> mode323(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getTypeMoney(list, temp);
			double full = 0d,addMoney = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSst_tp_code().equals(temp.getSht_tp_code())){
					if(full < sale.getSst_buy_full()){
						if(total_money >= sale.getSst_buy_full()){
							full = sale.getSst_buy_full();
							addMoney = sale.getSst_increased_amount();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSst_index()){
							present.setSsp_addMoney(addMoney);
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"加"+addMoney+"送商品");
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode324(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_amount = getTypeAmount(list, temp);
			double amount = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSst_tp_code().equals(temp.getSht_tp_code())){
					if(amount < sale.getSst_buy_number()){
						if(total_amount >= sale.getSst_buy_number()){
							amount = sale.getSst_buy_number();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_index() == _sale.getSst_index()){
						gift.add(present);
					}
				}
				temp.setSht_remark("满"+amount+"个送商品");
			}
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode325(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getTypeMoney(list, temp);
			double full = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSst_tp_code().equals(temp.getSht_tp_code())){
					if(full < sale.getSst_buy_full()){
						if(total_money >= sale.getSst_buy_full()){
							full = sale.getSst_buy_full();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSst_index()){
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"元送赠品");
			return gift;
		}
		//满减---品牌
		private static void mode332(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getBrandMoney(list, temp);
			double full = 0d,plusMoney = 0d;
			for(SaleForm sale:saleList){
				if(sale.getSsb_bd_code().equals(temp.getSht_bd_code())){
					if(full < sale.getSsb_buy_full()){
						if(total_money >= sale.getSsb_buy_full()){
							full = sale.getSsb_buy_full();
							plusMoney = sale.getSsb_reduce_amount();
						}
					}
				}
			}
			if(plusMoney > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double sale_money = NumberUtil.buildMoney(decimal, type, (sell_money/total_money)*plusMoney);
				double money = sell_money-sale_money;
				temp.setSht_remark("满"+full+"减"+plusMoney);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		//品牌---满多少加多少送商品
		private static List<T_Shop_Sale_Present> mode333(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getBrandMoney(list, temp);
			double full = 0d,addMoney = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSsb_bd_code().equals(temp.getSht_bd_code())){
					if(full < sale.getSsb_buy_full()){
						if(total_money >= sale.getSsb_buy_full()){
							full = sale.getSsb_buy_full();
							addMoney = sale.getSsb_increased_amount();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSsb_index()){
							present.setSsp_addMoney(addMoney);
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"加"+addMoney+"送商品");
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode334(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_amount = getBrandAmount(list, temp);
			double amount = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSsb_bd_code().equals(temp.getSht_bd_code())){
					if(amount < sale.getSsb_buy_number()){
						if(total_amount >= sale.getSsb_buy_number()){
							amount = sale.getSsb_buy_number();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_index() == _sale.getSsb_index()){
						gift.add(present);
					}
				}
				temp.setSht_remark("满"+amount+"个送赠品");
			}
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode335(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getBrandMoney(list, temp);
			double full = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSsb_bd_code().equals(temp.getSht_bd_code())){
					if(full < sale.getSsb_buy_full()){
						if(total_money >= sale.getSsb_buy_full()){
							full = sale.getSsb_buy_full();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSsb_index()){
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"元送赠品");
			return gift;
		}
		private static void mode342(List<T_Sell_Shop_Temp> list,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getProductMoney(list, temp);
			double full = 0d,plusMoney = 0d;
			for(SaleForm sale:saleList){
				if(sale.getSsp_pd_code().equals(temp.getSht_pd_code())){
					if(full < sale.getSsp_buy_full()){
						if(total_money >= sale.getSsp_buy_full()){
							full = sale.getSsp_buy_full();
							plusMoney = sale.getSsp_reduce_amount();
						}
					}
				}
			}
			if(plusMoney > 0){
				double sell_money = NumberUtil.buildMoney(decimal, type, temp.getSht_sell_price()*temp.getSht_amount());
				double sale_money = NumberUtil.buildMoney(decimal, type, (sell_money/total_money)*plusMoney);
				double money = sell_money-sale_money;
				temp.setSht_remark("买满"+full+"减"+plusMoney);
				updateSale(temp, money, sell_money, decimal, type);
			}else{
				delSale(decimal, type, temp);
			}
		}
		private static List<T_Shop_Sale_Present> mode343(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getProductMoney(list, temp);
			double full = 0d,addMoney = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSsp_pd_code().equals(temp.getSht_pd_code())){
					if(full < sale.getSsp_buy_full()){
						if(total_money >= sale.getSsp_buy_full()){
							full = sale.getSsp_buy_full();
							addMoney = sale.getSsp_increased_amount();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSsp_index()){
							present.setSsp_addMoney(addMoney);
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"加"+addMoney+"送商品");
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode344(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_amount = getProductAmount(list, temp);
			double amount = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSsp_pd_code().equals(temp.getSht_pd_code())){
					if(amount < sale.getSsp_buy_number()){
						if(total_amount >= sale.getSsp_buy_number()){
							amount = sale.getSsp_buy_number();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_index() == _sale.getSsp_index()){
						gift.add(present);
					}
				}
				temp.setSht_remark("满"+amount+"个送赠品");
			}
			return gift;
		}
		private static List<T_Shop_Sale_Present> mode345(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList,T_Sell_Shop_Temp temp,List<SaleForm> saleList,String decimal,String type){
			double total_money = getProductMoney(list, temp);
			double full = 0d;
			SaleForm _sale = null;
			List<T_Shop_Sale_Present> gift = new ArrayList<T_Shop_Sale_Present>();
			for(SaleForm sale:saleList){
				if(sale.getSsp_pd_code().equals(temp.getSht_pd_code())){
					if(full < sale.getSsp_buy_full()){
						if(total_money >= sale.getSsp_buy_full()){
							full = sale.getSsp_buy_full();
							_sale = sale;
						}
					}
				}
			}
			if(null != _sale){
				for(T_Shop_Sale_Present present:giftList){
					if(present.getSsp_ss_code().equals(_sale.getSs_code())){
						if(present.getSsp_index() == _sale.getSsp_index()){
							gift.add(present);
						}
					}
				}
			}
			temp.setSht_remark("满"+full+"元送赠品");
			return gift;
		}
		//更新促销的金额
		private static void updateSale(T_Sell_Shop_Temp temp,double money,double sell_money,String decimal,String type){
			delHandRate(temp);
			temp.setSht_money(money);
			temp.setSht_sale_money(sell_money-money);
			temp.setSht_price(NumberUtil.buildMoney(decimal, type, money/temp.getSht_amount()));
		}
		//属于最高级别的促销，但不满足，清除促销信息。
		private static void delSale(String decimal,String type,T_Sell_Shop_Temp temp){
			temp.setSht_price(temp.getSht_sell_price());
			temp.setSht_money((NumberUtil.buildMoney(decimal, type,temp.getSht_sell_price()*temp.getSht_amount())));
			temp.setSht_sale_money(0d);
			temp.setSht_sale_code("");
			temp.setSht_sale_model("");
			temp.setSht_remark("");
			temp.setSht_sale_send_money(0d);
		}
		private static void delHandRate(T_Sell_Shop_Temp temp){
			temp.setSht_hand_money(0d);
			temp.setSht_handtype(0);
			temp.setSht_handvalue(0d);
		}
		private static void delVipRate(T_Sell_Shop_Temp temp){
			temp.setSht_vip_money(0d);
			temp.setSht_viptype(0);
			temp.setSht_vipvalue(0d);
		}
		@SuppressWarnings("unchecked")
		private static void model(List<T_Sell_Shop_Temp> list,List<T_Shop_Sale_Present> giftList
				,String model,T_Sell_Shop_Temp temp,List<SaleForm> saleList
				,String money_decimal,String money_type,Map<String,Object> param){
			List<T_Shop_Sale_Present> allGift = null;
			if(param.containsKey("addGift")){
				allGift = (List<T_Shop_Sale_Present>)param.get("addGift");
				if(null != allGift && allGift.size() > 0){
					//删除重复的赠品，因为计算赠品是每条数据都送，所以直记最后一次的
					for(int i = allGift.size()-1;i>0;i--){
						if(allGift.get(i).getSsp_ss_code().equals(temp.getSht_sale_code())){
							allGift.remove(i);
						}
					}
				}
			}else{
				allGift = new ArrayList<T_Shop_Sale_Present>();
			}
			//全场--折扣
			if("111".equals(model)){
				mode111(temp, saleList,money_decimal,money_type);
			}
			if("112".equals(model)){
				mode112(list,temp, saleList,money_decimal,money_type);
			}
			if("113".equals(model)){
				mode113(list,temp, saleList,money_decimal,money_type);
			}
			//类别--折扣
			if("121".equals(model)){
				mode121(temp, saleList,money_decimal,money_type);
			}
			if("122".equals(model)){
				mode122(list,temp, saleList,money_decimal,money_type);
			}
			if("123".equals(model)){
				mode123(list,temp, saleList,money_decimal,money_type);
			}
			//品牌--折扣
			if("131".equals(model)){
				mode131(temp, saleList,money_decimal,money_type);
			}
			if("132".equals(model)){
				mode132(list,temp, saleList,money_decimal,money_type);
			}
			if("133".equals(model)){
				mode133(list,temp, saleList,money_decimal,money_type);
			}
			//商品折扣
			if("141".equals(model)){
				mode141(temp, saleList,money_decimal,money_type);
			}
			if("142".equals(model)){
				mode142(list,temp, saleList,money_decimal,money_type);
			}
			if("143".equals(model)){
				mode143(list,temp, saleList,money_decimal,money_type);
			}
			
			//特价--商品直接特价
			if("241".equals(model)){
				mode241(temp, saleList,money_decimal,money_type);
			}
			//特价--商品件数特价
			if("242".equals(model)){
				mode242(list,temp, saleList,money_decimal,money_type);
			}
			
			//全场--买满送
			if("311".equals(model)){//买满几件送最低价商品
				mode311(list,temp, saleList,money_decimal,money_type);
			}
			if("312".equals(model)){
				mode312(list,temp, saleList,money_decimal,money_type);
			}
			if("313".equals(model)){
				List<T_Shop_Sale_Present> gift = mode313(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("314".equals(model)){
				List<T_Shop_Sale_Present> gift = mode314(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("315".equals(model)){
				List<T_Shop_Sale_Present> gift = mode315(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			//类别--买满送
			if("322".equals(model)){
				mode322(list,temp, saleList,money_decimal,money_type);
			}
			if("323".equals(model)){
				List<T_Shop_Sale_Present> gift = mode323(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("324".equals(model)){
				List<T_Shop_Sale_Present> gift = mode324(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("325".equals(model)){
				List<T_Shop_Sale_Present> gift = mode325(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			//品牌--买满送
			if("332".equals(model)){
				mode332(list,temp, saleList,money_decimal,money_type);
			}
			if("333".equals(model)){
				List<T_Shop_Sale_Present> gift = mode333(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("334".equals(model)){
				List<T_Shop_Sale_Present> gift = mode334(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("335".equals(model)){
				List<T_Shop_Sale_Present> gift = mode335(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			//商品--买满送
			if("342".equals(model)){
				mode342(list,temp, saleList,money_decimal,money_type);
			}
			if("343".equals(model)){
				List<T_Shop_Sale_Present> gift = mode343(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("344".equals(model)){
				List<T_Shop_Sale_Present> gift = mode344(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			if("345".equals(model)){
				List<T_Shop_Sale_Present> gift = mode345(list,giftList,temp, saleList,money_decimal,money_type);
				allGift.addAll(gift);
			}
			param.put("addGift", allGift);
		}
	}
}
