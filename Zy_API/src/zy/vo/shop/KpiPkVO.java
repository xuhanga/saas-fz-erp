package zy.vo.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.shop.kpipk.T_Shop_KpiPkList;

public class KpiPkVO {
	public static List<Map<String, Object>> buildDetailJson(List<T_Shop_KpiPkList> kpiPkLists){
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		List<String> kiCodes = new ArrayList<String>();
		Map<String, List<T_Shop_KpiPkList>> detailsMap = new HashMap<String, List<T_Shop_KpiPkList>>();
		for (T_Shop_KpiPkList item : kpiPkLists) {
			if(!kiCodes.contains(item.getKpl_ki_code())){
				kiCodes.add(item.getKpl_ki_code());
			}
			if(!detailsMap.containsKey(item.getKpl_ki_code())){
				detailsMap.put(item.getKpl_ki_code(), new ArrayList<T_Shop_KpiPkList>());
			}
			detailsMap.get(item.getKpl_ki_code()).add(item);
		}
		for (String kiCode : kiCodes) {
			Map<String, Object> item = new HashMap<String, Object>();
			List<T_Shop_KpiPkList> temps = detailsMap.get(kiCode);
			item.put("kpl_ki_code", kiCode);
			item.put("ki_name", temps.get(0).getKi_name());
			for (T_Shop_KpiPkList temp : temps) {
				item.put("id"+temp.getKpl_code(), temp.getKpl_id());
				item.put("complete"+temp.getKpl_code(), temp.getKpl_complete());
				item.put("score"+temp.getKpl_code(), temp.getKpl_score());
			}
			resultList.add(item);
		}
		return resultList;
	}
}
