package zy.vo.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class PlanVO {
	public static List<Map<String, Object>> buildPreDataMonth(Map<String, Object> data){
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		for (int i = 1; i <= 12; i++) {
			Map<String, Object> item = new HashMap<String, Object>();
			item.put("id", i+"");
			item.put("month", i+"月份");
			if(data.containsKey(String.valueOf(i))){
				item.put("premoney", data.get(String.valueOf(i)).toString());
			}else {
				item.put("premoney", "0.00");
			}
			result.add(item);
		}
		return result;
	}
}
