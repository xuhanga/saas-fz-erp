package zy.vo.sort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.approve.T_Approve_Record;
import zy.entity.base.shop.T_Base_Shop;
import zy.entity.sort.settle.T_Sort_Settle;
import zy.entity.sort.settle.T_Sort_SettleList;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.util.CommonUtil;
import zy.util.PrintUtil;
import zy.util.StringUtil;
import zy.vo.common.PrintVO;

public class SortSettleVO {
	public static Map<String,Object> buildPrintJson(Map<String,Object> paramMap){
		PrintVO.List2Map(paramMap);
		return getPrintHTML(paramMap);
	}
	
	@SuppressWarnings("unchecked")
	public static String[] getHeadFieldHTML(Map<String,Object> paramMap){
		StringBuffer headHtml = new StringBuffer(""),footHtml = new StringBuffer("");
		List<T_Sys_PrintField> printFields = (List<T_Sys_PrintField>)paramMap.get("fields");
		Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
		T_Sort_Settle settle= (T_Sort_Settle)paramMap.get("settle");
		T_Base_Shop shop = (T_Base_Shop)paramMap.get("shop");
		List<T_Approve_Record> approveRecords = (List<T_Approve_Record>)paramMap.get("approveRecords");
		String end_approver = "";
		if(approveRecords != null){
			for (T_Approve_Record approveRecord : approveRecords) {
				if (CommonUtil.AR_STATE_APPROVED.equals(approveRecord.getAr_state())) {
					end_approver = approveRecord.getAr_us_name();
				}
			}
		}
		
		headHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		headHtml.append("<tbody>");
		headHtml.append("<tr>");
		headHtml.append("<td colspan=\"3\" class=\"head-title\"><b class=\"head-font\">"+printSetMap.get(PrintUtil.PrintSet.PAGE_TITLE)+"&nbsp;</b></td>");
		headHtml.append("</tr>");
		int index = -1;
		double i = 1;
		for(T_Sys_PrintField printField:printFields){
			String colspan = "";
			if(printField.getSpf_position().intValue() == PrintUtil.FALSE){
				if(index != printField.getSpf_line()){
					if(index == -1){
						headHtml.append("<tr>");
					}else{
						i ++;
						headHtml.append("</tr><tr>");
					}
				}
				if(printField.getSpf_colspan() > 1){
					colspan = "colspan='"+printField.getSpf_colspan()+"'";
				}
				
				if(PrintUtil.PrintField.MAKEDATE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_date()).append("</td>");
				}
				if(PrintUtil.PrintField.MANAGER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_manager()).append("</td>");
				}
				if(PrintUtil.PrintField.NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_number()).append("</td>");
				}
				if(PrintUtil.PrintField.SHOP_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getShop_name()).append("</td>");
				}
				if(PrintUtil.PrintField.TEL.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_tel()).append("</td>");
				}
				if(PrintUtil.PrintField.MOBILE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_mobile()).append("</td>");
				}
				if(PrintUtil.PrintField.BANK_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getBa_name()).append("</td>");
				}
				if(PrintUtil.PrintField.LINKMAN.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_man()).append("</td>");
				}
				if(PrintUtil.PrintField.DISCOUNT_MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_discount_money()).append("</td>");
				}
				if(PrintUtil.PrintField.PREPAY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_prepay()).append("</td>");
				}
				if(PrintUtil.PrintField.RECEIVED.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_received()).append("</td>");
				}
				if(PrintUtil.PrintField.RECEIVEDMORE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(settle.getSt_receivedmore()).append("</td>");
				}
				if(PrintUtil.PrintField.DEPT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSp_receivable()-shop.getSp_received()-shop.getSp_prepay()).append("</td>");
				}
				if(PrintUtil.PrintField.ADDRESS.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(shop.getSpi_addr()).append("</td>");
				}
				if(PrintUtil.PrintField.REMARK.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(StringUtil.trimString(settle.getSt_remark())).append("</td>");
				}
				index = printField.getSpf_line();
			}else if(printField.getSpf_position().intValue() == PrintUtil.TRUE){
				if(PrintUtil.PrintField.END_MAKER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(StringUtil.trimString(settle.getSt_maker())).append("</span>");
				}
				if(PrintUtil.PrintField.END_APPROVER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：").append(end_approver).append("</span>");
				}
			}
		}
		headHtml.append("</tr>");
		//标题高度
		double titleheight = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TITLE_HEIGHT));
		//表头每行高度
		double head_height = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.HEAD_HEIGHT));
		//表格数据与表头间距
		double table_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOP));
		double page_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.PAGE_TOP));
		//总高度
		double sum_height = page_top + table_top + titleheight + (head_height * i);
		headHtml.append(" </tbody>");
		headHtml.append("</table>");
		String[] html = new String[3];
		html[0] = headHtml.toString();
		html[1] = footHtml.toString();
		html[2] = sum_height + "";
		return html;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Sort_SettleList> settleList = (List<T_Sort_SettleList>)paramMap.get("settleList");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			for(T_Sys_PrintData printData:printDatas){
				html.append("<th>");
				html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
				html.append(printData.getSpd_name());
				html.append("</div>");
				html.append("</th>");
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0;
			T_Sort_SettleList totalSettle = new T_Sort_SettleList();
			totalSettle.setStl_receivable(0d);
			totalSettle.setStl_received(0d);
			totalSettle.setStl_discount_money_yet(0d);
			totalSettle.setStl_prepay_yet(0d);
			totalSettle.setStl_unreceivable(0d);
			totalSettle.setStl_discount_money(0d);
			totalSettle.setStl_prepay(0d);
			totalSettle.setStl_real_received(0d);
			html.append("<tbody>");			
			for(T_Sort_SettleList item: settleList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.NUMBER.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.NUMBER)).append(">");
						html.append(item.getStl_bill_number()+"</td>");
					}
					if(PrintUtil.PrintData.TYPE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TYPE)).append(">");
						html.append(item.getStl_type()+"</td>");
					}
					if(PrintUtil.PrintData.RECEIVABLE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RECEIVABLE)).append(">");
						html.append(item.getStl_receivable()+"</td>");
						totalSettle.setStl_receivable(totalSettle.getStl_receivable()+item.getStl_receivable());
					}
					if(PrintUtil.PrintData.RECEIVED.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.RECEIVED)).append(">");
						html.append(item.getStl_received()+"</td>");
						totalSettle.setStl_received(totalSettle.getStl_received()+item.getStl_received());
					}
					if(PrintUtil.PrintData.DISCOUNT_MONEY_YET.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.DISCOUNT_MONEY_YET)).append(">");
						html.append(item.getStl_discount_money_yet()+"</td>");
						totalSettle.setStl_discount_money_yet(totalSettle.getStl_discount_money_yet()+item.getStl_discount_money_yet());
					}
					if(PrintUtil.PrintData.PREPAY_YET.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PREPAY_YET)).append(">");
						html.append(item.getStl_prepay_yet()+"</td>");
						totalSettle.setStl_prepay_yet(totalSettle.getStl_prepay_yet()+item.getStl_prepay_yet());
					}
					if(PrintUtil.PrintData.UNRECEIVABLE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNRECEIVABLE)).append(">");
						html.append(item.getStl_unreceivable()+"</td>");
						totalSettle.setStl_unreceivable(totalSettle.getStl_unreceivable()+item.getStl_unreceivable());
					}
					if(PrintUtil.PrintData.DISCOUNT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.DISCOUNT_MONEY)).append(">");
						html.append(item.getStl_discount_money()+"</td>");
						totalSettle.setStl_discount_money(totalSettle.getStl_discount_money()+item.getStl_discount_money());
					}
					if(PrintUtil.PrintData.PREPAY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PREPAY)).append(">");
						html.append(item.getStl_prepay()+"</td>");
						totalSettle.setStl_prepay(totalSettle.getStl_prepay()+item.getStl_prepay());
					}
					if(PrintUtil.PrintData.REAL_RECEIVED.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REAL_RECEIVED)).append(">");
						html.append(item.getStl_real_received()+"</td>");
						totalSettle.setStl_real_received(totalSettle.getStl_real_received()+item.getStl_real_received());
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(StringUtil.trimString(item.getStl_remark())+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.NUMBER.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TYPE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RECEIVABLE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.RECEIVED.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.DISCOUNT_MONEY_YET.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.PREPAY_YET.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.UNRECEIVABLE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.DISCOUNT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.PREPAY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.REAL_RECEIVED.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.NUMBER.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TYPE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.RECEIVABLE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_receivable())+"</td>");
					}
					if(PrintUtil.PrintData.RECEIVED.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_received())+"</td>");
					}
					if(PrintUtil.PrintData.DISCOUNT_MONEY_YET.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_discount_money_yet())+"</td>");
					}
					if(PrintUtil.PrintData.PREPAY_YET.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_prepay_yet())+"</td>");
					}
					if(PrintUtil.PrintData.UNRECEIVABLE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_unreceivable())+"</td>");
					}
					if(PrintUtil.PrintData.DISCOUNT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_discount_money())+"</td>");
					}
					if(PrintUtil.PrintData.PREPAY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_prepay())+"</td>");
					}
					if(PrintUtil.PrintData.REAL_RECEIVED.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+String.format("%.2f", totalSettle.getStl_real_received())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = dataSize;
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
}
