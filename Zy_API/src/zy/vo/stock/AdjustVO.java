package zy.vo.stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.approve.T_Approve_Record;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.stock.T_Stock_SizeCommon;
import zy.entity.stock.adjust.T_Stock_Adjust;
import zy.entity.stock.adjust.T_Stock_AdjustList;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.PriceLimitUtil;
import zy.util.PrintUtil;
import zy.util.StringUtil;
import zy.vo.common.PrintVO;
import zy.vo.common.SizeHorizontalVO;

public class AdjustVO {
	private static List<T_Stock_SizeCommon> getSizeModeList(List<T_Stock_AdjustList> list,int maxColNumArray, Map<String, List<String>> map) {
		// 尺码横排结果
		List<T_Stock_SizeCommon> lstRes = new ArrayList<T_Stock_SizeCommon>();
		int[] subArray = null;
		T_Stock_SizeCommon temp = null;
		int Amount_Total = 0;
		double UnitMoney_Total = 0d;
		String pd_code = "";
		String cr_code = "";
		String br_code = "";
		List<String> sizeListWithSzgIdList = null;
		for (T_Stock_AdjustList item : list) {
			if (!pd_code.equals(item.getAjl_pd_code())
					|| !cr_code.equals(item.getAjl_cr_code())
					|| !br_code.equals(item.getAjl_br_code())
					) {
				pd_code = StringUtil.trimString(item.getAjl_pd_code());
				cr_code = StringUtil.trimString(item.getAjl_cr_code());
				br_code = StringUtil.trimString(item.getAjl_br_code());
				Amount_Total = 0;
				UnitMoney_Total = 0d;
				temp = new T_Stock_SizeCommon();
				subArray = new int[maxColNumArray];
				temp.setSingle_amount(subArray);
				lstRes.add(temp);
			}
			temp.setId(item.getAjl_id());
			temp.setPd_code(item.getAjl_pd_code());
			temp.setPd_no(item.getPd_no());
			temp.setPd_name(item.getPd_name());
			temp.setPd_unit(item.getPd_unit());
			temp.setBd_name(item.getBd_name());
			temp.setTp_name(item.getTp_name());
			temp.setPd_year(item.getPd_year());
			temp.setPd_season(item.getPd_season());
			temp.setSzg_code(item.getAjl_szg_code());
			temp.setCr_code(item.getAjl_cr_code());
			temp.setCr_name(item.getCr_name());
			temp.setBr_code(item.getAjl_br_code());
			temp.setBr_name(item.getBr_name());
			temp.setUnitprice(item.getAjl_unitprice());
			Amount_Total += item.getAjl_amount();
			UnitMoney_Total += item.getAjl_amount()*item.getAjl_unitprice();
			temp.setTotalamount(Amount_Total);
			temp.setUnitmoney(UnitMoney_Total);
			
			//获得尺码组编号对应的尺码编号信息
			sizeListWithSzgIdList = (List<String>)map.get(item.getAjl_szg_code());
			//然后根据对应的下标确定其位置
			int szIdIndex = sizeListWithSzgIdList.indexOf(item.getAjl_sz_code());
			//根据下标位置得到，其对应的尺码数量，然后进行累加
			subArray[szIdIndex] += item.getAjl_amount();
		}
		return lstRes;
	}
	
	public static Map<String, Object> getJsonSizeData(List<T_Base_SizeList> sizeGroupList,List<T_Stock_AdjustList> dataList){
		if (sizeGroupList != null && sizeGroupList.size() > 0) {
			int[] maxColNumArray = new int[1];
            Map<String, List<String>> map = new HashMap<String, List<String>>();
            
            List<List<String>> lstRes = SizeHorizontalVO.getSizeModeTitleInfoList(map, maxColNumArray, sizeGroupList);
            List<T_Stock_SizeCommon> list = getSizeModeList(dataList,maxColNumArray[0],map);
            int sizeMaxSize = maxColNumArray[0];
			int[] totalSizeBySingle = new int[sizeMaxSize];
			Collections.sort(lstRes, new Comparator<List<String>>() {
				@Override
				public int compare(List<String> o1, List<String> o2) {
					return o1.size() > o2.size() ? -1 : 0;
				}
			});
			int Amount_Total = 0;
			double UnitMoney_Total = 0d;
			HashMap<String, Object> sizeGroupMap = new HashMap<String, Object>();
			List<Map<String, Object>> values = new ArrayList<Map<String, Object>>();
			HashMap<String, Object> entry = null;
			if (list == null || list.isEmpty()) {
			} else {
				for (T_Stock_SizeCommon item : list) {
					entry = new HashMap<String, Object>();
					entry.put("id", item.getId());
					entry.put("pd_code", item.getPd_code());
					entry.put("pd_no", item.getPd_no());
					entry.put("pd_name", item.getPd_name());
					entry.put("pd_unit", item.getPd_unit());
					entry.put("cr_code", item.getCr_code());
					entry.put("cr_name", item.getCr_name());
					entry.put("br_code", item.getBr_code());
					entry.put("br_name", item.getBr_name());
					int[] amounts = item.getSingle_amount();
					if (lstRes == null || lstRes.size() <= 0) {
					} else {
						String[] header = null;
						int amountSize = 0;
						if (map.containsKey(item.getSzg_code())) {
							amountSize = map.get(item.getSzg_code()).size();
						}
						List<String> headers = lstRes.get(0);
						for (int i = 0; i < headers.size(); i++) {
							header = headers.get(i).split("_AND_");
							if (i >= amountSize) {
								entry.put(header[0], "");
							} else {
								entry.put(header[0],amounts[i] == 0 ? "": amounts[i] + "");
								totalSizeBySingle[i] = totalSizeBySingle[i] + amounts[i];
							}
						}
					}
					entry.put("Amount_Total", item.getTotalamount());
					entry.put("unit_price", item.getUnitprice());
					entry.put("unit_money", item.getUnitmoney());
					entry.put("sizeGroupCode", item.getSzg_code());
					Amount_Total += item.getTotalamount();
					UnitMoney_Total += item.getUnitmoney();
					values.add(entry);
				}
			}
			if (lstRes == null || lstRes.size() <= 0) {
			} else {
				String[] header = null;
                List<String> headers = lstRes.get(0);
                for (int i = 0; i < headers.size(); i++) {
                    header = headers.get(i).split("_AND_");
                    sizeGroupMap.put(header[0], totalSizeBySingle[i] == 0 ? "" : totalSizeBySingle[i]);
                }
			}
			HashMap<String, Object> userData = new HashMap<String, Object>();
			userData.put("pd_no", "合计：");
			userData.put("Amount_Total", Amount_Total);
			userData.put("unit_money", UnitMoney_Total);
			userData.putAll(sizeGroupMap);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("data", values);
			resultMap.put("userData", userData);
			return resultMap;
		}
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<T_Stock_AdjustList> convertMap2Model(Map<String, Object> data,T_Sys_User user){
		List<T_Stock_AdjustList> resultList = new ArrayList<T_Stock_AdjustList>();
		List products = (List)data.get("products");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		T_Stock_AdjustList model = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			model = new T_Stock_AdjustList();
			model.setAjl_pd_code(StringUtil.trimString(product.get("pd_code")));
			model.setAjl_cr_code(StringUtil.trimString(product.get("cr_code")));
			model.setAjl_sz_code(StringUtil.trimString(product.get("sz_code")));
			model.setAjl_szg_code(StringUtil.trimString(product.get("pd_szg_code")));
			model.setAjl_br_code(StringUtil.trimString(product.get("br_code")));
			model.setAjl_sub_code(model.getAjl_pd_code()+model.getAjl_cr_code()+model.getAjl_sz_code()+model.getAjl_br_code());
			model.setAjl_amount(Integer.parseInt(StringUtil.trimString(product.get("amount"))));
			model.setAjl_unitprice(Double.parseDouble(StringUtil.trimString(product.get("unitPrice"))));
			model.setAjl_remark("");
			model.setAjl_us_id(user.getUs_id());
			model.setCompanyid(user.getCompanyid());
			model.setOperate_type(StringUtil.trimString(product.get("operate_type")));
			resultList.add(model);
		}
		return resultList;
	}
	
	@SuppressWarnings("rawtypes")
	public static List<String[]> convertMap2Model_import(Map<String, Object> data){
		List<String[]> resultList = new ArrayList<String[]>();
		List products = (List)data.get("datas");
		if (products == null || products.size() == 0) {
			return resultList;
		}
		String[] item = null;
		for (int i = 0; i < products.size(); i++) {
			Map product = (Map)products.get(i);
			item = new String[2];
			item[0] = StringUtil.trimString(product.get("barCode"));
			item[1] = StringUtil.trimString(product.get("amount"));
			resultList.add(item);
		}
		return resultList;
	}
	
	public static Map<String,Object> buildPrintJson(Map<String,Object> paramMap,Integer printMode){
		Map<String,Object> htmlMap = null;
		PrintVO.List2Map(paramMap);
		if(printMode != null){
			if(printMode.intValue() == 0){
				htmlMap = getPrintListHTML(paramMap);
			}
			if(printMode.intValue() == 1){
				htmlMap = getPrintSizeHTML(paramMap);
			}
			if(printMode.intValue() == 2){
				htmlMap = getPrintSumHTML(paramMap);
			}
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] getHeadFieldHTML(Map<String,Object> paramMap){
		StringBuffer headHtml = new StringBuffer(""),footHtml = new StringBuffer("");
		List<T_Sys_PrintField> printFields = (List<T_Sys_PrintField>)paramMap.get("fields");
		Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
		T_Stock_Adjust adjust = (T_Stock_Adjust)paramMap.get("adjust");
		T_Approve_Record approveRecord = (T_Approve_Record)paramMap.get("approveRecord");
		T_Sys_User user = (T_Sys_User)paramMap.get("user");
		headHtml.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"head-print\">");
		headHtml.append("<tbody>");
		headHtml.append("<tr>");
		headHtml.append("<td colspan=\"3\" class=\"head-title\"><b class=\"head-font\">"+printSetMap.get(PrintUtil.PrintSet.PAGE_TITLE)+"&nbsp;</b></td>");
		headHtml.append("</tr>");
		int index = -1;
		double i = 1;
		for(T_Sys_PrintField printField:printFields){
			String colspan = "";
			if(printField.getSpf_position().intValue() == PrintUtil.FALSE){
				if(index != printField.getSpf_line()){
					if(index == -1){
						headHtml.append("<tr>");
					}else{
						i ++;
						headHtml.append("</tr><tr>");
					}
				}
				if(printField.getSpf_colspan() > 1){
					colspan = "colspan='"+printField.getSpf_colspan()+"'";
				}
				if(PrintUtil.PrintField.MAKEDATE.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(adjust.getAj_date()).append("</td>");
				}
				if(PrintUtil.PrintField.MANAGER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(adjust.getAj_manager()).append("</td>");
				}
				if(PrintUtil.PrintField.NUMBER.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(adjust.getAj_number()).append("</td>");
				}
				if(PrintUtil.PrintField.DEPOT_NAME.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(adjust.getDepot_name()).append("</td>");
				}
				if(PrintUtil.PrintField.AMOUNT.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(adjust.getAj_amount()).append("</td>");
				}
				if(PrintUtil.PrintField.MONEY.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(PriceLimitUtil.checkPriceLimit(adjust.getAj_money(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())).append("</td>");
				}
				if(PrintUtil.PrintField.REMARK.equals(printField.getSpf_code())){
					headHtml.append("<td "+colspan+">").append(printField.getSpf_name()).append(":").append(adjust.getAj_remark()).append("</td>");
				}
				index = printField.getSpf_line();
			}else if(printField.getSpf_position().intValue() == PrintUtil.TRUE){
				if(PrintUtil.PrintField.END_APPROVER.equals(printField.getSpf_code())){
					footHtml.append("<span style='float:left;width:45mm;'>").append(printField.getSpf_name()).append("：");
					if(approveRecord!=null){
						footHtml.append(approveRecord.getAr_us_name());
					}
					footHtml.append("</span>");
				}
			}
		}
		headHtml.append("</tr>");
		//标题高度
		double titleheight = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TITLE_HEIGHT));
		//表头每行高度
		double head_height = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.HEAD_HEIGHT));
		//表格数据与表头间距
		double table_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOP));
		double page_top = Double.parseDouble((String)printSetMap.get(PrintUtil.PrintSet.PAGE_TOP));
		//总高度
		double sum_height = page_top + table_top + titleheight + (head_height * i);
		headHtml.append(" </tbody>");
		headHtml.append("</table>");
		String[] html = new String[3];
		html[0] = headHtml.toString();
		html[1] = footHtml.toString();
		html[2] = sum_height + "";
		return html;
	}
	
	public static boolean IsExistBras(List<T_Stock_AdjustList> adjustLists){
		boolean flag = false;
		for(T_Stock_AdjustList item :adjustLists){
			if(StringUtil.isNotEmpty(item.getAjl_br_code())){
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintListHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Stock_AdjustList> adjustList = (List<T_Stock_AdjustList>)paramMap.get("adjustList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			boolean isHas = IsExistBras(adjustList);
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th>");
						html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
						html.append(printData.getSpd_name());
						html.append("</div>");
						html.append("</th>");
					}
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,amount = 0;
			double unitMoney = 0d;
			html.append("<tbody>");			
			for(T_Stock_AdjustList item: adjustList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.SZ_NAME)).append(">");
						html.append(item.getSz_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getAjl_amount()+"</td>");
						amount += item.getAjl_amount();
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAjl_unitprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAjl_unitmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
						unitMoney += item.getAjl_unitmoney();
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getAjl_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td></td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+amount+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(unitMoney, CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			if(isHas){
				colspan = dataSize+1;
			}else{
				colspan = dataSize;
			}
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSizeHTML(Map<String,Object> paramMap){
		StringBuffer html = new StringBuffer("");
		Map<String,Object> htmlMap = null;
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Stock_AdjustList> adjustList = (List<T_Stock_AdjustList>)paramMap.get("adjustList");
			List<T_Base_SizeList> sizeGroupList = (List<T_Base_SizeList>)paramMap.get("sizeGroupList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			int[] maxColNumArray = new int[1];
			Map<String, List<String>> sizeMap = new HashMap<String, List<String>>();
			List<List<String>> listResult = SizeHorizontalVO.getSizeModeTitleInfoList(sizeMap, maxColNumArray, sizeGroupList);
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			List<T_Stock_SizeCommon> sizeCommonList = getSizeModeList(adjustList, maxColNumArray[0], sizeMap);
			if(listResult.size()==0 || sizeCommonList.size()==0) {
				return null;
			}
			int[] totalSizeBySingle = new int[maxColNumArray[0]];
			boolean isHas = IsExistBras(adjustList);
			
			//根据商家ID和货号查询出对应的尺码组信息在title输出
		    int rowSize = listResult.size()+1;
		    html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
		    html.append("<thead>");
		    html.append("<tr>");
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())){
					if(isHas){
						html.append("<th rowspan=\""+rowSize+"\">");
						html.append(printData.getSpd_name());
						html.append("</th>");
					}
		    	}else if(PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){
		    		html.append("<th colspan=\""+maxColNumArray[0]+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}else if(!PrintUtil.PrintData.REMARK.equals(printData.getSpd_code())){
					html.append("<th rowspan=\""+(rowSize)+"\">");
					html.append(printData.getSpd_name());
					html.append("</th>");
				}
			}

		    html.append("</tr>"); 
		    if(printDataMap.containsKey(PrintUtil.PrintData.SZ_NAME)){
			    for(int i=0;i<listResult.size();i++){  
			    	html.append("<tr>");
			    	List<String> sizeList = (List<String>)listResult.get(i);
			    	for (int k = 0;k<maxColNumArray[0];k++){
			    		html.append("<th>");//入库单详细信息
			    		if (k < sizeList.size()){
			    			html.append(sizeList.get(k).split("_AND_")[1]);
			    		}else{
			    			html.append("-");
			    		}
			    		html.append("</th>");
			    	}
			    	html.append("</tr>");
			    }
		    }
		    html.append("</thead>");
		    int i = 0,amount = 0;
		    double unitMoney = 0d;
			for (T_Stock_SizeCommon item : sizeCommonList) {
				i++;
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td nowrap").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.CR_NAME)).append(">");
						html.append(item.getCr_name()+"</td>");
					}
					if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
						if (isHas){
							html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BR_NAME)).append(">");
							html.append(item.getBr_name()+"</td>");
						}
					}
					if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
						
						int[] arr = item.getSingle_amount();
						for(int k = 0;k< maxColNumArray[0];k++){	
							html.append("<td align='center'>");
							if (k<arr.length){
								html.append(arr[k] == 0 ? "" : arr[k]);//单个尺码数量
								totalSizeBySingle[k] = totalSizeBySingle[k]+ arr[k];
							}
							html.append("</td>");
						}
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getTotalamount()+"</td>");
						amount += item.getTotalamount();
					}
					
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getUnitmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getUnitmoney();
					}
				}
				html.append("</tr>");
			}
			html.append("<tfoot>");
			boolean hasMaxSize = false;
			html.append("<tr>");
			for (int j = 0; j < printDatas.size(); j++) {
				if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
					html.append("<td>总计</td>");
				}
				if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.CR_NAME.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.BR_NAME.equals(printDatas.get(j).getSpd_code())){
					if (isHas){
						html.append("<td></td>");
					}
				}
				if(PrintUtil.PrintData.SZ_NAME.equals(printDatas.get(j).getSpd_code())){
					for(int k = 0;k< maxColNumArray[0];k++){	
						hasMaxSize = true;
						html.append("<td style=\"text-align:center;\">");
						if(totalSizeBySingle[k] != 0){
							html.append(totalSizeBySingle[k]);
						}
						html.append("</td>");
					}	
					if (!hasMaxSize){
						html.append("<td>");html.append("</td>");
					} 
				}
				if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){//数量合计
					html.append("<td style=\"text-align:right;\">");
					html.append(amount+"</td>");
				}
				if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
					html.append("<td></td>");
				}
				if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
					html.append("<td style=\"text-align:right;\"><span>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</span></td>");
				}
			}
			html.append("</tr>");
			
			int dataSize = printDatas.size()-1;
			if(printDataMap.containsKey(PrintUtil.PrintData.REMARK)){
				dataSize -= 1;
			}
			int colspan = 0;
			if(isHas){
				colspan = dataSize;
			}else if(printDataMap.containsKey(PrintUtil.PrintData.BR_NAME)){
				colspan = dataSize-1;
			}else{
				colspan = dataSize;
			}
			colspan += maxColNumArray[0];
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tr>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String,Object> getPrintSumHTML(Map<String,Object> paramMap){
		Map<String,Object> htmlMap = null;
		StringBuffer html = new StringBuffer("");
		try{
			T_Sys_Print print = (T_Sys_Print)paramMap.get("print");
			List<T_Sys_PrintData> printDatas = (List<T_Sys_PrintData>)(paramMap.get("datas"));
			Map<String, Object> printSetMap = (Map<String, Object>)paramMap.get("printSetMap");
			Map<String, Object> printDataMap = (Map<String, Object>)paramMap.get("printDataMap");
			List<T_Stock_AdjustList> adjustList = (List<T_Stock_AdjustList>)paramMap.get("adjustList");
			T_Sys_User user = (T_Sys_User)paramMap.get("user");
			
			//获取表头与表尾的数据
			String[] htmlArray = getHeadFieldHTML(paramMap);
			
			html.append("<table cellpadding=\"4\" cellspacing=\"0\" class=\"data-print\">");
			html.append("<thead>");
			html.append("<tr>");
			
			int dataSize = printDatas.size();
			for(T_Sys_PrintData printData:printDatas){
				if(PrintUtil.PrintData.BR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.CR_NAME.equals(printData.getSpd_code())||
						PrintUtil.PrintData.SZ_NAME.equals(printData.getSpd_code())){//汇总模式不显示尺码杯型颜色
				}else{
					html.append("<th>");
					html.append("<div style=\"width='"+printData.getSpd_width()+"mm'\">");
					html.append(printData.getSpd_name());
					html.append("</div>");
					html.append("</th>");
				}
			}
			html.append("</tr>");
			html.append("</thead>");
			int i = 0,amount = 0;
			double unitMoney = 0d;
			html.append("<tbody>");			
			for(T_Stock_AdjustList item: adjustList){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.INDEX)).append(">");
						html.append((i+1)+"</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NO)).append(">");
						html.append(item.getPd_no()+"</td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_NAME)).append(">");
						html.append(item.getPd_name()+"</td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.BD_NAME)).append(">");
						html.append(item.getBd_name()+"</td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.TP_NAME)).append(">");
						html.append(item.getTp_name()+"</td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_SEASON)).append(">");
						html.append(item.getPd_season()+"</td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_YEAR)).append(">");
						html.append(item.getPd_year()+"</td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.PD_UNIT)).append(">");
						html.append(item.getPd_unit()+"</td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.AMOUNT)).append(">");
						html.append(item.getAjl_amount()+"</td>");
						amount += item.getAjl_amount();
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_PRICE)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAjl_unitprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.UNIT_MONEY)).append(">");
						html.append(PriceLimitUtil.checkPriceLimit(item.getAjl_unitmoney(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit()));
						html.append("</td>");
						unitMoney += item.getAjl_unitmoney();
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td").append(PrintVO.getFieldStyleValue(printDataMap, PrintUtil.PrintData.REMARK)).append(">");
						html.append(item.getAjl_remark()+"</td>");
					}
				}
				html.append("</tr>");
				i++;
			}
			html.append("</tbody>");
			String subtotal = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_SUBTOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(subtotal)){
				html.append("<tfoot>");
				//列表模式下当前页合计算输出
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>小计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;' tdata='subSum' format='#'>###</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						if(PriceLimitUtil.hasPriceLimit(CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())){
							html.append("<td style='text-align:right;' tdata='subSum' format='#,##0.00'>###</td>");
						}else {
							html.append("<td style='text-align:right;'>***</td>");
						}
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			
			String total = (String)printSetMap.get(PrintUtil.PrintSet.TABLE_TOTAL);
			if(String.valueOf(PrintUtil.TRUE).equals(total)){
				html.append("<tr>");
				for (int j = 0; j < printDatas.size(); j++) {
					if(PrintUtil.PrintData.INDEX.equals(printDatas.get(j).getSpd_code())){
						html.append("<td>总计</td>");
					}
					if(PrintUtil.PrintData.PD_NO.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.BD_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.TP_NAME.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_SEASON.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_YEAR.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.PD_UNIT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.AMOUNT.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+amount+"</td>");
					}
					if(PrintUtil.PrintData.UNIT_PRICE.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
					if(PrintUtil.PrintData.UNIT_MONEY.equals(printDatas.get(j).getSpd_code())){
						html.append("<td style='text-align:right;'>"+PriceLimitUtil.checkPriceLimit(unitMoney,CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");
					}
					if(PrintUtil.PrintData.REMARK.equals(printDatas.get(j).getSpd_code())){
						html.append("<td></td>");
					}
				}
				html.append("</tr>");
			}
			html.append("<tr>");
			int colspan = 0;
			colspan = dataSize-2;
			html.append("<td colspan='"+colspan+"'>");
			html.append(htmlArray[1]);
			html.append("<div style='float:right;'>");
			html.append("<span  tdata='pageNO' format=\"#\">第#页</span>");
			html.append("<b>/</b>");
			html.append("<span tdata='pageCount' format=\"#\">总#页</span>");
			html.append("</div>");
			String end_endshow = (String)printSetMap.get(PrintUtil.PrintSet.END_ENDSHOW);
			if(String.valueOf(PrintUtil.FALSE).equals(end_endshow)){
				if(StringUtil.isNotEmpty(print.getSp_remark())){
					html.append("<br/><div style='float:left;'>");
	                html.append(StringUtil.trimString(print.getSp_remark()));
	                html.append("</div>");
				}
            }
			html.append("</td>");
			html.append("</tfoot>");
			html.append("</table>");
			htmlMap = new HashMap<String, Object>(27);
			htmlMap.put("listHtml", html.toString());
			htmlMap.put("headHtml", htmlArray[0]);
			htmlMap.put("styleHtml", PrintVO.getPrintStyleHTML(paramMap));
			htmlMap.put("totalHeadHeight", htmlArray[2]);
			htmlMap.putAll(printSetMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return htmlMap;
	}
	
}
