package zy.vo.stock;

import java.util.List;

import zy.entity.stock.price.T_Stock_Price;
import zy.entity.stock.price.T_Stock_PriceList;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.PriceLimitUtil;
import zy.util.StringUtil;

public class StockPriceVO {
	public static String getPrintHeaderHtml(T_Stock_Price bill){
		StringBuffer buf = new StringBuffer("");
		buf.append("<table id='printTitle' border='0' cellspacing='0' cellpadding='0' width='100%' style='font-size:11pt;'>");
		buf.append("<tr><td colspan='3' style='height:6mm;' align='center'><b style='font-size:13pt;'>成本调价单</b></td></tr>");
		buf.append("<tr><td style='width:33%;height:6mm;'>制单日期："+bill.getPc_date()+"</td>");
		buf.append("<td style='width:33%;height:6mm;'>经&nbsp;&nbsp;办&nbsp;&nbsp;人："+StringUtil.trimString(bill.getPc_manager())+"</td>");
		buf.append("<td style='width:33%;height:6mm;'>单据编号："+bill.getPc_number()+"</td>");
		buf.append("</tr>");
		buf.append("<tr><td colspan='3' style='height:6mm;'>摘要信息："+StringUtil.trimString(bill.getPc_remark())+"</td></tr>");
		buf.append("</table>");//主表结束
		return buf.toString();
	}
	
	public static String getPrintListHTML(List<T_Stock_PriceList> details,T_Sys_User user){
		StringBuffer html = new StringBuffer("");
		int rowIndex=0;
		html.append("<table id='printData' cellpadding=\"0\" cellspacing=\"0\" width='100%'>");
		html.append("<thead>");
		html.append("<tr id='printTrTable'>");
		html.append("<td style='width:5%;'><b>");html.append("序号");html.append("</b></td>");
		html.append("<td style='width:10%;'><b>");html.append("商品货号");html.append("</b></td>");
		html.append("<td style='width:15%;'><b>");html.append("商品名称");html.append("</b></td>");
		html.append("<td style='width:6%;'><b>");html.append("单位");html.append("</b></td>");
		html.append("<td style='width:15%;'><b>");html.append("调整前单价");html.append("</b></td>");
		html.append("<td style='width:15%;'><b>");html.append("调整后单价");html.append("</b></td>");
		html.append("</tr>");
		html.append("</thead>");
		for(T_Stock_PriceList item :details){
			rowIndex++;
			html.append("<tr>");
			html.append("<td align='center'>"+rowIndex+"</td>");//序号
			html.append("<td align='left' >"+item.getPd_no()+"</td>");//商品货号
			html.append("<td align='left' >"+item.getPd_name()+"</td>");//商品名称				
			html.append("<td align='center' >"+item.getPd_unit()+"</td>");//单位
			html.append("<td align='right' >"+PriceLimitUtil.checkPriceLimit(item.getPcl_oldprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");//调整后单价
			
			html.append("<td align='right' >"+PriceLimitUtil.checkPriceLimit(item.getPcl_newprice(),CommonUtil.PRICE_LIMIT_COST, user.getUs_limit())+"</td>");//调整后单价
			html.append("</tr>");
		}
		html.append("<tr style='background-color:#FFFFFF;'>");
		html.append("<tfoot>");
		html.append("<tr>");
		html.append("<td colspan='6' style='text-align: right;border-left: 0px;'>");
		html.append("<span tdata='pageNO' format='#'>第#页</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		html.append("<span tdata='pageCount' format='#' align='center'>总#页</span>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</tfoot>");
		html.append("</table>");
		return html.toString();
	}
}
