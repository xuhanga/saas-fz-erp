package zy.vo.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zy.entity.stock.useable.T_Stock_Useable;
import zy.util.StringUtil;

public class UseableVO {
	public static List<T_Stock_Useable> convertMap2Model(Map<String, Object> data,Integer companyid){
		List<T_Stock_Useable> resultList = new ArrayList<T_Stock_Useable>();
		List datas = (List)data.get("datas");
		if (datas == null || datas.size() == 0) {
			return resultList;
		}
		T_Stock_Useable model = null;
		for (int i = 0; i < datas.size(); i++) {
			Map item = (Map)datas.get(i);
			model = new T_Stock_Useable();
			if(StringUtil.isNotEmpty(item.get("ua_id"))){
				model.setUa_id(Integer.parseInt(item.get("ua_id").toString()));
			}
			model.setUa_code(StringUtil.trimString(item.get("ua_code")));
			model.setUa_join(Integer.parseInt(StringUtil.trimString(item.get("ua_join"))));
			model.setCompanyid(companyid);
			resultList.add(model);
		}
		return resultList;
	}
}
