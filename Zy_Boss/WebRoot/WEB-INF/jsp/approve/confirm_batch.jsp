<%@ page contentType="text/html; charset=gbk" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style>
body{font-size:12px;margin:0;}
.box1{width:280px;height:240px;overflow:hidden;margin:0px 10px;}
.box1 dl{display:inline;float:left;line-height:25px;width:280px;margin:10px 0 0;font-family:Microsoft YaHei;}
.sp_area{border:#ddd solid 1px;resize:none;height:80px;padding:5px;width:268px;font-family:Microsoft YaHei;}
.box1 dl.spsub{text-align:right;}
.box1 dl.spsub span{float:left;color:#888;}
.box1 dl.spsub span a{color:#f60;}
.bottons{border:none;color:#fff;height:30px;border-radius:3px;width:60px;cursor:pointer;font-family:Microsoft YaHei;}
.c1{background:#08d;}
.c2{background:#eee;color:#666;border:#ddd solid 1px;}
</style>

<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script>
<script>
	var api = frameElement.api, W = api.opener;
	var ar_infos;
    //保存审批信息
	function save(){
		var ar_state = $("#ar_state").val();
		var ar_describe = $("#ar_describe").val();
		if(ar_describe == '审批意见'){
			ar_describe = '';
		}
		if(ar_state == 2 && ar_describe == ''){
			W.$.dialog.tips("请填写审批意见",1,"32X32/hits.png",function(){
				$("#ar_describe").focus();
			});
			return;
		}
		ar_infos = {};
		ar_infos.ar_state = ar_state;
		ar_infos.ar_describe = ar_describe;
		doClose();
	}
	
	function fontMaxLength(obj){
		var info = obj.value;
		var len = info.length;
		document.getElementById("info_size").innerText=len;
		if( len > 60){
			obj.value=info.substring(0,60);
			document.getElementById("info_size").innerText=60;
		}
	}
	
	function doClose(){
		api.close();
	}
	$(function(){
		$("#tip").text(api.data.tip);
	});
</script>
</head>
<body>
<div class="box1">
<form name="form1" method="post" action="" id="form1">
	<dl>
		<span id="tip" style="color:red;"></span>
	</dl>
	<dl id="state">
		<label class="radio" style="width:30px">
	 			<input name="redio1" type="radio" value="1" checked="checked"/>同意
 	 	</label>
 	 	<label class="radio" style="width:30px">
 	 		<input name="redio1" type="radio" value="2" />不同意
 	 	</label>
		<input type="hidden" id="ar_state" name="ar_state" value="1"/>
	</dl>
    <dl>
    	<textarea class="sp_area" name="ar_describe" id="ar_describe" style="color:#cccccc;"
    			onblur="if(this.value == ''){this.style.color = '#cccccc'; this.value = '审批意见'; }" 
    			onfocus="if(this.value == '审批意见'){this.value =''; this.style.color = '#666666'; }" 
    			onkeydown="javascript:fontMaxLength(this);" 
    			onkeyup="javascript:fontMaxLength(this);">审批意见</textarea>
    </dl>
    <dl class="spsub">
	    <span>字数长度：<a id="info_size">0</a>/60</span>
	    <input type="button" id="submit" class="bottons c1" value="提交" onclick="javascript:return save();"/>&nbsp;
	    <input type="button" class="bottons c2" id="close" value="关闭" onclick="javascript:doClose();"/>
    </dl>
</form>
<script>
$(function(){
	$_state = $("#state").cssRadio({ callback: function($_obj){
		$("#ar_state").val($_obj.find("input").val());
	}});
});
</script>
</div>
</body>
</html>