<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ar_id" name="ar_id" value="${area.ar_id}"/>
<table class="pad_t20" width="100%" style="padding:10px;">
	<tr style="line-height:45px; ">
		<td align="right" >编号：</td>
		<td align="left">
		 	<input type="text" readonly="readonly" class="main_Input w146" id="ar_code" name="ar_code" value="${area.ar_code}"/>
		</td>
	</tr>
	<tr>
		<td align="right" >名称：</td>
		<td align="left">
		 	<input type="text" class="main_Input w146" id="ar_name" name="ar_name" value="${area.ar_name}"/>
		</td>
	</tr>
</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/area/area_update.js"></script>
</body>
</html>