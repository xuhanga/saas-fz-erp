<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD xhtml 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/dtd/xhtml1-Strict.dtd"/>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>颜色批量导入</title>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/leadingIn/leadingIn.css"/>
</head>
<body>
	<input type="hidden" id="isHid" value=""/>
	<div class="wrapper">
		<div style="width:480px;" class="mod-inner">
	      <h3>批量导入颜色</h3>
	      <ul id="import-steps" class="mod-steps">
	          <li><span id="span1" class="current">1.下载模版</span>&nbsp;&nbsp;&gt;</li>
	          <li><span id="span2">2.导入Excel</span>&nbsp;&nbsp;&gt;</li>
	          <li><span id="span3">3.导入完毕</span></li>
	      </ul>
	      <div class="cf" id="import-wrap">
	          <div class="step-item" id="import-step1">
	              <div class="ctn">
	                  <h3 class="tit">温馨提示：</h3>
	                  <p>导入模板的格式不能修改，录入方法请参考导入说明模板。</p>
	              </div>
	              <p><a class="link" href="<%=basePath%>resources/excel/import_color_temp.xls">下载导入数据模版</a></p>
	              <p><a class="link" href="<%=basePath%>resources/excel/import_color_explain.xls">下载导入说明模版</a></p>
	              <p><a class="link" id="resultInfo">查看导入信息</a></p>
	              <div class="step-btns">
	                  <a id="step1_next" rel="step2" class="ui-btn ui-btn-sp">下一步</a>
	              </div>
	          </div>
	
	          <div style="display:none;" class="step-item" id="import-step2">
	              <div class="ctn file-import-ctn"> 
	                  <form method="post" name="uploadFile" id="uploadFile" enctype="multipart/form-data">
	                  <span class="tit">请选择要导入文件：</span>
	                 	<input style="height:24px;" id="excel" name="excel" type="file">
	                  </form>
	              </div>
	              <div class="step-btns">
	                  <a id="step2_previous" rel="step1" class="ui-btn mrb">上一步</a>
	                  <a id="step2_import" class="ui-btn ui-btn-sp">导入</a>
	              </div>					
	          </div>
	
	          <div style="display: none;" class="step-item" id="import-step3">
	              <div id="import-result" class="ctn file-import-ctn">
              		文件已上传，正在导入数据，请稍后
              		<a class="link" id="resultInfo2" style="display: none;">查看导入记录</a>
	              </div>
	              <div class="step-btns">
	                  <a id="step3_previous" class="ui-btn mrb" style="display: none;">上一步</a>
	                  <a id="step3_finish" class="ui-btn ui-btn-sp" style="display: none;">完成</a>
	              </div>
	          </div>
	      </div>
		</div>
	</div>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>"); 
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/spin.min.js\"></sc"+"ript>");
</script>
<script type="text/javascript">
	var api = frameElement.api,W = api.opener;
	var opts = {
	  lines: 9, // The number of lines to draw
	  length: 7, // The length of each line
	  width: 2, // The line thickness
	  radius: 0, // The radius of the inner circle
	  corners: 0, // Corner roundness (0..1)
	  rotate: 0, // The rotation offset
	  direction: 1, // 1: clockwise, -1: counterclockwise
	  color: '#000', // #rgb or #rrggbb or array of colors
	  speed: 0.9, // Rounds per second
	  trail: 35, // Afterglow percentage
	  shadow: false, // Whether to render a shadow
	  hwaccel: false, // Whether to use hardware acceleration
	  className: 'spinner', // The CSS class to assign to the spinner
	  zIndex: 2e9, // The z-index (defaults to 2000000000)
	  top: '3', // Top position relative to parent in px
	  left: 'auto' // Left position relative to parent in px
	};
	var spinner = new Spinner(opts);
	$(function(){
		$("#step1_next").click(function(){
			$("#import-step1").hide();
			$("#import-step2").show();
			$("#import-steps li span.current").removeClass("current");
			$("#span li span.current").removeClass("current");
			$("#span2").addClass("current");
		});
		
		$("#step2_previous").click(function(){
			$("#import-step2").hide();
			$("#import-step1").show();
			$("#import-steps li span.current").removeClass("current");
			$("#span1").addClass("current");
		});
		
		$("#step2_import").click(function(){
		    var str = $("#excel").val();
		    if (!str) {
		    	W.Public.tips({type: 2, content : "请选择Excel文件！"});
		        return false;
		    }
		    var strRegex = "(.xls|.XLS)$";
		    var re = new RegExp(strRegex);
		    if (!re.test(str)){
		    	W.Public.tips({type: 2, content : "只能导入.xls类型的Excel文件！"});
		       	return false;
		    } else {
		    	var options = {
		        	url:"<%=basePath%>base/color/save_import_color",
		        	type:"post",
		        	timeout:40000,
					dataType:"json",
			        success:function(data){
			        	$("#isHid").val("");
			        	resetFileInput();// 清空
			        	spinner.stop();
			        	if(undefined != data && data.stat == 200){
							W.Public.tips({type: 1, content : data.message});
							$("#step3_previous").show();
				        	$("#resultInfo2").show();
				        	$("#step3_finish").show();
						}else{
							$("#isHid").val("1");
							$("#import-result").empty();
							$("#import-result").append("<font color='red'>导入失败,错误信息:</font><br />");
		        			$("#import-result").append(data.message);
						}
				    }
		    	};
		    	$("#uploadFile").ajaxSubmit(options);
		    }
		    $("#import-step2").hide();
			$("#import-step3").show();
		    var target = document.getElementById("import-result");
		    spinner.spin(target);
		    setTimeout(stopUpload, 30000);
		});
		
		$("#step3_previous").click(function(){
			$(this).hide();
			$("#step3_finish").hide();
			$("#resultInfo2").hide();
			$("#import-step3").hide();
			$("#import-step2").show();
		});
		
		// 清空file的值
		function resetFileInput(){
			var file = $("#excel");
	   		file.after(file.clone().val(""));
	   		file.remove();
		}
		
		$("#resultInfo").click(function(){
			W.$.dialog({ 
			    //id设置用来区别唯一性 防止div重复
			   	id:'leadingInResult',
			   	//设置标题
			   	title:'导入结果',
			   	//设置最大化最小化按钮(false：隐藏 true:关闭)
			   	max: false,
			   	min: false,
			   	lock:false,
			   	//设置长宽
			   	width:400,
			   	height:310,
			   	fixed:false,
			   	//设置是否拖拽(false:禁止，true可以移动)
			   	drag: true,
			   	resize:false,
			   	//设置页面加载处理
			   	content:'url:<%=basePath%>sys/importinfo/to_import_result?ii_type=color'
		    });
		});
		
		$("#resultInfo2").click(function(){
			W.$.dialog({ 
			    //id设置用来区别唯一性 防止div重复
			   	id:'leadingInResult',
			   	//设置标题
			   	title:'导入结果',
			   	//设置最大化最小化按钮(false：隐藏 true:关闭)
			   	max: false,
			   	min: false,
			   	lock:false,
			   	//设置长宽
			   	width:400,
			   	height:310,
			   	fixed:false,
			   	//设置是否拖拽(false:禁止，true可以移动)
			   	drag: true,
			   	resize:false,
			   	//设置页面加载处理
			   	content:'url:<%=basePath%>sys/importinfo/to_import_result?ii_type=color'
		    });
		});
		
		$("#step3_finish").click(function(){
			api.close();
		});
		
		function stopUpload(){
			var isHid = document.getElementById("isHid").value;
			if(isHid == ""){
				spinner.stop();
				$("#step3_previous").show();
				$("#resultInfo2").show();
				$("#step3_finish").show();
				document.getElementById("isHid").value = "1";
			}
		}
	})
</script>
</body>
</html>