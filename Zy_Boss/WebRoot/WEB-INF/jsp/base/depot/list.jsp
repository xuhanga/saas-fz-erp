<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD xhtml 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/dtd/xhtml1-Strict.dtd"/>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
	<div class="mod-search cf">
	    <div class="fl">
	      <div id="filter-menu" class="ui-btn-menu fl">
		    		 <span style="float:left" class="ui-btn menu-btn">
			     		 <strong>条件：</strong>
			     		 <input type="text" class="main_Input w146" id="SearchContent" name="SearchContent" value="" />
						 <b></b>
				  	 </span>
			 		<div class="con short" >
						<ul class="ul-inline">
							<li>
								<table class="searchbar">
									 <tr>
										  <td align="right">店铺：</td>
										  <td>
										  		<span class="ui-combo-wrap" id="spanShop"></span>
												<input type="hidden" name="dp_shop_code" id="dp_shop_code" value=""/>
								  		  </td>
									 </tr>
								</table>
							</li>
							<li style="float:right;margin-top: 5px">
								<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
			       				<a class="ui-btn mrb" id="btn_reset">重置</a>
							</li>
						</ul>
					</div>
		</div>
    </div>
    <div class="fl-m">
    	<a href="javascript:void(0);" class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    <a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" name="btn-add">新增</a>
	    <a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-default" name="btn-default">设主仓库</a>
    </div>
</div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/base/depot/depot_list.js"></script>
</body>
</html>