<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form method="post" action="" id="form1" name="form1">
	<input type="hidden" id="dp_id" name="dp_id" value="${model.dp_id}"/>
	<table class="table_add">
		<tr>
			<td width="60px" align="right"><font color="red">*</font>名称：
			</td>
			<td align="left" width="180px">
				<input class="main_Input w146" type="text" name="dp_name" id="dp_name" value="${model.dp_name}" maxlength="20"/>
			</td>
			<td align="right" width="60px">联系人：
			</td >
			<td align="left" width="180px">
				<input class="main_Input w146" type="text" name="dp_man" id="dp_man" maxlength="16" value="${model.dp_man}"/>
			</td>
		</tr>
	    <tr>
	     	<td align="right"><b style="color:red;">*</b>店铺：</td>
			<td>
				<span class="ui-combo-wrap" id="spanShop"></span>
				<input type="hidden" name="dp_shop_code" id="dp_shop_code" value="${model.dp_shop_code}"/>
			</td>  	
			<td align="right">手机：
			</td>
			<td>
				<input class="main_Input w146" type="text" name="dp_mobile"  id="dp_mobile" value="${model.dp_mobile}" maxlength="11" />
			</td>
		</tr>
		<tr>
			<td align="right">电话： 
		    </td>
			<td>
		    	<input class="main_Input w146" type="text" name="dp_tel" id="dp_tel" maxlength="15" value="${model.dp_tel}"/>
		    </td>
		    <td align="right">状态： 
		    </td>
			<td>
				<span class="ui-combo-wrap" id="spanState"></span>
				<input type="hidden" name="dp_state" id="dp_state" value="${model.dp_state}"/>
		    </td>
		</tr>
		<tr>
			<td align="right">地址：
			</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="dp_addr" id="dp_addr" value="${model.dp_addr}" style="width:395px"/>
			</td>
		</tr>
		<tr>
			<td align="right">备注：
			</td>
		  	<td colspan="3">
				<input class="main_Input" type="text" name="dp_remark" id="dp_remark" value="${model.dp_remark}" style="width:395px"/>
			</td>
		</tr>
	</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/depot/depot_update.js"></script>
</body>
</html>