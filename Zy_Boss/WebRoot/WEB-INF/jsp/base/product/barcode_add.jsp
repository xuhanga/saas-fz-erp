<%@page import="zy.util.StringUtil"%>
<%@page import="zy.util.CommonUtil"%>
<%@page import="zy.entity.sys.set.T_Sys_Set"%>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
</script> 
<style>
.opselect{border:#ddd solid 1px;height:25px;line-height:25px;padding:0 0 0 5px;}
</style>
</head>
<body >
<%
	T_Sys_Set set = (T_Sys_Set)request.getSession().getAttribute(CommonUtil.KEY_SYSSET);
%>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="ST_SubCodeRule" id="ST_SubCodeRule" value="<%=StringUtil.trimString(set.getSt_subcode_rule())%>"/><!-- 条形码生成规则明细 -->
<input type="hidden" name="ST_SubCodeIsRule" id="ST_SubCodeIsRule" value="<%=StringUtil.trimString(set.getSt_subcode_isrule())%>"/><!-- 条形码生成规则 0默认 1自己配置 -->

<input type="hidden" id="pd_code" name="pd_code" value="${product.pd_code}"/>
<input type="hidden" id="pd_szg_code" name="pd_szg_code" value="${product.pd_szg_code}"/>
<input type="hidden" id="pd_bra" name="pd_bra" value="${product.pd_bra}"/>
<div class="mainwra">
	<table width="100%">
		<tr class="list">
			<td colspan="6">
				<font style="color: red;font-size: 8pt;">条形码生成规则：<%=StringUtil.trimString(set.getSt_subcode_rule())%></font>
			</td>
		</tr>
		<tr class="list">
			<td colspan="6">
				批量打印数量：<input type="text" id="changeNum" name="changeNum" class="main_Input" value="" style="width:80px;" onkeyup="javascript:onKeyUpChangeNum(this);"/>
		    	<input type="button" class="ui-btn" value="打 印" id="btn-print" onclick="javascript:void(0);"/>
		    	<input type="button" class="ui-btn" value="打印预览" id="btn-print-preview" onclick="javascript:void(0);"/>
		    	<input type="button" class="ui-btn" value="打印设置" id="btn-printSite" onclick="javascript:void(0);"/>
		    	<input type="button" class="ui-btn ui-btn-sp" value="按规则一键批量增加" id="saveBar" onclick="javascript:allSaveBar();"/>
			</td>
		</tr>
		<tr class="list">
			<td style="width: 120px;">商品货号：<span id="span_pd_no">${product.pd_no}</span><input type="hidden" id="pd_no" name="pd_no" value="${product.pd_no}"/></td>
			<td style="width: 140px;">商品名称：<span id="span_pd_name">${product.pd_name}</span><input type="hidden" id="pd_name" name="pd_name" value=">${product.pd_name}"/></td>
			<td colspan="4">
				颜色：
				<span class="ui-combo-wrap" id="span_color"></span>
				<input type="hidden" name="cr_code" id="cr_code" value=""/>
			
				尺码：
				<span class="ui-combo-wrap" id="span_size"></span>
				<input type="hidden" name="sz_code" id="sz_code" value=""/>
				<c:if test="${product.pd_bra == '1'}">
				杯型：
				<span class="ui-combo-wrap" id="span_bra"></span>
				</c:if>
				<input type="hidden" name="br_code" id="br_code" value=""/>
			</td>
		</tr>
		<tr class="list last">
			<td>商品品牌：<span id="span_pd_bd_name">${product.pd_bd_name}</span></td>
			<td>商品类别：<span id="span_pd_tp_name">${product.pd_tp_name}</span></td>
			<td colspan="4">
				系统条形码：
				<label id="label_barcode"></label>
				<input type="hidden" id="bc_sys_barcode" name="bc_sys_barcode" value=""/>
				<input type="hidden" id="bc_subcode" name="bc_subcode" value=""/>
				<input type="button" class="ui-btn" value="复制" id="copyBtn" onclick="javascript:handle.copyBarcode();"/>
			
				自建条形码：<input class="main_Input" type="text" id="bc_barcode" name="bc_barcode" style="width:80px;" maxlength="28" onkeyup="javascript:if(event.keyCode==13){handle.oneSaveBarcode();}else{value=value.replace(/[^\w\.\-\/]/ig,'')};"/>
				<input type="button" class="ui-btn ui-btn-sp" value="添加" id="saveBar" onclick="javascript:handle.oneSaveBarcode();"/>
				<input type="text" style="display: none;"/>
			</td>
		</tr>
	</table>
	<div class="grid-wrap">
	   <div id="listMode">
	       <table id="grid"></table>
	       <div id="page"></div>
	   </div>
	 </div>
</div>
</form>
</body>
<script src="<%=basePath%>data/base/product/product_barcode_add.js"></script>
<script src="<%=basePath%>resources/util/forbidBackSpace.js"></script>
<object id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> 
	<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0 pluginspage="install_lodop32.exe"></embed>
</object>
<script language="javascript" src="<%=basePath%>resources/util/barcode_print.js"></script>
<script language="javascript" src="<%=basePath%>resources/util/LodopFuncs.js"></script>
</html>