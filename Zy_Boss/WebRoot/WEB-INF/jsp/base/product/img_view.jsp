<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/AutoResizeImg.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/slider.js\"></sc"+"ript>");
</script> 
</head>
<body style="margin:0;padding:0;">
<div id="pdImgShow" class="flexslider">
	<ul class="slides">
	<c:if test="${fn:length(imgs) == 0}">
		<li><img src="<%=basePath %>resources/grid/images/nophoto.png" onload="AutoResizeImage(380,300,this);" /></li>
	</c:if>
	<c:if test="${fn:length(imgs) > 0}">
		<c:forEach var="img" items="${imgs}" varStatus="as">
			<li><img src="<%=serverPath%>${img.pdm_img_path}" onload="AutoResizeImage(380,300,this);" /></li>
		</c:forEach>
	</c:if>
	</ul>
</div>
</body>
<script type="text/javascript">
$(function(){
	$('#pdImgShow').flexslider({
		animation: "slide",
		direction:"horizontal",
		easing:"swing"
	});
});
</script>
</html>
