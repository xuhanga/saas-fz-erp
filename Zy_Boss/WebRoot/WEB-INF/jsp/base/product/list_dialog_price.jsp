<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<input type="hidden" id="sp_shop_code" value="${sp_shop_code }"/>
<input type="hidden" id="discount" value="${discount }"/>
<input type="hidden" id="sp_shop_type" value="${sp_shop_type }"/>
<input type="hidden" id="pd_no" value="${pd_no }"/>
<div class="mainwra">
	<div class="mod-search cf">
		<div class="fl">
	      <ul class="ul-inline">
	        <li>
	          <input type="text" id="SearchContent" name="SearchContent" onkeyup="javascript:if(event.keyCode==13){$('#btn-search').click()};"
	          value= "${pd_no }" title="支持货号/名称/拼音简码模糊查询" class="ui-input ui-input-ph matchCon"/>
	          <input type="text" style="display:none" />       
	        </li>
	        <li>&nbsp;
	        	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	        </li>
	      </ul>
	    </div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
<script src="<%=basePath%>data/base/product/product_list_dialog_price.js"></script>
</body>
</html>