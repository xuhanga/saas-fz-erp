<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<c:if test="${shop_type != 4}">
<input type="hidden" name="sp_shop_type" id="sp_shop_type" value="3"/>
</c:if>
<c:if test="${shop_type == 4}">
<input type="hidden" name="sp_shop_type" id="sp_shop_type" value="4"/>
</c:if>
<div class="wrapper">
<div class="mod-search cf">
	    <div class="fl">
	      <ul class="ul-inline">
	        <li>
	          <input type="text" id="SearchContent" name="SearchContent" class="ui-input ui-input-ph matchCon" value="" style="width: 180px;"/>
	        </li>
	        <li>&nbsp;
	        	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	        </li>
	        <li>
	        	<c:if test="${shop_type != 4}">
				<input type="radio" onclick="radio_click(this);" id="zyd" name="priceflag" value="3" checked="checked"/> 自营店
				<input type="radio" onclick="radio_click(this);" id="hhd" name="priceflag" value="5" /> 合伙店
				<input type="radio" onclick="radio_click(this);" id="jmd" name="priceflag" value="4" /> 加盟店&nbsp;
				</c:if>
				<c:if test="${shop_type == 4}">
				<input type="radio" onclick="radio_click(this);" id="jmd" name="priceflag" value="4" checked="checked"/> 加盟店&nbsp;
				</c:if>
            </li>
	      </ul>
	    </div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
<script src="<%=basePath%>data/base/shop/shop_list_price_dialog.js"></script>
</body>
</html>