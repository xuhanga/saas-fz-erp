<%@ page contentType="text/html; charset=gbk" language="java" %>
<%@ include file="/common/path.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>商品分类</title>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/ztree/ztree/ztree.css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/base.css"/>
<style>
.treemenu{margin-top:1px;margin-left:5px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.3.2.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ztree/jquery.ztree.core-3.4.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ztree/jquery.ztree.excheck-3.4.min.js\"></sc"+"ript>");
</script>
<script>
var selectedTreeNode;
var setting = {
	check: {
		enable: false,
		chkboxType: { "Y" : "s", "N" : "s" }
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback:{
		onClick:function(event, treeId, treeNode, clickFlag){
			var id = treeNode.id;
			selectedTreeNode = treeNode;
			var data = {};
			data.shop_type=id;
			window.parent.frames["workFrame"].THISPAGE.reloadData(data);
		}
	}
};
$(function(){
	var config = parent.parent.CONFIG;
	var shop_types = config.SHOP_TYPES;
	var zNodes =[{id:'',name:'所有门店',open:true,children:shop_types}];
	$.fn.zTree.init($("#role-tree"), setting, zNodes);
});
</script>
</head>  
<body>
	<div class="treemenu" style="overflow-y:auto;">
		<div id="roleTree" class="divlistContent" >
			<ul id="role-tree" class="ztree"></ul>
		</div>
	</div>
</body>
</html>