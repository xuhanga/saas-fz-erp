<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/change.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<table class="pad_t15" style="margin:10px;">
	<tr>
		<td align="left" colspan="3">名称:<input class="main_Input" style="width:260px;" type="text" name="szg_name" id="szg_name" value=""/></td>
	</tr>
	<tr>
		<td valign="top">
			<div>查询:<input type="text" name="sz_name" id="sz_name" title="可根据尺码名称或拼音简码查询颜色" style="margin:5px; width:76px;border:1px #c6e7f6 solid;"/></div>
			<select id="left_item" dir="ltr" name="left_item" size="25" style="align:center;padding:5px; width:112px;height:220px; border:1px solid #ddd" multiple="multiple" ondblclick="change(form1.left_item, form1.right_item)">
			</select>
		</td>
		<td align="center">
			<div style="padding:10px;margin-top: 30px;">
				<input id="btn-up" name="btn-up" type="button" class="btn_icon" value="上移" onClick="doUp()"><p></p>
				<input id="btn-down" name="btn-down" type="button" class="btn_icon" value="下移" onClick="doDown()"><p></p>
				<input id="btn-right" name="btn-right" type="button" class="btn_icon" value=" &gt; " onClick="change(form1.left_item, form1.right_item)"><p></p>
				<input id="btn-rightAll" name="btn-rightAll" type="button" class="btn_icon" value=" &gt;&gt; " onClick="changeAll(form1.left_item, form1.right_item)"><p></p>
				<input id="btn-left" name="btn-left" type="button" class="btn_icon" value=" &lt; " onClick="change(form1.right_item, form1.left_item)"><p></p>
				<input name="btn-leftAll" id="btn-leftAll" type="button" class="btn_icon" value=" &lt;&lt; " onClick="changeAll(form1.right_item, form1.left_item)"><p></p>
			</div>
		</td>
		<td valign="top"> 
			<div style="margin-top:30px;">
				<select id="right_item" name="right_item" size="25" style="padding:5px;width:112px;height:220px;margin-top:5px; border:1px solid #ddd" ondblclick="change(form1.right_item, form1.left_item)" multiple="multiple">
				</select>
			</div>
		</td>
	</tr>
</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/base/size/size_add_group.js"></script>
</body>
</html>