<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>
								 	<td align="right">时间类型：</td>
									<td id="timetypetd">
								  		<label class="radio" id="label" style="width:50px"><input name="redioApprove" type="radio" id="TimeType0" value="0" checked="checked"/>制单日期</label>
								  		<label class="radio" id="label" style="width:50px"><input name="redioApprove" type="radio" id="TimeType1" value="1" />审核日期</label>
								  		<input type="hidden" name="timeType" id="timeType" value="0"/>
							  		</td>
								  </tr>
								 <tr>     
									<td align="right">审核状态：</td>
									<td id="ar_state">
			                            <label class="radio" style="width:30px">
							  	 			<input name="radio_ar_state" type="radio" value="" checked="checked"/>全部
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="0" />待审批
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="1" />已审批
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="2" />已退回
								  	 	</label>
								  	 	<input type="hidden" id="fe_ar_state" name="fe_ar_state" value=""/>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">付款状态：</td>
									<td id="pay_state">
			                            <label class="radio" style="width:30px">
							  	 			<input name="radio_pay_state" type="radio" value="" checked="checked"/>全部
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_pay_state" type="radio" value="0" />未付款
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_pay_state" type="radio" value="1" />未付清
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_pay_state" type="radio" value="2" />已付清
								  	 	</label>
								  	 	<input type="hidden" id="fe_pay_state" name="fe_pay_state" value=""/>
							  	 	</td>
								 </tr>
								 <tr>
									  <td align="right">批发客户：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="client_name" id="client_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryClient();"/>
										<input type="hidden" id="fe_client_code" name="fe_client_code" value=""/>
							  		</td>
								  </tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<input type="hidden" id="type" value="client"/>
	    	<a name="mode-btn" class="t-btn btn-blc on"  onclick="displayMode(this,'client');"><i></i>批发客户</a>
			<a name="mode-btn" class="t-btn btn-bc" onclick="displayMode(this,'property');"><i></i>费用类型</a>
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
	<div class="grid-wrap" style="float:left;margin-left:5px;">
	    <table id="detailGrid">
	    </table>
    	<div id="detailPage"></div>
	</div>
</div>
</form>
<script>
function displayMode(obj,type){
	$("#type").val(type);
	$("a[name='mode-btn']").removeClass("on");
	$(obj).addClass("on");
	switch(type){
		case 'client' : 
			$('#grid').setLabel("code","客户编号");
			$('#grid').setLabel("name","客户名称");
			$('#detailGrid').setLabel("code","费用类型编号");
			$('#detailGrid').setLabel("name","费用类型名称");
			$('#detailGrid').hideCol("date");
			break ;
		case 'property' : 
			$('#grid').setLabel("code","费用类型编号");
			$('#grid').setLabel("name","费用类型名称");
			$('#detailGrid').setLabel("code","客户编号");
			$('#detailGrid').setLabel("name","客户名称");
			$('#detailGrid').showCol("date");
			break ;
		default :
			break ;
	}	
	THISPAGE.reloadData();
}
</script>
<script src="<%=basePath%>data/batch/fee/fee_report.js"></script>
</body>
</html>