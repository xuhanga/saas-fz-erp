<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
h3{font-size:16px;}
.pricebt{background:#f60;padding:6px 5px;color:#fff;cursor: pointer;margin-left:5px;}
.price-pop{background:#fed;position:absolute;z-index:999;line-height:30px;top:100px;right:100px;border:1px solid #bbb;width:160px;background-color:#fff;}
.pricebt,.price-pop{-moz-border-radius: 3px;      /* Gecko browsers */
    	-webkit-border-radius: 3px;   /* Webkit browsers */
    	border-radius:3px;}
.priceslist{width:160px;}
.priceslist li{border-bottom:#aaa dotted 1px;color:#666;cursor:pointer;display:inline;float:left;width:160px;text-align:left;}
.priceslist li:hover{background:#eee;}
.priceslist li span{color:#f60;float:left;width:80px;text-align: right;}
.subtitles,.pricebt,.priceslist{font-family:"微软雅黑","Yahei";}
.poptxts{background:url(<%=basePath%>resources/grid/images/poptxt.png);width:92px;height:58px;position:absolute;padding:0 5px;line-height:25px;}
</style>

<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>

<div class="mainwra">
<input type="hidden" id="serverPath" name="serverPath" value="<%=serverPath%>"/>
<input type="hidden" id="od_type" name="od_type" value="${od_type }"/>
<input type="hidden" id="temp_unitPrice" name="temp_unitPrice" value=""/>
<input type="hidden" id="pd_sell_price" name="pd_sell_price" value=""/>
	<div style="float:left;width:620px;margin-left:10px;height:96px;overflow:hidden;">
    	<table width="100%" id="ProductInfo">
	    		<tr>
	    			<td rowspan="2" width="130">
	    				<img id="ProductPhoto" src="<%=basePath%>resources/grid/images/nophoto.png" style="cursor:pointer;border:#ddd solid 1px;" width="120" height="80" />
	    			</td>
	    			<td>
	    				<h2><span id="pd_no" ></span>(<span id="pd_name" ></span>)</h2>
	    				<p>
	    					<span id="pd_year" style="font-weight:bold"></span> 年份&nbsp;
	    					<span id="pd_season" style="font-weight:bold"></span>&nbsp;
	    					<span id="tp_name" style="font-weight:bold"></span>&nbsp;
	    					<span id="bd_name" style="font-weight:bold"></span>
	    				</p>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>单价：
	    				<input type="text" id="unitPrice_show" name="unitPrice_show" class="ui-input ui-input-ph w120" style="width: 50px;" 
	    						maxlength="12" onkeypress="javascript:onlyPositiveNumber(this)" onblur="return valNumber(this);"/>
	              		<span id="unitPrice" style="display:none"></span>
	              		
	              		
	              		<span id="spanPriceMore" class="pricebt">价格列表</span>
	              		<div id="divPriceMore" class="price-pop" style="display:none;">
					    	<ul id="ulPrice" class="priceslist">
					    	</ul>
					    </div>
	              		
		    			<span id="chkGift" style="width:40px;display:none;" >
							<label class="chk over" style="margin:6px 0 0 10px;">
								<input name="chkGift" id="chkGift_Input" type="checkbox"/>赠品
							</label>
						</span>
						<span id="isGift" style="margin:6px 0 0 10px;">
	    				</span>
	    			</td>
	    		</tr>
	    	</table>
    </div>
      <div id="noDetail" class="grid-wrap" style="float:left;margin-left:10px;width:620px;">
          <table id="sizeDetail">
          </table>
          <div id="sizePage"></div>
      </div>
	<div style="float:left;width:100%;margin-top:5px;">
		<span style="float:left;color:red;width:200px;line-height:30px;" id="spanDescribe">注：录入数量
		</span>
	    <span style="float:left;width:220px;line-height:30px;">快捷保存：按<strong>Ctrl+Enter</strong>键或按<strong>+</strong>键</span>
        <span> 
        <span id="chkVaildateStock" style="float:left;width:100px;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" />验证库存量
			</label>
		</span>
        <span id="chkRealStock" style="float:left;width:80px;">
			<label class="chk over" style="margin-top: 6px;" title="显示实际库存">
				<input name="box" type="checkbox" />实际库存
			</label>
		</span>
		<span id="chkUseableStock" style="float:left;width:80px;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" />可用库存
			</label>
		</span>
          <a class="t-btn" style="display:none;float:right;" id="btnExit">关闭</a>
          <input title="按Ctrl+Enter键或按+键" class="t-btn btn-red" style="display:none;width:30px;float:right;" id="btnSave" value="保存"/>
        </span>
	</div>
</div>
<script src="<%=basePath%>data/batch/order/order_temp_update.js"></script>
</body>
</html>