<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="mainwra">
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0" style="border-bottom:#ccc solid 1px;">
	    <tr>
	      <td colspan="4" class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存单据"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
	    
	    <tr class="list first">
			<td align="right" width="60px">客户名称：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="client_name" id="client_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryClient();"/>
				<input type="hidden" name="pp_client_code" id="pp_client_code" value="" />
			</td>
			<td align="right" width="80px">预收款余额：</td>
			<td>
				<input type="text" name="ci_prepay" id="ci_prepay" class="main_Input w146" value="" disabled="disabled"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="pp_manager" id="pp_manager" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right">预收日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="pp_date" id="pp_date" onclick="WdatePicker()" value="" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">银行账户：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="ba_name" id="ba_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank();"/>
				<input type="hidden" name="pp_ba_code" id="pp_ba_code" value="" />
			</td>
			<td align="right">账户余额：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="ba_balance" id="ba_balance"  value=""  disabled="disabled"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">预收金额：</td>
			<td>
				<input type="text" class="main_Input w146"  name="pp_money" id="pp_money"  value="0.00"
						onkeypress="javascript:return onlyDoubleNumber(this,event,true);" onblur="return valDoubleNumber(this,true);" />
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input w146" type="text" name="pp_remark" id="pp_remark" value=""/>
			</td>
		</tr>
    </table>
</div>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				pp_client_code : "required",
				pp_ba_code : "required",
				pp_manager : "required"
			},
			messages : {
				pp_client_code : "请选择批发客户",
				pp_ba_code : "请选择银行账户",
				pp_manager : "请选择经办人"
			}
		});
	});
</script>
<script src="<%=basePath%>data/batch/prepay/prepay_add.js"></script>
</body>
</html>