<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/DialogSelect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">单据类型：</td>
									<td id="td_se_type">
			                            <label class="radio" style="width:30px">
							  	 			<input name="radio_se_type" type="radio" value="" checked="checked"/>全部
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
							  	 			<input name="radio_se_type" type="radio" value="0"/>批发单
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_se_type" type="radio" value="1" />退货单
								  	 	</label>
								  	 	<input type="hidden" name="se_type" id="se_type" value=""/>
							  	 	</td>
								 </tr>
								 <tr>
									  <td align="right">批发客户：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="client_name" id="client_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryClient(false,'se_client_code','client_name');"/>
										<input type="hidden" id="se_client_code" name="se_client_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
										<td align="right">发货仓库：</td>
										<td>
										  	<input class="main_Input" type="text" id="depot_name" name="depot_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryDepot(false,'se_depot_code','depot_name','batch');"/>
										     <input type="hidden" name="se_depot_code" id="se_depot_code" value=""/>
								  		</td>
								  </tr>
								  <tr>
									  <td align="right">经办人员：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="se_manager" id="se_manager" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryEmp(false,'se_manager');"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品品牌：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="bd_name" id="bd_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryBrand(true,'bd_code','bd_name');"/>
										<input type="hidden" id="bd_code" name="bd_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品分类：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="tp_name" id="tp_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryType(true,'tp_code','tp_name');"/>
										<input type="hidden" id="tp_code" name="tp_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品季节：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_season"></span>
										<input type="hidden" name="pd_season" id="pd_season"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品年份：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_year"></span>
										<input type="hidden" name="pd_year" id="pd_year"/>
							  		</td>
								  </tr>
									<tr>
										<td align="right">商品名称：</td>
										<td>
											<input class="main_Input" type="text" readonly="readonly" name="pd_name" id="pd_name" value="" style="width:170px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryProduct(false,'pd_code','pd_name');"/>
											<input type="hidden" id="pd_code" name="pd_code" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<input type="hidden" id="CurrentMode" value="0"/>
	    	<a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			<a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
	    	<input id="btn_close" class="t-btn" type="button" value="返回"/>
	    </div> 
	 </div>
	 
	 <div class="grid-wrap">
	    <div id="list-grid" style="display:'';width: 100%;">
	        <table id="grid"></table>
	        <div id="page"></div>
	    </div>
	    <div id="size-grid" style="display:none; width: 100%;">
	        <table id="sizeGrid"></table>
	        <div id="sizePage"></div>
	    </div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/batch/report/report_sell_detail.js"></script>
</body>
</html>