<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.notpassbarcode {
	background: url('<%=basePath%>resources/grid/images/poptxt1.png');
	width: 198px;
	height: 143px;
	position: absolute;
	font-size: 12px;
}
.notcheckbody {
	float: left;
	width: 155px;
	margin: 15px 0 0 10px;
	height: 110px;
	overflow-x: hidden;
	overflow-y: auto;
	SCROLLBAR-HIGHLIGHT-COLOR: #fee7cf;
	SCROLLBAR-ARROW-COLOR: #fee7cf;
}
.notcheckbody p {
	float: left;
	width: 155px;
	height: 16px;
	overflow: hidden;
	padding-left: 5px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 

function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="se_type" name="se_type" value="${se_type }"/>
<input type="hidden" id="se_isdraft" name="se_isdraft" value="0"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存单据"/>
	        <input id="btn-import-order" class="t-btn btn-bblue" type="button" value="调入订单"/>
	        <input id="btn-import" class="t-btn btn-black" type="button" title="" value="盘点机导入"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div >
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
		<tr>
			<td width="220">
				货号查询：<input name="pd_no" type="text" id="pd_no" class="ui-input ui-input-ph w120" onkeyup="javascript:if (event.keyCode==13){Choose.selectProduct();};"/>
               <input type="button" value=" " class="btn_select" onclick="javascript:Choose.selectProduct();"/>
			</td>
			<td width="280">
				扫描条码：<input name="text" type="text" id="barcode" onkeyup="javascript:if (event.keyCode==13){Choose.barCode()};" class="ui-input ui-input-ph w146"/>
				<input id="barcode_amount" class="ui-input ui-input-ph" value="1" style="width:20px" maxlength="4"
						onkeypress="return onlyPositiveNumber(event)"
						onkeyup="javascript:valNumber(this);"/>
			</td>
			<td width="330" id="priceTypeTd">
			    <label class="radio radiow7">
			        <input type="radio" name="priceType" checked="checked" value="1"/>
			        <b>最近批发价</b>
			    </label>
			    <label class="radio radiow6">
			        <input type="radio" name="priceType" value="2"/>
			        <b>折扣率:</b>
			    </label>
			    <input name="text" type="text" id="ci_rate" disabled="disabled" class="ui-input ui-input-ph" style="width:40px; color:red;" 
			    		value="1.0" maxlength="4"
			           onkeyup="javascript:doOnlyDouble(this);"
			           onkeypress="javascript:if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
			           onkeydown="javascript:doOnlyDouble(this);"
			           />
			    <label class="radio radiow6">
			        <input type="radio" name="priceType" value="3"/>
			        <b>批发价:</b>
			    </label>
			    <span class="ui-combo-wrap" id="span_batch_price">
				</span>
				<input type="hidden" name="batch_price" id="batch_price"/>
				<input type="hidden" name="ci_default" id="ci_default"/>
			</td>
			<td>
				<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
			    <a class="t-btn btn-red" id="btnClear">清除</a>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">批发客户：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="client_name" id="client_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryClient();"/>
				<input type="hidden" name="se_client_code" id="se_client_code" value="" />
			</td>
			<td align="right" width="60px">客户店铺：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="client_shop_name" id="client_shop_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryClientShop();"/>
				<input type="hidden" name="se_client_shop_code" id="se_client_shop_code" value="" />
			</td>
			<td align="right" width="60px">${se_type eq 0 ? '发':'收' }货仓库：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="depot_name" id="depot_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot();"/>
				<input type="hidden" name="se_depot_code" id="se_depot_code" value="" />
			</td>
			<td align="right" width="60px">手工单号：</td>
			<td>
				<input class="main_Input w146" type="text" name="se_handnumber" id="se_handnumber" value="" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="se_manager" id="se_manager" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="se_make_date" id="se_make_date" onclick="WdatePicker()" value="" />
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input w146" type="text" name="se_amount" id="se_amount" value="" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_money" value="" readonly="readonly"/>
			</td>
			
		</tr>
		<tr class="list">
			<td align="right" width="60px">备注：</td>
			<td colspan="3">
				<input class="main_Input" style="width:390px" type="text" name="se_remark" id="se_remark" value=""/>
			</td>
			<td align="right">返点金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_rebatemoney" value="" readonly="readonly"/>
			</td>
			<td align="right">应收金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_receivable" value="" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last" style="display:${se_type eq 1 ? 'none' : '' };">
			<td align="right">物流公司：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="stream_name" id="stream_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryStream();"/>
				<input type="hidden" name="se_stream_code" id="se_stream_code" value="" />
			</td>
			<td align="right">物流金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_stream_money" name="se_stream_money" value="0.00"
					onkeypress="javascript:return onlyDoubleNumber(this,event);" onblur="return valDoubleNumber(this);"
					onkeyup="calcReceivable();" />
			</td>
			<td align="right">优惠金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_discount_money" name="se_discount_money" value="0.00"
					onkeypress="javascript:return onlyDoubleNumber(this,event);" onblur="return valDoubleNumber(this);"
					onkeyup="calcReceivable();" />
			</td>
			<td align="right">单据性质：</td>
			<td>
				<span class="ui-combo-wrap" id="span_property"></span>
				<input type="hidden" name="se_property" id="se_property"/>
			</td>
		</tr>
		
		
	</table>
</div>
<input type="hidden" id="CheckNotPassTextShow" value=""/>
<div id="BarCodeCheckNotPass" class="notpassbarcode" title="双击关闭错误提示" style="display:none;" ondblclick="this.style.display='none'">
 	<span class="notcheckbody" id="CheckNotPassText"></span>
</div>

</form>
<script>
function calcReceivable(){
	var se_money = $("#se_money").val();
	var se_rebatemoney = $("#se_rebatemoney").val();
	var se_discount_money = $("#se_discount_money").val();
	var se_stream_money = $("#se_stream_money").val();
	if(se_rebatemoney == "" || isNaN(se_rebatemoney)){
		se_rebatemoney = 0;
	}
	if(se_discount_money == "" || isNaN(se_discount_money)){
		se_discount_money = 0;
	}
	if(se_stream_money == "" || isNaN(se_stream_money)){
		se_stream_money = 0;
	}
	$("#se_receivable").val(PriceLimit.formatByBatch(se_money-se_rebatemoney-se_discount_money+parseFloat(se_stream_money)));
}
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				se_client_code : "required",
				se_depot_code : "required",
				se_manager : "required"
			},
			messages : {
				se_client_code : "请选择批发客户",
				se_depot_code : "请选择仓库",
				se_manager : "请选择经办人"
			}
		});
	});
</script>
<script src="<%=basePath%>data/batch/sell/sell_add.js"></script>
</body>
</html>