<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="se_id" name="se_id" value="${sell.se_id }"/>
<input type="hidden" id="se_type" name="se_type" value="${sell.se_type }"/>
<input type="hidden" id="se_number" name="se_number" value="${sell.se_number }"/>
<input type="hidden" id="se_ar_state" name="se_ar_state" value="${sell.se_ar_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	        
	        <span style="float:right; padding-right:10px;">
	        	<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
            </span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">批发客户：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="client_name" id="client_name" value="${sell.client_name }"/>
				<input type="hidden" name="se_client_code" id="se_client_code" value="${sell.se_client_code }" />
			</td>
			<td align="right" width="60px">客户店铺：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="client_shop_name" id="client_shop_name" value="${sell.client_shop_name }"/>
				<input type="hidden" name="se_client_shop_code" id="se_client_shop_code" value="${sell.se_client_shop_code }" />
			</td>
			<td align="right" width="60px">${sell.se_type eq 0 ? '发':'收' }货仓库：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="depot_name" id="depot_name" value="${sell.depot_name }"/>
				<input type="hidden" name="se_depot_code" id="se_depot_code" value="${sell.se_depot_code }" />
			</td>
			<td align="right" width="60px">手工单号：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="se_handnumber" id="se_handnumber" value="${sell.se_handnumber }" />
			</td>
		</tr>
		<tr class="list last">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="se_manager" id="se_manager" value="${sell.se_manager }"/>
			</td>
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="se_make_date" id="se_make_date" value="${sell.se_make_date }"/>
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input w146" type="text" name="se_amount" id="se_amount" value="${sell.se_amount }" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_money" value="${sell.se_money }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">单据编号：</td>
			<td>
				<input class="main_Input w146" type="text" value="${sell.se_number }" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input w146" type="text" name="se_remark" id="se_remark" value="${sell.se_remark }"/>
			</td>
			<td align="right">返点金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_rebatemoney" value="${sell.se_rebatemoney }" readonly="readonly"/>
			</td>
			<td align="right">应收金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_receivable" value="${sell.se_receivable }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last" style="display:${sell.se_type eq 1 ? 'none' : '' };">
			<td align="right">物流公司：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="stream_name" id="stream_name" value="${sell.stream_name }"/>
			</td>
			<td align="right">物流金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_stream_money" value="${sell.se_stream_money }"/>
			</td>
			<td align="right">优惠金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="se_discount_money" name="se_discount_money" value="${sell.se_discount_money }"/>
			</td>
			<td align="right">单据性质：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="se_property" id="se_property" value="${sell.se_property }"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/batch/sell/sell_view.js"></script>
</body>
</html>