<%@page import="zy.util.StringUtil"%>
<%@page import="zy.util.CommonUtil"%>
<%@page import="zy.entity.sys.set.T_Sys_Set"%>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
</script> 
<style>
.opselect{border:#ddd solid 1px;height:25px;line-height:25px;padding:0 0 0 5px;}
</style>
</head>
<body>
<%
	T_Sys_Set set = (T_Sys_Set)request.getSession().getAttribute(CommonUtil.KEY_SYSSET);
%>
<div class="wrapper" >
	<input type="hidden" id="et_number" name="et_number" value="${number }"/>
	<input type="hidden" name="ST_SubCodeRule" id="ST_SubCodeRule" value="<%=StringUtil.trimString(set.getSt_subcode_rule())%>"/><!-- 条形码生成规则明细 -->
	<input type="hidden" name="ST_SubCodeIsRule" id="ST_SubCodeIsRule" value="<%=StringUtil.trimString(set.getSt_subcode_isrule())%>"/><!-- 条形码生成规则 0默认 1自己配置 -->
	<font style="color: red;font-size: 12px;">条形码生成规则：<%=StringUtil.trimString(set.getSt_subcode_rule()) %></font>
	    <div class="fr">
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-print">打印</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-print-preview">打印预览</a>
	    	<!-- <a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-printSite">打印设置</a> -->
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-bulkCopyBarcode">批量复制</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-bulkSaveBarcode">批量保存</a>
	    </div>
	 </div>
	<div class="grid-wrap" style="padding:0px;margin-left:15px;">
	    <table id="grid">
	    </table>
	   	<div id="page"></div>
	</div>
	</div>
</body>
<script src="<%=basePath%>data/buy/enter/enter_print_barcode.js"></script>
<script src="<%=basePath%>resources/util/forbidBackSpace.js"></script>
<object id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0> 
	<embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0 pluginspage="install_lodop32.exe"></embed>
</object>
<script language="javascript" src="<%=basePath%>resources/util/barcode_print.js"></script>
<script language="javascript" src="<%=basePath%>resources/util/LodopFuncs.js"></script>
</html>