<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="et_id" name="et_id" value="${enter.et_id }"/>
<input type="hidden" id="et_type" name="et_type" value="${enter.et_type }"/>
<input type="hidden" id="et_number" name="et_number" value="${enter.et_number }"/>
<input type="hidden" id="et_ar_state" name="et_ar_state" value="${enter.et_ar_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn-print" class="t-btn btn-yellow" type="button" value="条码打印" onclick="javascript:handle.doBarcode();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	        <span style="float:right; padding-right:10px;">
	        	<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
            </span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">供货厂商：</td>
			<td width="130px">
				<input class="main_Input" type="text" readonly="readonly" name="supply_name" id="supply_name" value="${enter.supply_name }"/>
				<input type="hidden" name="et_supply_code" id="et_supply_code" value="${enter.et_supply_code }" />
			</td>
			<td align="right" width="60px">收获仓库：</td>
			<td width="130px">
				<input class="main_Input" type="text" readonly="readonly" name="depot_name" id="depot_name" value="${enter.depot_name }"/>
				<input type="hidden" name="et_depot_code" id="et_depot_code" value="${enter.et_depot_code }" />
			</td>
			<td align="right" width="60px">手工单号：</td>
			<td width="130px">
				<input class="main_Input" type="text" readonly="readonly" name="et_handnumber" id="et_handnumber" value="${enter.et_handnumber }" />
			</td>
			<td align="right" width="60px">单据性质：</td>
			<td>
				<input readonly type="text" class="main_Input"  name="et_property" id="et_property" value="${enter.et_property }"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="et_manager" id="et_manager" value="${enter.et_manager }"/>
			</td>
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate" style="width:198px;"  name="et_make_date" id="et_make_date" value="${enter.et_make_date }"/>
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input" type="text" name="et_amount" id="et_amount" value="${enter.et_amount }" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input" type="text" id="et_money" value="${enter.et_money }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">单据编号：</td>
			<td>
				<input class="main_Input" type="text" value="${enter.et_number }" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input" type="text" name="et_remark" id="et_remark" value="${enter.et_remark }" readonly="readonly"/>
			</td>
			<td align="right" style="display:${enter.et_type eq 1 ? 'none' : '' };">优惠金额：</td>
			<td style="display:${enter.et_type eq 1 ? 'none' : '' };">
				<input class="main_Input" type="text" id="et_discount_money" name="et_discount_money" value="${enter.et_discount_money }" readonly="readonly"/>
			</td>
			<td align="right" style="display:${enter.et_type eq 1 ? 'none' : '' };">应付金额：</td>
			<td style="display:${enter.et_type eq 1 ? 'none' : '' };">
				<input class="main_Input" type="text" id="et_payable" value="${enter.et_payable }" readonly="readonly"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/buy/enter/enter_view.js"></script>
</body>
</html>