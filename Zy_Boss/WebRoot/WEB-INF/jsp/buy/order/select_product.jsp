<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
h3{font-size:16px;}
.pricebt{background:#f60;padding:6px 5px;color:#fff;cursor: pointer;margin-left:5px;}
.price-pop{background:#fed;position:absolute;z-index:999;line-height:30px;top:100px;right:100px;border:1px solid #bbb;width:160px;background-color:#fff;}
.pricebt,.price-pop{-moz-border-radius: 3px;      /* Gecko browsers */
    	-webkit-border-radius: 3px;   /* Webkit browsers */
    	border-radius:3px;}
.priceslist{width:160px;}
.priceslist li{border-bottom:#aaa dotted 1px;color:#666;cursor:pointer;display:inline;float:left;width:160px;text-align:right;}
.priceslist li:hover{background:#eee;}
.priceslist li span{color:#f60;float:right;width:80px;text-align:left;}
.subtitles,.pricebt,.priceslist{font-family:"微软雅黑","Yahei";}
.poptxts{background:url(<%=basePath%>resources/grid/images/poptxt.png);width:92px;height:58px;position:absolute;padding:0 5px;line-height:25px;}
</style>

<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>

<div class="mainwra">
<input type="hidden" id="serverPath" name="serverPath" value="<%=serverPath%>"/>
<input type="hidden" id="od_type" name="od_type" value="${od_type }"/>
<input type="hidden" id="temp_unitPrice" name="temp_unitPrice" value=""/>

	<div style="float:left;width:400px;height:96px;overflow:hidden;">
    	<table width="100%" height="90">
        	<tr>
            	<td>
              		<input type="text" class="ui-input ui-input-ph" style="width:200px;" id="SearchContent" name="SearchContent" value=""/>
              		<a class="ui-btn mrb" id="search">查询</a>
            	</td>
            </tr>
            <tr>
            	<td>
            		<span id="exactQuery" style="float:left;width:60px;">
            			<label class="chk" style="margin-top:6px; " title="显示已录">
                			<input type="checkbox" name="box"/>精确
             			</label>
            		</span>
            		<span id="alreadyExist" style="float:left;width:60px;">
            			<label class="chk" style="margin-top:6px; " title="显示已录">
                			<input type="checkbox" name="box"/>已录
              			</label>
            		</span>
            	</td>
        	</tr>
    	</table>
    </div>
	<div style="float:left;width:620px;margin-left:10px;height:96px;overflow:hidden;">
    	<table width="100%" id="ProductInfo" style="display:none;">
	    		<tr>
	    			<td rowspan="3" width="130">
	    				<img id="ProductPhoto" src="<%=basePath%>resources/grid/images/nophoto.png" style="cursor:pointer;border:#ddd solid 1px;" width="120" height="80" />
	    			</td>
	    			<td width="145">
	    				<input type="radio" name="priceType" id="priceType2" value="2" onclick="javascript:radio_click('2');" />
						折扣率：<input name="sp_rate" type="text" id="sp_rate" class="main_Input" maxlength="4" style="color: red;font: bold;width:30px;" 
									onkeyup="if(isNaN(value))execCommand('undo');calUnitPrice()" 
									onkeydown="checkzero();" 
									onafterpaste="if(isNaN(value))execCommand('undo')"/>
						<input type="hidden" id="priceType" value=""/>
	    			</td>
	    			<td rowspan="2">
	    				<h2><span id="pd_no" ></span>(<span id="pd_name" ></span>)</h2>
	    				<p>
	    					<span id="pd_year" style="font-weight:bold"></span> 年份&nbsp;
	    					<span id="pd_season" style="font-weight:bold"></span>&nbsp;
	    					<span id="tp_name" style="font-weight:bold"></span>&nbsp;
	    					<span id="bd_name" style="font-weight:bold"></span>
	    				</p>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>
	    			<input type="radio" name="priceType" id="priceType1" value="1" onclick="javascript:radio_click('1');" />
	    			<input type="hidden" name="last_buy_price" id="last_buy_price"/>
		              	最后一次进价
	    			</td>
	    		</tr>
	    		<tr>
	    			<td id="td_price">
	    			&nbsp;&nbsp;&nbsp;&nbsp;进货价：
	    				<input type="text" id="unitPrice_show" name="unitPrice_show" class="ui-input ui-input-ph w120" style="width: 50px;" 
	    						maxlength="12" onkeypress="javascript:onlyPositiveNumber(this)" onblur="return valNumber(this);"/>
	              		<span id="unitPrice" style="display:none"></span>
	    			</td>
	    			<td>
		    			<span id="chkGift" style="width:40px" >
							<label class="chk over" style="margin:6px 0 0 10px;">
								<input name="chkGift" type="checkbox" />赠品
							</label>
						</span>
	    			
	    			</td>
	    			<td style="display:none;">
	              		<!-- 零售价： -->
	              		零售价：
	              		<span id="pd_sell_price_show"></span>
	              		<span id="pd_sell_price" style="display:none"></span>
	              		
	    			</td>
	    		</tr>
	    	</table>
    </div>
        <div class="grid-wrap" style="float:left;width:400px;">
            <table id="grid">
            </table>
            <div id="page"></div>
        </div>
    
      <div id="noDetail" class="grid-wrap" style="float:left;margin-left:10px;width:620px;">
          <table id="sizeDetail">
          </table>
          <div id="sizePage"></div>
      </div>
	<div style="float:left;width:100%;margin-top:5px;">
		<span style="float:left;color:red;width:200px;line-height:30px;" id="spanDescribe">注：录入数量
		</span>
	    <span style="float:left;width:220px;line-height:30px;">快捷保存：按<strong>Ctrl+Enter</strong>键或按<strong>+</strong>键</span>
        <span> 
        <span id="chkVaildateStock" style="float:left;width:100px;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" />验证库存量
			</label>
		</span>
        <span id="chkRealStock" style="float:left;width:80px;">
			<label class="chk over" style="margin-top: 6px;" title="显示实际库存">
				<input name="box" type="checkbox" />实际库存
			</label>
		</span>
		<span id="chkUseableStock" style="float:left;width:80px;">
			<label class="chk over" style="margin-top: 6px;">
				<input name="box" type="checkbox" />可用库存
			</label>
		</span>
			<a class="t-btn" style="display:none;float:right;" id="btnExit">关闭</a>
          	<input title="按Ctrl+Enter键或按+键" class="t-btn btn-red" style="display:none;width:30px;float:right;" id="btnSave" value="保存"/>
          	<input title="按Ctrl+Enter键或按+键" class="t-btn btn-bblue" style="display:none;width:90px;float:right;" id="btnSaveAndNext" value="保存并下一条"/>
        </span>
	</div>
</div>
<script>
	function checkzero(){
		if (document.getElementById("sp_rate").value >1 || document.getElementById("sp_rate").value <0){
			$.dialog.tips("你输入的折扣率必须是在0和1之间！",2,"32X32/hits.png");
			document.getElementById("sp_rate").value="";
			return;
		}
	}
	function calUnitPrice(){
		var unitPrice = 0;
		var priceType = $("#priceType").val();
		if(priceType == "1"){
			unitPrice = parseFloat($("#last_buy_price").val());
		}else if(priceType == "2" && $("#sp_rate").val() != ""){
			unitPrice = parseFloat($("#sp_rate").val())*parseFloat($("#pd_sell_price").text());
		}
		if($("#unitPrice_show").val() != "***"){
			$("#unitPrice_show").val(unitPrice.toFixed(2));
		}
		$("#unitPrice").text(unitPrice.toFixed(2));
	}
	//单选框单击事件，折扣率，最近价
	function radio_click(val){
		$("#priceType").val(val);
		calUnitPrice();
	}
</script>
<script src="<%=basePath%>data/buy/order/select_product.js?v=20190707"></script>
</body>
</html>