<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.notpassbarcode {
	background: url(<%=basePath%>resources/grid/images/poptxt1.png);
	width: 198px;
	height: 143px;
	position: absolute;
	font-size: 12px;
}
.notcheckbody {
	float: left;
	width: 155px;
	margin: 15px 0 0 10px;
	height: 110px;
	overflow-x: hidden;
	overflow-y: auto;
	SCROLLBAR-HIGHLIGHT-COLOR: #fee7cf;
	SCROLLBAR-ARROW-COLOR: #fee7cf;
}
.notcheckbody p {
	float: left;
	width: 155px;
	height: 16px;
	overflow: hidden;
	padding-left: 5px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 
function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="od_id" name="od_id" value="${order.od_id }"/>
<input type="hidden" id="od_type" name="od_type" value="${order.od_type }"/>
<input type="hidden" id="od_number" name="od_number" value="${order.od_number }"/>
<input type="hidden" id="od_ar_state" name="od_ar_state" value="${order.od_ar_state }"/>
<input type="hidden" id="od_state" name="od_state" value="${order.od_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-stop" class="t-btn btn-bblue" type="button" value="终止" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	        
	        <span style="float:right; padding-right:10px;">
	        	<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
            </span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">供货厂商：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="supply_name" id="supply_name" value="${order.supply_name }"/>
				<input type="hidden" name="od_supply_code" id="od_supply_code" value="${order.od_supply_code }" />
			</td>
			<td align="right" width="60px">收获仓库：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="depot_name" id="depot_name" value="${order.depot_name }"/>
				<input type="hidden" name="od_depot_code" id="od_depot_code" value="${order.od_depot_code }" />
			</td>
			<td align="right" width="60px">手工单号：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="od_handnumber" id="od_handnumber" value="${order.od_handnumber }" />
			</td>
			<td align="right" width="60px">单据编号：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" value="${order.od_number }" readonly="readonly"/>
			</td>
			<td align="right" width="60px">备注：</td>
			<td>
				<input class="main_Input w146" type="text" name="od_remark" id="od_remark" value="${order.od_remark }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="od_manager" id="od_manager" value="${order.od_manager }"/>
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input w146" type="text" name="od_amount" id="od_amount" value="${order.od_amount }" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="od_money" value="${order.od_money }" readonly="readonly"/>
			</td>
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="od_make_date" id="od_make_date" value="${order.od_make_date }"/>
			</td>
			<td align="right">交货日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="od_delivery_date" id="od_delivery_date" value="${order.od_delivery_date }"/>
			</td>
		</tr>
	</table>
</div>
<input type="hidden" id="CheckNotPassTextShow" value=""/>
<div id="BarCodeCheckNotPass" class="notpassbarcode" title="双击关闭错误提示" style="display:none;" ondblclick="this.style.display='none'">
 	<span class="notcheckbody" id="CheckNotPassText"></span>
</div>

</form>
<script src="<%=basePath%>data/buy/order/order_view.js"></script>
</body>
</html>