<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/DialogSelect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">排行类型：</td>
									<td id="td_type">
			                            <label class="radio" style="width:30px">
							  	 			<input name="radio_type" type="radio" value="supply" checked="checked"/>供应商汇总
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_type" type="radio" value="brand" />品牌汇总
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_type" type="radio" value="type" />类别汇总
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_type" type="radio" value="product" />商品汇总
								  	 	</label>
								  	 	<label class="radio" style="width:30px;display:none;">
								  	 		<input name="radio_type" type="radio" value="season" />季节汇总
								  	 	</label>
								  	 	<input type="hidden" name="type" id="type" value="supply"/>
							  	 	</td>
								 </tr>
								 <tr>
									  <td align="right">供货厂商：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="supply_name" id="supply_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQuerySupply(false,'et_supply_code','supply_name');"/>
										<input type="hidden" id="et_supply_code" name="et_supply_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">收货仓库：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="depot_name" id="depot_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryDepot(false,'et_depot_code','depot_name','buy');"/>
										<input type="hidden" id="et_depot_code" name="et_depot_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">经办人员：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="et_manager" id="et_manager" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryEmp(false,'et_manager');"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品品牌：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="bd_name" id="bd_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryBrand(true,'bd_code','bd_name');"/>
										<input type="hidden" id="bd_code" name="bd_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品分类：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="tp_name" id="tp_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryType(true,'tp_code','tp_name');"/>
										<input type="hidden" id="tp_code" name="tp_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品季节：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_season"></span>
										<input type="hidden" name="pd_season" id="pd_season"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品年份：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_year"></span>
										<input type="hidden" name="pd_year" id="pd_year"/>
							  		</td>
								  </tr>
									<tr>
										<td align="right">商品名称：</td>
										<td>
											<input class="main_Input" type="text" readonly="readonly" name="pd_name" id="pd_name" value="" style="width:170px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:DialogSelect.doQueryProduct(false,'pd_code','pd_name');"/>
											<input type="hidden" id="pd_code" name="pd_code" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/buy/report/summary_enter.js"></script>
</body>
</html>