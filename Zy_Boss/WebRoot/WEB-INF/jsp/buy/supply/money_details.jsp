<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" style="width: 510px;">
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right" width="60">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								<tr>
						        	<td align="right">单据类型：</td>
						        	<td>
							        	<span id="type_all">
							              	<label class="chk" style="margin-top:0px; " title="全部">全部
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="type_0">
							              	<label class="chk" style="margin-top:0px; " title="进货单">进货单
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="type_1">
							              	<label class="chk" style="margin-top:0px; " title="退货单">退货单
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="type_2">
							              	<label class="chk" style="margin-top:0px; " title="期初单">期初单
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="type_3">
							              	<label class="chk" style="margin-top:0px; " title="费用单">费用单
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="type_4">
							              	<label class="chk" style="margin-top:0px; " title="预付款单">预付款单
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="type_5">
							              	<label class="chk" style="margin-top:0px; " title="预付款单">退款单
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <input type="hidden" id="type" name="type" value=""/>
						             </td>
						        </tr>
								<tr>
						        	<td align="right">付款状态：</td>
						        	<td>
							        	<span id="pay_state_all">
							              	<label class="chk" style="margin-top:0px; " title="全部">全部
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="pay_state_0">
							              	<label class="chk" style="margin-top:0px; " title="未付款">未付款
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="pay_state_1">
							              	<label class="chk" style="margin-top:0px; " title="未付清">未付清
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <span id="pay_state_2">
							              	<label class="chk" style="margin-top:0px; " title="已付清">已付清
							                	<input type="checkbox" name="box" checked="checked"/>
							              	</label>
							              </span>
							              <input type="hidden" id="pay_state" name="pay_state" value=""/>
						             </td>
						        </tr>
						        <tr>
									  <td align="right">供货厂商：</td>
										<td>
										  	<input class="main_Input" type="text" id="supply_name" name="supply_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQuerySupply();"/>
										     <input type="hidden" name="supply_code" id="supply_code" value=""/>
								  		</td>
								  </tr>
								  <tr>
										<td align="right">经办人员：</td>
										<td>
										  	<input class="main_Input" type="text" readonly="readonly" name="manager" id="manager" value="" style="width:170px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
								  		</td>
									</tr>
									<tr>
										<td align="right">单据编号：</td>
										<td>
											<input type="text" id="number" name="number" class="main_Input" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/buy/supply/supply_money_details.js"></script>
</body>
</html>