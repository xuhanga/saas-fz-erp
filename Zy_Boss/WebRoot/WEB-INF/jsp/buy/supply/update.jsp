<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table class="pad_t20" style="background:#fff;">
	  <tr>
	    <td width="90" ><div align="right"><font color="red">*</font>供货商编号：</div></td>
	    <td width="230" >
	    <input class="main_Input" disabled type="text" name="sp_code_disabled" id="sp_code_disabled" value="${supply.sp_code}"/>
	    </td>
	    <td  width="90"><div align="right"><font color="red">*</font>供货商名称：</div></td>
	    <td >
	   		<input class="main_Input" type="text" name="sp_name" id="sp_name" maxlength="20"
	     	title="请输入供应商名称，不能超过20个汉字" value="${supply.sp_name}" />
	   	</td>
	  </tr>
	  <tr>
	    <td ><div align="right">联系人：</div></td>
	    <td ><input class="main_Input" type="text" maxlength="12" name="spi_man" id="spi_man" value="${supply.spi_man}"/></td>
	    <td ><div align="right">手机：</div></td>
	    <td ><input class="main_Input" type="text" name="spi_mobile" id="spi_mobile"  maxlength="11" value="${supply.spi_mobile}" onblur="javascript:handle.checkMobile(this);" /></td>
	  </tr>
	  <tr>
	    <td ><div align="right">电话：</div></td>
	    <td ><input class="main_Input" type="text" name="spi_tel" id="spi_tel" onblur="javascript:handle.isTel(this);"  maxlength="15" value="${supply.spi_tel}"/></td>
	    <td ><div align="right">地址：</div></td>
	    <td  ><input class="main_Input" maxlength="35" title="请输入地址，不能超过35个汉字" type="text" name="spi_addr" id="spi_addr" value="${supply.spi_addr}" /></td>
	  </tr>
	  <tr>
	    <td ><div align="right">开户银行：</div></td>
	    <td ><input class="main_Input" type="text" name="spi_bank_open" id="spi_bank_open" maxlength="20" value="${supply.spi_bank_open}"/></td>
	    <td ><div align="right">银行账户：</div></td>
	    <td ><input class="main_Input" type="text" onblur="javascript:handle.doCheckNumber(this);"  onkeydown="javascript:handle.doCheckNumber(this);"  name="spi_bank_code" id="spi_bank_code" value="${supply.spi_bank_code}"/></td>
	  </tr>
	  <tr>
	    <td ><div align="right">进货周期：</div></td>
	    <td ><input class="main_Input" type="text" name="sp_buy_cycle" id="sp_buy_cycle"  value="${supply.sp_buy_cycle}" onblur="javascript:handle.doCheckNumber(this);"  onkeydown="javascript:handle.doCheckNumber(this);"/></td>
	    <td ><div align="right">结账周期：</div></td>
	    <td ><input class="main_Input" type="text" name="sp_settle_cycle" id="sp_settle_cycle" value="${supply.sp_settle_cycle}" onblur="javascript:handle.doCheckNumber(this);" onkeydown="javascript:handle.doCheckNumber(this);"/></td>
	  </tr>
	  <tr>
	  	<td ><div align="right">保证金：</div></td>
	  	<td ><input class="main_Input" type="text" name="spi_earnest" id="spi_earnest" value="${supply.spi_earnest}"/></td>
	  	<td ><div align="right">装修押金：</div></td>
	  	<td ><input class="main_Input" type="text" name="spi_deposit" id="spi_deposit" value="${supply.spi_deposit}"/></td>
	  </tr>
	   <tr>
	    <td ><div align="right">折扣率：</div></td>
	    <td ><input class="main_Input" type="text" name="sp_rate" id="sp_rate" onblur="javascript:handle.doOnlyDouble(this);" value="${supply.sp_rate}"/></td>
	    <td ><div align="right">期初欠款：</div></td>
	  	<td ><input class="main_Input" type="text" name="sp_init_debt" id="sp_init_debt" value="${supply.sp_init_debt}" ${sessionScope.user.sp_init eq 1 ? 'disabled' : ''}/></td>
	  </tr>
	  <tr>
	     <td ><div align="right">所在地区：</div></td>
	     <td >
	    	<span class="ui-combo-wrap" id="span_sp_ar_Code"></span>
			<input type="hidden" name="sp_ar_code" id="sp_ar_code" value="${supply.sp_ar_code}"/>
	     </td>
	    <td ><div align="right">备注：</div></td>
	    <td ><input title="请输入备注，不能超过50个汉字" maxlength="50" class="main_Input" type="text" name="spi_remark" id="spi_remark" value="${supply.spi_remark}" /></td>
	  </tr>
	</table>
</div>
<input type="hidden" id="sp_id" name="sp_id" value="${supply.sp_id}"/>
<input type="hidden" id="sp_code" name="sp_code" value="${supply.sp_code}"/>
<input type="hidden" name="sp_ar_name" id="sp_ar_name"/>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/buy/supply/supply_update.js"></script>
</body>
</html>