<%@page import="zy.util.StringUtil"%>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>智慧数在线平台</title>
<link href="<%=basePath%>resources/css/index.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
var basepath = '${static_path}';
document.write("<scr"+"ipt src=\""+basepath+"/resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\""+basepath+"/resources/dialog/lhgdialog.js?self=false\"></sc"+"ript>");
</script>
<script type="text/javascript">
$(function(){
	if($("#co_code").val().trim() != ""){
		if($("#us_account").val().trim() != ""){
			$("#us_pass").select().focus();
		}else{
			$("#us_account").focus();
		}
	}else{
		$("#co_code").select().focus();	
	}
	$("#co_code").on("keyup",function(event){
		if(event.keyCode == 13){
			$("#us_account").select().focus();
		}
	});
	$("#us_account").on("keyup",function(event){
		if(event.keyCode == 13){
			$("#us_pass").select().focus();
		}
	});
	$("#us_pass").on("keyup",function(event){
		if(event.keyCode == 13){
			doLogin();
		}
	});
});
function doLogin(){
	var us_pass=$.trim($("#us_pass").val());
	var us_account=$.trim($("#us_account").val());
	var co_code=$.trim($("#co_code").val());
	var remember = $("#remember").val();
	if(us_account == ''){
		$.dialog.tips("请输入用户编号!",1,"32X32/hits.png");
		$("#us_account").focus();
		return;
	}
	
	if(us_pass == ''){//后台密码
		$.dialog.tips("请输入商家密码!",1,"32X32/hits.png");
		$("#us_pass").focus();
		return;
	}
	var url = basepath+"login";
	$.dialog.tips("正在登录中...",600,"loading.gif");
	$.ajax({
		type:"POST",
		data:{"us_account":us_account,"us_pass":us_pass,"co_code":co_code,"remember":remember},
		url:url,
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				window.location.replace(basepath+"main");
			}else{
				$.dialog.tips("帐户异常!",1,"32X32/hits.png");
			}
		}
	 });
}
/* function doRegist(){
    $.dialog({ 
	   	id:'doRegist_id',
	   	title:'用户注册',
	   	max: false,
	   	min: false,
	   	width:660,
	   	height:340,
	   	fixed:false,
	   	drag: true,
	   	resize:false,
	   	content:'url:'+'toreg',
	   	lock:true
    });
} */
</script>
</head>

<body>
	<div class="big-bg">
    	<div class="login">
        	<div class="login-n">
        		<span class="login_title">
        			<h3>后台管理</h3>
        			<b></b>
        		</span>
        		<%
		      		String us_co_code = "",us_account = "",check = "",rememberVal="0";
		      		java.util.Map<String,String> login = zy.interceptor.CookieHandler.getCookies(request);
		      		if(login != null){
			      		String remember = login.get("us_remember");
			      		if(remember != null && remember.equals("1")){
			      			us_co_code = login.get("us_co_code");
			      			us_account = login.get("us_account");
			      			rememberVal = login.get("us_remember");
			      			check = "checked";
			      		}
		      		}
		      	%>
        		<div>
                	<label>编号：</label><input class="login-user" type="text" id="co_code" name="co_code" value="<%=StringUtil.trimString(us_co_code)%>"/>
                </div>
                <div>
                	<label>帐号：</label><input class="login-user" type="text" id="us_account" name="us_account" value="<%=StringUtil.trimString(us_account)%>"/>
                </div>
                <div>
                	<label>密码：</label><input class="login-user" type="password" id="us_pass" name="us_pass" value=""/>
                </div>
                <div style="margin-left: 35px;">
                	<input id="remember" type="hidden" name="remember" value="<%=StringUtil.trimString(rememberVal)%>"/>
					<input type="checkbox" <%=StringUtil.trimString(check)%> onclick="javascript:if(this.checked){$('#remember').val(1);}else{$('#remember').val(0);}"/> 
					记住账号
                </div>
                 <p>
                	<input type="button" class="btn_submit" onclick="javascript:doLogin();" value="登 录" /> 
                </p>
            </div>        	
        </div>
    </div>
</body>
</html>
