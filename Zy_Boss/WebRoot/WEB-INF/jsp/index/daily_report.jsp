<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>每日必看</title>
<meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/everyday.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.5.0.12.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts-more.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/modules/solid-gauge.js\"></sc"+"ript>");
</script>
<script>
	 function chageActivityMode(mode){
		 if(mode == "1"){
			 $("#todayActMode").addClass("cur");
			 $("#lastWeekDayActMode").removeClass();
			 $("#todayAct").show();
			 $("#lastWeekDayAct").hide();
		 }else if(mode == "2"){
			 $("#todayActMode").removeClass();
			 $("#lastWeekDayActMode").addClass("cur");
			 $("#todayAct").hide();
			 $("#lastWeekDayAct").show();
		 }
	 }
	</script>
</head>
<body id="main_content">
<input type="hidden" name="Si_Code" id="Si_Code" value=""/>
<div class="main" id="main" style="overflow:hidden;">

<div class="weather">
 	<div class="weather_tit">天气情况</div>
	<div class="weather weather_info">
    	<h3 id="city_name">&nbsp;</h3>
        <i></i>
        <span class="first" id="we_name"></span>
        <span class="second">最高温度：<a id="we_max_tmp"></a>℃</span>
        <span class="third">最低温度：<a id="we_min_tmp"></a>℃</span>
    </div>
 </div>
 <div class="act">
 	<table  border="0" cellspacing="0" cellpadding="0" width="100%" height="100%" class="tablet">
	       <tr class="t_top">
	         <th scope="col" width="100">计划进度</th>
	         <th scope="col" width="90">完成</th>
	         <th scope="col" width="90">计划</th>
	         <th scope="col" class="t_last">进度条</th>
	       </tr>
	       <tr class="t_second">
	         <th scope="row"><span id="theDay"></span></th>
	         <td><span id="day_sellMoney"></span></td>
	         <td><span id="day_planMoney"></span></td>
	         <td class="t_last">
	           	<div class="chartdw" id="chartdw">
		         <div class="chart_b color_g" id="monthChart">
		             <span style="width:0px;" id="finishMonthChart"></span>
		             <!-- <b>此日</b> -->
		             <i id="monthPercent"></i>
		         </div>
		         </div>
	         </td>
	       </tr>
	       <tr>
	         <th scope="row"><span id="theWeek"></span></th>
	         <td><span id="week_sellMoney"></span></td>
	         <td><span id="week_planMoney"></span></td>
	         <td class="t_last">
	         <div class="chartdw">
	         <div class="chart_b color_b" id="yearChart">
	             <span style="width:0px;" id="finishYearChart"></span>
	             <!-- <b>此周</b> -->
	             <i id="yearPercent"></i>
	         </div>
	         </div>
	         </td>
	       </tr>
    </table>
 </div>


 <div class="everyday" id="everyday">
 	<div class="evetop">
    	<div class="inputser">
    		<label style="float:left;">&nbsp;&nbsp;&nbsp;店铺选择：</label>	
    		<span style="float:left;">
    		<input class="main_Input" type="text" id="shop_name" name="shop_name" value="" readonly="readonly" style="width:170px; " />
		     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
		     <input type="hidden" id="shopCode" value=""/>
	          <i class="trigger"></i>
	    </span>
	    <label style="float:left;">&nbsp;&nbsp;日期：</label>
    	<input type="text" class="main_Input Wdate-select" style="float:left; width:100px; padding:6px 5px 5px 5px; border:1px solid #e9e9e9; margin:3px 0px;" 
    			readonly="readonly" id="querydate" name="querydate" onclick="WdatePicker();THISPAGE.changeDate();"  value="" />
    	<input type="hidden" id="srcQueryDate" value=""/>
    	<span class="week_Day" id="week"></span>
    	</div>
    	<span  onclick="javascript:THISPAGE.changeMode('HourPeriod');" id="HourPeriodMode">店铺时段图表</span>
    	<span class="cur" onclick="javascript:THISPAGE.changeMode('StoreKPI');" id="StoreKPIMode">店铺KPI图例</span>
    	<input type="hidden" id="CurrentMode" value="HourPeriod"/>
    </div>
    
    <div class="biao tong" style="display:none;width:100%;" id="HourPeriodDiv">
    	<div id="container" style="background:#fff; width:100%; float:left;">
    	</div>
    	<script>
    	document.getElementById("container").style.height=(document.documentElement.clientHeight-220) + "px";
    	</script>
    </div>
    
    
    <div class="listkpi tong" id="everydayDiv">
         <table id="everydayTable" class="bigtable" cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
            <tr>
            	<td style="border-left: medium none;"><div><i class="iconfont bg_red">&#xe602;</i><span class="iconame">销售数量</span><span id="sellAmount"></span></div></td>
            	<td><div><i class="iconfont bg_yellow">&#xe649;</i></i><span class="iconame">会员消费</span><span id="sellMoney_vip"></span></div></td>
                <td><div><i class="iconfont bg_orange">&#xe620;</i><span class="iconame">会员占比</span><span id="sellMoney_vipRate"></span></div></td>
                <td><div><i class="iconfont bg_gray">&#xe64c;</i><span class="iconame">成本金额</span><span id="costMoney"></span></div></td>
                <td><div><i class="iconfont bg_violet">&#xe64f;</i><span class="iconame">毛利润</span><span id="profits"></span></div></td>
                <td><div><i class="iconfont bg_blue">&#xe621;</i><span class="iconame">利润率</span><span id="profits_rate"></span></div></td>
            </tr>
            <tr>
            	<td style="border-left: medium none;"><div><i class="iconfont bg_red">&#xe62a;</i><span class="iconame">销售额</span><span id="sellMoney"></span></div></td>
            	<td><div><i class="iconfont bg_yellow">&#xe709;</i><span class="iconame">进店量</span><span id="comeAmount"></span></div></td>
                <td><div><i class="iconfont bg_orange">&#xe657;</i><span class="iconame">接待人数</span><span id="receiveAmount"></span></div></td>
                <td><div><i class="iconfont bg_gray">&#xe644;</i><span class="iconame">接待率</span><span id="receiveRate"></span></div></td>
                <td><div><i class="iconfont bg_violet">&#xe63d;</i><span class="iconame">试穿人数</span><span id="tryAmount"></span></div></td>
                <td><div><i class="iconfont bg_blue">&#xe644;</i><span class="iconame">试穿率</span><span id="tryRate"></span></div></td>
            </tr>
            <tr>
            	<td style="border-left: medium none;"><div><i class="iconfont bg_red">&#xe651;</i><span class="iconame">成交单数</span><span id="dealCount"></span></div></td>
            	<td><div><i class="iconfont bg_yellow">&#xe650;</i><span class="iconame">成交率</span><span id="dealRate"></span></div></td>
                <td><div><i class="iconfont bg_orange">&#xe64c;</i><span class="iconame" id="HuanBiLastWeekSpan">环比上周一</span><span id="HuanBiLastWeek"></span></div></td>
                <td><div><i class="iconfont bg_gray">&#xe64f;</i><span class="iconame">同比去年</span><span id="TongBiLastYear"></span></div></td>
                <td><div><i class="iconfont bg_violet">&#xe8df;</i><span class="iconame">环比昨日</span><span id="HuanBiYesterday"></span></div></td>
                <td><div><i class="iconfont bg_blue">&#xe6b0;</i><span class="iconame">平均折扣</span><span id="avg_Rate"></span></div></td>
            </tr>
            <tr>
            	<td style="border-left: medium none;"><div><i class="iconfont bg_red">&#xe911;</i><span class="iconame">平均件单价</span><span id="avgPrice"></span></div></td>
            	<td><div><i class="iconfont bg_yellow">&#xe640;</i><span class="iconame">最高客单价</span><span id="max_sellMoney"></span></div></td>
                <td><div><i class="iconfont bg_orange">&#xe619;</i><span class="iconame">平均客单价</span><span id="avgSellPrice"></span></div></td>
                <td><div><i class="iconfont bg_gray">&#xe629;</i><span class="iconame">连带率</span><span id="jointRate"></span></div></td>
                <td><div><i class="iconfont bg_violet">&#xe622;</i><span class="iconame">回头率</span><span id="backRate"></span></div></td>
                <td><div><i class="iconfont bg_blue">&#xe636;</i><span class="iconame">新增会员</span><span id="newVipCount"></span></div></td>
            </tr>
         </table>
    </div>
 </div>

</div>
<div class="actdiv">
	<div class="evetop">
		<span onclick="javascript:chageActivityMode('2');" id="lastWeekDayActMode">同期活动</span>
		<span class="cur" onclick="javascript:chageActivityMode('1');" id="todayActMode">此日活动</span>
	</div>
</div>
<div class="actTableDiv">
	<table id="todayAct" class="activity" cellpadding="0" cellspacing="0" border="0" width="260">
	</table>
	<table id="lastWeekDayAct" class="activity" cellpadding="0" cellspacing="0" border="0" style="display:none;" width="260">
	</table>
</div>

<div class="sidenav">
    <dl>
    	<dt>序号</dt><dd>统计项目</dd><dd>战报详情</dd>
    </dl>
</div>
<div class="side">
  <div class="sideinfo">
  	
    <dl>
      <dt>1</dt><dd>现金消费额</dd><dd><b id="sh_cash" name="sh_cash">0</b> 元</dd>
    </dl>
    <dl>
      <dt>2</dt><dd>储值卡消费</dd><dd><b id="sh_cd_money" name="sh_cd_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>3</dt><dd>代金券消费</dd><dd><b id="sh_vc_money" name="sh_vc_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>4</dt><dd>优惠券消费</dd><dd><b id="sh_ec_money" name="sh_ec_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>5</dt><dd>刷卡消费</dd><dd><b id="sh_bank_money" name="sh_bank_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>6</dt><dd>商场卡消费</dd><dd><b id="sh_mall_money" name="sh_mall_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>7</dt><dd>微信消费</dd><dd><b id="sh_wx_money" name="sh_wx_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>8</dt><dd>支付宝消费</dd><dd><b id="sh_ali_money" name="sh_ali_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>9</dt><dd>抹零合计</dd><dd><b id="sh_lost_money" name="sh_lost_money">0</b> 元</dd>
    </dl>
    <dl>
      <dt>10</dt><dd>新增储值卡</dd><dd><b id="cardCount" name="cardCount">0</b></dd>
    </dl>
    <dl>
      <dt>11</dt><dd>新增代金券</dd><dd><b id="voucherCount" name="voucherCount">0</b></dd>
    </dl>
    <dl>
      <dt>12</dt><dd>新增优惠券</dd><dd><b id="ecouponCount" name="ecouponCount">0</b></dd>
    </dl>
    <dl>
      <dt>13</dt><dd>前台调出总计</dd><dd><b id="allocate_amount_out" name="allocate_amount_out">0</b></dd>
    </dl>
    <dl>
      <dt>14</dt><dd>前台调入总计</dd><dd><b id="allocate_amount_in" name="allocate_amount_in">0</b></dd>
    </dl>
    <dl>
      <dt>15</dt><dd>今日会员生日</dd><dd><b id="vipBirthday" name="vipBirthday">0</b></dd>
    </dl>
  </div>
</div>
<script>
document.getElementById("everydayTable").style.height= document.getElementById("everydayDiv").offsetHeight+"px";
</script>
<script src="<%=basePath%>data/index/daily_report.js"></script>
</body>
</html>

