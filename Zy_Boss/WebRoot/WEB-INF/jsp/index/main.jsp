<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/main.css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/tab.css"/>
<link type="text/css" rel="Stylesheet" href="<%=basePath%>resources/css/skin/steady.css" id="cssfile" />
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
</script>
<script type="text/javascript">
function doLogout(){
	$.dialog.confirm('确定退出吗?', function(){
		window.parent.location.replace("<%=basePath%>logout");
	});
}
/**系统配置*/
var CONFIG={
	BASEPATH:'<%=basePath%>',						//项目路径
	SERVERPATH:'<%=serverPath%>',					//项目路径
	TODAY:'${requestScope.today}',
	PRINT_NAME:'\u53a6\u95e8\u667a\u6167\u6570\u7f51\u7edc\u79d1\u6280\u6709\u9650\u516c\u53f8',
	PRINT_KEY:'2CCC74198DEF8B1D8D08C1CF3F7E04F2',
	BASEROWNUM:15,									//基本资料分页条数
	BASEROWLIST:[15,30,50],							//基本资料分页条数
	DATAROWNUM:100,									//单据分页条数
	DATAROWLIST:[100,200,300],						//单据分页条数
	OPTIONLIMIT:{PRINT:'P',INSERT:'I',DELETE:'D',UPDATE:'U',SELECT:'S',EXPORT:'E',APPROVE:'A',FINISH:'F',IMPORT:'R',BACK:'B'},
	PRICELIMIT:{COST:'C',SELL:'S',DISTRIBUTE:'D',VIP:'V',BATCH:'W',ENTER:'E',PROFIT:'P'},
	SHOP_TYPES:[],//应该动态生成
	SHOP_TYPES_QUERY:[]
};
/**系统参数*/
var SYSTEM={
	MENULIMIT:eval('(${sessionScope.menulimit})'),
	PRICELIMIT:'${sessionScope.user.us_limit}',
	COMPANYCODE:'${sessionScope.user.co_code}',
	SHOP_TYPE:'${sessionScope.user.shoptype}',
	USERS:'${sessionScope.user.co_users}',
	SHOPS:'${sessionScope.user.co_shops}',
	INIT:'${sessionScope.user.sp_init}',
	VERSION:'${sessionScope.user.co_ver}',
	SHOP_CODE:'${sessionScope.user.us_shop_code}',
	US_ID:'${sessionScope.user.us_id}',
	US_CODE:'${sessionScope.user.us_code}',
	US_END:'${sessionScope.user.us_end}',
	US_PAY:'${sessionScope.user.us_pay}',
	US_DEFAULT:'${sessionScope.user.us_default}',
	US_RO_CODE:'${sessionScope.user.us_ro_code}',
	SHOP_UPCODE:'${sessionScope.user.shop_upcode}',
	USEABLE:'${sessionScope.sysset.st_useable}'
};
$(function(){
	$.ajax({
		type:"POST",
		url:CONFIG.BASEPATH+'common/type/list',
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				var pdata = [],qdata=[{id:"",name:"无"}];
				for(key in data.data){
					var p = {};
					p.id=data.data[key].ty_id;
					p.name=data.data[key].ty_name;
					pdata.push(p);
					qdata.push(p);
				}
				CONFIG.SHOP_TYPES = pdata;
				CONFIG.SHOP_TYPES_QUERY = qdata;
			}
		}
	});
});
</script>
<title>智慧数在线平台</title>
</head>
<body id="info_body">
  <div class="divMain">
	<div class="divLeft">
        <ul class="left-menu">
        	<c:forEach var="menu" items="${sessionScope.menu}" varStatus="as">
        		<c:if test="${menu.mn_child == 'Y' && menu.mn_app == '0'}">
					<li><span class="list" dataLink='info?mn_code=${menu.mn_code}' jerichotabindex='${menu.mn_code}'><i class='iconfont'>${menu.mn_icon}</i><b>${menu.mn_name}</b></span></li>
        		</c:if>
        	</c:forEach>
		</ul>
    </div>
    <div class="divTop">
    	<div class="CompanyData">
          <a href="javascript:void(0)">
        	<span class="CompanyName">(${sessionScope.user.co_code})${sessionScope.user.companyname}[${sessionScope.user.us_name}]</span>
            <span class="UserName"></span>
          </a>
        </div>
        <ul class="ext">
        	<li>
        	<span dataType="iframe" class="list" dataLink="home"><b>首页</b></span>
        	</li>
        	<li>
        	<span dataType="iframe" class="list" dataLink="home/to_daily_report"><b>每日战报</b></span>
        	</li>
        	<li>
        	<span dataType="iframe" class="list" dataLink="sys/workbench/to_workbench_my"><b>工作台</b></span>
        	</li>
            <li><span><a href="javascript:void(0);" onclick="javascript:doLogout();">退出</a></span></li>
        </ul>
    </div>
    <div class="divRight">
    </div>
  </div>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/tab.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>data/index/main.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
</script>
</body>
</html>