<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ac_id" name="ac_id" value="${access.ac_id }"/>
<input type="hidden" id="ac_number" name="ac_number" value="${access.ac_number }"/>
<input type="hidden" id="ac_ar_state" name="ac_ar_state" value="${access.ac_ar_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div >
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">经办人员：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="ac_manager" id="ac_manager" value="${access.ac_manager }"/>
			</td>
			<td align="right" width="60px">制单日期：</td>
			<td width="165px">
				<input readonly type="text" class="main_Input w146"  name="ac_date" id="ac_date" value="${access.ac_date }" />
			</td>
			<td align="right" width="60px">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="ac_money" value="${access.ac_money }" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">单据编号：</td>
			<td>
				<input class="main_Input w146" type="text" value="${access.ac_number }" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" style="width:390px" type="text" name="ac_remark" id="ac_remark" value="${access.ac_remark }" readonly="readonly"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/money/access/access_view.js"></script>
</body>
</html>