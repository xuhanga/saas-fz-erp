<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%">
		<tr class="list first">
			<td align="right" width="100px"><font color="red">*</font>账户名称：</td>
			<td>
				<input class="main_Input" type="text" name="ba_name" id="ba_name" value="" maxlength="30"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right" width="100px"><font color="red">*</font>期初余额：</td>
			<td>
				<input class="main_Input" type="text" name="ba_init_balance" id="ba_init_balance" value="" maxlength="10"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">备注：</td>
			<td>
				<input class="main_Input" type="text" name="ba_remark" id="ba_remark" value="" maxlength="100"/>
			</td>
		</tr>
		<tr class="list last">
			<td id="errorTip" class="errorTip" colspan="2" style="padding-left: 80px;"></td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				ba_name : "required",
				ba_init_balance : {
					required : true,
					number : true,
					min:0
				}
			},
			messages : {
				ba_name : "请输入账户名称",
				ba_init_balance : {
					required : "请输入期初余额",
					number : "期初余额必须为数字",
					min : "期初余额必须为正数"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/money/bank/bank_add.js"></script>
</body>
</html>