<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
<div class="mod-search cf">
	    <div class="fl">
	      <div id="filter-menu" class="ui-btn-menu fl">
		  	 	<span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
			  	 <div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td id="date">日期选择：
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>
										<td>银行账户：
										  	<input class="main_Input" type="text" id="ba_name" name="ba_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank();"/>
										     <input type="hidden" name="ba_code" id="ba_code" value=""/>
								  		</td>
									</tr>
									<tr>
										<td>经办人员：
										  	<input class="main_Input" type="text" readonly="readonly" name="br_manager" id="br_manager" value="" style="width:170px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
								  		</td>
									</tr>
									<tr>
						        	<td>类型：
							        	<span id="type_all"><label class="chk" style="margin-top:0px; " title="全部">全部<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_1"><label class="chk" style="margin-top:0px; " title="供应商预付款">供应商预付款<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_2"><label class="chk" style="margin-top:0px; " title="客户预收款">客户预收款<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_3"><label class="chk" style="margin-top:0px; " title="店铺预收款">店铺预收款<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_4"><label class="chk" style="margin-top:0px; " title="供应商结算单">供应商结算单<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_5"><label class="chk" style="margin-top:0px; " title="客户结算单">客户结算单<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_6"><label class="chk" style="margin-top:0px; " title="店铺结算单">店铺结算单<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_7"><label class="chk" style="margin-top:0px; " title="工资支出">工资支出<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_8"><label class="chk" style="margin-top:0px; " title="其他费用支出">其他费用支出<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_9"><label class="chk" style="margin-top:0px; " title="其他收入">其他收入<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_10"><label class="chk" style="margin-top:0px; " title="期初金额">期初金额<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_11"><label class="chk" style="margin-top:0px; " title="代金券发放">代金券发放<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_12"><label class="chk" style="margin-top:0px; " title="储值卡充值">储值卡充值<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_13"><label class="chk" style="margin-top:0px; " title="银行存取款">银行存取款<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_14"><label class="chk" style="margin-top:0px; " title="储值卡发放">储值卡发放<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_15"><label class="chk" style="margin-top:0px; " title="储值卡减值">储值卡减值<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_16"><label class="chk" style="margin-top:0px; " title="日结现金流向">日结现金流向<input type="checkbox" name="box" checked="checked"/></label></span>
							            <span id="type_17"><label class="chk" style="margin-top:0px; " title="物流公司结算单">物流公司结算单<input type="checkbox" name="box" checked="checked"/></label></span>
							              <input type="hidden" id="type" name="type" value=""/>
						             </td>
						        </tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
	    </div>
	    <div class="fl-m">
			<a class="ui-btn fl mrb" id="btn-search">查询</a>
		</div>
	    <div class="fr">
	    </div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/money/bank/bank_report_sum.js"></script>
</body>
</html>