<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="type" id="type" value="${type }"/>
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
   			<c:choose>
				<c:when test="${ type eq 'shop'}">
					店铺名称：
		   			<input class="main_Input" type="text" readonly="readonly" name="shop_name" id="shop_name" value="" style="width:170px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
					<input type="hidden" id="shopCode" name="shopCode" value=""/>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
			年份
			<span class="ui-combo-wrap" id="span_year"></span>
			<input type="hidden" name="year" id="year"/>
	   		
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<c:choose>
	    		<c:when test="${ type eq 'head'}">
	    			<span style="color:red;">
					1.业务收入=（门店的配送总金额+批发配送总金额）-（门店退货总金额+批发退货金额）此处门店含(加盟店+合伙店)<br>
					2.业务成本=供应商进货总金额-供应商退货总金额
					</span>
				</c:when>
				<c:when test="${ type eq 'shop'}">
					<span style="color:red;">
					1.业务收入=门店的零售总金额-零售退货-零售换货<br>
					2.业务成本=门店的零售成本
					</span>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
	    
	    
	    
	    
	    
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
	<div class="grid-wrap" style="float:left;margin-left:5px;">
	    <table id="detailGrid">
	    </table>
    	<div id="detailPage"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/money/expense/report_expense.js"></script>
</body>
</html>