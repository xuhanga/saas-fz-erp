<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ep_id" name="ep_id" value="${expense.ep_id }"/>
<input type="hidden" id="ep_number" name="ep_number" value="${expense.ep_number }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存单据"/>
	        <input id="btnClear" class="t-btn btn-red" type="button" value="清除"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div >
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">店铺名称：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="shop_name" id="shop_name" value="${expense.shop_name }" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
				<input type="hidden" name="ep_shop_code" id="ep_shop_code" value="${expense.ep_shop_code }" />
			</td>
			<td align="right" width="60px">银行账户：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="ba_name" id="ba_name" value="${expense.ba_name }" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank();"/>
				<input type="hidden" name="ep_ba_code" id="ep_ba_code" value="${expense.ep_ba_code }" />
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="ep_manager" id="ep_manager" value="${expense.ep_manager }" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right" width="60px">所属部门：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="dm_name" id="dm_name" value="${expense.dm_name }"/>
				<input type="hidden" name="ep_dm_code" id="ep_dm_code" value="${expense.ep_dm_code }" />
			</td>
		</tr>
		<tr class="list last">
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="ep_date" id="ep_date" onclick="WdatePicker()" value="${expense.ep_date }" />
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="ep_money" value="" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input w146" type="text" name="ep_remark" id="ep_remark" value="${expense.ep_remark }"/>
			</td>
			<td id="td_share" align="right">
              <label class="chk" style="margin-top:0px; " title="按月分摊">
                <input type="checkbox" name="box" ${expense.ep_share eq 1 ? 'checked' : '' }/>
              </label>
              <input type="hidden" id="ep_share" name="ep_share" value="${expense.ep_share }"/>
            </td>
			<td align="left">按月分摊<span style="color:red;"></span></td>
		</tr>
	</table>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				ep_shop_code : "required",
				ep_ba_code : "required",
				ep_manager : "required"
			},
			messages : {
				ep_shop_code : "请选择店铺",
				ep_ba_code : "请选择银行账户",
				ep_manager : "请选择经办人"
			}
		});
	});
</script>
<script src="<%=basePath%>data/money/expense/expense_add.js"></script>
</body>
</html>