<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ep_id" name="ep_id" value="${expense.ep_id }"/>
<input type="hidden" id="ep_number" name="ep_number" value="${expense.ep_number }"/>
<input type="hidden" id="ep_ar_state" name="ep_ar_state" value="${expense.ep_ar_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div >
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">店铺名称：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="shop_name" id="shop_name" value="${expense.shop_name }"/>
				<input type="hidden" name="ep_shop_code" id="ep_shop_code" value="${expense.ep_shop_code }" />
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="ep_manager" id="ep_manager" value="${expense.shop_name }"/>
			</td>
			<td align="right" width="60px">银行账户：</td>
			<td>
				<input class="main_Input w146" type="text" readonly="readonly" name="ba_name" id="ba_name" value="${expense.ba_name }"/>
				<input type="hidden" name="ep_ba_code" id="ep_ba_code" value="${expense.ep_ba_code }" />
			</td>
		</tr>
		<tr class="list last">
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="ep_money" value="${expense.ep_money }" readonly="readonly"/>
			</td>
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="ep_date" id="ep_date" value="${expense.ep_date }" />
			</td>
			<td align="right">备注：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  name="ep_remark" id="ep_remark" value="${expense.ep_remark }" />
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/money/expense/expense_view.js"></script>
</body>
</html>