<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存单据"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div >
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%" id="table_entire">
		<tr>
			<td width="60" align="right">优惠金额：</td>
			<td width="165">
                <input type="text" id="discount_money" name="discount_money" class="main_Input w146" value="0.00" maxlength="10"
                		onkeypress="javascript:return onlyDoubleNumber(this,event);" onblur="return valDoubleNumber(this);"/>
            </td>
            <td width="60" align="right">实付金额：</td>
            <td width="165">
                <input type="text" name="paid" id="paid" class="main_Input w146" value="0.00" maxlength="10"
                		onkeypress="javascript:return onlyDoubleNumber(this,event,true);" onblur="return valDoubleNumber(this,true);"/>
            </td>
            <td id="AutoMatchTd">
              <a href="javascript:void(0);" id="AutoMatch" class="t-btn btn-green">自动分配</a>
            </td>
			
		</tr>
	</table>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">物流公司：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="stream_name" id="stream_name" value="${stream.se_name }"/>
				<input type="hidden" name="st_stream_code" id="st_stream_code" value="${stream.se_code }" />
			</td>
			<td align="right" width="60px">银行账户：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="ba_name" id="ba_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank();"/>
				<input type="hidden" name="st_ba_code" id="st_ba_code" value="" />
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="st_manager" id="st_manager" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right" width="60px">付款日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="st_date" id="st_date" onclick="WdatePicker()" value="" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">实际欠款：</td>
			<td>
				<input class="main_Input w146" type="text" id="realdebt" disabled="disabled"
					value="<fmt:formatNumber value='${stream.se_payable - stream.se_payabled}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">优惠金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="st_discount_money" value="0.00" disabled="disabled"/>
			</td>
			<td align="right">实际付款：</td>
			<td>
				<input class="main_Input w146" type="text" id="st_paid" value="0.00" disabled="disabled"/>
			</td>
			<td align="right">剩余欠款：</td>
			<td>
				<input class="main_Input w146" type="text" id="leftdebt" value="0.00" disabled="disabled"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" style="width:390px" type="text" name="st_remark" id="st_remark" value=""/>
			</td>
		</tr>
	</table>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				st_ba_code : "required",
				st_manager : "required"
			},
			messages : {
				st_ba_code : "请选择银行账户",
				st_manager : "请选择经办人"
			}
		});
	});
</script>
<script src="<%=basePath%>data/money/streamsettle/streamsettle_add.js"></script>
</body>
</html>