<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="CurrentMode" value="0"/>
<div class="wrapper">
    <div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>销售时间</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>
									  <td align="right">店铺名称：</td>
									  <td>
									  	<input class="ui-input" style="width:174px;" type="text" readonly="readonly" name="shop_name" id="shop_name" value="" />
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
										<input type="hidden" id="shl_shop_code" name="shl_shop_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品品牌：</td>
									  <td>
									  	<input class="ui-input" style="width:174px;" type="text" readonly="readonly" name="bd_name" id="bd_name" value="" />
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBrand();"/>
										<input type="hidden" id="bd_code" name="bd_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品类别：</td>
									  <td>
									  	<input class="ui-input" style="width:174px;" type="text" readonly="readonly" name="tp_name" id="tp_name" value="" />
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryType();"/>
										<input type="hidden" id="tp_code" name="tp_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品年份：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_year" style="width: 206px;height: 30px;"></span>
          								<input type="hidden"  name="pd_year"  id="pd_year"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品季节：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_season"></span>
										<input type="hidden" name="pd_season" id="pd_season"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品款式：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_style"></span>
										<input type="hidden" name="pd_style" id="pd_style"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品货号：</td>
									  <td>
									  	<input class="main_Input" type="text" name="pd_no" id="pd_no" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">最低销量：</td>
									  <td>
									  	<input class="main_Input" type="text" name="amount" id="amount" value="5"/>
							  		</td>
								  </tr>
								  <tr>
			                      	   <td align="right">上市时间：</td>
			                      	   <td>
				                        	<span id="chkss_day">
							              		<label class="chk" style="margin-top:0px; ">
							                		<input type="checkbox" name="box"/>
							              		</label>
							            	</span>
				                             	小于或等于<input type="text" class="main_Input" style="width:50px; text-align:right;" onkeyup="Utils.checkObject(this,2)" onfocus="this.select();" id="ss_day" name="ss_day" maxlength="4" value="90"/>天
			                        	</td>
					              </tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<a href="javascript:void(0);" class="t-btn btn-bl on " id="sum_mode_button" onclick="javascript:Utils.change_mode(0);">汇总模式</a>
        	<a href="javascript:void(1);" class="t-btn btn-bc " id="list_mode_button" onclick="javascript:Utils.change_mode(1);">明细模式</a>
	    </div> 
	 </div>
    <div class="grid-wrap" id="sum_mode" style="display:'';">
        <table id="grid">
        </table>
        <div id="page"></div>
    </div>
    <div class="grid-wrap" id="list_mode" style="display:none;">
        <table id="list_grid">
        </table>
        <div id="list_page"></div>
    </div>
</div>
</form>
<script src="<%=basePath%>data/report/sell_max.js"></script>
</body>
</html>