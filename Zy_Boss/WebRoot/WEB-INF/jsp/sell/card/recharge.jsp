<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="cd_id" name="cd_id" value="${card.cd_id }"/>
<div class="border">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">储值卡号：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="cdl_cardcode" id="cdl_cardcode" value="${card.cd_cardcode }" style="width:100px;"/>
						<span id="span_cdl_type">
							<label class="radio" style="width:30px">
				  	 			<input name="redio1" type="radio" value="1" checked="checked"/>充值
					  	 	</label>
					  	 	<label class="radio" style="width:30px">
					  	 		<input name="redio1" type="radio" value="2" />减值
					  	 	</label>
							<input type="hidden" id="cdl_type" name="cdl_type" value="1"/>
						</span>
					</td>
					<td align="right" width="80px">卡上余额：</td>
					<td>
						<input readonly type="text" class="main_Input" value="${card.cd_money }" />
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b><span name="label1">充</span>值金额：</td>
					<td>
						<input class="main_Input" type="text" name="cdl_money" id="cdl_money" value=""/>
					</td>
					<td align="right"><span name="label1">充</span>值日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate"  name="cdl_date" id="cdl_date" onclick="WdatePicker()" value="" style="width:198px;"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>实<span name="label2">收</span>现金：</td>
					<td>
						<input class="main_Input" type="text" name="cdl_realcash" id="cdl_realcash" value=""/>
					</td>
					<td align="right"><b>*</b>实<span name="label2">收</span>刷卡：</td>
					<td>
						<input class="main_Input" type="text" name="cdl_bankmoney" id="cdl_bankmoney" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>现金账户：</td>
					<td>
						<input class="main_Input" type="text" readonly="readonly" name="ba_name" id="ba_name" value="" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank('cdl_ba_code','ba_name');"/>
						<input type="hidden" name="cdl_ba_code" id="cdl_ba_code" value=""/>
					</td>
					<td align="right"><b>*</b>刷卡账户：</td>
					<td>
						<input class="main_Input" type="text" readonly="readonly" name="bank_name" id="bank_name" value="" style="width:170px;"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBank('cdl_bank_code','bank_name');"/>
						<input type="hidden" name="cdl_bank_code" id="cdl_bank_code" value=""/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td id="errorTip" class="errorTip" colspan="3"></td>
				</tr>
			</table>
</div>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				cdl_money : {
					required : true,
					digits:true,
					min:1
				},
				cdl_realcash : {
					required : true,
					digits:true
				},
				cdl_bankmoney : {
					required : true,
					digits:true
				},
				cdl_ba_code : {
					required : true
				},
				cdl_bank_code : {
					required : true
				}
			},
			messages : {
				cdl_money : {
					required : "请输入充值金额",
					digits : "请输入正整数"
				},
				cdl_realcash : {
					required : "请输入实收现金",
					digits : "请输入正整数"
				},
				cdl_bankmoney : {
					required : "请输入实收刷卡",
					digits : "请输入正整数"
				},
				cdl_ba_code : "请选择现金账户",
				cdl_bank_code : "请选择刷卡账户"
			}
		});
	});
</script>
<script src="<%=basePath%>data/sell/card/card_recharge.js"></script>
</body>
</html>