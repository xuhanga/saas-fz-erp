<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:35px; }
	/* .main_Input{width: 130px; height: 16px;} */
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/lundarDate.js\"></sc"+"ript>");
</script>
<script>
var api = frameElement.api, W = api.opener;
var _callback = api.data.callback;
function doClose(){
	api.close();
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="cd_id" name="cd_id" value="${card.cd_id }"/>
<div class="border">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">储值卡号：</td>
					<td>
						<input class="main_Input" type="text" name="cd_cardcode" id="cd_cardcode" value="${card.cd_cardcode }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">有效日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate"  name="cd_enddate" id="cd_enddate" onclick="WdatePicker()" value="${card.cd_enddate }" style="width:198px;"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">用户姓名：</td>
					<td>
						<input class="main_Input" type="text" name="cd_name" id="cd_name" value="${card.cd_name }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">手机号码：</td>
					<td>
						<input class="main_Input" type="text" name="cd_mobile" id="cd_mobile" value="${card.cd_mobile }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">身份证号：</td>
					<td>
						<input class="main_Input" type="text" name="cd_idcard" id="cd_idcard" value="${card.cd_idcard }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">总计金额：</td>
					<td>
						<input class="main_Input" type="text" name="cd_money" id="cd_money" value="${card.cd_money }" disabled="disabled"/>
					</td>
					
				</tr>
				<tr class="list">
					<td align="right">剩余金额：</td>
					<td>
						<input class="main_Input" type="text" value="${card.cd_money-card.cd_used_money }" disabled="disabled"/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td id="errorTip" class="errorTip" colspan="3"></td>
				</tr>
			</table>
</div>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				cd_mobile : {
					isMobile : true
				}
			},
			messages : {
				cd_mobile : "请输入正确的手机号码"
			}
		});
	});
</script>
<script src="<%=basePath%>data/sell/card/card_update.js"></script>
</body>
</html>