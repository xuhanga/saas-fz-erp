<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="ca_id" id="ca_id" value="${model.ca_id}"/>
<div class="border">
	<table class="table_add" style="width:95%;margin: 5px;">
		<tr class="list first">
			<td>
				<a id="chk_cash" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>收银权限
					</label>
				</a>
			</td>
			<td>
				<a id="chk_back" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>退货功能
					</label>
				</a>
			</td>
			<td>
				<a id="chk_change" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>换货功能
					</label>
				</a>
			</td>
			<td>
				<a id="chk_set" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>设置功能
					</label>
				</a>
			</td>
		</tr>
		<tr class="list">
			<td>
				<a id="chk_vip_manage" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>会员管理
					</label>
				</a>
			</td>
			<td>
				<a id="chk_vip_add" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>新增会员
					</label>
				</a>
			</td>
			<td>
				<a id="chk_point_change" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>积分兑换
					</label>
				</a>
			</td>
			<td>
				<a id="chk_gift" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>礼品发放
					</label>
				</a>
			</td>
		</tr>
		<tr class="list">
			<td>
				<a id="chk_vip_edit" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>会员修改
					</label>
				</a>
			</td>
			<td>
				<a id="chk_product_query" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>商品查询
					</label>
				</a>
			</td>
			<td>
				<a id="chk_product_stock" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>商品库存
					</label>
				</a>
			</td>
			<td>
				<a id="chk_bank_run" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>银行收支
					</label>
				</a>
			</td>
		</tr>
		<tr class="list">
			<td>
				<a id="chk_voucher_query" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>代金券查询
					</label>
				</a>
			</td>
			<td>
				<a id="chk_voucher_add" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>新增代金券
					</label>
				</a>
			</td>
			<td>
				<a id="chk_card_query" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>储值卡查询
					</label>
				</a>
			</td>
			<td>
				<a id="chk_card_add" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>新增储值卡
					</label>
				</a>
			</td>
		</tr>
		<tr class="list">
			<td>
				<a id="chk_vip_point" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>积分管理
					</label>
				</a>
			</td>
			<td>
				<a id="chk_dayend" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>交班功能
					</label>
				</a>
			</td>
			<td>
				<a id="chk_sell_mobile" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>手机下单
					</label>
				</a>
			</td>
			<td>
				<a id="chk_card_charge" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>储值卡充值
					</label>
				</a>
			</td>
		</tr>
		<tr class="list">
			<td>
				<a id="chk_swap_manage" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>调拨管理
					</label>
				</a>
			</td>
			<td>
				<a id="chk_edit_pass" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>修改密码
					</label>
				</a>
			</td>
			<td>
				<a id="chk_sell_list" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>零售记录
					</label>
				</a>
			</td>
			<td>
				<a id="chk_put_up" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>挂单功能
					</label>
				</a>
			</td>
		</tr>
		<tr class="list">
			<td>
				<a id="chk_hand_rate" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>手动打折
					</label>
				</a>
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
			</td>
		</tr>
		<!-- <tr class="list">
			<td>
				<a id="chk_line_sell" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>线上交易
					</label>
				</a>
			</td>
			<td>
				<a id="chk_show_salve" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>副导购显示
					</label>
				</a>
			</td>
			<td>
				<a id="chk_show_area" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>位置显示
					</label>
				</a>
			</td>
			<td>
				<a id="chk_back_number" class="chkbox fl">
					<label class="chk">
						<input type="checkbox"/>小票退货
					</label>
				</a>
			</td>
		</tr> -->
	</table>
</div>
</form>
<div class="footdiv">
	<a id="chk_all" class="fl">
		<label class="chk">
			<input type="checkbox" id="btn_all"/>全选
		</label>
	</a>
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/sell/cashier/cashier_limit.js"></script>
</body>
</html>