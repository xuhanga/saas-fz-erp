<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:28px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="mainwra">
<div class="border" style="border-bottom:#ccc solid 1px;">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>

		<table width="100%">
			<tr class="list first">
				<td align="right" width="80px"><b>*</b>活动门店：</td>
				<td>
					<input class="main_Input" type="text" readonly="readonly" name="sc_shop_name" id="sc_shop_name" value="" style="width:170px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
					<input type="hidden" name="sc_shop_code" id="sc_shop_code" value="" />
				</td>
				<td align="right" width="80px"><b>*</b>方案名称：</td>
				<td width="130px">
					<input class="main_Input" type="text" name="sc_name" id="sc_name" value="" maxlength="30"/>
				</td>
				<td colspan="2"> 
	          		&nbsp;&nbsp;&nbsp;&nbsp;
	            	<span id="show_chat">
			        	<label class="chk" style="margin-top:0px; " title="是否在微商城显示" >是否在微商城显示
			            	<input type="checkbox" name="chatbox" checked="checked"/>
			            </label>
		        	</span>
		        	<input type="hidden" id="sc_show_chat" name="sc_show_chat" value=""/>
	          	</td>
			</tr>
			<tr class="list">
				<td align="right" width="80px"><b>*</b>发放数量：</td>
				<td width="130px">
					<input class="main_Input" type="text" name="sc_amount" id="sc_amount" value="0" onkeyup="javascript:handle.ifNumber(this);" onkeydown="javascript:handle.ifNumber(this);"/>
				</td>
				<td align="right">活动时间：</td>
				<td>
					<input readonly type="text" class="main_Input Wdate"  name="sc_begindate" id="sc_begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'sc_enddate\',{d:0})}'})" value="${begindate }" style="width:88px;"/>~
					<input readonly type="text" class="main_Input Wdate"  name="sc_enddate" id="sc_enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'sc_begindate\',{d:0})}'})" value="${enddate }" style="width:88px;"/>
				</td>
				<td colspan="2"> 
	          		&nbsp;&nbsp;&nbsp;&nbsp;
	            	<span id="show_sysinfo" >
			        	<label class="chk" style="margin-top:0px; " title="显示系统生成优惠券信息" >显示系统生成优惠券信息
			            	<input type="checkbox" name="box" checked="checked"/>
			            </label>
		        	</span>
		        	<input type="hidden" id="sc_show_sysinfo" name="sc_show_sysinfo" value=""/>
	          	</td>
			</tr>
			<tr class="list">
	        	<td align="right">面值：</td>
	        	<td>
	          		<input type="text" style="width:40px;" class="main_Input" id="sc_minus_money" name="sc_minus_money" value="0" onkeyup="javascript:handle.ifMoney(this);" onkeydown="javascript:handle.ifMoney(this);"/> 元
	         	</td>
	        	<td align="right">使用条件：</td>
	        	<td>
	         		满 <input type="text" style="width:40px;" class="main_Input" id="sc_full_money" name="sc_full_money" value="0" onkeyup="javascript:handle.ifMoney(this);" onkeydown="javascript:handle.ifMoney(this);"/> 元
	         	</td>
	         	<td> 
	         	</td>
	         	<td> 
	         	</td>
	      	</tr>
	      	<tr class="list">
	        	<td align="right">使用说明：</td>
	        	<td colspan="5">
	         		<textarea name="sc_remark" resize="none" id="sc_remark" rows="5" cols="100"></textarea>
	        	</td>
	      	</tr>
	      	<tr class="list last">
	      		<td align="right" height="35">范围选择：</td>
	        	<td colspan="5" id="sm_scope_div">
					<input type="radio" name="sc_type_radio" value="0" checked="checked" onclick="javascript:Utils.clickScType('0');"/>全场&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="sc_type_radio" value="1" onclick="javascript:Utils.clickScType('1');"/>类别&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="sc_type_radio" value="2" onclick="javascript:Utils.clickScType('2');"/>品牌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="sc_type_radio" value="3" onclick="javascript:Utils.clickScType('3');"/>商品 
					<input type="hidden" id="sc_type" value="0"/>
	        	</td>
	      	</tr>
		</table>
	</div>
	<div style="display:none;" id="detailDiv">
		<div class="grid-wrap">
			<table id="grid">
			</table>
			<div id="page"></div>
		</div>
	 </div>
</div>
</form>
<script src="<%=basePath%>data/sell/coupon/coupon_add.js"></script>
</body>
</html>