<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ecl_id" name="ecl_id" value="${temp.ecl_id }"/>
<input type="hidden" id="ec_type" name="ec_type" value="${ec_type }"/>
<div class="border">
	<table width="100%">
		<c:choose>
			<c:when test="${ec_type eq 0}"><!-- 线上领取 -->
				<tr class="list first">
					<td align="right" width="60px">满：</td>
					<td>
						<input class="main_Input" type="text" name="ecl_limitmoney" id="ecl_limitmoney" value="${temp.ecl_limitmoney }" maxlength="7"/>元
					</td>
				</tr>
				<tr class="list">
					<td align="right">减：</td>
					<td>
						<input class="main_Input" type="text" name="ecl_money" id="ecl_money" value="${temp.ecl_money }" maxlength="7"/>元
					</td>
				</tr>
				<tr class="list">
					<td align="right">发放数量：</td>
					<td>
						<input class="main_Input" type="text" name="ecl_amount" id="ecl_amount" value="${temp.ecl_amount }" maxlength="7"/>
						<input class="main_Input" type="hidden" name="ecl_usedmoney" id="ecl_usedmoney" value="${temp.ecl_usedmoney }" maxlength="7"/>
					</td>
				</tr>
			</c:when>
			<c:when test="${ec_type eq 1}"><!-- 收银发放 -->
				<tr class="list first">
					<td align="right" width="60px">消费：</td>
					<td>
						<input class="main_Input" type="text" name="ecl_usedmoney" id="ecl_usedmoney" value="${temp.ecl_usedmoney }" maxlength="7"/>元
						<input class="main_Input" type="hidden" name="ecl_amount" id="ecl_amount" value="${temp.ecl_amount }" maxlength="7"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">赠送：</td>
					<td>
						<input class="main_Input" type="text" name="ecl_money" id="ecl_money" value="${temp.ecl_money }" maxlength="7"/>元
					</td>
				</tr>
				<tr class="list">
					<td align="right">消费：</td>
					<td>
						<input class="main_Input" type="text" name="ecl_limitmoney" id="ecl_limitmoney" value="${temp.ecl_limitmoney }" maxlength="7"/>元可使用
					</td>
				</tr>
			</c:when>
			<c:otherwise></c:otherwise>
		</c:choose>
		<tr class="list last">
			<td align="right">备注：</td>
			<td>
				<input class="main_Input" type="text" name="ecl_remark" id="ecl_remark" value="${temp.ecl_remark }" maxlength="100"/>
			</td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/sell/ecoupon/ecoupon_temp_add.js"></script>
</body>
</html>