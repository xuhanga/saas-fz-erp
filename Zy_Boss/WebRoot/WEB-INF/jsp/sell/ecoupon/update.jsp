<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:28px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ec_id" name="ec_id" value="${eCoupon.ec_id }"/>
<input type="hidden" id="ec_number" name="ec_number" value="${eCoupon.ec_number }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" name="btn-save" class="t-btn btn-red" type="button" value="保存"/>
	        <input id="btn_close" name="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>

		<table width="100%">
			<tr class="list first">
				<td align="right" width="70px"><b>*</b>方案名称：</td>
				<td width="160px">
					<input class="main_Input w120" type="text" name="ec_name" id="ec_name" value="${eCoupon.ec_name }" maxlength="30"/>
				</td>
				<td align="right" width="70px">方案级别：</td>
				<td width="160px">
					<span class="ui-combo-wrap" id="span_level">
					</span>
					<input type="hidden" name="ec_level" id="ec_level" value="${eCoupon.ec_level }"/>
				</td>
				<td align="right" width="70px"><b>*</b>经办人员：</td>
				<td>
					<input class="main_Input" type="text" readonly="readonly" name="ec_manager" id="ec_manager" value="${eCoupon.ec_manager }" style="width:96px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
				</td>
			</tr>
			<tr class="list">
				<td align="right"><b>*</b>活动门店：</td>
				<td>
					<input class="main_Input" type="text" readonly="readonly" name="shop_names" id="shop_names" value="${eCoupon.shop_names }" title="${eCoupon.shop_names }" style="width:96px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
					<input type="hidden" name="shop_codes" id="shop_codes" value="${eCoupon.shop_codes }" />
				</td>
				<td align="right">有效天数：</td>
				<td>
					<input class="main_Input w120" type="text" name="ec_deadline" id="ec_deadline" value="${eCoupon.ec_deadline }" maxlength="4"/>
				</td>
				<td align="right">发放方式：</td>
				<td>
					<span class="ui-combo-wrap" id="spanType"></span>
					<input type="hidden" name="ec_type" id="ec_type" value="${eCoupon.ec_type}"/>
				</td>
			</tr>
			<tr class="list last">
				<td align="right">活动时间：</td>
				<td colspan="2">
					<input readonly type="text" class="main_Input Wdate"  name="ec_begindate" id="ec_begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'ec_enddate\',{d:0})}'})" value="${eCoupon.ec_begindate }" style="width:88px;"/>~
					<input readonly type="text" class="main_Input Wdate"  name="ec_enddate" id="ec_enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'ec_begindate\',{d:0})}'})" value="${eCoupon.ec_enddate }" style="width:88px;"/>
				</td>
				<td align="right">备注：</td>
				<td colspan="2">
					<input class="main_Input" style="width:200px;" type="text" name="ec_remark" id="ec_remark" value="${eCoupon.ec_remark }"/>
				</td>
			</tr>
			<tr class="list" id="modeTr">
				<td align="right">发放条件：</td>
				<td id="modeTd" colspan="2">
					<label class="radio" style="width:30px">
		  	 			<input name="ec_mode_" type="radio" value="1" onclick="javascript:modeClick(this);" ${eCoupon.ec_mode==1?"checked":"" }/>全场
			  	 	</label>
			  	 	<label class="radio" style="width:30px">
			  	 		<input name="ec_mode_" type="radio" value="2" onclick="javascript:modeClick(this);" ${eCoupon.ec_mode==2?"checked":"" }/>品牌
			  	 	</label>
			  	 	<label class="radio" style="width:30px">
			  	 		<input name="ec_mode_" type="radio" value="3" onclick="javascript:modeClick(this);" ${eCoupon.ec_mode==3?"checked":"" }/>类别
			  	 	</label>
			  	 	<label class="radio" style="width:30px">
			  	 		<input name="ec_mode_" type="radio" value="4" onclick="javascript:modeClick(this);" ${eCoupon.ec_mode==4?"checked":"" }/>商品
			  	 	</label>
					<input type="hidden" id="ec_mode" name="ec_mode" value="${eCoupon.ec_mode }"/>
				</td>
				<td colspan="3" id="mode_code_td">
					<span id="mode_label"></span>
					<span id="mode_names">${eCoupon.mode_names }</span>
					<input type="hidden" id="mode_codes" name="mode_codes" value="${eCoupon.mode_codes }"/>
				</td>
			</tr>
			<tr class="list">
				<td align="right">使用范围：</td>
				<td  id="use_modeTd" colspan="2">
					<label class="radio" style="width:30px">
		  	 			<input name="ec_use_mode_" type="radio" value="1" onclick="javascript:usemodeClick(this);" ${eCoupon.ec_use_mode==1?"checked":"" }/>全场
			  	 	</label>
			  	 	<label class="radio" style="width:30px">
			  	 		<input name="ec_use_mode_" type="radio" value="2" onclick="javascript:usemodeClick(this);" ${eCoupon.ec_use_mode==2?"checked":"" }/>品牌
			  	 	</label>
			  	 	<label class="radio" style="width:30px">
			  	 		<input name="ec_use_mode_" type="radio" value="3" onclick="javascript:usemodeClick(this);" ${eCoupon.ec_use_mode==3?"checked":"" }/>类别
			  	 	</label>
			  	 	<label class="radio" style="width:30px">
			  	 		<input name="ec_use_mode_" type="radio" value="4" onclick="javascript:usemodeClick(this);" ${eCoupon.ec_use_mode==4?"checked":"" }/>商品
			  	 	</label>
					<input type="hidden" id="ec_use_mode" name="ec_use_mode" value="${eCoupon.ec_use_mode }"/>
				</td>
				<td colspan="3" id="use_mode_code_td">
					<span id="use_mode_label"></span>
					<span id="use_mode_names">${eCoupon.use_mode_names }</span>
					<input type="hidden" id="use_mode_codes" name="use_mode_codes" value="${eCoupon.use_mode_codes }"/>
				</td>
			</tr>
		</table>
</div>
<div class="grid-wrap">
    <div id="list-grid">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>


</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				ec_name : "required",
				shop_codes : "required",
				ec_manager : "required",
				ec_begindate : "required",
				ec_enddate : "required",
				ec_deadline : {
					required : true,
					digits:true,
					min:1
				}
			},
			messages : {
				ec_name : "请输入方案名称",
				shop_codes : "请选择活动门店",
				ec_manager : "请选择经办人",
				ec_begindate : "请选择开始时间",
				ec_enddate : "请选择结束时间",
				ec_deadline : {
					required : "请输入有效天数",
					digits : "请输入正整数"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/sell/ecoupon/ecoupon_update.js"></script>
</body>
</html>