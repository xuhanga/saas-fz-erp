<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc" + "ript>");
    document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
</script>
</head>
<body>
<div class="wrapper">
    <div class="mod-search cf">
		 <div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
		   		 <span style="float:left">
		     		 <strong>供货商:</strong>
		     		 <input type="text" id="sp_name" class="main_Input atfer_select" readonly="readonly" value="${sp_name }"/>
			  	 </span>
			</div>
		</div>
    </div>
    <div class="grid-wrap">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div style="float:left;margin-top:5px; width:100%;">
		<span style="float:left;color:red;width:380px;line-height:30px;" id="spanDescribe">
			注：店铺-销售数量/库存数量/在途数量；仓库-库存数量
		</span>
		<span style="float:right;width:300px;text-align:right;">
	      	<a class="ui-btn mrb" style="display:none;" id="btnExit">关闭</a>
		</span>
	</div>
</div>
<script src="<%=basePath%>data/sell/report/query_stock_detail.js"></script>
</body>
</html>

