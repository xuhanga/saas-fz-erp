<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

<style>
    .upimgs{border:#ddd solid  1px;margin-top:10px;float:left;height:230px;width:330px;overflow:hidden;}
	.upimgshow{float:left;margin:5px 5px 0;height:185px;width:320px;overflow:hidden;}
	.upimgin{flaot:left;margin:5px 5px 0;height:35px;width:320px;overflow:hidden;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script>
<script src="<%=basePath%>resources/util/AutoResizeImg.js"></script>
<script src="<%=basePath%>resources/util/UpImgPreview.js"></script>
<script>
var api = frameElement.api, W = api.opener;
//返回
function doBack(){
	parent.childrenFlush();
	api.close();
}
</script>

</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="ftpURL" name="ftpURL" value=""/>
<input type="hidden" id="flush_falg" name="flush_falg" value=""/>
<input type="hidden" id="CurrentMode" value="0"/>
<div class="mainwra">
	<div class="border">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2" class="top-b border-b">
	            	<input type="button" id="btnSave" class="t-btn btn-red" onclick="javascript:return Utils.saveGift();" value="发布"/>
	            	<a class="t-btn"  onclick="javascript:doBack();">返回</a>
	                <span style="float:right;margin-right:10px;">
	                	<a id="sumModeButton" class="t-btn btn-bl"  onclick="javascript:doChangeMode(2);"><i></i>汇总</a>
           				<a id="listModeButton" class="t-btn btn-blc on"  onclick="javascript:doChangeMode(0);"><i></i>列表</a>
           				<a id="sizeModeButton" class="t-btn btn-bc" onclick="javascript:doChangeMode(1);"><i></i>尺码</a>
	                </span>
	        	</td>
			</tr>
			<tr class="list first">
				<td align="right" width="100">发布店铺：</td>
				<td>
					<input type="text" id="shop_name" name="shop_name" readonly="readonly" class="main_Input atfer_select w146" style="width:169px" value=""/>
					<input type="button" value="" class="btn_select" id="btnChooseStore" onclick="javascript:Utils.doQueryShop();"/>
					<input type="hidden" name="gil_shop_code" id="gil_shop_code" value=""/>
				</td>
			</tr>
			<tr class="list ">
	             <td align="right">货号查询：</td>
	             <td>
	                <input name="pd_no" style="width:169px" type="text" id="pd_no" class="main_Input" onkeydown="javascript:if (event.keyCode==13){return Utils.selectProduct()};" />
	                <input type="button"  class="btn_select" onclick="javascript:Utils.selectProduct();" /> 
	             </td>
			</tr>
			<tr class="list last">
				<td align="right">兑换日期：</td>
				<td> 
				  	<input type="text" class="main_Input Wdate" style="width:100px;" readonly="readonly" id="gi_begindate" name="gi_begindate"  value="${begindate}" 
				  onclick="WdatePicker({isShowToday:false,dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'gi_enddate\',{d:0})}'})"/> ~ 
					<input style="width:100px;" class="main_Input Wdate" readonly="readonly" id="gi_enddate" name="gi_enddate" value="${enddate}" 
				onclick="WdatePicker({isShowToday:false,dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'gi_begindate\',{d:0})}'})" />
				</td>
			</tr>
	     </table>
	</div>
	<div class="grid-wrap" id="detailDiv">
	  	<div id="listMode" style="display:'';">
	        <table id="grid"></table>
	        <div id="page"></div>
	    </div>
		<div id="sizeMode" style="display:none;">
	        <table id="sizeGroupGrid"></table>
	        <div id="sizeGroupPage"></div>
	    </div>
	    <div id="sumMode" style="display:none;">
	       <table id="sumGrid"></table>
	       <div id="sumPage"></div>
	   </div>
	</div>
</div>
</form>
<script>
	// 列表和尺码模式切换
	function doChangeMode(obj){
		var tbleListMode = document.getElementById("listMode");
		var tbleSizeMode = document.getElementById("sizeMode");
		var tbleSumMode = document.getElementById("sumMode");
		var listModeButton = document.getElementById("listModeButton");
		var sizeModeButton = document.getElementById("sizeModeButton");
		var sumModeButton = document.getElementById("sumModeButton");
		if (obj == 0){
			tbleListMode.style.display = '';
			tbleSizeMode.style.display = 'none';
			tbleSumMode.style.display = 'none';
			listModeButton.className='t-btn btn-blc on';
			sizeModeButton.className='t-btn btn-bc';
			sumModeButton.className = 't-btn btn-bl';
			tbleSizeMode.style.border = 0;
			Utils.ajaxGetGiftTempList();
			$("#CurrentMode").val(0);
		}else if (obj == 1){
			tbleListMode.style.display = 'none';
			tbleSizeMode.style.display = ''; 
			tbleSumMode.style.display = 'none';
			listModeButton.className='t-btn btn-blc';
			sizeModeButton.className='t-btn btn-bc on';
			sumModeButton.className = 't-btn btn-bl';
			Utils.ajaxGetGiftTempTitles();
			$("#CurrentMode").val(1);
		}else if(obj == 2){
			tbleListMode.style.display = 'none';
			tbleSizeMode.style.display = 'none';
			tbleSumMode.style.display = '';
			listModeButton.className = 't-btn btn-blc';
			sizeModeButton.className = 't-btn btn-bc';
			sumModeButton.className = 't-btn btn-bl on';
			Utils.ajaxGetGiftTempSumList();
			$("#CurrentMode").val(2);
		}
	}
	
</script>
</body>
<script src="<%=basePath%>data/shop/gift/gift_add.js"></script>
</html>

