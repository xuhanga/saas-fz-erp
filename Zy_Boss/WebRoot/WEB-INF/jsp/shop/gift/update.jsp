<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>

<style>
    .upimgs{border:#ddd solid  1px;margin-top:10px;float:left;height:230px;width:330px;overflow:hidden;}
	.upimgshow{float:left;margin:5px 5px 0;height:185px;width:320px;overflow:hidden;}
	.upimgin{flaot:left;margin:5px 5px 0;height:35px;width:320px;overflow:hidden;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script>
<script src="<%=basePath%>resources/util/AutoResizeImg.js"></script>
<script src="<%=basePath%>resources/util/UpImgPreview.js"></script>
<script>
var api = frameElement.api, W = api.opener;
function doBack(){
	/* parent.childrenFlush(); */
	api.close();
}
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="flush_falg" name="flush_falg" value=""/>
<input type="hidden" id="gi_id" name="gi_id" value="${gift.gi_id }"/>
<input type="hidden" id="ftpURL" name="ftpURL" value=""/>
<input type="hidden" id="gi_state" name="gi_state" value="${gift.gi_state }"/>
<input type="hidden" id="gi_pd_code" name="gi_pd_code" value="${gift.gi_pd_code }"/>
<input type="hidden" id="CurrentMode" value="0"/>
<div class="mainwra">
	<div class="border">
		<table width="100%"  cellspacing="0" cellpadding="0">
			<tr>
              	<td colspan="2" class="top-b border-b">
              		<input type="button" id="btnSave" class="t-btn btn-red" onclick="javascript:return Utils.saveUpdateGift();" value="修改"/>
                  	<a class="t-btn"  onclick="javascript:doBack();">返回</a>
                  	<span style="float:right;margin-right:10px;">
         				<a id="listModeButton" class="t-btn btn-bl on"  onclick="javascript:doChangeMode(0);"><i></i>列表</a>
         				<a id="sizeModeButton" class="t-btn btn-bc" onclick="javascript:doChangeMode(1);"><i></i>尺码</a>
               		</span>
              	</td>
          	</tr>
         	<tr class="list first">
		    	<td align="right" width="100">发布店铺：</td>
		    	<td>
		    		<input class="main_Input" disabled="disabled" type="text" id="sp_name" name="sp_name"  value="${gift.sp_name }"/>
					<input type="hidden" id="gi_shop_code" name="gi_shop_code" value="${gift.gi_shop_code }"/>
		        </td>
		    </tr>
		    <tr class="list ">
	            <td align="right">兑换积分数：</td>
	            <td>
	                <input type="text" name="gi_point" id="gi_point" class="main_Input" value="${gift.gi_point }"
	                	maxlength="8" onkeypress="javascript:return onlyPositiveNumber(this,event);" onblur="return valNumber(this);"/>
	                <input type="hidden" id="gi_point_old" name="gi_point_old" value="${gift.gi_point }"/>
	            </td>
		    </tr>
			<tr class="list ">
	        	<td align="right" >发布数量:</td>
	        	<td>
	           		<input class="main_Input" type="text" style="font-weight:bold"  name="totalamount" id="totalamount" readonly="readonly" value="${gift.totalamount }"/>
	        	</td>
		    </tr>
		    <tr class="list ">
		    	<td align="right">兑换结束日期：</td>
		    	<td>
		    		<input type="text" class="main_Input Wdate" style="width:198px;display:none;" readonly="readonly" 
		    		id="gi_begindate" name="gi_begindate"  value="${gift.gi_begindate }" 
		    		onclick="WdatePicker({isShowToday:false,dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'gi_enddate\',{d:0})}'})"/>
				 	<input style="width:198px;" class="main_Input Wdate" readonly="readonly" 
				 	id="gi_enddate" name="gi_enddate" value="${gift.gi_enddate }" 
				 	onclick="WdatePicker({isShowToday:false,dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'gi_begindate\',{d:0})}'})" />
		        </td>
			</tr>
		</table>
	</div>
  
	<div class="grid-wrap" id="detailDiv">
		<div id="listMode" style="display:'';">
	       	<table id="grid"></table>
	       	<div id="page"></div>
	   	</div>
		<div id="sizeMode" style="display:none;">
	      	<table id="sizeGroupGrid"></table>
	      	<div id="sizeGroupPage"></div>
	  	</div>
	</div>
  
</div>
</form>
<script>
function onlyPositiveNumber(obj,event){
	var keyCode = event.keyCode||event.which;
	if(keyCode == 8 || keyCode == 9 || keyCode == 37 || keyCode == 39){//退格键、tab键、左右方向键
		return;
	}
	if(!(((keyCode >= 48) && (keyCode <= 57)) || (keyCode == 13))){ 
		return false;
	}
 }
function valNumber(obj){
	var gi_point_old = $("#gi_point_old").val();
	var num = obj.value; 
	if(num.length > 0){
		if(num != "-" && isNaN(num)){
			$(obj).val(gi_point_old).select();
			Public.tips({type: 2, content : '请输入数字！'});
			return false;
		}
	}
}
	// 列表和尺码模式切换
	function doChangeMode(obj){
		var tbleListMode = document.getElementById("listMode");
		var tbleSizeMode = document.getElementById("sizeMode");
		var sizeModeButton = document.getElementById("sizeModeButton");
		var listModeButton = document.getElementById("listModeButton");
		if (obj == 0){
			tbleListMode.style.display = '';
			tbleSizeMode.style.display = 'none';
			listModeButton.className='t-btn btn-bl on';
			sizeModeButton.className='t-btn btn-bc';
			tbleSizeMode.style.border = 0;
			Utils.ajaxGetGiftTempList();
			$("#CurrentMode").val(0);
		}else if (obj == 1){
			tbleListMode.style.display = 'none';
			tbleSizeMode.style.display = ''; 
			listModeButton.className='t-btn btn-bl';
			sizeModeButton.className='t-btn btn-bc on';
			Utils.ajaxGetGiftTempTitles();
			$("#CurrentMode").val(1);
		}
	}
	
</script>
<script src="<%=basePath%>data/shop/gift/gift_update.js"></script>
</body>
</html>

