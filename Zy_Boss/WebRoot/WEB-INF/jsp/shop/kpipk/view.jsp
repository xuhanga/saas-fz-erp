<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/reward.css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<style type="text/css">
	.bottonfiled{position: fixed;top: 0;right: 25px;background-color: rgba(0, 0, 0, 0.2);border-radius: 0px 0px 3px 3px;padding: 8px;z-index: 9999;}
	.pk_top{position:fixed;left:1%;top:10px;right:1%;height:150px;border:#ddd solid 1px;}
	.baseinfo{position:absolute;left:1%;right:1%;top:0;background:#fff;height:150px;border:#ddd solid 1px;z-index:999}
	.baseinfo h2{float:left;width:100%;height:45px;border-bottom:#ddd solid 1px;background:#f8f8f8;text-align:center}
	.baseinfo h3{float:left;height:60px;width:60px;margin:15px 0 0 15px;border:#ddd solid 5px;background:#fff;overflow:hidden;border-radius:50%}
	.baseinfo dl{float:left;margin:10px 0 0 15px;width:70%;}
	.baseinfo dl dd{float:left;width:240px;line-height:30px;}
	.mainwra{position:fixed;left:0;top:10px;bottom:0px;right:0;overflow-x:hidden;overflow-y:auto;}
	.mapinfo{float:left;border:#ddd solid 1px;height:300px;width:98%;margin:170px 1% 0;overflow:hidden}
	.tab_nav{float:left;width:98%;height:40px;margin:15px 1% 0;}
	.tab_nav span{display:inline;float:left;height:35px;line-height:35px;width:70px;background:#f4f4f4;border:#ddd solid 1px;padding:0 10px;border-radius:3px 0 0 3px;cursor:pointer;}
	.tab_nav span.selted{background:#f60;border:#f30 solid 1px;color:#fff;}
	.tab_nav span.navr{border-radius:0 3px 3px 0;}
	.gridinfo{float:left;width:98%;margin:10px 1% 0;}
	.zongjie{float:left;height:150px;border:#ddd solid 1px;width:100%;}
</style>
</head>

<body>
<div class="mainwra">
<input type="hidden" id="kp_id" name="kp_id" value="${ kpiPk.kp_id}"/>
<input type="hidden" id="kp_number" name="kp_number" value="${ kpiPk.kp_number}"/>
<input type="hidden" id="kp_type" name="kp_type" value="${ kpiPk.kp_type}"/>
<input type="hidden" id="kp_state" name="kp_state" value="${ kpiPk.kp_state}"/>
<input type="hidden" id="kp_score" name="kp_score" value="${ kpiPk.kp_score}"/>
	<div class="baseinfo">
		<h2>
			<span style="float:left;margin-top:5px">
			<input type="button" class="t-btn btn-red" id="btn-save" onclick="javascript:handle.complete();" value="总结"/>
			<a class="t-btn" id="btn_close" >返回</a>
			</span>
		</h2>
		<dl>
			<dd>
				<span>考核范围：</span>
				<span>
					<c:choose>
						<c:when test="${ kpiPk.kp_type eq 0}">
							店铺
						</c:when>
						<c:when test="${ kpiPk.kp_type eq 1}">
							员工组
						</c:when>
						<c:when test="${ kpiPk.kp_type eq 2}">
							员工
						</c:when>
					</c:choose>
				</span>
			</dd>
			<dd>
				<span>单据编号：</span>
				<span>${kpiPk.kp_number }</span>
			</dd>
			<dd>
				<span>时间范围：</span>
				<span>${kpiPk.kp_begin }</span>~<span>${kpiPk.kp_end }</span>
			</dd>
			<dd>
				<span>标准分数：</span>
				<span>${kpiPk.kp_score }</span>
			</dd>
			<dd>
				<span>奖励：</span>
				<span id="reward_names">${kpiPk.reward_names }</span>
				<span class="iconfont i-hand" id="btn-reward" title="增加" onclick="javascript:Utils.doQueryReward();">&#xe639;</span>
				<input type="hidden" id="reward_codes" name="reward_codes" value=""/>
			</dd>
			
			<dd>
				<span>当前获胜者：</span>
				<span id="winner"></span>
			</dd>
			<dd>
				<span>当前最高分：</span>
				<span id="maxScore"></span>
			</dd>
			<dd >
				<span>摘要信息：</span>
				<span>${kpiPk.kp_remark }</span>
			</dd>
		</dl>	
	</div>
	<div class="mapinfo">
		<div id="container"></div>
	</div>
	<div class="tab_nav">
		<span id="nav_ffxz" class="selted"><i class="iconfont">&#x3435;</i> 发放奖励</span>
		<span id="nav_mbzj" class="navr"><i class="iconfont">&#xe632;</i> PK总结</span>
		<!-- <span id="medalTip" style="color:red;display:none;">*员工组发放勋章是针对组内每个员工发放勋章</span> -->
	</div>
	<div class="gridinfo">
		<div id="ffxz" class="grid-wrap">
			<table id="grid">
			</table>
		  	<div id="page"></div>
		</div>
		<div id="pkzj" class="zongjie" style="display:none"><!-- 完成情况 -->
			<c:choose>
				<c:when test="${ kpiPk.kp_state eq 0}">
					<div class="tagContent" id="tagContent0" style="display: block;">
		               <textarea id="kp_summary" style="border:0;border-bottom:#ddd solid 1px;font-size:14px;resize: none;font-family:Microsoft Yahei;overflow:hidden;width:100%;height:120px;" 
		               		onkeyup="javascript:calcLeftWordCount(this);" maxlength="200"></textarea>
		               <p style="color:red;font-family: Georgia;">&nbsp;&nbsp;200/<input disabled maxLength="4" style="font-family: Georgia;border:0;background:#fff;" size="3" value="200"/></p>
		           	</div>
				</c:when>
				<c:when test="${ kpiPk.kp_state eq 1}">
					${kpiPk.kp_summary }
				</c:when>
			</c:choose>
		</div>
	</div>
</div>
<script src="<%=basePath%>resources/chart/highcharts.4.0.3.js"></script>
<script src="<%=basePath%>data/shop/kpipk/kpipk_view.js"></script>
<script type="text/javascript">
	function calcLeftWordCount(obj){//计算剩余字数
		var maxLength = $(obj).attr("maxlength");
		var length = $(obj).val().length;
		if(length>maxLength){
			$(obj).val($(obj).val().substring(0,maxLength));
		}
		length = $(obj).val().length;
		$(obj).next().find("input").val(maxLength-length);
	}

	$(function(){
		$("#nav_ffxz").click(function(){
			$(".tab_nav span").removeClass("selted");
			$(this).addClass("selted");
			$("#ffxz").css("display","");
			$("#pkzj").css("display","none");
		});
		$("#nav_mbzj").click(function(){
			$(".tab_nav span").removeClass("selted");
			$(this).addClass("selted");
			$("#ffxz").css("display","none");
			$("#pkzj").css("display","");
		});
		$(".mainwra").scroll(function(){
			var scrollTop = $(this).scrollTop();
			var scrollW = $(this).width()/100 + 15;
			if(scrollTop > 20){
				$(".baseinfo").css({
					'position':"fixed",
					'top':"10px",
					'right': scrollW + "px"
				});
			}else{
				$(".baseinfo").css({
					'position':"absolute",
					'top':"0px",
					'right':"1%"
				});
			}
		})

	}) 
</script>
</body>
</html>