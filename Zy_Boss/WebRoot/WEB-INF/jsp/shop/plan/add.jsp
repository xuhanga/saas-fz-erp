<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.btn_Next{float:left;margin:0;font-size:14px;text-align:center;height:30px;line-height:30px;width:59px;background:#ddd;font-weight:bold;color:#fff;cursor:pointer;border:0;}
	.btn_Next:hover{background:#ccc;}
	.fileset{margin:10px 0 0 10px;border:#ddd solid 1px;width:350px;height:280px;padding:10px;}
	.fileset legend{margin-left:10px;padding:0 10px;}
	.fileset p{float:left;width:100%;line-height:35px;}
	.fileset em{float:left;width:90px;text-align:right;}
	.main_table{border-collapse:collapse;border:#080 solid 1px;font-family:"Microsoft Yahei"}
	.main_table td{border:#080 solid 1px;}
	.main_table th{border:#080 solid 1px;text-align:center;}
	.main_table .tabledate td{border-width:0;line-height:25px;}
	.main_table .tabledate td b{font-size:14px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<script>
function plan_set(){
	$("a[name='step']").removeClass("btn-green");
	$("#step_1").addClass("btn-green");
	$("#mainDiv").show();
	$("#monthDiv").hide();
}
function plan_month(){
	var pl_type = $("#pl_type").val();
	var pl_sell_money = null;
	if(pl_type == "0"){
		pl_sell_money = formatFloat($("#pl_sell_money").val());
	}else if(pl_type == "1"){
		pl_sell_money = formatFloat($("#pl_sell_money_1").val());
	}else if(pl_type == "2"){
		pl_sell_money = formatFloat($("#pl_sell_money_2").val());
	}
	if(pl_sell_money == 0){
		Public.tips({type: 2, content : "年度目标销售金额没有设置！"});
		return;
	}
	
	$("a[name='step']").removeClass("btn-green");
	$("#step_2").addClass("btn-green");
	$("#mainDiv").hide();
	$("#monthDiv").show();
	THISPAGE.initMonthDataGrid();
	THISPAGE.initPlanSellMoney();
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	      </td>
	      <td class="top-b border-b" width="320" align="right">
	         <a class="t-btn btn-green" name="step" id="step_1" onclick="javascript:plan_set()" >目标设置</a>
	         <a class="t-btn" name="step" id="step_2" onclick="javascript:plan_month()" >计划到月</a>
	         <a class="t-btn btn-red" id="btn-save" onclick="javascript:handle.save();">保存</a>
	         <a class="t-btn" id="btn_close" >返回</a>
	       </td>
	    </tr>
    </table>
    	<div id="mainDiv">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">计划名称：</td>
					<td width="165px">
						<input class="main_Input w146" type="text" name="pl_name" id="pl_name" value="" />
					</td>
					<td align="right" width="120px">店铺名称：</td>
					<td width="165px">
						<input class="main_Input" type="text" readonly="readonly" name="shop_name" id="shop_name" value="" style="width:122px"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
						<input type="hidden" name="pl_shop_code" id="pl_shop_code" value="" />
					</td>
					<td align="right" width="120px">制定日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate w146"  name="pl_date" id="pl_date" onclick="WdatePicker()" value="" />
					</td>
				</tr>
				<tr class="list">
					<td align="right">计划年份：</td>
					<td>
						<span class="ui-combo-wrap" id="span_year"></span>
						<input type="hidden" name="pl_year" id="pl_year"/>
					</td>
					<td align="right">计划方式：</td>
					<td>
						<span class="ui-combo-wrap" id="span_pl_type"></span>
						<input type="hidden" name="pl_type" id="pl_type"/>
					</td>
					<td align="right">备注：</td>
					<td>
						<input class="main_Input w146" type="text" name="pl_remark" id="pl_remark" value=""/>
					</td>
					
				</tr>
				<tr class="list last" id="grow_set_tr">
					
					<td align="right">预计毛利率：</td>
					<td>
						<input id="pl_gross_profit_rate" class="main_Input w146" value="" maxlength="4"
								onkeypress="return onlyDoubleNumber(this,event)"
								onkeyup="javascript:valDoubleNumber(this);calcPlanMoney_Type_0();calcPlanMoney_Type_1();"/>
					</td>
					<td align="right" name="type_0">同期销售增长百分比：</td>
					<td name="type_0">
						<input id="pl_sell_grow_percent" class="main_Input w146" value="" maxlength="4"
								onkeypress="return onlyDoubleNumber(this,event)"
								onkeyup="javascript:valDoubleNumber(this);calcPlanMoney_Type_0();"/>
					</td>
					<td align="right" name="type_0">费用开支增长百分比：</td>
					<td name="type_0">
						<input id="pl_expense_grow_percent" class="main_Input w146" value="" maxlength="4"
								onkeypress="return onlyDoubleNumber(this,event)"
								onkeyup="javascript:valDoubleNumber(this);calcPlanMoney_Type_0();"/>
					</td>
					<td align="right" name="type_1" style="display:none;">盈亏平衡增长百分比：</td>
					<td name="type_1" style="display:none;">
						<input id="pl_breakeven_grow_percent" class="main_Input w146" value="" maxlength="4"
								onkeypress="return onlyDoubleNumber(this,event)"
								onkeyup="javascript:valDoubleNumber(this);calcPlanMoney_Type_1();"/>
					</td>
				</tr>
			</table>
			<div style="width: 100%;border-bottom:1px solid #e4dddd;padding-bottom:15px;" id="plan_data_0">
				
				<div style="margin-top: 10px;">&nbsp;&nbsp;&nbsp;<i class="iconfont color-f30">&#xe709;</i>
					<font color="gray"><b>针对已有营业数据的店铺，制定来年销售计划，系统自动分配到年月。</b></font>
				</div>
				<table style="margin-top: 10px;margin-left: 15px;">
					<tr>
						<td width="350">
							<fieldset class="fileset">
								<legend style="color: blue;"><b>同期数据</b></legend>
								<p><em>销售金额：</em><input value="" type="text" id="pl_sell_money_pre" name="pl_sell_money_pre" class="main_Input" readonly="readonly" /></p>
								<p><em>销售成本：</em><input value="" type="text" id="pl_cost_money_pre" name="pl_cost_money_pre" class="main_Input" readonly="readonly"/></p>
								<p><em>费用开支：</em><input value="" type="text" id="pl_expense_pre" name="pl_expense_pre" class="main_Input" readonly="readonly"/></p>
								<p><em>毛利润：</em><input value="" type="text" id="pl_gross_money_pre" name="pl_gross_money_pre" class="main_Input" readonly="readonly"/></p>
								<p><em>净利润：</em><input value="" type="text" id="pl_net_money_pre" name="pl_net_money_pre" class="main_Input" readonly="readonly"/></p>
								<p><em>盈亏平衡点：</em><input value="" type="text" id="pl_breakeven_pre" name="pl_breakeven_pre" class="main_Input" readonly="readonly"/></p>
								<p><em>毛利率：</em><input value="" type="text" id="pl_gross_profit_rate_pre" name="pl_gross_profit_rate_pre" class="main_Input" readonly="readonly"/></p>
							</fieldset>
						</td>
						<td>
							<fieldset class="fileset">
								<legend><b style="color: green;">计划结果</b></legend>
								<p><em>费用开支：</em><input value="" type="text" id="pl_expense" name="pl_expense" class="main_Input" readonly="readonly"/></p>
								<p><em>盈亏平衡点：</em><input value="" type="text" id="pl_breakeven" name="pl_breakeven" class="main_Input" readonly="readonly"/></p>
								<p><em>目标销售金额：</em><input value="" type="text" id="pl_sell_money" name="pl_sell_money" class="main_Input" readonly="readonly"/></p>
								<p><em>目标销售成本：</em><input value="" type="text" id="pl_cost_money" name="pl_cost_money" class="main_Input" readonly="readonly"/></p>
								<p><em>目标毛利润：</em><input value="" type="text" id="pl_gross_money" name="pl_gross_money" class="main_Input" readonly="readonly"/></p>
								<p><em>目标净利润：</em><input value="" type="text" id="pl_net_money" name="pl_net_money" class="main_Input" readonly="readonly" /></p>
							</fieldset>
						</td>
					</tr>
				</table>
			</div>
			<div style="width:100%;border-bottom:1px solid #e4dddd;display:none;padding-bottom:15px;" id="plan_data_1">
				<div style="margin-top: 20px;">&nbsp;&nbsp;&nbsp;<i class="iconfont color-f30">&#xe709;</i>
					<font color="gray"><b>针对新开店铺，根据初步预估所需费用，制定出来年销售计划。</b></font>
				</div>
				<table style="margin-top:10px;margin-left:15px;">
					<tr>
						<td width="450">
							<fieldset class="fileset" style="width:410px;">
								<legend><b style="color: blue;">相关费用</b></legend>
								<div class="grid-wrap">
							        <table id="expensegrid">
							        </table>
							        <div id="expensepage"></div>
							    </div>
							</fieldset>
						</td>
						<td>
							<fieldset class="fileset">
								<legend><b style="color: green;">计划结果</b></legend>
								<p><em>费用开支：</em><input value="" type="text" id="pl_expense_1" name="pl_expense" class="main_Input" readonly="readonly"/></p>
								<p><em>盈亏平衡点：</em><input value="" type="text" id="pl_breakeven_1" name="pl_breakeven" class="main_Input" readonly="readonly"/></p>
								<p><em>目标销售金额：</em><input value="" type="text" id="pl_sell_money_1" name="pl_sell_money" class="main_Input" readonly="readonly"/></p>
								<p><em>目标销售成本：</em><input value="" type="text" id="pl_cost_money_1" name="pl_cost_money" class="main_Input" readonly="readonly"/></p>
								<p><em>目标毛利润：</em><input value="" type="text" id="pl_gross_money_1" name="pl_gross_money" class="main_Input" readonly="readonly"/></p>
								<p><em>目标净利润：</em><input value="" type="text" id="pl_net_money_1" name="pl_net_money" class="main_Input" readonly="readonly" /></p>
							</fieldset>
							
							
						</td>
					</tr>
				</table>
			</div>
			<div style="width:100%;border-bottom:1px solid #e4dddd;display:none;padding-bottom:15px;margin-top:10px;" id="plan_data_2">
				<div style="margin-top:20px;">&nbsp;&nbsp;&nbsp;<i class="iconfont color-f30">&#xe709;</i>
					<font color="gray"><b>只需一步，省去繁琐的计算，直接确定来年销售目标。</b></font>
				</div>
				<table style="margin-left:15px;">
					<tr>
						<td>
							<fieldset class="fileset" style="width:500px;height:80px;">
								<legend><b style="color: green;">计划结果</b></legend>
								<div style="margin-bottom: 5px;margin-top:10px;">
									目标销售金额：<input value="" type="text" id="pl_sell_money_2" name="pl_sell_money" class="main_Input" style="margin:0;ime-mode:disabled;"
												onkeypress="return onlyDoubleNumber(this,event)" onkeyup="javascript:valDoubleNumber(this);" maxlength="10"/>
								</div>						
							</fieldset>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<!-- 计划到月 -->	
		<div id="monthDiv" style="display: none;width:100%;border-bottom:#ddd solid 0px;padding-bottom:15px;">
			<div id="monthDivHTML" style="margin:10px 0 0 10px; overflow-x:hidden;overflow-y:hidden;">
				<div class="grid-wrap">
			        <table id="monthdatagrid">
			        </table>
			        <div id="monthdatapage"></div>
			    </div>
			</div>
	    </div> 
	
</div>
</form>
<script src="<%=basePath%>data/shop/plan/plan_add.js"></script>
</body>
</html>