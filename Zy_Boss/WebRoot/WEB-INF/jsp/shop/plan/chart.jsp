<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	计划门店：
	    	<input class="main_Input" type="text" id="shop_name" name="shop_name" value="" readonly="readonly" style="width:170px; " />
		     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
		     <input type="hidden" name="pl_shop_code" id="pl_shop_code" value=""/>
		     计划年份：
			<span class="ui-combo-wrap" id="span_year"></span>
			<input type="hidden" name="pl_year" id="pl_year"/>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	    	<a class="ui-btn mrb" id="btn_reset">重置</a>
		</div>
	    <div class="fr">
    		<input type="hidden" id="CurrentMode" value="0"/>
			<a name="mode-btn" class="t-btn btn-bl on"  onclick="Utils.display(this,0);"><i></i>店铺</a>
		    <a name="mode-btn" class="t-btn btn-blc"  onclick="Utils.display(this,1);"><i></i>月份</a>
	    </div> 
	 </div>
	
</div>
<div id="container" style="background:#fff; width:100%; float:left;" ></div>
<div style="height:36px;width:100%; line-height:36px;float:left;"></div>
<script type="text/javascript">
document.getElementById("container").style.height=(document.documentElement.clientHeight-72) + "px";
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.4.0.3.js\"></sc"+"ript>");
</script>
</form>
<script src="<%=basePath%>data/shop/plan/plan_chart.js"></script>
</body>
</html>