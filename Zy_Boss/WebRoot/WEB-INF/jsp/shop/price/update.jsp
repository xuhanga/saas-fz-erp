<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet"
	type="text/css" />
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet"
	type="text/css" />
<link href="<%=basePath%>resources/grid/css/ui.base.css"
	rel="stylesheet" type="text/css" />
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
</script>
<script>
	//控制控件只能输入数字,含.(小数点) 
	function onlyPositiveNumberWithPoint(obj){
		if(!(((window.event.keyCode >= 48) && (window.event.keyCode <= 57)) 
				|| (window.event.keyCode == 13) || (window.event.keyCode == 46) 
				)){ 
			window.event.keyCode = 0 ;
			return;
		}
		var position = 0; 
		if (document.selection) {	//for IE
			obj.focus();
			var sel = document.selection.createRange();
			sel.moveStart('character', -obj.value.length);
			position = sel.text.length;
		} else if (obj.selectionStart || obj.selectionStart == '0') {
			position = obj.selectionStart;
		}
		var ch=String.fromCharCode(window.event.keyCode);
		var value = obj.value.substring(0,position)+ch+obj.value.substring(position);//输入后的值
		if(document.selection.createRange().text == '' &&
				!(/^\d+\.?\d{0,2}$/.test(value))){//验证只能输入两位小数
			window.event.keyCode = 0 ;
		}
	}
	function valNumberWithPoint(obj){
		var num = obj.value; 
		if(num.length > 0){
			if(isNaN(num)){
				obj.value='';
				window.parent.$.dialog.tips('请输入数字!',2,'32X32/hits.png');
				return false;
			}
		}
	}
</script>
<script>
	//设置显示哪些价格
	function setShowPrice(){
		if($("#SellPrice").attr("checked")){$("td[show='SellPrice']").show();$("#SellPrice").val(1)}else{$("td[show='SellPrice']").hide();$("#SellPrice").val(0)}
		if($("#VipPrice").attr("checked")){$("td[show='VipPrice']").show();$("#VipPrice").val(1)}else{$("td[show='VipPrice']").hide();$("#VipPrice").val(0)}
		if($("#SortPrice").attr("checked")){$("td[show='SortPrice']").show();$("#SortPrice").val(1)}else{$("td[show='SortPrice']").hide();$("#SortPrice").val(0)}
		if($("#CostPrice").attr("checked")){$("td[show='CostPrice']").show();$("#CostPrice").val(1)}else{$("td[show='CostPrice']").hide();$("#CostPrice").val(0)}
	}
	//判断是否显示价格
	$(function(){
		if($("#SellPrice").attr("checked")){
			$("td[show='SellPrice']").show();
			$("#SellPrice").val(1)
		}else{
			$("td[show='SellPrice']").hide();
			$("#SellPrice").val(0)
		}
		if($("#VipPrice").attr("checked")){$("td[show='VipPrice']").show();$("#VipPrice").val(1)}else{$("td[show='VipPrice']").hide();$("#VipPrice").val(0)}
		if($("#SortPrice").attr("checked")){$("td[show='SortPrice']").show();$("#SortPrice").val(1)}else{$("td[show='SortPrice']").hide();$("#SortPrice").val(0)}
		if($("#CostPrice").attr("checked")){$("td[show='CostPrice']").show();$("#CostPrice").val(1)}else{$("td[show='CostPrice']").hide();$("#CostPrice").val(0)}
	});
	
</script>


</head>
<body>
	<form name="form1" method="post" action="" id="form1">
		<input type="hidden" id="flushFlag" name="flushFlag" value="" /> 
		<input type="hidden" id="sp_id" name="sp_id" value="${price.sp_id }"/>
		<input type="hidden" id='sp_shop_type' name="sp_shop_type" value="${price.sp_shop_type }" />
		<input type="hidden" name="sp_number" id="sp_number" value="${price.sp_number }"/>
		<%-- <input type="hidden" id="sp_is_sellprice" value="${price.sp_is_sellprice }"/>
		<input type="hidden" id="sp_is_vipprice" value="${price.sp_is_vipprice }"/>
		<input type="hidden" id="sp_is_sortprice" value="${price.sp_is_sortprice }"/>
		<input type="hidden" id="sp_is_costprice" value="${price.sp_is_costprice }"/> --%>
		<!-- 标记店铺类型 -->
		<div class="mainwra">
			<div class="border">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="3" class="top-b border-b">
							<input id="btn-save" class="t-btn btn-red" type="button" value="保存修改"/>
							<input id="btn_close" class="t-btn" type="button" value="返回"/>
						</td>
					</tr>
					<tr class="list first">
						<td style="width:240px;">分店列表： <span class="Periphery">
								<input type="text" id="shop_name"
								class="main_Input atfer_select w120" value="${price.sp_sp_name }" /> <input
								type="button" value=" " class="btn_select"
								onclick="javascript:Utils.doQueryShop();" /> <input
								type="hidden" id="sp_shop_code" name="sp_shop_code" value="${price.sp_shop_code }" />
						</span>
						</td>
						<td width="240">经办人员： <input type="text" id="sp_manager"
							name="sp_manager" readonly="readonly"
							class="main_Input atfer_select w120" value="${price.sp_manager }" /> <input
							type="button" value=" " class="btn_select"
							onclick="javascript:Utils.doQueryEmp();" />
						</td>
						<td>
							单据编号：
            				<input class="main_Input" type="text" disabled value="${price.sp_number }" />
						</td>
					</tr>
					<tr class="list last">
						<td>制作日期： <input class="main_Input Wdate w146"
							name="sp_makerdate" style="border:1px cfcbca solid;"
							readonly="readonly" onclick="WdatePicker();" id="sp_makerdate"
							value="${price.sp_makerdate}" />
						</td>
						<td colspan="2">摘要信息：<input style="width:300px;" type="text"
							id="sp_remark" name="sp_remark" value="${price.sp_remark}"
							class="main_Input" />
						</td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" class="top-b border-t"
					width="100%">
					<tr>
						<td style="width:240px;">货号查询： <span class="Periphery">
								<input name="pd_no" type="text" id="pd_no"
								class="main_Input atfer_select w120"
								onkeyup="javascript:if (event.keyCode==13){Utils.queryProductInfo($(this).val());};"
								value="" /> <input type="button" value="" class="btn_select"
								id="openProduct" /> <input type="text" style="display:none" />
						</span>
						</td>
						<td width="420"><span id="SellPrice"> <label
								class="chk" style="margin-top:6px;" title="零售价"> <input
									type="checkbox" name="box" ${price.sp_is_sellprice eq 1 ? "checked" : "" } />零售价
							</label>
						</span> <span id="VipPrice"> <label class="chk"
								style="margin-top:6px;" title="会员价"> <input
									type="checkbox" name="box" ${price.sp_is_vipprice eq 1 ? "checked" : "" }/>会员价
							</label>
						</span> 
						
						<c:choose>
						<c:when test="${price.sp_shop_type != 3 && price.sp_shop_type != 4 }">
							<span id="SortPrice" style=''>
		          				<label class="chk" style="margin-top:6px;" title="配送价">
		            				<input type="checkbox" name="box" ${price.sp_is_sortprice eq 1 ? "checked" : "" }/>配送价
		         				</label>
				          	</span>
						</c:when>
						<c:otherwise>
							<span id="SortPrice" style='display:none;'>
		          				<label class="chk" style="margin-top:6px;" title="配送价">
		            				<input type="checkbox" name="box" ${price.sp_is_sortprice eq 1 ? "checked" : "" }/>配送价
		         				</label>
				          	</span>
						</c:otherwise>
						</c:choose>
						<span id="CostPrice" style="${price.sp_shop_type eq 5 ? '' : 'display:none' };">
					    	<label class="chk" style="margin-top:6px;" title="成本价">
					        	<input type="checkbox" name="box" ${price.sp_is_costprice eq 1 ? "checked" : '' }/>成本价
					    	</label>
				        </span>
						
						 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span
							id="btnModifySamePinosPriceAll"> <label class="chk"
								style="margin-top:6px;" title="一键修改相同货号价格"> <input
									type="checkbox" name="box" />一键修改相同货号价格
							</label>
						</span></td>
						<td id="DiscountTd" width="140">零售折扣率： <input name="discount"
							type="text" id="discount" class="main_Input"
							style="color: red;font: bold;width:30px;"
							onkeyup="if(isNaN(value))execCommand('undo')"
							onkeydown="checkzero();"
							onafterpaste="if(isNaN(value))execCommand('undo')" value="1" />
						</td>

						<td id="UnitPriceTd" width="130">零售价： <input name="UnitPrice"
							type="text" id="UnitPrice" title="一键修改零售价" value=""
							style="width:60px;" class="main_Input" maxlength="11"
							onkeypress="return onlyPositiveNumberWithPoint(this)"
							onkeyup="javascript:valNumberWithPoint(this);javascript:if(event.keyCode==13){$('#btnModifyPriceAll').click();};" />
						</td>
						<td width="100" id="btnModifyPriceAllTd"><a
							class="t-btn btn-green" id="btnModifyPriceAll">一键修改</a></td>
						<!-- <td width="70"><a href="javascript:void(0);"
							class="ui-btn ui-btn-sp mrb" id="btn-import">导入</a></td> -->
						<td><a class="t-btn btn-red" onclick="javascript:handle.temp_clear();">清除</a>
						</td>
					</tr>
				</table>

			</div>

			<div class="grid-wrap">
				<div id="listMode" style="display:'';">
					<table id="grid"></table>
					<div id="page"></div>
				</div>
			</div>
		</div>
	</form>
	<script src="<%=basePath%>data/shop/price/price_add.js"></script>
</body>
</html>