<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/style.css" rel="stylesheet" type="text/css"/>
<style>
OL LI {
	MARGIN: 8px
}

.con_zzjs_net {
	FONT-SIZE: 12px;
	MARGIN-RIGHT: 2px;
	WIDTH: 480px;
	height: 200px;
}

#tags {
	PADDING-RIGHT: 0px;
	PADDING-LEFT: 0px;
	PADDING-BOTTOM: 0px;
	MARGIN: 0px 0px 0px 0px;
	WIDTH: 650px;
	PADDING-TOP: 0px;
	HEIGHT: 23px
}

#tags LI {
	FLOAT: left;
	MARGIN-RIGHT: 1px;
	LIST-STYLE-TYPE: none;
	HEIGHT: 23px
}

#tags LI A {
	PADDING-RIGHT: 10px;
	PADDING-LEFT: 10px;
	FLOAT: left;
	PADDING-BOTTOM: 0px;
	COLOR: #999;
	LINE-HEIGHT: 23px;
	PADDING-TOP: 0px;
	HEIGHT: 23px;
	TEXT-DECORATION: none
}

#tags LI.emptyTag {
	BACKGROUND: none transparent scroll repeat 0% 0%;
	WIDTH: 4px
}

#tags LI.selectTag {
	BACKGROUND-POSITION: left top;
	MARGIN-BOTTOM: -2px;
	POSITION: relative;
	HEIGHT: 25px
}

#tags LI.selectTag A {
	BACKGROUND-POSITION: right top;
	COLOR: #000;
	LINE-HEIGHT: 25px;	
	HEIGHT: 25px
}

#tagContent {
	BORDER-RIGHT: #ddd 1px solid;
	PADDING-RIGHT: 1px;
	BORDER-TOP: #ddd 1px solid;
	PADDING-LEFT: 1px;
	PADDING-BOTTOM: 1px;
	BORDER-LEFT: #ddd 1px solid;
	PADDING-TOP: 1px;
	BORDER-BOTTOM: #ddd 1px solid;
	WIDTH: 790px;
	HEIGHT: 425px
}

.tagContent {
	PADDING-RIGHT: 2px;
	DISPLAY: none;
	PADDING-LEFT: 0px;
	PADDING-BOTTOM: 0px;
	WIDTH: 100%;
	PADDING-TOP: 5px;
	HEIGHT: 450px
}

#tagContent DIV.selectTag {
	DISPLAY: block
}

#tagContent li {
	float: left;
	padding-left: 1px;
	padding-bottom: 7px;
}

#tagContent li span {
	float: right;
	color: #ccc;
	font-size: 10px;
}
.contents{border:#ddd solid 1px;width:99%;margin:10px auto 0;overflow:hidden;}
.contents h3{background:url(Images/navbg.jpg) repeat-x;border-bottom:#08b solid 1px;color:#08c;display:inline;float:left;font-size:12px;height:30px;line-height:30px;padding-left:10px;width:100%;}
.cbody{display:inline;float:left;width:100%;margin-top:10px;overflow-x:hidden;overflow-y:auto;}
.mxmod{display:inline;float:left;height:30px;line-height:30px;margin:0;width:100%;}
.tbbody{display:inline;float:left;height:350px;margin-left:10px;overflow-x:hidden;overflow-y:auto;width:100%;}
.mntable{border-left:#ccc solid 1px;border-top:#ccc solid 1px;margin-left:10px;}
.mntable td{background:#FFF;border-bottom:#ccc solid 1px;border-right:#ccc solid 1px;}
.mntable .thc{background:url(../../images/nav_bt_bg.png) repeat-x;height:30px;border-bottom:#ccc solid 1px;border-right:#ccc solid 1px;font-weight:700;text-align:center;}
.btn_pn{background:#08c;border:none;color:#fff;font-weight:700;height:30px;margin-left:10px;overflow:hidden;padding:0;width:80px;}
.btn_save{background:#f60;border:none;color:#fff;font-weight:700;height:30px;margin-left:10px;overflow:hidden;padding:0;width:80px;}
.btn_adds{background:#08b;border:none;color:#fff;font-weight:700;height:26px;margin-left:10px;overflow:hidden;padding:0;width:55px;}

.black-tr td{background-color:#f2f4f5;cursor:pointer;}
.write-tr td{background-color:#ffffff;cursor:pointer;}
.Wdate{width:78px;}
.atfer_select{width:155px;}
.w180{width:180px;}
</style>
<script>
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
</script>
</head>
  <body>
	<form name="form1" method="post" action="" id="form1">
	<input type="hidden" id="ss_model" name="ss_model" value="111"/>
	<input type="hidden" id="ifUpdatePriority" name="ifUpdatePriority" value="0"/><!-- 是否更新促销方案优先级  0否 1是 -->
	<div class="mainwra">
  <div class="border" style="border-bottom:#ccc solid 1px;_height:100%;">
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="top-b border-b"><a onclick="javascript:goBack();" class="t-btn">关闭</a></td>
      </tr>
    </table>
    <div id="sales1" style="height:500px;">
        <div class="contents">
          <div class="cbody">
          	<table width="99%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td width="80" height="40" align="right"><font color="red">*</font>促销名称：</td>
					<td width="300"><input class="main_Input w180" type="text" id="ss_name" name="ss_name" value="" maxlength="19"/></td>
					<td align="right" width="80">促销级别：</td>
					<td width="300"><span class="ui-combo-wrap" id="span_priority" style="margin-left:3px;"></span> 
						<input type="hidden" name="priority" id="priority"/>
						<input type="hidden" id="ss_priority" name="ss_priority" value="1" />
					</td>
					<td width="80" align="right"><font color="red">*</font>促销店铺：</td>
					<td>
						<input type="text" class="main_Input atfer_select" id="ss_sp_name" name="ss_sp_name" value="" readonly="readonly"/> 
						<input type="button" value=" " class="btn_select" onclick="javascript:Utils.doQueryShop();" /> 
						<input type="hidden" id="ss_shop_code" name="ss_shop_code" value="" />
					</td>
				</tr>
			<tr>
              <td align="right" height="40">促销日期：</td>
              <td>
                <input type="text" class="main_Input Wdate" readonly="readonly" id="ss_begin_date" name="ss_begin_date" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'ss_end_date\',{d:0})}'})"  value="" /> ~
                <input class="main_Input Wdate" readonly="readonly" id="ss_end_date" name="ss_end_date" onclick="WdatePicker({minDate:'#F{$dp.$D(\'ss_begin_date\',{d:0})}'})"   value="" />
              </td>
              <td align="right">会员类别：</td>
              <td>
                <input type="text" class="main_Input atfer_select" id="mt_name" name="mt_name" readonly="readonly" value="不做限制"/>
                <input type="button" value=" " class="btn_select" onclick="javascript:Utils.doQueryMemberType();"/>
                <input type="hidden" class="main_Input" id="ss_mt_code" name="ss_mt_code" value="ALL"/>
              </td>
              <td align="right">是否积分：</td>
              <td>
                <input type="radio" name="integral" value="1" checked="checked" onclick="javascript:changeIntegral(this);"/>是
                <input type="radio" name="integral" value="0" onclick="javascript:changeIntegral(this);"/>否
                <input type="hidden" id="ss_point" name="ss_point" value="1"/>
              </td>
            </tr>
            <tr>
              <td align="right" height="40">促销日：</td>
              <td>
                  <input type="checkbox" id="week" name="week" value="1" checked="checked" onclick="javascript:saveWeek();"/>一
                  <input type="checkbox" id="week" name="week" value="2" checked="checked" onclick="javascript:saveWeek();"/>二
                  <input type="checkbox" id="week" name="week" value="3" checked="checked" onclick="javascript:saveWeek();"/>三
                  <input type="checkbox" id="week" name="week" value="4" checked="checked" onclick="javascript:saveWeek();"/>四
                  <input type="checkbox" id="week" name="week" value="5" checked="checked" onclick="javascript:saveWeek();"/>五
                  <input type="checkbox" id="week" name="week" value="6" checked="checked" onclick="javascript:saveWeek();"/>六
                  <input type="checkbox" id="week" name="week" value="7" checked="checked" onclick="javascript:saveWeek();"/>七
                  <input type="hidden" id="ss_week" name="ss_week" value=""/>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </table>
          </div>
        </div>
        <div class="contents">
          
          <div class="cbody">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="80px;" align="right">模式选择：</td>
              <td>
                <input type="radio" id="SM_Model" name="SM_Model" value="1" checked="checked" onclick="javascript:radioClick();"/>折扣&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="SM_Model" name="SM_Model" value="2" onclick="javascript:radioClick();"/>特价&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" id="SM_Model" name="SM_Model" value="3" onclick="javascript:radioClick();"/>买满送
              </td>
            </tr>
            <tr>
              <td align="right">范围选择：</td>
              <td>
              <div id="SM_ScopeDiv1" style="width: 100%;">
                  <input type="radio" id="SM_Scope1" name="SM_Scope1" value="1" checked="checked" onclick="javascript:radioClick();"/>全场&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="SM_Scope1" name="SM_Scope1" value="2" onclick="javascript:radioClick();"/>类别&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" id="SM_Scope1" name="SM_Scope1" value="3" onclick="javascript:radioClick();"/>品牌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="SM_Scope1" name="SM_Scope1" value="4" onclick="javascript:radioClick();"/>商品
              </div>
              <div id="SM_ScopeDiv2" style="width: 100%;display: none;">
                  <input type="radio" id="SM_Scope2" name="SM_Scope2" value="1" disabled="disabled"/>全场&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="SM_Scope2" name="SM_Scope2" value="2" disabled="disabled"/>类别&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" id="SM_Scope2" name="SM_Scope2" value="3" disabled="disabled"/>品牌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="SM_Scope2" name="SM_Scope2" value="4" checked="checked" onclick="javascript:radioClick();"/>商品
              </div>
              <div id="SM_ScopeDiv3" style="width: 100%;display: none;">
                  <input type="radio" id="SM_Scope3" name="SM_Scope3" value="1" checked="checked" onclick="javascript:radioClick();"/>全场&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="SM_Scope3" name="SM_Scope3" value="2" onclick="javascript:radioClick();"/>类别&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" id="SM_Scope3" name="SM_Scope3" value="3" onclick="javascript:radioClick();"/>品牌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="SM_Scope3" name="SM_Scope3" value="4" onclick="javascript:radioClick();"/>商品
              </div>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td style="line-height:20px;">
                <div id="SM_RuleDiv1" style="width: 100%;">
                    <input type="radio" id="SM_Rule1" name="SM_Rule1" value="1" checked="checked" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#87D150">直接折扣</span><br />
                    &nbsp;&nbsp;&nbsp; 进行直接折扣，例如：全场95折、某一类别\品牌打9折、特定商品打8折。<br />
                    <input type="radio" id="SM_Rule1" name="SM_Rule1" value="2" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#4993E6">买满多少金额折扣</span><br />
                    &nbsp;&nbsp;&nbsp; 买满多少金额进行折扣，例如：全场买满100元打95折、某一类别\品牌买满100元打9折、特定商品买满100元打8折。<br />
                    <input type="radio" id="SM_Rule1" name="SM_Rule1" value="3" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#F26722">件数折扣</span><br />
                    &nbsp;&nbsp;&nbsp; 买满多少数量进行折扣，例如：全场买满10件打95折、某一类别\品牌打买满10件打9折、特定商品买满10件打8折。<br />
                </div>
                <div id="SM_RuleDiv2" style="width: 100%;display: none;">
                    <input type="radio" id="SM_Rule2" name="SM_Rule2" value="1" checked="checked" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#87D150">直接特价</span><br />
                    &nbsp;&nbsp;&nbsp; 选择的商品直接特价。<br />
                    <input type="radio" id="SM_Rule2" name="SM_Rule2" value="2" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#F26722">件数特价</span><br />
                    &nbsp;&nbsp;&nbsp; 选择的商品买满一定数量时执行新价格，例如：原价150元商品买满2件则每件140元。<br />
                </div>
                <div id="SM_RuleDiv3" style="width:100%;display: none;">
                    <!-- <input type="radio" id="SM_Rule3" name="SM_Rule3" value="1" checked="checked" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#87D150">买满N元送M元电子券</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定金额，送一定金额券，按最高金额送，例如买满100元送20或30元券，那么买满100元送30.<br /> -->
                    <input type="radio" id="SM_Rule3" name="SM_Rule3" value="1" checked="checked" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#F26722">买满N件商品送M件商品</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定数量商品，赠送一定数量商品，例如满3件商品送1件商品。<br />
                    <input type="radio" id="SM_Rule3" name="SM_Rule3" value="2" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#4993E6">买满N元减M元</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定金额，减一定金额，固定直减，例如满100减10，那么满200还是减10。<br />
                    <input type="radio" id="SM_Rule3" name="SM_Rule3" value="3" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#F26722">买满N元加M元送商品</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定金额，加一定金额，赠送商品，例如满100元加10元送商品A或者B<br />
                    <input type="radio" id="SM_Rule3" name="SM_Rule3" value="4" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#25A774">买满多少个商品送赠品</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定数量，赠送商品，例如买1件商品送赠品A或者B，买2件商品赠送赠品A或者B<br />
                    <input type="radio" id="SM_Rule3" name="SM_Rule3" value="5" onclick="javascript:radioClick();"/><span style="font-weight:bold;color:#25A774">买满M元送赠品</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定金额，赠送商品，例如买100元送赠品A，买200元赠送赠品B<br />
                </div>
              </td>
            </tr>
          </table>
          </div>
        </div>
        <div style="width:100%;text-align:left;margin-top:5px;">
           <input type="button" class="btn_pn" onclick="javascript:goNext();" value="下 一 步" />
        </div>
      </div>
      
      <div style="display:none;height:500px" id="sales2">
        <div class="contents">
          
          <div class="cbody" style="height:400px;display:none;" id="detail111">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：直接折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
            <table class="mntable" style="width:480px;" align="left" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="thc" width="60">序号</td>
                <td class="thc" style="width:102px;">折扣</td>
              </tr>
              <tr>
                <td align="center"><b>1</b></td>
                <td><input class='main_Input' style='text-align: right;width: 50px;text-align: right;' type="text" value="1.0" id="ssa_discount111" name="ssa_discount111" /></td>
              </tr>
            </table>
            </div>
          </div>
          
          <div class="cbody" style="height:400px;display:none;" id="detail112">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
              	<table width="480" border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;height:30px;">
	                <tr>
	                    <td width="210"><input value="添加" type="button" class="btn_adds" onclick="insertRows112('112')"/></td>
	                    <td width="310" align="right">统一金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssa_buy_full112');}" style='text-align: right;width: 50px;'/></td>
	                </tr>
                </table>
            <table class="mntable" style="width:480px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table112">
              	<tr>
	                 <td class="thc" style="width:60px;">序号</td>
	                <td class="thc" style="width:102px">买满金额</td>
	                <td class="thc" style="width:102px;">折扣</td>
	                <td class="thc" style="width:50px;">操作</td>
              	</tr>
            </table>
            </div>
          </div>  
               
          <div class="cbody" style="height:400px;display:none;" id="detail113">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：件数折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;height:30px;">
              	<tr>
                	<td><input value="添加" type="button" class="btn_adds" onclick="insertRows113('113')"/></td>
              	</tr>
            </table>
            <table class="mntable" style="width:400px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table113">
              	<tr>
	                <td class="thc" style="width:60px;">序号</td>
	                <td class="thc" style="width:102px">买满数量</td>
	                <td class="thc" style="width:102px;">折扣</td>
	                <td class="thc" style="width:50px;">操作</td>
              	</tr>
            </table>
            </div>
          </div>
          
          <div class="cbody" style="height:400px;display:none;" id="detail121">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：直接折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()" style="width:55px;"/></td>
                <td width="360">&nbsp;</td>
                <td width="150" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_discount121');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:500px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table121">
                <tr>
	                <td class="thc" style="width:60px;">序号</td>
	                <td class="thc" style="width:80px;">类别编码</td>
	                <td class="thc" >类别名称</td>
	                <td class="thc" style="width:102px;">折扣</td>
	                <td class="thc" style="width:50px;">操作</td>
                </tr>
            </table>
            </div>
          </div>
          
          <div class="cbody" style="height:400px;display:none;" id="detail122">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" type="button" class="btn_adds" onclick="doQueryType()"/></td>
                <td width="460" align="right">统一金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_buy_full122');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_discount122');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table122">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">类别编码</td>
                <td class="thc" >类别名称</td>
                <td class="thc" style="width:102px;">满金额</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail123">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：件数折&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()"/></td>
                <td width="460" align="right">统一件数：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_buy_number123');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_discount123');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table123">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">类别编码</td>
                <td class="thc" >类别名称</td>
                <td class="thc" style="width:102px;">买满数量</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail131">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：直接折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
                <td width="360" align="right"></td>
                <td width="150" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_discount131');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:500px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table131">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">品牌编码</td>
                <td class="thc" >品牌名称</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail132">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
                <td width="460" align="right">统一金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_buy_full132');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_discount132');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table132">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">品牌编码</td>
                <td class="thc" >品牌名称</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
        </div>
          <div class="cbody" style="height:400px;display:none;" id="detail133">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：件数折&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
                <td width="460" align="right">统一件数：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_buy_number133');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_discount133');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:680px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table133">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">品牌编码</td>
                <td class="thc" style="">品牌名称</td>
                <td class="thc" style="width:102px;">买满数量</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail141">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：直接折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="360" align="right"></td>
                <td width="150" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_discount141');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:580px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table141">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail142">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="510" align="right">统一金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_full142');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_discount142');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:730px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table142">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail143">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：件数折扣&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="510" align="right">统一数量：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_number143');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_discount143');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:730px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table143">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满数量</td>
                <td class="thc" style="width:102px;">折扣</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail241">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：直接特价&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="360" align="right">
                	统一特价：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_special_offer241');}" 
                	style='text-align: right;width: 50px;'/>
                </td>
                <td width="150" align="right">
                	统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputPrice(this,'ssp_special_offer241','RetailPrice241');}" 
                	style='text-align: right;width: 50px;'/>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:580px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table241">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">特价</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail242">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：直接特价&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="510" align="right">
                	统一数量：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_number242');}" 
                	style='text-align: right;width: 50px;'/>
                	统一特价：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_special_offer242');}" 
                	style='text-align: right;width: 50px;'/>
                </td>
                <td width="150" align="right">
                	统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputPrice(this,'ssp_special_offer242','RetailPrice242');}" 
                	style='text-align: right;width: 50px;'/>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:730px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table242">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满数量</td>
                <td class="thc" style="width:102px;">特价</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <!-- <div class="cbody" style="height:400px;display:none;" id="detail311">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元送M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="insertRows311('311')"/></td>
                <td width="360" align="right"></td>
                <td width="150" align="right"></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:320px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table311">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">赠送金额</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div> -->
          <div class="cbody" style="height:400px;display:none;" id="detail311">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N件商品送M件商品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
            	<table width="480" border="0" cellspacing="0" cellpadding="0" style="margin-left:10px;height:30px;">
	                <tr>
	                    <td width="210"><input value="添加" type="button" class="btn_adds" onclick="insertRows311('311')"/></td>
	                </tr>
                </table>
	            <table class="mntable" style="width:480px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table311">
	              	<tr>
		                 <td class="thc" style="width:60px;">序号</td>
		                <td class="thc" style="width:102px">买满件数</td>
		                <td class="thc" style="width:102px;">赠送件数</td>
		                <td class="thc" style="width:50px;">操作</td>
	              	</tr>
	            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail312">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元减M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="insertRows312('312')"/></td>
                <td width="360" align="right"></td>
                <td width="150" align="right"></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:500px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table312">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">减少金额</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:450px;display:none;" id="detail313">
            <div class="mxmod">
            	<input value="添加" class="btn_adds" type="button" onclick="insertRows313('313')"/>
            	<span style="margin-left:10px;">促销规则：买满N元加M元送商品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span>
            </div>
            <div class="tbbody">
            <div style="width:100%">
	            <table class="mntable" style="width:400px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table313">
	              	<tr>
		                <td class="thc" style="width:50px;">序号</td>
		                <td class="thc" style="width:102px;">买满金额</td>
		                <td class="thc" style="width:102px;">增加金额</td>
		                <td class="thc" style="width:100px;">操作</td>
	              	</tr>
	            </table>
            </div>
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable313">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:60px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:80px;">颜色</td>
                <td class="thc" style="width:60px;">尺码</td>
                <td class="thc" style="width:60px;">杯型</td>
                <td class="thc" style="width:102px;">数量</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail314">
	            <div class="mxmod">
	            	<span style="margin-left:10px;">促销规则：买满多少金额商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span>
	            </div>
            	<div class="tbbody">
	            	<div style="width:100%">
			            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              	<tr>
				                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="insertRows314('314')"/></td>
				                <td width="200" align="right">
				                	统一数量：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssa_buy_number314');}" 
				                	style='text-align: right;width: 50px;'/>
				                </td>
				                <td>&nbsp;</td>
			              	</tr>
			            </table>
			            <table class="mntable" style="width:300px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table314">
			              	<tr>
				                <td class="thc" style="width:50px;">序号</td>
				                <td class="thc" style="width:102px;">买满数量</td>
				                <td class="thc" style="width:100px;">操作</td>
			              	</tr>
			            </table>
		            </div>
		            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable314">
		              	<tr>
			                <td class="thc" style="width:50px;">序号</td>
			                <td class="thc" style="width:60px;">货号</td>
			                <td class="thc" style="">商品名称</td>
			                <td class="thc" style="width:80px;">颜色</td>
			                <td class="thc" style="width:60px;">尺码</td>
			                <td class="thc" style="width:60px;">杯型</td>
			                <td class="thc" style="width:102px;">数量</td>
			                <td class="thc" style="width:50px;">操作</td>
		              	</tr>
		            </table>
	            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail315">
	            <div class="mxmod">
	            	<span style="margin-left:10px;">促销规则：买满多少金额商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span>
	            </div>
            	<div class="tbbody">
	            	<div style="width:100%">
			            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              	<tr>
				                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="insertRows315('315')"/></td>
				                <td width="200" align="right">
				                	统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssa_buy_full315');}" 
				                	style='text-align: right;width: 50px;'/>
				                </td>
				                <td>&nbsp;</td>
			              	</tr>
			            </table>
			            <table class="mntable" style="width:300px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table315">
			              	<tr>
				                <td class="thc" style="width:50px;">序号</td>
				                <td class="thc" style="width:102px;">买满金额</td>
				                <td class="thc" style="width:100px;">操作</td>
			              	</tr>
			            </table>
		            </div>
		            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable315">
		              	<tr>
			                <td class="thc" style="width:50px;">序号</td>
			                <td class="thc" style="width:60px;">货号</td>
			                <td class="thc" style="">商品名称</td>
			                <td class="thc" style="width:80px;">颜色</td>
			                <td class="thc" style="width:60px;">尺码</td>
			                <td class="thc" style="width:60px;">杯型</td>
			                <td class="thc" style="width:102px;">数量</td>
			                <td class="thc" style="width:50px;">操作</td>
		              	</tr>
		            </table>
	            </div>
          </div>
          <!-- <div class="cbody" style="height:400px;display:none;" id="detail321">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元送M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()"/></td>
                <td width="460" align="right">统一金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ST_Buy_Full321');}" style='text-align: right;width: 50px;'/></td>
                <td width="150" align="right">统一折扣：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ST_Donation_Amount321');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table321">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">类别编码</td>
                <td class="thc" >类别名称</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">赠送金额</td>
                <td class="thc" style="width:60px;">操作</td>
              </tr>
            </table>
            </div>
          </div> -->
          <div class="cbody" style="height:400px;display:none;" id="detail322">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元减M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()"/></td>
                <td width="400" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_buy_full322');}" style='text-align: right;width: 50px;'/></td>
                <td width="200" align="right">统一减少金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_reduce_amount322');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table322">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">类别编码</td>
                <td class="thc">类别名称</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">减少金额</td>
                <td class="thc" style="width:60px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail323">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元加M元送商品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()"/></td>
                <td width="400" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_buy_full323');}" style='text-align: right;width: 50px;'/></td>
                <td width="200" align="right">统一增加金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_increased_amount323');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <div style="width:100%">
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table323">
              <tr>
                <td class="thc" style="width:60px;">序号</td>
                <td class="thc" style="width:80px;">类别编码</td>
                <td class="thc">类别名称</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">增加金额</td>
                <td class="thc" style="width:100px;">操作</td>
              </tr>
            </table>
            </div>
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable323">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:60px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:80px;">颜色</td>
                <td class="thc" style="width:60px;">尺码</td>
                <td class="thc" style="width:60px;">杯型</td>
                <td class="thc" style="width:102px;">数量</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail324">
            	<div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少个商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            	<div class="tbbody">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              	<tr>
			                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()"/></td>
			                <td width="200" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_buy_number324');}" style='text-align: right;width: 50px;'/></td>
			                <td>&nbsp;</td>
		              	</tr>
		            </table>
		            <div style="width:100%">
			            <table class="mntable" style="width:500px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table324">
			              	<tr>
				                <td class="thc" style="width:50px;">序号</td>
				                <td class="thc" style="width:80px;">类别编码</td>
				                <td class="thc" style="">类别名称</td>
				                <td class="thc" style="width:102px;">买满数量</td>
				                <td class="thc" style="width:100px;">操作</td>
			              	</tr>
			            </table>
		            </div>
		            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable324">
		              	<tr>
			                <td class="thc" style="width:50px;">序号</td>
			                <td class="thc" style="width:80px;">货号</td>
			                <td class="thc" style="">商品名称</td>
			                <td class="thc" style="width:80px;">颜色</td>
			                <td class="thc" style="width:50px;">尺码</td>
			                <td class="thc" style="width:50px;">杯型</td>
			                <td class="thc" style="width:102px;">数量</td>
			                <td class="thc" style="width:50px;">操作</td>
		              	</tr>
		            </table>
	            </div>
	         </div>
	         <div class="cbody" style="height:400px;display:none;" id="detail325">
            	<div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：类别</span></div>
            	<div class="tbbody">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              	<tr>
			                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryType()"/></td>
			                <td width="200" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'sst_buy_full325');}" style='text-align: right;width: 50px;'/></td>
			                <td>&nbsp;</td>
		              	</tr>
		            </table>
		            <div style="width:100%">
			            <table class="mntable" style="width:500px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table325">
			              	<tr>
				                <td class="thc" style="width:50px;">序号</td>
				                <td class="thc" style="width:80px;">类别编码</td>
				                <td class="thc" style="">类别名称</td>
				                <td class="thc" style="width:102px;">买满金额</td>
				                <td class="thc" style="width:100px;">操作</td>
			              	</tr>
			            </table>
		            </div>
		            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable325">
		              	<tr>
			                <td class="thc" style="width:50px;">序号</td>
			                <td class="thc" style="width:80px;">货号</td>
			                <td class="thc" style="">商品名称</td>
			                <td class="thc" style="width:80px;">颜色</td>
			                <td class="thc" style="width:50px;">尺码</td>
			                <td class="thc" style="width:50px;">杯型</td>
			                <td class="thc" style="width:102px;">数量</td>
			                <td class="thc" style="width:50px;">操作</td>
		              	</tr>
		            </table>
	            </div>
	         </div>
	         <!-- <div class="cbody" style="height:400px;display:none;" id="detail331">
		            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元送M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
		            <div class="tbbody">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr>
		                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
		                <td width="410" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'SB_Buy_Full331');}" style='text-align: right;width: 50px;'/></td>
		                <td width="200" align="right">统一赠送金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'SB_Donation_Amount331');}" style='text-align: right;width: 50px;'/></td>
		                <td>&nbsp;</td>
		              </tr>
		            </table>
		            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table331">
		              <tr>
		                <td class="thc" style="width:60px;">序号</td>
		                <td class="thc" style="width:80px;">品牌编码</td>
		                 <td class="thc" style="">品牌名称</td>
		                <td class="thc" style="width:102px;">买满金额</td>
		                <td class="thc" style="width:102px;">赠送金额</td>
		                <td class="thc" style="width:50px;">操作</td>
		              </tr>
		            </table>
	            </div>
          </div> -->
          <div class="cbody" style="height:400px;display:none;" id="detail332">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元减M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
                <td width="410" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_buy_full332');}" style='text-align: right;width: 50px;'/></td>
                <td width="200" align="right">统一减少金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_reduce_amount332');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table332">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">品牌编码</td>
                <td class="thc" style="">品牌名称</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">减少金额</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail333">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元加M元送商品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
            <div class="tbbody">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
						<td width="410" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_buy_full333');}" style='text-align: right;width: 50px;'/></td>
						<td width="200" align="right">统一增加金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_increased_amount333');}" style='text-align: right;width: 50px;'/></td>
						<td>&nbsp;</td>
					</tr>
				</table>
            <div style="width:100%;">
				<table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table333">
					<tr>
						<td class="thc" style="width:50px;">序号</td>
						<td class="thc" style="width:80px;">品牌编码</td>
						<td class="thc" style="">品牌名称</td>
						<td class="thc" style="width:102px;">买满金额</td>
						<td class="thc" style="width:102px;">增加金额</td>
						<td class="thc" style="width:100px;">操作</td>
					</tr>
				</table>
				<table class="mntable" style="width:600px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable333">
					<tr>
						<td class="thc" style="width:50px;">序号</td>
						<td class="thc" style="width:80px;">货号</td>
						<td class="thc" style="">商品名称</td>
						<td class="thc" style="width:80px;">颜色</td>
						<td class="thc" style="width:50px;">尺码</td>
						<td class="thc" style="width:50px;">杯型</td>
						<td class="thc" style="width:102px;">数量</td>
						<td class="thc" style="width:50px;">操作</td>
					</tr>
				</table>
            </div>
            </div>
        </div>
          <div class="cbody" style="height:400px;display:none;" id="detail334">
	            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少个商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
	            <div class="tbbody">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              	<tr>
			                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
			                <td width="260" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_buy_number334');}" style='text-align: right;width: 50px;'/></td>
			                <td>&nbsp;</td>
		              	</tr>
		            </table>
		            <div style="width:100%;">
			            <table class="mntable" style="width:450px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table334">
			              	<tr>
				                <td class="thc" style="width:50px;">序号</td>
				                <td class="thc" style="width:80px;">品牌编码</td>
				                <td class="thc" style="">品牌名称</td>
				                <td class="thc" style="width:102px;">买满数量</td>
				                <td class="thc" style="width:100px;">操作</td>
			              	</tr>
			            </table>
		            </div>
		            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable334">
		             	<tr>
			                <td class="thc" style="width:50px;">序号</td>
			                <td class="thc" style="width:80px;">货号</td>
			                <td class="thc" style="">商品名称</td>
			                <td class="thc" style="width:80px;">颜色</td>
			                <td class="thc" style="width:50px;">尺码</td>
			                <td class="thc" style="width:50px;">杯型</td>
			                <td class="thc" style="width:102px;">数量</td>
			                <td class="thc" style="width:50px;">操作</td>
		              	</tr>
		            </table>
	            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail335">
	            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：品牌</span></div>
	            <div class="tbbody">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              	<tr>
			                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="doQueryBrand()"/></td>
			                <td width="260" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssb_buy_full335');}" style='text-align: right;width: 50px;'/></td>
			                <td>&nbsp;</td>
		              	</tr>
		            </table>
		            <div style="width:100%;">
			            <table class="mntable" style="width:450px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table335">
			              	<tr>
				                <td class="thc" style="width:50px;">序号</td>
				                <td class="thc" style="width:80px;">品牌编码</td>
				                <td class="thc" style="">品牌名称</td>
				                <td class="thc" style="width:102px;">买满金额</td>
				                <td class="thc" style="width:100px;">操作</td>
			              	</tr>
			            </table>
		            </div>
		            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable335">
		             	<tr>
			                <td class="thc" style="width:50px;">序号</td>
			                <td class="thc" style="width:80px;">货号</td>
			                <td class="thc" style="">商品名称</td>
			                <td class="thc" style="width:80px;">颜色</td>
			                <td class="thc" style="width:50px;">尺码</td>
			                <td class="thc" style="width:50px;">杯型</td>
			                <td class="thc" style="width:102px;">数量</td>
			                <td class="thc" style="width:50px;">操作</td>
		              	</tr>
		            </table>
	            </div>
          </div>
          <!-- <div class="cbody" style="height:400px;display:none;" id="detail341">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元送M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="410" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'SP_Buy_Full341');}" style='text-align: right;width: 50px;'/></td>
                <td width="200" align="right">统一赠送金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'SP_Donation_Amount341');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table341">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">赠送金额</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div> -->
          <div class="cbody" style="height:400px;display:none;" id="detail342">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元减M元&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="510" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_full342');}" style='text-align: right;width: 50px;'/></td>
                <td width="200" align="right">统一减少金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_reduce_amount342');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table342">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">减少金额</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail343">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N元加M元送商品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="210" align="right">统一买满金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_full343');}" style='text-align: right;width: 50px;'/></td>
                <td width="200" align="right">统一增加金额：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_increased_amount343');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <div style="width:100%;">
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table343">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满金额</td>
                <td class="thc" style="width:102px;">增加金额</td>
                <td class="thc" style="width:100px;">操作</td>
              </tr>
            </table>
            </div>
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable343">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:70px">颜色</td>
                <td class="thc" style="width:50px;">尺码</td>
                <td class="thc" style="width:50px;">杯型</td>
                <td class="thc" style="width:102px;">数量</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
          </div>
          <div class="cbody" style="height:400px;display:none;" id="detail344">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少个商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
                <td width="200" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_number344');}" style='text-align: right;width: 50px;'/></td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <div style="width:100%;">
            <table class="mntable" style="width:550px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table344">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:102px;">零售价</td>
                <td class="thc" style="width:102px;">买满数量</td>
                <td class="thc" style="width:100px;">操作</td>
              </tr>
            </table>
            </div>
            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable344">
              <tr>
                <td class="thc" style="width:50px;">序号</td>
                <td class="thc" style="width:80px;">货号</td>
                <td class="thc" style="">商品名称</td>
                <td class="thc" style="width:70px">颜色</td>
                <td class="thc" style="width:50px;">尺码</td>
                <td class="thc" style="width:50px;">杯型</td>
                <td class="thc" style="width:102px;">数量</td>
                <td class="thc" style="width:50px;">操作</td>
              </tr>
            </table>
            </div>
     	 </div>
     	 <div class="cbody" style="height:400px;display:none;" id="detail345">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满多少金额商品送赠品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：商品</span></div>
            <div class="tbbody">
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              	<tr>
		                <td width="70" align="center"><input value="添加" class="btn_adds" type="button" onclick="queryProductInfo()"/></td>
		                <td width="200" align="right">统一：<input type='text' class='main_Input' value='' onkeyup="javascript:if(event.keyCode==13){allinputChange(this,'ssp_buy_full345');}" style='text-align: right;width: 50px;'/></td>
		                <td>&nbsp;</td>
	            	</tr>
	            </table>
	            <div style="width:100%;">
		            <table class="mntable" style="width:550px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="table345">
		              <tr>
		                <td class="thc" style="width:50px;">序号</td>
		                <td class="thc" style="width:80px;">货号</td>
		                <td class="thc" style="">商品名称</td>
		                <td class="thc" style="width:102px;">零售价</td>
		                <td class="thc" style="width:102px;">买满金额</td>
		                <td class="thc" style="width:100px;">操作</td>
		              </tr>
		            </table>
	            </div>
	            <table class="mntable" style="width:650px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0"  id="gttable345">
	              <tr>
	                <td class="thc" style="width:50px;">序号</td>
	                <td class="thc" style="width:80px;">货号</td>
	                <td class="thc" style="">商品名称</td>
	                <td class="thc" style="width:70px">颜色</td>
	                <td class="thc" style="width:50px;">尺码</td>
	                <td class="thc" style="width:50px;">杯型</td>
	                <td class="thc" style="width:102px;">数量</td>
	                <td class="thc" style="width:50px;">操作</td>
	              </tr>
	            </table>
            </div>
          </div>
      </div>
      <div style="width: 100%;text-align:left;margin-top:10px">
          <input type="button" class="btn_pn" onclick="javascript:goUp();" value="上一步" />
          <input type="button" class="btn_save" onclick="javascript:saveSales();" value="保  存" />
      </div>
      </div>    
  </div>
</div>
</form>
<script src="<%=basePath%>data/shop/sale/sale_add.js"></script>
<script type="text/javascript">
	var api = frameElement.api, W = api.opener;
	$(function(){
		var date=new Date();  
		document.getElementById("ss_begin_date").value = DateToFullDateTimeString(date);
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate()+30);
		document.getElementById("ss_end_date").value = DateToFullDateTimeString(date);
		saveWeek();
	});
	//js日期格式化函数
	function DateToFullDateTimeString(date){   
	    var year = date.getFullYear();  
	    var month = date.getMonth();  
	    var day = date.getDate();  
	    var hour = date.getHours();  
	    var minute = date.getMinutes();  
	    var second = date.getSeconds();  
	  
	    var datestr;  
	  	
	    if (month <9)  
	    {  
	        month = '0' + (month + 1);  
	    }else{
	    	month = month + 1;  
	    }  
	    if (day < 10)  
	    {  
	        day = '0' + day;  
	    }  
	    if (hour < 10)  
	    {  
	        hour = '0' + hour;  
	    }  
	    if (minute < 10)  
	    {  
	        minute = '0' + minute;  
	    }  
	    if (second < 10)  
	    {  
	        second = '0' + second;  
	    }  
	    datestr = year + '-' + month + '-' + day;
	    return datestr;  
	}
	function goBack(){
		$.dialog.confirm('确定要返回查询页面吗？', function(){
				api.close();
			}
		);
	}
	function changeIntegral(obj){
		document.getElementById("ss_point").value = obj.value;
	}
	function saveWeek(){
		var days = "";
		$("input[name='week']:checked").each(function(){
			days += $(this).val();			
		});
		$("#ss_week").val(days);
	}
	function goNext(){
		var ss_name = $("#ss_name").val();
		if(ss_name == null || ss_name==""){
			Public.tips({type: 2, content : '请输入促销名称!'});
			return;
		}
		var ss_shop_code = $("#ss_shop_code").val();
		if(ss_shop_code == null || ss_shop_code==""){
			Public.tips({type: 2, content : '请选择促销店铺!'});
			return;
		}
		var ss_priority = $("#ss_priority").val();
		$.ajax({
			type:"post",
			url:config.BASEPATH+'shop/sale/queryPriority',
			data:'ss_priority='+ss_priority+'&ss_shop_code='+ss_shop_code,
			cache:false,
			dataType:"json",
			success:function(data){
				/* var smCodes = ["111","112","113","121","122","123","131",
							   "132","133","141","142","143","241","242","311","312",
							   "313","314","321","322","323","324","331","332","333",
							   "334","341","342","343","344","315","325","335","345"]; */
				var smCodes = ["111","112","113","121","122","123","131",
							   "132","133","141","142","143","241","242","311","312",
							   "313","314","322","323","324","332","333",
							   "334","342","343","344","315","325","335","345"];
				var smCode = $("#ss_model").val();
				if(data.message == "sucess"){
					$.dialog.confirm('相同店铺在同一时间内存在促销级别相同，是否继续操作?',function(){
						document.getElementById("sales1").style.display="none";
						document.getElementById("sales2").style.display="";
						for(var i=0;i<smCodes.length;i++){
							document.getElementById("detail"+smCodes[i]).style.display="none";
						}
						document.getElementById("detail"+smCode).style.display="";
						//把是否更新促销优先级 设为更新
						document.getElementById("ifUpdatePriority").value = "1";
					},function(){
						return;
					});
				}else{
					document.getElementById("sales1").style.display="none";
					document.getElementById("sales2").style.display="";
					for(var i=0;i<smCodes.length;i++){
						document.getElementById("detail"+smCodes[i]).style.display="none";
					}
					document.getElementById("detail"+smCode).style.display="";
				}
			}
		});
	}
	function goUp(){
		document.getElementById("sales1").style.display="";
		document.getElementById("sales2").style.display="none";
		document.getElementById("ifUpdatePriority").value = "0";
	}
	function radioClick(){
		var smModel = document.getElementsByName("SM_Model");
		var smModelVal = "";
		var smScopeVal = "";
		var smRuleVal = "";
		for(var i=0;i<smModel.length;i++){
			if(smModel[i].checked){
				smModelVal = smModel[i].value;
			}
		}
		if(smModelVal=="1"){
			document.getElementById("SM_ScopeDiv1").style.display="";
			document.getElementById("SM_ScopeDiv2").style.display="none";
			document.getElementById("SM_ScopeDiv3").style.display="none";
			
			document.getElementById("SM_RuleDiv1").style.display="";
			document.getElementById("SM_RuleDiv2").style.display="none";
			document.getElementById("SM_RuleDiv3").style.display="none";
			
			var sm_Scope1 = document.getElementsByName("SM_Scope1");
			for(var i=0;i<sm_Scope1.length;i++){
				if(sm_Scope1[i].checked){
					smScopeVal = sm_Scope1[i].value;
				}
			}
		
			var sm_Rule1 = document.getElementsByName("SM_Rule1");
			for(var i=0;i<sm_Rule1.length;i++){
				if(sm_Rule1[i].checked){
					smRuleVal = sm_Rule1[i].value;
				}
			}
		}
		if(smModelVal=="2"){
			document.getElementById("SM_ScopeDiv1").style.display="none";
			document.getElementById("SM_ScopeDiv2").style.display="";
			document.getElementById("SM_ScopeDiv3").style.display="none";
			
			document.getElementById("SM_RuleDiv1").style.display="none";
			document.getElementById("SM_RuleDiv2").style.display="";
			document.getElementById("SM_RuleDiv3").style.display="none";
			
			var sm_Scope2 = document.getElementsByName("SM_Scope2");
			for(var i=0;i<sm_Scope2.length;i++){
				if(sm_Scope2[i].checked){
					smScopeVal = sm_Scope2[i].value;
				}
			}
		
			var sm_Rule2 = document.getElementsByName("SM_Rule2");
			for(var i=0;i<sm_Rule2.length;i++){
				if(sm_Rule2[i].checked){
					smRuleVal = sm_Rule2[i].value;
				}
			}
		}
		if(smModelVal=="3"){
			document.getElementById("SM_ScopeDiv1").style.display="none";
			document.getElementById("SM_ScopeDiv2").style.display="none";
			document.getElementById("SM_ScopeDiv3").style.display="";
			
			document.getElementById("SM_RuleDiv1").style.display="none";
			document.getElementById("SM_RuleDiv2").style.display="none";
			document.getElementById("SM_RuleDiv3").style.display="";
			
			var sm_Scope3 = document.getElementsByName("SM_Scope3");
			for(var i=0;i<sm_Scope3.length;i++){
				if(sm_Scope3[i].checked){
					smScopeVal = sm_Scope3[i].value;
				}
			}
		
			var sm_Rule3 = document.getElementsByName("SM_Rule3");
			for(var i=0;i<sm_Rule3.length;i++){
				if(sm_Rule3[i].checked){
					smRuleVal = sm_Rule3[i].value;
				}
			}
		}
		var sm_code = smModelVal+smScopeVal+smRuleVal;
		$("#ss_model").val(sm_code);
		/* var sm_codes = "311,321,331,341,";
		if(sm_codes.indexOf(sm_code) > -1){
			$("#SP_UseDays").attr("readonly",false).css("background","#fff");
		}else{ 
			$("#SP_UseDays").attr("readonly",true).css("background","#ddd");
		}*/
	}
</script>  
</body>
</html>