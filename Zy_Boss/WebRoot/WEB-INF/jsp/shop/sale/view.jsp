<%@page import="zy.entity.shop.sale.T_Shop_Sale_Present"%>
<%@page import="zy.entity.shop.sale.T_Shop_Sale_Product"%>
<%@page import="zy.entity.shop.sale.T_Shop_Sale_Brand"%>
<%@page import="zy.entity.shop.sale.T_Shop_Sale_Type"%>
<%@page import="zy.entity.shop.sale.T_Shop_Sale_All"%>
<%@page import="zy.entity.shop.sale.T_Shop_Sale"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="zy.util.StringUtil"%>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet" type="text/css"/>
<style>
OL LI {
	MARGIN: 8px
}

.con_zzjs_net {
	FONT-SIZE: 12px;
	MARGIN-RIGHT: 2px;
	WIDTH: 480px;
	height: 200px;
}

#tags {
	PADDING-RIGHT: 0px;
	PADDING-LEFT: 0px;
	PADDING-BOTTOM: 0px;
	MARGIN: 0px 0px 0px 0px;
	WIDTH: 650px;
	PADDING-TOP: 0px;
	HEIGHT: 23px
}

#tags LI {
	FLOAT: left;
	MARGIN-RIGHT: 1px;
	LIST-STYLE-TYPE: none;
	HEIGHT: 23px
}

#tags LI A {
	PADDING-RIGHT: 10px;
	PADDING-LEFT: 10px;
	FLOAT: left;
	PADDING-BOTTOM: 0px;
	COLOR: #999;
	LINE-HEIGHT: 23px;
	PADDING-TOP: 0px;
	HEIGHT: 23px;
	TEXT-DECORATION: none
}

#tags LI.emptyTag {
	BACKGROUND: none transparent scroll repeat 0% 0%;
	WIDTH: 4px
}

#tags LI.selectTag {
	BACKGROUND-POSITION: left top;
	MARGIN-BOTTOM: -2px;
	POSITION: relative;
	HEIGHT: 25px
}

#tags LI.selectTag A {
	BACKGROUND-POSITION: right top;
	COLOR: #000;
	LINE-HEIGHT: 25px;	
	HEIGHT: 25px
}

#tagContent {
	BORDER-RIGHT: #ddd 1px solid;
	PADDING-RIGHT: 1px;
	BORDER-TOP: #ddd 1px solid;
	PADDING-LEFT: 1px;
	PADDING-BOTTOM: 1px;
	BORDER-LEFT: #ddd 1px solid;
	PADDING-TOP: 1px;
	BORDER-BOTTOM: #ddd 1px solid;
	WIDTH: 790px;
	HEIGHT: 425px
}

.tagContent {
	PADDING-RIGHT: 2px;
	DISPLAY: none;
	PADDING-LEFT: 0px;
	PADDING-BOTTOM: 0px;
	WIDTH: 100%;
	PADDING-TOP: 5px;
	HEIGHT: 450px
}

#tagContent DIV.selectTag {
	DISPLAY: block
}

#tagContent li {
	float: left;
	padding-left: 1px;
	padding-bottom: 7px;
}

#tagContent li span {
	float: right;
	color: #ccc;
	font-size: 10px;
}
.contents{border:#ddd solid 1px;width:99%;margin:10px auto 0;overflow:hidden;}
.contents h3{background:url(Images/navbg.jpg) repeat-x;border-bottom:#08b solid 1px;color:#08c;display:inline;float:left;font-size:12px;height:30px;line-height:30px;padding-left:10px;width:100%;}
.cbody{display:inline;float:left;width:100%;margin-top:10px;overflow-x:hidden;overflow-y:auto;}
.mxmod{display:inline;float:left;height:30px;line-height:30px;margin:0;width:100%;}
.tbbody{display:inline;float:left;height:350px;margin-left:10px;overflow-x:hidden;overflow-y:auto;width:100%;}
.mntable{border-left:#ccc solid 1px;border-top:#ccc solid 1px;margin-left:10px;}
.mntable td{background:#FFF;border-bottom:#ccc solid 1px;border-right:#ccc solid 1px;}
.mntable .thc{background:url(../../images/nav_bt_bg.png) repeat-x;height:30px;border-bottom:#ccc solid 1px;border-right:#ccc solid 1px;font-weight:700;text-align:center;}
.btn_pn{background:#08c;border:none;color:#fff;font-weight:700;height:30px;margin-left:10px;overflow:hidden;padding:0;width:80px;}
.btn_save{background:#f60;border:none;color:#fff;font-weight:700;height:30px;margin-left:10px;overflow:hidden;padding:0;width:80px;}
.btn_adds{background:#08b;border:none;color:#fff;font-weight:700;height:26px;margin-left:10px;overflow:hidden;padding:0;width:55px;}

.black-tr td{background-color:#f2f4f5;cursor:pointer;}
.write-tr td{background-color:#ffffff;cursor:pointer;}
.Wdate{width:78px;}
.atfer_select{width:155px;}
.w180{width:180px;}
</style>
<script>
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
</script>
</head>
<body>
	<%	
  		/* LoginInfo loginInfo = com.rx.fz.business.sys.BaseUserBusiness.getBaseUserBySession(request.getSession());
  		java.util.Map<String,String>  theMenu = EnterpriseBaseInfoBusiness.getMenuLimitBySession(request.getSession());//获取权限
	    String batchLimit = (String)theMenu.get(MenuLimit.PROM_PLAN);//获取权限字段 */
	    java.util.Map<String,Object> saleMap = (java.util.Map<String,Object>)request.getAttribute("saleMap");
		T_Shop_Sale t_Shop_Sale = (T_Shop_Sale)saleMap.get("t_Shop_Sale");
		if(t_Shop_Sale==null){
			t_Shop_Sale = new T_Shop_Sale();
		}
		/* String approve = request.getParameter("approve"); */
	%>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="ss_id" name="ss_id" value="<%=t_Shop_Sale.getSs_id() %>"/>
<input type="hidden" id="ss_code" name="ss_code" value="<%=t_Shop_Sale.getSs_code() %>"/>
<input type="hidden" id="ss_state" name="ss_state" value="<%=t_Shop_Sale.getSs_state() %>"/>
<input type="hidden" id="ss_model" name="ss_model" value="<%=t_Shop_Sale.getSs_model() %>"/>
<div class="mainwra">
  <div class="border">
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="top-b border-b">
        <%-- <%if(0 == t_sales_Promtotion.getSP_Stop_Status() && t_sales_Promtotion.getSP_Auditing_Status() == 1){ %>
        <a onclick="javascript:auditingUpdate('<%=StringUtil.trimString(t_sales_Promtotion.getSP_Code()) %>','2');" id="stop" class="t-btn btn-red">终止</a>
        <%}if(0 == t_sales_Promtotion.getSP_Auditing_Status() && "1".equals(approve)){ %>
        <a onclick="javascript:approve('<%=StringUtil.trimString(t_sales_Promtotion.getSP_Code()) %>');" id="approve" class="t-btn btn-green">审核</a>
        <%} %> --%>
        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	    <input id="btn-stop" class="t-btn btn-bblue" type="button" value="终止" style="display:none;"/>
	    <input id="btn_close" class="t-btn" type="button" value="返回"/>
        </td>
      </tr>
    </table>
	<div id="sales1" style="margin-top:0px;">
      <div class="contents">
        <div class="cbody">
        <table width="99%" border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td width="80" align="right"><font color="red">*</font>方案名称：</td>
            <td width="270"><input class="main_Input" type="text" id="ss_name" name="ss_name" disabled value="<%=StringUtil.trimString(t_Shop_Sale.getSs_name()) %>" /></td>
            <td width="80" align="right">促销编号：</td>
            <td width="180"><input class="main_Input" type="text" value="<%=StringUtil.trimString(t_Shop_Sale.getSs_code()) %>" id="ss_code" name="ss_code" style="" disabled/></td>
            <td width="80" align="right"><font color="red">*</font>分店名称：</td>
            <td>
              <input type="text" class="main_Input" id="ss_sp_name" name="ss_sp_name" value="<%=StringUtil.trimString(t_Shop_Sale.getSs_sp_name())%>"   disabled/>
              <input type="hidden" id="ss_shop_code" name="ss_shop_code" value="<%=StringUtil.trimString(t_Shop_Sale.getSs_shop_code()) %>"/>
            </td>
          </tr>
          <tr>
            <td align="right">促销时间：</td>
            <td><input type="text" class="main_Input Wdate" style="width:75px;" readonly="readonly"  id="ss_begin_date" name="ss_begin_date" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'SP_End_Date\',{d:0})}'})"  value="<%=StringUtil.trimString(t_Shop_Sale.getSs_begin_date()) %>" disabled/>~
					<input style="width:75px;" class="main_Input Wdate" readonly="readonly"  id="ss_end_date" name="ss_end_date" onclick="WdatePicker({minDate:'#F{$dp.$D(\'SP_Begin_Date\',{d:0})}'})"   value="<%=StringUtil.trimString(t_Shop_Sale.getSs_end_date()) %>" disabled/></td>
            <td align="right">会员等级：</td>
            <td><%if("ALL".equals(StringUtil.trimString(t_Shop_Sale.getSs_mt_code()))){ %>
			  		<input type="text" class="main_Input" id="ss_mt_name" name="ss_mt_name" readonly="readonly"  disabled value="不做限制"/>
			  		<%}else if("Y".equals(StringUtil.trimString(t_Shop_Sale.getSs_mt_code()))){ %>
			  		<input type="text" class="main_Input" id="ss_mt_name" name="ss_mt_name" readonly="readonly"  disabled value="所有会员"/>
			  		<%}else if("N".equals(StringUtil.trimString(t_Shop_Sale.getSs_mt_code()))){ %>
			  		<input type="text" class="main_Input" id="ss_mt_name" name="ss_mt_name" readonly="readonly"  disabled value="非会员"/>
			  		<%}else{ %>
			  		<input type="text" class="main_Input" id="ss_mt_name" name="ss_mt_name" readonly="readonly"  disabled value="<%=StringUtil.trimString(t_Shop_Sale.getSs_mt_name()) %>"/>
					<%} %>
			  		<input type="hidden" class="main_Input" id="ss_mt_code" name="ss_mt_code" value="<%=StringUtil.trimString(t_Shop_Sale.getSs_mt_code()) %>"/></td>
            <td align="right">促销级别：</td>
            <td><span class="standard_select" style="margin-left:3px;width:200px;">
					 <span class="select_shelter" style="border-top:0px solid #FFFFFF;border-bottom:0px solid #FFFFFF;width:200px;">
						<select id="priority" name="priority"  style="width:195px" disabled>
							<%if("5".equals(StringUtil.trimString(t_Shop_Sale.getSs_priority()))){ %>
							<option value="5" selected="selected">最低</option>
							<%}else{ %>
							<option value="5">最低</option>
							<%}if("2".equals(StringUtil.trimString(t_Shop_Sale.getSs_priority()))){ %>
							<option value="2" selected="selected">较高</option>
							<%}else{ %>
							<option value="2">较高</option>
							<%}if("3".equals(StringUtil.trimString(t_Shop_Sale.getSs_priority()))){ %>
							<option value="3" selected="selected">一般</option>
							<%}else{ %>
							<option value="3">一般</option>
							<%}if("4".equals(StringUtil.trimString(t_Shop_Sale.getSs_priority()))){ %>
							<option value="4" selected="selected">较低</option>
							<%}else{ %>
							<option value="4">较低</option>
							<%}if("1".equals(StringUtil.trimString(t_Shop_Sale.getSs_priority()))){ %>
							<option value="1" selected="selected">最高</option>
							<%}else{ %>
							<option value="1">最高</option>
							<%} %>
						</select>
					</span>
				</span>
          </tr>
          <tr>
            <td align="right">促销日：</td>
			<td><input type="hidden" id="SP_Day" name="t_Sales_Promtotion.SP_Day" value=""/>
            		<%	String spDay = StringUtil.trimString(t_Shop_Sale.getSs_week());
            			String check = "";
					 	if(spDay.indexOf("1") > -1){ 
					 		check = "checked";
					 %>
					 	<input type="checkbox" id="week" name="week" value="1" <%=check%> disabled/>一
					 <% check = "";} if(spDay.indexOf("2") > -1){ check = "checked";%>
					 	<input type="checkbox" id="week" name="week" value="2" <%=check%> disabled/>二
					 <% check = "";} if(spDay.indexOf("3") > -1){ check = "checked"; %>
					 	<input type="checkbox" id="week" name="week" value="3" <%=check%> disabled/>三
					 <% check = "";} if(spDay.indexOf("4") > -1){ check = "checked";%>
					 	<input type="checkbox" id="week" name="week" value="4" <%=check%> disabled/>四
					 <% check = "";} if(spDay.indexOf("5") > -1){ check = "checked";%>
					 	<input type="checkbox" id="week" name="week" value="5" <%=check%> disabled/>五
					 <% check = "";} if(spDay.indexOf("6") > -1){ check = "checked";%>
					 	<input type="checkbox" id="week" name="week" value="6" <%=check%> disabled/>六
					 <% check = "";} if(spDay.indexOf("7") > -1){ check = "checked";%>
					 	<input type="checkbox" id="week" name="week" value="7" <%=check%> disabled/>七
					 <% check = "";} %>
		 	</td>
            <td align="right">是否积分：</td>
            <td><%if("1".equals(StringUtil.trimString(t_Shop_Sale.getSs_point()))){ %>
					<input type="radio" name="integral" value="1" checked="checked" disabled/>是
					<%}else{ %>
					<input type="radio" name="integral" value="1" disabled/>是
					<%}if("0".equals(StringUtil.trimString(t_Shop_Sale.getSs_point()))){ %>
					<input type="radio" name="integral" value="0" checked="checked" disabled/>否
					<%}else{ %>
					<input type="radio" name="integral" value="0" disabled/>否
					<%} %></td>
            <td align="right"></td>
            <td></td>
          </tr>
          <%-- <tr>
            <td align="right">操作员：</td>
            <td><input type="text" id="SP_Operate_Man" class="main_Input" name="t_Sales_Promtotion.SP_Operate_Man" value="<%=StringUtil.trimString(t_sales_Promtotion.getSP_Operate_Man()) %>" disabled/></td>
            <td align="right">操作日期：</td>
            <td><input type="text" class="main_Input"  id="SP_Operate_Date" name="t_Sales_Promtotion.SP_Operate_Date" value="<%=StringUtil.trimString(t_sales_Promtotion.getSP_Operate_Date()) %>" disabled/></td>
            <td align="right">终止人：</td>
            <td><input class="main_Input" type="text" id="" name="" value="<%=StringUtil.trimString(t_sales_Promtotion.getSP_Stop_Man()) %>" disabled/></td>
            <td align="right">终止日期：</td>
            <td><input type="text" class="main_Input" id="" name="" value="<%=StringUtil.trimString(t_sales_Promtotion.getSP_Stop_Date()) %>" disabled/></td>
          </tr>
          <tr>
            <td align="right">审核人：</td>
            <td><input class="main_Input"  type="text" id="" name="" value="<%=StringUtil.trimString(t_sales_Promtotion.getSP_Auditing_Man()) %>" disabled/></td>
            <td align="right">审核日期：</td>
            <td><input class="main_Input"  type="text" id="" name="" value="<%=StringUtil.trimString(t_sales_Promtotion.getSP_Auditing_Date()) %>"  disabled/></td>
            <%if("311,321,331,341".indexOf(t_Shop_Sale.getSs_model()) > -1){ %>
            <td align="right" >有效天数：</td>
            <td><input class="main_Input" type="text" id="SP_UseDays" name="t_Sales_Promtotion.SP_UseDays" value="<%=t_sales_Promtotion.getSP_UseDays()%>" disabled/></td>
            <% }%>
          </tr> --%>
        </table>
        </div>
        <div class="cbody">
        <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="80" align="right">模式选择：</td>
            <td>
            <input type="radio" id="ssm_model" name="ssm_model" value="1" <%if(t_Shop_Sale.getSsm_model()==1){ %>checked="checked"<%}%> disabled/>折扣&nbsp;
			<input type="radio" id="ssm_model" name="ssm_model" value="2" <%if(t_Shop_Sale.getSsm_model()==2){ %>checked="checked"<%}%> disabled/>特价&nbsp;
            <input type="radio" id="ssm_model" name="ssm_model" value="3" <%if(t_Shop_Sale.getSsm_model()==3){ %>checked="checked"<%}%> disabled/>买满送
            </td>
          </tr>
          <tr>
            <td align="right">范围选择：</td>
            <td>
            <input type="radio" id="ssm_scope2" name="ssm_scope2" value="1" <%if(t_Shop_Sale.getSsm_scope()==1){ %>checked="checked"<%}%> disabled/>全场&nbsp;
            <input type="radio" id="ssm_scope2" name="ssm_scope2" value="2" <%if(t_Shop_Sale.getSsm_scope()==2){ %>checked="checked"<%}%> disabled/>类别&nbsp;
			<input type="radio" id="ssm_scope2" name="ssm_scope2" value="3" <%if(t_Shop_Sale.getSsm_scope()==3){ %>checked="checked"<%}%> disabled/>品牌&nbsp;
            <input type="radio" id="ssm_scope2" name="ssm_scope2" value="4" <%if(t_Shop_Sale.getSsm_scope()==4){ %>checked="checked"<%}%> disabled/>商品</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td style="line-height:20px;">
			<%if(t_Shop_Sale.getSsm_model()==1){%>
			<div id="SM_RuleDiv1" style="width: 100%;">
				<input type="radio" id="SM_Rule1" name="SM_Rule1" value="1" <%if(t_Shop_Sale.getSsm_rule() == 1){%>checked="checked"<%}%> disabled/><span style="font-weight:bold;color:#87D150">直接折扣</span><br />
				&nbsp;&nbsp;&nbsp; 针对选择的范围，进行直接折扣，例如：全场95折、某一类别\品牌打9折、特定商品打8折。<br />
				<input type="radio" id="SM_Rule1" name="SM_Rule1" value="2" <%if(t_Shop_Sale.getSsm_rule() == 2){%>checked="checked"<%}%> disabled/><span style="font-weight:bold;color:#4993E6">买满多少金额折扣</span><br />
				&nbsp;&nbsp;&nbsp; 针对选择的范围，买满多少金额进行折扣，例如：全场买满100元打95折、某一类别\品牌买满100元打9折、<br />
				&nbsp;&nbsp;&nbsp; 特定商品买满100元打8折。<br />
				<input type="radio" id="SM_Rule1" name="SM_Rule1" value="3" <%if(t_Shop_Sale.getSsm_rule() == 3){%>checked="checked"<%}%> disabled/><span style="font-weight:bold;color:#F26722">件数折扣</span><br />
				&nbsp;&nbsp;&nbsp; 针对选择的范围，买满多少数量进行折扣，例如：全场买满10件打95折、某一类别\品牌打买满10件打9折、<br />
				&nbsp;&nbsp;&nbsp; 特定商品买满10件打8折。<br />
			</div>
			<%}
			if(t_Shop_Sale.getSsm_model()==2){
			%>
			<div id="SM_RuleDiv2" style="width: 100%;">
				<input type="radio" id="SM_Rule2" name="SM_Rule2" value="1" <% if(t_Shop_Sale.getSsm_rule() == 1){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#87D150">直接折扣</span><br />
				&nbsp;&nbsp;&nbsp; 选择的商品直接特价。<br /> 
				<input type="radio" id="SM_Rule2" name="SM_Rule2" value="2" <% if(t_Shop_Sale.getSsm_rule() == 2){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#F26722">件数折扣</span><br />
				&nbsp;&nbsp;&nbsp; 选择的商品买满一定数量时执行新价格，例如：原价150元衣服买满2件则每件140元。<br />
			</div>
			<%}
			if(t_Shop_Sale.getSsm_model()==3){
			%>
			<div id="SM_RuleDiv3" style="width: 100%;">
				<%-- <input type="radio" id="SM_Rule3" name="SM_Rule3" value="1" <% if(t_Shop_Sale.getSsm_rule() == 1){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#87D150">买满N元送M元</span><br />
				&nbsp;&nbsp;&nbsp; 买满一定金额，送一定金额券，按最高金额送，例如买满100元送20或30元券，那么买满100元送30。<br /> --%>
				<input type="radio" id="SM_Rule3" name="SM_Rule3" value="1" <% if(t_Shop_Sale.getSsm_rule() == 1){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#F26722">买满N件商品送M件商品</span><br />
                    &nbsp;&nbsp;&nbsp; 买满一定数量商品，赠送一定数量商品，例如满3件商品送1件商品。<br />
				<input type="radio" id="SM_Rule3" name="SM_Rule3" value="2" <% if(t_Shop_Sale.getSsm_rule() == 2){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#4993E6">买满N元减M元</span><br />
				&nbsp;&nbsp;&nbsp; 买满一定金额，减一定金额，固定直减，例如满100减10，那么满200还是减10。<br />
				<input type="radio" id="SM_Rule3" name="SM_Rule3" value="3" <% if(t_Shop_Sale.getSsm_rule() == 3){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#F26722">买满N元加M元送商品</span><br />
				&nbsp;&nbsp;&nbsp; 买满一定金额，加一定金额，赠送商品，例如满100元加10元送商品A或者B<br />
				<input type="radio" id="SM_Rule3" name="SM_Rule3" value="4" <% if(t_Shop_Sale.getSsm_rule() == 4){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#25A774">买满多少个商品送赠品</span><br />
				&nbsp;&nbsp;&nbsp; 买满一定数量，赠送商品，例如买1件衣服送赠品A或者B，买2件衣服赠送赠品A或者B<br />
				<input type="radio" id="SM_Rule3" name="SM_Rule3" value="5" <% if(t_Shop_Sale.getSsm_rule() == 5){%>checked="checked"<%} %> disabled/><span style="font-weight:bold;color:#25A774">买满M元送赠品</span><br />
                &nbsp;&nbsp;&nbsp; 买满一定金额，赠送商品，例如买100元送赠品A，买200元赠送赠品B<br />
			</div>
			<%
			}
			 %>
            </td>
          </tr>
        </table>
        </div>
      </div>
		<div style="width:99%;margin:5px 5px 0;text-align:right;">
			<input type="button" class="btn_pn" onclick="javascript:goNext();" value="下 一 步" />
		</div>
	</div>
    
	<div style="display:none;" id="sales2">
		<%
		List<T_Shop_Sale_All> shop_sale_allList = (List<T_Shop_Sale_All>)saleMap.get("shop_sale_allList");
		if(shop_sale_allList == null){shop_sale_allList = new ArrayList<T_Shop_Sale_All>();}
		
		List<T_Shop_Sale_Type> shop_sale_typeList = (List<T_Shop_Sale_Type>)saleMap.get("shop_sale_typeList");
		if(shop_sale_typeList == null){shop_sale_typeList = new ArrayList<T_Shop_Sale_Type>();}
		
		List<T_Shop_Sale_Brand> shop_sale_brandList = (List<T_Shop_Sale_Brand>)saleMap.get("shop_sale_brandList");
		if(shop_sale_brandList == null){shop_sale_brandList = new ArrayList<T_Shop_Sale_Brand>();}
		
		List<T_Shop_Sale_Product> shop_sale_productList = (List<T_Shop_Sale_Product>)saleMap.get("shop_sale_productList");
		if(shop_sale_productList == null){shop_sale_productList = new ArrayList<T_Shop_Sale_Product>();}
		
		List<T_Shop_Sale_Present> shop_sale_presentList = (List<T_Shop_Sale_Present>)saleMap.get("shop_sale_presentList");
		if(shop_sale_presentList == null){shop_sale_presentList = new ArrayList<T_Shop_Sale_Present>();}
		
		if("111".equals(t_Shop_Sale.getSs_model())){
		 %>
		<div class="class_cbody" style="width:99%;height: 440px;" id="detail111">
			促销规则：直接折扣<br/>
			促销范围：全场<br />
			<table class="main_table" width="200">
				<tr>
					<td class="head" style="width:40px">序号</td>
					<td class="head">折扣</td>
				</tr>
				<%for(int i=0;i<shop_sale_allList.size();i++){ %>
				<tr>
					<td align="center" ><b><%=i+1 %></b></td>
					<td align="center" ><input type="text" value="<%=shop_sale_allList.get(i).getSsa_discount() %>" id="ssa_discount111" name="ssa_discount111" style="text-align: right;" disabled/></td>
				</tr>
				<%} %>
			</table>
		</div>
		<%}if("112".equals(t_Shop_Sale.getSs_model())){  %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail112">
			促销规则：买满多少金额折扣<br/>
			促销范围：全场<br />
			<div style="overflow-x:hidden;width:320px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:260px;" id="table112">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:108px">买满金额</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_allList.size();i++){ %>
					<tr>
						<td  align="center"><b><%=i+1 %></b></td>
						<td align="right"><b><input type="text" style="width:100px" value="<%=shop_sale_allList.get(i).getSsa_buy_full() %>" id="ssa_buy_full112" name="ssa_buy_full112" style="text-align: right;" disabled/></b></td>
						<td align="center"><input type="text" style="width:100px" value="<%=shop_sale_allList.get(i).getSsa_discount() %>" id="ssa_discount112" name="ssa_discount112" style="text-align: right;" disabled/></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("113".equals(t_Shop_Sale.getSs_model())){  %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail113">
			促销规则：件数折扣<br/>
			促销范围：全场<br />
			<div style="width:500px;;height:350px; position:relative; left:0px; overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:450px;" id="table113">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head">买满数量</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_allList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="right"><input type="text" value="<%=shop_sale_allList.get(i).getSsa_buy_number() %>" id="ssa_buy_number113" name="ssa_buy_number113" style="text-align: right;" disabled/></td>
						<td align="center" ><input type="text" value="<%=shop_sale_allList.get(i).getSsa_discount() %>" id="ssa_discount113" name="ssa_discount113" style="text-align: right;" disabled/></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("121".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail121">
			促销规则：直接折扣<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:420px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:400px;" id="table121">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">类别编码</td>
						<td class="head" style="width:150px">类别名称</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_typeList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><input type='text' value='<%=shop_sale_typeList.get(i).getSst_tp_code() %>' id='sst_tp_code121' name='sst_tp_code121' style='text-align: right;' disabled='disabled'/></td>
						<td align="left"><%=shop_sale_typeList.get(i).getTp_name() %></td>
						<td align="center"><input type='text' value='<%=shop_sale_typeList.get(i).getSst_discount() %>' id='sst_discount121' name='sst_discount121' style='text-align: right;' disabled/></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("122".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail122">
			促销规则：买满多少金额折扣<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:700px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:680px;" id="table122">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">类别编号</td>
						<td class="head" style="width:150px;">类别名称</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_typeList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_typeList.get(i).getSst_tp_code() %></td>
						<td align="left"><%=shop_sale_typeList.get(i).getTp_name() %></td>
						<td align="right"><b><%=shop_sale_typeList.get(i).getSst_buy_full()%></b></td>
						<td align="center"><%=shop_sale_typeList.get(i).getSst_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("123".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail123">
			促销规则：件数折<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:700px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:680px;" id="table123">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">类别编号</td>
						<td class="head" style="width:150px;">类别名称</td>
						<td class="head" style="width:100px;">买满数量</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_typeList.size();i++){ %>
					<tr>
						<td  align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_typeList.get(i).getSst_tp_code() %></td>
						<td align="left"><%=shop_sale_typeList.get(i).getTp_name() %></td>
						<td  align="right"><b><%=shop_sale_typeList.get(i).getSst_buy_number()%></b></td>
						<td  align="center"><%=shop_sale_typeList.get(i).getSst_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("131".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail131">
			促销规则：直接折扣<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:600px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:400pxpx;" id="table131">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">品牌编号</td>
						<td class="head" style="width:150px">品牌名称</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_brandList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_brandList.get(i).getSsb_bd_code() %></td>
						<td align="left"><%=shop_sale_brandList.get(i).getBd_name() %></td>
						<td align="center"><%=shop_sale_brandList.get(i).getSsb_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("132".equals(t_Shop_Sale.getSs_model())){  %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail132">
			促销规则：买满多少金额折扣<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:558px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table132">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">品牌编号</td>
						<td class="head" style="width:150px">品牌名称</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_brandList.size();i++){ %>
					<tr>
						<td algin="center"><b><%=i+1 %></b></td>
						<td algin="left"><%=shop_sale_brandList.get(i).getSsb_bd_code() %></td>
						<td algin="left"><%=shop_sale_brandList.get(i).getBd_name() %></td>
						<td algin="right"><%=shop_sale_brandList.get(i).getSsb_buy_full() %></td>
						<td style="text-algin:center"><%=shop_sale_brandList.get(i).getSsb_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("133".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 380px;" id="detail133">
			促销规则：件数折<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:518px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:500px;" id="table133">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">品牌编号</td>
						<td class="head" style="width:150px;">品牌名称</td>
						<td class="head" style="width:100px;">买满数量</td>
						<td class="head" >折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_brandList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_brandList.get(i).getSsb_bd_code() %></td>
						<td align="left"><%=shop_sale_brandList.get(i).getBd_name() %></td>
						<td align="right"><b><%=shop_sale_brandList.get(i).getSsb_buy_number() %></b></td>
						<td  align="right"><%=shop_sale_brandList.get(i).getSsb_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("141".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail141">
			促销规则：直接折扣<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:518px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:500px;" id="table141">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">款号</td>
						<td class="head" style="width:150px;">商品名称</td>
						<td class="head" style="width:100px;">零售价</td>
						<td class="head" >折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="center"><%=shop_sale_productList.get(i).getSsp_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("142".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail142">
			促销规则：买满多少金额折扣<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:618px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:scroll;">
				<table class="main_table" style="width:600px;" id="table142">
					<tr>
						<td class="head" style="width: 40px;">序号</td>
						<td class="head" style="width:150px;">款号</td>
						<td class="head" style="width:150px;">商品名称</td>
						<td class="head" style="width:100px;">零售价</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_buy_full() %></b></td>
						<td align="center"><%=shop_sale_productList.get(i).getSsp_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("143".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail143">
			促销规则：件数折扣<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:618px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="table143">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">款号</td>
						<td class="head" style="width:150px;">商品名称</td>
						<td class="head" style="width:100px;">零售价</td>
						<td class="head" style="width:100px;">买满数量</td>
						<td class="head">折扣</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td align="center"><%=i+1 %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_buy_number() %></b></td>
						<td align="center"><%=shop_sale_productList.get(i).getSsp_discount() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("241".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail241">
			促销规则：直接特价<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:618px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="table241">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">款号</td>
						<td class="head" style="width:150px;">商品名称</td>
						<td class="head" style="width:100px;">零售价</td>
						<td class="head">特价</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="center"><%=shop_sale_productList.get(i).getSsp_special_offer() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("242".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail242">
			促销规则：件数特价<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:718px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:700px;" id="table242">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">款号</td>
						<td class="head" style="width:150px;">商品名称</td>
						<td class="head" style="width:100px;">零售价</td>
						<td class="head" style="width:100px;">买满数量</td>
						<td class="head">特价</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_buy_number() %></b></td>
						<td align="center"><%=shop_sale_productList.get(i).getSsp_special_offer() %></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%-- <%}if("311".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail311">
			促销规则：买满N元送M元<br/>
			促销范围：全场<br />
			<div style="overflow-x:hidden;width:258px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:240px;" id="table311">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head"  >赠送金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_allList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="right"><b><%=shop_sale_allList.get(i).getSsa_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_allList.get(i).getSsa_donation_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div> --%>
		<%}if("311".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="cbody" style="height:400px;" id="detail311">
            <div class="mxmod"><span style="margin-left:10px;">促销规则：买满N件商品送M件商品&nbsp;&nbsp;&nbsp;&nbsp;促销范围：全场</span></div>
            <div class="tbbody">
	            <table class="mntable" style="width:480px;margin-top:5px;" align="left" border="0" cellspacing="0" cellpadding="0" id="table311">
	              	<tr>
		                 <td class="thc" style="width:60px;">序号</td>
		                <td class="thc" style="width:102px">买满件数</td>
		                <td class="thc" style="width:102px;">赠送件数</td>
	              	</tr>
	              	<%for(int i=0;i<shop_sale_allList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="right"><b><%=shop_sale_allList.get(i).getSsa_buy_number() %></b></td>
						<td align="right"><b><%=shop_sale_allList.get(i).getSsa_donation_number() %></b></td>
					</tr>
					<%} %>
	            </table>
            </div>
         </div>
		<%}if("312".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" class="class_cbody" style="width: 99%;height: 440px;" id="detail312">
			促销规则：买满N元减M元<br/>
			促销范围：全场<br />
			<div style="overflow-x:hidden;width:258px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:240px;" id="table312">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">减少金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_allList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="right"><b><%=shop_sale_allList.get(i).getSsa_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_allList.get(i).getSsa_reduce_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("313".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail313">
			促销规则：买满N元加M元送商品<br/>
			促销范围：全场<br />
			<div style="overflow-x:hidden;width:258px;;height:70px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:240px;" id="table313">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">增加金额</td>
					</tr>
					<%for(T_Shop_Sale_All model_All:shop_sale_allList){ %>
					<tr onclick="javascript:doShowProduct('313','<%=model_All.getSsa_index()%>')">
						<td align="center"><b><%=model_All.getSsa_index()%></b></td>
						<td align="right"><b><%=model_All.getSsa_buy_full() %></b></td>
						<td align="right"><b><%=model_All.getSsa_increased_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:618px;;height:300px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable313">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px;">颜色</td>
						<td class="head" style="width:60px;">尺码</td>
						<td class="head" style="width:60px;">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-313-<%=present.getSsp_index()%> comm-313">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("314".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="table314">
			促销规则：买满多少个商品送赠品<br/>
			促销范围：全场<br />
			<div style="width:158px;;height:300px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:140px;">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head">买满数量</td>
					</tr>
					<%for(T_Shop_Sale_All model_All:shop_sale_allList){ %>
					<tr onclick="javascript:doShowProduct('314','<%=model_All.getSsa_index()%>')">
						<td align="center"><b><%=model_All.getSsa_index()%></b></td>
						<td align="right"><%=model_All.getSsa_buy_number()%></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:800px;;height:300px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:780px;" id="gttable314">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px;">尺码</td>
						<td class="head" style="width:60px;">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-314-<%=present.getSsp_index()%> comm-314">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("315".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="table315">
			促销规则：买满多少金额商品送赠品<br/>
			促销范围：全场<br />
			<div style="float:left;width:260px;;height:300px; position:relative; left:0px; border:#cfcbca solid 0pt;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:240px;">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head">买满金额</td>
					</tr>
					<%for(T_Shop_Sale_All model_All:shop_sale_allList){ %>
					<tr onclick="javascript:doShowProduct('315','<%=model_All.getSsa_index()%>')">
						<td align="center"><b><%=model_All.getSsa_index()%></b></td>
						<td align="right"><%=model_All.getSsa_buy_full()%></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="float:left;width:600px;;height:300px; position:relative; left:0px; border:solid 0pt #cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable315">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px;">尺码</td>
						<td class="head" style="width:60px;">杯型</td>
						<td class="head" style="width:60px;">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-315-<%=present.getSsp_index()%> comm-315">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%-- <%}if("321".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail321">
			促销规则：买满N元送M元<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:558px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table321">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px">类别编号</td>
						<td class="head" style="width:150px">类别名称</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head">赠送金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_typeList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_typeList.get(i).getSst_tp_code() %></td>
						<td align="left"><%=shop_sale_typeList.get(i).getTp_name() %></td>
						<td align="right"><b><%=shop_sale_typeList.get(i).getSst_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_typeList.get(i).getSst_donation_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div> --%>
		<%}if("322".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail322">
			促销规则：买满N元送M元<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:558px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table322">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">类别编号</td>
						<td class="head" style="width:150px">类别名称</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head">减少金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_typeList.size();i++){ %>
					<tr>
						<td align="right"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_typeList.get(i).getSst_tp_code() %></td>
						<td align="left"><%=shop_sale_typeList.get(i).getTp_name() %></td>
						<td align="right"><b><%=shop_sale_typeList.get(i).getSst_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_typeList.get(i).getSst_reduce_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("323".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail323">
			促销规则：买满N元加M元送商品<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:558px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table323">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">类别编号</td>
						<td class="head" style="width:150px">类别名称</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head">增加金额</td>
					</tr>
					<%for(T_Shop_Sale_Type model_Type:shop_sale_typeList){ %>
					<tr onclick="javascript:doShowProduct('323','<%=model_Type.getSst_index()%>')">
						<td align="center"><b><%=model_Type.getSst_index()%></b></td>
						<td align="left"><%=model_Type.getSst_tp_code() %></td>
						<td align="left"><%=model_Type.getTp_name() %></td>
						<td align="right"><b><%=model_Type.getSst_buy_full() %></b></td>
						<td align="right"><b><%=model_Type.getSst_increased_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:600px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:580px;" id="gttable323">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-323-<%=present.getSsp_index()%> comm-323">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("324".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail324">
			促销规则：买满多少个商品送赠品<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:458px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:440px;" id="table324">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">类别编号</td>
						<td class="head" style="width:150px">类别名称</td>
						<td class="head">买满数量</td>
					</tr>
					<%for(T_Shop_Sale_Type model_Type:shop_sale_typeList){ %>
					<tr onclick="javascript:doShowProduct('324','<%=model_Type.getSst_index()%>')">
						<td align="center"><b><%=model_Type.getSst_index()%></b></td>
						<td align="left"><%=model_Type.getSst_tp_code()%></td>
						<td align="left"><%=model_Type.getTp_name()%></td>
						<td align="right"><b><%=model_Type.getSst_buy_number()%></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:618px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable324">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-324-<%=present.getSsp_index()%> comm-324">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("325".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail325">
			促销规则：买满多少金额送赠品<br/>
			促销范围：类别<br/>
			<div style="overflow-x:hidden;width:458px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:440px;" id="table325">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">类别编号</td>
						<td class="head" style="width:150px">类别名称</td>
						<td class="head">买满金额</td>
					</tr>
					<%for(T_Shop_Sale_Type model_Type:shop_sale_typeList){ %>
					<tr onclick="javascript:doShowProduct('325','<%=model_Type.getSst_index()%>')">
						<td align="center"><b><%=model_Type.getSst_index()%></b></td>
						<td align="left"><%=model_Type.getSst_tp_code()%></td>
						<td align="left"><%=model_Type.getTp_name()%></td>
						<td align="right"><b><%=model_Type.getSst_buy_full()%></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:618px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable325">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-325-<%=present.getSsp_index()%> comm-325">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%-- <%}if("331".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail331">
			促销规则：买满N元送M元<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:558px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table331">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">品牌编号</td>
						<td class="head" style="width:150px">品牌名称</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">赠送金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_brandList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_brandList.get(i).getSsb_bd_code() %></td>
						<td align="left"><%=shop_sale_brandList.get(i).getBd_name() %></td>
						<td align="right"><b><%=shop_sale_brandList.get(i).getSsb_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_brandList.get(i).getSsb_donation_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div> --%>
		<%}if("332".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail332">
			促销规则：买满N元减M元<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:558px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table332">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">品牌编号</td>
						<td class="head" style="width:150px;">品牌名称</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">减少金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_brandList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_brandList.get(i).getSsb_bd_code() %></td>
						<td align="left"><%=shop_sale_brandList.get(i).getBd_name() %></td>
						<td align="right"><b><%=shop_sale_brandList.get(i).getSsb_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_brandList.get(i).getSsb_reduce_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("333".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail333">
			促销规则：买满N元加M元送商品<br/>
			促销范围：品牌<br/>
			<input value="添加品牌" type="button" onclick="doQueryBrand()"/> <br />
			<div style="overflow-x:hidden;width:558px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table333">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">品牌编号</td>
						<td class="head" style="width:150px">品牌名称</td>
						<td class="head" style="width:100px;">买满金额</td>
						<td class="head">增加金额</td>
					</tr>
					<%for(T_Shop_Sale_Brand model_Brand:shop_sale_brandList){ %>
					<tr onclick="javascript:doShowProduct('333','<%=model_Brand.getSsb_index()%>')">
						<td align="center"><b><%=model_Brand.getSsb_index()%></b></td>
						<td align="left"><%=model_Brand.getSsb_bd_code() %></td>
						<td align="left"><%=model_Brand.getBd_name() %></td>
						<td align="right"><b><%=model_Brand.getSsb_buy_full() %></b></td>
						<td align="right"><b><%=model_Brand.getSsb_increased_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:618px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable333">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-333-<%=present.getSsp_index()%> comm-333">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("334".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail334">
			促销规则：买满多少个商品送赠品<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:458px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:440px;" id="table334">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">品牌编号</td>
						<td class="head" style="width:150px">品牌名称</td>
						<td class="head" >买满数量</td>
					</tr>
					<%for(T_Shop_Sale_Brand model_Brand:shop_sale_brandList){ %>
					<tr onclick="javascript:doShowProduct('334','<%=model_Brand.getSsb_index()%>')">
						<td align="center"><b><%=model_Brand.getSsb_index()%></b></td>
						<td align="left"><%=model_Brand.getSsb_bd_code() %></td>
						<td align="left"><%=model_Brand.getBd_name() %></td>
						<td align="right"><b><%=model_Brand.getSsb_buy_number() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:618px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable334">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-334-<%=present.getSsp_index()%> comm-334">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("335".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail335">
			促销规则：买满多少金额送赠品<br/>
			促销范围：品牌<br/>
			<div style="overflow-x:hidden;width:458px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:440px;" id="table335">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">品牌编号</td>
						<td class="head" style="width:150px">品牌名称</td>
						<td class="head" >买满金额</td>
					</tr>
					<%for(T_Shop_Sale_Brand model_Brand:shop_sale_brandList){ %>
					<tr onclick="javascript:doShowProduct('335','<%=model_Brand.getSsb_index()%>')">
						<td align="center"><b><%=model_Brand.getSsb_index()%></b></td>
						<td align="left"><%=model_Brand.getSsb_bd_code() %></td>
						<td align="left"><%=model_Brand.getBd_name() %></td>
						<td align="right"><b><%=model_Brand.getSsb_buy_full() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:618px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:600px;" id="gttable335">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-335-<%=present.getSsp_index()%> comm-335">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%-- <%}if("341".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail341">
			促销规则：买满N元送M元<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:658px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:640px;" id="table341">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="">商品名称</td>
						<td class="head" style="width:100px">零售价</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head" style="width:100px">赠送金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_donation_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div> --%>
		<%}if("342".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail342">
			促销规则：买满N元减M元<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:658px;;height:350px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:640px;" id="table342">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:100px">零售价</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head">减少金额</td>
					</tr>
					<%for(int i=0;i<shop_sale_productList.size();i++){ %>
					<tr>
						<td  align="center"><b><%=i+1 %></b></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_no() %></td>
						<td align="left"><%=shop_sale_productList.get(i).getPd_name() %></td>
						<td align="right"><%=shop_sale_productList.get(i).getPd_sell_price()%></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_buy_full() %></b></td>
						<td align="right"><b><%=shop_sale_productList.get(i).getSsp_reduce_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("343".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail343">
			促销规则：买满N元加M元送商品<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:658px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:640px;" id="table343">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:100px">零售价</td>
						<td class="head" style="width:100px">买满金额</td>
						<td class="head">增加金额</td>
					</tr>
					<%for(T_Shop_Sale_Product model_Product:shop_sale_productList){ %>
					<tr onclick="javascript:doShowProduct('343','<%=model_Product.getSsp_index()%>')">
						<td align="center"><b><%=model_Product.getSsp_index()%></b></td>
						<td align="left"><%=model_Product.getPd_no() %></td>
						<td align="left"><%=model_Product.getPd_name() %></td>
						<td align="right"><b><%=model_Product.getPd_sell_price()%></b></td>
						<td align="right"><b><%=model_Product.getSsp_buy_full() %></b></td>
						<td align="right"><b><%=model_Product.getSsp_increased_amount() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:600px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:580px;" id="gttable343">
					<tr>
						<td class="head" style="width:40px;">序号</td>
						<td class="head" style="width:150px;">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px;">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-343-<%=present.getSsp_index()%> comm-343">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("344".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail344">
			促销规则：买满多少个商品送赠品<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:558px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table344">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:100px">零售价</td>
						<td class="head">买满数量</td>
					</tr>
					<%for(T_Shop_Sale_Product model_Product:shop_sale_productList){ %>
					<tr onclick="javascript:doShowProduct('344','<%=model_Product.getSsp_index()%>')">
						<td align="center"><b><%=model_Product.getSsp_index()%></b></td>
						<td align="left"><%=model_Product.getPd_no() %></td>
						<td align="left"><%=model_Product.getPd_name() %></td>
						<td align="right"><%=model_Product.getPd_sell_price()%></td>
						<td align="right"><%=model_Product.getSsp_buy_number() %></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:600px;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:580px;" id="gttable344">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-344-<%=present.getSsp_index()%> comm-344">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%}if("345".equals(t_Shop_Sale.getSs_model())){ %>
		<div class="class_cbody" style="width: 99%;height: 440px;" id="detail345">
			促销规则：买满多少金额商品送赠品<br/>
			促销范围：商品<br/>
			<div style="overflow-x:hidden;width:558px;;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:540px;" id="table345">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="">商品名称</td>
						<td class="head" style="width:80px">零售价</td>
						<td class="head" style="width:100px">买满金额</td>
					</tr>
					<%for(T_Shop_Sale_Product model_Product:shop_sale_productList){ %>
					<tr onclick="javascript:doShowProduct('345','<%=model_Product.getSsp_index()%>')">
						<td align="center"><b><%=model_Product.getSsp_index()%></b></td>
						<td align="left"><%=model_Product.getPd_no() %></td>
						<td align="left"><%=model_Product.getPd_name() %></td>
						<td align="right"><%=model_Product.getPd_sell_price()%></td>
						<td align="right"><%=model_Product.getSsp_buy_full() %></td>
					</tr>
					<%} %>
				</table>
			</div>
			<div style="overflow-x:hidden;width:600px;height:200px; position:relative; left:0px; border-style:solid; border-width:0pt;  border-color:#cfcbca;overflow-x:hidden; overflow-y:auto;">
				<table class="main_table" style="width:580px;" id="gttable345">
					<tr>
						<td class="head" style="width:40px">序号</td>
						<td class="head" style="width:150px">款号</td>
						<td class="head" style="width:150px">商品名称</td>
						<td class="head" style="width:80px">颜色</td>
						<td class="head" style="width:60px">尺码</td>
						<td class="head" style="width:60px">杯型</td>
						<td class="head">数量</td>
					</tr>
					<%for(T_Shop_Sale_Present present:shop_sale_presentList){ %>
					<tr class="product-345-<%=present.getSsp_index()%> comm-345">
						<td align="center"><b><%=present.getSsp_index()%></b></td>
						<td align="left"><%=present.getPd_no() %></td>
						<td align="left"><%=present.getPd_name() %></td>
						<td align="left"><%=present.getCr_name() %></td>
						<td align="center"><%=present.getSz_name() %></td>
						<td align="center"><%=StringUtil.trimString(present.getBr_name())%></td>
						<td align="right"><b><%=present.getSsp_count() %></b></td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%} %>
		<div style="width:99%;text-align:left;">
			<input type="button" class="btn_pn" onclick="javascript:goUp();" value="上一步" />
		</div>
	</div>
  </div>
</div>
</form>
<script src="<%=basePath%>data/shop/sale/sale_view.js"></script>
<script type="text/javascript">
	<%-- var batchLimit = '<%=batchLimit%>';
	
	function saveTarget(){
		document.form1.action="<%=basePath%>office/saveTarget.action";
		document.form1.submit();
	} --%>
	
	<%-- function auditingUpdate(spCode,operateType){
		if(batchLimit.indexOf('A') >= -1){ 
			//判断终止促销方案是否过期，如果过期则不再终止
			var SP_End_Date = document.getElementById("SP_End_Date").value;
			//获取当前时间
			var myDate = new Date();
			var year = myDate.getFullYear();
			var month = myDate.getMonth()+1;
			var date = myDate.getDate();
			if(month<10){
				month = "0"+month;
			}
			if(date<10){
				date = "0"+date;
			}
			var nowDate = year+"-"+month+"-"+date;
			
			var reg=new RegExp("-","g"); //创建正则RegExp对象
			var tempEndDate=SP_End_Date.replace(reg,"\/");//促销方案结束时间
			var tempNowDate=nowDate.replace(reg,"\/");//当前时间
			if(Date.parse(new Date(tempNowDate))>Date.parse(new Date(tempEndDate))){
				Public.tips({type:1,content:'该促销方案已过期!'});
				return;
			}
			document.getElementById("stop").disabled=true;
			$.dialog({ 
			   	id:'auditingUpdate',
			   	title:'终止促销',
			   	max: false,
			   	min: false,
			   	width:'320px',
			   	height:'220px',
			   	fixed:false,
			   	data:{arr:'arr',SP_Stop_Cause:'SP_Stop_Cause'},
			   	//设置是否拖拽(false:禁止，true可以移动)
			   	drag: true,
			   	resize:false,
			   	//设置页面加载处理
			   	content:'url:<%=basePath%>pages/sales/SalesStopCause.jsp',
			   	lock:true,
			   	close:function (){
			   		var arr = document.getElementById("arr").value;		   		
			   		if(arr=="2"){
			   			var SP_Stop_Cause = $("#SP_Stop_Cause").val();
			   			SP_Stop_Cause = Public.encodeURI(SP_Stop_Cause); 
			   			var url = "spCode="+spCode+"&SP_Stop_Cause="+SP_Stop_Cause;
			   			$.ajax({
							type:"get",
							url:"<%=basePath%>sales/stopSalesPromtotion.action?"+url,
							data:"",
							cache:false,
							dataType:"html",
							success:function(data){
							  if(data == "success"){
								 	window.location.replace("<%=basePath%>sales/querySalesDetil.action?spCode="+spCode);				 	
								}else{
									Public.tips({type:1,content:'处理失败,请联系管理员!'});
									document.getElementById("stop").disabled=false;
								}
							}
						 });
			   		}else{
			   			document.getElementById("stop").disabled=false;
			   		}
			   	}
		    });	
		}else{
			Public.tips({type:1,content:'您没有终止权限!'});
		}
	} --%>
</script>
</body>
</html>