<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.notpassbarcode {
	background: url('<%=basePath%>resources/grid/images/poptxt1.png');
	width: 198px;
	height: 143px;
	position: absolute;
	font-size: 12px;
}
.notcheckbody {
	float: left;
	width: 155px;
	margin: 15px 0 0 10px;
	height: 110px;
	overflow-x: hidden;
	overflow-y: auto;
	SCROLLBAR-HIGHLIGHT-COLOR: #fee7cf;
	SCROLLBAR-ARROW-COLOR: #fee7cf;
}
.notcheckbody p {
	float: left;
	width: 155px;
	height: 16px;
	overflow: hidden;
	padding-left: 5px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 

function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="at_type" name="at_type" value="${at_type }"/>
<input type="hidden" id="allot_up" name="allot_up" value="${allot_up }"/>
<input type="hidden" id="shop_type" name="shop_type" value="${sessionScope.user.shoptype }"/>
<input type="hidden" id="at_isdraft" name="at_isdraft" value="0"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="保存单据"/>
	        <input id="btn-copy" class="t-btn btn-bblue" type="button" value="复制单据"/>
	        <input id="btn-import" class="t-btn btn-black" type="button" title="" value="盘点机导入"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div >
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
		<tr>
			<td width="220">
				货号查询：<input name="pd_no" type="text" id="pd_no" class="ui-input ui-input-ph w120" onkeyup="javascript:if (event.keyCode==13){Choose.selectProduct();};"/>
               <input type="button" value=" " class="btn_select" onclick="javascript:Choose.selectProduct();"/>
			</td>
			<td width="280">
				扫描条码：<input name="text" type="text" id="barcode" onkeyup="javascript:if (event.keyCode==13){Choose.barCode()};" class="ui-input ui-input-ph w146"/>
				<input id="barcode_amount" class="ui-input ui-input-ph" value="1" style="width:20px" maxlength="4"
						onkeypress="return onlyPositiveNumber(event)"
						onkeyup="javascript:valNumber(this);"/>
			</td>
			<td width="200" id="priceTypeTd">
			    <label class="radio radiow7">
			        <input type="radio" name="priceType" checked="checked" value="1"/>
			        <b>最近配送价</b>
			    </label>
			    <label class="radio radiow6">
			        <input type="radio" name="priceType" value="2"/>
			        <b>折扣率:</b>
			    </label>
			    <input name="text" type="text" id="sp_rate" disabled="disabled" class="ui-input ui-input-ph" style="width:40px; color:red;" 
			    		value="1.0" maxlength="4"
			           onkeyup="javascript:doOnlyDouble(this);"
			           onkeypress="javascript:if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
			           onkeydown="javascript:doOnlyDouble(this);"
			           />
			</td>
			<td>
				<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
			    <a class="t-btn btn-red" id="btnClear">清除</a>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">配货门店：</td>
			<td width="165px">
				<c:choose>
					<c:when test="${ sessionScope.user.shoptype eq 1 or (sessionScope.user.shoptype eq 2 and allot_up eq 0)}">
						<input class="main_Input" type="text" readonly="readonly" name="shop_name" id="shop_name" value="" style="width:122px"/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
						<input type="hidden" name="at_shop_code" id="at_shop_code" value="" />
					</c:when>
					<c:otherwise>
						<input class="main_Input w146" type="text" readonly="readonly" name="shop_name" id="shop_name" value="${ sessionScope.user.shop_name}"/>
						<input type="hidden" name="at_shop_code" id="at_shop_code" value="${ sessionScope.user.us_shop_code}" />
					</c:otherwise>
				</c:choose>
			</td>
			<td align="right" width="60px">
				<c:choose>
					<c:when test="${ sessionScope.user.shoptype eq 1 or (sessionScope.user.shoptype eq 2 and allot_up eq 0)}">
						<!-- 总公司、分公司的配货发货单退货确认单 -->
						<c:if test="${at_type eq 0}">
							发货仓库：
						</c:if>
						<c:if test="${at_type eq 1}">
							收货仓库：
						</c:if>
					</c:when>
					<c:otherwise>
						<!-- 分公司、加盟店、合伙店的配货申请单、退货申请单 -->
						<c:if test="${at_type eq 0}">
							收货仓库：
						</c:if>
						<c:if test="${at_type eq 1}">
							退货仓库：
						</c:if>
					</c:otherwise>
				</c:choose>
			</td>
			<td width="165px">
				<c:choose>
					<c:when test="${ sessionScope.user.shoptype eq 1 or (sessionScope.user.shoptype eq 2 and allot_up eq 0)}">
						<!-- 总公司、分公司的配货发货单退货确认单 -->
						<c:if test="${at_type eq 0}">
							<input class="main_Input" type="text" readonly="readonly" name="outdepot_name" id="outdepot_name" value="" style="width:122px"/>
							<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot('at_outdp_code','outdepot_name');"/>
							<input type="hidden" name="at_outdp_code" id="at_outdp_code" value="" />
						</c:if>
						<c:if test="${at_type eq 1}">
							<input class="main_Input" type="text" readonly="readonly" name="indepot_name" id="indepot_name" value="" style="width:122px"/>
							<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot('at_indp_code','indepot_name');"/>
							<input type="hidden" name="at_indp_code" id="at_indp_code" value="" />
						</c:if>
					</c:when>
					<c:otherwise>
						<!-- 分公司、加盟店、合伙店的配货申请单、退货申请单 -->
						<c:if test="${at_type eq 0}">
							<input class="main_Input" type="text" readonly="readonly" name="indepot_name" id="indepot_name" value="" style="width:122px"/>
							<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot('at_indp_code','indepot_name');"/>
							<input type="hidden" name="at_indp_code" id="at_indp_code" value="" />
						</c:if>
						<c:if test="${at_type eq 1}">
							<input class="main_Input" type="text" readonly="readonly" name="outdepot_name" id="outdepot_name" value="" style="width:122px"/>
							<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot('at_outdp_code','outdepot_name');"/>
							<input type="hidden" name="at_outdp_code" id="at_outdp_code" value="" />
						</c:if>
					</c:otherwise>
				</c:choose>
				
			</td>
			<td align="right" width="60px">手工单号：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" name="at_handnumber" id="at_handnumber" value="" />
			</td>
			<td align="right" width="60px">单据性质：</td>
			<td>
				<span class="ui-combo-wrap" id="span_property"></span>
				<input type="hidden" name="at_property" id="at_property"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="at_manager" id="at_manager" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right">制单日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="at_date" id="at_date" onclick="WdatePicker()" value="" />
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input w146" type="text" name="at_amount" id="at_amount" value="" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="at_money" value="" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right" width="60px">备注：</td>
			<td colspan="3">
				<input class="main_Input" style="width:390px" type="text" name="at_remark" id="at_remark" value=""/>
			</td>
		</tr>
		
	</table>
</div>
<input type="hidden" id="CheckNotPassTextShow" value=""/>
<div id="BarCodeCheckNotPass" class="notpassbarcode" title="双击关闭错误提示" style="display:none;" ondblclick="this.style.display='none'">
 	<span class="notcheckbody" id="CheckNotPassText"></span>
</div>

</form>
<script src="<%=basePath%>data/sort/allot/allot_add.js"></script>
</body>
</html>