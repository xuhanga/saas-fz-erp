<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="at_type" name="at_type" value="${at_type }"/>
<input type="hidden" id="allot_up" name="allot_up" value="${allot_up }"/>
<input type="hidden" id="fromJsp" name="fromJsp" value="warn"/>
<input type="hidden" id="shop_type" name="shop_type" value="${sessionScope.user.shoptype }"/>
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">审核状态：</td>
									<td id="ar_state">
			                            <label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="" checked="checked"/>全部</label>
			                            <c:choose>
											<c:when test="${ at_type eq 0 and (sessionScope.user.shoptype eq 1 or sessionScope.user.shoptype eq 2)}">
												<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="0" />待审批</label>
										  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="1" />已审批</label>
										  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="5" />已拒收</label>
											</c:when>
											<c:when test="${at_type eq 1 and (sessionScope.user.shoptype eq 1 or sessionScope.user.shoptype eq 2)}">
												<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="0" />待审批</label>
										  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="3" />在途</label>
											</c:when>
											<c:when test="${at_type eq 0 and (sessionScope.user.shoptype eq 4 or sessionScope.user.shoptype eq 5)}">
												<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="0" />待审批</label>
										  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="3" />在途</label>
											</c:when>
											<c:when test="${ at_type eq 1 and (sessionScope.user.shoptype eq 4 or sessionScope.user.shoptype eq 5)}">
												<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="0" />待审批</label>
										  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="1" />已审批</label>
										  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="5" />已拒收</label>
											</c:when>
										</c:choose>
			                            
								  	 	<!-- <label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="0" />待审批</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="1" />已审批</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="2" />已退回</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="3" />在途</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="4" />完成</label>
								  	 	<label class="radio" style="width:30px"><input name="radio_ar_state" type="radio" value="5" />已拒收</label> -->
								  	 	<input type="hidden" id="at_ar_state" name="at_ar_state" value=""/>
							  	 	</td>
								 </tr>
								  <tr style="display:${sessionScope.user.shoptype eq 1 or (sessionScope.user.shoptype eq 2 and allot_up eq 0) ? '' : 'none' };">
									  <td align="right">配货门店：</td>
										<td>
										  	<input class="main_Input" type="text" id="shop_name" name="shop_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
										     <input type="hidden" name="at_shop_code" id="at_shop_code" value=""/>
								  		</td>
								  </tr>
								  <tr style="display:${(at_type eq 0 and (sessionScope.user.shoptype eq 1 or (sessionScope.user.shoptype eq 2 and allot_up eq 0)))
								  						or (at_type eq 1 and (sessionScope.user.shoptype eq 3 or (sessionScope.user.shoptype eq 2 and allot_up eq 1))) 
								  						? '' : 'none' };">
										<td align="right">${at_type eq 0 ? '发':'退' }货仓库：</td>
										<td>
										  	<input class="main_Input" type="text" id="outdepot_name" name="outdepot_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot('at_outdp_code','outdepot_name');"/>
										     <input type="hidden" name="at_outdp_code" id="at_outdp_code" value=""/>
								  		</td>
									</tr>
									<tr style="display:${(at_type eq 1 and (sessionScope.user.shoptype eq 1 or (sessionScope.user.shoptype eq 2 and allot_up eq 0))) 
														or (at_type eq 0 and (sessionScope.user.shoptype eq 3 or (sessionScope.user.shoptype eq 2 and allot_up eq 1))) 
														? '' : 'none' };">
										<td align="right">收货仓库：</td>
										<td>
										  	<input class="main_Input" type="text" id="indepot_name" name="indepot_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryDepot('at_indp_code','indepot_name');"/>
										     <input type="hidden" name="at_indp_code" id="at_indp_code" value=""/>
								  		</td>
									</tr>
									<tr>
										<td align="right">经办人员：</td>
										<td>
										  	<input class="main_Input" type="text" readonly="readonly" name="at_manager" id="at_manager" value="" style="width:170px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
								  		</td>
									</tr>
									<tr>
										<td align="right">单据编号：</td>
										<td>
											<input type="text" id="at_number" name="at_number" class="main_Input" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/sort/allot/allot_list.js"></script>
</body>
</html>