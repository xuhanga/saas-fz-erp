<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="st_id" name="st_id" value="${settle.st_id }"/>
<input type="hidden" id="st_number" name="st_number" value="${settle.st_number }"/>
<input type="hidden" id="st_ar_state" name="st_ar_state" value="${settle.st_ar_state }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-approve" class="t-btn btn-green" type="button" value="审核" style="display:none;"/>
	        <input id="btn-reverse" class="t-btn btn-red" type="button" value="反审核" style="display:none;"/>
	        <input id="btn-print" class="t-btn btn-cblue" type="button" value="打印预览" onclick="javascript:handle.doPrint();"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div >
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">店铺名称：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="shop_name" id="shop_name" value="${settle.shop_name }"/>
				<input type="hidden" name="st_shop_code" id="st_shop_code" value="${settle.st_shop_code }" />
			</td>
			<td align="right" width="80px">银行账户：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="ba_name" id="ba_name" value="${settle.ba_name }"/>
				<input type="hidden" name="st_ba_code" id="st_ba_code" value="${settle.st_ba_code }" />
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" readonly="readonly" name="st_manager" id="st_manager" value="${settle.st_manager }"/>
			</td>
			<td align="right" width="75px">单据编号：</td>
			<td>
				<input readonly type="text" class="main_Input w146"  value="${settle.st_number }" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">应收金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="unreceive" readonly="readonly"
					value="<fmt:formatNumber value='${shop.sp_receivable - shop.sp_received}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">预收款余额：</td>
			<td>
				<input class="main_Input w146" type="text" id="sp_prepay" readonly="readonly"
					value="<fmt:formatNumber value='${shop.sp_prepay}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">实际欠款：</td>
			<td>
				<input class="main_Input w146" type="text" id="realdebt" readonly="readonly"
					value="<fmt:formatNumber value='${shop.sp_receivable - shop.sp_received - shop.sp_prepay}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">收款日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate w146"  name="st_date" id="st_date" value="${settle.st_date }" />
			</td>
			
		</tr>
		<tr class="list">
			<td align="right">优惠金额：</td>
			<td>
				<input class="main_Input w146" type="text" id="st_discount_money" readonly="readonly"
					value="<fmt:formatNumber value='${settle.st_discount_money}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">使用预收款：</td>
			<td>
				<input class="main_Input w146" type="text" id="st_prepay" readonly="readonly"
					value="<fmt:formatNumber value='${settle.st_prepay}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">实际收款：</td>
			<td>
				<input class="main_Input w146" type="text" id="st_received" readonly="readonly"
					value="<fmt:formatNumber value='${settle.st_received}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
			<td align="right">转入预收款：</td>
			<td>
				<input class="main_Input w146" type="text" id="st_receivedmore" readonly="readonly"
					value="<fmt:formatNumber value='${settle.st_receivedmore}' pattern='#.##' minFractionDigits='2' />"/>
			</td>
		</tr>
		<tr class="list last">
			
			<td align="right">备注：</td>
			<td colspan="5">
				<input class="main_Input" style="width:655px" type="text" name="st_remark" id="st_remark" value="${settle.st_remark }" readonly="readonly"/>
			</td>
			<td align="right">剩余欠款：</td>
			<td>
				<c:if test="${settle.st_ar_state eq 0}">
					<input class="main_Input w146" type="text" id="leftdebt" readonly="readonly"
						value="<fmt:formatNumber value='${shop.sp_receivable - shop.sp_received - shop.sp_prepay - settle.st_discount_money - settle.st_received - settle.st_receivedmore}' pattern='#.##' minFractionDigits='2' />"/>
				</c:if>
				<c:if test="${settle.st_ar_state eq 1}">
					<input class="main_Input w146" type="text" id="leftdebt" readonly="readonly"
						value="<fmt:formatNumber value='${shop.sp_receivable - shop.sp_received - shop.sp_prepay}' pattern='#.##' minFractionDigits='2' />"/>
				</c:if>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/sort/settle/settle_view.js"></script>
</body>
</html>