<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.main_Input{
	width: 195px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ba_id" name="ba_id" value="${batch.ba_id }"/>
<input type="hidden" id="ba_number" name="ba_number" value="${batch.ba_number }"/>
<input type="hidden" id="ck_number" name="ck_number" value="${check.ck_number }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	        <span id="errorTip" class="errorTip"></span>
	        <span style="float:right; padding-right:10px;">
	       	 	<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
	        </span>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">盘点批次：</td>
			<td width="200px">
				<input class="main_Input" type="text" value="${batch.ba_number}" readonly="readonly" />
			</td>
			<td align="right" width="60px">盘点仓库：</td>
			<td width="200px">
				<input class="main_Input" type="text" readonly="readonly" value="${batch.depot_name }"/>
			</td>
				<c:choose>
       				<c:when test="${batch.ba_scope eq 0}">
       					<td align="right" width="60px">全场盘点：</td>
       					<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="全场"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 1}">
       					<td align="right" width="60px">类别盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 2}">
       					<td align="right" width="60px">品牌盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 3}">
       					<td align="right" width="60px">单品盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 4}">
       					<td align="right" width="60px">年份盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:when test="${batch.ba_scope eq 5}">
       					<td align="right" width="60px">季节盘点：</td>
						<td width="200px"><input class="main_Input" type="text" readonly="readonly" value="${batch.ba_scope_name}"/></td>
       				</c:when>
       				<c:otherwise>
       				</c:otherwise>
       			</c:choose>
			<td align="right" width="60px">发起时间：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate" value="${batch.ba_date}" style="width:197px;"/>
			</td>
		</tr>
		<tr class="list">
			<td align="right">盘点备注：</td>
			<td>
				<input class="main_Input" type="text" value="${batch.ba_remark}" readonly="readonly"/>
			</td>
			<td align="right">库存数量：</td>
			<td>
				<input class="main_Input" type="text" name="ck_stockamount" id="ck_stockamount" value="" readonly="readonly"/>
			</td>
			<td align="right">盘点数量：</td>
			<td>
				<input class="main_Input" type="text" name="ck_amount" id="ck_amount" value="" readonly="readonly"/>
			</td>
			<td align="right">差异数量：</td>
			<td>
				<input class="main_Input" type="text" id="sub_amount" value="" readonly="readonly"/>
			</td>
		</tr>
		<tr class="list last">
		
			<td align="right">经办人员：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="ck_manager" id="ck_manager" value="${check.ck_manager}"/>
			</td>
			<td align="right">备注：</td>
			<td colspan="3">
				<input class="main_Input" type="text" name="ck_remark" id="ck_remark" value="${check.ck_remark}"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/stock/check/check_check_view.js"></script>
</body>
</html>