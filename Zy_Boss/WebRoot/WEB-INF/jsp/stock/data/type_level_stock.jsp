<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="tp_upcode" id="tp_upcode" value="${tp_upcode }"/>
<div class="wrapper">
	<div class="mod-search cf">
		<div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
				<span style="float:left" class="ui-btn menu-btn"> 
					<strong>商品名称</strong>
					<input class="main_Input" type="text" readonly="readonly" name="pd_name" id="pd_name" value="" style="width:170px;"/>
					<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProduct();"/>
					<input type="hidden" id="pd_code" name="pd_code" value=""/>
					<b></b>
				</span>
				<div class="con" style="width:340px;">
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>
									<td align="right">店铺名称：</td>
									<td>
										<input class="main_Input" type="text" id="shop_name" name="shop_name" value="" readonly="readonly" style="width:170px; " /> 
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();" /> 
										<input type="hidden" name="shopCodes" id="shopCodes" value="" />
									</td>
								</tr>
								<tr>
									  <td align="right">商品品牌：</td>
									  <td>
									  	<input class="main_Input" type="text" readonly="readonly" name="bd_name" id="bd_name" value="" style="width:170px;"/>
										<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryBrand();"/>
										<input type="hidden" id="bd_code" name="bd_code" value=""/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品季节：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_season"></span>
										<input type="hidden" name="pd_season" id="pd_season"/>
							  		</td>
								  </tr>
								  <tr>
									  <td align="right">商品年份：</td>
									  <td>
									  	<span class="ui-combo-wrap" id="span_pd_year"></span>
										<input type="hidden" name="pd_year" id="pd_year"/>
							  		</td>
								  </tr>
							</table>
						</li>
						<li style="float:right;margin-top: 10px;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a> 
							<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="fl-m">
			<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
		<div class="fr">
			<a href="javascript:void(0);" class="ui-btn" id="btn_close">返回</a>
		</div>
	</div>
		<div class="grid-wrap" id="list-grid">
			<table id="grid"></table>
			<div id="page"></div>
		</div>
	</div>
</form>
<script src="<%=basePath%>data/stock/data/type_level_stock.js"></script>
</body>
</html>