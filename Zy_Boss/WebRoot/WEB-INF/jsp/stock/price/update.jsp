<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="pc_id" name="pc_id" value="${price.pc_id }"/>
<input type="hidden" id="pc_number" name="pc_number" value="${price.pc_number }"/>
<input type="hidden" id="pc_isdraft" name="pc_isdraft" value="0"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red iconfont" type="button" value="保存单据"/>
	        <input id="btn_close" class="t-btn iconfont" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div>
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
		<tr>
			<td width="220">
				货号查询：<input name="pd_no" type="text" id="pd_no" class="ui-input ui-input-ph w120" onkeyup="javascript:if (event.keyCode==13){Choose.selectProduct();};"/>
               <input type="button" value=" " class="btn_select" onclick="javascript:Choose.selectProduct();"/>
			</td>
			<td>
			    <a class="t-btn btn-red" id="btnClear">清除</a>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">经办人员：</td>
			<td width="220px">
				<input class="main_Input" type="text" readonly="readonly" name="pc_manager" id="pc_manager" value="${price.pc_manager }" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right" width="60px">申请日期：</td>
			<td width="220px">
				<input readonly type="text" class="main_Input Wdate"  name="pc_date" id="pc_date" onclick="WdatePicker()" value="${price.pc_date }" style="width:198px;"/>
			</td>
			<td align="right" width="60px">备注：</td>
			<td>
				<input class="main_Input" type="text" name="pc_remark" id="pc_remark" value="${price.pc_remark }" style="width:397px;"/>
			</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/stock/price/price_add.js"></script>
</body>
</html>