<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.tablelist{width:100%;border:#ddd solid 1px; border-width:1px 0 0 1px;}
.tablelist td{background:#fff;border:#ddd solid 1px;line-height:30px; border-width:0 1px 1px 0;}
.tablelist th{border:#ddd solid 1px;line-height:30px;background:#f1f1f1; border-width:0 1px 1px 0;}

.datalist{width:100%;border:#ddd solid 1px; border-width:1px 0 0 1px;}
.datalist td{background:#fff;border:#ddd solid 1px;line-height:30px; border-width:0 1px 1px 0;}
.datalist td.list-left{padding-left:2px;}
.datalist th{border:#ddd solid 1px;line-height:30px;background:#f1f1f1; border-width:0 1px 1px 0;}

.listnum{background:#f1f1f1;text-align:center;}
.stempdiv{margin:0px;}
.areabody{border:#ddd solid 1px;}
.objlist{line-height:40px;}
.objlist a{background:#eee;border:#ddd solid 1px;padding:5px 8px;margin-left:5px;}
.objlist span{background:#fff;border:#ddd solid 1px;padding:5px 8px;margin-left:5px;}
.objlist .thePage{background:#377bda;border:#418cb6 solid 1px;color:#fff;padding:5px 8px;margin-left:5px;}

.uboxstyle .select_box{width:82px;height:28px;}
.uboxstyle div.tag_select{border:#ddd solid 1px;display:block;color:#555;height:28px;background:transparent url("<%=basePath%>resources/img/dropdown1.png") no-repeat right 0 #fff;padding:0 10px;line-height:28px;}
.uboxstyle div.tag_select_hover{border:#ddd solid 1px;display:block;color:#555;height:28px;background:transparent url("<%=basePath%>resources/img/dropdown1.png") no-repeat right -30px #fff;padding:0 10px;line-height:28px;}
.uboxstyle div.tag_select_open{border:#ddd solid 1px;display:block;color:#555;height:28px;background:transparent url("<%=basePath%>resources/img/dropdown1.png") no-repeat right -30px #fff;padding:0 10px;line-height:28px;}
.uboxstyle ul.tag_options{background:#fff;border:#ddd solid 1px;border-top:0;position:absolute;padding:0;margin:0;list-style:none;padding:0 0 0px;margin:0;width:80px;max-height:200px;overflow-y:auto;}
.uboxstyle ul.tag_options li{display:block;padding:0 10px;height:28px;text-decoration:none;line-height:28px;color:#555;}
.uboxstyle ul.tag_options li.open_hover{background:#f1f1f1;color:#666}
.uboxstyle ul.tag_options li.open_selected{background:#ddd;color:#fff}

.head-input{height:26px;width:128px;line-height:26px}
.head-input:hover{border:1px solid #4bb8f3;height:28px;width:130px;line-height:26px}
.tableHeadSet{background:#fff;border-collapse:collapse;border:#ddd solid 1px;border-bottom:0;margin-top:10px;}
.tableHeadSet td{padding-bottom:5px;border:#ddd solid 1px;}
.tableHeadSet th{background:#f1f1f1;border-bottom:#ddd solid 1px;text-align:left;font-size:14px;line-height:40px;padding-left:10px;}
.tableHstab,.tableHstab td{border:0px;}
.tableHstab{margin-top:5px;}
.headSetFirst{background:#fff;}
.headSetFirst td{padding-top:5px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ueditor/ueditor.config.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ueditor/ueditor.all.js\"></sc"+"ript>");
</script> 
<script language="javascript">
function up_next(s_id){
	for(i=1;i<4;i++){
	   if(i==s_id){
		   document.getElementById("stemp"+i).style.display="";
		   document.getElementById("stempbt"+i).className="t-btn btn-green";
	   }else{
		   document.getElementById("stemp"+i).style.display="none";
		   document.getElementById("stempbt"+i).className="t-btn";
	   }
	}
}
</script>
</head>
<body id="main_content">

<div class="mainwra">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
     	<td width="60" align="right" style="display:none;">单据类型：</td>
     	<td width="60" align="right" style="display:none;">
			<select id="selectType" onchange="javascript:THISPAGE.initPrintList();">
				<c:forEach var="print" items="${prints}" varStatus="as">
					<option value="${print.pt_type}">${print.pt_name}</option>
				</c:forEach>
			</select> 
     	</td>
       <td width="60" align="right">模板名称：</td>
        <td width="170">
       		<input type="text" class="ui-input ui-input-ph w146" name="sp_name" id="sp_name" value=""/>
       		<input type="hidden" class="ui-input ui-input-ph w146" name="sp_id" id="sp_id" value=""/>
       	</td>
       	<td></td>
       <td width="420" align="right">
         <a class="t-btn btn-green" name="stempbt1" id="stempbt1" onclick="javascript:up_next(1)" >表头设置</a>
         <a class="t-btn" name="stempbt2" id="stempbt2" onclick="javascript:up_next(2)" >表格设置</a>
         <a class="t-btn" name="stempbt3" id="stempbt3" onclick="javascript:up_next(3)" >表尾设置</a>
         <a class="t-btn btn-red" id="btnSave" >保存</a>
         <a class="t-btn" id="btn_close" >返回</a>
       </td>
     </tr>
  </table>
    <div id="stemp1" class="stempdiv">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeadSet">
      <tr>
        <th colspan="3">表头设置</th>
      </tr>
      <tr>
        <td width="380" valign="top" style="border-bottom:0px;"><table class="tableHstab" width="360" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right" width="70">纸张高度：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="page_height" /></td>
            <td align="right" width="50">左边距：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="page_left" /></td>
            <td align="right" width="50">右边距：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="page_right" /></td>
            </tr>
          <tr>
            <td align="right">纸张宽度：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="page_width" /></td>
            <td align="right">上边距：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="page_top" /></td>
            <td align="right">下边距：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="page_bottom" /></td>
            </tr>
          <tr>
            <td align="right">打印方向：</td>
            <td colspan="5" id="radioDirection">
                 <input type="radio" name="page_direction" value="1" checked onclick="javascript:$('#page_direction').val($(this).val());"/>纵向
                 <input type="radio" name="page_direction" value="2" onclick="javascript:$('#page_direction').val($(this).val());"/>横向
                 <input type="text" id="page_direction" value="1"/>
            </td>
            </tr>
        </table></td>
        <td width="260" valign="top" style="border-bottom:0px;"><table class="tableHstab" width="240" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right" width="70">打印标题：</td>
            <td><input type="text" class="ui-input ui-input-ph w120" name="page_title" id="page_title" /></td>
          </tr>
          <tr>
            <td align="right">标题高度：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="title_height" /></td>
          </tr>
          <tr>
            <td align="right">标题字体：</td>
            <td>
                <div id="sefs" class="uboxstyle">
                  <select name="title_font" id="title_font">
                    <option value="宋体">宋体</option>
                    <option value="楷体">楷体</option>
                    <option value="微软雅黑">微软雅黑</option>
                  </select> 
                </div>
            </td>
          </tr>
          <tr>
            <td align="right">标题字号：</td>
            <td>
                <div id="sefs" class="uboxstyle">
                  <select name="title_size" id="title_size">
                    <option value="9">9 px</option>
                    <option value="10">10 px</option>
                    <option value="11">11 px</option>
                    <option value="12">12 px</option>
                    <option value="14">14 px</option>
                    <option value="16" selected="selected">16 px</option>
                    <option value="18">18 px</option>
                    <option value="20">20 px</option>
                    <option value="24">24 px</option>
                  </select>
                </div>
            </td>
          </tr>
        </table></td>
        <td valign="top" style="border-bottom:0px;"><table class="tableHstab" width="210" border="0" align="left" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right" width="80">表头字体：</td>
            <td>
                <div id="vbngm" class="uboxstyle">
                  <select name="head_font" id="head_font">
                    <option value="宋体">宋体</option>
                    <option value="楷体">楷体</option>
                    <option value="微软雅黑">微软雅黑</option>
                  </select>
                </div>
            </td>
          </tr>
          <tr>
            <td align="right">表头字号：</td>
            <td>
              <div id="jkger" class="uboxstyle">
                <select name="head_size" id="head_size">
                  <option value="9">9 px</option>
                  <option value="10">10 px</option>
                  <option value="11">11 px</option>
                  <option value="12" selected="selected">12 px</option>
                  <option value="14">14 px</option>
                  <option value="16">16 px</option>
                  <option value="18">18 px</option>
                  <option value="20">20 px</option>
                  <option value="24">24 px</option>
                </select>
              </div>
            </td>
          </tr>
          <tr>
            <td align="right">表头高度：</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="head_height" value=""/></td>
          </tr>
        </table></td>
      </tr>
    </table>    
    <div class="grid-wrap">
      <table id="grid">
      </table>
    </div>
  </div>
  <!-- 第二步 -->
  <div id="stemp2" class="stempdiv" style="display:none;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableHeadSet">
      <tr>
        <th>表格设置</th>
      </tr>
      <tr>
        <td style="border-bottom:0px;"><table class="tableHstab" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right" width="110">表头字体：</td>
            <td width="140">
              <div id="cvrth" class="uboxstyle">
                <select name="table_font" id="table_font">
                  <option value="宋体">宋体</option>
                  <option value="楷体">楷体</option>
                  <option value="微软雅黑">微软雅黑</option>
                </select>
              </div>
            </td>
            <td align="right" width="70">表头字号：</td>
            <td width="140">
              <div id="ggjhtkliu" class="uboxstyle">
                <select name="table_size" id="table_size">
                  <option value="9">9 px</option>
                  <option value="10">10 px</option>
                  <option value="11">11 px</option>
                  <option value="12" selected="selected">12 px</option>
                  <option value="14">14 px</option>
                  <option value="16">16 px</option>
                  <option value="18">18 px</option>
                  <option value="20">20 px</option>
                  <option value="24">24 px</option>
                </select>
              </div>
            </td>
            <td align="right" width="70">表头行高度:</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="table_headheight" value=""/></td>
            
          </tr>
          <tr>
            <td align="right">数据表格上边距：</td>
            <td><input type="text" class="ui-input ui-input-ph" id="table_top" style="width:40px" value=""/></td>
            <td align="right">表格行高度:</td>
            <td><input type="text" class="ui-input ui-input-ph" style="width:40px" id="table_dataheight" value=""/></td>
            <td colspan="2">
            &nbsp;&nbsp;
                <ul id="table_data" style="width:400px;float:left;">
                    <li style="width:80px;float:left;">
                        <input type="checkbox" id="table_bold"/>标题加粗
                    </li>
                    <li style="width:60px;float:left;">
                        <input type="checkbox" id="table_subtotal"/>小计
                    </li>
                    <li style="width:60px;float:left;">
                        <input type="checkbox" id="table_total"/>总计
                    </li>
                </ul>
            </td>
          </tr>
        </table></td>
      </tr>
    </table>
    
    <div class="grid-wrap">
      <table id="gridData">
      </table>
    </div>
    
  </div>
  <!-- 第三步 -->
  <div id="stemp3" class="stempdiv" style="display:none;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableHeadSet">
      <tr>
        <th colspan="2">表尾设置</th>
      </tr> 
      <tr>
      	<td width="100" style="padding-top:10px;" align="right" valign="top" rowspan="2" id="" >备注：</td>
        <td>
             <input type="radio" name="end_endshow" value="0" checked onclick="javascript:$('#end_endshow').val($(this).val());"/>显示 
             <input type="radio" name="end_endshow" value="1" onclick="javascript:$('#end_endshow').val($(this).val());"/>不显示
             <input type="text" id="end_endshow" value="0"/>
      	</td>
        
      </tr>
      <tr>
        <td style="padding:10px;">
        	<script id="sp_remark" name="sp_remark" type="text/plain" style="width:99%;height:200px;"></script>
        </td>
        
      </tr>
    </table>
  </div>
</div>
<script type="text/javascript">
	var toolbars = [
           ['fullscreen','undo', 'redo','bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', '|','justifyleft','justifyright','justifycenter','justifyjustify', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc'],
           ['fontfamily','fontsize','paragraph']
       ];
    var ue = UE.getEditor('sp_remark',{
    	toolbars:toolbars,
    	autoHeightEnabled: false,
    	elementPathEnabled:false,
    	maximumWords:500
    });
</script>
<script src="<%=basePath%>data/sys/print/print_set.js"></script>
</body>
</html>