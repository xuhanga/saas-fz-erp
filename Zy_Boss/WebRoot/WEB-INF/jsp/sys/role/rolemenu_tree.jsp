<%@ page contentType="text/html; charset=gbk" language="java" %>
<%@ include file="/common/path.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>��Ʒ����</title>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/ztree/ztree/ztree.css"/>
<style>
.treemenu{margin-top:1px;margin-left:5px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.3.2.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ztree/jquery.ztree.core-3.4.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ztree/jquery.ztree.excheck-3.4.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");

</script>
<script>
var api = frameElement.api, W = api.opener;
var selectedTreeNode;
var setting = {
	check: {
		enable: true,
		chkboxType: { "Y" : "s", "N" : "s" }
	},
	data: {
		simpleData: {
			enable: true
		}
	}
};
$(function(){
	$.ajax({
		type:"post",
		url:"${static_path}sys/role/rolemenu_tree",
		data:{ro_id:'${ro_id}'},
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data){
				$.fn.zTree.init($("#role-tree"), setting, eval("(" + data.data + ")"));
			}
		}
	});
});
function doSelect(){
	var treeObj = $.fn.zTree.getZTreeObj("role-tree");
	var nodes = treeObj.getCheckedNodes(true);
	return nodes;
}
</script>
</head>  
<body>
	<div class="treemenu" >
		<div id="roleTree" class="divlistContent" style="height:402px;overflow-y:auto">
			<ul id="role-tree" class="ztree"></ul>
		</div>
	</div>
</body>
</html>