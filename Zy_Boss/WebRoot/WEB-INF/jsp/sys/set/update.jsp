<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.list td{border-bottom:#ccc dotted 1px;height:40px;}
.last td{border-bottom:0px;}
.main_Input{width: 225px; height: 16px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");

</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="st_id" name="st_id" value="${set.st_id}"/>
<div class="border">
	<div class="content">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td class="top-b border-b">
					<input type="button" id="btn-save" class="t-btn btn-red" style="float:right;" value="保存" onclick="javascript:save();"/>
				</td>
			</tr>
		</table>
		<div id="mainContent" style="overflow-x:hidden;overflow-y:auto;">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" id="data">
			<tr class="list">
				<td width="350" align="right">采购使用加权平均算法修改成本价：</td>
          		<td id="td_st_buy_calc_costprice">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_buy_calc_costprice" type="radio" value="0" ${set.st_buy_calc_costprice eq 0 ? 'checked' : '' }/>不启用
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_buy_calc_costprice" type="radio" value="1" ${set.st_buy_calc_costprice eq 1 ? 'checked' : '' }/>启用
	  	 			</label>
					<input type="hidden" id="st_buy_calc_costprice" name="st_buy_calc_costprice" value="${set.st_buy_calc_costprice}"/>
          		</td>
	        </tr>
			<tr class="list">
				<td align="right">采购到自营店(综合与高级版)：</td>
          		<td id="td_st_buy_os">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_buy_os" type="radio" value="1" ${set.st_buy_os eq 1 ? 'checked' : '' }/>是
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_buy_os" type="radio" value="0" ${set.st_buy_os eq 0 ? 'checked' : '' }/>否
	  	 			</label>
					<input type="hidden" id="st_buy_os" name="st_buy_os" value="${set.st_buy_os}"/>
          		</td>
	        </tr>
			<tr class="list">
				<td align="right">批发选自营仓库(综合与高级版)：</td>
          		<td id="td_st_batch_os">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_batch_os" type="radio" value="1" ${set.st_batch_os eq 1 ? 'checked' : '' }/>是
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_batch_os" type="radio" value="0" ${set.st_batch_os eq 0 ? 'checked' : '' }/>否
	  	 			</label>
					<input type="hidden" id="st_batch_os" name="st_batch_os" value="${set.st_batch_os}"/>
          		</td>
	        </tr>
			<tr class="list">
				<td align="right">批发显示返点/扣点：</td>
          		<td id="td_st_batch_showrebates">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_batch_showrebates" type="radio" value="1" ${set.st_batch_showrebates eq 1 ? 'checked' : '' }/>是
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_batch_showrebates" type="radio" value="0" ${set.st_batch_showrebates eq 0 ? 'checked' : '' }/>否
	  	 			</label>
					<input type="hidden" id="st_batch_showrebates" name="st_batch_showrebates" value="${set.st_batch_showrebates}"/>
          		</td>
	        </tr>
			<tr class="list">
				<td align="right">单据审核后打印：</td>
          		<td id="td_st_ar_print">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_ar_print" type="radio" value="1" ${set.st_ar_print eq 1 ? 'checked' : '' }/>是
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_ar_print" type="radio" value="0" ${set.st_ar_print eq 0 ? 'checked' : '' }/>否
	  	 			</label>
					<input type="hidden" id="st_ar_print" name="st_ar_print" value="${set.st_ar_print}"/>
          		</td>
	        </tr>
			<tr class="list">
				<td align="right">盘点机器分隔符：</td>
          		<td id="td_st_blank">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_blank" type="radio" value="0" ${set.st_blank eq 0 ? 'checked' : '' }/>逗号
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_blank" type="radio" value="1" ${set.st_blank eq 1 ? 'checked' : '' }/>1个空格
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_blank" type="radio" value="2" ${set.st_blank eq 2 ? 'checked' : '' }/>2个空格
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_blank" type="radio" value="3" ${set.st_blank eq 3 ? 'checked' : '' }/>3个空格
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_blank" type="radio" value="4" ${set.st_blank eq 4 ? 'checked' : '' }/>分号
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_blank" type="radio" value="5" ${set.st_blank eq 5 ? 'checked' : '' }/>Tab键
	  	 			</label>
					<input type="hidden" id="st_blank" name="st_blank" value="${set.st_blank}"/>
          		</td>
	        </tr>
	        <tr class="list">
				<td align="right">系统条码规则：</td>
          		<td id="td_st_subcode_isrule">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_subcode_isrule" type="radio" value="0" ${set.st_subcode_isrule eq 0 ? 'checked' : '' }/>系统配置
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_subcode_isrule" type="radio" value="1" ${set.st_subcode_isrule eq 1 ? 'checked' : '' }/>规则设置
	  	 			</label>
					<input type="hidden" id="st_subcode_isrule" name="st_subcode_isrule" value="${set.st_subcode_isrule}"/>
					<span id="rule_span" style="color:red;">编码+颜色+尺码+杯型</span>
					<span id="rule_set" style="display:${set.st_subcode_isrule eq 1 ? '' : 'none'};">
						<span class="ui-combo-wrap" id="span_rule_one"></span>
						<span class="ui-combo-wrap" id="span_rule_two"></span>
						<span class="ui-combo-wrap" id="span_rule_three"></span>
						<span class="ui-combo-wrap" id="span_rule_four"></span>
						<input type="hidden" name="st_subcode_rule" id="st_subcode_rule" value="${set.st_subcode_rule}"/>
					</span>
          		</td>
	        </tr>
	        <tr class="list">
				<td align="right">仓库调拨单价格按：</td>
          		<td id="td_st_allocate_unitprice">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_allocate_unitprice" type="radio" value="0" ${set.st_allocate_unitprice eq 0 ? 'checked' : '' }/>零售价
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_allocate_unitprice" type="radio" value="1" ${set.st_allocate_unitprice eq 1 ? 'checked' : '' }/>成本价
	  	 			</label>
					<input type="hidden" id="st_allocate_unitprice" name="st_allocate_unitprice" value="${set.st_allocate_unitprice}"/>
          		</td>
	        </tr>
	        <tr class="list">
				<td align="right">库存盘点显示：</td>
          		<td>
          			<span id="chk_st_check_showstock" style="float:left;width:80px;">
		  		 		<label class="chk" style="margin-top:6px;">
							<input type="checkbox" ${set.st_check_showstock eq 1 ? 'checked' : '' }/>库存数量
						</label>
					</span>
					<input type="hidden" id="st_check_showstock" name="st_check_showstock" value="${set.st_check_showstock}"/>
          			<span id="chk_st_check_showdiffer" style="float:left;width:80px;">
		  		 		<label class="chk" style="margin-top:6px;">
							<input type="checkbox" ${set.st_check_showdiffer eq 1 ? 'checked' : '' }/>相差数量
						</label>
					</span>
					<input type="hidden" id="st_check_showdiffer" name="st_check_showdiffer" value="${set.st_check_showdiffer}"/>
          		</td>
	        </tr>
	        <tr class="list">
				<td align="right">强制验证库存：</td>
          		<td id="td_st_force_checkstock">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_force_checkstock" type="radio" value="1" ${set.st_force_checkstock eq 1 ? 'checked' : '' }/>是
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_force_checkstock" type="radio" value="0" ${set.st_force_checkstock eq 0 ? 'checked' : '' }/>否
	  	 			</label>
					<input type="hidden" id="st_force_checkstock" name="st_force_checkstock" value="${set.st_force_checkstock}"/>
          		</td>
	        </tr>
	        <tr class="list">
				<td align="right">从上级店铺扣款(自营店)：</td>
          		<td id="td_st_use_upmoney">
          			<label class="radio" style="width:30px">
  	 					<input name="_st_use_upmoney" type="radio" value="1" ${set.st_use_upmoney eq 1 ? 'checked' : '' }/>是
	  	 			</label>
	  	 			<label class="radio" style="width:30px">
	  	 				<input name="_st_use_upmoney" type="radio" value="0" ${set.st_use_upmoney eq 0 ? 'checked' : '' }/>否
	  	 			</label>
					<input type="hidden" id="st_use_upmoney" name="st_use_upmoney" value="${set.st_use_upmoney}"/>
          		</td>
	        </tr>
        </table>
        </div>
	</div>
</div>
</form>
<script>
$("#mainContent").height($(parent).height()-169);
</script>
<script src="<%=basePath%>data/sys/set/set_update.js"></script>
<script>
var st_subcode_isrule = '${set.st_subcode_isrule}';
if(st_subcode_isrule == "0"){
	$("#rule_span").text("编码+颜色+尺码+杯型");
}
</script>
</body>
</html>