<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css"" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
</script>
</head>
<body>
<form name="form1" method="post" action="" id="form1" style="margin-left:10px;">
	<table class="pad_t20" width="100%" >
		<tr style="line-height:45px; ">
			<td nowrap="nowrap">
				帐号：<input class="main_Input" type="text" name="us_account" id="us_account" onfocus="javascript:this.select();" style="width:170px;" value="" maxlength=15 title="不能超过十五个字符"/>
					<input type="button" class="btn_select" value="" name="btn_emp" id="btn_emp"/>
			</td>
			<td nowrap="nowrap">
				名称：<input class="main_Input" type="text" name="us_name" id="us_name" value="" maxlength=10/>
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td  nowrap="nowrap" align="left">
				角色：<input class="main_Input" type="text" id="ro_name" name="ro_name" value="" readonly="readonly" style="width:170px; " />
			  	<input type="button" class="btn_select" value="" name="btn_role" id="btn_role"/>
			    <input type="hidden" name="us_ro_code" id="us_ro_code" value=""/>
			    <input type="hidden" name="ro_shop_type" id="ro_shop_type" value=""/>
			</td>
			<td nowrap="nowrap">
				密码：<input class="main_Input" type="password" name="us_pass" id="us_pass" value="" maxlength=15 title="不能超过十五个字符"/>
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td  nowrap="nowrap" align="left">
				门店：<input class="main_Input" type="text" id="sp_name" name="sp_name" value="" readonly="readonly" style="width:170px; " />
			  	<input type="button" class="btn_select" value="" name="btn_shop" id="btn_shop"/>
			    <input type="hidden" name="us_shop_code" id="us_shop_code" value=""/>
			</td>
			<td nowrap="nowrap">
				状态：
				<input type="radio" name="us_state" id="us_state" value="0" checked="checked"/>正常
				<input type="radio" name="us_state" id="us_state" value="1" />停用
			</td>
		</tr>
		<tr style="line-height:45px; ">
			<td align="left" colspan="2">权限：
				<input type="hidden" name="us_limit" id="us_limit" value=""/>
				<input type="checkbox" name="checkID" id="checkID" value="C" checked="checked"/>&nbsp;成本价
				<input type="checkbox" name="checkID" id="checkID" value="S" checked="checked"/>&nbsp;零售价
				<input type="checkbox" name="checkID" id="checkID" value="D" checked="checked"/>&nbsp;配送价
				<input type="checkbox" name="checkID" id="checkID" value="V" checked="checked"/>&nbsp;会员价
				<input type="checkbox" name="checkID" id="checkID" value="W" checked="checked"/>&nbsp;批发价
				<input type="checkbox" name="checkID" id="checkID" value="E" checked="checked"/>&nbsp;进货价
				<input type="checkbox" name="checkID" id="checkID" value="P" checked="checked"/>&nbsp;利润
			</td>
		</tr>
	</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/sys/user/user_add.js?v=${time}"></script>
</body>
</html>