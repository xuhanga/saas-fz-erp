<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>我的工作台</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/info.css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
</script>
<script>
function addTags(obj,name){
	window.parent.addTags(obj,name);
}
</script>
<script language=Javascript> 
$(function(){
	function getCurDate(){
		 var d = new Date();
		 var week;
		 switch (d.getDay()){
			 case 1: week="星期一"; break;
			 case 2: week="星期二"; break;
			 case 3: week="星期三"; break;
			 case 4: week="星期四"; break;
			 case 5: week="星期五"; break;
			 case 6: week="星期六"; break;
			 default: week="星期天";
		 }
		 var years = d.getFullYear();
		 var month = add_zero(d.getMonth()+1);
		 var days = add_zero(d.getDate());
		 var hours = add_zero(d.getHours());
		 var minutes = add_zero(d.getMinutes());
		 var seconds=add_zero(d.getSeconds());
		 //var ndate = years+"年"+month+"月"+days+"日 "+hours+":"+minutes+":"+seconds+" "+week;
		 var ndate = "<p class='datetime'>"+week+"</p><p class='dateyear'>"+hours+" : "+minutes+" : "+seconds+"</p><p class='dateyear'>"+years+"-"+month+"-"+days+"</p>"
		 return ndate;
	}

	function add_zero(temp){
	 if(temp<10) return "0"+temp;
	 else return temp;
	}
	$(".datelist").html(getCurDate());
	setInterval(function(){$(".datelist").html(getCurDate());},1000);
})
</script>	
<style type="text/css">
.icons{float:left; width:100%;}
.mywork_l{position:fixed;left:15px;top:15px;right:295px;bottom:15px;}
.mywork_r{position:fixed;top:15px;right:15px;bottom:15px;width:260px;}
.mywork_div{float:left;width:100%;border:#ddd solid 1px;padding-bottom:10px;font-family:Microsoft YaHei;}
.mywork_div h3{float:left;width:100%;height:35px;line-height:35px;border-bottom:#ddd solid 1px;background:#f5f5f5;}
.mywork_div h3 em{float:left;margin-left:10px;color:#08b;font-size:16px;}
.baseIcons{width:100%;}
.baseIcons li{margin:10px 0 0 10px;}
.mt15{margin-top:0px}
.my_report{background:#1A8E8C;border-radius:3px;line-height:60px;font-size:35px;color:#fff;}
.my_report:hover{color:#fff;}
.my_report_add{line-height:60px;font-size:50px;color:#ccc;}
.dblist li{float:left;height:30px;line-height:30px;width:240px;padding:0 10px;cursor:pointer;}
.dblist li span{float:right;color:#f30}
.baseIcons{height:220px;overflow:auto;}
.datelist{background:#29A1B1;border:none;margin: 0;float: left;width: 100%;text-align: center;color: #fff;}
.datelist p{float:left;margin:0;text-align:center;}
.datetime{width:40%;height:60px;font-size:26px;line-height:60px;}
.dateyear{width:60%;height:30px;font-size:18px;line-height:30px;}
.dblist .bg_green{color:#1b96a9;}
.dblist .bg_blue{color:#56abe4;}
.dblist .bg_yellow{color:#f4c631;}
.dblist .bg_orange{color:#ea8010;}
.dblist .bg_red{color:#eb4f38;}
.dblist .bg_gray{color:#33475f;}
.dblist .bg_violet{color:#9d55b8;}
</style>
</head>
<body>
<input type="hidden" id="refresh" value=""/>
<div class="mywork_l">
	<div class="mywork_div">
		<h3><em>日常工作</em>
		<i class="iconfont fr mr10" title="修改" id="modifyButton" style="cursor:pointer;">&#xe60a;</i>
		</h3>
		<div class="icons">
		<ul class="baseIcons" id="dailyWorkMenu">
		</ul>
		</div>
	</div>
	<div class="mywork_div mt15">
		<h3><em>日常报表</em></h3>
		<div class="icons">
		<ul class="baseIcons" id="dailySheetMenu">
		</ul>
		</div>
	</div>
</div>
<div class="mywork_r">
	<!-- <div class="mywork_div datelist">
		
	</div> -->
	<div class="mywork_div mt15">
		<h3><em>待办事项</em>&nbsp;<span></span>
			<i class="iconfont fr mr10" title="刷新" id="refreshToDo" style="cursor:pointer;">&#xe63a;</i>
			<i class="iconfont fr mr10" title="增加" id="addToDoButton" style="cursor:pointer;">&#xe639;</i>
			</h3>
		<input type="hidden" id="todoids" value=""/>
		<ul class="dblist" id="todoList">
		</ul>
	</div>
</div>
<script src="<%=basePath%>data/sys/workbench/workbench_my.js"></script>
</body>
</html>
