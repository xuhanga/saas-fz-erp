<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title>会员生日</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/css/iconfont/iconfont.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
</script>
</head>
<body id="main_content">
<input type="hidden" id="consumeDay" name="consumeDay" value="${consumeDay }"/>
<input type="hidden" id="agowarnDay" name="agowarnDay" value="${agowarnDay }"/>
<input type="hidden" id="searchDateType" value=""/>
<input type="hidden" id="searchMonth" value=""/>
<input type="hidden" id="CurrentMode" value="0" />
<form name="form1" method="post" action="" id="form1">
<div class="wrapper">
	<div class="mod-search cf">
	    <div class="fl">
			<div id="filter-menu" class="ui-btn-menu fl">
			     		<span style="float:left;height:33px;" class="ui-btn menu-btn">
			     	   	生日类型：<span class="ui-combo-wrap" id="year" style="height:25px;">
						<i class="trigger" style="padding:0px;"></i>
						</span>&nbsp;&nbsp;&nbsp;
		     		 	<input type="text" class="main_Input Wdate-select" readonly="readonly"  id="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}', dateFmt:'MM-dd'});handle.removeButtonClass();" value="" />
						 -
						<input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}', dateFmt:'MM-dd'});handle.removeButtonClass();" value="" />
					 <b></b>
			  	 </span>
				<div class="con" style="width:380px;">
					<ul class="ul-inline">
						<li>
							<table class="searchbar">					
								 <tr>
									<td>办卡门店：
										 <input class="main_Input atfer_select" type="text" style="height:13px;" id="sp_name" name="sp_name" readonly="readonly"/>
										 <input type="button" class="btn_select" id="btn_shop" style="height:27px;margin-left:-8px;"/>
										 <input type="hidden" id="vm_shop_code" name="vm_shop_code" />
							  		</td>
								  </tr>
								 <tr>
									<td>会员卡号：
							  	 		<input class="main_Input" type="text" id="vm_cardcode" name="vm_cardcode" value="" />
							  		</td>
								  </tr>
								  <tr>
									<td>
									          会员姓名：
									    <input class="main_Input" type="text" id="vm_name" name="vm_name" value="" />
							  		</td>
								  </tr>						  
								  <tr>
								 	<td>手机号码：
							    		<input class="main_Input" type="text" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" id="vm_mobile" name="vm_mobile" value="" maxlength="11"/>
								 	</td>
								  </tr>
								  <tr>
								  	<td>N天未消费：
								  		<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="begin_notbuy_day" name="begin_notbuy_day" style="width:78px"/> 至
								  	 	<input class="main_Input" type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="end_notbuy_day" name="end_notbuy_day" style="width:78px" />
								  	</td>
								  </tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
				<span style="float:left;margin-left:10px;">&nbsp;&nbsp;&nbsp;<a class="ui-btn mrb" id="btn_Search">查询</a></span>
				<span style="float:left;margin-left:10px;">
					<a href="javascript:void(1);" class="t-btn btn-bl on" id="todayButton" onclick="javascript:handle.doChangeMode(0);">今日生日</a>
				    <a href="javascript:void(0);" class="t-btn btn-blc" id="nearButton" onclick="javascript:handle.doChangeMode(1);">临近生日</a>
				    <a href="javascript:void(0);" class="t-btn btn-bc" id="monthButton" onclick="javascript:handle.doChangeMode(2);">本月生日</a>
			    </span>
			</div>
	    </div>
      </div>

     <div class="grid-wrap">
	    <table id="grid">
	    </table>
	    <div id="page"></div>
	</div>
</div>
	
</div>
</form>
<script src="<%=basePath%>data/vip/member/member_birthday_list.js"></script>
<script>
$(document).ready(function(){
	THISPAGE1.getyearInfo();
	THISPAGE1.getmonthInfo();
	$("#year").css("width","66px");
	$("#month").css("width","56px");
	$("#year").children("input").css("width","36px");
	$("#month").children("input").css("width","26px");
});
<%-- function querySMSBalance(){
	$.ajax({
		type:"POST",
		url:"<%=basePath%>office/querySMSBalanceHTMl.action",
		cache:false,
		dataType:"html",
		success:function(data){
			
		}
	});
} --%>


var yearInfo={};
var monthInfo={};
var jsonstrY = {data:{items:[{hour:"",hourText:"请选择"},		             	          
                             {hour:"0",hourText:"公历"}, 
                             {hour:"1",hourText:"农历"}
                             ]}};
var jsonstrM = {data:{items:[		             	          
	                             {hour:"1",hourText:"01"}, 
	                             {hour:"2",hourText:"02"}, 
	                             {hour:"3",hourText:"03"}, 
	                             {hour:"4",hourText:"04"}, 
	                             {hour:"5",hourText:"05"}, 
	                             {hour:"6",hourText:"06"}, 
	                             {hour:"7",hourText:"07"}, 
	                             {hour:"8",hourText:"08"}, 
	                             {hour:"9",hourText:"09"}, 
	                             {hour:"10",hourText:"10"},
	                             {hour:"11",hourText:"11"},
	                             {hour:"12",hourText:"12"}
	                             
			                            	                             
		 ]}};
var THISPAGE1 = {
		initY:function(){
			this.yearCombo = Business.yearCombo($('#year'), {
				defaultSelected: ''
			});
		},
		getyearInfo:function(){
			yearInfo = jsonstrY.data.items;
			THISPAGE1.initY();
		},
		initM:function(){
			var myDate = new Date();
			this.monthCombo = Business.monthCombo($('#month'), {
				defaultSelected:parseInt(myDate.getMonth(),10)
			});
		},
		getmonthInfo:function(){
			monthInfo = jsonstrM.data.items;
			THISPAGE1.initM();
		}
	}

Business.yearCombo = function($_obj, opts){
	if ($_obj.length == 0) { return };
	var opts = $.extend(true, {
		data: function(){
			return yearInfo;
		},
		width: 200,
		height: 300,
		formatText: function(row){
			return row.hourText;
		},
		text: 'name',
		value: 'id',
		defaultFlag: true,
		cache: false,
		editable: false,
		callback: {
			onChange: function(data){
				$("#searchDateType").val(data.hour);
				handle.removeButtonClass();
			}
		}
	}, opts);
	var yearCombo = $_obj.combo(opts).getCombo();	
	return yearCombo;
};	

Business.monthCombo = function($_obj, opts){
	if ($_obj.length == 0) { return };
	var opts = $.extend(true, {
		data: function(){
			return monthInfo;
		},
		width: 200,
		height: 300,
		formatText: function(row){
			return row.hourText;
		},
		text: 'name',
		value: 'id',
		defaultFlag: true,
		cache: false,
		editable: false,
		callback: {
			onChange: function(data){
				$("#searchMonth").val(data.hour);
			}
		}
	}, opts);
	var monthCombo = $_obj.combo(opts).getCombo();	
	return monthCombo;
};	

</script>
</body>
</html>