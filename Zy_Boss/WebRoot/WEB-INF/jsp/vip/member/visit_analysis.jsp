<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css">
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css">
<style type="text/css">
	body{font-family:Microsoft YaHei;}
	.btn_subok{border-radius:3px;background:#08c;border:#08b solid 1px;height:30px;width:60px;color:#fff;cursor:pointer;}
	.btn_close{border-radius:3px;background:#f1f1f1;border:#ddd solid 1px;height:30px;width:60px;color:#333;cursor:pointer;}
	.mtable td{padding:2px 0;line-height:30px;}
	
	.tel_visit{float:left;width:100%;font-family:Microsoft YaHei;}
	.tel_visit dl{float:left;width:100%;line-height:30px;}
	.tel_visit dl dt{float:left;width:15%;text-align:right;padding-top:10px;}
	.tel_visit dl dd{float:left;width:80%;padding-top:10px;}
	
	.visit_base{position:fixed;top:0;left:0;bottom:0;width:170px;background:#565D67;color:#fff;}
	.visit_base img{float:left;height:80px;width:80px;background:#fff;border-radius:50%;margin:15px 43px 0;overflow:hidden;border:#fff solid 2px;}
	.visit_base h2{float:left;height:40px;line-height:40px;width:100%;text-align:center;font-size:16px;}
	.visit_base dl{float:left;width:100%;line-height:30px;}
	.visit_base dl dt{float:left;width:45px;text-align:right;}
	.visit_base dl dd{float:left;width:120px;}
		
	.visit_nav{position:fixed;top:0;left:170px;right:0;height:45px;background:#8ABC1D;}
	.visit_nav li{display:inline;float:left;width:25%;text-align:center;height:45px;line-height:45px;position:relative;color:#fff;font-size:16px;cursor:pointer;}
	.visit_nav li.dhhf{background:#358EEC;}
	.visit_nav li.dxhf{background:#3EB982;}
	.visit_nav li.tsdzq{background:#FFA811;}
	.visit_nav li span{position:absolute;height:20px;line-height:20px;text-align:center;width:20px;font-size:30px;left:50%;margin-left:-10px;bottom:-8px;display:none;}
	.visit_nav li.dhhf span{color:#358EEC;}
	.visit_nav li.dxhf span{color:#3EB982;}
	.visit_nav li.tsdzq span{color:#FFA811;}
	.visit_nav li.wxhf span{color:#8ABC1D;}
	.visit_box{position:fixed;bottom:0;left:170px;top:60px;right:0;}
	.visit_submit{position:absolute;bottom:0;left:0;right:0;height:35px;background:#f4f4f4;padding:5px 10px 0 0;text-align:right;border-top:#ddd solid 1px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script>
 </head>
<body>
<div class="visit_base">
	<!-- <img src="" height="84" width="84" alt="" /> -->
	<h2>${member.vm_name}</h2>
	<dl style="text-align:center;color:#ff0;">
		${member.mt_name} （${member.vm_cardcode }）
	</dl>
	<dl>
		<dt>手机：</dt><dd>${member.vm_mobile}</dd>
	</dl>
	<dl>
		<dt>办卡：</dt><dd>${member.vm_date}</dd>
	</dl>
	<dl>
		<dt>消费：</dt><dd>${member.vm_total_money} 元</dd>
	</dl>
	<dl>
		<dt>积分：</dt><dd>${member.vm_points - member.vm_used_points}</dd>
	</dl>
	<dl>
		<dt>店铺：</dt><dd>${member.shop_name}</dd>
	</dl>
</div>

<div class="visit_nav">
	<ul>
		<li class="dhhf" onclick="changeMode(1);"><i class="iconfont">&#xe64d;</i> 电话回访<span style="display:block">◆</span></li>
		<!-- <li class="dxhf" onclick="changeMode(2)"><i class="iconfont">&#xe661;</i> 短信回访<span>◆</span></li> -->
		<!-- <li class="tsdzq" onclick="changeMode(4)"><i class="iconfont">&#xe600;</i> 推送电子券<span>◆</span></li> -->
		<!-- <li class="wxhf" onclick="changeMode(3)" id="wx_visit"><i class="iconfont">&#xe658;</i> 微信回访<span>◆</span></li> -->
	</ul>
</div>
<!-- 电话回访 -->
<div class="visit_box" id="visitMode1">
	<form name="form1" method="post" action="" id="form1">
		<div class="mainwra">
			<input type="hidden" id="vi_type" name="vi_type" value="${vi_type }"/>
            <input type="hidden" id="vi_remark" name="vi_remark" value=""/>
            <input type="hidden" id="vi_date" name="vi_date" value=""/>
            <input type="hidden" id="vi_vm_code" name="vi_vm_code" value="${member.vm_code}"/>
			<input type="hidden" id="vi_vm_mobile" name="vi_vm_mobile" value="${member.vm_mobile}"/>
			<input type="hidden" id="vi_vm_name" name="vi_vm_name" value="${member.vm_name}"/>
        	<input type="hidden" id="vi_visit_date" name="vi_visit_date"  value="${nowDate }"/>
        	<input type="hidden" id="vi_consume_date" name="vi_consume_date" value="${nowDate }"/>
			<input type="hidden" id="vi_way" name="vi_way" value="1"/>
			<div class="tel_visit">
				<dl>
					<dt>回访人员：</dt>
					<dd>
						<input readonly type="text" id="vi_manager" name="vi_manager" class="main_Input atfer_select w120" title="" value=""/>
			            <input type="button" id="btn_emp" value="" class="btn_select"/>
			            <input type="hidden" id="vi_manager_code" name="vi_manager_code" />
			            <!-- 回访人所在店铺 --><input type="hidden" id="vi_shop_code" name="vi_shop_code">
					</dd>
				</dl>
				<dl>
					<dt>满&nbsp;意&nbsp;度：</dt>
					<dd>
						<span class="ui-combo-wrap" id="span_satisfaction"></span>
			            <input type="hidden" id="vi_satisfaction" name="vi_satisfaction" value="0"/> 
					</dd>
				</dl>
				<dl>
					<dt>回访内容：</dt>
					<dd>
						<textarea class="main_Input" type="text" id="vi_content" name="vi_content" value="" style="resize:none;width:455px;height:50px;"></textarea>
					</dd>
				</dl>
				<dl>
					<dt>反馈信息：</dt>
					<dd>
						<span class="ui-combo-wrap" id="span_is_arrive"></span>
			            <input type="hidden" id="vi_is_arrive" name="vi_is_arrive" value="1"/> 
					</dd>
				</dl>
				<dl id="divnext">
			    	<dt align="right">下次联系时间 ：</dt>
			    	<dd><input id="nextTime" class="main_Input Wdate w146" onclick="WdatePicker()" type="text" value="${nowDate }"></dd>
			    	<dt align="right">联系计划内容：</dt>
			    	<dd><input id="txtplan" class="main_Input" type="text" style="width:455px;"></dd>
			    </dl>
			    <dl id="divarrive" style="display: none;">
			    	<dt align="right">到店日期 ：</dt>
			    	<dd><input id="arrivedate" class="main_Input Wdate w146" onclick="WdatePicker()" type="text" value="${nowDate }"></dd>
			    	<dt align="right">注意事项：</dt>
			    	<dd><input id="precautions" class="main_Input" type="text" style="width:455px;"></dd>
			    </dl>
			    <dl id="divno" style="display: none;">
			    	<dt align="right">反馈问题：</dt>
			    	<dd><input id="txtother" class="main_Input" style="width:455px;" type="text"></dd>
			    </dl>
			</div>
		</div>
		<div class="visit_submit"><input type="button" id="btn_save" class="btn_subok" value="提交" /></div>
	</form>
</div>
<%-- <!-- 短信回访 -->
<div class="visit_box" id="visitMode2" style="display:none;">
	<div class="tel_visit">
		<dl>
			<dt>内容：</dt>
			<dd>
				<textarea style="width:500px;height:80px;font:14px/30px 'Microsoft YaHei';resize:none;" id="Vip_Content" name="Vip_Content" 
				onkeyup="javascript:if(value.length>150) value=value.substr(0,150);countPoint(this);" 
				onkeydown="javascript:if(value.length>150) value=value.substr(0,150);countPoint(this);" ></textarea>
			<p>余额[<b id="lastMoney">0</b>]&nbsp;&nbsp;(每条短信限<b id="smsFontCount">63</b>个字符,当前短信数为<b id="smsCount" style="color: red;">1</b>条,可发送<b  style="color:blue;" id="sendCount"></b>条)<br />
			       您总共输入字数:<b style="color:red;" id="smsSize">0</b>。</p>
			<input type="hidden" id="VipSMSCount" value="1" />
			</dd>
		</dl>
	</div>
	<div class="visit_submit">
		<input type="button" id="saveSmsBtn" value="发送" class="btn_subok" onclick="javascript:saveRetailService();"/>
		<input type="button" value="关闭" class="btn_close" onclick="javascript:doClose();"/>
	</div>
</div>
<!-- 推送电子券 -->
<div class="visit_box" id="visitMode4" style="display:none;">
	<%@ include file="/pages/vip/sendCoupon_query.jsp"%>
</div>
<!-- 微信回访 -->
<div class="visit_box" id="visitMode3" style="display:none;">
	<input type="hidden" id="vip_id" name="vip_id" value="${vip_id }"/>
	<div class="tel_visit">
		<dl>
			<dt>标题：</dt>
			<dd><input type="text" class="main_Input w150" style="width:490px;" name="Vip_title" id="Vip_title" value="" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt>内容：</dt>
			<dd>
				<textarea style="width:500px;height:80px;font:14px/30px 'Microsoft YaHei';resize:none;" id="WX_Content" name="WX_Content" 
					onkeyup="javascript:if(value.length>250) value=value.substr(0,250);countWXPoint(this);" 
					onkeydown="javascript:if(value.length>250) value=value.substr(0,250);countWXPoint(this);" ></textarea>
				<p>*每条消息限<b id="smsFontCount">250</b>个字符,您已输入字符:<b style="color:red;" id="msgSize">0</b>。</p>
			</dd>
		</dl>
	</div>
	<div class="visit_submit">
		<input type="button" id="saveWXBtn" value="发送" class="btn_subok" onclick="javascript:sendWXMsg();"/>
		<input type="button" value="关闭" class="btn_close" onclick="javascript:doClose();"/>
	</div>
</div> --%>
<script type="text/javascript">
$(function(){
	$(".visit_nav li").each(function(){
		$(this).click(function(){
			$(".visit_nav li span").css("display","none");
			$(this).children("span").css("display","block");
		})
	});
	/* ajaxQuerySMSBalance(); */
})

function changeMode(id){
	$("#vi_way").val(id);
	for(var i=1; i<=4; i++){
		if(id == i){
			document.getElementById("visitMode"+i).style.display = "";
		}else{
			document.getElementById("visitMode"+i).style.display = "none";
		}
	}	
}
</script>
</body>
<script src="<%=basePath%>data/vip/member/member_visit_analysis.js"></script>
</html>