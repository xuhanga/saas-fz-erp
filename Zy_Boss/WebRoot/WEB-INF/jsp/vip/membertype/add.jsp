<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%">
				<tr class="list first">
					<td align="right" width="100px"><font color="red">*</font>类别名称：</td>
					<td>
						<input class="main_Input" type="text" name="mt_name" id="mt_name" value="" maxlength="30"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><font color="red">*</font>享受折扣：</td>
					<td>
						<input class="main_Input" type="text" name="mt_discount" id="mt_discount" value="" maxlength="4"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" >自动升级：</td>
					<td align="left" id="td_auto_up">
					 	<label class="radio" style="width:30px">
			  	 			<input name="redio1" type="radio" value="1" checked="checked"/>是
				  	 	</label>
				  	 	<label class="radio" style="width:30px">
				  	 		<input name="redio1" type="radio" value="0" />否
				  	 	</label>
						<input type="hidden" id="mt_isauto_up" name="mt_isauto_up" value="1"/>
					</td>
				</tr>
				
				<tr class="list">
					<td align="right">升级下线积分：</td>
					<td>
						<input class="main_Input" type="text" name="mt_min_point" id="mt_min_point" value="0" maxlength="6"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">消费：</td>
					<td>
						<input class="main_Input" style="width:70px;" type="text" name="mt_money" id="mt_money" value="1" maxlength="6"/>元&nbsp;积
						<input class="main_Input" style="width:70px;" type="text" name="mt_point" id="mt_point" value="1" maxlength="6"/>分
					</td>
				</tr>
				<tr class="list last">
					<td align="right">积分：</td>
					<td>
						<input class="main_Input" type="text" name="mt_point_to_money" id="mt_point_to_money" value="1" maxlength="10"/>
						分&nbsp;抵1元
					</td>
				</tr>
				<tr>
					<td align="right">备注：</td>
		            <td align="left">
		                <textarea style="font-size:12px" id="mt_mark" name="mt_mark" rows="3" cols="35"></textarea>
		            </td>
		        </tr>
			</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/vip/membertype/membertype_add.js"></script>
</body>
</html>