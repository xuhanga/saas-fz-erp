<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/comm-search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>/resources/ueditor/third-party/highcharts/highcharts.js\"></sc"+"ript>");
</script>
<style type="text/css">
.listcon{float:left;width:100%;padding-bottom:10px;}
.listcon dl{float:left;width:200px;border:#ddd solid 1px;margin:10px 0 0 10px;height:90px;text-align:center;cursor:pointer}
.listcon dl:hover{border:#f60 solid 1px;}
.listcon dl dt{float:left;height:30px;background:#f4f4f4;color:#f60;width:100%;}
</style>
</head>
<body>
<input type="hidden" id="CurrentMode" value="0" />
<input type="hidden" id="ags_beg_age" name="ags_beg_age" value="" />
<input type="hidden" id="ags_end_age" name="ags_end_age" value="" />
<div class="mainwra">
	<div class="border">
		<table width="100%">
			<tr>
				<td class="top-b border-b">
				    <div id="filter-menu" class="ui-btn-menu fl" style="margin-top: 10px">
	    			<span style="float:left" class="ui-btn menu-btn">
			     		 会员类别：
					  	<input type="text" class="ui-input ui-input-ph" style="width:158px" id="mt_name" name="mt_name" value="" />
					  	<input type="button" class="btn_select" id="btn_membertype" name="btn_membertype" value="" />
					  	<input type="hidden" id="vm_mt_code" name="vm_mt_code" />
			     		<b></b>
			  		</span>
			 		<div class="con" style="width:420px">
						<ul class="ul-inline">
							<li>
								<table class="searchbar">
									 <tr>
									 	<td>
									 	   发卡店铺：
									 	   	<input class="main_Input" type="text" id="sp_name" name="sp_name" value="" readonly="readonly" style="width:158px; " />
									     	<input type="button" id="btn_shop" name="btn_shop" value="" class="btn_select"/>
									     	<input type="hidden" name="vm_shop_code" id="vm_shop_code" value=""/>
									 	</td>
									 </tr>
									<tr>
									  <td>发卡人员：
									  	<input class="main_Input" type="text" id="vm_manager" name="vm_manager" value="" readonly="readonly" style="width:158px; " />
									     <input type="button" id="btn_emp" name="btn_emp" value="" class="btn_select"/>
									     <input type="hidden" name="vm_manager_code" id="vm_manager_code" value=""/>
							  		 </td>
								  </tr>
								</table>
							</li>
							<li style="float:right;">
								<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       					<a class="ui-btn mrb" id="btn_reset">重置</a>
							</li>
						</ul>
					</div>
					<div class="fl-m"><a class="ui-btn fl mrb" id="btn_search">查询</a></div>
		
			</div>
					<span style="float:right;width:240px;padding:10px 10px 10px 0">
			    		<a href="javascript:void(1);" class="ui-btn fl mrb " id="columnButton" onclick="javascript:Utils.doTableModel(0);"><i class="iconfont">&#xe62c;</i> 柱形模式</a>
			        	<a href="javascript:void(0);" class="ui-btn fl mrb " id="listButton" onclick="javascript:Utils.doTableModel(1);"><i class="iconfont">&#xe607;</i> 列表模式</a>
			    	</span>
				</td>
			</tr>
		</table>
	</div>
	<div id="gridDiv">
        <input type="hidden"  id="ageScope"/>
        <div class="border">
        	<table width="100%">
				<tr>
				<td>
					<div id="headHtml" class="listcon">
	        	
	       			</div>
				</td>
				</tr>
			</table>
        </div>
        <div class="grid-wrap" style="float:left" >
        	<table id="grid">
	        </table>
	        <div id="page"></div>
        </div>
    </div>
     <input  type="hidden" id="gridReload" value=""/>
	<div id="container" style="background:#fff;float:left;width:100%;height:100%;border:#e9e9e9 solid 1px;" ></div>
</div>
<script src="<%=basePath%>data/vip/report/vipage_analysis_list.js"></script>
</body>
<script type="text/javascript">
	document.getElementById("container").style.height= "500px";
</script>
</html>