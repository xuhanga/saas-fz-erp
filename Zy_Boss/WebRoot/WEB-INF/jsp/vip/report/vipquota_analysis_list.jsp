<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
</script>
<style>
.uitable{font-family:"微软雅黑";}
</style>
<script type="text/javascript">
	$(function(){
		$("#analysis td div").hover(function(){
			$(this).find("span").slideDown();
		},function(){
			$(this).find("span").slideUp();
		})
	})
</script>
</head>
<body >
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="lossDay" id="lossDay" value="${lossDay }"/ >
<input type="hidden" name="begindate_hid" id="begindate_hid" value="${begindate }"/ >
<input type="hidden" name="enddate_hid" id="enddate_hid" value="${enddate }"/ >
<input type="hidden" name="begindate" id="begindate" value="${begindate }"/ >
<input type="hidden" name="enddate" id="enddate" value="${enddate }"/ >
<input type="hidden" name="consumeDayDate" id="consumeDayDate" value="${consumeDayDate }"/ >
<input type="hidden" name="consumeDay" id="consumeDay" value="${consumeDay }"/ >
<div class="mainwra">
	<div class="mod-search cf">
	    <div class="fl">
	        	<table >
	        		<tr>
	        			<td>办卡店铺：</td>
	        			<td>
	        				<input class="main_Input" type="text" id="sp_name" name="sp_name" value="" readonly="readonly" style="width:170px; " />
							<input type="button" id="btn_shop" name="btn_shop" value="" class="btn_select"/>
							<input type="hidden" name="vm_shop_code" id="vm_shop_code" value=""/>
	        			</td>
	        			<td>
	        			    &nbsp;
	        				<input type="text" id="vs_loss_day" class="ui-input ui-input-ph" style="width:35px" value="${lossDay }" onkeyup="javascript:handle.changeBeginDate();"/>
	        				天未消费为流失会员
        				</td>
        				<td>&nbsp;
	        				<a class="ui-btn mrb" id="btn_search">查询</a>
	        				<a class="ui-btn mrb" id="btn_reset">重置</a>
	        			</td>
	        		</tr>
	        	</table>
	        
	    </div>
	</div>
<table width="100%" cellspacing="0" cellpadding="0" class="uitable">
  <tr id="analysis">
    <td width="20%" height="110">
    	<div class="uitabc" onclick="javascript:THISPAGE.reloadDataByType('loss');">
    		<span class="remarks">
    			<p>流失会员数：<b id="loss_vip_count"></b></p>
    			<p>会员总数：<b id="total_vip1"></b></p>
    			<p>注：流失率=N天内未消费会员/会员总数</p>
    		</span>
          <dl>
        	<dt class="liushi"><b id="loss_vip_rate" class="fontRate"></b></dt>
        	<dd>流失率</dd>
          </dl>
        </div>
    </td>
    <td width="20%">
        <div class="uitabc" onclick="javascript:THISPAGE.reloadDataByType('hold');">
        	<span class="remarks">
    			<p>会员保留人数：<b id="viphold_count"></b></p>
    			<p>会员总数：<b id="total_vip2"></b></p>
    			<p>注：保留率=会员保留人数/会员总数</p>
    		</span>
          <dl>
        	<dt class="baoliu"><b id="hold_vip_rate" class="fontRate"></b></dt>
        	<dd>保留率</dd>
          </dl>
        </div>
    </td>
    <td width="20%">
        <div class="uitabc" onclick="javascript:THISPAGE.reloadDataByType('goup');">
    		<span class="remarks">
    			<p>周期新增会员的消费数量：<b id="goup_vip_count"></b></p>
    			<p>周期新增的会员：<b id="total_vip4"></b></p>
    			<p>注：转化率=周期新增会员的消费数量/周期新增的会员</p>
    		</span>
          <dl>
        	<dt class="zhuanhua"><b id="goup_vip_rate" class="fontRate"></b></dt>
        	<dd>转化率</dd>
          </dl>
        </div>
    </td>
    <td width="20%">
        <div class="uitabc" onclick="javascript:THISPAGE.reloadDataByType('buyback');">
        	<span class="remarks">
    			<p>周期内会员消费人数：<b id="buyback_vip_count"></b></p>
    			<p>会员总数：<b id="total_vip5"></b></p>
    			<p>注：回购率=周期内会员消费人数/会员总数</p>
    		</span>
          <dl>
        	<dt class="huigou"><b id="buyback_vip_rate" class="fontRate"></b></dt>
        	<dd>回购率</dd>
          </dl>
        </div>
    </td>
    <td width="20%">
        <div class="uitabc" onclick="javascript:THISPAGE.reloadDataByType('newvip');">
        	<span class="remarks">
    			<p>周期内增加会员数：<b id="new_vip_vount"></b></p>
    			<p>总会员数：<b id="total_vip3"></b></p>
    			<p>注：增长率=周期内新增会员数量/(会员总数-新增总数)</p>
    		</span>
          <dl>
        	<dt class="zengzhang"><b id="new_vip_rate" class="fontRate"></b></dt>
        	<dd>增长率</dd>
          </dl>
        </div>
    </td>
  </tr>
</table>

<div class="grid-wrap" style="float:left;margin-top:5px;">
	<table id="grid"></table>
    <div id="page"></div>
</div>
	
</div>
</form>
<script src="<%=basePath%>data/vip/report/vipquota_analysis_list.js"></script>
</body>
</html>