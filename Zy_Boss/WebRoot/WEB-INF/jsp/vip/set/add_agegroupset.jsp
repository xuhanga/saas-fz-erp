<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title>消费回访设置添加</title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<table class="pad_t20"  style="font-size: 12px" >
	<col style="width:120px"/>
	<col style="width:300px"/>
	<tr>
		<td align="right"><font color="red">*</font>起始年龄：</td>
		<td align="left">
			<input class="main_Input" type="text" name="ags_beg_age" id="ags_beg_age" value=""/>
		</td>
	</tr>
	<tr>
	   <td align="right"><font color="red">*</font>终止年龄：</td>
		<td align="left">
			<input class="main_Input" type="text" name="ags_end_age" id="ags_end_age" value=""/>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="button" value="确认" class="btn_close" id="submit_id" name="submit_id" onclick="javascript:saveAgeGroupInfo();"/>
			<input type="button" value="关闭" class="btn_close" onclick="javascript:doClose();"/>
		</td>
	</tr>
</table>
</body>
<script>
	var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
 	var api = frameElement.api, W = api.opener;
 	$(document).ready(function(){
		$("#ags_beg_age").focus();
	});
	
	//保存会员年龄段设置
	function saveAgeGroupInfo(){
	    var ags_beg_age = $.trim($("#ags_beg_age").val());
	    if(ags_beg_age == ""){
	    	Public.tips({type: 2, content : '请输入起始年龄！'});
	    	return;
	    }else if(isNaN(ags_beg_age)){
	    	Public.tips({type: 2, content : '起始年龄只能输入数字！'});
	    	return;
	    }
		var ags_end_age = $.trim($("#ags_end_age").val());
	    if(ags_end_age == ""){
	    	Public.tips({type: 2, content : '请输入终止年龄！'});
	    	return;
	    }else if(isNaN(ags_end_age)){
	    	Public.tips({type: 2, content : '终止年龄只能输入数字！'});
	    	return;
	    }
		$.ajax({
	    	async:false,
			type:"POST",
			url:config.BASEPATH+"vip/set/saveAgeGroupSet",
			data:"ags_beg_age="+ags_beg_age+"&ags_end_age="+ags_end_age,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '添加成功!'});
					api.close();
				}else{
					Public.tips({type: 1, content : '添加失败,请重试!'});
				}
			}
		});
	         
	}
	 // 关闭按钮
	function doClose(){
		api.close();
	}
	
</script>
</html>