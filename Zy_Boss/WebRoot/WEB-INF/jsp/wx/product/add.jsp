<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 

function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="发布"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
</div>
<div >
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">发布门店：</td>
			<td width="185px">
				<input class="main_Input w146" type="text" disabled="disabled" value="${ sessionScope.user.shop_name}"/>
			</td>
			<td align="right" width="60px">商品分类：</td>
			<td width="165px">
				<input class="main_Input" type="text" readonly="readonly" name="pt_name" id="pt_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProductType();"/>
				<input type="hidden" name="wp_pt_code" id="wp_pt_code" value="" />
			</td>
			<td align="right" width="60px">商品属性：</td>
			<td width="185px">
				<input class="main_Input" type="text" readonly="readonly" name="property_name" id="property_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProperty();"/>
				<input type="hidden" name="wp_property_code" id="wp_property_code" value="" />
			</td>
			<td align="right" width="60px">售后承诺：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="promise_name" id="promise_name" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryPromise();"/>
				<input type="hidden" name="wp_promise_code" id="wp_promise_code" value="" />
			</td>
		</tr>
		<tr class="list">
			<td align="right">关联门店：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="shop_names" id="shop_names" value="" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryShop();"/>
				<input type="hidden" name="shop_codes" id="shop_codes" value="" />
			</td>
			<td align="right">商品折扣：</td>
			<td>
				<input class="main_Input w146" type="text" name="price_rate" id="price_rate" value="1.00"/>
			</td>
			<td align="right">商品类型：</td>
			<td id="td_wp_type">
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="0" checked="checked"/>正常商品</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="1" />新品</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="2" />折扣商品</label>
		  	 	<input type="hidden" id="wp_type" name="wp_type" value="0"/>
	  	 	</td>
	  	 	<td align="right">允许试穿：</td>
			<td id="td_wp_istry">
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="1"/>是</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="0" checked="checked"/>否</label>
		  	 	<input type="hidden" id="wp_istry" name="wp_istry" value="0"/>
		  	 	<span id="wp_try_way_0" style="display:none;">
		        	<label class="chk" title="到店试穿" >到店试穿
		            	<input type="checkbox" name="chatbox"/>
		            </label>
	        	</span>
	        	<span id="wp_try_way_1" style="display:none;">
		        	<label class="chk" title="送货上门" >送货上门
		            	<input type="checkbox" name="chatbox"/>
		            </label>
	        	</span>
	  	 	</td>
		</tr>
		<tr class="list last">
			<td align="right">允许购买：</td>
			<td id="td_wp_isbuy" colspan="3">
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_isbuy" type="radio" value="1" checked="checked"/>是</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_isbuy" type="radio" value="0"/>否</label>
		  	 	<input type="hidden" id="wp_isbuy" name="wp_isbuy" value="1"/>
	        	<span id="wp_buy_way_1">
		        	<label class="chk" title="上门自取" >上门自取
		            	<input type="checkbox" name="chatbox"/>
		            </label>
	        	</span>
	        	<span id="wp_buy_way_0">
		        	<label class="chk" title="快递" >快递
		            	<input type="checkbox" name="chatbox"/>
		            </label>
	        	</span>
	        	&nbsp;&nbsp;&nbsp;
	        	<span id="wp_postfree" style="display:none;">
		        	<label class="chk" title="包邮" >包邮
		            	<input type="checkbox" name="chatbox"/>
		            </label>
	        	</span>
	  	 	</td>
		</tr>
	</table>
</div>
</form>
<script src="<%=basePath%>data/wx/product/product_add.js"></script>
</body>
</html>