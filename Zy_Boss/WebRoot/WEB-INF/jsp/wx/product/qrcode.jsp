<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<style type="text/css" rel="stylesheet">
.border dt{float:left;margin:10px;height:140px;width:140px;}
.border dd{float:left;line-height:30px;margin-top:10px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/qrcode.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/zclip/jquery.zclip.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
</script> 
</head>
<body>
<div class="mainwra" style="bottom:15px;">
	<div class="border" style="border-bottom:#ccc solid 1px;height:230px;">
		<dl>
			<dd>
				<p>商品货号：<span id="pd_no"></span></p>
				<p>商品名称：<span id="pd_name"></span></p>
				<p><font style="color:red">注：右键点击二维码另存为图片</font></p>
				<p>商品链接：<a style="color:#f30;" id="oCopyUrl">点击这里复制</a></p>
				<p>
					<textarea onclick="this.select();" style="border:#ddd solid 1px;width:200px;padding:2px;height:50px;resize:none" id="goodsUrlId" readonly>
					</textarea>
				</p>
				
			</dd>
			<dt>
				<p id="qrcode"></p>
			</dt>
			
		</dl>
	</div>
</div>
</body>
<script>
var api = frameElement.api;
var qrvalue = "https://www.baidu.com";
	$("#oCopyUrl").zclip({
		path: "<%=basePath%>resources/util/zclip/ZeroClipboard.swf",
		copy: function(){
			return qrvalue;
		},
		afterCopy:function(){
			Public.tips({type: 3, content : "复制成功"});
	    }
	});
	var qrcode = new QRCode(document.getElementById("qrcode"), {
		width : 180,
		height : 180
	});
	qrcode.clear();
	qrcode.makeCode(qrvalue);
	$("#goodsUrlId").val(qrvalue);
	$("#pd_no").text(api.data.pd_no);
	$("#pd_name").text(api.data.wp_pd_name);
</script>

</html>