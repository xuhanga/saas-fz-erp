<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ueditor/ueditor.config.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ueditor/ueditor.all.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 

function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="wp_id" id="wp_id" value="${ product.wp_id}"/>
<input type="hidden" name="wp_number" id="wp_number" value="${ product.wp_number}"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red" type="button" value="发布"/>
	        <input id="btn_close" class="t-btn" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
</div>
<div id="mainContent" style="overflow-x:hidden;overflow-y:auto;">
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">发布门店：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" disabled="disabled" value="${ sessionScope.user.shop_name}"/>
			</td>
			<td align="right" width="60px">商品货号：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" disabled="disabled" value="${ product.pd_no}"/>
			</td>
			<td align="right" width="60px">商品名称：</td>
			<td width="165px">
				<input class="main_Input w146" type="text" name="wp_pd_name" id="wp_pd_name" value="${ product.wp_pd_name}"/>
			</td>
			<td align="right" width="60px">商品分类：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="pt_name" id="pt_name" value="${ product.wp_pt_name}" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProductType();"/>
				<input type="hidden" name="wp_pt_code" id="wp_pt_code" value="${ product.wp_pt_code}" />
			</td>
			
		</tr>
		<tr class="list">
			<td align="right">商品属性：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="property_name" id="property_name" value="${ product.wp_property_name}" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryProperty();"/>
				<input type="hidden" name="wp_property_code" id="wp_property_code" value="" />
			</td>
			<td align="right">售后承诺：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="promise_name" id="promise_name" value="${ product.wp_promise_name}" style="width:122px"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryPromise();"/>
				<input type="hidden" name="wp_promise_code" id="wp_promise_code" value="" />
			</td>
			<td align="right">商品折扣：</td>
			<td>
				<input class="main_Input w146" type="text" name="price_rate" id="price_rate" value="1.00"/>
			</td>
			<td align="right">商品类型：</td>
			<td id="td_wp_type">
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="0" ${ product.wp_type eq 0 ? 'checked' : ''}/>正常商品</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="1" ${ product.wp_type eq 1 ? 'checked' : ''}/>新品</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_type" type="radio" value="2" ${ product.wp_type eq 2 ? 'checked' : ''}/>折扣商品</label>
		  	 	<input type="hidden" id="wp_type" name="wp_type" value="${ product.wp_type}"/>
	  	 	</td>
		</tr>
		<tr class="list">
			<td align="right">虚拟销售：</td>
			<td>
				<input class="main_Input w146" type="text" name="wp_virtual_amount" id="wp_virtual_amount" value="${ product.wp_virtual_amount}"/>
			</td>
			<td align="right">个人限购：</td>
			<td>
				<input class="main_Input w146" type="text" name="wp_buy_limit" id="wp_buy_limit" value="${ product.wp_buy_limit}"/>
			</td>
			<td align="right">零售价：</td>
			<td>
				<input class="main_Input w146" type="text" name="wp_sell_price" id="wp_sell_price" value="${ product.wp_sell_price}"/>
			</td>
			<td align="right">允许试穿：</td>
			<td id="td_wp_istry">
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="1" ${ product.wp_istry eq 1 ? 'checked' : ''}/>是</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_istry" type="radio" value="0" ${ product.wp_istry eq 0 ? 'checked' : ''}/>否</label>
		  	 	<input type="hidden" id="wp_istry" name="wp_istry" value="${ product.wp_istry}"/>
		  	 	<span id="wp_try_way_0" style="display:${ product.wp_istry eq 1 ? '' : 'none'};">
		        	<label class="chk" title="到店试穿" >到店试穿
		            	<input type="checkbox" name="chatbox" ${fn:contains(product.wp_try_way,'0') ? 'checked' : ''}/>
		            </label>
	        	</span>
	        	<span id="wp_try_way_1" style="display:${ product.wp_istry eq 1 ? '' : 'none'};">
		        	<label class="chk" title="送货上门" >送货上门
		            	<input type="checkbox" name="chatbox" ${fn:contains(product.wp_try_way,'1') ? 'checked' : ''}/>
		            </label>
	        	</span>
	  	 	</td>
		</tr>
		<tr class="list last">
			<td align="right">商品权重：</td>
			<td>
				<input class="main_Input w146" type="text" name="wp_weight" id="wp_weight" value="${ product.wp_weight}"/>
			</td>
			<td align="right">副标题：</td>
			<td>
				<input class="main_Input w146" type="text" name="wp_subtitle" id="wp_subtitle" value="${ product.wp_subtitle}"/>
			</td>
			<td align="right">折扣价：</td>
			<td>
				<input class="main_Input w146" type="text" name="wp_rate_price" id="wp_rate_price" value="${ product.wp_rate_price}"/>
			</td>
			<td align="right">允许购买：</td>
			<td id="td_wp_isbuy">
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_isbuy" type="radio" value="1" ${ product.wp_isbuy eq 1 ? 'checked' : ''}/>是</label>
		  	 	<label class="radio" style="width:30px"><input name="radio_wp_isbuy" type="radio" value="0" ${ product.wp_isbuy eq 0 ? 'checked' : ''}/>否</label>
		  	 	<input type="hidden" id="wp_isbuy" name="wp_isbuy" value="${ product.wp_isbuy}"/>
	        	<span id="wp_buy_way_1" style="display:${ product.wp_isbuy eq 1 ? '' : 'none'};">
		        	<label class="chk" title="上门自取" >上门自取
		            	<input type="checkbox" name="chatbox" ${fn:contains(product.wp_buy_way,'1') ? 'checked' : ''}/>
		            </label>
	        	</span>
	        	<span id="wp_buy_way_0" style="display:${ product.wp_isbuy eq 1 ? '' : 'none'};">
		        	<label class="chk" title="快递" >快递
		            	<input type="checkbox" name="chatbox" ${fn:contains(product.wp_buy_way,'0') ? 'checked' : ''}/>
		            </label>
	        	</span>
	        	&nbsp;&nbsp;&nbsp;
	        	<span id="wp_postfree" style="display:${ product.wp_isbuy eq 1 and fn:contains(product.wp_buy_way,'0') ? '' : 'none'};">
		        	<label class="chk" title="包邮" >包邮
		            	<input type="checkbox" name="chatbox" ${ product.wp_postfree eq 1 ? 'checked' : ''}/>
		            </label>
	        	</span>
	  	 	</td>
		</tr>
		<tr class="list last">
			<td colspan="8">
				<script id="wp_desc" name="wp_desc" type="text/plain" style="width:90%;height:380px;" disabled="disabled">${ product.wp_desc}</script>
	        </td>
	        <%-- <td colspan="8">
	        	<iframe src="<%=basePath%>resources/util/bootstrap-wysiwyg/editor.jsp" frameborder="0" height="380px" width="100%" scrolling="no"></iframe>
	        </td> --%>
		</tr>
	</table>
</div>
</form>
<script type="text/javascript">
    var wp_desc = UE.getEditor('wp_desc',{
    	autoHeightEnabled: false,
    	elementPathEnabled:false,
    	maximumWords:3000
    });
    UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;  
    UE.Editor.prototype.getActionUrl = function(action) {  
        if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadvideo') {  
            return '${context_path}wx/product/uploadimage';  
        } else {  
            return this._bkGetActionUrl.call(this, action);  
        }  
    } 
    $("#mainContent").height($(parent).height()-80);
</script>

<script src="<%=basePath%>data/wx/product/product_update.js"></script>
</body>
</html>