<%@ page contentType="text/html; charset=gbk" language="java" %>
<%@ include file="/common/path.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>商品分类</title>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/ztree/ztree/ztree.css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/base.css"/>
<style>
.treemenu{margin-top:1px;margin-left:5px;overflow-y:auto;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery-1.3.2.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ztree/jquery.ztree.core-3.4.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/ztree/jquery.ztree.excheck-3.4.min.js\"></sc"+"ript>");
</script>
<script>
var api = frameElement.api
var dblClick = false;
var multiselect = false;//默认单选
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var data;
var setting = {
	check: {
		enable: multiselect,
		chkboxType: { "Y" : "s", "N" : "s" }
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback:{
		onClick:function(event, treeId, treeNode, clickFlag){
			data = {};
			data.pt_code=treeNode.id;
			data.pt_name=treeNode.name;
			data.pt_upcode=treeNode.pId;
		},
		onDblClick:function(event, treeId, treeNode){
			if(!multiselect){//单选双击关闭
				data = {};
				data.pt_code=treeNode.id;
				data.pt_name=treeNode.name;
				data.pt_upcode=treeNode.pId;
				dblClick = true;
				api.close();
			}
		}
	}
};
$(function(){
	$.ajax({
		type:"post",
		url:"${static_path}wx/producttype/tree",
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data){
				$.fn.zTree.init($("#role-tree"), setting, eval("(" + data.data + ")"));
			}
		}
	});
});
function doSelect(){
	if(multiselect){//多选
		var treeObj = $.fn.zTree.getZTreeObj("role-tree");
		var nodes = treeObj.getCheckedNodes(true);
		if(nodes == "" || nodes.length == 0){
			return false;
		}
		var result = [];
		for (var i = 0; i < nodes.length; i++) {
			if(nodes[i].id == 0){
				continue;
			}
			var node = {};
			node.pt_code = nodes[i].id;
			node.pt_name = nodes[i].name;
			node.pt_upcode = nodes[i].pId;
			result.push(node);
		}
		return result;
	} else {//单选
		if (data == null || data == undefined) {
			return false;
		}
		return data;
	}
}
</script>
</head>  
<body>
	<div class="treemenu" >
		<div id="roleTree" class="divlistContent" style="overflow-y:auto;">
			<ul id="role-tree" class="ztree"></ul>
		</div>
	</div>
</body>
</html>