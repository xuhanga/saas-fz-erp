<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:40px; }
	table tr td b {color:red;}
	.main_Input{width: 165px; height: 16px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.form.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/UpImgPreview.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="pt_id" id="pt_id" value="${productType.pt_id }"/>
<input type="hidden" name="pt_code" id="pt_code" value="${productType.pt_code }"/>
<div class="border">
	<table width="100%" class="pad_t20">
	    <tr class="list first">
			<td align="right" width="100px"><b>*</b>分类名称：</td>
		    <td>
			      <input class="main_Input" type="text"  name="pt_name"  id="pt_name" value="${productType.pt_name }" maxlength="30" />
		    </td>
	     </tr>
	     <tr class="list" style="display:none;">
			<td align="right"><b>*</b>权重：</td>
		    <td>
			      <input class="main_Input" type="text"  name="pt_weight"  id="pt_weight" value="${productType.pt_weight }" maxlength="6" />
		    </td>
	     </tr>
	     <tr class="list">
			<td align="right">状态：</td>
		    <td id="state">
		    	<label class="radio" style="width:30px">
	  	 			<input name="_pt_state" type="radio" value="0" ${productType.pt_state eq 0 ? 'checked':'' }/>正常
		  	 	</label>
		  	 	<label class="radio" style="width:30px">
		  	 		<input name="_pt_state" type="radio" value="1" ${productType.pt_state eq 1 ? 'checked':'' }/>停用
		  	 	</label>
	  			<input type="hidden" name="pt_state" id="pt_state" value="${productType.pt_state }"/>
		    </td>
	     </tr>
	     <tr class="list last">
			<td align="right">图片：</td>
			<td>
				<input class="main_Input" type="file" name="img_type" id="img_type" />
				<span style="color:red;">【大小不要超过200K。】</span>
				<c:choose>
					<c:when test="${ !empty productType.pt_img_path}">
						<img id="imgview" style="display:inherit;" width="200" height="160" 
							src="<%=serverPath%>${productType.pt_img_path}"/>
					</c:when>
					<c:otherwise>
						<img id="imgview" style="display:inherit;" width="200" height="160" />
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</table>
</div>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/wx/producttype/producttype_add.js"></script>
</body>
</html>