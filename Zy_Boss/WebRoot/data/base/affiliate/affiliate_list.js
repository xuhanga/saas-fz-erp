var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = eval('system.MENULIMIT.'+Public.getUrlParam('menu_code'));
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/affiliate/list';
var pdata = {};
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 300;
		var width = 330;
		var selected = window.parent.frames["treeFrame"].selectedTreeNode;
		if(oper == 'add'){
			if(selected == undefined){
				Public.tips({type: 2, content : '请选择父级区域!'});
				return false;
			}
			title = '新增';
			url = config.BASEPATH+"base/affiliate/to_add";
			data = {oper: oper, callback:this.callback};
			if(selected != undefined){
				data.af_upcode = selected.id;
			}
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"base/affiliate/to_update?af_id="+id;
			data = {oper: oper,callback: this.callback};
			if(selected != undefined){
				data.af_upcode = selected.id;
			}
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/affiliate/del',
					data:{"af_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 70, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
	    	{name: 'af_code',label:'编号',index: 'af_code',width:80},
	    	{name: 'af_name',label:'名称',index: 'af_name',width:120},
	    	{name: 'af_man',label:'联系人',index: 'af_man',width:120},
	    	{name: 'af_tel',label:'联系电话',index: 'af_tel',width:120},
	    	{name: 'af_mobile',label:'手机号码',index: 'af_mobile',width:120},
	    	{name: 'af_addr',label:'联系地址',index: 'af_addr',width:120},
	    	{name: 'af_remark',label:'备注',index: 'af_remark',width:180}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'af_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("af_upcode") && data.af_upcode != ''){
				param += "&af_upcode="+data.af_upcode;
			}
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
}
THISPAGE.init();