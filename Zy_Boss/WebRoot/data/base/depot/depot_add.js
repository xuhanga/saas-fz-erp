var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		var dp_name = $("#dp_name").val().trim();
		var dp_shop_code = $("#dp_shop_code").val();
	    if(dp_name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#dp_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#dp_name").val(dp_name);//防止空格
	    }
	    if(null == dp_shop_code || "" == dp_shop_code){
	    	W.Public.tips({type: 1, content : "请选择店铺"});
	     	setTimeout("api.zindex()",400);
	        return;
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/depot/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					setTimeout("api.close()",300);
					handle.loadData();
				}else{
					W.Public.tips({type: 1, content : data.message});
					W.isRefresh = true;
					setTimeout("api.index()",300);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
		this.doShop();
	},
	initParam:function(){
		shopCombo = $('#spanShop').combo({
			value: 'sp_code',
	        text: 'sp_name',
			width : 158,
			height : 30,
			listId : '',
			defaultSelected: 0,
			editable : true,
			callback : {
				onChange : function(data) {
					$("#dp_shop_code").val(data.sp_code);
				}
			}
		}).getCombo();
		var pdata = [
				{id:'0',name:'正常'},
				{id:'1',name:'停用'},
				]
		stateCombo = $('#spanState').combo({
			data:pdata,
			value: 'id',
	        text: 'name',
			width : 158,
			height : 30,
			listId : '',
			defaultSelected: 0,
			editable : true,
			callback : {
				onChange : function(data) {
					$("#dp_state").val(data.id);
				}
			}
		}).getCombo();
	},
	doShop:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'base/shop/up_sub_list',
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					shopCombo.loadData(data.data);
				}
			}
		});
	},
	initDom:function(){
		$("#dp_name").val("").focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();