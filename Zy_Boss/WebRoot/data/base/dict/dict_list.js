var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var queryurl = config.BASEPATH+'base/dict/uplist';
var querylisturl = config.BASEPATH+'base/dict/list';
var _limitMenu = system.MENULIMIT.BASE12;
var _menuParam = config.OPTIONLIMIT;
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 200;
		var width = 250;
		if(oper == 'add'){
			id = $("#grid").jqGrid('getGridParam','selrow');
			var upcode = $("#grid").jqGrid("getRowData",id).dt_code;
			title = '新增';
			url = config.BASEPATH+"base/dict/to_add";
			data = {oper: oper,upcode:upcode,callback: this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"base/dict/to_update?dtl_id="+id;
			data = {oper: oper,upcode:upcode,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/dict/del',
					data:{"dtl_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#gridList').jqGrid('delRowData', rowIds);
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#gridList").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#gridList").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#gridList").jqGrid('addRowData', data.id, data, 'first');
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		this.initGridList();
	},
	initDom:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
            {label:'ID',name: 'dt_id', index: 'dt_id', width: 0,hidden:true},
	    	{label:'名称',name: 'dt_name', index: 'dt_name', width: 120},
	    	{label:'dt_code',name: 'dt_code', index: 'dt_code', hidden:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: 200,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: false,
			rowNum:30,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'dt_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				if(ids.length>0){
					$("#grid tr:eq(1)").trigger("click");
				}else{
					$("#gridList").clearGridData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				var params = "";
				params += "dtl_upcode="+$("#grid").jqGrid("getRowData",rowid).dt_code;
				$("#gridList").jqGrid('setGridParam',{datatype:"json",url:querylisturl+"?"+params}).trigger("reloadGrid");
			}
	    });
	},
	initGridList:function(){
		var gridWH = Public.setGrid();
		var colModel = [
            {name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
	    	{label:'编号',name: 'dtl_code', index: 'dtl_code', width: 100},
	    	{label:'名称',name: 'dtl_name', index: 'dtl_name', width: 140}
	    ];
		$('#gridList').jqGrid({
			datatype: 'local',
			width: 400,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#pageList',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'dtl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParam:function(){
		var param = "";
		return param;
	},
	reset:function(){
	},
	reloadData:function(){
		var param = THISPAGE.buildParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			};
			handle.operate('add');
		});
		//修改
		$('#gridList').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#gridList').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();