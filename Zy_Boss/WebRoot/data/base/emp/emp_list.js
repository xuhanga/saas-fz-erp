var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE07;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/emp/list';
var pdata = {};

var Utils = {
	doQueryReward:function(rowid){
		var rowData = $('#grid').jqGrid('getRowData', rowid);
		commonDia = $.dialog({
			title : '奖励查询',
			content : 'url:'+config.BASEPATH+'base/empreward/to_stat',
			data : {em_code:rowData.em_code},
			width : 520,
			height : 340,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:true,
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		var height = 350;
		var width = 500;
		var selected = window.parent.frames["treeFrame"].selectedTreeNode;
		if(oper == 'add'){
			title = '新增员工信息';
			url = config.BASEPATH+"base/emp/to_add";
			data = {oper: oper, callback:this.callback};
			if(selected != undefined){
				data.af_upcode = selected.id;
			}
		}else if(oper == 'edit'){
			title = '修改员工信息';
			url = config.BASEPATH+"base/emp/to_update?em_id="+id;
			data = {oper: oper,callback: this.callback};
			if(selected != undefined){
				data.em_id = selected.id;
			}
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/emp/del',
					data:{"em_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},operFmatter:function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-reward" title="奖励">&#x3435;</i>';
		html_con += '</div>';
		return html_con;
	},formatState:function(val, opt, row){
		//0在职1离职
		if(val == "0"){
			return "在职";
		}else if(val == "1"){
			return "离职";
		}else{
			return val;
		}
	},formatType:function(val, opt, row){
		//0职员1导购员2业务员
		if(val == "0"){
			return "导购员";
		}else if(val == "1"){
			return "业务员";
		}else if(val == "2"){
			return "职员";
		}else if(val == "3"){
			return "店长";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate', label:'操作',width: 80, fixed:true, formatter: handle.operFmatter, title: false,sortable:false},
		    {name: 'em_code', label:'编号',index: 'em_code', width: 50, title: false,fixed:true},
	    	{name: 'em_name', label:'姓名',index: 'em_name', width: 70, title: false},
	    	{name: 'em_sex', label:'性别',index: 'em_sex', width: 40, title: false,align:'center'},
	    	{name: 'em_birthday', label:'生日',index: 'em_birthday', width: 80, title: false,align:'center'},
	    	{name: 'em_mobile', label:'手机',index: 'em_mobile', width: 90, title: false},
	    	{name: 'em_state', label:'状态',index: 'em_state', width: 40, title: false,align:'center',formatter:handle.formatState},
	    	{name: 'em_dm_name', label:'部门',index: 'em_dm_name', width:50, title: false,align:'center'},
	    	{name: 'em_type', label:'类型',index: 'em_type', width: 55, title: false,align:'center',formatter:handle.formatType},
	    	{name: 'em_shop_name', label:'门店',index: 'em_shop_name', width: 110, title: false},
	    	{name: 'reward_count', label:'奖励个数',index: '', width: 80, sortable: false,align:'center'},
	    	{name: 'em_indate', label:'进店时间',index: 'em_indate', width: 80, title: false,align:'center'},
	    	{name: 'em_workdate', label:'工作时间',index: 'em_workdate', width: 80, title: false,align:'center'},
	    	{name: 'em_addr', label:'住址',index: 'em_addr', width: 100, title: true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'em_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("dm_code") && data.dm_code != ''){
				param += "&dm_code="+data.dm_code;
			}
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			Public.tips({type: 2, content : '暂未开放!'});
			return;
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-reward', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			Utils.doQueryReward(id);
		});
	}
}
THISPAGE.init();