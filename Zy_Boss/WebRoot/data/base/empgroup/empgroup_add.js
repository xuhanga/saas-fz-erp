var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var _height = api.config.height-182,_width = api.config.width-34;//获取弹出框弹出宽、高
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'base/emp/list';
var handle = {
	formatType:function(val){
		if(val == 0){
			return "导购员";
		}
		if(val == 1){
			return "业务员";
		}
		if(val == 2){
			return "职员";
		}
		if(val == 3){
			return "店长";
		}
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.eg_id;
		pdata.eg_code=data.eg_code;
		pdata.eg_name=data.eg_name;
		pdata.eg_remark=data.eg_remark;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

function ok_Click(){
	var eg_name = $("#eg_name").val();
	if(eg_name == ''){
		Public.tips({type: 2, content : '请输入员工组名称！'});
		$("#eg_name").focus();
		return false;
	}
	var params = '';
	params += 'eg_name='+Public.encodeURI($('#eg_name').val());
	params += '&eg_remark='+Public.encodeURI($('#eg_remark').val());
	
    var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
    if(ids == null || ids == '' || ids.length == 0){
    	Public.tips({type: 2, content : '请选择员工！'});
    	return false;
    }
    var codes = [];
	for(var i=0;i<ids.length;i++){
		var rowData = $('#grid').jqGrid('getRowData', ids[i]);
		codes.push(rowData.em_code);
	}
	params += '&codes='+codes.join(",");
	$.ajax({
		type:"POST",
		url:config.BASEPATH+"base/empgroup/save",
		data:params,
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				handle.loadData(data.data);
				api.close();
			}else{
				Public.tips({type: 1, content : data.message});
			}
		}
	});
	return false;
}


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#eg_name").focus();
	},
	initGrid:function(){
		var colModel = [
			{name: 'em_code', label:'员工编号',index: 'em_code', width: 100, title: false},
	    	{name: 'em_name', label:'姓名',index: 'em_name', width: 140, title: false},
	    	{name: 'em_type', label:'类型',index: 'em_type', width: 100, title: false,formatter:handle.formatType},
	    	{name: 'em_shop_code',index: 'em_shop_code', hidden:true, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:true,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'em_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				
            }
	    });
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();