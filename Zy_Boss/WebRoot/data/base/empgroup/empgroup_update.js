var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var _height = api.config.height-182,_width = api.config.width-34;//获取弹出框弹出宽、高
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'base/empgroup/listEmps?eg_code='+$("#eg_code").val();
var handle = {
	formatType:function(val){
		if(val == 0){
			return "导购员";
		}
		if(val == 1){
			return "业务员";
		}
		if(val == 2){
			return "职员";
		}
		if(val == 3){
			return "店长";
		}
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.eg_id;
		pdata.eg_code=data.eg_code;
		pdata.eg_name=data.eg_name;
		pdata.eg_remark=data.eg_remark;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

function ok_Click(){
	var eg_name = $("#eg_name").val();
	if(eg_name == ''){
		Public.tips({type: 2, content : '请输入员工组名称！'});
		$("#eg_name").focus();
		return false;
	}
	var params = '';
	params += 'eg_id='+$('#eg_id').val();
	params += '&eg_code='+$('#eg_code').val();
	params += '&eg_name='+Public.encodeURI($('#eg_name').val());
	params += '&eg_remark='+Public.encodeURI($('#eg_remark').val());
	
    var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
    if(ids == null || ids == '' || ids.length == 0){
    	Public.tips({type: 2, content : '请选择员工！'});
    	return false;
    }
    var codes = [];
	for(var i=0;i<ids.length;i++){
		var rowData = $('#grid').jqGrid('getRowData', ids[i]);
		codes.push(rowData.em_code);
	}
	params += '&codes='+codes.join(",");
	$.ajax({
		type:"POST",
		url:config.BASEPATH+"base/empgroup/update",
		data:params,
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				handle.loadData(data.data);
				api.close();
			}else{
				Public.tips({type: 1, content : data.message});
			}
		}
	});
	return false;
}


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#eg_name").select();
	},
	initGrid:function(){
		var colModel = [
			{label:'员工编号',name: 'em_code', index: 'em_code', width: 100},
	    	{label:'姓名',name: 'em_name', index: 'em_name', width: 140},
	    	{label:'类型',name: 'em_type', index: 'em_type', width: 100,formatter:handle.formatType},
	    	{label:'',name: 'em_state', index: 'em_state', width: 100,hidden:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:true,//多选
			viewrecords: true,
			rowNum:999,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'em_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").jqGrid('getDataIDs');
				for (var i = 0; i < ids.length; i++) {
					var rowData = $("#grid").jqGrid("getRowData", ids[i]);
					if (rowData.em_state == '1') {
						$("#grid").jqGrid('setSelection', ids[i], false);
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();