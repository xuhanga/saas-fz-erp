var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var Utils = {
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:false},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_bd_code").val(selected.bd_code);
					$("#pd_bd_name").val(selected.bd_name);
				}
			},
			cancel:true
		});
	},
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货单位',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 400,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_sp_code").val(selected.sp_code);
					$("#pd_sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:false},
			width : 340,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_tp_code").val(selected.tp_code);
					$("#pd_tp_name").val(selected.tp_name);
					$("#pd_tp_upcode").val(selected.tp_upcode);
				}
			},
			cancel:true
		});
	},
    addColor : function(){//添加颜色
    	$.dialog({ 
			title: '添加颜色',
			max: false,
			min: false,
			width: 250,
			height: 200,
			fixed:false,
			drag: true,
			content : 'url:'+config.BASEPATH+'base/color/to_add_buyProduct',
			close: function () {
				handle.showColor();
		   	}
		});
	},
	doQueryBra:function(){
		var pd_bra = $('#pd_bra').val();
		if(pd_bra == 0){
			Public.tips({type: 2, content : "请选择文胸为是！"});
			return;
		}
		commonDia = $.dialog({
			title : '选择杯型',
			content : 'url:'+config.BASEPATH+'base/bra/to_list_dialog',
			data : {multiselect:true},
			width : 400,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var codes = [];
					var names = [];
					for(var i=0;i<selected.length;i++){
						codes.push(selected[i].br_code);
						names.push(selected[i].br_name);
					}
					$("#pdb_br_codes").val(codes.join(","));
					$("#pdb_br_name").val(names.join(","));
				}
			},
			cancel:true
		});
	},
	doQuerySzg:function(){
		commonDia = $.dialog({
			title : '选择尺码组',
			content : 'url:'+config.BASEPATH+'base/size/to_list_group_dialog',
			data : {multiselect:false},
			width : 580,
			height : 470,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_szg_code").val(selected.szg_code);
					$("#pd_szg_name").val(selected.szg_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	pdNoIfExist:function(){//验证货号是否存在
		var pd_no = $("#pd_no_dis").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/pdNoIfExist",
			data:"pd_no="+pd_no,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == "exist"){
						Public.tips({type: 2, content : "您输入的编号已存在,请核对后再重新输入!"});
						document.getElementById("pd_no_dis").value="";
						document.getElementById("pd_no_dis").focus();
						return;
					}
				}
			}
		});
	},
	save:function(){
//		if (!$("#form1").valid()) {
//			return;
//		}
		var pd_no_dis = $('#pd_no_dis').val();
		$('#pd_no').val(pd_no_dis);
		var color_select = document.getElementById('color_select').length;
		if(color_select<=0){
			Public.tips({type: 2, content : "请选择颜色！"});
		    return;
		}
		//获取商品对应颜色codes列表
		var select_color_codes = "";
		var color_list = form1.color_select.options;
		for(i=0;i<color_list.length;i++){
			select_color_codes += color_list.item(i).value+",";
		}
		select_color_codes = select_color_codes.substring(0,select_color_codes.length-1);
		form1.pdc_cr_codes.value = select_color_codes;
		
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					W.isRefresh = true;
					/*handle.loadData(data.data);*/
					handle.retainData();
					setTimeout("api.zindex()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	update:function(){
		var pd_no_dis = $('#pd_no_dis').val();
		$('#pd_no').val(pd_no_dis);
		var color_select = document.getElementById('color_select').length;
		if(color_select<=0){
			Public.tips({type: 2, content : "请选择颜色！"});
		    return;
		}
		//获取商品对应颜色codes列表
		var select_color_codes = "";
		var color_list = form1.color_select.options;
		for(i=0;i<color_list.length;i++){
			select_color_codes += color_list.item(i).value+",";
		}
		select_color_codes = select_color_codes.substring(0,select_color_codes.length-1);
		form1.pdc_cr_codes.value = select_color_codes;
		
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					W.isRefresh = true;
					$('#pd_no_dis').removeAttr('disabled');
		       		$('#pd_id').val('');
		       		$('#pd_code').val('');
					handle.retainData();
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	saveByBar:function(){//添加条形码保存
		var pd_no_dis = $('#pd_no_dis').val();
		$('#pd_no').val(pd_no_dis);
		var color_select = document.getElementById('color_select').length;
		if(color_select<=0){
			Public.tips({type: 2, content : "请选择颜色！"});
		    return;
		}
		//获取商品对应颜色codes列表
		var select_color_codes = "";
		var color_list = form1.color_select.options;
		for(i=0;i<color_list.length;i++){
			select_color_codes += color_list.item(i).value+",";
		}
		select_color_codes = select_color_codes.substring(0,select_color_codes.length-1);
		form1.pdc_cr_codes.value = select_color_codes;
		
		var pd_id = $('#pd_id').val();
		if(pd_id != ""){
			handle.addBarCode(pd_id);
			return;
		}
		$.dialog.confirm('您确定保存商品信息，并进行条形码维护？', function(){
			$("#btn-save").attr("disabled",true);
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"base/product/save",
				data:$('#form1').serialize(),
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : data.message});
						W.isRefresh = true;
			       		$('#pd_id').val(data.data.pd_id);
			       		$('#pd_code').val(data.data.pd_code);
			       		$('#pd_no_dis').attr('disabled','block');
			       		handle.addBarCode(data.data.pd_id);
						setTimeout("api.zindex()",500);
					}else{
						Public.tips({type: 1, content : data.message});
						setTimeout("api.zindex()",500);
					}
					$("#btn-save").attr("disabled",false);
				}
			});
		});
	},
	addBarCode: function(id){//条形码录入
    	$.dialog({
		   	id:id,
		   	title:'条形码录入',
			max: false,
			min: false,
			width:800,
			height:500,
			fixed:true,
			drag: true,
		   	resize:false,
		   	lock:false,
		   	content:'url:'+config.BASEPATH+'base/product/to_barcode_add?pd_id='+id
	    });
    },
	retainData:function(){
		var retainDataObj = document.getElementById("pi_retain_data");
   		if(retainDataObj.checked){
   			document.getElementById("pd_no_dis").value = "";
    		document.getElementById("pd_no").value = "";
    		changeAll(form1.right_item, form1.left_item);
    	}else{
    		document.getElementById("pd_no_dis").value = "";
    		document.getElementById("pd_no").value = "";
    		document.getElementById("pd_number").value = "";
    		document.getElementById("sort_discount").value = "1";
    		document.getElementById("pd_sort_price").value = "0";
    		document.getElementById("pd_bd_name").value = "";
    		document.getElementById("pd_bd_code").value = "";
    		document.getElementById("pd_name").value = "";
    		document.getElementById("pd_commission").value = "";
    		document.getElementById("pd_commission_rate").value = "";
    		document.getElementById("pd_tp_name").value = "";
    		document.getElementById("pd_tp_code").value = "";
    		document.getElementById("pd_tp_upcode").value = "";
    		document.getElementById("BatchDiscount").value = "";
    		document.getElementById("pd_batch_price").value = "0";
    		document.getElementById("pd_sp_name").value = "";
    		document.getElementById("pd_sp_code").value = "";
    		document.getElementById("pd_score").value = "";
    		document.getElementById("pd_szg_name").value = "";
    		document.getElementById("pd_szg_code").value = "";
    		document.getElementById("pd_date").value = "";
    		document.getElementById("pd_returndate").value = "";
    		document.getElementById("pdb_br_name").value = "";
    		document.getElementById("pdb_br_codes").value = "";
    		document.getElementById("pd_buy_cycle").value = "10";
    		document.getElementById("pd_sell_cycle").value = "90";
    		document.getElementById("colorSearch").value = "";
    		changeAll(form1.right_item, form1.left_item);
    		document.getElementById("pdc_cr_codes").value = "";
    		document.getElementById("pd_in_fabric").value = "";
    		document.getElementById("discount").value = "1";
    		document.getElementById("pd_sign_price").value = "0";
    		document.getElementById("pd_sell_price").value = "0";
    		document.getElementById("pd_vip_price").value = "0";
    		document.getElementById("pd_buy_price").value = "0";
    		document.getElementById("pd_cost_price").value = "0";
    		document.getElementById("pd_style").value = "";
    		document.getElementById("pd_salesman_comm").value = "";
    		document.getElementById("pd_salesman_commrate").value = "";
    		document.getElementById("pd_wash_explain").value = "";
    		styleCombo.loadData(styleInfo,[0]);
    		document.getElementById("pd_season").value = "";
    		seasonCombo.loadData(seasonInfo,[0]);
    		document.getElementById("pd_unit").value = "";
    		unitCombo.loadData(unitInfo,[0]);
    		document.getElementById("pd_fabric").value = "";
    		stuffCombo.loadData(stuffInfo,[0]);
    		document.getElementById("pd_price_name").value = "";
    		priceCombo.loadData(priceInfo,[0]);
    		document.getElementById("pd_year").value = "";
    		yearCombo.loadData(yearInfo.data.items,[7]);
    		isbraCombo.loadData(isbraInfo.data.items,[0]);
    		document.getElementById("pd_change").value = "1";
    		isChangeCombo.loadData(isChangeInfo.data.items,[1]);
    		document.getElementById("pd_buy").value = "1";
    		isBuyCombo.loadData(isBuyInfo.data.items,[1]);
    		document.getElementById("pd_vip_sale").value = "1";
    		isVipSaleCombo.loadData(isVipSaleInfo.data.items,[1]);
    		document.getElementById("pd_present").value = "0";
    		isPresentCombo.loadData(isPresentInfo.data.items,[0]);
    		document.getElementById("pd_gift").value = "0";
    		isGiftCombo.loadData(isGiftInfo.data.items,[0]);
    		document.getElementById("pd_point").value = "1";
    		isPointCombo.loadData(isPointInfo.data.items,[1]);
    		document.getElementById("pd_sale").value = "0";
    		isSaleCombo.loadData(isSaleInfo.data.items,[0]);
    		
    		THISPAGE.initDate();
    	}
	},
	/*loadData:function(data){
		var pdata = {};
		
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},*/
	changeInPrice:function(){//进价折扣
		var pd_sell_price = document.getElementById("pd_sell_price").value;
		var discount = document.getElementById("discount").value;
		document.getElementById("pd_buy_price").value=(pd_sell_price * discount).toFixed(2);
	 	document.getElementById("pd_cost_price").value=(pd_sell_price * discount).toFixed(2);
	},
	changeBatchPrice:function(){//批发折扣
		var pd_sell_price = document.getElementById("pd_sell_price").value;
	 	var BatchDiscount = document.getElementById("BatchDiscount").value;
	 	document.getElementById("pd_batch_price").value = (pd_sell_price * BatchDiscount).toFixed(2);
	 	document.getElementById("pd_batch_price1").value = (pd_sell_price * BatchDiscount).toFixed(2);
	 	document.getElementById("pd_batch_price2").value = (pd_sell_price * BatchDiscount).toFixed(2);
	 	document.getElementById("pd_batch_price3").value = (pd_sell_price * BatchDiscount).toFixed(2);
	},
	changeSorthPrice:function(){//配送折扣
		var pd_sell_price = document.getElementById("pd_sell_price").value;
	 	var sort_discount = document.getElementById("sort_discount").value;
	 	document.getElementById("pd_sort_price").value = (pd_sell_price * sort_discount).toFixed(2);
	},
	changePrice:function(){
		var erg = /^[0-9]+([.]{1}[0-9]{1,8})?$/;
		var discount = $("#discount").val();
		var pds_sign_price = $("#pd_sign_price").val();
		if(!erg.test(pds_sign_price)){
			pds_sign_price = 0;
		}
		var disPrice = parseFloat(pds_sign_price*discount).toFixed(2);
		$("#pd_cost_price").val(disPrice);
		$("#pd_sell_price").val(parseFloat(pds_sign_price));
		$("#pd_vip_price").val(pds_sign_price);
		$("#pd_buy_price").val(parseFloat(disPrice));
		
		var BatchDiscount = document.getElementById("BatchDiscount").value;
		if(BatchDiscount != "" && !isNaN(BatchDiscount)){//更新批发价
			document.getElementById("pd_batch_price").value = (parseFloat(pds_sign_price) * BatchDiscount).toFixed(2);
			document.getElementById("pd_batch_price1").value = (parseFloat(pds_sign_price) * BatchDiscount).toFixed(2);
		}
	},
	changeRetailPrice:function(){
		var pd_sell_price = document.getElementById("pd_sell_price").value;
		var discount = document.getElementById("discount").value;
		document.getElementById("pd_vip_price").value = pd_sell_price;
		document.getElementById("pd_cost_price").value = (pd_sell_price * discount).toFixed(2);
		document.getElementById("pd_buy_price").value = (pd_sell_price * discount).toFixed(2);
		var BatchDiscount = document.getElementById("BatchDiscount").value;
		if(BatchDiscount != "" && !isNaN(BatchDiscount)){//更新批发价
			document.getElementById("pd_batch_price").value = (pd_sell_price * BatchDiscount).toFixed(2);
			document.getElementById("pd_batch_price1").value = (pd_sell_price * BatchDiscount).toFixed(2);
		}
	},
	showSize:function(code){//显示尺码
		var szg_code = code;
		if(szg_code.length > 0){
			$.ajax({
				type:"get",
				url:config.BASEPATH +"base/size/sizeListByProduct",
				data:"szg_code="+szg_code,
				cache:false,
				dataType:"json",
				success:function(data){
					if(data.size_html != undefined){
						$("#size_list").html(data.size_html);
					}else{
						$("#size_list").html("");
					}
				}
			});
		}else{
			$("#size_list").html("");
		}
	},
	showColor:function(){//显示颜色
		var colorSearch = document.getElementById("colorSearch").value;
		colorSearch = encodeURI(encodeURI(colorSearch));
		var hadSelect_codes = "";
		var color_list = form1.right_item.options;
		for(i=0;i<color_list.length;i++){
			hadSelect_codes += color_list.item(i).value+",";
		}
		hadSelect_codes = hadSelect_codes.substring(0,hadSelect_codes.length-1);
		$.ajax({
			type:"get",
			url:config.BASEPATH +"base/color/colorListByProduct",
			data:"colorSearch="+colorSearch+"&colorCodes="+hadSelect_codes,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data.color_html != undefined){
					$("#color_list").html(data.color_html);
				}else{
					$("#color_list").html("");
				}
			}
		});
	},
	product_assist:function(){
    	$.dialog({
    		//id设置用来区别唯一性 防止div重复
    	   	id:'productAssist',
    	   	//设置标题
    	   	title:'辅助模板设置',
    	   	//设置最大化最小化按钮(false：隐藏 true:关闭)
    	   	max: false,
    	   	min: false,
    	   	lock:true,
    	   	//设置长宽
    	   	width:440,
    	   	height:380,
    	   	//设置是否拖拽(false:禁止，true可以移动)
    	   	drag: true,
    	   	resize:false,
   		   	//设置页面加载处理
  		   	fixed:false,
    	   	content:"url:"+config.BASEPATH+"base/product/to_product_assist_template",
    	   	close: function(){
    	   		
  		   	}
        });
	}
//	braCheckShow:function(obj){//显示或隐藏杯型
//		if(obj.checked){
//			$('.bras').show();
//			$('#pd_bra').val('1');
//		}
//		else{
//			$('.bras').hide();
//			$('#pd_bra').val('0');
//			//清除已选的杯型
//			//chooseItemsAll(form1.bra_selectcolumns, form1.bra_choices);
//		}
//	},
//	showBra:function(){//显示杯型
//		$.ajax({
//			type:"get",
//			url:config.BASEPATH +"base/bra/braListByProduct",
//			data:"",
//			cache:false,
//			dataType:"json",
//			success:function(data){
//				if(data.bra_html != undefined){
//					$("#bra_list").html(data.bra_html);
//				}else{
//					$("#bra_list").html("");
//				}
//			}
//		});
//	}
};

var comboOpts = {
	value : 'Code',
	text : 'Name',
	width : 142,
	height : 300,
	listHeight : 300,
	listId : '',
	defaultSelected : 0,
	editable : false
};
var styleCombo;
var styleInfo = {};
var seasonCombo;
var seasonInfo = {};
var unitCombo;
var unitInfo = {};
var stuffCombo;
var stuffInfo = {};
var priceCombo;
var priceInfo = {};
var yearCombo;
var yearInfo={};
var isbraCombo;
var isbraInfo={};
var isChangeCombo;
var isChangeInfo = {};
var isBuyCombo;
var isBuyInfo = {};
var isVipSaleCombo;
var isVipSaleInfo = {};
var isPresentCombo;
var isPresentInfo = {};
var isGiftCombo;
var isGiftInfo = {};
var isPointCombo;
var isPointInfo = {};
var isSaleCombo;
var isSaleInfo = {};
var ComboUtil = {
	dictCombo : function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			cache:false,
			dataType:"json",
			success:function(data){
				//款式
				styleInfo = data.data.styleitems;
				styleCombo = $('#span_style').combo({
					data:styleInfo,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							styleInfo = data.data.styleitems;
							return data.data.styleitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_style").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//季节
				seasonInfo = data.data.seasonitems;
				seasonCombo = $('#span_season').combo({
					data:seasonInfo,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							seasonInfo = data.data.seasonitems;
							return data.data.seasonitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_season").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//单位
				unitInfo = data.data.unitnameitems;
				unitCombo = $('#span_unit').combo({
					data:unitInfo,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							unitInfo = data.data.unitnameitems;
							return data.data.unitnameitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_unit").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//面料
				stuffInfo = data.data.stuffitems;
				stuffCombo = $('#span_fabric').combo({
					data:stuffInfo,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							stuffInfo = data.data.stuffitems;
							return data.data.stuffitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_fabric").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//价格特性
				priceInfo = data.data.priceitems;
				priceCombo = $('#span_price').combo({
					data:priceInfo,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: 0,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							priceInfo = data.data.priceitems;
							return data.data.priceitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_price_name").val(data.dtl_name);
						}
					}
				}).getCombo(); 
			}
		 });
	},
	yearCombo : function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
					{year_Code:""+parseInt(currentYear-7)+"",year_Name:""+parseInt(currentYear-7)+""},
					{year_Code:""+parseInt(currentYear-6)+"",year_Name:""+parseInt(currentYear-6)+""},
					{year_Code:""+parseInt(currentYear-5)+"",year_Name:""+parseInt(currentYear-5)+""},
					{year_Code:""+parseInt(currentYear-4)+"",year_Name:""+parseInt(currentYear-4)+""},
					{year_Code:""+parseInt(currentYear-3)+"",year_Name:""+parseInt(currentYear-3)+""},
					{year_Code:""+parseInt(currentYear-2)+"",year_Name:""+parseInt(currentYear-2)+""},
					{year_Code:""+parseInt(currentYear-1)+"",year_Name:""+parseInt(currentYear-1)+""},
					{year_Code:""+parseInt(currentYear)+"",year_Name:""+parseInt(currentYear)+""},
					{year_Code:""+parseInt(currentYear+1)+"",year_Name:""+parseInt(currentYear+1)+""},
					{year_Code:""+parseInt(currentYear+2)+"",year_Name:""+parseInt(currentYear+2)+""},
					{year_Code:""+parseInt(currentYear+3)+"",year_Name:""+parseInt(currentYear+3)+""},
					{year_Code:""+parseInt(currentYear+4)+"",year_Name:""+parseInt(currentYear+4)+""},
					{year_Code:""+parseInt(currentYear+5)+"",year_Name:""+parseInt(currentYear+5)+""},
					{year_Code:""+parseInt(currentYear+6)+"",year_Name:""+parseInt(currentYear+6)+""},
					{year_Code:""+parseInt(currentYear+7)+"",year_Name:""+parseInt(currentYear+7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'year_Code',
			text: 'year_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 7,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.year_Code);
				}
			}
		}).getCombo();
	},
	isbraCombo : function(){
		isbraInfo = {
				data:{items:[
					{isbra_Code:'0',isbra_Name:'否'},
					{isbra_Code:'1',isbra_Name:'是'},
					]}
		}
		isbraCombo = $('#span_bra').combo({
			data:isbraInfo.data.items,
			value: 'isbra_Code',
			text: 'isbra_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_bra").val(data.isbra_Code);
					if(data.isbra_Code == '0'){
						$("#pdb_br_codes").val('');
						$("#pdb_br_name").val('');
					}
				}
			}
		}).getCombo();
	},
	isChangeCombo : function(){
		isChangeInfo = {
				data:{items:[
					{isChange_Code:'0',isChange_Name:'否'},
					{isChange_Code:'1',isChange_Name:'是'},
					]}
		}
		isChangeCombo = $('#span_change').combo({
			data:isChangeInfo.data.items,
			value: 'isChange_Code',
			text: 'isChange_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_change").val(data.isChange_Code);
				}
			}
		}).getCombo();
	},
	isBuyCombo : function(){
		isBuyInfo = {
				data:{items:[
					{isBuy_Code:'0',isBuy_Name:'否'},
					{isBuy_Code:'1',isBuy_Name:'是'},
					]}
		}
		isBuyCombo = $('#span_buy').combo({
			data:isBuyInfo.data.items,
			value: 'isBuy_Code',
			text: 'isBuy_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_buy").val(data.isBuy_Code);
				}
			}
		}).getCombo();
	},
	isVipSaleCombo : function(){
		isVipSaleInfo = {
				data:{items:[
					{isVipSale_Code:'0',isVipSale_Name:'否'},
					{isVipSale_Code:'1',isVipSale_Name:'是'},
					]}
		}
		isVipSaleCombo = $('#span_vip_sale').combo({
			data:isVipSaleInfo.data.items,
			value: 'isVipSale_Code',
			text: 'isVipSale_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_vip_sale").val(data.isVipSale_Code);
				}
			}
		}).getCombo();
	},
	isPresentCombo : function(){
		isPresentInfo = {
				data:{items:[
					{isPresent_Code:'0',isPresent_Name:'否'},
					{isPresent_Code:'1',isPresent_Name:'是'},
					]}
		}
		isPresentCombo = $('#span_present').combo({
			data:isPresentInfo.data.items,
			value: 'isPresent_Code',
			text: 'isPresent_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_present").val(data.isPresent_Code);
				}
			}
		}).getCombo();
	},
	isGiftCombo : function(){
		isGiftInfo = {
				data:{items:[
					{isGift_Code:'0',isGift_Name:'否'},
					{isGift_Code:'1',isGift_Name:'是'},
					]}
		}
		isGiftCombo = $('#span_gift').combo({
			data:isGiftInfo.data.items,
			value: 'isGift_Code',
			text: 'isGift_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_gift").val(data.isGift_Code);
				}
			}
		}).getCombo();
	},
	isPointCombo : function(){
		isPointInfo = {
				data:{items:[
					{isPoint_Code:'0',isPoint_Name:'否'},
					{isPoint_Code:'1',isPoint_Name:'是'},
					]}
		}
		isPointCombo = $('#span_point').combo({
			data:isPointInfo.data.items,
			value: 'isPoint_Code',
			text: 'isPoint_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_point").val(data.isPoint_Code);
				}
			}
		}).getCombo();
	},
	isSaleCombo : function(){
		isSaleInfo = {
				data:{items:[
					{isSale_Code:'0',isSale_Name:'否'},
					{isSale_Code:'1',isSale_Name:'是'},
					]}
		}
		isSaleCombo = $('#span_sale').combo({
			data:isSaleInfo.data.items,
			value: 'isSale_Code',
			text: 'isSale_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_sale").val(data.isSale_Code);
				}
			}
		}).getCombo();
	}
//	sizeGroupCombo : function(){
//		var sizeGroupInfo = {};
//		sizeGroupCombo = $('#span_size_group').combo({
//			data:config.BASEPATH +"base/size/listGroupByProduct",
//			value: 'szg_code',
//			text: 'szg_name',
//			width :80,
//			height: 30,
//			listId:'',
//			defaultSelected: 0,
//			editable: true,
//			ajaxOptions:{
//				formatData: function(data){
//					sizeGroupInfo = data.data.items;
//					return data.data.items;
//				}
//			},
//			callback:{
//				onChange: function(data){
//					$("#pd_szg_code").val(data.szg_code);
//					handle.showSize(data.szg_code);
//				}
//			}
//		}).getCombo(); 
//	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initDate();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		ComboUtil.dictCombo();
		ComboUtil.yearCombo();
		//ComboUtil.sizeGroupCombo();
		ComboUtil.isbraCombo();
		ComboUtil.isChangeCombo();
		ComboUtil.isBuyCombo();
		ComboUtil.isVipSaleCombo();
		ComboUtil.isPresentCombo();
		ComboUtil.isGiftCombo();
		ComboUtil.isPointCombo();
		ComboUtil.isSaleCombo();
		handle.showColor();
		//handle.showBra();
	},
	initDate:function(){
		var mydate = new Date();
		var currentdate = mydate.getFullYear() + "-" + (mydate.getMonth()+1) + "-" + mydate.getDate(); 
		document.getElementById('pd_date').value = currentdate;
		document.getElementById('pd_returndate').value = currentdate;
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			var pd_code = $('#pd_code').val();
			if (pd_code==''){
				handle.save();
	    	}else{
	    		handle.update();
	    		selContent('1');
	    	}
			
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$("#btn_add_bar").click(function(){//条码
			var id = $(this).parent().data('id');
			handle.saveByBar();
		});
		
		$("#btn_add_assist").click(function(){//辅助属性模板
			handle.product_assist();
		});
	}
};
THISPAGE.init();