var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api;
var _limitMenu = system.MENULIMIT.BASE26;
var _menuParam = config.OPTIONLIMIT;
var handle = {
	save:function(){
	    var bs_paper_width = document.getElementById("bs_paper_width");
	    if(bs_paper_width.value == ""){
	    	Public.tips({type: 2, content : '请输入纸张宽度！'});
	    	bs_paper_width.focus();
	    	return;
	    }
	    if(isNaN(bs_paper_width.value)){
	    	Public.tips({type: 2, content : '纸张宽度只能为数字！'});
	    	bs_paper_width.focus();
	    	return;
		}
	    var bs_paper_height = document.getElementById("bs_paper_height");
	    if(bs_paper_height.value == ""){
	    	Public.tips({type: 2, content : '请输入纸张高度！'});
	    	bs_paper_height.focus();
	    	return;
	    }
	    if(isNaN(bs_paper_height.value)){
	    	Public.tips({type: 2, content : '纸张高度只能为数字！'});
		  	bs_paper_height.focus();
	    	return;
		}
	    var bs_top_backgauge = document.getElementById("bs_top_backgauge");
	    if(bs_top_backgauge.value == ""){
	    	Public.tips({type: 2, content : '请输入整页上边距！'});
	    	bs_top_backgauge.focus();
	    	return;
	    }
	    if(isNaN(bs_top_backgauge.value)){
	    	Public.tips({type: 2, content : '整页上边距只能为数字！'});
		  	bs_top_backgauge.focus();
	    	return;
		}
	    var bs_left_backgauge = document.getElementById("bs_left_backgauge");
	    if(bs_left_backgauge.value == ""){
	    	Public.tips({type: 2, content : '请输入整页左边距！'});
	    	bs_left_backgauge.focus();
	    	return;
	    }
	    if(isNaN(bs_left_backgauge.value)){
	    	Public.tips({type: 2, content : '整页左边距只能为数字！'});
		  	bs_left_backgauge.focus();
	    	return;
		}
	    var bs_hight_spacing = document.getElementById("bs_hight_spacing");
	    if(bs_hight_spacing.value == ""){
	    	Public.tips({type: 2, content : '请输入条形码高间距！'});
		  	bs_hight_spacing.focus();
	    	return;
		}
	    var bs_width_spacing = document.getElementById("bs_width_spacing");
	    if(bs_width_spacing.value == ""){
	    	Public.tips({type: 2, content : '请输入条形码宽间距！'});
		  	bs_width_spacing.focus();
	    	return;
		}
	    var bs_width = document.getElementById("bs_width");
		if(bs_width.value == ""){
			Public.tips({type: 2, content : '请输入条形码宽度！'});
		  	bs_width.focus();
	    	return;
		}
		if(isNaN(bs_width.value)){
			Public.tips({type: 2, content : '条形码宽度只能为数字！'});
	  		bs_width.focus();
	  		return;
		}
		var bs_height = document.getElementById("bs_height");
		if(bs_height.value == ""){
			Public.tips({type: 2, content : '请输入条形码高度！'});
		  	bs_height.focus();
	    	return;
		}
		if(isNaN(bs_height.value)){
			Public.tips({type: 2, content : '条形码高度只能为数字！'});
		  	bs_height.focus();
	    	return;
		}
		var bs_p_space = document.getElementById("bs_p_space");
		if(bs_p_space.value == ""){
			Public.tips({type: 2, content : '请输入商品信息间距！'});
		  	bs_p_space.focus();
	      	return;
		}
		if(isNaN(bs_p_space.value)){
		  	Public.tips({type: 2, content : '商品信息间距只能为数字！'});
		  	bs_p_space.focus();
	      	return;
		 }
		 var bs_ps_space = document.getElementById("bs_ps_space");
		 if(bs_ps_space.value == ""){
			Public.tips({type: 2, content : '请输入信息和条码间距！'});
		  	bs_ps_space.focus();
	      	return;
		 }
		 if(isNaN(bs_ps_space.value)){
			Public.tips({type: 2, content : '信息和条码间距只能为数字！'});
		  	bs_ps_space.focus();
	      	return;
		 }
		 var bs_plan_name = document.getElementById("bs_plan_name").value;
		 if(bs_plan_name == ""){
			 Public.tips({type: 2, content : '请输入方案名称！'});
			 document.getElementById("bs_plan_name").focus();
			 return;
		 }
		 $("#btn-save").attr("disabled",true);
		 $.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/save_barcode_printset",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	}
}

var ifshowInfo = {
	data:{items:[
			{ifshow_Code:"0",ifshow_Name:"不显示"},
			{ifshow_Code:"1",ifshow_Name:"显示"}
		]}
}
var rowInfo = {
	data:{items:[
			{row_Code:"0",row_Name:"请选择"},
			{row_Code:"1",row_Name:"第一行"},
			{row_Code:"2",row_Name:"第二行"},
			{row_Code:"3",row_Name:"第三行"},
			{row_Code:"4",row_Name:"第四行"},
			{row_Code:"5",row_Name:"第五行"},
			{row_Code:"6",row_Name:"第六行"},
			{row_Code:"7",row_Name:"第七行"},
			{row_Code:"8",row_Name:"第八行"},
			{row_Code:"9",row_Name:"第九行"},
			{row_Code:"10",row_Name:"第十行"},
			{row_Code:"11",row_Name:"第十一行"}
		]}
}
	
var ComboUtil = {
	lineCombo : function(){
		var lineInfo = {
			data:{items:[
					{line_Code:"1",line_Name:"1"},
					{line_Code:"2",line_Name:"2"},
					{line_Code:"3",line_Name:"3"}
				]}
		}
		var selectLine = $("#bs_line").val();
		var index = 0,_year="";
		 for(var key in lineInfo.data.items){
			if(lineInfo.data.items[key].line_Name ==  selectLine){
				index = key;
				_year = lineInfo.data.items[key].line_Name;
				break;
			}
		 }
		lineCombo = $('#span_line').combo({
			data:lineInfo.data.items,
			value: 'line_Code',
			text: 'line_Name',
			width : 100,
			height : 50,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_line").val(data.line_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	fontnameCombo : function(){
		var fontnameInfo = {
			data:{items:[
					{font_name_Code:"1",font_name_Name:"宋体"},
					{font_name_Code:"2",font_name_Name:"仿宋"},
					{font_name_Code:"3",font_name_Name:"微软雅黑"},
					{font_name_Code:"4",font_name_Name:"新宋体"},
					{font_name_Code:"5",font_name_Name:"黑体"},
					{font_name_Code:"6",font_name_Name:"楷体"}
				]}
		}
		var selectFontName = $("#bs_font_name").val();
		var index = 0,_year="";
		 for(var key in fontnameInfo.data.items){
			if(fontnameInfo.data.items[key].font_name_Name ==  selectFontName){
				index = key;
				_year = fontnameInfo.data.items[key].font_name_Name;
				break;
			}
		 }
		 fontnameCombo = $('#span_font_name').combo({
			data:fontnameInfo.data.items,
			value: 'font_name_Code',
			text: 'font_name_Name',
			width : 100,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_font_name").val(data.font_name_Name);
					changePreView();
				}
			}
		}).getCombo();
	},
	fontsizeCombo : function(){
		var fontsizeInfo = {
			data:{items:[
					{font_size_Code:"4",font_size_Name:"4"},
					{font_size_Code:"5",font_size_Name:"5"},
					{font_size_Code:"6",font_size_Name:"6"},
					{font_size_Code:"7",font_size_Name:"7"},
					{font_size_Code:"8",font_size_Name:"8"},
					{font_size_Code:"9",font_size_Name:"9"},
					{font_size_Code:"10",font_size_Name:"10"},
					{font_size_Code:"11",font_size_Name:"11"},
					{font_size_Code:"12",font_size_Name:"12"},
					{font_size_Code:"13",font_size_Name:"13"},
					{font_size_Code:"14",font_size_Name:"14"}
				]}
		}
		var selectFontSize = $("#bs_font_size").val();
		var index = 0,_year="";
		 for(var key in fontsizeInfo.data.items){
			if(fontsizeInfo.data.items[key].font_size_Name ==  selectFontSize){
				index = key;
				_year = fontsizeInfo.data.items[key].font_size_Name;
				break;
			}
		 }
		 fontsizeCombo = $('#span_font_size').combo({
			data:fontsizeInfo.data.items,
			value: 'font_size_Code',
			text: 'font_size_Name',
			width : 100,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_font_size").val(data.font_size_Name);
					changePreView();
				}
			}
		}).getCombo();
	},
	boldCombo : function(){
		var boldInfo = {
			data:{items:[
					{bold_Code:"1",bold_Name:"是"},
					{bold_Code:"0",bold_Name:"否"}
				]}
		}
		var selectBold = $("#bs_bold").val();
		var index = 0,_year="";
		 for(var key in boldInfo.data.items){
			if(boldInfo.data.items[key].bold_Code ==  selectBold){
				index = key;
				_year = boldInfo.data.items[key].bold_Code;
				break;
			}
		 }
		 boldCombo = $('#span_bold').combo({
			data:boldInfo.data.items,
			value: 'bold_Code',
			text: 'bold_Name',
			width : 100,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_bold").val(data.bold_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	languageCombo : function(){
		var languageInfo = {
			data:{items:[
					{language_Code:"1",language_Name:"是"},
					{language_Code:"0",language_Name:"否"}
				]}
		}
		var selectLanguage = $("#bs_language").val();
		var index = 0,_year="";
		 for(var key in languageInfo.data.items){
			if(languageInfo.data.items[key].language_Code ==  selectLanguage){
				index = key;
				_year = languageInfo.data.items[key].language_Code;
				break;
			}
		 }
		 languageCombo = $('#span_language').combo({
			data:languageInfo.data.items,
			value: 'language_Code',
			text: 'language_Name',
			width : 100,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_language").val(data.language_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	pdnoIfshowCombo : function(){
		var selectPdnoIfshow = $("#bs_pdno_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectPdnoIfshow){
				index = key;
				break;
			}
		 }
		 pdnoIfshowCombo = $('#span_pdno_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_pdno_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	pdnoRowCombo : function(){
		var selectPdnoRow = $("#bs_pdno_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectPdnoRow){
				index = key;
				break;
			}
		 }
		 pdnoRowCombo = $('#span_pdno_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_pdno_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	pdnameIfshowCombo : function(){
		var selectPdnameIfshow = $("#bs_pdname_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectPdnameIfshow){
				index = key;
				break;
			}
		 }
		 pdnameIfshowCombo = $('#span_pdname_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_pdname_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	pdnameRowCombo : function(){
		var selectPdnameRow = $("#bs_pdname_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectPdnameRow){
				index = key;
				break;
			}
		 }
		 pdnameRowCombo = $('#span_pdname_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_pdname_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	crIfshowCombo : function(){
		var selectCrIfshow = $("#bs_cr_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectCrIfshow){
				index = key;
				break;
			}
		 }
		 crIfshowCombo = $('#span_cr_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_cr_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	crRowCombo : function(){
		var selectCrRow = $("#bs_cr_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectCrRow){
				index = key;
				break;
			}
		 }
		 crRowCombo = $('#span_cr_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_cr_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	bsIfshowCombo : function(){
		var selectBsIfshow = $("#bs_bs_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectBsIfshow){
				index = key;
				break;
			}
		 }
		 bsIfshowCombo = $('#span_bs_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_bs_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	bsRowCombo : function(){
		var selectBsRow = $("#bs_bs_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectBsRow){
				index = key;
				break;
			}
		 }
		 bsRowCombo = $('#span_bs_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_bs_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	szIfshowCombo : function(){
		var selectSzIfshow = $("#bs_sz_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectSzIfshow){
				index = key;
				break;
			}
		 }
		 szIfshowCombo = $('#span_sz_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_sz_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	szRowCombo : function(){
		var selectSzRow = $("#bs_sz_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectSzRow){
				index = key;
				break;
			}
		 }
		 szRowCombo = $('#span_sz_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_sz_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	bdIfshowCombo : function(){
		var selectBdIfshow = $("#bs_bd_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectBdIfshow){
				index = key;
				break;
			}
		 }
		 bdIfshowCombo = $('#span_bd_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_bd_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	bdRowCombo : function(){
		var selectBdRow = $("#bs_bd_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectBdRow){
				index = key;
				break;
			}
		 }
		 bdRowCombo = $('#span_bd_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_bd_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	priceIfshowCombo : function(){
		var selectPriceIfshow = $("#bs_price_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectPriceIfshow){
				index = key;
				break;
			}
		 }
		 priceIfshowCombo = $('#span_price_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_price_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	priceRowCombo : function(){
		var selectPriceRow = $("#bs_price_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectPriceRow){
				index = key;
				break;
			}
		 }
		 priceRowCombo = $('#span_price_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_price_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	signpriceIfshowCombo : function(){
		var selectSignpriceIfshow = $("#bs_signprice_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectSignpriceIfshow){
				index = key;
				break;
			}
		 }
		 signpriceIfshowCombo = $('#span_signprice_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_signprice_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	signpriceRowCombo : function(){
		var selectSignpriceRow = $("#bs_signprice_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectSignpriceRow){
				index = key;
				break;
			}
		 }
		 signpriceRowCombo = $('#span_signprice_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_signprice_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	vippriceIfshowCombo : function(){
		var selectVippriceIfshow = $("#bs_vipprice_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectVippriceIfshow){
				index = key;
				break;
			}
		 }
		 vippriceIfshowCombo = $('#span_vipprice_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_vipprice_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	vippriceRowCombo : function(){
		var selectVippriceRow = $("#bs_vipprice_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectVippriceRow){
				index = key;
				break;
			}
		 }
		 vippriceRowCombo = $('#span_vipprice_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_vipprice_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	fillingIfshowCombo : function(){
		var selectFillingIfshow = $("#bs_filling_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectFillingIfshow){
				index = key;
				break;
			}
		 }
		 fillingIfshowCombo = $('#span_filling_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_filling_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	fillingRowCombo : function(){
		var selectFillingRow = $("#bs_filling_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectFillingRow){
				index = key;
				break;
			}
		 }
		 fillingRowCombo = $('#span_filling_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_filling_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	classIfshowCombo : function(){
		var selectClassIfshow = $("#bs_class_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectClassIfshow){
				index = key;
				break;
			}
		 }
		 classIfshowCombo = $('#span_class_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_class_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	classRowCombo : function(){
		var selectClassRow = $("#bs_class_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectClassRow){
				index = key;
				break;
			}
		 }
		 classRowCombo = $('#span_class_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_class_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	safestandardIfshowCombo : function(){
		var selectSafestandardIfshow = $("#bs_safestandard_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectSafestandardIfshow){
				index = key;
				break;
			}
		 }
		 safestandardIfshowCombo = $('#span_safestandard_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_safestandard_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	safestandardRowCombo : function(){
		var selectSafestandardRow = $("#bs_safestandard_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectSafestandardRow){
				index = key;
				break;
			}
		 }
		 safestandardRowCombo = $('#span_safestandard_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_safestandard_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	typeIfshowCombo : function(){
		var selectTypeIfshow = $("#bs_type_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectTypeIfshow){
				index = key;
				break;
			}
		 }
		 typeIfshowCombo = $('#span_type_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_type_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	typeRowCombo : function(){
		var selectTypeRow = $("#bs_type_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectTypeRow){
				index = key;
				break;
			}
		 }
		 typeRowCombo = $('#span_type_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_type_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	executionstandardIfshowCombo : function(){
		var selectExecutionstandardIfshow = $("#bs_executionstandard_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  selectExecutionstandardIfshow){
				index = key;
				break;
			}
		 }
		 executionstandardIfshowCombo = $('#span_executionstandard_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_executionstandard_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	executionstandardRowCombo : function(){
		var selectExecutionstandardRow = $("#bs_executionstandard_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  selectExecutionstandardRow){
				index = key;
				break;
			}
		 }
		 executionstandardRowCombo = $('#span_executionstandard_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_executionstandard_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	fabricIfshowCombo : function(){
		var fabricIfshow = $("#bs_fabric_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  fabricIfshow){
				index = key;
				break;
			}
		 }
		 fabricIfshowCombo = $('#span_fabric_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_fabric_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	fabricRowCombo : function(){
		var fabricdRow = $("#bs_fabric_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  fabricdRow){
				index = key;
				break;
			}
		 }
		 fabricRowCombo = $('#span_fabric_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_fabric_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	infabricIfshowCombo : function(){
		var infabricIfshow = $("#bs_infabric_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  infabricIfshow){
				index = key;
				break;
			}
		 }
		 infabricIfshowCombo = $('#span_infabric_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_infabric_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	infabricRowCombo : function(){
		var infabricdRow = $("#bs_infabric_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  infabricdRow){
				index = key;
				break;
			}
		 }
		 infabricRowCombo = $('#span_infabric_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_infabric_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	placeIfshowCombo : function(){
		var placeIfshow = $("#bs_place_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  placeIfshow){
				index = key;
				break;
			}
		 }
		 placeIfshowCombo = $('#span_place_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_place_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	placeRowCombo : function(){
		var placeRow = $("#bs_place_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  placeRow){
				index = key;
				break;
			}
		 }
		 placeRowCombo = $('#span_place_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_place_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	washexplainIfshowCombo : function(){
		var washexplainIfshow = $("#bs_washexplain_ifshow").val();
		var index = 0;
		 for(var key in ifshowInfo.data.items){
			if(ifshowInfo.data.items[key].ifshow_Code ==  washexplainIfshow){
				index = key;
				break;
			}
		 }
		 washexplainIfshowCombo = $('#span_washexplain_ifshow').combo({
			data:ifshowInfo.data.items,
			value: 'ifshow_Code',
			text: 'ifshow_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_washexplain_ifshow").val(data.ifshow_Code);
					changePreView();
				}
			}
		}).getCombo();
	},
	washexplainRowCombo : function(){
		var washexplainRow = $("#bs_washexplain_row").val();
		var index = 0;
		 for(var key in rowInfo.data.items){
			if(rowInfo.data.items[key].row_Code ==  washexplainRow){
				index = key;
				break;
			}
		 }
		 washexplainRowCombo = $('#span_washexplain_row').combo({
			data:rowInfo.data.items,
			value: 'row_Code',
			text: 'row_Name',
			width : 80,
			height : 60,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#bs_washexplain_row").val(data.row_Code);
					changePreView();
				}
			}
		}).getCombo();
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		ComboUtil.lineCombo();
		ComboUtil.fontnameCombo();
		ComboUtil.fontsizeCombo();
		ComboUtil.boldCombo();
		ComboUtil.languageCombo();
		ComboUtil.pdnoIfshowCombo();
		ComboUtil.pdnoRowCombo();
		ComboUtil.pdnameIfshowCombo();
		ComboUtil.pdnameRowCombo();
		ComboUtil.crIfshowCombo();
		ComboUtil.crRowCombo();
		ComboUtil.bsIfshowCombo();
		ComboUtil.bsRowCombo();
		ComboUtil.szIfshowCombo();
		ComboUtil.szRowCombo();
		ComboUtil.bdIfshowCombo();
		ComboUtil.bdRowCombo();
		ComboUtil.priceIfshowCombo();
		ComboUtil.priceRowCombo();
		ComboUtil.signpriceIfshowCombo();
		ComboUtil.signpriceRowCombo();
		ComboUtil.vippriceIfshowCombo();
		ComboUtil.vippriceRowCombo();
		ComboUtil.fillingIfshowCombo();
		ComboUtil.fillingRowCombo();
		
		ComboUtil.typeIfshowCombo();
		ComboUtil.typeRowCombo();
		ComboUtil.classIfshowCombo();
		ComboUtil.classRowCombo();
		ComboUtil.safestandardIfshowCombo();
		ComboUtil.safestandardRowCombo();
		ComboUtil.executionstandardIfshowCombo();
		ComboUtil.executionstandardRowCombo();
		ComboUtil.fabricIfshowCombo();
		ComboUtil.fabricRowCombo();
		ComboUtil.infabricIfshowCombo();
		ComboUtil.infabricRowCombo();
		ComboUtil.placeIfshowCombo();
		ComboUtil.placeRowCombo();
		ComboUtil.washexplainIfshowCombo();
		ComboUtil.washexplainRowCombo();
		/*$_chk_BSU_Show_FieName= $("#chk_BSU_Show_FieName").cssCheckbox();
    	$_chk_BSU_SubCode_ShowUp= $("#chk_BSU_SubCode_ShowUp").cssCheckbox();
    	$_chk_BSU_Price_ShowDown= $("#chk_BSU_Price_ShowDown").cssCheckbox();*/
    	changePreView();
	},
	initGrid:function(){
	},
	buildParams : function(){
	},
	reset:function(){
	},
	reloadData:function(data){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		$("#btn_close").click(function(){
			api.close();
		});
		//新增
		$('#btn-save').on('click', function(e){
			handle.save();
		});
	}
}
THISPAGE.init();
function changePreView(){
	var previewObj = document.getElementById("preview");
	var paperDivObj = document.getElementById("paperDiv");
	var preview2Obj = document.getElementById("preview2");
	var paperDiv1Obj = document.getElementById("paperDiv1");
	var paperDiv2Obj = document.getElementById("paperDiv2");
	
	var preview3Obj = document.getElementById("preview3");
	var paperDiv3Obj = document.getElementById("paperDiv3");
	var paperDiv4Obj = document.getElementById("paperDiv4");
	var paperDiv5Obj = document.getElementById("paperDiv5");
	
	var bs_line = document.getElementById("bs_line").value;
    var bs_paper_width = document.getElementById("bs_paper_width").value;
    var bs_paper_height = document.getElementById("bs_paper_height").value;
    var bs_top_backgauge = document.getElementById("bs_top_backgauge").value;
    var bs_left_backgauge = document.getElementById("bs_left_backgauge").value;
    var bs_hight_spacing = document.getElementById("bs_hight_spacing").value;
    var bs_width_spacing = document.getElementById("bs_width_spacing").value;
    var bs_language= document.getElementById("bs_language").value;
    
    var bs_width = document.getElementById("bs_width").value;
    var bs_height = document.getElementById("bs_height").value;
    var bs_ps_space = document.getElementById("bs_ps_space").value;//信息与条码距离
    var bs_p_space = document.getElementById("bs_p_space").value;//商品信息间距
    
    var bs_font_size = document.getElementById("bs_font_size").value;//获取字体大小
	var bs_font_name = document.getElementById("bs_font_name").value;//获取字体
	var bs_bold = document.getElementById("bs_bold").value;//是否加粗
	 
    var oneRowInnerHTML = "";
    var twoRowInnerHTML = "";
    var threeRowInnerHTML = "";
    var fourRowInnerHTML = "";
    var fiveRowInnerHTML = "";
    var sixRowInnerHTML = "";
    var sevenRowInnerHTML = "";
    var eightRowInnerHTML = "";
    var nineRowInnerHTML = "";
    var tenRowInnerHTML = "";
    var elevenRowInnerHTML  = "";
    
//    var BSU_Show_FieName = document.getElementById("BSU_Show_FieName");//是否显示字段名
    var showFieName = "";
//    if(BSU_Show_FieName.checked){
    	showFieName = "1";
//    }else{
//    	showFieName = "0";
//    }
//    var BSU_SubCode_ShowUp = document.getElementById("BSU_SubCode_ShowUp");//上方显示条形码
    var subCode_ShowUp = "";
   /* if(BSU_SubCode_ShowUp.checked){
    	subCode_ShowUp = "1";
    }else{*/
    	subCode_ShowUp = "0";
//    }
//    var BSU_Price_ShowDown = document.getElementById("BSU_Price_ShowDown");//下面显示价格
    var price_ShowDown = "";
//    if(BSU_Price_ShowDown.checked){
//    	price_ShowDown = "1";
//    }else{
    	price_ShowDown = "0";
//    }
    
    
    var pdnoText = " 1001";
    var pdNameText = " 长袖衬衫";
	var crText = " 红色";
	var bsText = " A";
	var szText = " XL";
	var bdText = " 智慧数";
	var retailText = " ￥89";
	var priceVipText = " ￥80";
	var priceSignText = " ￥92";
	var typeText = "上衣";
	var classText = " 一等品";
	var safeStandardText = " AQBZ-2017";
	var filling = " 78%棉21%锦纶";
	var executionStandard=' A类标准';
	var fabricText = " 纯棉";
	var infabricText = " 化纤";
	var placeText = " 福建";
	var washexplainText = " 不可干洗";
	if(showFieName == "1"){//是否显示字段名称
		if(bs_language == "1"){//英语
			pdnoText = " No:1001";
			pdNameText = " Name:长袖衬衫";
			crText = " Col:红色";
			bsText = " Cup:A";
			szText = " Size:XL";
			bdText = " Brand:智慧数";
			retailText = " RMB:￥89";
			priceSignText = " Sign RMB:￥92";
			priceVipText = " Vip RMB:￥80";
			typeText = " Type:上衣";
			classText = " Grade:一等品";
			safeStandardText = " Standard:AQBZ-2017";
			filling = " Ingredient:78%棉21%锦纶";
			executionStandard=' Executive Standard:A类标准';	
			fabricText = " Fabric:纯棉";
			infabricText = " Infabric:化纤";
			placeText = " Place:福建";
			washexplainText= " WashExplain:不可干洗";
	    }else{
	    	pdnoText = " 货号：1001";
	    	pdNameText = " 品名：长袖衬衫";
			crText = " 颜色：红色";
			bsText = " 杯型：A";
			szText = " 尺码：XL";
			bdText = " 品牌：智慧数";
			retailText = " 零售价：￥89";
			priceSignText = " 标牌价：￥92";
			priceVipText = " 会员价：￥80";
			typeText = " 类别：上衣";
			classText = " 等级：一等品";
			safeStandardText = "安全标准：AQBZ-2017";
			filling = " 成份：78%棉21%锦纶";
			executionStandard=' 执行标准：A类标准';
			fabricText = " 面料：纯棉";
			infabricText = " 里料：化纤";
			placeText = " 产地：福建";
			washexplainText= " 洗涤说明：不可干洗";
	    }
		
	}
	var bs_pdno_ifshow = document.getElementById("bs_pdno_ifshow").value;
	var bs_pdno_row = document.getElementById("bs_pdno_row").value;;//货号显示在第几排
	  if(bs_pdno_ifshow != "0"){
		  if(bs_pdno_row == "1"){
			  oneRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "2"){
			  twoRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "3"){
			  threeRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "4"){
			  fourRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "5"){
			  fiveRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "6"){
			  sixRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "7"){
			  sevenRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "8"){
			  eightRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "9"){
			  nineRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "10"){
			  tenRowInnerHTML += pdnoText;
		  }else if(bs_pdno_row == "11"){
			  elevenRowInnerHTML += pdnoText;
		  }
	  }
	  
	 var bs_pdname_ifshow = document.getElementById("bs_pdname_ifshow").value;
	 var bs_pdname_row = document.getElementById("bs_pdname_row").value;//品名显示在第几排
	  if(bs_pdname_ifshow != "0"){
		  if(bs_pdname_row == "1"){
			  oneRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "2"){
			  twoRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "3"){
			  threeRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "4"){
			  fourRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "5"){
			  fiveRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "6"){
			  sixRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "7"){
			  sevenRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "8"){
			  eightRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "9"){
			  nineRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "10"){
			  tenRowInnerHTML += pdNameText;
		  }else if(bs_pdname_row == "11"){
			  elevenRowInnerHTML += pdNameText;
		  }
	  }
	  
	var bs_cr_ifshow = document.getElementById("bs_cr_ifshow").value;
	var bs_cr_row = document.getElementById("bs_cr_row").value;//颜色显示在第几排
	  if(bs_cr_ifshow != "0"){
		  if(bs_cr_row == "1"){
			  oneRowInnerHTML += crText;
		  }else if(bs_cr_row == "2"){
			  twoRowInnerHTML += crText;
		  }else if(bs_cr_row == "3"){
			  threeRowInnerHTML += crText;
		  }else if(bs_cr_row == "4"){
			  fourRowInnerHTML += crText;
		  }else if(bs_cr_row == "5"){
			  fiveRowInnerHTML += crText;
		  }else if(bs_cr_row == "6"){
			  sixRowInnerHTML += crText;
		  }else if(bs_cr_row == "7"){
			  sevenRowInnerHTML += crText;
		  }else if(bs_cr_row == "8"){
			  eightRowInnerHTML += crText;
		  }else if(bs_cr_row == "9"){
			  nineRowInnerHTML += crText;
		  }else if(bs_cr_row == "10"){
			  tenRowInnerHTML += crText;
		  }else if(bs_cr_row == "11"){
			  elevenRowInnerHTML += crText;
		  }
	  }
	  
	  var bs_bs_ifshow = document.getElementById("bs_bs_ifshow").value;
		var bs_bs_row = document.getElementById("bs_bs_row").value;//杯型显示在第几排
	  if(bs_bs_ifshow != "0"){
		  if(bs_bs_row == "1"){
			  oneRowInnerHTML += bsText;
		  }else if(bs_bs_row == "2"){
			  twoRowInnerHTML += bsText;
		  }else if(bs_bs_row == "3"){
			  threeRowInnerHTML += bsText;
		  }else if(bs_bs_row == "4"){
			  fourRowInnerHTML += bsText;
		  }else if(bs_bs_row == "5"){
			  fiveRowInnerHTML += bsText;
		  }else if(bs_bs_row == "6"){
			  sixRowInnerHTML += bsText;
		  }else if(bs_bs_row == "7"){
			  sevenRowInnerHTML += bsText;
		  }else if(bs_bs_row == "8"){
			  eightRowInnerHTML += bsText;
		  }else if(bs_bs_row == "9"){
			  nineRowInnerHTML += bsText;
		  }else if(bs_bs_row == "10"){
			  tenRowInnerHTML += bsText;
		  }else if(bs_bs_row == "11"){
			  elevenRowInnerHTML += bsText;
		  }
	  }
	  
	  var bs_sz_ifshow = document.getElementById("bs_sz_ifshow").value;
		var bs_sz_row = document.getElementById("bs_sz_row").value;//尺码显示在第几排
	  if(bs_sz_ifshow != "0"){
		  if(bs_sz_row == "1"){
			  oneRowInnerHTML += szText;
		  }else if(bs_sz_row == "2"){
			  twoRowInnerHTML += szText;
		  }else if(bs_sz_row == "3"){
			  threeRowInnerHTML += szText;
		  }else if(bs_sz_row == "4"){
			  fourRowInnerHTML += szText;
		  }else if(bs_sz_row == "5"){
			  fiveRowInnerHTML += szText;
		  }else if(bs_sz_row == "6"){
			  sixRowInnerHTML += szText;
		  }else if(bs_sz_row == "7"){
			  sevenRowInnerHTML += szText;
		  }else if(bs_sz_row == "8"){
			  eightRowInnerHTML += szText;
		  }else if(bs_sz_row == "9"){
			  nineRowInnerHTML += szText;
		  }else if(bs_sz_row == "10"){
			  tenRowInnerHTML += szText;
		  }else if(bs_sz_row == "11"){
			  elevenRowInnerHTML += szText;
		  }
	  }
	  
	  var bs_bd_ifshow = document.getElementById("bs_bd_ifshow").value;
		var bs_bd_row = document.getElementById("bs_bd_row").value;//品牌显示在第几排
	  if(bs_bd_ifshow != "0"){
		  if(bs_bd_row == "1"){
			  oneRowInnerHTML += bdText;
		  }else if(bs_bd_row == "2"){
			  twoRowInnerHTML += bdText;
		  }else if(bs_bd_row == "3"){
			  threeRowInnerHTML += bdText;
		  }else if(bs_bd_row == "4"){
			  fourRowInnerHTML += bdText;
		  }else if(bs_bd_row == "5"){
			  fiveRowInnerHTML += bdText;
		  }else if(bs_bd_row == "6"){
			  sixRowInnerHTML += bdText;
		  }else if(bs_bd_row == "7"){
			  sevenRowInnerHTML += bdText;
		  }else if(bs_bd_row == "8"){
			  eightRowInnerHTML += bdText;
		  }else if(bs_bd_row == "9"){
			  nineRowInnerHTML += bdText;
		  }else if(bs_bd_row == "10"){
			  tenRowInnerHTML += bdText;
		  }else if(bs_bd_row == "11"){
			  elevenRowInnerHTML += bdText;
		  }
	  }
	  
	  var bs_price_ifshow = document.getElementById("bs_price_ifshow").value;
		var bs_price_row = document.getElementById("bs_price_row").value;
	  
	  if(bs_price_ifshow != "0"){
		  if(bs_price_row == "1"){
			  oneRowInnerHTML += retailText;
		  }else if(bs_price_row == "2"){
			  twoRowInnerHTML += retailText;
		  }else if(bs_price_row == "3"){
			  threeRowInnerHTML += retailText;
		  }else if(bs_price_row == "4"){
			  fourRowInnerHTML += retailText;
		  }else if(bs_price_row == "5"){
			  fiveRowInnerHTML += retailText;
		  }else if(bs_price_row == "6"){
			  sixRowInnerHTML += retailText;
		  }else if(bs_price_row == "7"){
			  sevenRowInnerHTML += retailText;
		  }else if(bs_price_row == "8"){
			  eightRowInnerHTML += retailText;
		  }else if(bs_price_row == "9"){
			  nineRowInnerHTML += retailText;
		  }else if(bs_price_row == "10"){
			  tenRowInnerHTML += retailText;
		  }else if(bs_price_row == "11"){
			  elevenRowInnerHTML += retailText;
		  }
	  }
	  
	  //标牌价
	  var bs_signprice_ifshow = document.getElementById("bs_signprice_ifshow").value;
		var bs_signprice_row = document.getElementById("bs_signprice_row").value;
	  
	  if(bs_signprice_ifshow != "0"){
		  if(bs_signprice_row == "1"){
			  oneRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "2"){
			  twoRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "3"){
			  threeRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "4"){
			  fourRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "5"){
			  fiveRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "6"){
			  sixRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "7"){
			  sevenRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "8"){
			  eightRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "9"){
			  nineRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "10"){
			  tenRowInnerHTML += priceSignText;
		  }else if(bs_signprice_row == "11"){
			  elevenRowInnerHTML += priceSignText;
		  }
	  }
	  
	  //会员价
	  var bs_vipprice_ifshow = document.getElementById("bs_vipprice_ifshow").value;
		var bs_vipprice_row = document.getElementById("bs_vipprice_row").value;
	  
	  if(bs_vipprice_ifshow != "0"){
		  if(bs_vipprice_row == "1"){
			  oneRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "2"){
			  twoRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "3"){
			  threeRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "4"){
			  fourRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "5"){
			  fiveRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "6"){
			  sixRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "7"){
			  sevenRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "8"){
			  eightRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "9"){
			  nineRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "10"){
			  tenRowInnerHTML += priceVipText;
		  }else if(bs_vipprice_row == "11"){
			  elevenRowInnerHTML += priceVipText;
		  }
	  }
	  
	  var bs_filling_ifshow = document.getElementById("bs_filling_ifshow").value;
		var bs_filling_row = document.getElementById("bs_filling_row").value;
	  
	  if(bs_filling_ifshow != "0"){
		  if(bs_filling_row == "1"){
			  oneRowInnerHTML += filling;
		  }else if(bs_filling_row == "2"){
			  twoRowInnerHTML += filling;
		  }else if(bs_filling_row == "3"){
			  threeRowInnerHTML += filling;
		  }else if(bs_filling_row == "4"){
			  fourRowInnerHTML += filling;
		  }else if(bs_filling_row == "5"){
			  fiveRowInnerHTML += filling;
		  }else if(bs_filling_row == "6"){
			  sixRowInnerHTML += filling;
		  }else if(bs_filling_row == "7"){
			  sevenRowInnerHTML += filling;
		  }else if(bs_filling_row == "8"){
			  eightRowInnerHTML += filling;
		  }else if(bs_filling_row == "9"){
			  nineRowInnerHTML += filling;
		  }else if(bs_filling_row == "10"){
			  tenRowInnerHTML += filling;
		  }else if(bs_filling_row == "11"){
			  elevenRowInnerHTML += filling;
		  }
	  }
	  
	  var bs_type_ifshow = document.getElementById("bs_type_ifshow").value;
	  var bs_type_row = document.getElementById("bs_type_row").value;
	  
	  if(bs_type_ifshow != "0"){
		  if(bs_type_row == "1"){
			  oneRowInnerHTML += typeText;
		  }else if(bs_type_row == "2"){
			  twoRowInnerHTML += typeText;
		  }else if(bs_type_row == "3"){
			  threeRowInnerHTML += typeText;
		  }else if(bs_type_row == "4"){
			  fourRowInnerHTML += typeText;
		  }else if(bs_type_row == "5"){
			  fiveRowInnerHTML += typeText;
		  }else if(bs_type_row == "6"){
			  sixRowInnerHTML += typeText;
		  }else if(bs_type_row == "7"){
			  sevenRowInnerHTML += typeText;
		  }else if(bs_type_row == "8"){
			  eightRowInnerHTML += typeText;
		  }else if(bs_type_row == "9"){
			  nineRowInnerHTML += typeText;
		  }else if(bs_type_row == "10"){
			  tenRowInnerHTML += typeText;
		  }else if(bs_type_row == "11"){
			  elevenRowInnerHTML += typeText;
		  }
	  }
	  
	  var bs_class_ifshow = document.getElementsByName("bs_class_ifshow").value;
	  var bs_class_row = document.getElementById("bs_class_row").value;
	  
	  if(bs_class_ifshow != "0"){
		  if(bs_class_row == "1"){
			  oneRowInnerHTML += classText;
		  }else if(bs_class_row == "2"){
			  twoRowInnerHTML += classText;
		  }else if(bs_class_row == "3"){
			  threeRowInnerHTML += classText;
		  }else if(bs_class_row == "4"){
			  fourRowInnerHTML += classText;
		  }else if(bs_class_row == "5"){
			  fiveRowInnerHTML += classText;
		  }else if(bs_class_row == "6"){
			  sixRowInnerHTML += classText;
		  }else if(bs_class_row == "7"){
			  sevenRowInnerHTML += classText;
		  }else if(bs_class_row == "8"){
			  eightRowInnerHTML += classText;
		  }else if(bs_class_row == "9"){
			  nineRowInnerHTML += classText;
		  }else if(bs_class_row == "10"){
			  tenRowInnerHTML += classText;
		  }else if(bs_class_row == "11"){
			  elevenRowInnerHTML += classText;
		  }
	  }
	  
	  var bs_safestandard_ifshow = document.getElementsByName("bs_safestandard_ifshow").value;
	  var bs_safestandard_row = document.getElementById("bs_safestandard_row").value;
	  
	  if(bs_safestandard_ifshow != "0"){
		  if(bs_safestandard_row == "1"){
			  oneRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "2"){
			  twoRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "3"){
			  threeRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "4"){
			  fourRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "5"){
			  fiveRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "6"){
			  sixRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "7"){
			  sevenRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "8"){
			  eightRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "9"){
			  nineRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "10"){
			  tenRowInnerHTML += safeStandardText;
		  }else if(bs_safestandard_row == "11"){
			  elevenRowInnerHTML += safeStandardText;
		  }
	  }
	  
	//执行标准
	  var bs_executionstandard_ifshow = document.getElementsByName("bs_executionstandard_ifshow").value;
	  var bs_executionstandard_row = document.getElementById("bs_executionstandard_row").value;
	  
	  if(bs_executionstandard_ifshow != "0"){
		  if(bs_executionstandard_row == "1"){
			  oneRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "2"){
			  twoRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "3"){
			  threeRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "4"){
			  fourRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "5"){
			  fiveRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "6"){
			  sixRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "7"){
			  sevenRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "8"){
			  eightRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "9"){
			  nineRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "10"){
			  tenRowInnerHTML += executionStandard;
		  }else if(bs_executionstandard_row == "11"){
			  elevenRowInnerHTML += executionStandard;
		  }
	  }
	  
	  var bs_fabric_ifshow = document.getElementsByName("bs_fabric_ifshow").value;
	  var bs_fabric_row = document.getElementById("bs_fabric_row").value;
	  
	  if(bs_fabric_ifshow != "0"){
		  if(bs_fabric_row == "1"){
			  oneRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "2"){
			  twoRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "3"){
			  threeRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "4"){
			  fourRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "5"){
			  fiveRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "6"){
			  sixRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "7"){
			  sevenRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "8"){
			  eightRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "9"){
			  nineRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "10"){
			  tenRowInnerHTML += fabricText;
		  }else if(bs_fabric_row == "11"){
			  elevenRowInnerHTML += fabricText;
		  }
	  }
	  
	  var bs_infabric_ifshow = document.getElementsByName("bs_infabric_ifshow").value;
	  var bs_infabric_row = document.getElementById("bs_infabric_row").value;
	  if(bs_infabric_ifshow != "0"){
		  if(bs_infabric_row == "1"){
			  oneRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "2"){
			  twoRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "3"){
			  threeRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "4"){
			  fourRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "5"){
			  fiveRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "6"){
			  sixRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "7"){
			  sevenRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "8"){
			  eightRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "9"){
			  nineRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "10"){
			  tenRowInnerHTML += infabricText;
		  }else if(bs_infabric_row == "11"){
			  elevenRowInnerHTML += infabricText;
		  }
	  }
	  
	  var bs_place_ifshow = document.getElementsByName("bs_place_ifshow").value;
	  var bs_place_row = document.getElementById("bs_place_row").value;
	  if(bs_place_ifshow != "0"){
		  if(bs_place_row == "1"){
			  oneRowInnerHTML += placeText;
		  }else if(bs_place_row == "2"){
			  twoRowInnerHTML += placeText;
		  }else if(bs_place_row == "3"){
			  threeRowInnerHTML += placeText;
		  }else if(bs_place_row == "4"){
			  fourRowInnerHTML += placeText;
		  }else if(bs_place_row == "5"){
			  fiveRowInnerHTML += placeText;
		  }else if(bs_place_row == "6"){
			  sixRowInnerHTML += placeText;
		  }else if(bs_place_row == "7"){
			  sevenRowInnerHTML += placeText;
		  }else if(bs_place_row == "8"){
			  eightRowInnerHTML += placeText;
		  }else if(bs_place_row == "9"){
			  nineRowInnerHTML += placeText;
		  }else if(bs_place_row == "10"){
			  tenRowInnerHTML += placeText;
		  }else if(bs_place_row == "11"){
			  elevenRowInnerHTML += placeText;
		  }
	  }
	  
	  var bs_washexplain_ifshow = document.getElementsByName("bs_washexplain_ifshow").value;
	  var bs_washexplain_row = document.getElementById("bs_washexplain_row").value;
	  if(bs_washexplain_ifshow != "0"){
		  if(bs_washexplain_row == "1"){
			  oneRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "2"){
			  twoRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "3"){
			  threeRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "4"){
			  fourRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "5"){
			  fiveRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "6"){
			  sixRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "7"){
			  sevenRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "8"){
			  eightRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "9"){
			  nineRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "10"){
			  tenRowInnerHTML += washexplainText;
		  }else if(bs_washexplain_row == "11"){
			  elevenRowInnerHTML += washexplainText;
		  }
	  }
	  
    if(bs_line == "1"){
    	var subCodeShow1Obj = document.getElementById("subCodeShow1");//条形码值
    	var subCodeShowFont1Obj = document.getElementById("subCodeShowFont1");//字体
    	
    	var subCodeShow1_UpObj = document.getElementById("subCodeShow1_Up");//条形码值
    	var subCodeShowFont1_UpObj = document.getElementById("subCodeShowFont1_Up");//字体
    	
    	var priceShow1_DownObj = document.getElementById("priceShow1_Down");//下方价格
    	var priceShowFont1_DownObj = document.getElementById("priceShowFont1_Down");//下方价格字体
    	
    	preview2Obj.style.display = "none";
    	preview3Obj.style.display = "none";
    	previewObj.style.display = "block";
    	previewObj.style.width = (parseFloat(bs_paper_width,10)+3) + "mm";
    	
    	previewObj.style.height = bs_paper_height + "mm";
    	paperDivObj.style.height = (bs_paper_height-2.5) + "mm";
    	paperDivObj.style.width = bs_paper_width + "mm";
    	paperDivObj.style.paddingLeft = "1.5mm";
    	
    	var divFont1Obj = document.getElementById("divFont1");
    	var divFont2Obj = document.getElementById("divFont2");
    	var divFont3Obj = document.getElementById("divFont3");
    	var divFont4Obj = document.getElementById("divFont4");
    	var divFont5Obj = document.getElementById("divFont5");
    	var divFont6Obj = document.getElementById("divFont6");
    	var divFont7Obj = document.getElementById("divFont7");
    	var divFont8Obj = document.getElementById("divFont8");
    	var divFont9Obj = document.getElementById("divFont9");
    	var divFont10Obj = document.getElementById("divFont10");
    	var font1Obj = document.getElementById("font1");
    	var font2Obj = document.getElementById("font2");
    	var font3Obj = document.getElementById("font3");
    	var font4Obj = document.getElementById("font4");
    	var font5Obj = document.getElementById("font5");
    	var font6Obj = document.getElementById("font6");
    	var font7Obj = document.getElementById("font7");
    	var font8Obj = document.getElementById("font8");
    	var font9Obj = document.getElementById("font9");
    	var font10Obj = document.getElementById("font10");
    	var font11Obj = document.getElementById("font10");
    	divFont1Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//商品信息大小
    	divFont2Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont3Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont4Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont5Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont6Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont7Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont8Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont9Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont10Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	
    	divFont1Obj.style.marginBottom = bs_p_space+"mm";//商品信息间距
    	divFont2Obj.style.marginBottom = bs_p_space+"mm";
    	divFont3Obj.style.marginBottom = bs_p_space+"mm";
    	divFont4Obj.style.marginBottom = bs_p_space+"mm";
    	divFont5Obj.style.marginBottom = bs_p_space+"mm";
    	divFont6Obj.style.marginBottom = bs_p_space+"mm";
    	divFont7Obj.style.marginBottom = bs_p_space+"mm";
    	divFont8Obj.style.marginBottom = bs_p_space+"mm";
    	divFont9Obj.style.marginBottom = bs_p_space+"mm";
    	divFont10Obj.style.marginBottom = bs_p_space+"mm";
    	
    	divFont1Obj.style.marginTop = (parseFloat(bs_top_backgauge,10)-1)+"mm";//整页上边距
    	if(oneRowInnerHTML == ""){
    		divFont1Obj.style.display = "none";
    	}else{
    		divFont1Obj.style.display = "block";
    		font1Obj.innerHTML = oneRowInnerHTML;
    		font1Obj.style.fontSize = bs_font_size+"pt";
    		font1Obj.face = bs_font_name;
    		font1Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font1Obj.style.fontWeight = "bold";
    		}else{
    			font1Obj.style.fontWeight = "normal";
    		}
    		
    	}
    	if(twoRowInnerHTML == ""){
    		divFont2Obj.style.display = "none";
    	}else{
    		divFont2Obj.style.display = "block";
    		font2Obj.innerHTML = twoRowInnerHTML;
    		font2Obj.style.fontSize = bs_font_size+"pt";
    		font2Obj.face = bs_font_name;
    		font2Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font2Obj.style.fontWeight = "bold";
    		}else{
    			font2Obj.style.fontWeight = "normal";
    		}
    	}
    	if(threeRowInnerHTML == ""){
    		divFont3Obj.style.display = "none";
    	}else{
    		divFont3Obj.style.display = "block";
    		font3Obj.innerHTML = threeRowInnerHTML;
    		font3Obj.style.fontSize = bs_font_size+"pt";
    		font3Obj.face = bs_font_name;
    		font3Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font3Obj.style.fontWeight = "bold";
    		}else{
    			font3Obj.style.fontWeight = "normal";
    		}
    	}
    	if(fourRowInnerHTML == ""){
    		divFont4Obj.style.display = "none";
    	}else{
    		divFont4Obj.style.display = "block";
    		font4Obj.innerHTML = fourRowInnerHTML;
    		font4Obj.style.fontSize = bs_font_size+"pt";
    		font4Obj.face = bs_font_name;
    		font4Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font4Obj.style.fontWeight = "bold";
    		}else{
    			font4Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(fiveRowInnerHTML == ""){
    		divFont5Obj.style.display = "none";
    	}else{
    		divFont5Obj.style.display = "block";
    		font5Obj.innerHTML = fiveRowInnerHTML;
    		font5Obj.style.fontSize = bs_font_size+"pt";
    		font5Obj.face = bs_font_name;
    		font5Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font5Obj.style.fontWeight = "bold";
    		}else{
    			font5Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sixRowInnerHTML == ""){
    		divFont6Obj.style.display = "none";
    	}else{
    		divFont6Obj.style.display = "block";
    		font6Obj.innerHTML = sixRowInnerHTML;
    		font6Obj.style.fontSize = bs_font_size+"pt";
    		font6Obj.face = bs_font_name;
    		font6Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font6Obj.style.fontWeight = "bold";
    		}else{
    			font6Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sevenRowInnerHTML == ""){
    		divFont7Obj.style.display = "none";
    	}else{
    		divFont7Obj.style.display = "block";
    		font7Obj.innerHTML = sevenRowInnerHTML;
    		font7Obj.style.fontSize = bs_font_size+"pt";
    		font7Obj.face = bs_font_name;
    		font7Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font7Obj.style.fontWeight = "bold";
    		}else{
    			font7Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(eightRowInnerHTML == ""){
    		divFont8Obj.style.display = "none";
    	}else{
    		divFont8Obj.style.display = "block";
    		font8Obj.innerHTML = eightRowInnerHTML;
    		font8Obj.style.fontSize = bs_font_size+"pt";
    		font8Obj.face = bs_font_name;
    		font8Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font8Obj.style.fontWeight = "bold";
    		}else{
    			font8Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(nineRowInnerHTML == ""){
    		divFont9Obj.style.display = "none";
    	}else{
    		divFont9Obj.style.display = "block";
    		font9Obj.innerHTML = nineRowInnerHTML;
    		font9Obj.style.fontSize = bs_font_size+"pt";
    		font9Obj.face = bs_font_name;
    		font9Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font9Obj.style.fontWeight = "bold";
    		}else{
    			font9Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(tenRowInnerHTML == ""){//成分
    		divFont10Obj.style.display = "none";
    	}else{
    		divFont10Obj.style.display = "block";
    		font10Obj.innerHTML = tenRowInnerHTML;
    		font10Obj.style.fontSize = bs_font_size+"pt";
    		font10Obj.face = bs_font_name;
    		font10Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font10Obj.style.fontWeight = "bold";
    		}else{
    			font10Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(elevenRowInnerHTML == ""){
    		divFont10Obj.style.display = "none";
    	}else{
    		divFont10Obj.style.display = "block";
    		font11Obj.innerHTML = elevenRowInnerHTML;
    		font11Obj.style.fontSize = bs_font_size+"pt";
    		font11Obj.face = bs_font_name;
    		font11Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font11Obj.style.fontWeight = "bold";
    		}else{
    			font11Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	var barcodeObj = document.getElementById("barcode");
    	barcodeObj.style.width = bs_width+"mm";
    	barcodeObj.style.height = bs_height+"mm";
    	barcodeObj.style.paddingLeft = bs_left_backgauge+"mm";
    	barcodeObj.style.marginTop = bs_ps_space+"mm";
    	
    	subCodeShow1Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
    	subCodeShowFont1Obj.style.fontSize = bs_font_size+"pt";
    	subCodeShowFont1Obj.face = bs_font_name;
    	subCodeShowFont1Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	if(bs_bold == "1"){
    		subCodeShowFont1Obj.style.fontWeight = "bold";
		}else{
			subCodeShowFont1Obj.style.fontWeight = "normal";
		}
    	if(subCode_ShowUp == "1"){
    		subCodeShow1_UpObj.style.display = "block";
        	subCodeShow1_UpObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	subCodeShow1_UpObj.style.marginBottom = 1+"mm";
        	subCodeShowFont1_UpObj.style.fontSize = bs_font_size+"pt";
        	subCodeShowFont1_UpObj.face = bs_font_name;
        	subCodeShowFont1_UpObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		subCodeShowFont1_UpObj.style.fontWeight = "bold";
    		}else{
    			subCodeShowFont1_UpObj.style.fontWeight = "normal";
    		}
    	}else{
    		subCodeShow1_UpObj.style.display = "none";
    	}
    	
    	if(price_ShowDown == "1"){//下方价格显示
    		priceShow1_DownObj.style.display = "block";
    		priceShow1_DownObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	priceShowFont1_DownObj.style.fontSize = bs_font_size+"pt";
        	priceShowFont1_DownObj.face = bs_font_name;
        	priceShowFont1_DownObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		priceShowFont1_DownObj.style.fontWeight = "bold";
    		}else{
    			priceShowFont1_DownObj.style.fontWeight = "normal";
    		}
    	}else{
    		priceShow1_DownObj.style.display = "none";
    	}
    	
    }else if(bs_line == "2"){
    	var subCodeShow2Obj = document.getElementById("subCodeShow2");//条形码值
    	var subCodeShowFont2Obj = document.getElementById("subCodeShowFont2");//字体
    	var subCodeShow3Obj = document.getElementById("subCodeShow3");//条形码值
    	var subCodeShowFont3Obj = document.getElementById("subCodeShowFont3");//字体
    	
    	var subCodeShow2_UpObj = document.getElementById("subCodeShow2_Up");//条形码值-上方条形码
    	var subCodeShowFont2_UpObj = document.getElementById("subCodeShowFont2_Up");//字体
    	var subCodeShow3_UpObj = document.getElementById("subCodeShow3_Up");//条形码值-上方条形码
    	var subCodeShowFont3_UpObj = document.getElementById("subCodeShowFont3_Up");//字体
    	
    	var priceShow2_DownObj = document.getElementById("priceShow2_Down");//下方价格
    	var priceShowFont2_DownObj = document.getElementById("priceShowFont2_Down");//下方价格字体
    	var priceShow3_DownObj = document.getElementById("priceShow3_Down");//下方价格
    	var priceShowFont3_DownObj = document.getElementById("priceShowFont3_Down");//下方价格字体
    	
    	previewObj.style.display = "none";
    	preview3Obj.style.display = "none";
   		preview2Obj.style.display = "block";
    	preview2Obj.style.width = (parseFloat(bs_paper_width,10)*2+parseFloat(bs_left_backgauge,10)*2+parseFloat(bs_width_spacing,10)) + "mm";
    	preview2Obj.style.height = bs_paper_height + "mm";
    	paperDiv1Obj.style.height = (bs_paper_height-2.5) + "mm";
    	paperDiv1Obj.style.width = bs_paper_width + "mm";
    	paperDiv1Obj.style.paddingLeft = bs_left_backgauge + "mm";
    	
    	paperDiv2Obj.style.height = (bs_paper_height-2.5) + "mm";
    	paperDiv2Obj.style.width = bs_paper_width + "mm";
    	paperDiv2Obj.style.paddingLeft = parseFloat(bs_width_spacing,10) + "mm";
    	
    	
    	var divFont21Obj = document.getElementById("divFont21");
    	var divFont22Obj = document.getElementById("divFont22");
    	var divFont23Obj = document.getElementById("divFont23");
    	var divFont24Obj = document.getElementById("divFont24");
    	var divFont25Obj = document.getElementById("divFont25");
    	var divFont26Obj = document.getElementById("divFont26");
    	var divFont27Obj = document.getElementById("divFont27");
    	var divFont28Obj = document.getElementById("divFont28");
    	var divFont29Obj = document.getElementById("divFont29");
    	var divFont30Obj = document.getElementById("divFont30");
    	var font21Obj = document.getElementById("font21");
    	var font22Obj = document.getElementById("font22");
    	var font23Obj = document.getElementById("font23");
    	var font24Obj = document.getElementById("font24");
    	var font25Obj = document.getElementById("font25");
    	var font26Obj = document.getElementById("font26");
    	var font27Obj = document.getElementById("font27");
    	var font28Obj = document.getElementById("font28");
    	var font29Obj = document.getElementById("font29");
    	var font30Obj = document.getElementById("font30");
    	
    	divFont21Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//商品信息大小
    	divFont22Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont23Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont24Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont25Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont26Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont27Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont28Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont29Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont30Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	
    	divFont21Obj.style.marginBottom = bs_p_space+"mm";//商品信息间距
    	divFont22Obj.style.marginBottom = bs_p_space+"mm";
    	divFont23Obj.style.marginBottom = bs_p_space+"mm";
    	divFont24Obj.style.marginBottom = bs_p_space+"mm";
    	divFont25Obj.style.marginBottom = bs_p_space+"mm";
    	divFont26Obj.style.marginBottom = bs_p_space+"mm";
    	divFont27Obj.style.marginBottom = bs_p_space+"mm";
    	divFont28Obj.style.marginBottom = bs_p_space+"mm";
    	divFont29Obj.style.marginBottom = bs_p_space+"mm";
    	divFont30Obj.style.marginBottom = bs_p_space+"mm";
    	
    	divFont21Obj.style.marginTop = (parseFloat(bs_top_backgauge,10)-1)+"mm";//整页上边距
    	if(oneRowInnerHTML == ""){
    		divFont21Obj.style.display = "none";
    	}else{
    		divFont21Obj.style.display = "block";
    		font21Obj.innerHTML = oneRowInnerHTML;
    		font21Obj.style.fontSize = bs_font_size+"pt";
    		font21Obj.face = bs_font_name;
    		font21Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font21Obj.style.fontWeight = "bold";
    		}else{
    			font21Obj.style.fontWeight = "normal";
    		}
    		
    	}
    	if(twoRowInnerHTML == ""){
    		divFont22Obj.style.display = "none";
    	}else{
    		divFont22Obj.style.display = "block";
    		font22Obj.innerHTML = twoRowInnerHTML;
    		font22Obj.style.fontSize = bs_font_size+"pt";
    		font22Obj.face = bs_font_name;
    		font22Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font22Obj.style.fontWeight = "bold";
    		}else{
    			font22Obj.style.fontWeight = "normal";
    		}
    	}
    	if(threeRowInnerHTML == ""){
    		divFont23Obj.style.display = "none";
    	}else{
    		divFont23Obj.style.display = "block";
    		font23Obj.innerHTML = threeRowInnerHTML;
    		font23Obj.style.fontSize = bs_font_size+"pt";
    		font23Obj.face = bs_font_name;
    		font23Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font23Obj.style.fontWeight = "bold";
    		}else{
    			font23Obj.style.fontWeight = "normal";
    		}
    	}
    	if(fourRowInnerHTML == ""){
    		divFont24Obj.style.display = "none";
    	}else{
    		divFont24Obj.style.display = "block";
    		font24Obj.innerHTML = fourRowInnerHTML;
    		font24Obj.style.fontSize = bs_font_size+"pt";
    		font24Obj.face = bs_font_name;
    		font24Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font24Obj.style.fontWeight = "bold";
    		}else{
    			font24Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(fiveRowInnerHTML == ""){
    		divFont25Obj.style.display = "none";
    	}else{
    		divFont25Obj.style.display = "block";
    		font25Obj.innerHTML = fiveRowInnerHTML;
    		font25Obj.style.fontSize = bs_font_size+"pt";
    		font25Obj.face = bs_font_name;
    		font25Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font25Obj.style.fontWeight = "bold";
    		}else{
    			font25Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sixRowInnerHTML == ""){
    		divFont26Obj.style.display = "none";
    	}else{
    		divFont26Obj.style.display = "block";
    		font26Obj.innerHTML = sixRowInnerHTML;
    		font26Obj.style.fontSize = bs_font_size+"pt";
    		font26Obj.face = bs_font_name;
    		font26Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font26Obj.style.fontWeight = "bold";
    		}else{
    			font26Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sevenRowInnerHTML == ""){
    		divFont27Obj.style.display = "none";
    	}else{
    		divFont27Obj.style.display = "block";
    		font27Obj.innerHTML = sevenRowInnerHTML;
    		font27Obj.style.fontSize = bs_font_size+"pt";
    		font27Obj.face = bs_font_name;
    		font27Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font27Obj.style.fontWeight = "bold";
    		}else{
    			font27Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(eightRowInnerHTML == ""){
    		divFont28Obj.style.display = "none";
    	}else{
    		divFont28Obj.style.display = "block";
    		font28Obj.innerHTML = eightRowInnerHTML;
    		font28Obj.style.fontSize = bs_font_size+"pt";
    		font28Obj.face = bs_font_name;
    		font28Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font28Obj.style.fontWeight = "bold";
    		}else{
    			font28Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(nineRowInnerHTML == ""){
    		divFont29Obj.style.display = "none";
    	}else{
    		divFont29Obj.style.display = "block";
    		font29Obj.innerHTML = nineRowInnerHTML;
    		font29Obj.style.fontSize = bs_font_size+"pt";
    		font29Obj.face = bs_font_name;
    		font29Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font29Obj.style.fontWeight = "bold";
    		}else{
    			font29Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(tenRowInnerHTML == ""){
    		divFont30Obj.style.display = "none";
    	}else{
    		divFont30Obj.style.display = "block";
    		font30Obj.innerHTML = tenRowInnerHTML;
    		font30Obj.style.fontSize = bs_font_size+"pt";
    		font30Obj.face = bs_font_name;
    		font30Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font30Obj.style.fontWeight = "bold";
    		}else{
    			font30Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	var barcode2Obj = document.getElementById("barcode2");
    	barcode2Obj.style.width = bs_width+"mm";
    	barcode2Obj.style.height = bs_height+"mm";
    	barcode2Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	barcode2Obj.style.marginTop = bs_ps_space+"mm";
    	
    	subCodeShow2Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
    	subCodeShowFont2Obj.style.fontSize = bs_font_size+"pt";
    	subCodeShowFont2Obj.face = bs_font_name;
    	subCodeShowFont2Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	if(bs_bold == "1"){
    		subCodeShowFont2Obj.style.fontWeight = "bold";
		}else{
			subCodeShowFont2Obj.style.fontWeight = "normal";
		}
    	
    	
    	var divFont31Obj = document.getElementById("divFont31");
    	var divFont32Obj = document.getElementById("divFont32");
    	var divFont33Obj = document.getElementById("divFont33");
    	var divFont34Obj = document.getElementById("divFont34");
    	var divFont35Obj = document.getElementById("divFont35");
    	var divFont36Obj = document.getElementById("divFont36");
    	var divFont37Obj = document.getElementById("divFont37");
    	var divFont38Obj = document.getElementById("divFont38");
    	var divFont39Obj = document.getElementById("divFont39");
    	var divFont40Obj = document.getElementById("divFont40");
    	var font31Obj = document.getElementById("font31");
    	var font32Obj = document.getElementById("font32");
    	var font33Obj = document.getElementById("font33");
    	var font34Obj = document.getElementById("font34");
    	var font35Obj = document.getElementById("font35");
    	var font36Obj = document.getElementById("font36");
    	var font37Obj = document.getElementById("font37");
    	var font38Obj = document.getElementById("font38");
    	var font39Obj = document.getElementById("font39");
    	var font40Obj = document.getElementById("font40");
    	
    	divFont31Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//商品信息大小
    	divFont32Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont33Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont34Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont35Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont36Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont37Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont38Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont39Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont40Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	
    	divFont31Obj.style.marginBottom = bs_p_space+"mm";//商品信息间距
    	divFont32Obj.style.marginBottom = bs_p_space+"mm";
    	divFont33Obj.style.marginBottom = bs_p_space+"mm";
    	divFont34Obj.style.marginBottom = bs_p_space+"mm";
    	divFont35Obj.style.marginBottom = bs_p_space+"mm";
    	divFont36Obj.style.marginBottom = bs_p_space+"mm";
    	divFont37Obj.style.marginBottom = bs_p_space+"mm";
    	divFont38Obj.style.marginBottom = bs_p_space+"mm";
    	divFont39Obj.style.marginBottom = bs_p_space+"mm";
    	divFont40Obj.style.marginBottom = bs_p_space+"mm";
    	
    	divFont31Obj.style.marginTop = (parseFloat(bs_top_backgauge,10)-1)+"mm";//整页上边距
    	if(oneRowInnerHTML == ""){
    		divFont31Obj.style.display = "none";
    	}else{
    		divFont31Obj.style.display = "block";
    		font31Obj.innerHTML = oneRowInnerHTML;
    		font31Obj.style.fontSize = bs_font_size+"pt";
    		font31Obj.face = bs_font_name;
    		font31Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font31Obj.style.fontWeight = "bold";
    		}else{
    			font31Obj.style.fontWeight = "normal";
    		}
    		
    	}
    	if(twoRowInnerHTML == ""){
    		divFont32Obj.style.display = "none";
    	}else{
    		divFont32Obj.style.display = "block";
    		font32Obj.innerHTML = twoRowInnerHTML;
    		font32Obj.style.fontSize = bs_font_size+"pt";
    		font32Obj.face = bs_font_name;
    		font32Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font32Obj.style.fontWeight = "bold";
    		}else{
    			font32Obj.style.fontWeight = "normal";
    		}
    	}
    	if(threeRowInnerHTML == ""){
    		divFont33Obj.style.display = "none";
    	}else{
    		divFont33Obj.style.display = "block";
    		font33Obj.innerHTML = threeRowInnerHTML;
    		font33Obj.style.fontSize = bs_font_size+"pt";
    		font33Obj.face = bs_font_name;
    		font33Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font33Obj.style.fontWeight = "bold";
    		}else{
    			font33Obj.style.fontWeight = "normal";
    		}
    	}
    	if(fourRowInnerHTML == ""){
    		divFont34Obj.style.display = "none";
    	}else{
    		divFont34Obj.style.display = "block";
    		font34Obj.innerHTML = fourRowInnerHTML;
    		font34Obj.style.fontSize = bs_font_size+"pt";
    		font34Obj.face = bs_font_name;
    		font34Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font34Obj.style.fontWeight = "bold";
    		}else{
    			font34Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(fiveRowInnerHTML == ""){
    		divFont35Obj.style.display = "none";
    	}else{
    		divFont35Obj.style.display = "block";
    		font35Obj.innerHTML = fiveRowInnerHTML;
    		font35Obj.style.fontSize = bs_font_size+"pt";
    		font35Obj.face = bs_font_name;
    		font35Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font35Obj.style.fontWeight = "bold";
    		}else{
    			font35Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sixRowInnerHTML == ""){
    		divFont36Obj.style.display = "none";
    	}else{
    		divFont36Obj.style.display = "block";
    		font36Obj.innerHTML = sixRowInnerHTML;
    		font36Obj.style.fontSize = bs_font_size+"pt";
    		font36Obj.face = bs_font_name;
    		font36Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font36Obj.style.fontWeight = "bold";
    		}else{
    			font36Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sevenRowInnerHTML == ""){
    		divFont37Obj.style.display = "none";
    	}else{
    		divFont37Obj.style.display = "block";
    		font37Obj.innerHTML = sevenRowInnerHTML;
    		font37Obj.style.fontSize = bs_font_size+"pt";
    		font37Obj.face = bs_font_name;
    		font37Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font37Obj.style.fontWeight = "bold";
    		}else{
    			font37Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(eightRowInnerHTML == ""){
    		divFont38Obj.style.display = "none";
    	}else{
    		divFont38Obj.style.display = "block";
    		font38Obj.innerHTML = eightRowInnerHTML;
    		font38Obj.style.fontSize = bs_font_size+"pt";
    		font38Obj.face = bs_font_name;
    		font38Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font38Obj.style.fontWeight = "bold";
    		}else{
    			font38Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(nineRowInnerHTML == ""){
    		divFont39Obj.style.display = "none";
    	}else{
    		divFont39Obj.style.display = "block";
    		font39Obj.innerHTML = nineRowInnerHTML;
    		font39Obj.style.fontSize = bs_font_size+"pt";
    		font39Obj.face = bs_font_name;
    		font39Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font39Obj.style.fontWeight = "bold";
    		}else{
    			font39Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(tenRowInnerHTML == ""){
    		divFont40Obj.style.display = "none";
    	}else{
    		divFont40Obj.style.display = "block";
    		font40Obj.innerHTML = tenRowInnerHTML;
    		font40Obj.style.fontSize = bs_font_size+"pt";
    		font40Obj.face = bs_font_name;
    		font40Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font40Obj.style.fontWeight = "bold";
    		}else{
    			font40Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	var barcode3Obj = document.getElementById("barcode3");
    	barcode3Obj.style.width = bs_width+"mm";
    	barcode3Obj.style.height = bs_height+"mm";
    	barcode3Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	barcode3Obj.style.marginTop = bs_ps_space+"mm";
    	
    	subCodeShow3Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
    	subCodeShowFont3Obj.style.fontSize = bs_font_size+"pt";
    	subCodeShowFont3Obj.face = bs_font_name;
    	subCodeShowFont3Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	if(bs_bold == "1"){
    		subCodeShowFont3Obj.style.fontWeight = "bold";
		}else{
			subCodeShowFont3Obj.style.fontWeight = "normal";
		}
    	
    	if(subCode_ShowUp == "1"){
    		subCodeShow2_UpObj.style.display = "block";
        	subCodeShow2_UpObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	subCodeShow2_UpObj.style.marginBottom = 1+"mm";
        	subCodeShowFont2_UpObj.style.fontSize = bs_font_size+"pt";
        	subCodeShowFont2_UpObj.face = bs_font_name;
        	subCodeShowFont2_UpObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		subCodeShowFont2_UpObj.style.fontWeight = "bold";
    		}else{
    			subCodeShowFont2_UpObj.style.fontWeight = "normal";
    		}
    	}else{
    		subCodeShow2_UpObj.style.display = "none";
    	}
    	
    	if(subCode_ShowUp == "1"){
    		subCodeShow3_UpObj.style.display = "block";
        	subCodeShow3_UpObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	subCodeShow3_UpObj.style.marginBottom = 1+"mm";
        	subCodeShowFont3_UpObj.style.fontSize = bs_font_size+"pt";
        	subCodeShowFont3_UpObj.face = bs_font_name;
        	subCodeShowFont3_UpObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		subCodeShowFont3_UpObj.style.fontWeight = "bold";
    		}else{
    			subCodeShowFont3_UpObj.style.fontWeight = "normal";
    		}
    	}else{
    		subCodeShow3_UpObj.style.display = "none";
    	}
    	if(price_ShowDown == "1"){//下方价格显示
    		priceShow2_DownObj.style.display = "block";
    		priceShow2_DownObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	priceShowFont2_DownObj.style.fontSize = bs_font_size+"pt";
        	priceShowFont2_DownObj.face = bs_font_name;
        	priceShowFont2_DownObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		priceShowFont2_DownObj.style.fontWeight = "bold";
    		}else{
    			priceShowFont2_DownObj.style.fontWeight = "normal";
    		}
    	}else{
    		priceShow2_DownObj.style.display = "none";
    	}
    	if(price_ShowDown == "1"){//下方价格显示
    		priceShow3_DownObj.style.display = "block";
    		priceShow3_DownObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	priceShowFont3_DownObj.style.fontSize = bs_font_size+"pt";
        	priceShowFont3_DownObj.face = bs_font_name;
        	priceShowFont3_DownObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		priceShowFont3_DownObj.style.fontWeight = "bold";
    		}else{
    			priceShowFont3_DownObj.style.fontWeight = "normal";
    		}
    	}else{
    		priceShow3_DownObj.style.display = "none";
    	}
    }else if(bs_line == "3"){
    	var subCodeShow4Obj = document.getElementById("subCodeShow4");//条形码值
    	var subCodeShowFont4Obj = document.getElementById("subCodeShowFont4");//字体
    	var subCodeShow5Obj = document.getElementById("subCodeShow5");//条形码值
    	var subCodeShowFont5Obj = document.getElementById("subCodeShowFont5");//字体
    	var subCodeShow6Obj = document.getElementById("subCodeShow6");//条形码值
    	var subCodeShowFont6Obj = document.getElementById("subCodeShowFont6");//字体
    	
    	var subCodeShow4_UpObj = document.getElementById("subCodeShow4_Up");//条形码值-上方条形码
    	var subCodeShowFont4_UpObj = document.getElementById("subCodeShowFont4_Up");//字体
    	var subCodeShow5_UpObj = document.getElementById("subCodeShow5_Up");//条形码值-上方条形码
    	var subCodeShowFont5_UpObj = document.getElementById("subCodeShowFont5_Up");//字体
    	var subCodeShow6_UpObj = document.getElementById("subCodeShow6_Up");//条形码值-上方条形码
    	var subCodeShowFont6_UpObj = document.getElementById("subCodeShowFont6_Up");//字体
    	
    	var priceShow4_DownObj = document.getElementById("priceShow4_Down");//下方价格
    	var priceShowFont4_DownObj = document.getElementById("priceShowFont4_Down");//下方价格字体
    	var priceShow5_DownObj = document.getElementById("priceShow5_Down");//下方价格
    	var priceShowFont5_DownObj = document.getElementById("priceShowFont5_Down");//下方价格字体
    	var priceShow6_DownObj = document.getElementById("priceShow6_Down");//下方价格
    	var priceShowFont6_DownObj = document.getElementById("priceShowFont6_Down");//下方价格字体
    	
    	previewObj.style.display = "none";
    	preview2Obj.style.display = "none";
    	preview3Obj.style.display = "block";
    	
    	preview3Obj.style.width = (parseFloat(bs_paper_width,10)*3+parseFloat(bs_left_backgauge,10)*2+parseFloat(bs_width_spacing,10)*2) + "mm";
    	preview3Obj.style.height = bs_paper_height + "mm";
    	paperDiv3Obj.style.height = (bs_paper_height-2.5) + "mm";
    	paperDiv3Obj.style.width = bs_paper_width + "mm";
    	paperDiv3Obj.style.paddingLeft = bs_left_backgauge + "mm";
    	
    	paperDiv4Obj.style.height = (bs_paper_height-2.5) + "mm";
    	paperDiv4Obj.style.width = bs_paper_width + "mm";
    	paperDiv4Obj.style.paddingLeft = parseFloat(bs_width_spacing,10) + "mm";
    	
    	paperDiv5Obj.style.height = (bs_paper_height-2.5) + "mm";
    	paperDiv5Obj.style.width = bs_paper_width + "mm";
    	paperDiv5Obj.style.paddingLeft = parseFloat(bs_width_spacing,10) + "mm";
    	
    	var divFont41Obj = document.getElementById("divFont41");
    	var divFont42Obj = document.getElementById("divFont42");
    	var divFont43Obj = document.getElementById("divFont43");
    	var divFont44Obj = document.getElementById("divFont44");
    	var divFont45Obj = document.getElementById("divFont45");
    	var divFont46Obj = document.getElementById("divFont46");
    	var divFont47Obj = document.getElementById("divFont47");
    	var divFont48Obj = document.getElementById("divFont48");
    	var divFont49Obj = document.getElementById("divFont49");
    	var divFont50Obj = document.getElementById("divFont50");
    	var font41Obj = document.getElementById("font41");
    	var font42Obj = document.getElementById("font42");
    	var font43Obj = document.getElementById("font43");
    	var font44Obj = document.getElementById("font44");
    	var font45Obj = document.getElementById("font45");
    	var font46Obj = document.getElementById("font46");
    	var font47Obj = document.getElementById("font47");
    	var font48Obj = document.getElementById("font48");
    	var font49Obj = document.getElementById("font49");
    	var font50Obj = document.getElementById("font50");
    	
    	divFont41Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//商品信息大小
    	divFont42Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont43Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont44Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont45Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont46Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont47Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont48Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont49Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont50Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	
    	divFont41Obj.style.marginBottom = bs_p_space+"mm";//商品信息间距
    	divFont42Obj.style.marginBottom = bs_p_space+"mm";
    	divFont43Obj.style.marginBottom = bs_p_space+"mm";
    	divFont44Obj.style.marginBottom = bs_p_space+"mm";
    	divFont45Obj.style.marginBottom = bs_p_space+"mm";
    	divFont46Obj.style.marginBottom = bs_p_space+"mm";
    	divFont47Obj.style.marginBottom = bs_p_space+"mm";
    	divFont48Obj.style.marginBottom = bs_p_space+"mm";
    	divFont49Obj.style.marginBottom = bs_p_space+"mm";
    	divFont50Obj.style.marginBottom = bs_p_space+"mm";
    	
    	divFont41Obj.style.marginTop = (parseFloat(bs_top_backgauge,10)-1)+"mm";//整页上边距
    	if(oneRowInnerHTML == ""){
    		divFont41Obj.style.display = "none";
    	}else{
    		divFont41Obj.style.display = "block";
    		font41Obj.innerHTML = oneRowInnerHTML;
    		font41Obj.style.fontSize = bs_font_size+"pt";
    		font41Obj.face = bs_font_name;
    		font41Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font41Obj.style.fontWeight = "bold";
    		}else{
    			font41Obj.style.fontWeight = "normal";
    		}
    		
    	}
    	if(twoRowInnerHTML == ""){
    		divFont42Obj.style.display = "none";
    	}else{
    		divFont42Obj.style.display = "block";
    		font42Obj.innerHTML = twoRowInnerHTML;
    		font42Obj.style.fontSize = bs_font_size+"pt";
    		font42Obj.face = bs_font_name;
    		font42Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font42Obj.style.fontWeight = "bold";
    		}else{
    			font42Obj.style.fontWeight = "normal";
    		}
    	}
    	if(threeRowInnerHTML == ""){
    		divFont43Obj.style.display = "none";
    	}else{
    		divFont43Obj.style.display = "block";
    		font43Obj.innerHTML = threeRowInnerHTML;
    		font43Obj.style.fontSize = bs_font_size+"pt";
    		font43Obj.face = bs_font_name;
    		font43Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font43Obj.style.fontWeight = "bold";
    		}else{
    			font43Obj.style.fontWeight = "normal";
    		}
    	}
    	if(fourRowInnerHTML == ""){
    		divFont44Obj.style.display = "none";
    	}else{
    		divFont44Obj.style.display = "block";
    		font44Obj.innerHTML = fourRowInnerHTML;
    		font44Obj.style.fontSize = bs_font_size+"pt";
    		font44Obj.face = bs_font_name;
    		font44Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font44Obj.style.fontWeight = "bold";
    		}else{
    			font44Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(fiveRowInnerHTML == ""){
    		divFont45Obj.style.display = "none";
    	}else{
    		divFont45Obj.style.display = "block";
    		font45Obj.innerHTML = fiveRowInnerHTML;
    		font45Obj.style.fontSize = bs_font_size+"pt";
    		font45Obj.face = bs_font_name;
    		font45Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font45Obj.style.fontWeight = "bold";
    		}else{
    			font45Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sixRowInnerHTML == ""){
    		divFont46Obj.style.display = "none";
    	}else{
    		divFont46Obj.style.display = "block";
    		font46Obj.innerHTML = sixRowInnerHTML;
    		font46Obj.style.fontSize = bs_font_size+"pt";
    		font46Obj.face = bs_font_name;
    		font46Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font46Obj.style.fontWeight = "bold";
    		}else{
    			font46Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sevenRowInnerHTML == ""){
    		divFont47Obj.style.display = "none";
    	}else{
    		divFont47Obj.style.display = "block";
    		font47Obj.innerHTML = sevenRowInnerHTML;
    		font47Obj.style.fontSize = bs_font_size+"pt";
    		font47Obj.face = bs_font_name;
    		font47Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font47Obj.style.fontWeight = "bold";
    		}else{
    			font47Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(eightRowInnerHTML == ""){
    		divFont48Obj.style.display = "none";
    	}else{
    		divFont48Obj.style.display = "block";
    		font48Obj.innerHTML = eightRowInnerHTML;
    		font48Obj.style.fontSize = bs_font_size+"pt";
    		font48Obj.face = bs_font_name;
    		font48Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font48Obj.style.fontWeight = "bold";
    		}else{
    			font48Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(nineRowInnerHTML == ""){
    		divFont49Obj.style.display = "none";
    	}else{
    		divFont49Obj.style.display = "block";
    		font49Obj.innerHTML = nineRowInnerHTML;
    		font49Obj.style.fontSize = bs_font_size+"pt";
    		font49Obj.face = bs_font_name;
    		font49Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font49Obj.style.fontWeight = "bold";
    		}else{
    			font49Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(tenRowInnerHTML == ""){
    		divFont50Obj.style.display = "none";
    	}else{
    		divFont50Obj.style.display = "block";
    		font50Obj.innerHTML = tenRowInnerHTML;
    		font50Obj.style.fontSize = bs_font_size+"pt";
    		font50Obj.face = bs_font_name;
    		font50Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font50Obj.style.fontWeight = "bold";
    		}else{
    			font50Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	
    	var barcode4Obj = document.getElementById("barcode4");
    	barcode4Obj.style.width = bs_width+"mm";
    	barcode4Obj.style.height = bs_height+"mm";
    	barcode4Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	barcode4Obj.style.marginTop = bs_ps_space+"mm";
    	
    	subCodeShow4Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
    	subCodeShowFont4Obj.style.fontSize = bs_font_size+"pt";
    	subCodeShowFont4Obj.face = bs_font_name;
    	subCodeShowFont4Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	if(bs_bold == "1"){
    		subCodeShowFont4Obj.style.fontWeight = "bold";
		}else{
			subCodeShowFont4Obj.style.fontWeight = "normal";
		}
    	
    	var divFont51Obj = document.getElementById("divFont51");
    	var divFont52Obj = document.getElementById("divFont52");
    	var divFont53Obj = document.getElementById("divFont53");
    	var divFont54Obj = document.getElementById("divFont54");
    	var divFont55Obj = document.getElementById("divFont55");
    	var divFont56Obj = document.getElementById("divFont56");
    	var divFont57Obj = document.getElementById("divFont57");
    	var divFont58Obj = document.getElementById("divFont58");
    	var divFont59Obj = document.getElementById("divFont59");
    	var divFont60Obj = document.getElementById("divFont60");
    	var font51Obj = document.getElementById("font51");
    	var font52Obj = document.getElementById("font52");
    	var font53Obj = document.getElementById("font53");
    	var font54Obj = document.getElementById("font54");
    	var font55Obj = document.getElementById("font55");
    	var font56Obj = document.getElementById("font56");
    	var font57Obj = document.getElementById("font57");
    	var font58Obj = document.getElementById("font58");
    	var font59Obj = document.getElementById("font59");
    	var font60Obj = document.getElementById("font60");
    	
    	divFont51Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//商品信息大小
    	divFont52Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont53Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont54Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont55Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont56Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont57Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont58Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont59Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont60Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	
    	divFont51Obj.style.marginBottom = bs_p_space+"mm";//商品信息间距
    	divFont52Obj.style.marginBottom = bs_p_space+"mm";
    	divFont53Obj.style.marginBottom = bs_p_space+"mm";
    	divFont54Obj.style.marginBottom = bs_p_space+"mm";
    	divFont55Obj.style.marginBottom = bs_p_space+"mm";
    	divFont56Obj.style.marginBottom = bs_p_space+"mm";
    	divFont57Obj.style.marginBottom = bs_p_space+"mm";
    	divFont58Obj.style.marginBottom = bs_p_space+"mm";
    	divFont59Obj.style.marginBottom = bs_p_space+"mm";
    	divFont60Obj.style.marginBottom = bs_p_space+"mm";
    	
    	divFont51Obj.style.marginTop = (parseFloat(bs_top_backgauge,10)-1)+"mm";//整页上边距
    	if(oneRowInnerHTML == ""){
    		divFont51Obj.style.display = "none";
    	}else{
    		divFont51Obj.style.display = "block";
    		font51Obj.innerHTML = oneRowInnerHTML;
    		font51Obj.style.fontSize = bs_font_size+"pt";
    		font51Obj.face = bs_font_name;
    		font51Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font51Obj.style.fontWeight = "bold";
    		}else{
    			font51Obj.style.fontWeight = "normal";
    		}
    		
    	}
    	if(twoRowInnerHTML == ""){
    		divFont52Obj.style.display = "none";
    	}else{
    		divFont52Obj.style.display = "block";
    		font52Obj.innerHTML = twoRowInnerHTML;
    		font52Obj.style.fontSize = bs_font_size+"pt";
    		font52Obj.face = bs_font_name;
    		font52Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font52Obj.style.fontWeight = "bold";
    		}else{
    			font52Obj.style.fontWeight = "normal";
    		}
    	}
    	if(threeRowInnerHTML == ""){
    		divFont53Obj.style.display = "none";
    	}else{
    		divFont53Obj.style.display = "block";
    		font53Obj.innerHTML = threeRowInnerHTML;
    		font53Obj.style.fontSize = bs_font_size+"pt";
    		font53Obj.face = bs_font_name;
    		font53Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font53Obj.style.fontWeight = "bold";
    		}else{
    			font53Obj.style.fontWeight = "normal";
    		}
    	}
    	if(fourRowInnerHTML == ""){
    		divFont54Obj.style.display = "none";
    	}else{
    		divFont54Obj.style.display = "block";
    		font54Obj.innerHTML = fourRowInnerHTML;
    		font54Obj.style.fontSize = bs_font_size+"pt";
    		font54Obj.face = bs_font_name;
    		font54Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font54Obj.style.fontWeight = "bold";
    		}else{
    			font54Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(fiveRowInnerHTML == ""){
    		divFont55Obj.style.display = "none";
    	}else{
    		divFont55Obj.style.display = "block";
    		font55Obj.innerHTML = fiveRowInnerHTML;
    		font55Obj.style.fontSize = bs_font_size+"pt";
    		font55Obj.face = bs_font_name;
    		font55Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font55Obj.style.fontWeight = "bold";
    		}else{
    			font55Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sixRowInnerHTML == ""){
    		divFont56Obj.style.display = "none";
    	}else{
    		divFont56Obj.style.display = "block";
    		font56Obj.innerHTML = sixRowInnerHTML;
    		font56Obj.style.fontSize = bs_font_size+"pt";
    		font56Obj.face = bs_font_name;
    		font56Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font56Obj.style.fontWeight = "bold";
    		}else{
    			font56Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sevenRowInnerHTML == ""){
    		divFont57Obj.style.display = "none";
    	}else{
    		divFont57Obj.style.display = "block";
    		font57Obj.innerHTML = sevenRowInnerHTML;
    		font57Obj.style.fontSize = bs_font_size+"pt";
    		font57Obj.face = bs_font_name;
    		font57Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font57Obj.style.fontWeight = "bold";
    		}else{
    			font57Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(eightRowInnerHTML == ""){
    		divFont58Obj.style.display = "none";
    	}else{
    		divFont58Obj.style.display = "block";
    		font58Obj.innerHTML = eightRowInnerHTML;
    		font58Obj.style.fontSize = bs_font_size+"pt";
    		font58Obj.face = bs_font_name;
    		font58Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font58Obj.style.fontWeight = "bold";
    		}else{
    			font58Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(nineRowInnerHTML == ""){
    		divFont59Obj.style.display = "none";
    	}else{
    		divFont59Obj.style.display = "block";
    		font59Obj.innerHTML = nineRowInnerHTML;
    		font59Obj.style.fontSize = bs_font_size+"pt";
    		font59Obj.face = bs_font_name;
    		font59Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font59Obj.style.fontWeight = "bold";
    		}else{
    			font59Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(tenRowInnerHTML == ""){
    		divFont60Obj.style.display = "none";
    	}else{
    		divFont60Obj.style.display = "block";
    		font60Obj.innerHTML = tenRowInnerHTML;
    		font60Obj.style.fontSize = bs_font_size+"pt";
    		font60Obj.face = bs_font_name;
    		font60Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font60Obj.style.fontWeight = "bold";
    		}else{
    			font60Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	var barcode5Obj = document.getElementById("barcode5");
    	barcode5Obj.style.width = bs_width+"mm";
    	barcode5Obj.style.height = bs_height+"mm";
    	barcode5Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	barcode5Obj.style.marginTop = bs_ps_space+"mm";
    	
    	subCodeShow5Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
    	subCodeShowFont5Obj.style.fontSize = bs_font_size+"pt";
    	subCodeShowFont5Obj.face = bs_font_name;
    	subCodeShowFont5Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	if(bs_bold == "1"){
    		subCodeShowFont5Obj.style.fontWeight = "bold";
		}else{
			subCodeShowFont5Obj.style.fontWeight = "normal";
		}
    	
    	var divFont61Obj = document.getElementById("divFont61");
    	var divFont62Obj = document.getElementById("divFont62");
    	var divFont63Obj = document.getElementById("divFont63");
    	var divFont64Obj = document.getElementById("divFont64");
    	var divFont65Obj = document.getElementById("divFont65");
    	var divFont66Obj = document.getElementById("divFont66");
    	var divFont67Obj = document.getElementById("divFont67");
    	var divFont68Obj = document.getElementById("divFont68");
    	var divFont69Obj = document.getElementById("divFont69");
    	var divFont70Obj = document.getElementById("divFont70");
    	var font61Obj = document.getElementById("font61");
    	var font62Obj = document.getElementById("font62");
    	var font63Obj = document.getElementById("font63");
    	var font64Obj = document.getElementById("font64");
    	var font65Obj = document.getElementById("font65");
    	var font66Obj = document.getElementById("font66");
    	var font67Obj = document.getElementById("font67");
    	var font68Obj = document.getElementById("font68");
    	var font69Obj = document.getElementById("font69");
    	var font70Obj = document.getElementById("font70");
    	
    	divFont61Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//商品信息大小
    	divFont62Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont63Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont64Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont65Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont66Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont67Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont68Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont69Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	divFont70Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";
    	
    	divFont61Obj.style.marginBottom = bs_p_space+"mm";//商品信息间距
    	divFont62Obj.style.marginBottom = bs_p_space+"mm";
    	divFont63Obj.style.marginBottom = bs_p_space+"mm";
    	divFont64Obj.style.marginBottom = bs_p_space+"mm";
    	divFont65Obj.style.marginBottom = bs_p_space+"mm";
    	divFont66Obj.style.marginBottom = bs_p_space+"mm";
    	divFont67Obj.style.marginBottom = bs_p_space+"mm";
    	divFont68Obj.style.marginBottom = bs_p_space+"mm";
    	divFont69Obj.style.marginBottom = bs_p_space+"mm";
    	divFont70Obj.style.marginBottom = bs_p_space+"mm";
    	
    	divFont61Obj.style.marginTop = (parseFloat(bs_top_backgauge,10)-1)+"mm";//整页上边距
    	if(oneRowInnerHTML == ""){
    		divFont61Obj.style.display = "none";
    	}else{
    		divFont61Obj.style.display = "block";
    		font61Obj.innerHTML = oneRowInnerHTML;
    		font61Obj.style.fontSize = bs_font_size+"pt";
    		font61Obj.face = bs_font_name;
    		font61Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font61Obj.style.fontWeight = "bold";
    		}else{
    			font61Obj.style.fontWeight = "normal";
    		}
    		
    	}
    	if(twoRowInnerHTML == ""){
    		divFont62Obj.style.display = "none";
    	}else{
    		divFont62Obj.style.display = "block";
    		font62Obj.innerHTML = twoRowInnerHTML;
    		font62Obj.style.fontSize = bs_font_size+"pt";
    		font62Obj.face = bs_font_name;
    		font62Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font62Obj.style.fontWeight = "bold";
    		}else{
    			font62Obj.style.fontWeight = "normal";
    		}
    	}
    	if(threeRowInnerHTML == ""){
    		divFont63Obj.style.display = "none";
    	}else{
    		divFont63Obj.style.display = "block";
    		font63Obj.innerHTML = threeRowInnerHTML;
    		font63Obj.style.fontSize = bs_font_size+"pt";
    		font63Obj.face = bs_font_name;
    		font63Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font63Obj.style.fontWeight = "bold";
    		}else{
    			font63Obj.style.fontWeight = "normal";
    		}
    	}
    	if(fourRowInnerHTML == ""){
    		divFont64Obj.style.display = "none";
    	}else{
    		divFont64Obj.style.display = "block";
    		font64Obj.innerHTML = fourRowInnerHTML;
    		font64Obj.style.fontSize = bs_font_size+"pt";
    		font64Obj.face = bs_font_name;
    		font64Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font64Obj.style.fontWeight = "bold";
    		}else{
    			font64Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(fiveRowInnerHTML == ""){
    		divFont65Obj.style.display = "none";
    	}else{
    		divFont65Obj.style.display = "block";
    		font65Obj.innerHTML = fiveRowInnerHTML;
    		font65Obj.style.fontSize = bs_font_size+"pt";
    		font65Obj.face = bs_font_name;
    		font65Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font65Obj.style.fontWeight = "bold";
    		}else{
    			font65Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sixRowInnerHTML == ""){
    		divFont66Obj.style.display = "none";
    	}else{
    		divFont66Obj.style.display = "block";
    		font66Obj.innerHTML = sixRowInnerHTML;
    		font66Obj.style.fontSize = bs_font_size+"pt";
    		font66Obj.face = bs_font_name;
    		font66Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font66Obj.style.fontWeight = "bold";
    		}else{
    			font66Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(sevenRowInnerHTML == ""){
    		divFont67Obj.style.display = "none";
    	}else{
    		divFont67Obj.style.display = "block";
    		font67Obj.innerHTML = sevenRowInnerHTML;
    		font67Obj.style.fontSize = bs_font_size+"pt";
    		font67Obj.face = bs_font_name;
    		font67Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font67Obj.style.fontWeight = "bold";
    		}else{
    			font67Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(eightRowInnerHTML == ""){
    		divFont68Obj.style.display = "none";
    	}else{
    		divFont68Obj.style.display = "block";
    		font68Obj.innerHTML = eightRowInnerHTML;
    		font68Obj.style.fontSize = bs_font_size+"pt";
    		font68Obj.face = bs_font_name;
    		font68Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font68Obj.style.fontWeight = "bold";
    		}else{
    			font68Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(nineRowInnerHTML == ""){
    		divFont69Obj.style.display = "none";
    	}else{
    		divFont69Obj.style.display = "block";
    		font69Obj.innerHTML = nineRowInnerHTML;
    		font69Obj.style.fontSize = bs_font_size+"pt";
    		font69Obj.face = bs_font_name;
    		font69Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font69Obj.style.fontWeight = "bold";
    		}else{
    			font69Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	if(tenRowInnerHTML == ""){
    		divFont70Obj.style.display = "none";
    	}else{
    		divFont70Obj.style.display = "block";
    		font70Obj.innerHTML = tenRowInnerHTML;
    		font70Obj.style.fontSize = bs_font_size+"pt";
    		font70Obj.face = bs_font_name;
    		font70Obj.style.paddingLeft = bs_left_backgauge+"mm";
    		if(bs_bold == "1"){
    			font70Obj.style.fontWeight = "bold";
    		}else{
    			font70Obj.style.fontWeight = "normal";
    		}
    	}
    	
    	var barcode6Obj = document.getElementById("barcode6");
    	barcode6Obj.style.width = bs_width+"mm";
    	barcode6Obj.style.height = bs_height+"mm";
    	barcode6Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	barcode6Obj.style.marginTop = bs_ps_space+"mm";
    	
    	subCodeShow6Obj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
    	subCodeShowFont6Obj.style.fontSize = bs_font_size+"pt";
    	subCodeShowFont6Obj.face = bs_font_name;
    	subCodeShowFont6Obj.style.paddingLeft = bs_left_backgauge+"mm";
    	if(bs_bold == "1"){
    		subCodeShowFont6Obj.style.fontWeight = "bold";
		}else{
			subCodeShowFont6Obj.style.fontWeight = "normal";
		}
    	
    	if(subCode_ShowUp == "1"){//上方条形码
    		subCodeShow4_UpObj.style.display = "block";
        	subCodeShow4_UpObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	subCodeShow4_UpObj.style.marginBottom = 1+"mm";
        	subCodeShowFont4_UpObj.style.fontSize = bs_font_size+"pt";
        	subCodeShowFont4_UpObj.face = bs_font_name;
        	subCodeShowFont4_UpObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		subCodeShowFont4_UpObj.style.fontWeight = "bold";
    		}else{
    			subCodeShowFont4_UpObj.style.fontWeight = "normal";
    		}
    	}else{
    		subCodeShow4_UpObj.style.display = "none";
    	}
    	
    	if(subCode_ShowUp == "1"){//上方条形码
    		subCodeShow5_UpObj.style.display = "block";
        	subCodeShow5_UpObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	subCodeShow5_UpObj.style.marginBottom = 1+"mm";
        	subCodeShowFont5_UpObj.style.fontSize = bs_font_size+"pt";
        	subCodeShowFont5_UpObj.face = bs_font_name;
        	subCodeShowFont5_UpObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		subCodeShowFont5_UpObj.style.fontWeight = "bold";
    		}else{
    			subCodeShowFont5_UpObj.style.fontWeight = "normal";
    		}
    	}else{
    		subCodeShow5_UpObj.style.display = "none";
    	}
    	
    	if(subCode_ShowUp == "1"){//上方条形码
    		subCodeShow6_UpObj.style.display = "block";
        	subCodeShow6_UpObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	subCodeShow6_UpObj.style.marginBottom = 1+"mm";
        	subCodeShowFont6_UpObj.style.fontSize = bs_font_size+"pt";
        	subCodeShowFont6_UpObj.face = bs_font_name;
        	subCodeShowFont6_UpObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		subCodeShowFont6_UpObj.style.fontWeight = "bold";
    		}else{
    			subCodeShowFont6_UpObj.style.fontWeight = "normal";
    		}
    	}else{
    		subCodeShow6_UpObj.style.display = "none";
    	}
    	
    	if(price_ShowDown == "1"){//下方价格显示
    		priceShow4_DownObj.style.display = "block";
    		priceShow4_DownObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	priceShowFont4_DownObj.style.fontSize = bs_font_size+"pt";
        	priceShowFont4_DownObj.face = bs_font_name;
        	priceShowFont4_DownObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		priceShowFont4_DownObj.style.fontWeight = "bold";
    		}else{
    			priceShowFont4_DownObj.style.fontWeight = "normal";
    		}
    	}else{
    		priceShow4_DownObj.style.display = "none";
    	}
    	
    	if(price_ShowDown == "1"){//下方价格显示
    		priceShow5_DownObj.style.display = "block";
    		priceShow5_DownObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	priceShowFont5_DownObj.style.fontSize = bs_font_size+"pt";
        	priceShowFont5_DownObj.face = bs_font_name;
        	priceShowFont5_DownObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		priceShowFont5_DownObj.style.fontWeight = "bold";
    		}else{
    			priceShowFont5_DownObj.style.fontWeight = "normal";
    		}
    	}else{
    		priceShow5_DownObj.style.display = "none";
    	}
    	
    	if(price_ShowDown == "1"){//下方价格显示
    		priceShow6_DownObj.style.display = "block";
    		priceShow6_DownObj.style.height = (parseInt(bs_font_size,10) * ptToMm)+"mm";//根据字体算高度
        	priceShowFont6_DownObj.style.fontSize = bs_font_size+"pt";
        	priceShowFont6_DownObj.face = bs_font_name;
        	priceShowFont6_DownObj.style.paddingLeft = bs_left_backgauge+"mm";
        	if(bs_bold == "1"){
        		priceShowFont6_DownObj.style.fontWeight = "bold";
    		}else{
    			priceShowFont6_DownObj.style.fontWeight = "normal";
    		}
    	}else{
    		priceShow6_DownObj.style.display = "none";
    	}
    }
}