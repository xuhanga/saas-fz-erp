var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var api = frameElement.api;
var _limitMenu = system.MENULIMIT.BASE26;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/product/barcodeplan';
var _height = api.config.height-142,_width = api.config.width-34;//获取弹出框弹出宽、高
var handle = {
	enablePlan: function(rowId){
		if(rowId != null && rowId != ''){
			$.ajax({
				type:"POST",
				url:config.BASEPATH +"base/product/enable_plan",
				data:"bs_id="+rowId,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '启动成功!'});
						THISPAGE.reloadData();
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		}else{
			Public.tips({type: 1, content : '请选择数据!'});
		}	
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH +"base/product/del_bar_plan",
					data:"bs_id="+rowId,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
		}else{
			Public.tips({type: 1, content : '请选择数据!'});
		}	
	},
	toPrintSet:function(bs_id){//打印设置
    	W.$.dialog({
    	   	id:'addPrintSet',
    	   	title:'条码打印设置',
    	   	max: false,
    	   	min: false,
    	   	width:740,
    	   	height:520,
    	   	fixed:false,
			drag: true,
    	   	content:"url:"+config.BASEPATH+"base/product/to_barcode_addPrintSet?bs_id="+bs_id,
    	   	close: function(){
    	   		THISPAGE.reloadData();
  		   	}
        });
	},
	operFmatter:function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		if(row.bs_plan_state == 0){
			html_con += '<i class="iconfont i-hand ui-icon-shenpi" title="启用">&#xe63c;</i>';
			html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		}
		html_con += '</div>';
		return html_con;
	},
	stateFmatter:function(val,opt,row){
		var _name = "";
		if(row.bs_plan_state == 0){
			_name = "未启用";
		}
		if(row.bs_plan_state == 1){
			_name = "已启用";
		}
		return _name;
	}
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate', label:'操作',width: 80, fixed:true, formatter: handle.operFmatter, title: false,sortable:false},
	    	{name: 'bs_plan_name', label:'方案名称',index: 'bs_plan_name', width: 160, title: false,sortable:false},
	    	{name: 'bs_plan_state', label:'方案状态',index: 'bs_plan_state', width: 80, title: false,sortable:false,formatter: handle.stateFmatter}
	    ];
		$('#grid').jqGrid({
			url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			viewrecords: true,
			rowNum:-1,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'bs_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	buildParams : function(){
	},
	reset:function(){
	},
	reloadData:function(data){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		//启动
		$('#grid').on('click', '.operating .ui-icon-shenpi', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.enablePlan(id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$("#btn_close").click(function(){
			api.close();
		});
		//新增
		$('#btn-add').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			e.preventDefault();
			handle.toPrintSet(0);
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.toPrintSet(id);
		});
	}
}
THISPAGE.init();