var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'base/product/list';
var handle = {
	initYear : function() {
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data : {
				items : [ {
					year_Code : "",
					year_Name : "全部"
				}, {
					year_Code : "" + parseInt(currentYear + 1) + "",
					year_Name : "" + parseInt(currentYear + 1) + ""
				}, {
					year_Code : "" + parseInt(currentYear) + "",
					year_Name : "" + parseInt(currentYear) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 1) + "",
					year_Name : "" + parseInt(currentYear - 1) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 2) + "",
					year_Name : "" + parseInt(currentYear - 2) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 3) + "",
					year_Name : "" + parseInt(currentYear - 3) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 4) + "",
					year_Name : "" + parseInt(currentYear - 4) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 5) + "",
					year_Name : "" + parseInt(currentYear - 5) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 6) + "",
					year_Name : "" + parseInt(currentYear - 6) + ""
				}, {
					year_Code : "" + parseInt(currentYear - 7) + "",
					year_Name : "" + parseInt(currentYear - 7) + ""
				} ]
			}
		}
		yearCombo = $('#span_year').combo({
			data : yearInfo.data.items,
			value : 'year_Code',
			text : 'year_Name',
			width : 206,
			height : 80,
			listId : '',
			defaultSelected : 0,
			editable : true,
			callback : {
				onChange : function(data) {
					$("#pd_year").val(data.year_Code);
				}
			}
		}).getCombo();
	},
	initDict : function() {
		seasonCombo = $('#span_pd_season_Code').combo({
			value : 'dtl_code',
			text : 'dtl_name',
			width : 206,
			height : 80,
			listId : '',
			defaultSelected : 0,
			editable : true,
			callback : {
				onChange : function(data) {
					if (data.dtl_name == "全部") {
						$("#pd_season").val("");
					} else {
						$("#pd_season").val(data.dtl_name);
					}
				}
			}
		}).getCombo();
		$.ajax({
			type : "POST",
			url : config.BASEPATH + "base/dict/listByProduct",
			data : "",
			cache : false,
			dataType : "json",
			success : function(data) {
				seasonInfo = data.data.seasonitemsquery;
				seasonCombo.loadData(seasonInfo);
			}
		});
	},
	doQueryBrand : function() {
		commonDia = W.$.dialog({
			title : '选择品牌',
			content : 'url:' + config.BASEPATH + 'base/brand/to_list_dialog',
			data : {
				multiselect : false
			},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock : true,
			ok : function() {
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close : function() {
				var selected = commonDia.content.doSelect();
				if (selected) {
					$("#pd_bd_code").val(selected.bd_code);
					$("#pd_bd_name").val(selected.bd_name);
				}
			},
			cancel : true
		});
	},
	doQueryType : function() {
		commonDia = W.$.dialog({
			title : '选择商品类别',
			content : 'url:' + config.BASEPATH + 'base/type/to_tree_dialog',
			data : {
				multiselect : true
			},
			width : 280,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock : true,
			ok : function() {
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close : function() {
				var selected = commonDia.content.doSelect();
				if (selected) {
					var pd_tp_code = [];
					var pd_tp_name = [];
					var pd_tp_upcode = [];
					for (var i = 0; i < selected.length; i++) {
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
						pd_tp_upcode.push(selected[i].tp_upcode);
					}
					$("#pd_tp_code").val(pd_tp_code.join(","));
					$("#pd_tp_name").val(pd_tp_name.join(","));
					$("#pd_tp_upcode").val(pd_tp_upcode.join(","));
				}
			},
			cancel : true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		handle.initYear();
		handle.initDict();
	},
	initGrid:function(){
		var colModel = [
		    {name: 'pd_code', label:'商品编号',index: 'pd_code', hidden:true},
			{name: 'pd_no', label:'商品货号',index: 'pd_no', width: 70, title: false},
	    	{name: 'pd_name', label:'名称',index: 'pd_name', width: 80, title: false},
	    	{name: 'pd_bd_name',label:'品牌', index: 'pd_bd_name', width: 80, title: true,sortable:true},
			{name: 'pd_tp_name',label:'类型', index: 'pd_tp_name', width: 80, title: true,sortable:true},
			{name: 'pd_season',label:'季节', index: 'pd_season', width: 40,align:'center', title: false,sortable:true},
			{name: 'pd_year',label:'年份', index: 'pd_year', width: 40, title: false,align:'center',sortable:true},
			{name: 'pd_cost_price',label:'成本价', index: 'pd_cost_price',align:'right', width: 50, title: false,sortable:true},
			{name: 'pd_sell_price',label:'零售价', index: 'pd_sell_price',align:'right', width: 50, title: false,sortable:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'pd_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					api.close();
				}
            }
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'searchContent='+$("#SearchContent").val();
		params += '&pd_no='+$("#pd_no").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&pd_season='+Public.encodeURI($("#pd_season").val());
		params += '&pd_tp_code='+$("#pd_tp_code").val();
		params += '&pd_tp_upcode='+$("#pd_tp_upcode").val();
		params += '&pd_bd_code='+$("#pd_bd_code").val();
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(event){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			$("#SearchContent").val('');
			$("#pd_no").val('');
			$("#pd_year").val('');
			yearCombo.loadData(yearInfo.data.items,[0]);
			$("#pd_season").val('');
			seasonCombo.loadData(seasonInfo,[0]);
			$("#pd_tp_name").val('');
			$("#pd_tp_code").val('');
			$("#pd_tp_upcode").val('');
			$("#pd_bd_name").val('');
			$("#pd_bd_code").val('');
		});
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			Public.tips({type: 2, content : "请选择商品！"});
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			Public.tips({type: 2, content : "请选择商品！"});
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();