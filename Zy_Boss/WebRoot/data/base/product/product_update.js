var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:false},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_bd_code").val(selected.bd_code);
					$("#pd_bd_name").val(selected.bd_name);
				}
			},
			cancel:true
		});
	},
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货单位',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 400,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_sp_code").val(selected.sp_code);
					$("#pd_sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:false},
			width : 340,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_tp_code").val(selected.tp_code);
					$("#pd_tp_name").val(selected.tp_name);
					$("#pd_tp_upcode").val(selected.tp_upcode);
				}
			},
			cancel:true
		});
	},
    addColor : function(){//添加颜色
    	$.dialog({ 
			title: '添加颜色',
			max: false,
			min: false,
			width: 250,
			height: 200,
			fixed:false,
			drag: true,
			content : 'url:'+config.BASEPATH+'base/color/to_add_buyProduct',
			close: function () {
				handle.showColor();
		   	}
		});
	},
	doQueryBra:function(){
		var pd_bra = $('#pd_bra').val();
		if(pd_bra == 0){
			Public.tips({type: 2, content : "请选择文胸为是！"});
			return;
		}
		commonDia = $.dialog({
			title : '选择杯型',
			content : 'url:'+config.BASEPATH+'base/bra/to_list_dialog',
			data : {multiselect:true},
			width : 400,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var codes = [];
					var names = [];
					for(var i=0;i<selected.length;i++){
						codes.push(selected[i].br_code);
						names.push(selected[i].br_name);
					}
					handle.braOptionChange(codes,names);
				}
			},
			cancel:true
		});
	},
	doQuerySzg:function(){
		commonDia = $.dialog({
			title : '选择尺码组',
			content : 'url:'+config.BASEPATH+'base/size/to_list_group_dialog',
			data : {multiselect:false},
			width : 580,
			height : 470,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pd_szg_code").val(selected.szg_code);
					$("#pd_szg_name").val(selected.szg_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
//		if (!$("#form1").valid()) {
//			return;
//		}
		var color_select = document.getElementById('color_select').length;
		if(color_select<=0){
			Public.tips({type: 2, content : "请选择颜色！"});
		    return;
		}
		//获取商品对应颜色codes列表
		var select_color_codes = "";
		var color_list = form1.color_select.options;
		for(i=0;i<color_list.length;i++){
			select_color_codes += color_list.item(i).value+",";
		}
		select_color_codes = select_color_codes.substring(0,select_color_codes.length-1);
		form1.pdc_cr_codes.value = select_color_codes;
		
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/product/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '商品资料修改成功！'});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	changeInPrice:function(){//进价折扣
		var pd_sell_price = document.getElementById("pd_sell_price").value;
		var discount = document.getElementById("discount").value;
		document.getElementById("pd_buy_price").value=(pd_sell_price * discount).toFixed(2);
	 	document.getElementById("pd_cost_price").value=(pd_sell_price * discount).toFixed(2);
	},
	changeBatchPrice:function(){//批发折扣
		var pd_sell_price = document.getElementById("pd_sell_price").value;
	 	var BatchDiscount = document.getElementById("BatchDiscount").value;
	 	document.getElementById("pd_batch_price").value = (pd_sell_price * BatchDiscount).toFixed(2);
	 	document.getElementById("pd_batch_price1").value = (pd_sell_price * BatchDiscount).toFixed(2);
	},
	changeSorthPrice:function(){//配送折扣
		var pd_sell_price = document.getElementById("pd_sell_price").value;
	 	var sort_discount = document.getElementById("sort_discount").value;
	 	document.getElementById("pd_sort_price").value = (pd_sell_price * sort_discount).toFixed(2);
	},
	changePrice:function(){
		var erg = /^[0-9]+([.]{1}[0-9]{1,8})?$/;
		var discount = $("#discount").val();
		var pds_sign_price = $("#pd_sign_price").val();
		if(!erg.test(pds_sign_price)){
			pds_sign_price = 0;
		}
		var disPrice = parseFloat(pds_sign_price*discount).toFixed(2);
		$("#pd_cost_price").val(disPrice);
		$("#pd_sell_price").val(parseFloat(pds_sign_price));
		$("#pd_vip_price").val(pds_sign_price);
		$("#pd_buy_price").val(parseFloat(disPrice));
		
		var BatchDiscount = document.getElementById("BatchDiscount").value;
		if(BatchDiscount != "" && !isNaN(BatchDiscount)){//更新批发价
			document.getElementById("pd_batch_price").value = (parseFloat(pds_sign_price) * BatchDiscount).toFixed(2);
			document.getElementById("pd_batch_price1").value = (parseFloat(pds_sign_price) * BatchDiscount).toFixed(2);
		}
	},
	changeRetailPrice:function(){
		var pd_sell_price = document.getElementById("pd_sell_price").value;
		var discount = document.getElementById("discount").value;
		document.getElementById("pd_vip_price").value = pd_sell_price;
		document.getElementById("pd_cost_price").value = (pd_sell_price * discount).toFixed(2);
		document.getElementById("pd_buy_price").value = (pd_sell_price * discount).toFixed(2);
		var BatchDiscount = document.getElementById("BatchDiscount").value;
		if(BatchDiscount != "" && !isNaN(BatchDiscount)){//更新批发价
			document.getElementById("pd_batch_price").value = (pd_sell_price * BatchDiscount).toFixed(2);
			document.getElementById("pd_batch_price1").value = (pd_sell_price * BatchDiscount).toFixed(2);
		}
	},
	showSize:function(code){//显示尺码
		var szg_code = code;
		if(szg_code.length > 0){
			$.ajax({
				type:"get",
				url:config.BASEPATH +"base/size/sizeListByProduct",
				data:"szg_code="+szg_code,
				cache:false,
				dataType:"json",
				success:function(data){
					if(data.size_html != undefined){
						$("#size_list").html(data.size_html);
					}else{
						$("#size_list").html("");
					}
				}
			});
		}else{
			$("#size_list").html("");
		}
	},
	showColor:function(){//显示颜色
		var colorSearch = $("#colorSearch").val();
		colorSearch = encodeURI(encodeURI(colorSearch));
		var hadSelect_codes = "";
		var color_list = form1.right_item.options;
		for(i=0;i<color_list.length;i++){
			hadSelect_codes += color_list.item(i).value+",";
		}
		hadSelect_codes = hadSelect_codes.substring(0,hadSelect_codes.length-1);
		$.ajax({
			type:"get",
			url:config.BASEPATH +"base/color/colorListByProduct",
			data:"colorSearch="+colorSearch+"&colorCodes="+hadSelect_codes,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data.color_html != undefined){
					$("#color_list").html(data.color_html);
				}else{
					$("#color_list").html("");
				}
			}
		});
	},
	colorOptionChange:function(source,target,pd_code) {
		var right_item = form1.right_item;
		var cr_code = right_item.value;
		if(cr_code == ""){
		 		return;
		}
	    $.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/checkOptionChange",
			data:"codes="+cr_code+"&optionType=color&pd_code="+pd_code,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == "sucess"){
						change(source,target);
					}else{
						Public.tips({type: 2, content : "所选颜色已存在业务关联，不能去除！"});
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	colorOptionChangeAll:function(source,target,pd_code) {
		var hadSelect_codes = "";
		var right_item = form1.right_item.options;
		for(i=0;i<right_item.length;i++){
			hadSelect_codes += right_item.item(i).value+",";
		}
		hadSelect_codes = hadSelect_codes.substring(0,hadSelect_codes.length-1);
		var cr_codes = hadSelect_codes;
		if(cr_codes == ""){
		 	return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/checkOptionChange",
			data:"codes="+cr_codes+"&optionType=color&pd_code="+pd_code,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == "sucess"){
						changeAll(source,target);
					}else{
						Public.tips({type: 2, content : "所选颜色已存在业务关联，不能去除！"});
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	sizeOptionChange:function(pd_szg_code,pd_code){//修改尺码组验证
		if(pd_szg_code == ""){
		 	return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/checkOptionChange",
			data:"codes="+pd_szg_code+"&optionType=size&pd_code="+pd_code,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == "sucess"){
						Utils.doQuerySzg();
					}else{
						Public.tips({type: 2, content : "尺码组存在业务关联，不能修改！"});
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	braOptionChange:function(br_codes,br_name){//修改尺码组验证
		if(br_codes.length == 0){
		 	return;
		}
		var pdb_br_codes = $("#pdb_br_codes_bak").val();
		if(pdb_br_codes == ""){
			$("#pdb_br_codes").val(br_codes.join(","));
			$("#pdb_br_name").val(br_name.join(","));
		 	return;
		}
		var pdb_br_codeList = pdb_br_codes.split(",");
		
		var br_codes_all = [];
		var len = 0;
		for(var i=0;i<pdb_br_codeList.length;i++){
			for(var j=0;j<br_codes.length;j++){
				if(pdb_br_codeList[i] == br_codes[j]){
					break;
				}
				if(j == br_codes.length-1){
					br_codes_all[len] = pdb_br_codeList[i];
					len ++;
				}
			}
		}
		if(br_codes_all.length == 0){
			$("#pdb_br_codes").val(br_codes.join(","));
			$("#pdb_br_name").val(br_name.join(","));
		 	return;
		}
		var pd_code = $("#pd_code").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/checkOptionChange",
			data:"codes="+br_codes_all.join(",")+"&optionType=bra&pd_code="+pd_code,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == "sucess"){
						$("#pdb_br_codes").val(br_codes.join(","));
						$("#pdb_br_name").val(br_name.join(","));
					}else{
						Public.tips({type: 2, content : "部分杯型存在业务关联，不能删除！"});
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	}
};

var comboOpts = {
	value : 'Code',
	text : 'Name',
	width : 142,
	height : 300,
	listHeight : 300,
	listId : '',
	defaultSelected : 0,
	editable : false
};

var ComboUtil = {
	dictCombo : function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			cache:false,
			dataType:"json",
			success:function(data){
				//款式
				var styleInfo = {};
				var styleSelect = $("#pd_style").val();
				var indexStyle = 0,_style_name="";
				for(var key in data.data.styleitems){
					if(data.data.styleitems[key].dtl_name ==  styleSelect){
						indexStyle = key;
						_style_name = data.data.styleitems[key].dtl_name;
						break;
					}
				}
				styleCombo = $('#span_style').combo({
					data:data.data.styleitems,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: indexStyle,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							styleInfo = data.data.styleitems;
							return data.data.styleitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_style").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//季节
				var seasonInfo = {};
				var seasonSelect = $("#pd_season").val();
				var indexSeason = 0,_season_name="";
				for(var key in data.data.seasonitems){
					if(data.data.seasonitems[key].dtl_name ==  seasonSelect){
						indexSeason = key;
						_season_name = data.data.seasonitems[key].dtl_name;
						break;
					}
				}
				seasonCombo = $('#span_season').combo({
					data:data.data.seasonitems,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: indexSeason,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							seasonInfo = data.data.seasonitems;
							return data.data.seasonitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_season").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//单位
				var unitInfo = {};
				var unitSelect = $("#pd_unit").val();
				var indexUnit = 0,_unit_name="";
				for(var key in data.data.unitnameitems){
					if(data.data.unitnameitems[key].dtl_name ==  unitSelect){
						indexUnit = key;
						_unit_name = data.data.unitnameitems[key].dtl_name;
						break;
					}
				}
				unitCombo = $('#span_unit').combo({
					data:data.data.unitnameitems,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: indexUnit,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							unitInfo = data.data.unitnameitems;
							return data.data.unitnameitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_unit").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//面料
				var stuffInfo = {};
				var stuffSelect = $("#pd_fabric").val();
				var indexStuff = 0,_stuff_name="";
				for(var key in data.data.stuffitems){
					if(data.data.stuffitems[key].dtl_name ==  stuffSelect){
						indexStuff = key;
						_stuff_name = data.data.stuffitems[key].dtl_name;
						break;
					}
				}
				stuffCombo = $('#span_fabric').combo({
					data:data.data.stuffitems,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: indexStuff,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							stuffInfo = data.data.stuffitems;
							return data.data.stuffitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_fabric").val(data.dtl_name);
						}
					}
				}).getCombo(); 
				
				//价格特性
				var priceInfo = {};
				var priceSelect = $("#pd_price_name").val();
				var indexPrice = 0,_price_name="";
				for(var key in data.data.priceitems){
					if(data.data.priceitems[key].dtl_name ==  priceSelect){
						indexPrice = key;
						_price_name = data.data.priceitems[key].dtl_name;
						break;
					}
				}
				priceCombo = $('#span_price').combo({
					data:data.data.priceitems,
					value: 'dtl_code',
					text: 'dtl_name',
					width : 142,
					height : 80,
					listId:'',
					defaultSelected: indexPrice,
					editable: true,
					ajaxOptions:{
						formatData: function(data){
							priceInfo = data.data.priceitems;
							return data.data.priceitems;
						}
					},
					callback:{
						onChange: function(data){
							$("#pd_price_name").val(data.dtl_name);
						}
					}
				}).getCombo(); 
			}
		 });
	},
	yearCombo : function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		var yearInfo = {
			data:{items:[
					{year_Code:""+parseInt(currentYear-7)+"",year_Name:""+parseInt(currentYear-7)+""},
					{year_Code:""+parseInt(currentYear-6)+"",year_Name:""+parseInt(currentYear-6)+""},
					{year_Code:""+parseInt(currentYear-5)+"",year_Name:""+parseInt(currentYear-5)+""},
					{year_Code:""+parseInt(currentYear-4)+"",year_Name:""+parseInt(currentYear-4)+""},
					{year_Code:""+parseInt(currentYear-3)+"",year_Name:""+parseInt(currentYear-3)+""},
					{year_Code:""+parseInt(currentYear-2)+"",year_Name:""+parseInt(currentYear-2)+""},
					{year_Code:""+parseInt(currentYear-1)+"",year_Name:""+parseInt(currentYear-1)+""},
					{year_Code:""+parseInt(currentYear)+"",year_Name:""+parseInt(currentYear)+""},
					{year_Code:""+parseInt(currentYear+1)+"",year_Name:""+parseInt(currentYear+1)+""},
					{year_Code:""+parseInt(currentYear+2)+"",year_Name:""+parseInt(currentYear+2)+""},
					{year_Code:""+parseInt(currentYear+3)+"",year_Name:""+parseInt(currentYear+3)+""},
					{year_Code:""+parseInt(currentYear+4)+"",year_Name:""+parseInt(currentYear+4)+""},
					{year_Code:""+parseInt(currentYear+5)+"",year_Name:""+parseInt(currentYear+5)+""},
					{year_Code:""+parseInt(currentYear+6)+"",year_Name:""+parseInt(currentYear+6)+""},
					{year_Code:""+parseInt(currentYear+7)+"",year_Name:""+parseInt(currentYear+7)+""}
				]}
		}
		var selectSize = $("#pd_year").val();
		var index = 0,_year="";
		 for(var key in yearInfo.data.items){
			if(yearInfo.data.items[key].year_Name ==  selectSize){
				index = key;
				_year = yearInfo.data.items[key].year_Name;
				break;
			}
		 }
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'year_Code',
			text: 'year_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.year_Code);
				}
			}
		}).getCombo();
	},
	isbraCombo : function(){
		var isCheck = false;
		var isbraInfo = {
				data:{items:[
					{isbra_Code:'0',isbra_Name:'否'},
					{isbra_Code:'1',isbra_Name:'是'},
					]}
		}
		var selectIsbra = $("#pd_bra").val();
		var index = 0,_isbra="";
		 for(var key in isbraInfo.data.items){
			if(isbraInfo.data.items[key].isbra_Code ==  selectIsbra){
				index = key;
				_isbra = isbraInfo.data.items[key].isbra_Name;
				break;
			}
		 }
		isbraCombo = $('#span_bra').combo({
			data:isbraInfo.data.items,
			value: 'isbra_Code',
			text: 'isbra_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					if(isCheck){
						//更新杯型判断是否存在数据关联
						var pdb_br_codes = $("#pdb_br_codes").val();
						var pd_code = $("#pd_code").val();
						if(pdb_br_codes != ""){
							$.ajax({
								type:"POST",
								url:config.BASEPATH +"base/product/checkOptionChange",
								data:"codes="+pdb_br_codes+"&optionType=bra&pd_code="+pd_code,
								cache:false,
								dataType:"json",
								success:function(data_ajax){
									if(undefined != data_ajax && data_ajax.stat == 200){
										if(data_ajax.data == "sucess"){
											$("#pd_bra").val(data.isbra_Code);
											if(data.isbra_Code == '0'){
												$("#pdb_br_codes").val('');
												$("#pdb_br_name").val('');
											}
										}else{
											Public.tips({type: 2, content : "原杯型已存在业务关联，不能去除！"});
										}
									}else{
										Public.tips({type: 1, content : data_ajax.message});
									}
								}
							});
						}else{
							$("#pd_bra").val(data.isbra_Code);
							if(data.isbra_Code == '0'){
								$("#pdb_br_codes").val('');
								$("#pdb_br_name").val('');
							}
						}
					}else{
						isCheck = true;
						$("#pd_bra").val(data.isbra_Code);
						if(data.isbra_Code == '0'){
							$("#pdb_br_codes").val('');
							$("#pdb_br_name").val('');
						}
					}
				}
			}
		}).getCombo();
	},
	isChangeCombo : function(){
		var isChangeInfo = {
				data:{items:[
					{isChange_Code:'0',isChange_Name:'否'},
					{isChange_Code:'1',isChange_Name:'是'},
					]}
		}
		var selectChange = $("#pd_change").val();
		var index = 0,_isbra="";
		 for(var key in isChangeInfo.data.items){
			if(isChangeInfo.data.items[key].isChange_Code ==  selectChange){
				index = key;
				_isbra = isChangeInfo.data.items[key].isChange_Name;
				break;
			}
		 }
		isChangeCombo = $('#span_change').combo({
			data:isChangeInfo.data.items,
			value: 'isChange_Code',
			text: 'isChange_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_change").val(data.isChange_Code);
				}
			}
		}).getCombo();
	},
	isBuyCombo : function(){
		var isBuyInfo = {
				data:{items:[
					{isBuy_Code:'0',isBuy_Name:'否'},
					{isBuy_Code:'1',isBuy_Name:'是'},
					]}
		}
		var selectChange = $("#pd_buy").val();
		var index = 0,_buy="";
		 for(var key in isBuyInfo.data.items){
			if(isBuyInfo.data.items[key].isBuy_Code ==  selectChange){
				index = key;
				_buy = isBuyInfo.data.items[key].isBuy_Name;
				break;
			}
		 }
		isBuyCombo = $('#span_buy').combo({
			data:isBuyInfo.data.items,
			value: 'isBuy_Code',
			text: 'isBuy_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_buy").val(data.isBuy_Code);
				}
			}
		}).getCombo();
	},
	isVipSaleCombo : function(){
		var isVipSaleInfo = {
				data:{items:[
					{isVipSale_Code:'0',isVipSale_Name:'否'},
					{isVipSale_Code:'1',isVipSale_Name:'是'},
					]}
		}
		var selectChange = $("#pd_vip_sale").val();
		var index = 0,_vip_sale="";
		 for(var key in isVipSaleInfo.data.items){
			if(isVipSaleInfo.data.items[key].isVipSale_Code ==  selectChange){
				index = key;
				_vip_sale = isVipSaleInfo.data.items[key].isVipSale_Name;
				break;
			}
		 }
		isVipSaleCombo = $('#span_vip_sale').combo({
			data:isVipSaleInfo.data.items,
			value: 'isVipSale_Code',
			text: 'isVipSale_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_vip_sale").val(data.isVipSale_Code);
				}
			}
		}).getCombo();
	},
	isPresentCombo : function(){
		var isPresentInfo = {
				data:{items:[
					{isPresent_Code:'0',isPresent_Name:'否'},
					{isPresent_Code:'1',isPresent_Name:'是'},
					]}
		}
		var selectChange = $("#pd_present").val();
		var index = 0,_present="";
		 for(var key in isPresentInfo.data.items){
			if(isPresentInfo.data.items[key].isPresent_Code ==  selectChange){
				index = key;
				_present = isPresentInfo.data.items[key].isPresent_Name;
				break;
			}
		 }
		isPresentCombo = $('#span_present').combo({
			data:isPresentInfo.data.items,
			value: 'isPresent_Code',
			text: 'isPresent_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_present").val(data.isPresent_Code);
				}
			}
		}).getCombo();
	},
	isGiftCombo : function(){
		var isGiftInfo = {
				data:{items:[
					{isGift_Code:'0',isGift_Name:'否'},
					{isGift_Code:'1',isGift_Name:'是'},
					]}
		}
		var selectChange = $("#pd_gift").val();
		var index = 0,_gift="";
		 for(var key in isGiftInfo.data.items){
			if(isGiftInfo.data.items[key].isGift_Code ==  selectChange){
				index = key;
				_gift = isGiftInfo.data.items[key].isGift_Name;
				break;
			}
		 }
		isGiftCombo = $('#span_gift').combo({
			data:isGiftInfo.data.items,
			value: 'isGift_Code',
			text: 'isGift_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_gift").val(data.isGift_Code);
				}
			}
		}).getCombo();
	},
	isPointCombo : function(){
		var isPointInfo = {
				data:{items:[
					{isPoint_Code:'0',isPoint_Name:'否'},
					{isPoint_Code:'1',isPoint_Name:'是'},
					]}
		}
		var selectChange = $("#pd_point").val();
		var index = 0,_point="";
		 for(var key in isPointInfo.data.items){
			if(isPointInfo.data.items[key].isPoint_Code ==  selectChange){
				index = key;
				_point = isPointInfo.data.items[key].isPoint_Name;
				break;
			}
		 }
		isPointCombo = $('#span_point').combo({
			data:isPointInfo.data.items,
			value: 'isPoint_Code',
			text: 'isPoint_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_point").val(data.isPoint_Code);
				}
			}
		}).getCombo();
	},
	isSaleCombo : function(){
		var isSaleInfo = {
				data:{items:[
					{isSale_Code:'0',isSale_Name:'否'},
					{isSale_Code:'1',isSale_Name:'是'},
					]}
		}
		var selectChange = $("#pd_sale").val();
		var index = 0,_sale="";
		 for(var key in isSaleInfo.data.items){
			if(isSaleInfo.data.items[key].isSale_Code ==  selectChange){
				index = key;
				_sale = isSaleInfo.data.items[key].isSale_Name;
				break;
			}
		 }
		isSaleCombo = $('#span_sale').combo({
			data:isSaleInfo.data.items,
			value: 'isSale_Code',
			text: 'isSale_Name',
			width : 142,
			height : 80,
			listId:'',
			defaultSelected: index,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_sale").val(data.isSale_Code);
				}
			}
		}).getCombo();
	}
//	sizeGroupCombo : function(){
//		var sizeGroupInfo = {};
//		sizeGroupCombo = $('#span_size_group').combo({
//			data:config.BASEPATH +"base/size/listGroupByProduct",
//			value: 'szg_code',
//			text: 'szg_name',
//			width :80,
//			height: 30,
//			listId:'',
//			defaultSelected: 0,
//			editable: true,
//			ajaxOptions:{
//				formatData: function(data){
//					sizeGroupInfo = data.data.items;
//					return data.data.items;
//				}
//			},
//			callback:{
//				onChange: function(data){
//					$("#pd_szg_code").val(data.szg_code);
//					handle.showSize(data.szg_code);
//				}
//			}
//		}).getCombo(); 
//	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initDate();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		ComboUtil.dictCombo();
		ComboUtil.yearCombo();
		//ComboUtil.sizeGroupCombo();
		ComboUtil.isbraCombo();
		ComboUtil.isChangeCombo();
		ComboUtil.isBuyCombo();
		ComboUtil.isVipSaleCombo();
		ComboUtil.isPresentCombo();
		ComboUtil.isGiftCombo();
		ComboUtil.isPointCombo();
		ComboUtil.isSaleCombo();
		handle.showColor();
		//handle.showBra();
	},
	initDate:function(){
		$("#pd_sign_price").val(PriceLimit.formatBySell($("#pd_sign_price_hid").val()));
		$("#pd_vip_price").val(PriceLimit.formatByVip($("#pd_vip_price_hid").val()));
		$("#pd_sell_price").val(PriceLimit.formatBySell($("#pd_sell_price_hid").val()));
		$("#pd_cost_price").val(PriceLimit.formatByCost($("#pd_cost_price_hid").val()));
		$("#pd_buy_price").val(PriceLimit.formatByEnter($("#pd_buy_price_hid").val()));
		$("#pd_sort_price").val(PriceLimit.formatByDistribute($("#pd_sort_price_hid").val()));
		$("#pd_batch_price").val(PriceLimit.formatByBatch($("#pd_batch_price_hid").val()));
		$("#pd_batch_price1").val(PriceLimit.formatByBatch($("#pd_batch_price1_hid").val()));
		$("#pd_batch_price2").val(PriceLimit.formatByBatch($("#pd_batch_price2_hid").val()));
		$("#pd_batch_price3").val(PriceLimit.formatByBatch($("#pd_batch_price3_hid").val()));
		if("***" != $("#pd_sell_price").val()){
			$("#discount").val(parseFloat(($("#pd_buy_price").val())/parseFloat($("#pd_sell_price").val())).toFixed(2));
			$("#BatchDiscount").val((parseFloat($("#pd_batch_price").val())/parseFloat($("#pd_sell_price").val())).toFixed(2));
			$("#sort_discount").val(parseFloat(($("#pd_sort_price").val())/parseFloat($("#pd_sell_price").val())).toFixed(2));
		}else{
			$("#discount").val("***");
			$("#BatchDiscount").val("***");
			$("#sort_discount").val("***");
		}
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();