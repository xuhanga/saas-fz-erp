var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;

$("#img_logo").uploadPreview({ 
	Img: "imgview", 
	Width: 320, 
	Height: 160,
	Callback:function(){
	}
});

var Utils = {
	save:function(obj){
		$(obj).attr("disabled",true);
		document.forms['form1'].encoding="multipart/form-data";
		$("#form1").ajaxSubmit({
			type:"POST",
			timeout:20000,
			url:config.BASEPATH+"base/shopline/save",
			cache:false,
			dataType:"json",
			success:function(data){
			    if(undefined != data && data.stat == 200){
				 	Public.tips({type: 3, content : "修改成功！！"});
				 	$("#spl_id").val($.trim(data.data.spl_id));
				}else{
					Public.tips({type: 1, content : data.message});
				}
			    $(obj).attr("disabled",false);
			}
		 });
	}
};


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();