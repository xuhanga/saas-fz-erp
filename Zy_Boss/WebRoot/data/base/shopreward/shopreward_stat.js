var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var _height = api.config.height-88,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'base/shopreward/statByShop?sp_code='+api.data.sp_code;

var handle = {
	formatIcon : function(val, opt, row){
		var html_con = '';
		html_con += '<div class="icons">';
		html_con += '<i class="iconfont '+row.rw_style+'">'+row.rw_icon+'</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
			{label:'图标',name: 'icon',width: 100, formatter: handle.formatIcon,align:'center',sortable:false},
			{label:'编号',name: 'sr_rw_code', index: 'sr_rw_code', width:80, hidden:true},
			{label:'名称',name: 'rw_name', index: 'rw_name', width: 140},
			{label:'个数',name: 'reward_count', index: 'reward_count', width: 80,align:'center'}
	    ];
		$('#grid').jqGrid({
			url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'sr_rw_code'
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();