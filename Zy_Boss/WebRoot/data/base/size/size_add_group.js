var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var szg_code = api.data.szg_code;
var handle = {
	initParam:function(){
		var rightItem = form1.right_item.options;
		var sz_codes = [];
		for(i=0;i<rightItem.length;i++){
			var param = {};
			param.sz_code = rightItem.item(i).value;
			sz_codes.push(param);
		}
		return sz_codes;
	},
	save:function(){
		var name = $("#szg_name").val().trim();
	    if(name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#szg_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#szg_name").val(name);//防止空格
	    }
	    var codes = handle.initParam();
	    if(null == codes || codes.length == 0){
	    	W.Public.tips({type: 1, content : "请选择尺码!"});
	    	return;
	    }
	    var pdata = {};
	    pdata.szg_name = name;
	    pdata.data = codes;
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/size/saveGroup",
			data:{"jsondata":JSON.stringify(pdata)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 1, content : data.message});
					handle.loadData(1);
					setTimeout("api.close()",500);
				}else{
					W.Public.tips({type: 1, content : "保存失败！"});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(flag){
		if(callback && typeof callback == 'function'){
			callback(flag,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
		var sz_name = $("#sz_name").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/size/list",
			data:{"sz_name":sz_name},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var html = "";
					for(key in data.data){
						html += '<option value="'+data.data[key].sz_code+'">'+data.data[key].sz_name+'</option>';
					}
					$("#left_item").html(html);
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	initDom:function(){
		$("#szg_name").val("").focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$("#sz_name").on('keyup',function(e){
			THISPAGE.initParam();
		});
	}
};
THISPAGE.init();