var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BASE15;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'base/type/list';
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 240;
		var width = 335;
		var selected = window.parent.frames["treeFrame"].selectedTreeNode;
		if(oper == 'add'){
			if(selected == undefined){
				$.dialog.tips("请选择左侧父类!",2,"32X32/fail.png");
				return false;
			}
			title = '新增商品类别';
			url = config.BASEPATH+"base/type/to_add";
			data = {oper: oper, callback: this.callback};
			if(selected != undefined){
				data.tp_upcode = selected.id;
			}
		}else if(oper == 'edit'){
			title = '修改商品类别';
			url = config.BASEPATH+"base/type/to_update?tp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'base/type/del',
					data:{"tp_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', rowIds);
							window.parent.frames["treeFrame"].location.reload(); 
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
			window.parent.frames["treeFrame"].location.reload(); 
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initParam:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate',label:'操作',width: 70, fixed:true, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
			{name: 'tp_code',label:'分类编号', index:'tp_code', width: 100, title: false,fixed:true},
			{name: 'tp_name',label:'分类名称', index: 'tp_name', width: 120, title: false},
			{name: 'tp_spell',label:'拼音简码', index: 'tp_spell', width: 120, title: false},
			{name: 'tp_rate',label:'导购员提成率', index: 'tp_rate', width: 100, title: false},
			{name: 'tp_salerate',label:'业务员提成率', index: 'tp_salerate', width: 100, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			page:1,
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'tp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("tp_upcode") && data.tp_upcode != ''){
				param += "&tp_upcode="+data.tp_upcode;
			}
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$_matchCon = $('#SearchContent');
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			var searchContent = $.trim($_matchCon.val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			};
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			};
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();