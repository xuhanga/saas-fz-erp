var config = parent.parent.parent.CONFIG,system=parent.parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var Utils = {
	errorTips:function(tip){
		W.Public.tips({type: 1, content : tip});
	},
	save:function(){
		var tp_name = $("#tp_name").val();
		var tp_spell = $("#tp_spell").val();
		var tp_rate = $("#tp_rate").val();
		var tp_salerate = $("#tp_salerate").val();
		
		if(tp_name == ""){
			this.errorTips("请输入分类名称");
			$("#tp_name").select().focus();
			return;
		}else{
	    	$("#tp_name").val(tp_name);//防止空格
	    }
		if(tp_rate == ""){
			$("#tp_rate").val("0");
		}
		if(tp_salerate == ""){
			$("#tp_salerate").val("0");
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/type/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					Utils.loadData(data.data);
					setTimeout("api.zindex()",500);
					THISPAGE.initDom();
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.tp_id;
		pdata.tp_name=data.tp_name;
		pdata.tp_spell=data.tp_spell;
		pdata.tp_rate=data.tp_rate;
		pdata.tp_salerate=data.tp_salerate;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#tp_name").select().focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			Utils.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();

function checkDiscount(){
	var tp_rate = document.getElementById("tp_rate").value;
	var tp_salerate = document.getElementById("tp_salerate").value;
	if(tp_rate<0 || tp_rate>1){
		Utils.errorTips("请输入0-1之间的数据！");
	    document.getElementById("tp_rate").value="0";
		document.getElementById("tp_rate").focus();
		setTimeout("api.zindex()",400);	
	    return false;
	}
	if(isNaN(tp_rate)){
		Utils.errorTips("请输入数字！");
	    document.getElementById("tp_rate").value="0";
		document.getElementById("tp_rate").focus();
		setTimeout("api.zindex()",400);	
	    return false;
	}
	if(tp_salerate<0 || tp_salerate>1){
		Utils.errorTips("请输入0-1之间的数据！");
	    document.getElementById("tp_salerate").value="0";
		document.getElementById("tp_salerate").focus();
		setTimeout("api.zindex()",400);	
	    return false;
	}
	if(isNaN(tp_salerate)){
		Utils.errorTips("请输入数字！");
		document.getElementById("tp_salerate").value="0";
		document.getElementById("tp_salerate").focus();
		setTimeout("api.zindex()",400);	
        return false;
     }
}