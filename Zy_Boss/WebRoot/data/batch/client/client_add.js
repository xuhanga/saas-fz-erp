var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	doQueryArea:function(){
		commonDia = $.dialog({
			title : '选择区域',
			content : 'url:'+config.BASEPATH+'base/area/to_tree_dialog',
			data : {multiselect:false},
			width : 280,
		   	height : 260,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					Public.tips({type: 2, content : '请选择区域!'});
					return false;
				}
				$("#ar_name").val(selected.ar_name);
				$("#ci_area").val(selected.ar_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ar_name").val(selected.ar_name);
					$("#ci_area").val(selected.ar_code);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		var saveUrl = config.BASEPATH+"batch/client/save";
		if($("#ci_id").val() != undefined){
			saveUrl = config.BASEPATH+"batch/client/update";
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ci_id;
		pdata.ci_code=data.ci_code;
		pdata.ci_name=data.ci_name;
		pdata.ci_man=data.ci_man;
		pdata.ci_tel=data.ci_tel;
		pdata.ci_mobile=data.ci_mobile;
		pdata.ci_rate=data.ci_rate;
		pdata.ci_batch_cycle=data.ci_batch_cycle;
		pdata.ci_settle_cycle=data.ci_settle_cycle;
		pdata.ci_earnest=data.ci_earnest;
		pdata.ci_deposit=data.ci_deposit;
		pdata.ci_addr=data.ci_addr;
		pdata.ci_remark=data.ci_remark;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#ci_name").focus();
		this.$_use_credit = $("#use_credit").cssRadio({ callback: function($_obj){
			var ci_use_credit = $_obj.find("input").val();
			$("#ci_use_credit").val(ci_use_credit);
			if(ci_use_credit == 0){
				$("#ci_credit_limit").val("");
				$("#ci_credit_limit").prop("disabled",true);
			}else if(ci_use_credit == 1){
				$("#ci_credit_limit").prop("disabled",false);
			}
		}});
		this.initCiDeault()
	},
	initCiDeault:function(){
		var data = [ 
            {Code : '0',Name : '批发价'}, 
            {Code : '1',Name : '批发价1'},
            {Code : '2',Name : '批发价2'},
            {Code : '3',Name : '批发价3'}
        ];
		$('#span_ci_default').combo({
			value : 'Code',
			text : 'Name',
			width : 207,
			listHeight : 300,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#ci_default").val(data.Code);
				}
			}
		}).getCombo().loadData(data,["Code",$("#ci_default").val(),0]);
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();