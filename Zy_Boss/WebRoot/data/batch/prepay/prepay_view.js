var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+$("#pp_number").val();
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"batch/prepay/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	doReverse:function(){
		$.dialog.confirm('确定要反审核单据吗？', function(){
			$("#btn-reverse").attr("disabled",true);
			var params = "";
			params += "number="+$("#pp_number").val();
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"batch/prepay/reverse",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "反审核成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reverse").attr("disabled",false);
	            }
	        });
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.pp_id;
		pdata.pp_number=data.pp_number;
		pdata.pp_ar_state=data.pp_ar_state;
		pdata.pp_ar_date=data.pp_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	doPrint:function(){
        LODOP.PRINT_INITA(0, 0, 1240, 600, "打印机");
        LODOP.SET_PRINT_PAGESIZE(2, 0, 0, "");
        LODOP.SET_PRINT_STYLEA(0, "Vorient", 3);
        LODOP.ADD_PRINT_HTM(30, "2%", "100%", 500, document.getElementById("danju").innerHTML);// 对象2
        LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);// ItemType的值：数字型，0--普通项 1--页眉页脚 2--页号项 3--页数项 4--多页项
        LODOP.SET_PRINT_STYLEA(0, "LinkedItem", 1);//“LinkedItem”：设置关联内容项的项目编号,此处的意思是把对象2作为（ItemType）页眉页脚关联对象一显示,下同
        LODOP.ADD_PRINT_TEXT(12, 441, 122, 30, "预收款收据");
        LODOP.SET_PRINT_STYLEA(0, "FontName", "微软雅黑");
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.SET_PRINT_STYLEA(0, "Bold", 1);
        LODOP.SET_PRINT_STYLEA(0, "Horient", 2);
        LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT", "Auto-Width");//整宽不变形
        LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED", 1);
        LODOP.PREVIEW();
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		var pp_ar_state = $("#pp_ar_state").val();
        if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
        if('view' == api.data.oper && pp_ar_state == '1'){
        	$("#btn-reverse").show();
        }
        if('readonly' == api.data.oper){
        	$("#btn-print").hide();
        }
	},
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-reverse').on('click', function(e){
			e.preventDefault();
			handle.doReverse();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();