var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BATCH13;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'batch/report/pageSellReport';
var _height = $(parent).height()-298,_width = $(parent).width()-192;

var handle = {
	viewDetail: function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		var params = {};
		params.se_type = $("#se_type").val();
		params.begindate = $("#begindate").val();
		params.enddate = $("#enddate").val();
		params.client_name = $("#client_name").val();
		params.se_client_code = $("#se_client_code").val();
		params.depot_name = $("#depot_name").val();
		params.se_depot_code = $("#se_depot_code").val();
		params.se_manager = $("#se_manager").val();
		params.bd_name = $("#bd_name").val();
		params.bd_code = $("#bd_code").val();
		params.tp_name = $("#tp_name").val();
		params.tp_code = $("#tp_code").val();
		params.pd_season = $("#pd_season").val();
		params.pd_year = $("#pd_year").val();
		params.pd_code = $("#pd_code").val();
		params.pd_name = $("#pd_name").val();
		var type = $("#type").val();
		switch(type){
			case 'brand':
				params.bd_code = rowData.code;
				params.bd_name = rowData.name;
				break ;
			case 'type':
				params.tp_code = rowData.code;
				params.tp_name = rowData.name;
				break ;
			case 'client':
				params.se_client_code = rowData.code;
				params.client_name = rowData.name;
				break ;
			case 'product' :
				params.pd_code = rowData.code;
				params.pd_name = rowData.name;
				break;
			case 'season' :
				params.pd_season = rowData.name;
				break;
			case 'depot' :
				params.depot_name = rowData.name;
				params.se_depot_code = rowData.code;
				break;
			case 'manager' : 
				params.se_manager = rowData.name;
				break;
			default :
				break ;
		}
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+"batch/report/to_report_sell_detail",
			data: params,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	operFmatter : function(val, opt, row){
		if(val == "合计"){
			return val;
		}
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="明细">&#xe608;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatRealAmount:function(val, opt, row){
		return row.amount-row.amount_th;
	},
	formatRealMoney:function(val, opt, row){
		return PriceLimit.formatMoney(row.money-row.money_th);
	},
	formatProfit:function(val, opt, row){
		return PriceLimit.formatMoney(row.money-row.money_th-row.cost_money);
	},
	formatReturnRate:function(val, opt, row){
		if(row.amount == 0){
			return "0.00";
		}
		return PriceLimit.formatMoney(row.amount_th/row.amount*100);
	},
	formatMoneyProportion:function(val, opt, row){
		if(val == "合计"){
			return "";
		}
		var userData = $("#grid").getGridParam('userData');
		if(userData.money-userData.money_th == 0){
			return "0.00";
		}
		return PriceLimit.formatMoney((row.money-row.money_th)/(userData.money-userData.money_th)*100);
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_se_type = $("#td_se_type").cssRadio({ callback: function($_obj){
			$("#se_type").val($_obj.find("input").val());
		}});
		this.$_type = $("#td_type").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			$("#type").val(type);
			$('#grid').showCol("code");
			$('#grid').hideCol("pd_no");
			switch(type){
				case 'brand' : 
					$('#grid').setLabel("code","品牌编号");
					$('#grid').setLabel("name","品牌名称");
					break ;
				case 'type' : 
					$('#grid').setLabel("code","类别编号");
					$('#grid').setLabel("name","类别名称");
					break ;
				case 'client' : 
					$('#grid').setLabel("code","客户编号");
					$('#grid').setLabel("name","客户名称");
					break ;
				case 'product' :
					$('#grid').setLabel("code","商品货号");
					$('#grid').setLabel("name","商品名称");
					$('#grid').hideCol("code");
					$('#grid').showCol("pd_no");
					break;
				case 'season' :
					$('#grid').setLabel("code","季节编号");
					$('#grid').setLabel("name","季节名称");
					$('#grid').hideCol("code");
					break;
				case 'depot' :
					$('#grid').setLabel("code","仓库编号");
					$('#grid').setLabel("name","仓库名称");
					break;
				case 'manager' : 
					$('#grid').setLabel("code","经办人员");
					$('#grid').setLabel("name","经办人员");
					$('#grid').hideCol("code");
					break;
				default :
					break ;
			}	
			THISPAGE.reloadData();
		}});
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initGrid:function(){
		var colModel = [
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'客户编号',name: 'code',index:'code',width:100},
	    	{label:'商品货号',name: 'pd_no',index:'pd_no',width:100,hidden:true},
	    	{label:'客户名称',name: 'name',index:'name',width:100},
        	{label:'批发数量',name: 'amount',index:'amount',width:80, align:'right'},
        	{label:'批发金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatByBatch},
        	{label:'退货数量',name: 'amount_th',index:'amount_th',width:80, align:'right'},
        	{label:'退货金额',name: 'money_th',index:'money_th',width:100, align:'right',formatter: PriceLimit.formatByBatch},
        	{label:'实际数量',name: 'real_amount',index:'real_amount',width:80, align:'right',formatter: handle.formatRealAmount},
	    	{label:'实际金额',name: 'real_money',index:'real_money',width:100, align:'right',formatter: handle.formatRealMoney},
	    	{label:'零售金额',name: 'sell_money',index:'sell_money',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'成本金额',name: 'cost_money',index:'cost_money',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'利润',name: 'profit',index:'profit',width:100, align:'right',formatter: handle.formatProfit},
	    	{label:'退货率(%)',name: 'return_rate',index:'sell_money',width:100, align:'right',formatter: handle.formatReturnRate},	
	    	{label:'金额占比(%)',name: 'money_proportion',index:'money_proportion',width:100, align:'right',formatter: handle.formatMoneyProportion}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				var footer = $('#grid').footerData()
				footer.operate = "合计";
				footer.money_proportion = "合计";
		    	$('#grid').footerData('set',footer);
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&se_type='+$("#se_type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&se_client_code='+$("#se_client_code").val();
		params += '&se_depot_code='+$("#se_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&se_manager='+Public.encodeURI($("#se_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#se_manager").val("");
		$("#client_name").val("");
		$("#se_client_code").val("");
		$("#depot_name").val("");
		$("#se_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_se_type.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//收银明细
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.viewDetail(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();