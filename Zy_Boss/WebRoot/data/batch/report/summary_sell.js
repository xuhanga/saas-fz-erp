var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BATCH13;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'batch/report/pageSellSummary';
var _height = $(parent).height()-328,_width = $(parent).width()-192;

var handle = {
	
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_se_type = $("#td_se_type").cssRadio({ callback: function($_obj){
			$("#se_type").val($_obj.find("input").val());
		}});
		this.$_type = $("#td_type").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			$("#type").val(type);
			$('#grid').showCol("code");
			switch(type){
				case 'brand' : 
					$('#grid').setLabel("code","品牌编号");
					$('#grid').setLabel("name","品牌名称");
					break ;
				case 'type' : 
					$('#grid').setLabel("code","类别编号");
					$('#grid').setLabel("name","类别名称");
					break ;
				case 'client' : 
					$('#grid').setLabel("code","客户编号");
					$('#grid').setLabel("name","客户名称");
					break ;
				default :
					break ;
			}	
			THISPAGE.reloadData();
		}});
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
		this.initYearMonth();
	},
	initYearMonth:function(){
		var currentYear = new Date().getFullYear();
		var data = [
			{Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""},
			{Code:""+parseInt(currentYear-4)+"",Name:""+parseInt(currentYear-4)+""},
			{Code:""+parseInt(currentYear-5)+"",Name:""+parseInt(currentYear-5)+""}
		];
		$('#span_year').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :100,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#year").val(data.Code);
				}
			}
		}).getCombo();
		
		var month_data = [
		    {Code:"",Name:"全部"},
			{Code:"1",Name:"1"},
			{Code:"2",Name:"2"},
			{Code:"3",Name:"3"},
			{Code:"4",Name:"4"},
			{Code:"5",Name:"5"},
			{Code:"6",Name:"6"},
			{Code:"7",Name:"7"},
			{Code:"8",Name:"8"},
			{Code:"9",Name:"9"},
			{Code:"10",Name:"10"},
			{Code:"11",Name:"11"},
			{Code:"12",Name:"12"}
		];
		$('#span_month').combo({
			data:month_data,
			value: 'Code',
			text: 'Name',
			width :100,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#month").val(data.Code);
				}
			}
		}).getCombo();
		
		
	},
	gridTotal:function(){
    	var grid = $('#grid');
		var footerData = {code:'合计'};	
		for (var i = 1 ; i <= 12 ; i++ ){
			footerData["amountMap."+i] = grid.getCol("amountMap."+i,false,'sum');
			footerData["moneyMap."+i] = grid.getCol("moneyMap."+i,false,'sum');
		}
		footerData["totalamount"] = grid.getCol("totalamount",false,'sum');
		footerData["totalmoney"] = grid.getCol("totalmoney",false,'sum');
		grid.footerData('set',footerData);
    },
	initGrid:function(){
		var month = $("#month").val();
		var colModel = [
	    	{label:'客户编号',name: 'code',index:'code',width:80,hidden:true},
	    	{label:'客户名称',name: 'name',index:'name',width:100}
	    ];
		if(month == ""){
			for (var i = 1; i <= 12; i++) {
	        	colModel.push({label:'数量',name: "amountMap."+i,index:'',width: 60, align:'right'});
				colModel.push({label:'金额',name: "moneyMap."+i,index:'',width: 60, align:'right',formatter: PriceLimit.formatByBatch});
			}
	        colModel.push({label:'数量',name: "totalamount",width: 60, align:'right'});
			colModel.push({label:'金额',name: "totalmoney",width: 60, align:'right',formatter: PriceLimit.formatByBatch});
		}else{
			colModel.push({label:'数量',name: "amountMap."+month,index:'',width: 60, align:'right'});
			colModel.push({label:'金额',name: "moneyMap."+month,index:'',width: 60, align:'right',formatter: PriceLimit.formatByBatch});
		}
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
		var groupHeaders = [];
		if(month == ""){
			for (var i=1 ;i <= 12; i++){
	        	groupHeaders.push({startColumnName : 'amountMap.'+i,numberOfColumns : 2, titleText : i+"月份"});
	        }
	        groupHeaders.push({startColumnName : 'totalamount',numberOfColumns : 2, titleText :'小计' });
		}else{
			groupHeaders.push({startColumnName : 'amountMap.'+month,numberOfColumns : 2, titleText : month+"月份"});
		}
        $("#grid").jqGrid('setGroupHeaders', { 
			useColSpanStyle : true, // 没有表头的列是否与表头列位置的空单元格合并 
			groupHeaders : groupHeaders
		});
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&year='+$("#year").val();
		params += '&month='+$("#month").val();
		params += '&se_type='+$("#se_type").val();
		params += '&se_client_code='+$("#se_client_code").val();
		params += '&se_depot_code='+$("#se_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&se_manager='+Public.encodeURI($("#se_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#se_manager").val("");
		$("#client_name").val("");
		$("#se_client_code").val("");
		$("#depot_name").val("");
		$("#se_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_se_type.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		$('#grid').GridUnload();
    	this.initGrid();
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();