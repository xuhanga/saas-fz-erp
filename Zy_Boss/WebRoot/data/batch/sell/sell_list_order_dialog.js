var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var od_type = $("#od_type").val();
var queryurl = config.BASEPATH+'batch/order/page4Sell';
var _height = (api.config.height-132)/2-20,_width = (api.config.width-34);//获取弹出框弹出宽、高
var Utils = {
	doQueryClient : function(){
		commonDia = $.dialog({
			title : '选择批发客户',
			content : 'url:'+config.BASEPATH+'batch/client/to_list_dialog',
			data : {multiselect:false},
			width : 650,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#od_client_code").val(selected.ci_code);
				$("#client_name").val(selected.ci_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#od_client_code").val(selected.ci_code);
					$("#client_name").val(selected.ci_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'batch'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	formatGift :function(val, opt, row){
		if(row.odl_pi_type == 1){
			return '赠品';
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initDetailGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'od_client_code',label:'',index: 'od_client_code',hidden:true},
	    	{name: 'od_depot_code',label:'',index: 'od_depot_code',hidden:true},
	    	{name: 'od_handnumber',label:'',index: 'od_handnumber',hidden:true},
	    	{name: 'od_delivery_date',label:'',index: 'od_delivery_date',hidden:true},
	    	{name: 'od_number',label:'单据编号',index: 'od_number',width:150},
	    	{name: 'client_name',label:'批发客户',index: 'client_name',width:120},
	    	{name: 'depot_name',label:'发货仓库',index: 'depot_name',width:120},
	    	{name: 'od_manager',label:'经办人',index: 'od_manager',width:100},
	    	{name: 'od_amount',label:'订货数量',index: 'od_amount',width:80,align:'right'},
	    	{name: 'od_make_date',label:'制单日期',index: 'od_make_date',width:80},
	    	{name: 'od_maker',label:'制单人',index: 'od_maker',width:100},
	    	{name: 'od_remark',label:'备注',index: 'od_remark',width:180,hidden:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'od_id'  //图标ID
			},
			loadComplete: function(data){
				$("#detailGrid").clearGridData();
				THISPAGE.detailGridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			onSelectRow: function (rowid, status) {
                var rowData = $("#grid").jqGrid("getRowData", rowid);
                THISPAGE.reloadDetailData(rowData.od_number);
            },
			ondblClickRow: function (rowid, iRow, iCol, e) {
				
            }
	    });
	},
	detailGridTotal:function(){
    	var grid=$('#detailGrid');
		var odl_amount=grid.getCol('odl_amount',false,'sum');
		var odl_realamount=grid.getCol('odl_realamount',false,'sum');
    	grid.footerData('set',{odl_amount:odl_amount,odl_realamount:odl_realamount});
    },
	initDetailGrid:function(){
		var colModel = [
		    {label:'',name: 'odl_pd_code', index: 'odl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'订单数量',name: 'odl_amount', index: 'odl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'实到数量',name: 'odl_realamount', index: 'odl_realamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'批发价',name: 'odl_unitprice', index: 'odl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByBatch},
	    	{label:'赠品',name: 'gift', index: 'odl_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'odl_pi_type', index: 'odl_pi_type', width: 100,hidden:true}
	    ];
		$('#detailGrid').jqGrid({
			loadonce:true,
			datatype: 'local',
			width: _width,
			height: _height-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#detailPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:true,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'odl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.detailGridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'od_type='+$("#od_type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&od_client_code='+$("#od_client_code").val();
		params += '&od_depot_code='+$("#od_depot_code").val();
		params += '&od_manager='+Public.encodeURI($("#od_manager").val());
		params += '&od_number='+Public.encodeURI($("#od_number").val());
		return params;
	},
	reset:function(){
		$("#od_client_code").val("");
		$("#od_depot_code").val("");
		$("#od_number").val("");
		$("#od_manager").val("");
		$("#client_name").val("");
		$("#depot_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	reloadDetailData:function(number){
		var querydetailurl = config.BASEPATH+'batch/order/detail_list/'+number;
		$("#detailGrid").jqGrid('setGridParam',{datatype:"json",page:1,url:querydetailurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};

function doSelect(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择订单！"});
		return false;
	}
	var rowIds = $("#detailGrid").jqGrid('getGridParam', 'selarrrow');
    if (rowIds.length == 0) {
    	Public.tips({type: 2, content : "请选择订单明细!"});
        return false;
    }
    var rowData = $("#grid").jqGrid("getRowData", selectedId);
    var detailDatas = [];
	for (var i = 0; i < rowIds.length; i++) {
		detailDatas.push($("#detailGrid").jqGrid("getRowData", rowIds[i]));
	}
	return {mainData:rowData,detailIds:rowIds,detailDatas:detailDatas};
}

THISPAGE.init();