var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'buy/dealings/page';
var _height = $(parent).height()-179,_width = $(parent).width()-32;

var handle = {
	openDetail:function(number,dl_type){
    	var url = '';
    	var data = {oper: 'readonly'};
    	if(dl_type == '0' || dl_type == '1'){
    		url = config.BASEPATH+"buy/enter/to_view/"+number;
    	}else if(dl_type == '3'){
    		url = config.BASEPATH+"buy/fee/to_view/"+number;
    	}else if(dl_type == '4' || dl_type == '5'){
    		url = config.BASEPATH+"buy/prepay/to_view/"+number;
    	}else if(dl_type == '6'){
    		url = config.BASEPATH+"buy/settle/to_view/"+number;
    	}
    	if(url == ''){
    		return;
    	}
    	$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	formatNumberLink : function(val, opt, row){
		var btnHtml = '<a href="javascript:void(0);" onclick="javascript:handle.openDetail(\''+row.dl_number+'\',' + row.dl_type + ');">'+row.dl_number+'</a>';
		return btnHtml;
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '采购单';
		}else if(val == 1){
			return '退货单';
		}else if(val == 2){
			return '期初单';
		}else if(val == 3){
			return '费用单';
		}else if(val == 4){
			return '预付款单';
		}else if(val == 5){
			return '退款单';
		}else if(val == 6){
			return '结算单';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var dl_buyamount=grid.getCol('dl_buyamount',false,'sum');
		var dl_discount_money=grid.getCol('dl_discount_money',false,'sum');
    	var dl_payable=grid.getCol('dl_payable',false,'sum');
    	var dl_payabled=grid.getCol('dl_payabled',false,'sum');
    	grid.footerData('set',{dl_date:'合计：',
    		dl_buyamount:dl_buyamount,
    		dl_discount_money:dl_discount_money,
    		dl_payable:dl_payable,
    		dl_payabled:dl_payabled});
    },
	initGrid:function(){
		var colModel = [
	    	{label:'日期',name: 'dl_date',index: 'dl_date',width:135},
	    	{label:'单据编号',name: 'numberlink',index: 'dl_number',width:150,formatter: handle.formatNumberLink},
	    	{label:'供货厂商',name: 'supply_name',index: 'client_name',width:120},
	    	{label:'类型',name: 'dl_type',index: 'dl_type',width:100,formatter: handle.formatType},
	    	{label:'数量',name: 'dl_buyamount',index: 'dl_buyamount',width:80,align:'right'},
	    	{label:'优惠金额',name: 'dl_discount_money',index: 'dl_discount_money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'应付金额',name: 'dl_payable',index: 'dl_payable',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'已付金额',name: 'dl_payabled',index: 'dl_payabled',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'欠款',name: 'dl_debt',index: 'dl_debt',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'备注',name: 'dl_remark',index: 'dl_remark',width:180}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'dl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'sp_code='+api.data.sp_code;
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		return params;
	},
	reset:function(){
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn-close').on('click', function(e){
			e.preventDefault();
			api.close();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();