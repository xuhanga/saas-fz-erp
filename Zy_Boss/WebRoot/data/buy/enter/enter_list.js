var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BUY02;
var _menuParam = config.OPTIONLIMIT;
var et_type = $("#et_type").val();
var queryurl = config.BASEPATH+'buy/enter/page';

var Utils = {
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#et_supply_code").val(selected.sp_code);
				$("#supply_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#et_supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'buy'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#et_depot_code").val(selected.dp_code);
				$("#depot_name").val(selected.dp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#et_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#et_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#et_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'add'){
			url = config.BASEPATH+"buy/enter/to_add/"+et_type;
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			url = config.BASEPATH+"buy/enter/to_update?et_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'view'){
			url = config.BASEPATH+"buy/enter/to_view?et_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"buy/enter/to_view?et_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	del: function(rowId){//删除
		var rowData = $("#grid").jqGrid("getRowData", rowId);
	 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'buy/enter/del',
				data:{"number":rowData.et_number},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowId);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
    },
    callback: function(data, oper, dialogWin){
		if(oper == "edit") {
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
		if(oper == "approve") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
		if(oper == 'view'){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.et_ar_state == '0') {
            btnHtml += '<input type="button" value="审核" class="btn_sp" onclick="javascript:handle.operate(\'approve\',' + opt.rowId + ');" />';
        }else if (row.et_ar_state == '2') {
            btnHtml += '<input type="button" value="修改" class="btn_xg" onclick="javascript:handle.operate(\'edit\',' + opt.rowId + ');" />';
        }else if (row.et_ar_state == '1') {//审核通过
        	btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
        } 
		btnHtml += '<input type="button" value="流程" class="btn_qh" onclick="javascript:listBillProcess(\''+row.et_number+'\',\'t_buy_enter\');" />';
		return btnHtml;
	},
	formatArState:function(val, opt, row){//0：未审核；1：已审核2：已退回
		if(val == 0){
			return '未审核';
		}else if(val == 1){
			return '已审核';
		}else if(val == 2){
			return '已退回';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_ar_state = $("#ar_state").cssRadio({ callback: function($_obj){
			$("#et_ar_state").val($_obj.find("input").val());
		}});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var et_amount=grid.getCol('et_amount',false,'sum');
    	var et_money=grid.getCol('et_money',false,'sum');
    	var et_retailmoney=grid.getCol('et_retailmoney',false,'sum');
    	var userData = $("#grid").getGridParam('userData');
    	grid.footerData('set',{et_number:'本页小计<br>总计',
    		et_amount:et_amount+"<br>"+userData.et_amount,
    		et_money:et_money.toFixed(2)+"<br>"+userData.et_money.toFixed(2),
    		et_retailmoney:et_retailmoney.toFixed(2)+"<br>"+userData.et_retailmoney.toFixed(2)});
    },
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 100, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'et_number',label:'单据编号',index: 'et_number',width:150},
	    	{name: 'et_order_number',label:'订单编号',index: 'et_order_number',width:150},
	    	{name: 'supply_name',label:'供货厂商',index: 'supply_name',width:120},
	    	{name: 'depot_name',label:(et_type == '0'?'收':'退')+'货仓库',index: 'depot_name',width:120},
	    	{name: 'et_manager',label:'经办人',index: 'et_manager',width:100},
	    	{name: 'et_amount',label:'总计数量',index: 'et_amount',width:80,align:'right'},
	    	{name: 'et_money',label:'进货金额',index: 'et_money',width:80,align:'right',formatter: PriceLimit.formatByEnter},
	    	{name: 'et_retailmoney',label:'零售金额',index: 'et_retailmoney',width:80,align:'right',formatter: PriceLimit.formatBySell},
	    	{name: 'et_ar_state',label:'审核状态',index: 'et_ar_state',width:80,formatter: handle.formatArState,hidden:$("#fromJsp").val() == "warn"},
	    	{name: 'et_ar_name',label:'审核人',index: 'et_ar_name',width:80,hidden:$("#fromJsp").val() == "warn"},
	    	{name: 'et_ar_date',label:'审核日期',index: 'et_ar_date',width:80,hidden:$("#fromJsp").val() == "warn"},
	    	{name: 'et_make_date',label:'制单日期',index: 'et_make_date',width:80},
	    	{name: 'et_maker',label:'制单人',index: 'et_maker',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-90,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'et_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'et_type='+$("#et_type").val();
		params += '&et_isdraft=0';
		params += '&et_ar_state='+$("#et_ar_state").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&et_number='+Public.encodeURI($("#et_number").val());
		return params;
	},
	reset:function(){
		$("#et_supply_code").val("");
		$("#et_depot_code").val("");
		$("#et_number").val("");
		$("#et_manager").val("");
		$("#supply_name").val("");
		$("#depot_name").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_ar_state.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		$('#btn-del').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			if(rowId == null || rowId == ''){
				Public.tips({type: 1, content : "您未选择任何数据"});
				return;
			}
			var rowData =$("#grid").jqGrid("getRowData", rowId);
			if(rowData.et_ar_state == "已审核"){
				Public.tips({type: 1, content : "单据已审核，不能删除"});
				return;
			}
			handle.del(rowId)
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		//导出
		$('#btn-export').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.EXPORT)) {
				return ;
			};
			window.open(config.BASEPATH+'buy/enter/report?' + THISPAGE.buildParams());
		});
	}
}
THISPAGE.init();