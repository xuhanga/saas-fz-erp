var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var _limitMenu = system.MENULIMIT.BASE26;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/enter/print_barcode/'+$("#et_number").val();
var ST_SubCodeRule = $("#ST_SubCodeRule").val();//获取生成方式
var ST_SubCodeIsRule = $("#ST_SubCodeIsRule").val();
var subCodeRules = ST_SubCodeRule.split("+");//分割

var handle = {
	copyBarcode:function(id){
		var grid=$('#grid'),row=grid.getRowData(id);
		grid.setCell(id,'bc_barcode',row.bc_sys_barcode);
	},
	bulkCopyBarcode:function(){
		$.dialog.confirm('确定要批量复制吗？',function(){
			var Ids=$("#grid").jqGrid("getGridParam", "selarrrow");
			var	length= Ids.length;
			for(var i=0;i<length;i++){
				var fuzhi=$("#grid").jqGrid("getRowData", Ids[i]).bc_sys_barcode;
				$('#grid').jqGrid('setCell',Ids[i],'bc_barcode',fuzhi);
			}
		});
	},
	save:function(rowId){
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return;
		};
		var row=$('#grid').jqGrid('getRowData',rowId);
		var bc_barcode = row.bc_barcode;
		if(bc_barcode == ""){
			Public.tips({type: 2, content : "请输入条形码！"});
			return;
		}
		var bc_id = row.bc_id;
		if(bc_id == ""){
			var pd_no = row.bc_pd_no;
			var pd_code= row.bc_pd_code;
			var cr_code =row.bc_color;
			var sz_code = row.bc_size;
			var br_code = row.bc_bra;
			var bc_subcode = row.bc_subcode;//子码
			var bc_sys_barcode =  row.bc_sys_barcode;//系统条码
			$.ajax({
				type:"POST",
				url:config.BASEPATH +"base/product/saveOneBarcode",
				data:"bc_barcode="+bc_barcode+"&pd_no="+pd_no+"&pd_code="+pd_code+"&cr_code="+cr_code+"&sz_code="+sz_code+"&br_code="+br_code+"&bc_subcode="+bc_subcode+"&bc_sys_barcode="+bc_sys_barcode,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : data.message});
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			}); 
		}else{
			$.ajax({
				type:"POST",
				url:config.BASEPATH +"base/product/saveOneBarcode",
				data:"bc_barcode="+bc_barcode+"&bc_id="+bc_id,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : data.message});
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			}); 
		}
	},
	bulkSaveBarcode:function(){
		var Ids = new Array();
		Ids = $("#grid").jqGrid("getGridParam", "selarrrow");
		$.dialog.confirm('确定要批量保存吗？',function(){
			var len=Ids.length;
			if(len==0){
				Public.tips({type: 2, content : '没有需要保存的数据！'});
				return;
			}
			var id="";
			var pd_no = "";
			var pd_code = "";
			var barcode = "";
			var cr_code = "";
			var sz_code= "";
			var br_code = "";
			var bc_subcode = "";
			var sys_barcode = "";
			var bc_barcode = "";
			
			var insert_pd_nos = "";
			var insert_pd_codes = "";
			var insert_cr_codes = "";
			var insert_sz_codes = "";
			var insert_br_codes = "";
			var insert_bc_subcodes = "";//子码
			var insert_bc_sys_barcodes =  "";//系统条码
			var insert_bc_barcodes = "";
			
			var update_bc_barcodes = "";
			var update_bc_ids="";
			
			for(var j=0;j<len;j++){
			    id=Ids[j];
			    var bc_id =$("#grid").jqGrid('getCell', id,'bc_id');
			    if(bc_id == ""){//新增
			    	pd_no = $("#grid").jqGrid('getCell', id,'bc_pd_no');
			    	insert_pd_nos += pd_no+",";
			    	pd_code = $("#grid").jqGrid('getCell', id,'bc_pd_code');
			    	insert_pd_codes += pd_code+",";
			    	cr_code = $("#grid").jqGrid('getCell', id,'bc_color');
			    	insert_cr_codes += cr_code+",";
			    	sz_code = $("#grid").jqGrid('getCell', id,'bc_size');
			    	insert_sz_codes += sz_code+",";
			    	br_code = $("#grid").jqGrid('getCell', id,'bc_bra');
			    	if(br_code == ""){
			    		br_code = "0";
			    	}
			    	insert_br_codes += br_code+",";
			    	bc_subcode = $("#grid").jqGrid('getCell', id,'bc_subcode');
			    	insert_bc_subcodes += bc_subcode+",";
			    	sys_barcode = $("#grid").jqGrid('getCell', id,'bc_sys_barcode');
			    	insert_bc_sys_barcodes += sys_barcode+",";
			    	bc_barcode = $("#grid").jqGrid('getCell', id,'bc_barcode');
			    	insert_bc_barcodes += bc_barcode+",";
			    	
			    }else{//修改
			    	barcode =$("#grid").jqGrid('getCell', id,'bc_barcode');
			    	update_bc_barcodes += barcode+",";
			    	update_bc_ids += bc_id+",";
			    }
			}
			if(update_bc_ids!= null && update_bc_ids !='' && update_bc_ids.length >0){
				update_bc_ids=update_bc_ids.substring(0,update_bc_ids.length-1);
				update_bc_barcodes=update_bc_barcodes.substring(0,update_bc_barcodes.length-1);
				handle.updateAllBarcode(update_bc_ids, update_bc_barcodes);
			}
			if(insert_pd_nos!= null && insert_pd_nos !='' && insert_pd_nos.length >0){
				insert_pd_nos=insert_pd_nos.substring(0,insert_pd_nos.length-1);
				insert_pd_codes=insert_pd_codes.substring(0,insert_pd_codes.length-1);
				insert_cr_codes=insert_cr_codes.substring(0,insert_cr_codes.length-1);
				insert_sz_codes=insert_sz_codes.substring(0,insert_sz_codes.length-1);
				insert_br_codes=insert_br_codes.substring(0,insert_br_codes.length-1);
				insert_bc_subcodes=insert_bc_subcodes.substring(0,insert_bc_subcodes.length-1);
				insert_bc_sys_barcodes=insert_bc_sys_barcodes.substring(0,insert_bc_sys_barcodes.length-1);
				insert_bc_barcodes=insert_bc_barcodes.substring(0,insert_bc_barcodes.length-1);
				handle.saveAllBarcode(insert_pd_nos,insert_pd_codes,insert_cr_codes,insert_sz_codes,insert_br_codes,insert_bc_subcodes,insert_bc_sys_barcodes,insert_bc_barcodes);
			}
		});
	},
	saveAllBarcode:function(pd_nos,pd_codes,cr_codes,sz_codes,br_codes,bc_subcodes,bc_sys_barcodes,bc_barcodes){
		var codeArr = bc_barcodes.split(",");
		var count=0;
		var tempCodes="";
		bc_barcodes = bc_barcodes.split(",");
		for(var i=0,size=codeArr.length;i<size;i++){
			tempCodes += '@'+bc_barcodes[i]+'@';
			count = tempCodes.split(codeArr[i]).length-1;
			if(count > 1){
				Public.tips({type: 2, content : '有重复条码！'});
				return;
			}
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/saveAllBarcode",
			data:"bc_barcodes="+bc_barcodes+"&pd_nos="+pd_nos+"&pd_codes="+pd_codes+"&cr_codes="+cr_codes+"&sz_codes="+sz_codes+"&br_codes="+br_codes+"&bc_subcodes="+bc_subcodes+"&bc_sys_barcodes="+bc_sys_barcodes,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		}); 
	},
	updateAllBarcode:function(bc_ids,bc_barcodes){
		var codeArr = bc_barcodes.split(",");
		var count=0;
		var tempCodes="";
		bc_barcodes = bc_barcodes.split(",");
		for(var i=0,size=codeArr.length;i<size;i++){
			tempCodes += '@'+bc_barcodes[i]+'@';
			count = tempCodes.split(codeArr[i]).length-1;
			if(count > 1){
				Public.tips({type: 2, content : '有重复条码！'});
				return;
			}
		}
		
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/product/saveAllBarcode",
			data:"bc_ids="+bc_ids+"&bc_barcodes="+bc_barcodes,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		}); 
	},
	printSet:function(){
    	$.dialog({
    		//id设置用来区别唯一性 防止div重复
    	   	id:'printPlan',
    	   	//设置标题
    	   	title:'条码打印设置',
    	   	//设置最大化最小化按钮(false：隐藏 true:关闭)
    	   	max: false,
    	   	min: false,
    	   	lock:true,
    	   	//设置长宽
    	   	width:440,
    	   	height:380,
    	   	//设置是否拖拽(false:禁止，true可以移动)
    	   	drag: true,
    	   	resize:false,
   		   	//设置页面加载处理
  		   	fixed:false,
    	   	content:"url:"+config.BASEPATH+"base/product/to_barcode_plan",
    	   	close: function(){
    	   		
  		   	}
        });
	},
	print:function(printState){//打印状态0直接打印 1打印预览
		var arrayObj = new Array();
		arrayObj=$('#grid').jqGrid('getGridParam','selarrrow');
		var i=0,addMark=0;
		if(arrayObj == "" ){
			Public.tips({type: 2, content : '没有可打印的记录！'});
            return;
        }
        var bc_barcodes = new Array();
        var bc_colornames = new Array();
        var bc_pd_nos= new Array();
        var bc_pd_names = new Array();
        var bc_branames = new Array();
       	var bc_sizenames = new Array();
       	var pd_bd_names = new Array();
       	var pd_sell_prices = new Array();
       	var pd_vip_prices = new Array();
       	var pd_sign_prices = new Array();
       	var pd_grades = new Array();
       	var pd_safes = new Array();
       	var pd_fills = new Array();
       	var pd_executes=new Array();
       	var pd_tp_names =new Array(); 
       	var pd_fabrics =new Array(); 
       	var pd_in_fabrics =new Array(); 
       	var pd_places =new Array();
       	var pd_wash_explains =new Array();
        if(arrayObj =="")
		{
			return;
		}
		else
		{
			var re= new RegExp("^[0-9]*[1-9][0-9]*$");//验证
			var tag = false;
			var k = 0;
			for (i=0; i<arrayObj.length; i++){
				if($("#grid").jqGrid('getCell', arrayObj[i],'bc_barcode')==""){
					Public.tips({type: 2, content : '请输入条形码！'});
					return;
				}
				var num = $("#grid").jqGrid('getCell', arrayObj[i],'bc_printnumber');
				if(num == ""){
					num = 1+"";
				}
				if(num.match(re)==null){
					Public.tips({type: 2, content : '打印数量只能输入正整数！'});
					return;
				}
				for(var n=0;n<num;n++){
					bc_barcodes[k] =$("#grid").jqGrid('getCell', arrayObj[i],'bc_barcode');
					bc_colornames[k] = $("#grid").jqGrid('getCell', arrayObj[i],'bc_colorname');
					bc_pd_nos[k] = $("#grid").jqGrid('getCell', arrayObj[i],'bc_pd_no');
					bc_branames[k] =  $("#grid").jqGrid('getCell', arrayObj[i],'bc_braname');
					bc_sizenames[k] = $("#grid").jqGrid('getCell', arrayObj[i],'bc_sizename');
					pd_bd_names[k] = $("#grid").jqGrid('getCell', arrayObj[i],'pd_bd_name');
					pd_sell_prices[k] = $("#grid").jqGrid('getCell', arrayObj[i],'pd_sell_price');
					pd_vip_prices[k]= $("#grid").jqGrid('getCell', arrayObj[i],'pd_vip_price');
					pd_sign_prices[k]= $("#grid").jqGrid('getCell', arrayObj[i],'pd_sign_price');
					bc_pd_names[k] = $("#grid").jqGrid('getCell', arrayObj[i],'bc_pd_name');
					pd_fills[k] = $("#grid").jqGrid('getCell', arrayObj[i],'pd_fill');
					pd_safes[k] = $("#grid").jqGrid('getCell', arrayObj[i],'pd_safe');
					pd_grades[k] = $("#grid").jqGrid('getCell', arrayObj[i],'pd_grade');
					pd_executes[k]=$("#grid").jqGrid('getCell', arrayObj[i],'pd_execute');
					pd_tp_names[k]=$("#grid").jqGrid('getCell', arrayObj[i],'pd_tp_name');
					pd_fabrics[k]=$("#grid").jqGrid('getCell', arrayObj[i],'pd_fabric');
					pd_in_fabrics[k]=$("#grid").jqGrid('getCell', arrayObj[i],'pd_in_fabric');
					pd_places[k]=$("#grid").jqGrid('getCell', arrayObj[i],'pd_place');
					pd_wash_explains[k]=$("#grid").jqGrid('getCell', arrayObj[i],'pd_wash_explain');
					k++;
					addMark++;
				}
			}
		}
		if(addMark==0)
		{
			Public.tips({type: 2, content : '请选择要打印的条形码！'});
			return;	
		}
		xdPrint.barCode(printState,bc_barcodes,bc_colornames,bc_pd_nos,bc_branames,bc_sizenames,pd_bd_names,pd_sell_prices,pd_sign_prices,pd_vip_prices,pd_grades,pd_safes,pd_fills,bc_pd_names,pd_executes,pd_tp_names,pd_fabrics,pd_in_fabrics,pd_places,pd_wash_explains);
	},
	operFmatter:function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-copy" title="复制">&#xe63a;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-shenpi" title="保存">&#xe616;</i>';
		html_con += '</div>';
		return html_con;
	},
	sysBarcodeFmatter:function(val, opt, row) {
		var pd_no = row.bc_pd_no;
		var color = row.bc_color;
		var size = row.bc_size;
		var bra = row.bc_bra;
		var pd_code = row.bc_pd_code;
		if(row.bc_sys_barcode == "" || row.bc_sys_barcode == null ){
			return handle.getBarcode(pd_no,color,size,bra,pd_code);
		}else{
			return row.bc_sys_barcode;
		}
	},
	subcodeFmatter:function(val, opt, row) {
		var cr_code = row.bc_color;
		var sz_code = row.bc_size;
		var br_code = row.bc_bra;
		var pd_code = row.bc_pd_code;
		var bc_subcode = pd_code + cr_code + sz_code + br_code;
		return bc_subcode;
	},
	getBarcode:function(pd_no,color,size,bra,pd_code){
		var barcode = "";
		if("0" == ST_SubCodeIsRule){
			barcode = pd_code+color+size+bra;
		}else{
			for(var i=0;i<subCodeRules.length;i++){
				if("货号"==subCodeRules[i]){
					barcode += pd_no;
					continue;
				}
				if("编码"==subCodeRules[i]){
					barcode += pd_code;
					continue;
				}
				if("颜色"==subCodeRules[i]){
					barcode += color;
					continue;
				}
				if("尺码"==subCodeRules[i]){
					barcode += size;
					continue;
				}
				if("杯型"==subCodeRules[i]){
					barcode += bra;
					continue;
				}
			}
		}
		return barcode;
	}
}
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'operate', label:'操作',width: 80, fixed:true, formatter: handle.operFmatter, title: false,sortable:false},
		    {name: 'bc_pd_no', label:'货号',index: 'pd_no', width: 80, title: false,fixed:true,sortable:true},
	    	{name: 'bc_pd_name', label:'商品名称',index: 'pd_name', width: 80, title: false,sortable:false},
	    	{name: 'pd_bd_name', label:'品牌',index: 'pd_bd_name', width: 80, title: false,sortable:false,hidden:true},
	    	{name: 'pd_tp_name', label:'类别',index: 'pd_tp_name', width:80, title: false,sortable:false,hidden:true},
	    	{name: 'bc_colorname',label:'颜色',index: 'bc_colorname',width: 60,title: false,fixed:true,sortable:true},
	    	{name: 'bc_sizename',label:'尺码',index: 'bc_sizename',width: 60,title: false,sortable:true},
	    	{name: 'bc_braname',label:'杯型',index: 'bc_braname',width: 40,title: false,sortable:true},
	    	{name: 'pd_sell_price', label:'零售价',index: 'pd_sell_price', width: 80, title: false,sortable:true},
	    	{name: 'bc_sys_barcode',label:'系统条形码',index: 'bc_sys_barcode', width: 150,title: false,sortable:false,formatter: handle.sysBarcodeFmatter},
	    	{name: 'bc_subcode',label:'子码',index: 'bc_subcode', formatter: handle.subcodeFmatter,hidden:true},
	    	{name: 'bc_barcode',label:'自建条形码',index: 'bc_barcode', width: 150,title: false,editable:true,sortable:false},
	    	{name: 'bc_printnumber',label:'打印数量', index: 'bc_printnumber', width: 80, editable:true, title: false},
	    	{name: 'pd_grade',label:'等级', hidden:true},
	    	{name: 'pd_safe',label:'安全标准', hidden:true},
	    	{name: 'pd_fill',label:'填充物',hidden:true},
	    	{name: 'pd_vip_price', label:'会员价', hidden:true},
	    	{name: 'pd_sign_price',label:'标牌价', hidden:true},
	    	{name: 'pd_execute',label:'执行标准',hidden:true},
	    	{name: 'bc_id',label:'',hidden:true},
	    	{name: 'id',label:'',hidden:true},
	    	{name: 'bc_size',label:'',hidden:true},
	    	{name: 'bc_bra',label:'',hidden:true},
	    	{name: 'bc_color',label:'',hidden:true},
	    	{name: 'bc_pd_code',label:'',hidden:true},
	    	{name: 'pd_fabric',label:'面料',hidden:true},
	    	{name: 'pd_in_fabric',label:'里料',hidden:true},
	    	{name: 'pd_place',label:'产地',hidden:true},
	    	{name: 'pd_wash_explain',label:'洗涤说明',hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'json',
			width: 1010,
			height: 320,
			gridview: true,
			onselectrow: false,
			cellEdit: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:true,//多选
			viewrecords: true,
			cellsubmit: 'clientArray',//可编辑列在编辑完成后会触发保存事件，clientArray表示只保存到表格不提交到服务器
			rowNum:-1,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initEvent:function(){
		 //复制
        $('#grid').on('click', '.operating .ui-icon-copy', function (e) {
        	e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
            var id = $(this).parent().data('id');
        	handle.copyBarcode(id);
        });
		 //修改
        $('#grid').on('click', '.operating .ui-icon-shenpi', function (e) {
        	e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
            var id = $(this).parent().data('id');
        	handle.save(id);
        });
        //批量复制
		$('#btn-bulkCopyBarcode').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			handle.bulkCopyBarcode();
		});
		//批量保存
		$('#btn-bulkSaveBarcode').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			handle.bulkSaveBarcode();
		});
		//打印设置
		$('#btn-printSite').on('click', function(e){
			e.preventDefault();
			handle.printSet();
		});
		//打印
		$('#btn-print').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.PRINT)) {
				return ;
			}
			handle.print(0);
		});
		$('#btn-print-preview').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.PRINT)) {
				return ;
			}
			handle.print(1);
		});
	}
}
THISPAGE.init();