var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY17;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/fee/listReport';
var querydetailurl = config.BASEPATH+'buy/fee/listReportDetail';
var _height = $(parent).height()-285,_width = $(parent).width()-32;

var Utils = {
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#fe_supply_code").val(selected.sp_code);
				$("#supply_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#fe_supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initDetailGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_timetype = $("#timetypetd").cssRadio({ callback: function($_obj){
			$("#timeType").val($_obj.find("input").val());
		}});
		this.$_pay_state = $("#pay_state").cssRadio({ callback: function($_obj){
			$("#fe_pay_state").val($_obj.find("input").val());
		}});
		this.$_ar_state = $("#ar_state").cssRadio({ callback: function($_obj){
			$("#fe_ar_state").val($_obj.find("input").val());
		}});
	},
	initGrid:function(){
		var colModel = [
	    	{label:'供货商编号',name: 'code',index:'code',width:100},	
	    	{label:'供货商名称',name: 'name',index:'name',width:160},
	    	{label:'支出金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			loadonce:true,
			width: _width/2-180,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			pgtext: '',
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			rowNum:9999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'code'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
		    	var ids = $("#grid").getDataIDs();
				if(ids.length>0){
					$("#grid tr:eq(1)").trigger("click");
				}else{
					$("#detailGrid").clearGridData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				var rowData = $('#grid').jqGrid('getRowData', rowid);
				var params = '';
				params += 'begindate='+$("#begindate").val();
				params += '&enddate='+$("#enddate").val();
				params += '&timeType='+$("#timeType").val();
				params += '&fe_ar_state='+$("#fe_ar_state").val();
				params += '&fe_pay_state='+$("#fe_pay_state").val();
				var type = $("#type").val();
				if(type == "supply"){
					params += '&type=property';
					params += '&fe_supply_code='+rowData.code;
				}else if(type == 'property'){
					params += '&type=supply';
					params += '&fel_mp_code='+rowData.code;
					params += '&fe_supply_code='+$("#fe_supply_code").val();
				}
				$("#detailGrid").jqGrid('setGridParam',{datatype:"json",url:querydetailurl+'?'+params}).trigger("reloadGrid");
			}
	    });
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{name:'合计：',money:money});
    },
    gridDetailTotal:function(){
    	var grid=$('#detailGrid');
		var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{code:'合计：',money:money});
    },
	initDetailGrid:function(){
		var colModel = [
	    	{label:'日期',name: 'date',index:'date',width:120,hidden:true},
	    	{label:'费用类型编号',name: 'code',index:'code',width:120},
	    	{label:'费用类型名称',name: 'name',index:'name',width:120},
	    	{label:'支出金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#detailGrid').jqGrid({
			datatype: 'local',
			width: _width/2,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#detailPage',//分页
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			pgtext: '',
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			rowNum:9999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false
			},
			loadComplete: function(data){
				THISPAGE.gridDetailTotal();
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&fe_supply_code='+$("#fe_supply_code").val();
		params += '&timeType='+$("#timeType").val();
		params += '&fe_ar_state='+$("#fe_ar_state").val();
		params += '&fe_pay_state='+$("#fe_pay_state").val();
		return params;
	},
	reset:function(){
		$("#supply_name").val("");
		$("#fe_supply_code").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_timetype.setValue(0);
		THISPAGE.$_pay_state.setValue(0);
		THISPAGE.$_ar_state.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();