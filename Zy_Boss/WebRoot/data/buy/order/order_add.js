var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var od_type = $("#od_type").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'buy/order/temp_list/'+od_type;
var querysumurl = config.BASEPATH+'buy/order/temp_sum/'+od_type;
var querysizeurl = config.BASEPATH+'buy/order/temp_size/'+od_type;

var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-290,_width = $(parent).width()-2;
var showMode = {
	display:function(mode){
		var ids = $("#grid").getDataIDs();
		if(ids == undefined || null == ids || ids.length == 0){
			if(mode != 0){
				Public.tips({type: 1, content : "未添加数据，不能切换模式"});
				return;
			}
		}
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/order/temp_size_title/'+od_type,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var Choose = {
	importDraft:function(){
		commonDia = $.dialog({
			title : '选择草稿',
			content : 'url:'+config.BASEPATH+'buy/order/to_list_draft_dialog/'+od_type,
			data : {},
			width : 680,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_supply_code").val(selected.od_supply_code);
					$("#od_depot_code").val(selected.od_depot_code);
					$("#supply_name").val(selected.supply_name);
					$("#depot_name").val(selected.depot_name);
					$("#od_make_date").val(selected.od_make_date);
					$("#od_delivery_date").val(selected.od_delivery_date);
					$("#od_handnumber").val(selected.od_handnumber);
					$("#od_manager").val(selected.od_manager);
					$("#od_remark").val(selected.od_remark);
					Choose.doImportDraft(selected);
				}
			},
			cancel:true
		});
	},
	doImportDraft:function(rowData){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/order/temp_import_draft/'+od_type,
			data:{od_number:rowData.od_number},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : "导入成功！"});
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	doImport:function(){
		isRefresh = false;
		var priceType = THISPAGE.$_priceType.getValue();
		var sp_rate = $("#sp_rate").val();
		var od_supply_code = $("#od_supply_code").val();
		if(od_supply_code == ""){
			Public.tips({type: 2, content : '请选择供应商!'});
			return;
		}
		$.dialog({ 
		   	id:'import',
		   	title:'盘点机导入',
		   	content:'url:'+config.BASEPATH+'common/to_barcodeimport',
		   	data : {postData:{priceType:priceType,sp_rate:sp_rate},
					saveSuccUrl:config.BASEPATH+"buy/order/temp_import/"+od_type
		   		},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:700,
		   	height:470,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	lock:true,
		   	close: function () {
		   		if(isRefresh){
		   			showMode.refresh();
		   		}
			}
		});
	},
	selectProduct:function(){
		var priceType = THISPAGE.$_priceType.getValue();
		var sp_rate = $("#sp_rate").val();
		var od_supply_code = $.trim($("#od_supply_code").val());
		var od_depot_code = $.trim($("#od_depot_code").val());
		if (od_supply_code == '') {
			Public.tips({type: 2, content : "请先选择供应商！"});
	        return;
	    }
		if (od_depot_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		$.dialog({
			title : '商品录入',
			content : 'url:'+config.BASEPATH+'buy/order/to_select_product/'+od_type,
			data : {priceType:priceType,sp_rate:sp_rate,dp_code:od_depot_code,searchContent:$.trim($("#pd_no").val())},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	barCode:function(){
		var CurrentMode = $("#CurrentMode").val();
		if(CurrentMode == 1){//若是尺码模式调整到列表模式
			showMode.display(0);
		}
		var barcode = $.trim($("#barcode").val());
		var barcode_amount = $.trim($("#barcode_amount").val());
		if(barcode == ""){
			return;
		}
		if (barcode_amount == "" || isNaN(barcode_amount)) {
			Public.tips({type: 2, content : '请正确输入数量!'});
	        $('#barcode_amount').select();
	        return;
	    }
		var od_supply_code = $("#od_supply_code").val();
		var od_depot_code = $("#od_depot_code").val();
		if(od_supply_code == ""){
			Public.tips({type: 2, content : '请选择供应商!'});
			return;
		}
		if(od_depot_code == ""){
			Public.tips({type: 2, content : '请选择仓库!'});
			return;
		}
		var priceType = THISPAGE.$_priceType.getValue();
		var params = "";
		params += "barcode="+barcode;
		params += "&amount="+barcode_amount;
		params += "&priceType="+priceType;
		params += "&sp_rate="+$("#sp_rate").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"buy/order/temp_save_bybarcode/"+od_type,
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data.result == 3){//条码不存在
						Choose.barcodeNotExist(barcode);
						return;
					}
					needSizeRefresh = true;
					$("#barcode").val("");
					var temp = data.data.temp;
					//列表模式
					if(data.data.result == 1){//update
						var rowData = $("#grid").jqGrid("getRowData", temp.odl_id);
						rowData.odl_amount = temp.odl_amount;
						rowData.odl_unitmoney = rowData.odl_amount * rowData.odl_unitprice;
						rowData.odl_retailmoney = rowData.odl_amount * rowData.odl_retailprice;
						$("#grid").delRowData(temp.odl_id);
						$("#grid").addRowData(temp.odl_id, rowData, 'first');
						THISPAGE.gridTotal();
					}else if(data.data.result == 2){//add
						$("#grid").addRowData(temp.odl_id, temp, 'first');
						THISPAGE.gridTotal();
					}
					//汇总模式
			    	var ids = $("#sumGrid").jqGrid('getDataIDs');
	            	var oldSumRowData = null;
	            	var sum_id = null;
					for(var i=0;i < ids.length;i++){
						var sumRowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(sumRowData.pd_code == temp.pd_code && sumRowData.odl_pi_type == '0'){
							oldSumRowData = sumRowData;
							sum_id = ids[i];
							break;
						}
					}
					if(oldSumRowData != null){
						oldSumRowData.odl_amount = parseInt(oldSumRowData.odl_amount) + parseInt(barcode_amount);
						oldSumRowData.odl_unitmoney = oldSumRowData.odl_amount * oldSumRowData.odl_unitprice;
						oldSumRowData.odl_retailmoney = oldSumRowData.odl_amount * oldSumRowData.odl_retailprice;
						$("#sumGrid").jqGrid('setRowData', sum_id, oldSumRowData);
						THISPAGE.sumGridTotal();
					}else{
						$("#sumGrid").addRowData(temp.odl_id, temp, 'first');
						THISPAGE.sumGridTotal();
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	barcodeNotExist:function(barcode){
		$("#barcode").val("无此商品").select();
		var BarCodeCheckNotPass = document.getElementById("BarCodeCheckNotPass");
		if(BarCodeCheckNotPass.style.display == 'none'){
			BarCodeCheckNotPass.style.display = '';
			
			var barCodeDiv=$("#BarCodeCheckNotPass");
			var barCodeDivTop=$("#barcode_amount").offset().top
				,barCodeDivLeft=$("#barcode_amount").offset().left;
			barCodeDiv.css(
			{
				"top":barCodeDivTop-150
				,"left":barCodeDivLeft+30
			}
			);
		}
		var CheckNotPassText = document.getElementById("CheckNotPassText");
		if($("#CheckNotPassTextShow").val().indexOf(barcode+";") > -1){
			return;
		}
		CheckNotPassText.innerHTML = CheckNotPassText.innerHTML + "<p>" + barcode + "</p>";
		$("#CheckNotPassTextShow").val($("#CheckNotPassTextShow").val()+barcode+";");
	}
};

var Utils = {
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
					$("#sp_rate").val(selected.sp_rate);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'buy'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemarkById:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/order/temp_updateRemarkById',
			data:{odl_id:rowid,odl_remark:Public.encodeURI(remark)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					var ids = $("#sumGrid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(row.odl_pd_code == rowData.odl_pd_code && row.odl_pi_type == rowData.odl_pi_type){
							$("#sumGrid").jqGrid('setRowData',ids[i],{odl_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateRemarkByPdCode:function(pd_code,remark,odl_pi_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/order/temp_updateRemarkByPdCode/'+od_type,
			data:{odl_pd_code:pd_code,odl_remark:Public.encodeURI(remark),odl_pi_type:odl_pi_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var ids = $("#grid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#grid").jqGrid("getRowData", ids[i]);
						if(row.odl_pd_code == pd_code && row.odl_pi_type == odl_pi_type){
							$("#grid").jqGrid('setRowData',ids[i],{odl_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateAmount:function(rowid,amount){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/order/temp_updateAmount',
			data:{odl_id:rowid,odl_amount:amount},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					needSizeRefresh = true;
					needSumRefresh = true;
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updatePrice:function(pd_code,unitPrice,odl_pi_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'buy/order/temp_updatePrice/'+od_type,
			data:{odl_pd_code:pd_code,odl_unitprice:unitPrice,odl_pi_type:odl_pi_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					showMode.refresh();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_update : function(id,type){
		var priceType = THISPAGE.$_priceType.getValue();
		var sp_rate = $("#sp_rate").val();
		var od_depot_code = $.trim($("#od_depot_code").val());
		if (od_depot_code == '') {
			Public.tips({type: 2, content : "请先选择仓库！"});
	        return;
	    }
		var params = {};
 		var rowData;
 		if(type == '0'){
 			rowData = $("#grid").jqGrid("getRowData", id);
 			params.pd_code = rowData.odl_pd_code;
 		}else if(type == '1'){
			rowData = $("#sizeGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.pd_code;
		}else if(type == '2'){
			rowData = $("#sumGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.odl_pd_code;
		}
 		params.odl_pi_type = rowData.odl_pi_type;
 		params.priceType = priceType;
 		params.sp_rate = sp_rate;
 		params.dp_code = od_depot_code;
		$.dialog({
			title : '修改商品',
			content : 'url:'+config.BASEPATH+'buy/order/to_temp_update/'+od_type,
			data : params,
			width : 850,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'buy/order/temp_del',
					data:{"odl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							needSizeRefresh = true;
							needSumRefresh = true;
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_delByPiCode: function(rowId,type){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
		 		var params = {};
		 		var rowData;
	    		if(type == '1'){
	    			rowData = $("#sizeGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.pd_code;
	    			params.odl_pi_type = rowData.odl_pi_type;
	    			params.cr_code = rowData.cr_code;
	    			params.br_code = rowData.br_code;
	    		}else if(type == '2'){
	    			rowData = $("#sumGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.odl_pd_code;
	    			params.odl_pi_type = rowData.odl_pi_type;
	    		}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'buy/order/temp_delByPiCode/'+od_type,
					data:params,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							showMode.refresh();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'buy/order/temp_clear/'+od_type,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						$("#sumGrid").clearGridData();
						THISPAGE.sumGridTotal();
						if($("#CurrentMode").val() == '1'){
							showMode.refresh();
						}else{
							needSizeRefresh = true;
						}
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(isdraft){
		if (!$("#form1").valid()) {
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择货号！'});
			return;
		}
		$("#od_isdraft").val(isdraft);
		$("#btn-save").attr("disabled",true);
		$("#btn-save-draft").attr("disabled",true);
		var saveUrl = config.BASEPATH+"buy/order/save";
		if($("#od_id").val() != undefined){
			saveUrl = config.BASEPATH+"buy/order/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
				$("#btn-save-draft").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.od_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatGift :function(val, opt, row){
		if(row.odl_pi_type == 1){
			return '赠品';
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#od_make_date").val(config.TODAY);
		$("#od_delivery_date").val(config.TODAY);
		this.$_priceType = $("#priceTypeTd").cssRadio({ callback: function($_obj){
			 pricrType = $_obj.find("input").val();
			if(pricrType == 1){
				$("#sp_rate").attr("disabled",true);
			}else{
				$("#sp_rate").attr("disabled",false);
			}
		}});
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var odl_amount=grid.getCol('odl_amount',false,'sum');
    	var odl_unitmoney=grid.getCol('odl_unitmoney',false,'sum');
    	var odl_retailmoney=grid.getCol('odl_retailmoney',false,'sum');
    	grid.footerData('set',{odl_amount:odl_amount,odl_unitmoney:odl_unitmoney,odl_retailmoney:odl_retailmoney});
    	$("#od_amount").val(odl_amount);
    	$("#od_money").val(PriceLimit.formatByEnter(odl_unitmoney));
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var odl_amount=grid.getCol('odl_amount',false,'sum');
    	var odl_unitmoney=grid.getCol('odl_unitmoney',false,'sum');
    	var odl_retailmoney=grid.getCol('odl_retailmoney',false,'sum');
    	grid.footerData('set',{odl_amount:odl_amount,odl_unitmoney:odl_unitmoney,odl_retailmoney:odl_retailmoney});
    	$("#od_amount").val(odl_amount);
    	$("#od_money").val(PriceLimit.formatByEnter(odl_unitmoney));
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'odl_pd_code', index: 'odl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'odl_amount', index: 'odl_amount', width: 70,align:'right',sorttype: 'int',editable:true},
	    	{label:'进货价',name: 'odl_unitprice', index: 'odl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter,editable:PriceLimit.hasEnter()},
	    	{label:'进货金额',name: 'odl_unitmoney', index: 'odl_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'odl_retailprice', index: 'odl_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'odl_retailmoney', index: 'odl_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'odl_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'odl_pi_type', index: 'odl_pi_type', width: 100,hidden:true},
	    	{label:'折扣率',name: 'odl_rate', index: 'odl_rate', width: 70,align:'right',sorttype: 'int',formatter: PriceLimit.formatByEnter_Sell},
	    	{label:'备注',name: 'odl_remark', index: 'odl_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'odl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'odl_amount' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					} 
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.temp_updateAmount(rowid, parseInt(value));
					return parseInt(value);
				}else if(cellname == 'odl_unitprice' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					handle.temp_updatePrice(rowData.odl_pd_code, parseFloat(value).toFixed(2), rowData.odl_pi_type);
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'odl_remark' && self.selectRow.value != value){
					handle.temp_updateRemarkById(rowid, value);
				}
				
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'odl_amount' && self.selectRow.value != value){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					rowData.odl_unitmoney = rowData.odl_amount * rowData.odl_unitprice;
					rowData.odl_retailmoney = rowData.odl_amount * rowData.odl_retailprice;
					rowData.odl_rate = rowData.odl_unitprice / rowData.odl_retailprice;
					$("#grid").jqGrid('setRowData', rowid, rowData);
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	initSumGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'',name: 'odl_pd_code', index: 'odl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'odl_amount', index: 'odl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'进货价',name: 'odl_unitprice', index: 'odl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter,editable:PriceLimit.hasEnter()},
	    	{label:'进货金额',name: 'odl_unitmoney', index: 'odl_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'odl_retailprice', index: 'odl_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'odl_retailmoney', index: 'odl_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'odl_pi_type', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'odl_pi_type', index: 'odl_pi_type', width: 100,hidden:true},
	    	{label:'折扣率',name: 'odl_rate', index: 'odl_rate', width: 70,align:'right',sorttype: 'int',formatter: PriceLimit.formatByEnter_Sell},
	    	{label:'备注',name: 'odl_remark', index: 'odl_remark', width: 180,editable:true}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'odl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '2');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(cellname == 'odl_unitprice' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					handle.temp_updatePrice(rowData.odl_pd_code, parseFloat(value).toFixed(2), rowData.odl_pi_type);
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'odl_remark' && self.selectRow.value != value){
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					handle.temp_updateRemarkByPdCode(rowData.odl_pd_code, value, rowData.odl_pi_type);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'odl_cr_code', width: 80},
	    	{label:'杯型',name: 'br_name', index: 'odl_br_code', width: 80},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'进货价',name: 'unit_price', index: 'unit_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter,editable:PriceLimit.hasEnter()},
	    	{label:'进货金额',name: 'unit_money', index: 'unit_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'retail_price', index: 'retail_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'retail_money', index: 'retail_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'赠品',name: 'gift', index: 'gift', width: 60,align:'center',formatter: handle.formatGift},
	    	{label:'',name: 'odl_pi_type', index: 'odl_pi_type', width: 100,hidden:true}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
			height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 225 + (dyns.length * 32),2);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '1');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(cellname == 'unit_price' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#sizeGrid").jqGrid("getRowData", rowid);
					handle.temp_updatePrice(rowData.pd_code, parseFloat(value).toFixed(2), rowData.odl_pi_type);
					return parseFloat(value).toFixed(2);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			Choose.doImport();
		});
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save(0);
		});
		$('#btn-save-draft').on('click', function(e){
			e.preventDefault();
			handle.save(1);
		});
		$('#btn-import-draft').on('click', function(e){
			e.preventDefault();
			Choose.importDraft();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        //修改-列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	handle.temp_update(id, '0');
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
		
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).odl_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-trash');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGrid').off('click','.operating .ui-icon-trash');
		$('#sizeGrid').off('click','.operating .ui-icon-image');
        //修改-尺码模式
        $('#sizeGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '1');
        });
        //修改-汇总模式
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '2');
        });
		
		 //删除-汇总模式
		$('#sumGrid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '2');
		});
		//删除-尺码模式
		$('#sizeGrid').on('click', '.operating .ui-icon-trash',function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '1');
		});
		
		$('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sumGrid").jqGrid("getRowData", id).odl_pd_code);
        });
	}
};

THISPAGE.init();