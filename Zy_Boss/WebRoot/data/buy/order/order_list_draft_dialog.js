var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var od_type = $("#od_type").val();
var queryurl = config.BASEPATH+'buy/order/page';
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var Utils = {
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'buy'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#od_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'od_supply_code',label:'',index: 'od_supply_code',hidden:true},
	    	{name: 'od_depot_code',label:'',index: 'od_depot_code',hidden:true},
	    	{name: 'od_handnumber',label:'',index: 'od_handnumber',hidden:true},
	    	{name: 'od_delivery_date',label:'',index: 'od_delivery_date',hidden:true},
	    	{name: 'od_number',label:'单据编号',index: 'od_number',width:150},
	    	{name: 'supply_name',label:'供货厂商',index: 'supply_name',width:120},
	    	{name: 'depot_name',label:'收货仓库',index: 'depot_name',width:120},
	    	{name: 'od_manager',label:'经办人',index: 'od_manager',width:100},
	    	{name: 'od_amount',label:'订货数量',index: 'od_amount',width:80,align:'right'},
	    	{name: 'od_make_date',label:'制单日期',index: 'od_make_date',width:80},
	    	{name: 'od_maker',label:'制单人',index: 'od_maker',width:100},
	    	{name: 'od_remark',label:'备注',index: 'od_remark',width:180,hidden:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'od_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				api.close();
            }
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'od_type='+$("#od_type").val();
		params += '&od_isdraft=1';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&od_supply_code='+$("#od_supply_code").val();
		params += '&od_depot_code='+$("#od_depot_code").val();
		params += '&od_manager='+Public.encodeURI($("#od_manager").val());
		params += '&od_number='+Public.encodeURI($("#od_number").val());
		return params;
	},
	reset:function(){
		$("#od_supply_code").val("");
		$("#od_depot_code").val("");
		$("#od_number").val("");
		$("#od_manager").val("");
		$("#supply_name").val("");
		$("#depot_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};

function doSelect(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择草稿！"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	return rowData;
}

THISPAGE.init();