var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'buy/report/pageEnterDetails';
var _height = $(parent).height()-285,_width = $(parent).width()-200;

var handle = {
	formatType:function(val, opt, row){
		if(val == 0){
			return '进货单';
		}else if(val == 1){
			return '退货单';
		}
		return val;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 40, fixed:true, formatter: Public.operImgFmatter, align:'center'},
	    	{label:'日期',name: 'et_make_date',index:'et_make_date',width:100},
	    	{label:'单据编号',name: 'etl_number',index:'etl_number',width:135},
	    	{label:'单据类型',name: 'etl_type',index:'etl_type',width:100,formatter: handle.formatType,align:'center'},
	    	{label:'供应商',name: 'supply_name',index:'supply_name',width:100},
	    	{label:'仓库',name: 'depot_name',index:'depot_name',width:100},
	    	{label:'商品货号',name: 'pd_no',index:'pd_no',width:100},
	    	{label:'',name: 'etl_pd_code',index:'etl_pd_code',width:100,hidden:true},
	    	{label:'商品名称',name: 'pd_name',index:'pd_name',width:100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60,align:'center'},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60,align:'center'},
	    	{label:'数量',name: 'etl_amount', index: 'etl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'单价',name: 'etl_unitprice', index: 'etl_unitprice', width: 80,align:'right',formatter: PriceLimit.formatByEnter},
	    	{label:'金额',name: 'etl_unitmoney', index: 'etl_unitmoney', width: 80,align:'right',formatter: PriceLimit.formatByEnter},
	    	{label:'零售价',name: 'etl_retailprice', index: 'etl_retailprice', width: 80,align:'right',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'etl_retailmoney', index: 'etl_retailmoney', width: 80,align:'right',formatter: PriceLimit.formatBySell},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'季节',name: 'pd_season', index: 'pd_season', width: 80,align:'center'},
	    	{label:'年份',name: 'pd_year', index: 'pd_year', width: 80,align:'center'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'etl_id'  //图标ID
			},
			loadComplete: function(data){
		    	$('#grid').footerData('set',{et_make_date:"合计"});
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+Public.encodeURI($("#pd_season").val());
		params += '&pd_year='+$("#pd_year").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#et_manager").val("");
		$("#supply_name").val("");
		$("#et_supply_code").val("");
		$("#depot_name").val("");
		$("#et_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).etl_pd_code);
        });
	}
}
THISPAGE.init();