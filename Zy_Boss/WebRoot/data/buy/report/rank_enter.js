var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BUY14;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/report/pageEnterRank';
var _height = $(parent).height()-298,_width = $(parent).width()-192;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_type = $("#td_type").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			$("#type").val(type);
			$('#grid').showCol("code");
			$('#grid').hideCol("operate");
			switch(type){
				case 'brand' : 
					$('#grid').setLabel("code","品牌编号");
					$('#grid').setLabel("name","品牌名称");
					break ;
				case 'type' : 
					$('#grid').setLabel("code","类别编号");
					$('#grid').setLabel("name","类别名称");
					break ;
				case 'supply' : 
					$('#grid').setLabel("code","供应商编号");
					$('#grid').setLabel("name","供应商名称");
					break ;
				case 'product' :
					$('#grid').setLabel("code","商品货号");
					$('#grid').setLabel("name","商品名称");
					$('#grid').showCol("operate");
					break;
				case 'season' :
					$('#grid').setLabel("code","季节编号");
					$('#grid').setLabel("name","季节名称");
					$('#grid').hideCol("code");
					break;
				default :
					break ;
			}	
			THISPAGE.reloadData();
		}});
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 40, fixed:true, formatter: Public.operImgFmatter, hidden:true,align:'center'},
	    	{label:'供应商编号',name: 'code',index:'code',width:100},	
	    	{label:'供应商名称',name: 'name',index:'name',width:100},	
	    	{label:'总计数量',name: 'amount',index:'amount',width:80, align:'right'},
	    	{label:'总计金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatMoney},	
	    	{label:'总计零售金额',name: 'sell_money',index:'sell_money',width:100, align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'code'  //图标ID
			},
			loadComplete: function(data){
				var footer = $('#grid').footerData()
				footer.code = "合计";
				delete footer.operate;
		    	$('#grid').footerData('set',footer);
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#et_manager").val("");
		$("#supply_name").val("");
		$("#et_supply_code").val("");
		$("#depot_name").val("");
		$("#et_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).code);
        });
	}
}
THISPAGE.init();