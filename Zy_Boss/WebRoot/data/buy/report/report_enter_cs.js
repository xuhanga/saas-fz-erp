var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BUY14;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/report/listEnterReport_csb';
var _height = $(parent).height()-180,_width = $(parent).width()-32;
var api = frameElement.api;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initGrid2();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initParams();
		Public.buildSeasonCombo("span_pd_season", "pd_season");
		Public.buildYearCombo("span_pd_year", "pd_year");
	},
	initParams:function(){
		if(api == undefined){
			return;
		}
		var params = api.data;
		$("#begindate").val($.trim(params.begindate));
		$("#enddate").val($.trim(params.enddate));
		$("#supply_name").val($.trim(params.supply_name));
		$("#et_supply_code").val($.trim(params.et_supply_code));
		$("#depot_name").val($.trim(params.depot_name));
		$("#et_depot_code").val($.trim(params.et_depot_code));
		$("#et_manager").val($.trim(params.et_manager));
		$("#bd_name").val($.trim(params.bd_name));
		$("#bd_code").val($.trim(params.bd_code));
		$("#tp_name").val($.trim(params.tp_name));
		$("#tp_code").val($.trim(params.tp_code));
		$("#pd_season").val($.trim(params.pd_season));
		$("#pd_year").val($.trim(params.pd_year));
		$("#pd_code").val($.trim(params.pd_code));
		$("#pd_name").val($.trim(params.pd_name));
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var amount=grid.getCol('amount',false,'sum');
    	var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{code:'合计',amount:amount,money:money});
    },
    grid2Total:function(){
    	var grid=$('#grid2');
    	var amount=grid.getCol('amount',false,'sum');
    	var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{code:'合计',amount:amount,money:money});
    },
	initGrid:function(){
		var colModel = [
	    	{label:'颜色编号',name: 'code',index:'code',width:70},	
	    	{label:'颜色名称',name: 'name',index:'name',width:80},	
        	{label:'数量',name: 'amount',index:'amount',width:70, align:'right',sorttype: 'float'},	
        	{label:'金额',name: 'money',index:'money',width:90, align:'right',formatter: PriceLimit.formatMoney,sorttype: 'float'},	
	    	{label:'数量占比(%)',name: 'amount_proportion',index:'amount_proportion',width:100, align:'right',sorttype: 'float'},	
	    	{label:'金额占比(%)',name: 'money_proportion',index:'money_proportion',width:100, align:'right',sorttype: 'float'}	
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?type=color'+THISPAGE.buildParams(),
            loadonce:true,
			datatype: 'json',
			width: _width/2-10,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				var footer = $('#grid').footerData();
				var ids = $("#grid").getDataIDs();
				for (var i = 0; i < ids.length; i++) {
					var rowData = $("#grid").jqGrid("getRowData", ids[i]);
					if(footer.amount != 0){
						$('#grid').setCell(ids[i],'amount_proportion',PriceLimit.formatMoney(100*rowData.amount/footer.amount));
					}
					if(footer.money != 0){
						$('#grid').setCell(ids[i],'money_proportion',PriceLimit.formatMoney(100*rowData.money/footer.money));
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initGrid2:function(){
		var colModel = [
	    	{label:'尺码编号',name: 'code',index:'code',width:70},	
	    	{label:'尺码名称',name: 'name',index:'name',width:80},	
        	{label:'数量',name: 'amount',index:'amount',width:70, align:'right',sorttype: 'float'},	
        	{label:'金额',name: 'money',index:'money',width:90, align:'right',formatter: PriceLimit.formatMoney,sorttype: 'float'},	
	    	{label:'数量占比(%)',name: 'amount_proportion',index:'amount_proportion',width:100, align:'right',sorttype: 'float'},	
	    	{label:'金额占比(%)',name: 'money_proportion',index:'money_proportion',width:100, align:'right',sorttype: 'float'}	
	    ];
		$('#grid2').jqGrid({
            url:queryurl+'?type=size'+THISPAGE.buildParams(),
            loadonce:true,
			datatype: 'json',
			width: _width/2-10,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page2',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.grid2Total();
				var footer = $('#grid2').footerData();
				var ids = $("#grid2").getDataIDs();
				for (var i = 0; i < ids.length; i++) {
					var rowData = $("#grid2").jqGrid("getRowData", ids[i]);
					if(footer.amount != 0){
						$('#grid2').setCell(ids[i],'amount_proportion',PriceLimit.formatMoney(100*rowData.amount/footer.amount));
					}
					if(footer.money != 0){
						$('#grid2').setCell(ids[i],'money_proportion',PriceLimit.formatMoney(100*rowData.money/footer.money));
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		params += '&br_code='+api.data.br_code;
		return params;
	},
	reset:function(){
		$("#pd_code").val("");$("#pd_name").val("");
		$("#et_manager").val("");
		$("#et_supply_code").val("");$("#supply_name").val("");
		$("#et_depot_code").val("");$("#depot_name").val("");
		$("#bd_code").val("");$("#bd_name").val("");
		$("#tp_code").val("");$("#tp_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param='type=color'+THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	reloadData2:function(){
		var param='type=size'+THISPAGE.buildParams();
		$("#grid2").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
			THISPAGE.reloadData2();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
			THISPAGE.reloadData2();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
}
THISPAGE.init();