var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BUY14;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/report/pageEnterRank';
var _height = $(parent).height()-298,_width = $(parent).width()-192;

var handle = {
	formatAmountProportion:function(val, opt, row){
		if(val == "合计"){
			return "";
		}
		var userData = $("#grid").getGridParam('userData');
		if(userData.amount == 0){
			return "0.00";
		}
		return PriceLimit.formatMoney(row.amount/userData.amount*100);
	},
	formatMoneyProportion:function(val, opt, row){
		if(val == "合计"){
			return "";
		}
		var userData = $("#grid").getGridParam('userData');
		if(userData.money == 0){
			return "0.00";
		}
		return PriceLimit.formatMoney(row.money/userData.money*100);
	},
	formatDifferMoney:function(val, opt, row){
		return PriceLimit.formatMoney(row.sell_money - row.money);
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_type = $("#td_type").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			$("#type").val(type);
			$('#grid').showCol("code");
			switch(type){
				case 'brand' : 
					$('#grid').setLabel("code","品牌编号");
					$('#grid').setLabel("name","品牌名称");
					break ;
				case 'type' : 
					$('#grid').setLabel("code","类别编号");
					$('#grid').setLabel("name","类别名称");
					break ;
				case 'supply' : 
					$('#grid').setLabel("code","供应商编号");
					$('#grid').setLabel("name","供应商名称");
					break ;
				case 'product' :
					$('#grid').setLabel("code","商品货号");
					$('#grid').setLabel("name","商品名称");
					break;
				case 'season' :
					$('#grid').setLabel("code","季节编号");
					$('#grid').setLabel("name","季节名称");
					$('#grid').hideCol("code");
					break;
				default :
					break ;
			}	
			THISPAGE.reloadData();
		}});
		this.initSeason();
		this.initYear();
	},
	initSeason:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/dict/list?dtl_upcode=KEY_SEASON",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					var seasons = data.data;
					seasons.unshift({dtl_code:"",dtl_name:"全部"});
					$('#span_pd_season').combo({
						value : 'dtl_code',
						text : 'dtl_name',
						width : 207,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								if(data.dtl_code != ""){
									$("#pd_season").val(data.dtl_name);
								}else{
									$("#pd_season").val("");
								}
							}
						}
					}).getCombo().loadData(seasons,["dtl_name",$("#pd_season").val(),0]);
				}
			}
		 });
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
		    {Code:"",Name:"全部"},
			{Code:""+parseInt(currentYear+1)+"",Name:""+parseInt(currentYear+1)+""},
			{Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""},
			{Code:""+parseInt(currentYear-4)+"",Name:""+parseInt(currentYear-4)+""},
			{Code:""+parseInt(currentYear-5)+"",Name:""+parseInt(currentYear-5)+""}
		];
		$('#span_pd_year').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.Code);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var colModel = [
	    	{label:'供应商编号',name: 'code',index:'code',width:100},	
	    	{label:'供应商名称',name: 'name',index:'name',width:100},	
	    	{label:'总计数量',name: 'amount',index:'amount',width:80, align:'right'},
	    	{label:'总计金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatMoney},	
	    	{label:'总计零售金额',name: 'sell_money',index:'sell_money',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'数量占比(%)',name: 'amount_proportion',index:'amount_proportion',width:100, align:'right',formatter: handle.formatAmountProportion},
	    	{label:'金额占比(%)',name: 'money_proportion',index:'money_proportion',width:100, align:'right',formatter: handle.formatMoneyProportion},
	    	{label:'进销差额',name: 'differ_money',index:'differ_money',width:100, align:'right',formatter: handle.formatDifferMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'code'  //图标ID
			},
			loadComplete: function(data){
				var footer = $('#grid').footerData()
				footer.code = "合计";
				footer.amount_proportion = "合计";
				footer.money_proportion = "合计";
		    	$('#grid').footerData('set',footer);
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&et_supply_code='+$("#et_supply_code").val();
		params += '&et_depot_code='+$("#et_depot_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&et_manager='+Public.encodeURI($("#et_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#et_manager").val("");
		$("#supply_name").val("");
		$("#et_supply_code").val("");
		$("#depot_name").val("");
		$("#et_depot_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();