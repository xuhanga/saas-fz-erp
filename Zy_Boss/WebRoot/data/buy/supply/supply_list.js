var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.BUY10;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/supply/list';
var pdata = {};
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 380;
		var width = 650;
		if(oper == 'add'){
			title = '新增供货单位';
			url = config.BASEPATH+"buy/supply/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			title = '修改供货单位';
			url = config.BASEPATH+"buy/supply/to_update?sp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId,sp_code){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'buy/supply/del',
					data:{"sp_id":rowId,"sp_code":sp_code},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{name: 'operate',label:'操作',width: 60, formatter: Public.operFmatter,align:'center', title: false,sortable:false},
			{name: 'sp_code',label:'供应商编号', index: 'sp_code', width:80, title: false,fixed:true},
			{name: 'sp_name',label:'供应商名称', index: 'sp_name', width: 140, title: false},
			{name: 'spi_man',label:'联系人', index: 'spi_man', width: 100, title: false,align:'left'},
			{name: 'spi_tel',label:'电话', index: 'spi_tel', width: 140, title: false},
			{name: 'spi_mobile',label:'手机号码', index: 'spi_mobile', width: 100, title: false},
			{label:'期初欠款',name: 'sp_init_debt', index: 'sp_init_debt',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{name: 'sp_rate',label:'折扣率', index: 'sp_rate',sorttype: 'float',width:80, title: false,align:'right'},
			{name: 'sp_buy_cycle',label:'进货周期', index: 'sp_buy_cycle', width:80, title: false,align:'center'},
			{name: 'sp_settle_cycle',label:'结账周期', index: 'sp_settle_cycle', width: 80, title: false,align:'center'},
			{name: 'spi_earnest',label:'保证金', index: 'spi_earnest', width:80, title: false,align:'right'},
			{name: 'spi_deposit',label:'装修押金', index: 'spi_deposit', width:80, title: false,align:'right'},
			{name: 'spi_addr',label:'地址', index: 'spi_addr', width:150, title: false,align:'left'},
			{name: 'spi_remark',label:'备注信息', index: 'spi_remark', width: 180, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:999,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("ar_upcode") && data.ar_upcode != ''){
				param += "&ar_upcode="+data.ar_upcode;
			}
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			THISPAGE.reloadData(pdata);
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			var rowData = $("#grid").jqGrid("getRowData",id);
			var sp_code= rowData.sp_code;
			handle.del(id,sp_code);
		});
	}
}
THISPAGE.init();