var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.MONEY07;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'buy/supply/list';
var _height = $(parent).height()-294,_width = $(parent).width()-32;
var pdata = {};
var handle = {
	formatActualDebt:function(val, opt, row){
		return PriceLimit.formatMoney(row.sp_payable-row.sp_payabled-row.sp_prepay);
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-settle" title="结算明细">&#xe64c;</i>&nbsp;';
		html_con += '<i class="iconfont i-hand ui-icon-dealings" title="往来明细">&#xe64f;</i>';
		html_con += '</div>';
		return html_con;
	},
	viewDealings:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'buy/dealings/to_list',
			data: {sp_code:rowData.sp_code},
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	viewSettleDetail:function(rowid){
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'buy/settle/to_list_detail',
			data: {sp_code:rowData.sp_code},
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var sp_init_debt=grid.getCol('sp_init_debt',false,'sum');
		var sp_payable=grid.getCol('sp_payable',false,'sum');
    	var sp_payabled=grid.getCol('sp_payabled',false,'sum');
    	var sp_prepay=grid.getCol('sp_prepay',false,'sum');
    	grid.footerData('set',{sp_code:'合计：',
    		sp_init_debt:sp_init_debt,
    		sp_payable:sp_payable,
    		sp_payabled:sp_payabled,
    		sp_prepay:sp_prepay,
    		actual_debt:''});
    },
	initGrid:function(){
		var colModel = [
			{name: 'operate',label:'操作',width: 60, formatter: handle.operFmatter,align:'center', title: false,sortable:false},
			{name: 'sp_code',label:'供应商编号', index: 'sp_code', width:80, title: false,fixed:true},
			{name: 'sp_name',label:'供应商名称', index: 'sp_name', width: 140, title: false},
			{name: 'spi_man',label:'联系人', index: 'spi_man', width: 100, title: false,align:'left'},
			{name: 'spi_tel',label:'电话', index: 'spi_tel', width: 140, title: false},
			{name: 'spi_mobile',label:'手机号码', index: 'spi_mobile', width: 100, title: false},
			{label:'期初欠款',name: 'sp_init_debt', index: 'sp_init_debt',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'应付金额',name: 'sp_payable', index: 'sp_payable',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'已付金额',name: 'sp_payabled', index: 'sp_payabled',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'预付款余额',name: 'sp_prepay', index: 'sp_prepay',sorttype: 'float',width:80, align:'right',formatter: PriceLimit.formatMoney},
			{label:'实际欠款',name: 'actual_debt', index: 'actual_debt',sorttype: 'float',width:80, align:'right',formatter: handle.formatActualDebt}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += "searchContent="+Public.encodeURI($.trim($('#SearchContent').val()));
		return params;
	},
	reloadData:function(){
		var param=this.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#grid').on('click', '.operating .ui-icon-settle', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.viewSettleDetail(id);
		});
		$('#grid').on('click', '.operating .ui-icon-dealings', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.viewDealings(id);
		});
	}
}
THISPAGE.init();