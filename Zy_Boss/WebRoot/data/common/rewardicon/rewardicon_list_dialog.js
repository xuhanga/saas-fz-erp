var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var multiselect = false;
if(undefined != api.data.multiselect){
	multiselect = api.data.multiselect;
}
var _height = api.config.height-88,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'common/rewardicon/list';

var handle = {
	operFmatter : function(val, opt, row){
		var html_con = '';
		html_con += '<div class="icons">';
		html_con += '<i class="iconfont '+row.ri_style+'">'+row.ri_icon+'</i>';
		html_con += '</div>';
		return html_con;
	}
};

var dblClick = false;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
		    {label:'图标',name: 'icon',width: 120, formatter: handle.operFmatter,align:'center'},
			{label:'',name: 'ri_icon', index: 'ri_icon', width: 100, hidden: true},
	    	{label:'',name: 'ri_style', index: 'ri_style', width: 150, hidden: true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:multiselect,//多选
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'ri_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				if(!multiselect){
					dblClick = true;
					api.close();
				}
            }
	    });
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		
	}
};
function doSelect(){
	if(multiselect){//多选
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		if(ids == null || ids.length == 0){
			return false;
		}
		var result = [];
		for (var i = 0; i < ids.length; i++) {
			result.push($("#grid").jqGrid("getRowData", ids[i]));
		}
		return result;
		
	}else{//单选
		var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
		if(selectedId == null || selectedId == ""){
			return false;
		}
		var rowData = $("#grid").jqGrid("getRowData", selectedId);
		return rowData;
	}
}
THISPAGE.init();