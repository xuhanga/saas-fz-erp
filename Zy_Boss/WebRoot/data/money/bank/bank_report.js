var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY13;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'money/bank/list';
var handle = {
	run : function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		$.dialog({
			title:false,
			content : 'url:'+config.BASEPATH+"money/bank/to_run",
			data: {ba_code:rowData.ba_code},
			width : false,
			height : false,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	operFmatter : function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="明细">&#xe608;</i>';
		html_con += '</div>';
		return html_con;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 40, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'ba_code',label:'编号',index: 'ba_code',width:80},
	    	{name: 'ba_name',label:'名称',index: 'ba_name',width:120},
	    	{name: 'ba_init_balance',label:'期初余额',index: 'ba_init_balance',width:120,align:'right'},
	    	{name: 'ba_balance',label:'账户余额',index: 'ba_balance',width:120,align:'right'},
	    	{name: 'ba_remark',label:'备注',index: 'ba_remark',width:180}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ba_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		var param="searchContent="+Public.encodeURI($('#SearchContent').val());
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.run(id);
		});
	}
}
THISPAGE.init();