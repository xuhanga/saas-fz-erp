var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'money/bank/pageRun';
var _height = $(parent).height()-150,_width = $(parent).width()-34;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'br_number',label:'单据编号',index: 'br_number',width:80},
	    	{name: 'br_date',label:'日期',index: 'br_date',width:135},
	    	{name: 'bt_name',label:'类型',index: 'br_bt_code',width:120},
	    	{name: 'br_manager',label:'经办人',index: 'br_manager',width:100},
	    	{name: 'br_remark',label:'摘要',index: 'br_remark',width:180},
	    	{name: 'br_enter',label:'存款金额',index: 'br_enter',width:120,align:'right',formatter: PriceLimit.formatMoney},
	    	{name: 'br_out',label:'取款金额',index: 'br_out',width:120,align:'right',formatter: PriceLimit.formatMoney},
	    	{name: 'br_balance',label:'账户余额',index: 'br_balance',width:120,align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:100,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'br_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'ba_code='+api.data.ba_code;
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
}
THISPAGE.init();