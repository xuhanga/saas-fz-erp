var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY14;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'money/bank/pageRun';
var _height = $(parent).height()-280,_width = $(parent).width()-195;

var Utils = {
	doQueryBank : function(){
		commonDia = $.dialog({
			title : '选择银行账户',
			content : 'url:'+config.BASEPATH+'money/bank/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ba_code").val(selected.ba_code);
				$("#ba_name").val(selected.ba_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ba_code").val(selected.ba_code);
					$("#ba_name").val(selected.ba_name);
				}
			},
			cancel:true
		});
	}
};


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'br_number',label:'单据编号',index: 'br_number',width:140},
	    	{name: 'br_date',label:'日期',index: 'br_date',width:135},
	    	{name: 'bank_name',label:'银行账户',index: 'ba_name',width:135},
	    	{name: 'bt_name',label:'类型',index: 'br_bt_code',width:120},
	    	{name: 'br_manager',label:'经办人',index: 'br_manager',width:100},
	    	{name: 'br_remark',label:'摘要',index: 'br_remark',width:180},
	    	{name: 'br_enter',label:'存款金额',index: 'br_enter',width:120,align:'right',formatter: PriceLimit.formatMoney},
	    	{name: 'br_out',label:'取款金额',index: 'br_out',width:120,align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:100,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'br_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var br_enter=grid.getCol('br_enter',false,'sum');
		var br_out=grid.getCol('br_out',false,'sum');
    	grid.footerData('set',{br_number:'本页合计：',br_enter:br_enter,br_out:br_out});
    },
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ba_code='+$("#ba_code").val();
		params += '&br_number='+Public.encodeURI($("#ba_code").val());
		return params;
	},
	reset:function(){
		$("#ba_code").val("");
		$("#ba_name").val("");
		$("#br_number").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();