var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.MONEY17;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'money/expense/listReport';
var querydetailurl = config.BASEPATH+'money/expense/listReportDetail';
var _height = $(parent).height()-285,_width = $(parent).width()-32;

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'expense'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ep_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ep_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initDetailGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_type = $("#td_type").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			$("#type").val(type);
			switch(type){
				case 'shop' : 
					$('#grid').setLabel("name","店铺名称");
					break ;
				case 'property' : 
					$('#grid').setLabel("name","费用类型");
					break ;
				case 'month' : 
					$('#grid').setLabel("name","月份");
					break ;
				default :
					break ;
			}	
			THISPAGE.reloadData();
		}});
	},
	initGrid:function(){
		var colModel = [
	    	{label:'',name: 'code',index:'code',width:60,hidden:true},	
	    	{label:'店铺',name: 'name',index:'name',width:140},
	    	{label:'支出金额',name: 'money',index:'money',width:100, align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			loadonce:true,
			width: _width/2-180,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			pgtext: '',
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			rowNum:9999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'code'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
		    	var ids = $("#grid").getDataIDs();
				if(ids.length>0){
					$("#grid tr:eq(1)").trigger("click");
				}else{
					$("#detailGrid").clearGridData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				var rowData = $('#grid').jqGrid('getRowData', rowid);
				var params = '';
				params += 'begindate='+$("#begindate").val();
				params += '&enddate='+$("#enddate").val();
				var type = $("#type").val();
				if(type == "shop"){
					params += '&ep_shop_code='+rowData.code;
				}else if(type == 'property'){
					params += '&epl_mp_code='+rowData.code;
					params += '&ep_shop_code='+$("#ep_shop_code").val();
				}else if(type == 'month'){
					params += '&month='+rowData.code;
					params += '&ep_shop_code='+$("#ep_shop_code").val();
				}
				$("#detailGrid").jqGrid('setGridParam',{datatype:"json",url:querydetailurl+'?'+params}).trigger("reloadGrid");
			}
	    });
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{name:'合计：',money:money});
    },
    gridDetailTotal:function(){
    	var grid=$('#detailGrid');
		var epl_money=grid.getCol('epl_money',false,'sum');
    	grid.footerData('set',{epl_sharedate:'合计：',epl_money:epl_money});
    },
	initDetailGrid:function(){
		var colModel = [
	    	{label:'日期',name: 'epl_sharedate',index:'epl_sharedate',width:120},
	    	{label:'费用类型',name: 'mp_name',index:'mp_name',width:120},
	    	{label:'支出金额',name: 'epl_money',index:'epl_money',width:100, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'备注',name: 'epl_remark',index:'epl_remark',width:160},
	    ];
		$('#detailGrid').jqGrid({
			datatype: 'local',
			width: _width/2,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#detailPage',//分页
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			pgtext: '',
			multiselect:false,//多选
			viewrecords: true,
			page: 1, //只有一页
			rowNum:9999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id:'epl_id'
			},
			loadComplete: function(data){
				THISPAGE.gridDetailTotal();
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ep_shop_code='+$("#ep_shop_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#ep_shop_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();