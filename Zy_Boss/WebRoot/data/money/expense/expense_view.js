var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var ep_number = $("#ep_number").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'money/expense/detail_list/'+ep_number;
var _height = $(parent).height()-243,_width = $(parent).width()-2;

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+$("#ep_number").val();
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"money/expense/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	doReverse:function(){
		$.dialog.confirm('确定要反审核单据吗？', function(){
			$("#btn-reverse").attr("disabled",true);
			var params = "";
			params += "number="+ep_number;
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"money/expense/reverse",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "反审核成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reverse").attr("disabled",false);
	            }
	        });
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ep_id;
		pdata.ep_number=data.ep_number;
		pdata.ep_ar_state=data.ep_ar_state;
		pdata.ep_ar_date=data.ep_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	doPrint:function(){
		var number = $("#ep_number").val();
		$.dialog({ 
		   	id:'print_model_select',
		   	title:'单据打印模板',
		   	data:{'number':number},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:350,
		   	height:300,
		   	fixed:false,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'sys/print/to_list/18'
	    });
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		var ep_ar_state = $("#ep_ar_state").val();
        if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
        if('view' == api.data.oper && ep_ar_state == '1'){
        	$("#btn-reverse").show();
        }
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var epl_money=grid.getCol('epl_money',false,'sum');
    	grid.footerData('set',{epl_mp_code:'合计：',epl_money:epl_money.toFixed(2)});
    	$("#ep_money").val(epl_money.toFixed(2));
    },
    Merger:function (CellName) {
        var ids = $("#grid").getDataIDs();
        var length = ids.length;
        for (var i = 0; i < length; i++) {
            var before = $("#grid").jqGrid('getRowData', ids[i]);
            //定义合并行数
            var rowSpanTaxCount = 1;
            for (var j = i + 1; j <= length; j++) {
                //和上边的信息对比 如果值一样就合并行数+1 然后设置rowspan 让当前单元格隐藏
                var end = $("#grid").jqGrid('getRowData', ids[j]);
                if (before[CellName] == end[CellName]) {
                    rowSpanTaxCount++;
                    $("#grid").setCell(ids[j], CellName, '', { display: 'none' });
                } else {
                    rowSpanTaxCount = 1;
                    break;
                }
                $("#" + CellName + "" + ids[i] + "").attr("rowspan", rowSpanTaxCount);
            }
        }
    },
	initGrid:function(){
		var self=this;
		var colModel = [
	    	{label:'编号',name: 'epl_mp_code', index: 'epl_mp_code', width: 100,
	    		cellattr: function(rowId, tv, rawObject, cm, rdata) {return 'id=\'epl_mp_code' + rowId + "\'";}},
	    	{label:'费用名称',name: 'mp_name', index: 'mp_name', width: 100,
	    		cellattr: function(rowId, tv, rawObject, cm, rdata) {return 'id=\'mp_name' + rowId + "\'";}},
	    	{label:'分摊日期',name: 'epl_sharedate', index: 'epl_sharedate', width: 100},
	    	{label:'金额',name: 'epl_money', index: 'epl_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'备注',name: 'epl_remark', index: 'epl_remark', width: 180}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'epl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				THISPAGE.Merger("epl_mp_code");
				THISPAGE.Merger("mp_name");
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-reverse').on('click', function(e){
			e.preventDefault();
			handle.doReverse();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();