var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.MONEY19;
var _menuParam = config.OPTIONLIMIT;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'money/streamsettle/pageBill';
var _height = $(parent).height()-169,_width = $(parent).width()-32;

var handle = {
	formatType:function(val, opt, row){
		if(val == 0){
			return '期初单';
		}else if(val == 1){
			return '批发物流单';
		}
		return val;
	},
	formatPayState:function(val, opt, row){
		if(val == 0){
			return '未结算';
		}else if(val == 1){
			return '部分结算';
		}else if(val == 2){
			return '已结算';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var seb_money=grid.getCol('seb_money',false,'sum');
		var seb_discount_money=grid.getCol('seb_discount_money',false,'sum');
    	var seb_payabled=grid.getCol('seb_payabled',false,'sum');
    	var seb_payable=grid.getCol('seb_payable',false,'sum');
    	grid.footerData('set',{
    		seb_money:seb_money,
    		seb_discount_money:seb_discount_money,
    		seb_payabled:seb_payabled,
    		seb_payable:seb_payable});
    },
	initGrid:function(){
		var colModel = [
	    	{label:'单据编号',name: 'seb_number',index: 'seb_number',width:150},
	    	{label:'单据类型',name: 'seb_type',index: 'seb_type',width:80,formatter: handle.formatType},
	    	{label:'物流公司名称',name: 'stream_name',index: 'stream_name',width:120},
	    	{label:'关联单据',name: 'seb_join_number',index: 'seb_join_number',width:150},
	    	{label:'日期',name: 'seb_date',index: 'seb_date',width:80},
	    	{label:'总计金额',name: 'seb_money',index: 'seb_money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'优惠金额',name: 'seb_discount_money',index: 'seb_discount_money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'已付金额',name: 'seb_payabled',index: 'seb_payabled',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'应付金额',name: 'seb_payable',index: 'seb_payable',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'结算状态',name: 'seb_pay_state',index: 'seb_pay_state',width:80,formatter: handle.formatPayState}
	    	
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'seb_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&seb_se_code='+api.data.se_code;
		params += '&seb_number='+Public.encodeURI($("#seb_number").val());
		return params;
	},
	reset:function(){
		$("#seb_number").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#btn-close').on('click', function(e){
			e.preventDefault();
			api.close();
		});
	}
}
THISPAGE.init();