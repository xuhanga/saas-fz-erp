var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var st_number = $("#st_number").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'money/streamsettle/detail_list/'+st_number;
var _height = $(parent).height()-285,_width = $(parent).width()-2;

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+st_number;
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"money/streamsettle/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	doReverse:function(){
		$.dialog.confirm('确定要反审核单据吗？', function(){
			$("#btn-reverse").attr("disabled",true);
			var params = "";
			params += "number="+st_number;
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"money/streamsettle/reverse",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "反审核成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-reverse").attr("disabled",false);
	            }
	        });
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.st_id;
		pdata.st_number=data.st_number;
		pdata.st_ar_state=data.st_ar_state;
		pdata.st_ar_date=data.st_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var totalNegative = 0.0;
var totalPositive = 0.0;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		var st_ar_state = $("#st_ar_state").val();
        if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
        if('view' == api.data.oper && st_ar_state == '1'){
        	$("#btn-reverse").show();
        }
        if('readonly' == api.data.oper){
        	$("#btn-print").hide();
        }
	},
	gridTotal:function(){
		var grid=$('#grid');
		var stl_payable=grid.getCol('stl_payable',false,'sum');
		var stl_payabled=grid.getCol('stl_payabled',false,'sum');
		var stl_discount_money_yet=grid.getCol('stl_discount_money_yet',false,'sum');
		var stl_unpayable=grid.getCol('stl_unpayable',false,'sum');
		var stl_discount_money=grid.getCol('stl_discount_money',false,'sum');
		var stl_real_pay=grid.getCol('stl_real_pay',false,'sum');
    	grid.footerData('set',{stl_bill_number:'合计：',
    		stl_payable:stl_payable,
    		stl_payabled:stl_payabled,
    		stl_discount_money_yet:stl_discount_money_yet,
    		stl_unpayable:stl_unpayable,
    		stl_discount_money:stl_discount_money,
    		stl_real_pay:stl_real_pay
    	});
    },
	initGrid:function(){
		var self=this;
		var colModel = [
	    	{label:'单据编号',name: 'stl_bill_number', index: 'stl_bill_number', width: 140},
	    	{label:'应付总金额',name: 'stl_payable', index: 'stl_payable', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'已付金额',name: 'stl_payabled', index: 'stl_payabled', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'已优惠金额',name: 'stl_discount_money_yet', index: 'stl_discount_money_yet', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'未付金额',name: 'stl_unpayable', index: 'stl_unpayable', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'优惠金额',name: 'stl_discount_money', index: 'stl_discount_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'实付金额',name: 'stl_real_pay', index: 'stl_real_pay', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatMoney},
	    	{label:'备注',name: 'stl_remark', index: 'stl_remark', width: 180}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'stl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-reverse').on('click', function(e){
			e.preventDefault();
			handle.doReverse();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();