var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.REPORT17;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'report/brand_run';
var handle = {
	formatRate:function(val,opt,row){
		if(null != row.shl_amount && 0 != row.shl_amount){
			return parseFloat(row.sd_amount/row.shl_amount).toFixed(2);
		}else{
			return 0.00;
		}
	}
}
var Utils = {
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'buy'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#depot_code").val(selected.dp_code);
				$("#depot_name").val(selected.dp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#depot_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_tp_code = [];
				var pd_tp_name = [];
				for(var i=0;i<selected.length;i++){
					pd_tp_code.push(selected[i].tp_code);
					pd_tp_name.push(selected[i].tp_name);
				}
				$("#tp_code").val(pd_tp_code.join(","));
				$("#tp_name").val(pd_tp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_tp_code = [];
					var pd_tp_name = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
					}
					$("#tp_code").val(pd_tp_code.join(","));
					$("#tp_name").val(pd_tp_name.join(","));
					}
				},
				cancel:true
			});
		},
		doQueryBrand : function(){
			commonDia = $.dialog({
				title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#bd_code").val(pd_bd_code.join(","));
				$("#bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#bd_code").val(pd_bd_code.join(","));
					$("#bd_name").val(pd_bd_name.join(","));
					}
				},
				cancel:true
			});
		}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		this.initYear();
		this.initDict();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initType:function(type){
		$("#tab_type").children("a").each(function(){
			$(this).removeClass("on");
		});
		$("#btn_"+type).addClass("on")
		$("#type").val(type);
		$("#grid").clearGridData(false);
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
			        {code:"",name:"全部"},
					{code:""+parseInt(currentYear+1)+"",name:""+parseInt(currentYear+1)+""},
					{code:""+parseInt(currentYear)+"",name:""+parseInt(currentYear)+""},
					{code:""+parseInt(currentYear-1)+"",name:""+parseInt(currentYear-1)+""},
					{code:""+parseInt(currentYear-2)+"",name:""+parseInt(currentYear-2)+""},
					{code:""+parseInt(currentYear-3)+"",name:""+parseInt(currentYear-3)+""},
					{code:""+parseInt(currentYear-4)+"",name:""+parseInt(currentYear-4)+""},
					{code:""+parseInt(currentYear-5)+"",name:""+parseInt(currentYear-5)+""},
					{code:""+parseInt(currentYear-6)+"",name:""+parseInt(currentYear-6)+""},
					{code:""+parseInt(currentYear-7)+"",name:""+parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'code',
			text: 'name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.code);
				}
			}
		}).getCombo();
	  },
	  initDict:function(){
		  seasonCombo = $('#span_season').combo({
			value: 'dtl_code',
			text: 'dtl_name',
			width :206,
			height: 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					if(data.dtl_name == "全部"){
						$("#pd_season").val("");
					}else{
						$("#pd_season").val(data.dtl_name);
					}
				}
			}
		}).getCombo(); 
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			data:"",
			cache:false,
			dataType:"json",
			success:function(data){
				seasonInfo = data.data.seasonitemsquery;
				seasonCombo.loadData(seasonInfo);
			}
		});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'编号',name: 'bd_code',index:'bd_code',hidden:true},
			{label:'名称',name: 'bd_name',index:'bd_name',width:100, align:'left'},
			{label:'采购进',name: 'etl_amount',index:'etl_amount',width:70,align:'center',sortable:false},
			{label:'采购退',name: 'etb_amount',index:'etb_amount',width:70, align:'center'},
			{label:'批发出',name: 'sel_amount',index:'sel_amount',width:70,align:'right'},
			{label:'批发退',name: 'seb_amount',index:'seb_amount',width:70,align:'right'},
			{label:'仓库调出',name: 'acl_dp_out',index:'acl_dp_out',width:70,align:'right'},
			{label:'仓库调入',name: 'acl_dp_in',index:'acl_dp_in',width:70,align:'right'},
			{label:'店铺调出',name: 'acl_sp_out',index:'acl_sp_out',width:70,align:'right'},
			{label:'店铺调入',name: 'acl_sp_in',index:'acl_sp_in',width:70,align:'right'},
			{label:'库存调整',name: 'ajl_amount',index:'ajl_amount',width:70,align:'right'},
			{label:'配货出',name: 'atl_amount',index:'atl_amount',width:70,align:'right'},
			{label:'配货退',name: 'atb_amount',index:'atb_amount',width:70,align:'right'},
			{label:'自营发',name: 'wtl_amount',index:'wtl_amount',width:70,align:'right'},
			{label:'自营退',name: 'wtb_amount',index:'wtb_amount',width:70,align:'right'},
			{label:'零售',name: 'shl_amount',index:'shl_amount',width:70,align:'right'},
			{label:'库存',name: 'sd_amount',index:'sd_amount',width:70,align:'right'}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页'
			page:1,
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: false,
            userDataOnFooter: false,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'sd_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&depot_code='+$("#depot_code").val();
		params += '&type='+$("#type").val();
		return params;
	},
	reset:function(){
		$("#depot_name").val("");
		$("#depot_code").val("");
		$("#bd_code").val("");
		$("#tp_code").val("");
		yearCombo.selectByIndex(0);
		seasonCombo.selectByIndex(0);
		THISPAGE.$_date.setValue(0);
		THISPAGE.initType("brand");
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var depot_code = $("#depot_code").val();
			if(null != depot_code && "" != depot_code){
				THISPAGE.reloadData();
			}else{
				Public.tips({type: 1, content : "请选择仓库!"});
			}
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			var depot_code = $("#depot_code").val();
			if(null != depot_code && "" != depot_code){
				THISPAGE.reloadData();
			}else{
				Public.tips({type: 1, content : "请选择仓库!"});
			}
		});
		$("#btn_brand").on('click',function(){
			THISPAGE.initType("brand");
		});
		$("#btn_type").on('click',function(){
			THISPAGE.initType("type");
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
