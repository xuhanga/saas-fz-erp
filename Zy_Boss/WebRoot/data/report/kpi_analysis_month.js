var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.REPORT15;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH + 'report/kpi_analysis_month';
var _height = $(parent).height()-258,_width = $(parent).width()-202;

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#shopCode").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	}
}

var handle = {
	formatMoney:function(value){
		if($.trim(value) == '' || isNaN(value)){
			return value;
		}
		return parseFloat(value).toFixed(2); 
	}
};

var THISPAGE = {
    init: function () {
        this.initDom();
        this.initGrid();
        this.initEvent();
    },
    initDom: function () {
    	this.initYear();
    },
    initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
			{Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""}
		];
		$('#span_year').combo({
			value: 'Code',
			text: 'Name',
			width :106,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#year").val(data.Code);
				}
			}
		}).getCombo().loadData(data);
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var planmoney=grid.getCol('planmoney',false,'sum');
    	var sellamount=grid.getCol('sellamount',false,'sum');
    	var sellmoney=grid.getCol('sellmoney',false,'sum');
    	var costmoney=grid.getCol('costmoney',false,'sum');
    	var lost_money=grid.getCol('lost_money',false,'sum');
    	var profits=grid.getCol('profits',false,'sum');
    	var receiveamount=grid.getCol('receiveamount',false,'sum');
    	var dealcount=grid.getCol('dealcount',false,'sum');
    	var sellmoney_vip=grid.getCol('sellmoney_vip',false,'sum');
    	var vipcount=grid.getCol('vipcount',false,'sum');
    	var cardcount=grid.getCol('cardcount',false,'sum');
    	var ecouponcount=grid.getCol('ecouponcount',false,'sum');
		grid.footerData('set', {
			month : '合计：',
			planmoney : planmoney,
			sellamount : sellamount,
			sellmoney : sellmoney,
			costmoney : costmoney,
			lost_money : lost_money,
			profits : profits,
			receiveamount : receiveamount,
			dealcount : dealcount,
			sellmoney_vip : sellmoney_vip,
			vipcount : vipcount,
			cardcount : cardcount,
			ecouponcount : ecouponcount
		});
    },
    initGrid: function () {
        var colModel = [
            {label: "月份", name: 'month', index: 'month', width: 70,align:'center'},
            {label: '计划金额',name: 'planmoney', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '销售数量',name: 'sellamount', index: '', width:80, align: 'right'},
            {label: '销售金额',name: 'sellmoney', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '完成率(%)',name: 'complete_rate', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '成本',name: 'costmoney', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '抹零',name: 'lost_money', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '利润',name: 'profits', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '利润率(%)',name: 'profits_rate', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '费用支出',name: 'expense_money', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '净利润',name: 'net_profits', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '接待人数',name: 'receiveamount', index: '', width:80, align: 'right'},
            {label: '成交单数',name: 'dealcount', index: '', width:80, align: 'right'},
            {label: '成交率(%)',name: 'deal_rate', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '连带率',name: 'joint_rate', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '平均件单价',name: 'avg_price', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '平均客单价',name: 'avg_sellprice', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '会员消费',name: 'sellmoney_vip', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '会员占比(%)',name: 'sellmoney_vip_proportion', index: '', width:80, align: 'right',formatter: handle.formatMoney},
            {label: '新增会员',name: 'vipcount', index: '', width:80, align: 'right'},
            {label: '发储值卡',name: 'cardcount', index: '', width:80, align: 'right'},
            {label: '发优惠券',name: 'ecouponcount', index: '', width:80, align: 'right'}
        ];
        $('#grid').jqGrid({
            loadonce: true, 
            datatype: 'local',
            width: _width,
			height: _height,
            gridview: true,
            onselectrow: false,
            colModel: colModel,
            rownumbers: true,//行号
            pgbuttons:true,
            recordtext: '{0} - {1} 共 {2} 条',
            rowNum: -1,//每页条数
            cmTemplate: {sortable:false,title:false},
            shrinkToFit: false,//表格是否自动填充
            footerrow: true,
            userDataOnFooter: true,
            jsonReader: {
            	root: 'data',
                repeatitems: false,
                id: 'id'
            },
            loadComplete: function (data) {
            	THISPAGE.gridTotal();
            }
        });
    },
    initParam:function(){
    	var params = '';
		params += 'year='+$("#year").val();
		params += '&shopCode='+$("#shopCode").val();
		return params;
    },
    reloadData: function (data) {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl + "?" + THISPAGE.initParam()}).trigger("reloadGrid");
    },
    reset:function(){
		$("#shop_name").val("");
		$("#shopCode").val("");
	},
    initEvent: function () {
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
    }
}
THISPAGE.init();