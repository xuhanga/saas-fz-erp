var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.REPORT17;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'report/sell_max';
var queryurllist = config.BASEPATH+'report/sell_max_list';
var needSumRefresh = false;
var needListRefresh = false;
var handle = {
	formatRate:function(val,opt,row){
		if(null != row.shl_amount && 0 != row.shl_amount){
			return parseFloat(row.sd_amount/row.shl_amount).toFixed(2);
		}else{
			return 0.00;
		}
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#shl_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#shl_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_tp_code = [];
				var pd_tp_name = [];
				for(var i=0;i<selected.length;i++){
					pd_tp_code.push(selected[i].tp_code);
					pd_tp_name.push(selected[i].tp_name);
				}
				$("#tp_code").val(pd_tp_code.join(","));
				$("#tp_name").val(pd_tp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_tp_code = [];
					var pd_tp_name = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
					}
					$("#tp_code").val(pd_tp_code.join(","));
					$("#tp_name").val(pd_tp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#bd_code").val(pd_bd_code.join(","));
				$("#bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#bd_code").val(pd_bd_code.join(","));
					$("#bd_name").val(pd_bd_name.join(","));
				}
			},
			cancel:true
		});
	},
	checkObject:function(chkObject, chkFlg) {
    	if ($.trim(chkObject.value) != "") {
        	var tell = chkObject.value;
        	tell = tell.replace("￥", "").replace("$", "");
        	var exrep;
        	switch (chkFlg) {
	            case 0: //浮点数,可带逗号的数字
	                exrep = /^[-+]?(\d+\,)*\d+(\.\d+)?$/;
	            case 1: //非负浮点数,可带逗号的数字.
	                exrep = /^(\d+\,)*\d+(\.\d+)?$/;
	                break;
	            case 2: //正整数+0
	                exrep = /^\d+$/;
	                break;
	            default:;
	        }
	        if (chkFlg == 2) {
	            if (!exrep.test(tell)) {//请输入最多带2位小数的数字
	                alert("输入类型不正确,请输入正整数和0 ");
	                chkObject.value = "0";
	                document.getElementById(chkObject.id).focus();
	                return false;
	            }
	        }
    	}
    	return true;
	},
	//列表和尺码模式切换
	change_mode:function(obj){
		var tble_list_mode = document.getElementById("list_mode");
		var tble_sum_mode = document.getElementById("sum_mode");
		var sum_mode_button = document.getElementById("sum_mode_button");
		var list_mode_button = document.getElementById("list_mode_button");
		if(obj == 0){// 汇总模式
			tble_sum_mode.style.display = '';
			tble_list_mode.style.display = 'none';
			sum_mode_button.className='t-btn btn-bl on';
			list_mode_button.className='t-btn btn-bc';
			Utils.ajaxGetSum();
			//刷新数据
			$("#CurrentMode").val(0);
		}else if(obj == 1){//列表模式
			tble_list_mode.style.display = ''; 
			tble_sum_mode.style.display = "none";
			list_mode_button.className='t-btn btn-bc on';
			sum_mode_button.className='t-btn btn-bl';
			Utils.ajaxGetList();
			$("#CurrentMode").val(1);
		}
	},
	ajaxGetSum:function(){//汇总模式
    	if(!needSumRefresh){
    		return;
    	}
    	THISPAGE.reloadData();
    	needSumRefresh = false;
    },
    ajaxGetList : function(){// 明细模式
    	if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadListData();
    	needListRefresh = false;
    },
    refreshCurrentPageDate:function(){
		var currentMode = $("#CurrentMode").val();
		if(currentMode == '0' ){//汇总模式
			needSumRefresh = true;
			needListRefresh = true;
			// 加载汇总模式数据
			Utils.ajaxGetSum();
		} else if(currentMode == '1'){//明细模式
			needSumRefresh = true;
			needListRefresh = true;
			//加载明细模式数据
			Utils.ajaxGetList();
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initListGrid();
		this.initEvent();
		this.initYear();
		this.initDict();
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_chkss_day = $("#chkss_day").cssCheckbox();
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
			        {year_Code:"",year_Name:"全部"},
					{year_Code:""+parseInt(currentYear+1)+"",year_Name:""+parseInt(currentYear+1)+""},
					{year_Code:""+parseInt(currentYear)+"",year_Name:""+parseInt(currentYear)+""},
					{year_Code:""+parseInt(currentYear-1)+"",year_Name:""+parseInt(currentYear-1)+""},
					{year_Code:""+parseInt(currentYear-2)+"",year_Name:""+parseInt(currentYear-2)+""},
					{year_Code:""+parseInt(currentYear-3)+"",year_Name:""+parseInt(currentYear-3)+""},
					{year_Code:""+parseInt(currentYear-4)+"",year_Name:""+parseInt(currentYear-4)+""},
					{year_Code:""+parseInt(currentYear-5)+"",year_Name:""+parseInt(currentYear-5)+""},
					{year_Code:""+parseInt(currentYear-6)+"",year_Name:""+parseInt(currentYear-6)+""},
					{year_Code:""+parseInt(currentYear-7)+"",year_Name:""+parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'year_Code',
			text: 'year_Name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.year_Code);
				}
			}
		}).getCombo();
	  },
	  initDict:function(){
		  seasonCombo = $('#span_season').combo({
			value: 'dtl_code',
			text: 'dtl_name',
			width :206,
			height: 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					if(data.dtl_name == "全部"){
						$("#pd_season").val("");
					}else{
						$("#pd_season").val(data.dtl_name);
					}
				}
			}
		}).getCombo();
		styleCombo = $('#span_style').combo({
			value: 'dtl_code',
			text: 'dtl_name',
			width :206,
			height: 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					if(data.dtl_name == "全部"){
						$("#pd_style").val("");
					}else{
						$("#pd_style").val(data.dtl_name);
					}
				}
			}
		}).getCombo(); 
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			data:"",
			cache:false,
			dataType:"json",
			success:function(data){
				//季节
				seasonInfo = data.data.seasonitemsquery;
				seasonCombo.loadData(seasonInfo);
				//款式
				styleInfo = data.data.styleitemsquery;
				styleCombo.loadData(styleInfo);
			}
		});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'货号',name: 'pd_no',index:'pd_no',width:100, align:'left'},
			{label:'名称',name: 'pd_name',index:'pd_name',width:100, align:'left'},
			{label:'品牌',name: 'bd_name',index:'bd_name',width: 100,align:'left', title: false},
			{label:'类别',name: 'tp_name',index:'tp_name',width: 100,align:'left', title: false},
			{label:'季节',name: 'pd_season',index:'pd_season',width:70,align:'center',sortable:false},
			{label:'年份',name: 'pd_year',index:'pd_year',width:70, align:'center'},
			{label:'款式',name: 'pd_style',index:'pd_style',width:60,align:'center', title: false},
			{label:'数量',name: 'shl_amount',index:'shl_amount',width:40,align:'right'},
			{label:'库存',name: 'sd_amount',index:'sd_amount',width:40,align:'right'},
			{label:'存销比',name: 'sh_rate',index:'sh_rate',width:70,align:'right',formatter: handle.formatRate},
			{label:'零售价',name: 'shl_sell_price',index:'shl_sell_price',width:70,align:'right'},
			{label:'上市天数',name: 'pd_days',index:'shl_sysdate',width:60,align:'right'},
			{label:'上市时间',name: 'shl_sysdate',index:'shl_sysdate',width:100,align:'right'},
			{label:'最后销售时间',name: 'shl_date',index:'shl_date',width:100,align:'right'}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
                total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initListGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'货号',name: 'pd_no',index:'pd_no',width:100, align:'left'},
			{label:'名称',name: 'pd_name',index:'pd_name',width:100, align:'left'},
			{label:'品牌',name: 'bd_name',index:'bd_name',width: 100,align:'left', title: false},
			{label:'类别',name: 'tp_name',index:'tp_name',width: 100,align:'left', title: false},
			{label:'季节',name: 'pd_season',index:'pd_season',width:70,align:'center',sortable:false},
			{label:'年份',name: 'pd_year',index:'pd_year',width:70, align:'center'},
			{label:'款式',name: 'pd_style',index:'pd_style',width:60,align:'center', title: false},
			{label:'颜色',name: 'cr_name',index:'cr_name',width:75,align:'center', title: false},
            {label:'尺码',name: 'sz_name',index:'sz_name',width:75,align:'center', title: false},
            {label:'杯型',name: 'br_name',index:'br_name',width:60,align:'center', title: false},
			{label:'数量',name: 'shl_amount',index:'shl_amount',width:40,align:'right'},
			{label:'库存',name: 'sd_amount',index:'sd_amount',width:40,align:'right'},
			{label:'存销比',name: 'sh_rate',index:'sh_rate',width:70,align:'right',formatter: handle.formatRate},
			{label:'零售价',name: 'shl_sell_price',index:'shl_sell_price',width:70,align:'right'},
			{label:'上市天数',name: 'pd_days',index:'shl_sysdate',width:60,align:'right'},
			{label:'上市时间',name: 'shl_sysdate',index:'shl_sysdate',width:100,align:'right'},
			{label:'最后销售时间',name: 'shl_date',index:'shl_date',width:100,align:'right'}
	    ];
		$('#list_grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#list_page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
                total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&type=0';
		params += '&amount='+$("#amount").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&pd_season='+Public.encodeURI($("#pd_season").val());
		params += '&pd_style='+Public.encodeURI($("#pd_style").val());
		var chkss_day = THISPAGE.$_chkss_day.chkVal().join() ? "0" : "";
		params += '&chkss_day='+chkss_day;
		params += '&ss_day='+$("#ss_day").val();
		params += '&pd_no='+$("#pd_no").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#shl_shop_code").val("");
		$("#bd_code").val("");
		$("#bd_name").val("");
		$("#tp_code").val("");
		$("#tp_name").val("");
		$("#pd_no").val("");
		$("#amount").val(5);
		yearCombo.selectByIndex(0);
		seasonCombo.selectByIndex(0);
		styleCombo.selectByIndex(0);
		THISPAGE.$_date.setValue(0);
		$("#ss_day").val(90);
		THISPAGE.$_chkss_day.chkNot();
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	reloadListData:function(){
		var param=THISPAGE.initParam();
		$("#list_grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurllist+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Utils.refreshCurrentPageDate();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			Utils.refreshCurrentPageDate();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
