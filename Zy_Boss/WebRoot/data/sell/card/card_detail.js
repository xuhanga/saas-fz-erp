var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api;
var _height = api.config.height-92,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'sell/card/listDetail?cd_code='+api.data.cd_code;

var Utils = {
	formatType:function(val, opt, row){//0：期初 1：充值  2：减值  3：消费 4:换货
		if(val == "0"){
			return "期初";
		}else if(val == "1"){
			return "充值";
		}else if(val == "2"){
			return "减值";
		}else if(val == "3"){
			return "消费";
		}else if(val == "4"){
			return "换货";
		}else{
			return val;
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'cdl_cardcode',label:'储值卡号',index: 'cdl_cardcode',width:100},
			{name: 'cdl_type',label:'类型',index: 'cdl_type',width:60,formatter:Utils.formatType},
			{name: 'cdl_date',label:'操作日期',index: 'cdl_date',width:135},
			{name: 'cdl_money',label:'金额',index: 'cdl_money',width:70,align:'right'},
			{name: 'cdl_realcash',label:'实收现金',index: 'cdl_realcash',width:70,align:'right'},
			{name: 'cdl_bankmoney',label:'实收刷卡',index: 'cdl_bankmoney',width:70,align:'right'},
			{name: 'shop_name',label:'发放门店',index: 'cdl_shop_code',width:100},
			{name: 'ba_name',label:'现金账户',index: 'cdl_ba_code',width:100},
			{name: 'bank_name',label:'刷卡账户',index: 'cdl_bank_code',width:100},
			{name: 'cdl_manager',label:'操作人',index: 'cdl_manager',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'cdl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
	}
};

THISPAGE.init();