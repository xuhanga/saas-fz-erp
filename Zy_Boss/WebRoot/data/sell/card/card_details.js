var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SELL14;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/card/pageDetail';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#cdl_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	formatPlusMoney:function(val, opt, row){
		if(row.cdl_money > 0){
			return row.cdl_money;
		}else{
			return '';
		}
	},
	formatMinusMoney:function(val, opt, row){
		if(row.cdl_money < 0){
			return row.cdl_money;
		}else{
			return '';
		}
	},
	formatType:function(val, opt, row){//0：期初 1：充值  2：减值  3：消费 4:换货
		if(val == "0"){
			return "期初";
		}else if(val == "1"){
			return "充值";
		}else if(val == "2"){
			return "减值";
		}else if(val == "3"){
			return "消费";
		}else if(val == "4"){
			return "换货";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'cdl_cardcode',label:'储值卡号',index: 'cdl_cardcode',width:100},
			{name: 'cdl_type',label:'类型',index: 'cdl_type',width:60,formatter:Utils.formatType},
			{name: 'cdl_date',label:'操作日期',index: 'cdl_date',width:150},
			{name: 'plus_money',label:'增金额',index: 'cdl_money',width:70,align:'right',formatter:Utils.formatPlusMoney},
			{name: 'minus_money',label:'减金额',index: 'cdl_money',width:70,align:'right',formatter:Utils.formatMinusMoney},
			{name: 'shop_name',label:'发放门店',index: 'cdl_shop_code',width:100},
			{name: 'cdl_manager',label:'经办人',index: 'cdl_manager',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'cdl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&cdl_shop_code='+$("#cdl_shop_code").val();
		params += '&cdl_cardcode='+Public.encodeURI($("#cdl_cardcode").val());
		return params;
	},
	reset:function(){
		$("#cdl_shop_code").val("");
		$("#cdl_cardcode").val("");
		$("#sp_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
	}
}
THISPAGE.init();