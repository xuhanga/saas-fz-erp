var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sell/cashier/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.close()",200);
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",200);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ca_id;
		pdata.ca_em_code=data.ca_em_code;
		pdata.em_name=data.st_name;
		pdata.ca_erase=data.ca_erase;
		pdata.ca_maxmoney=data.ca_maxmoney;
		pdata.ca_minrate=data.ca_minrate;
		pdata.ca_days=data.ca_days;
		pdata.shop_name=$("#shop_name").val();
		pdata.ca_state=data.ca_state;
		pdata.em_name=empCombo.getText();
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initCombo();
		this.initEvent();
	},
	initParam:function(){
		
	},
	initCombo:function(){
		empCombo = $('#spanEmp').combo({
	        value: 'em_code',
	        text: 'em_name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#ca_em_code").val(data.em_code);
				}
			}
		}).getCombo();
		var sp_code = $("#ca_shop_code").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'base/emp/listCombo',
			data:{"sp_code":sp_code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					empCombo.loadData(data.data);
				}
			}
		});
	},
	initDom:function(){
		$("#ca_shop_code").val(api.data.shop_code);
		$("#shop_name").val(api.data.shop_name);
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();