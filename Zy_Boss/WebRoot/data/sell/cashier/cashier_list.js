var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SELL02;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/cashier/page';
var pdata = {};
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 300,width = 350;
		var selected = window.parent.frames["treeFrame"].selectedTreeNode;
		if(selected == undefined || selected.id == "0"){
			Public.tips({type: 2, content : '请选择店铺!'});
			return false;
		}
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"sell/cashier/to_add";
			data = {oper: oper, callback:this.callback};
			data.shop_code = selected.id;
			data.shop_name = selected.name;
		}else if(oper == 'edit'){
			height = 350;
			title = '修改';
			url = config.BASEPATH+"sell/cashier/to_update?ca_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	reset: function(rowIds){
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('确定重置密码吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sell/cashier/reset/' + rowIds,
					data:{'us_id':rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		
							Public.tips({type: 3, content : '重置成功!'});
						}else{
							Public.tips({type: 1, content : '重置失败!'});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }	
    },
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sell/cashier/del',
					data:{"ca_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : '删除失败!'});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 2, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	stateFmatter:function(val,opt,row){
		var html = "停用";
		if(val != 1){
			html = "正常";
		}
		return html;
	},
	doLimit:function(code){
		$.dialog({
			title : "权限设置",
			content : 'url:'+config.BASEPATH+'sell/cashier/to_limit',
			data: {"em_code":code},
			width : 400,
			height : 280,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				
			}
		});
	},
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i><i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i><i class="iconfont i-hand ui-icon-person" title="密码重置">&#xe63a;</i>';
		html_con += '</div>';
		return html_con;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 60, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'ca_state',label:'状态',index: 'ca_state',formatter:handle.stateFmatter,width:60},
	    	{name: 'ca_em_code',label:'编号',index: 'ca_em_code',width:60},
	    	{name: 'em_name',label:'名称',index: 'em_name',width:80},
	    	{name: 'ca_erase',label:'最高抹零',index: 'ca_erase',align:'right',width:70},
	    	{name: 'ca_maxmoney',label:'最高让利',index: 'ca_maxmoney',align:'right',width:70},
	    	{name: 'ca_minrate',label:'最低折扣',index: 'ca_minrate',align:'right',width:70},
	    	{name: 'ca_days',label:'零售天数',index: 'ca_days',align:'right',width:70},
	    	{name: 'shop_name',label:'所属店铺',index: 'ca_shop_code',width:150}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'ca_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(data){
		var param="";
		if(data != null && "" != data){
			param += "?1=1";
			if(data.hasOwnProperty("sp_code") && data.sp_code != ''){
				param += "&sp_code="+data.sp_code;
			}
			if(data.hasOwnProperty("searchContent") && data.searchContent != ''){
				param += "&searchContent="+Public.encodeURI(data.searchContent);
			}
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#SearchContent').on('keyup',function(){
			if (event.keyCode==13){
				$('#btn-search').click();
			}
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var selected = window.parent.frames["treeFrame"].selectedTreeNode;
			if(selected == undefined || selected.id == "0"){
				Public.tips({type: 2, content : '请选择店铺!'});
				return false;
			}
			var searchContent = $.trim($('#SearchContent').val());
			var pdata = {};
			pdata.searchContent = searchContent;
			pdata.sp_code=selected.id;
			THISPAGE.reloadData(pdata);
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		//新增
		$('#btn-limit').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var rowId = $("#grid").jqGrid('getGridParam', 'selrow');
			if(rowId != null && rowId != ""){
				var grid = $("#grid").jqGrid("getRowData", rowId);
				handle.doLimit(grid.ca_em_code);
			}else{
				Public.tips({type: 2, content : '请选择一条数据!'});
			}
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		//重置密码
		$('#grid').on('click', '.operating .ui-icon-person', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.reset(id);
		});
	}
}
THISPAGE.init();