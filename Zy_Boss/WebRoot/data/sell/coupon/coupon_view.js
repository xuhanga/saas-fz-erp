var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'sell/coupon/listDetail';

var Utils = {
};

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+$("#sc_number").val();
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
	    	type: "POST",
	        url: config.BASEPATH+"sell/coupon/approve",
	        data: params,
	        cache:false,
	        dataType:"json",
	        success: function (data) {
	        	if(undefined != data && data.stat == 200){
	        		Public.tips({type: 3, content : data.message});
	        		W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	stop:function(){
		commonDia = $.dialog({ 
		   	id:'approve_stop',
		   	title:'终止优惠券方案',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_stop',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doStop(ar_infos);
		   		}
		   	}
	    });
	},
	doStop:function(ar_infos){
		$("#btn-stop").attr("disabled",true);
		var params = "";
		params += "number="+$("#sc_number").val();
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"sell/coupon/stop",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-stop").attr("disabled",false);
            }
        });
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid($("#sc_type").val());
		this.initEvent();
	},
	initDom:function(){
		if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
		var sc_state = $("#sc_state").val();
		if(sc_state == 1){//审核通过、未终止
			$("#btn-stop").show();
		}
		var sc_type = $("#sc_type").val();
		if(sc_type == '0'){
			$("#detailDiv").css("display","none");
			$(".border").css("border-bottom","#ddd solid 1px");
		}else{
			$("#detailDiv").css("display","");
			$(".border").css("border-bottom","");
		}
		this.$_show_sysinfo = $("#show_sysinfo").cssCheckbox();
		$("#show_sysinfo").find("input").attr("disabled","disabled");
		this.$_show_chat = $("#show_chat").cssCheckbox();
		$("#show_chat").find("input").attr("disabled","disabled");
	},
    initGrid:function(sc_type){
		if(sc_type == '0'){
			return;
		}
		var type = '';
		if(sc_type == '1'){
			type='类别';
		}else if(sc_type == '2'){
			type='品牌';
		}else if(sc_type == '3'){
			type='商品';
		}
		var sc_number = $("#sc_number").val();
		var _height = $(parent).height()-370,_width = $(parent).width()-31;
		var colModel = [
	    	{label:type+'编号',name: 'code', index: 'code',width: 150, fixed:true, align: 'left'},
	    	{label:type+'名称',name: 'name', index: 'name', width: 200, fixed:true ,title: true, align: 'left'}
	    ];
		$('#grid').jqGrid({
			url:queryurl+'?sc_number='+sc_number+'&sc_type='+sc_type,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-stop').on('click', function(e){
			e.preventDefault();
			handle.stop();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();