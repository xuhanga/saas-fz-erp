var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SELL04;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/ecoupon/page';

var Utils = {
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ec_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'add'){
			url = config.BASEPATH+"sell/ecoupon/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			url = config.BASEPATH+"sell/ecoupon/to_update?ec_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'view'){
			url = config.BASEPATH+"sell/ecoupon/to_view?ec_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"sell/ecoupon/to_view?ec_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sell/card/del',
					data:{"cd_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    callback: function(data, oper, dialogWin){
		if(oper == "edit") {
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
		if(oper == "approve") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.ec_state == '0') {
            btnHtml += '<input type="button" value="审核" class="btn_sp" onclick="javascript:handle.operate(\'approve\',' + opt.rowId + ');" />';
        } else if (row.ec_state == '1') {
            btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
        } else if (row.ec_state == '2') {
            btnHtml += '<input type="button" value="修改" class="btn_xg" onclick="javascript:handle.operate(\'edit\',' + opt.rowId + ');" />';
        }
		btnHtml += '<input type="button" value="流程" class="btn_qh" onclick="javascript:listBillProcess(\''+row.ec_number+'\',\'t_sell_ecoupon\');" />';
		return btnHtml;
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '线上领取';
		}else if(val == 1){
			return '收银发放';
		}
		return val;
	},
	formatMode:function(val, opt, row){//1.全场2.品牌3.类别4.商品
		if(val == 1){
			return '全场';
		}else if(val == 2){
			return '品牌';
		}else if(val == 3){
			return '类别';
		}else if(val == 4){
			return '商品';
		}
		return val;
	},
	formatState:function(val, opt, row){//0：未审核；1：已审核2：已退回
		if(val == 0){
			return '未审核';
		}else if(val == 1){
			return '已通过';
		}else if(val == 2){
			return '已退回';
		}else if(val == 4){
			return '已终止';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		this.$_ar_state = $("#ar_state").cssRadio({ callback: function($_obj){
			$("#ec_ar_state").val($_obj.find("input").val());
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 100, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'ec_state',label:'状态',index: 'ec_state',width:70,formatter: handle.formatState},
	    	{name: 'ec_number',label:'单据编号',index: 'ec_number',width:160},
	    	{name: 'ec_sysdate',label:'制单日期',index: 'ec_sysdate',width:100},
	    	{name: 'ec_name',label:'方案名称',index: 'ec_name',width:120},
	    	{name: 'ec_type',label:'发放方式',index: 'ec_type',width:65,formatter: handle.formatType},
	    	{name: 'ec_mode',label:'发放条件',index: 'ec_mode',width:65,formatter: handle.formatMode},
	    	{name: 'ec_use_mode',label:'使用范围',index: 'ec_use_mode',width:65,formatter: handle.formatMode},
	    	{name: 'ec_deadline',label:'有效天数',index: 'ec_deadline',width:70,align:'right'},
	    	{name: 'ec_manager',label:'经办人',index: 'ec_manager',width:70},
	    	{name: 'ec_remark',label:'备注',index: 'ec_remark',width:180}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'ec_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ec_ar_state='+$("#ec_ar_state").val();
		params += '&ec_manager='+Public.encodeURI($("#ec_manager").val());
		params += '&ec_name='+Public.encodeURI($("#ec_name").val());
		params += '&ec_number='+Public.encodeURI($("#ec_number").val());
		return params;
	},
	reset:function(){
		$("#ec_manager").val("");
		$("#ec_name").val("");
		$("#ec_number").val("");
		$("#sp_name").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_ar_state.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
	}
}
THISPAGE.init();