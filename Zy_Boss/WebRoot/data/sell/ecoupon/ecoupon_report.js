var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SELL10;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/ecoupon/report';

var handle = {
	formatState:function(val, opt, row){
		if(val == 0){
			return '未用';
		}else if(val == 1){
			return '已用';
		}
	},
	formatType:function(val, opt, row){
		if(undefined != val && null != val){
			if(val == 0){
				return '微商城';
			}else if(val == 1){
				return '前台收银';
			}
		}else{
			return "";
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		this.$_ar_state = $("#ar_state").cssRadio({ callback: function($_obj){
			$("#ecu_state").val($_obj.find("input").val());
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'ecu_state',label:'状态',index: 'ecu_state',width:60,formatter: handle.formatState,align:'center'},
	    	{name: 'ecu_date',label:'领取日期',index: 'ecu_date',width:100},
	    	{name: 'ecu_name',label:'姓名',index: 'ecu_name',width:80},
	    	{name: 'ecu_tel',label:'电话',index: 'ecu_tel',width:100},
	    	{name: 'vip_cardcode',label:'会员卡号',index: 'vip_cardcode',width:100},
	    	{name: 'ecu_money',label:'面值',index: 'ecu_money',width:80,align:'right'},
	    	{name: 'ecu_limitmoney',label:'满多少可用',index: 'ecu_limitmoney',width:80,align:'right'},
	    	{name: 'ecu_enddate',label:'有效期',index: 'ecu_enddate',width:90},
	    	{name: 'ecu_use_type',label:'使用方式',index: 'ecu_use_type',width:70,formatter: handle.formatType},
	    	{name: 'ecu_use_date',label:'使用时间',index: 'ecu_use_date',width:90},
	    	{name: 'ecu_use_number',label:'使用单据',index: 'ecu_use_number',width:120}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'ecu_id'  //图标ID
			},
			loadComplete: function(data){
				if(undefined != data && undefined != data.data){
					var _data = data.data.list;
					for(key in _data){
						var state = _data[key].ecu_state;
						if(state == 0){
							var flag = Public.checkDate(_data[key].ecu_enddate);//1-已过期,2-即将过期
							if(flag == 1){//已经过期
								$("#grid").jqGrid('setRowData',_data[key].ecu_id,false,{color:'#eb3f5f'});
							}
							if(flag == 2){//即将过期
								$("#grid").jqGrid('setRowData',_data[key].ecu_id,false,{color:'blue'});
							}
						}
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ecu_state='+$("#ecu_state").val();
		params += '&ecu_name='+$("#ecu_name").val();
		return params;
	},
	reset:function(){
		$("#ecu_state").val("");
		$("#ecu_name").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_ar_state.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();