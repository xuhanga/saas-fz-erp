var api = frameElement.api, oper = api.opener;
var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl= config.BASEPATH+'sell/report/query_stock_detail_size';

var allSizes = [];

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.getSizeTitle();
	},
	getSizeTitle:function(){/*获取尺码头*/
		$.ajax({
			type:"POST",
			async:false,
			url:config.BASEPATH+"sell/report/loadProductSize/"+api.data.pd_code,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					allSizes=data.data;
				}
			}
		});
	},
	gridTotal:function(){
    	var grid = $('#grid');
		var footerData = {cr_name:'合计'};
		var ids = $("#grid").getDataIDs();
		for (var i = 0; i < allSizes.length; i++) {
			var amount1 = 0, amount2 = 0;
			for (var j = 0; j < ids.length; j++) {
				var rowData = $("#grid").jqGrid("getRowData", ids[j]);
				var values = rowData["amountMap."+allSizes[i].sz_code];
				if($.trim(values) == ''){
					continue;
				}
				var temps = values.split("/");
				amount1 += parseInt(temps[0]);
				amount2 += parseInt(temps[1]);
			}
			footerData["amountMap."+allSizes[i].sz_code] = amount1+'/'+amount2;
		}
		
		var amount1 = 0, amount2 = 0;
		for (var j = 0; j < ids.length; j++) {
			var rowData = $("#grid").jqGrid("getRowData", ids[j]);
			var values = rowData["totalAmount"];
			if($.trim(values) == ''){
				continue;
			}
			var temps = values.split("/");
			amount1 += parseInt(temps[0]);
			amount2 += parseInt(temps[1]);
		}
		footerData["totalAmount"] = amount1+'/'+amount2;
		grid.footerData('set',footerData);
    },
	initGrid:function(){
		var colModel = [
            {name:'id',hidden:true},
	    	{label:'颜色', name: 'cr_name',index: '', width: 80,fixed:true},
	    	{label:'杯型', name: 'br_name',index: '', width: 50,align:'center',fixed:true}
		];	
		for ( var i = 0; i < allSizes.length; i++) {
			colModel.push({label:allSizes[i].sz_name,name: "amountMap."+allSizes[i].sz_code,index:'',width: 80, title: true,sortable:false,align:'right'});
		}
		
		colModel.push({label:'合计',name: "totalAmount",width: 80, title: true,sortable:false,align:'right'});
		$('#grid').jqGrid({
			loadonce:true, 
			url:queryurl + "?"+THISPAGE.buildParams(),
			datatype: 'json',
			width: _width,
			height: _height,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			viewrecords: true,
			multiSort: true,
//			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			rowNum:9999999,
			pgtext:false,
			shrinkToFit:false,
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id:'id'
			},
			gridComplete: function(data){
				THISPAGE.gridTotal();
			}
	    });
	},
    reloadData: function (data) {
    },
    buildParams:function(){
		var params = '';
		params += 'begindate='+api.data.begindate;
		params += '&enddate='+api.data.enddate;
		params += '&em_code='+api.data.em_code;
		params += '&shl_shop_code='+api.data.shl_shop_code;
		params += '&pd_code='+api.data.pd_code;
		return params;
	},
	initEvent:function(){
		//关闭
		$('#btnExit').on('click', function(e){
			api.close();
		});
	}
};
THISPAGE.init();
