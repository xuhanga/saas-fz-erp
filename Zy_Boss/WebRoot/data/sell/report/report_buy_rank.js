var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL19;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/buyRank';
var handle = {
	formatRate:function(val,opt,row){
		return PriceLimit.formatMoney(row.shl_price/row.shl_sell_price);
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#shl_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#shl_shop_code").val(sp_code.join(","));
					$("#shop_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQuerySupply : function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#sp_code").val(selected.sp_code);
				$("#sp_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#sp_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择导购员',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false,em_type:0},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#shl_main").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#shl_main").val(selected.em_code);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_tp_code = [];
				var pd_tp_name = [];
				for(var i=0;i<selected.length;i++){
					pd_tp_code.push(selected[i].tp_code);
					pd_tp_name.push(selected[i].tp_name);
				}
				$("#tp_code").val(pd_tp_code.join(","));
				$("#tp_name").val(pd_tp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_tp_code = [];
					var pd_tp_name = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
					}
					$("#tp_code").val(pd_tp_code.join(","));
					$("#tp_name").val(pd_tp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#bd_code").val(pd_bd_code.join(","));
				$("#bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#bd_code").val(pd_bd_code.join(","));
					$("#bd_name").val(pd_bd_name.join(","));
				}
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'名称',name: 'sp_name',index:'sp_name',width:100, align:'left'},
			{label:'销量',name: 'shl_amount',index:'shl_amount',width:100, align:'right'},
			{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:90,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后金额',name: 'shl_money',index:'shl_money',width:90,align:'right',formatter: PriceLimit.formatBySell},
			{label:'销售占比',name: 'shl_rate',index:'shl_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'平均价',name: 'shl_avg_price',index:'shl_avg_price',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'成本',name: 'shl_cost_money',index:'shl_cost_money',width:90,align:'right',formatter: PriceLimit.formatByCost},
			{label:'毛利',name: 'shl_profit',index:'shl_profit',width:90,align:'right',formatter: PriceLimit.formatByProfit},
			{label:'毛利率',name: 'shl_profit_rate',index:'shl_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'毛利占比',name: 'shl_profit_ratio',index:'shl_profit_ratio',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&sp_code='+$("#sp_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#shl_shop_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#sp_name").val("");
		$("#sp_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
