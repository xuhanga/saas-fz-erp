var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL20;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/empRank';
var handle = {
	formatRate:function(val,opt,row){
		if(null != row.shl_count && 0 != row.shl_count){
			return parseFloat(row.shl_amount/row.shl_count).toFixed(2);
		}else{
			return 0.00;
		}
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#shl_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#shl_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:true,em_type:0},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var em_code = [];
				var em_name = [];
				for(var i=0;i<selected.length;i++){
					em_code.push(selected[i].em_code);
					em_name.push(selected[i].em_name);
				}
				$("#em_code").val(em_code.join(","));
				$("#em_name").val(em_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					var em_code = [];
					var em_name = [];
					for(var i=0;i<selected.length;i++){
						em_code.push(selected[i].em_code);
						em_name.push(selected[i].em_name);
					}
					$("#em_code").val(em_code.join(","));
					$("#em_name").val(em_name.join(","));
				}
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initType:function(type){
		$("#tab_type").children("a").each(function(){
			$(this).removeClass("on");
		});
		$("#btn_"+type).addClass("on")
		$("#type").val(type);
		$("#grid").clearGridData(false);
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
            {label:'编号',name: 'shl_em_code',index:'shl_em_code',width:60},	
			{label:'姓名',name: 'em_name',index:'em_name',width:80, align:'left'},
			{label:'接待数',name: 'da_receive',index:'da_receive',width:60,align:'right'},
			{label:'接待率',name: 'da_receive_rate',index:'da_receive_rate',width:60,align:'right',formatter:handle.formatRate},
			{label:'试穿数',name: 'da_try',index:'da_try',width:60,align:'right'},
			{label:'办会员卡',name: 'shl_vips',index:'shl_vips',width:60,align:'right'},
			{label:'开单数',name: 'shl_count',index:'shl_count',width:60,align:'right'},
			{label:'连带率',name: 'shl_rate',index:'shl_rate',width:60,align:'right',formatter:handle.formatRate},
			{label:'数量',name: 'shl_amount',index:'shl_amount',width:60,align:'right'},
			{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后金额',name: 'shl_money',index:'shl_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'销售占比',name: 'shl_rate',index:'shl_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'平均价',name: 'shl_avg_price',index:'shl_avg_price',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'成本',name: 'shl_cost_money',index:'shl_cost_money',width:70,align:'right',formatter: PriceLimit.formatByCost},
			{label:'毛利',name: 'shl_profit',index:'shl_profit',width:70,align:'right',formatter: PriceLimit.formatByProfit},
			{label:'毛利率',name: 'shl_profit_rate',index:'shl_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'毛利占比',name: 'shl_profit_ratio',index:'shl_profit_ratio',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				THISPAGE.doReport(rowid);
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	doReport:function(id){
		var grid = $("#grid").jqGrid("getRowData", id);
		var pdata = {};
		pdata.begindate=$("#begindate").val();
		pdata.enddate=$("#enddate").val();
		pdata.shl_shop_code=$("#shl_shop_code").val();
		pdata.em_code=grid.shl_em_code;
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'sell/report/to_emp_type',
			data: pdata,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&type='+$("#type").val();
		params += '&em_code='+$("#em_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#shl_shop_code").val("");
		$("#em_code").val("");
		$("#em_name").val("");
		THISPAGE.initType("main");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$("#btn_main").on('click',function(){
			THISPAGE.initType("main");
		});
		$("#btn_slave").on('click',function(){
			THISPAGE.initType("slave");
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
