var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'sell/report/rank';
var _height = $(parent).height()-190,_width = $(parent).width()-40;
var api = frameElement.api;
var pdata = api.data;
var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();
		this.reloadData();
	},
	initType:function(type){
		$("#tab_type").children("a").each(function(){
			$(this).removeClass("on");
		});
		$("#btn_"+type).addClass("on")
		$("#type").val(type);
		$("#grid").clearGridData(false);
	},
	initGrid:function(){
		var colModel = [
			{label:'名称',name: 'bd_name',index:'bd_name',width:100, align:'left'},
			{label:'销量',name: 'shl_amount',index:'shl_amount',width:50,align:'right'},
			{label:'库存',name: 'sd_amount',index:'sd_amount',width:50,align:'right'},
			{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:90,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后金额',name: 'shl_money',index:'shl_money',width:90,align:'right',formatter: PriceLimit.formatBySell},
			{label:'销售占比',name: 'shl_rate',index:'shl_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'库存金额',name: 'sd_money',index:'sd_money',width:100,align:'right',formatter: PriceLimit.formatBySell},
			{label:'库存金额占比',name: 'sd_rate',index:'sd_rate',width:80,align:'right',formatter: PriceLimit.formatBySell},
			{label:'平均价',name: 'shl_avg_price',index:'shl_avg_price',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'成本',name: 'shl_cost_money',index:'shl_cost_money',width:90,align:'right',formatter: PriceLimit.formatByCost},
			{label:'毛利',name: 'shl_profit',index:'shl_profit',width:90,align:'right',formatter: PriceLimit.formatByProfit},
			{label:'毛利率',name: 'shl_profit_rate',index:'shl_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'毛利占比',name: 'shl_profit_ratio',index:'shl_profit_ratio',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+pdata.begindate;
		params += '&enddate='+pdata.enddate;
		params += '&em_code='+pdata.em_code;
		params += '&shl_shop_code='+pdata.shl_shop_code;
		params += '&type='+$("#type").val();
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$("#btn_brand").on('click',function(){
			THISPAGE.initType("brand");
			THISPAGE.reloadData();
		});
		$("#btn_type").on('click',function(){
			THISPAGE.initType("type");
			THISPAGE.reloadData();
		});
		$("#btn-close").on('click',function(){
			api.close();
		});
		
	}
}
THISPAGE.init();
