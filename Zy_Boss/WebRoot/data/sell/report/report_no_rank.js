var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL20;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/noRank';
var handle = {
	formatRate:function(val,opt,row){
		return PriceLimit.formatMoney(row.shl_price/row.shl_sell_price);
	},
	operFmatter:function(val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<span class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</span>&nbsp;&nbsp;';
		html_con += '<span class="iconfont i-hand ui-icon-calculator" title="尺码明细">&#xe608;</span>&nbsp;&nbsp;';
		html_con += '<span class="iconfont i-hand ui-icon-contact" title="库存明细">&#xe612;</span>';
		html_con += '</div>';
		return html_con;
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#shl_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#shl_shop_code").val(sp_code.join(","));
					$("#shop_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择导购员',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false,em_type:0},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#em_code").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#em_code").val(selected.em_code);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_tp_code = [];
				var pd_tp_name = [];
				for(var i=0;i<selected.length;i++){
					pd_tp_code.push(selected[i].tp_code);
					pd_tp_name.push(selected[i].tp_name);
				}
				$("#tp_code").val(pd_tp_code.join(","));
				$("#tp_name").val(pd_tp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_tp_code = [];
					var pd_tp_name = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
					}
					$("#tp_code").val(pd_tp_code.join(","));
					$("#tp_name").val(pd_tp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#bd_code").val(pd_bd_code.join(","));
				$("#bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#bd_code").val(pd_bd_code.join(","));
					$("#bd_name").val(pd_bd_name.join(","));
				}
			},
			cancel:true
		});
	},
	//店铺货号库存明细
	doQueryStockDetail:function(pd_code){
		if(pd_code != undefined && pd_code !=''){
			$.dialog({
				id: 'stockdetail',
				title: '货号店铺库存尺码明细',
				data:{
					pd_code:pd_code,
					begindate:$("#begindate").val(),
					enddate:$("#enddate").val(),
					em_code:$("#em_code").val(),
					shl_shop_code:$("#shl_shop_code").val(),
					tp_code:$("#tp_code").val(),
					bd_code:$("#bd_code").val()
				},
				min:false,
				width:980,
				height:500,
				fixed:false,
				drag: true,
				resize:false,
				content:'url:'+config.BASEPATH+'sell/report/toQueryStockDetail?pd_code='+pd_code
			});
		}
	},
	doQueryStockDetailSize:function(pd_code){
		if(pd_code != undefined && pd_code !=''){
			$.dialog({
				id: 'stockdetail_size',
				title: '尺码明细',
				data:{
					pd_code:pd_code,
					begindate:$("#begindate").val(),
					enddate:$("#enddate").val(),
					em_code:$("#em_code").val(),
					shl_shop_code:$("#shl_shop_code").val(),
					tp_code:$("#tp_code").val(),
					bd_code:$("#bd_code").val()
				},
				min:false,
				width:980,
				height:500,
				fixed:false,
				drag: true,
				resize:false,
				content:'url:'+config.BASEPATH+'sell/report/to_query_stock_detail_size'
			});
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    {label:'操作',name: 'operate', width: 90, fixed:true, formatter: handle.operFmatter, title: false,sortable:false,align:"center"},
		    {label:'商品编号',name: 'shl_pd_code',index:'shl_pd_code',width:100, align:'left',hidden:true},
			{label:'货号',name: 'pd_no',index:'pd_no',width:100, align:'left'},
			{label:'名称',name: 'pd_name',index:'pd_name',width:100, align:'left'},
			{label:'上市日期',name: 'pd_date',index:'pd_date',width:100, align:'left'},
			{label:'数量',name: 'shl_amount',index:'shl_amount',width:40,align:'right'},
			{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后金额',name: 'shl_money',index:'shl_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'销售占比',name: 'shl_rate',index:'shl_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'平均价',name: 'shl_avg_price',index:'shl_avg_price',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'成本',name: 'shl_cost_money',index:'shl_cost_money',width:70,align:'right',formatter: PriceLimit.formatByCost},
			{label:'毛利',name: 'shl_profit',index:'shl_profit',width:70,align:'right',formatter: PriceLimit.formatByProfit},
			{label:'毛利率',name: 'shl_profit_rate',index:'shl_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'毛利占比',name: 'shl_profit_ratio',index:'shl_profit_ratio',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'库存',name: 'sd_amount',index:'sd_amount',width:40,align:'right'}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
                total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&em_code='+$("#em_code").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	initNotPdCodeParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&em_code='+$("#em_code").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&bd_code='+$("#bd_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#em_code").val("");
		$("#em_name").val("");
		$("#shl_shop_code").val("");
		THISPAGE.$_date.setValue(0);
		$("#pd_name").val("");
		$("#pd_code").val("");
		$("#tp_code").val("");
		$("#tp_name").val("");
		$("#bd_code").val("");
		$("#bd_name").val("");
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).shl_pd_code);
        });
		//库存明细
        $('#grid').on('click','.operating .ui-icon-contact', function (e) {
        	var id = $(this).parent().data('id');
        	var row = $('#grid').getRowData(id);
        	Utils.doQueryStockDetail(row.shl_pd_code);
        });
        //尺码明细
        $('#grid').on('click','.operating .ui-icon-calculator', function (e) {
        	var id = $(this).parent().data('id');
        	var row = $('#grid').getRowData(id);
        	Utils.doQueryStockDetailSize(row.shl_pd_code);
        });
	}
}
THISPAGE.init();
