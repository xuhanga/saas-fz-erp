var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL11;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/noRate';
var handle = {
	formatRate:function(val,opt,row){
		return PriceLimit.formatMoney(row.shl_price/row.shl_sell_price);
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#shl_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#shl_shop_code").val(sp_code.join(","));
					$("#shop_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择导购员',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false,em_type:0},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#shl_main").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#shl_main").val(selected.em_code);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var pd_tp_code = [];
					var pd_tp_name = [];
					var pd_tp_upcode = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
						pd_tp_upcode.push(selected[i].tp_upcode);
					}
					$("#pd_tp_code").val(pd_tp_code.join(","));
					$("#pd_tp_name").val(pd_tp_name.join(","));
					$("#pd_tp_upcode").val(pd_tp_upcode.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#pd_bd_code").val(pd_bd_code.join(","));
				$("#pd_bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#pd_bd_code").val(pd_bd_code.join(","));
					$("#pd_bd_name").val(pd_bd_name.join(","));
				}
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initYear();
		this.initDict();
		this.initState();
	},
	initState:function(){
		  stateInfo = [
				        {id:"",name:"全部"},
						{id:"0",name:"零售"},
						{id:"1",name:"退货"},
						{id:"2",name:"换货"},
					  ];
		  stateCombo = $('#span_state').combo({
				data:stateInfo,
				value: 'id',
				text: 'name',
				width :206,
				height:80,
				listId:'',
				defaultSelected: 0,
				editable: true,
				callback:{
					onChange: function(data){
						$("#shl_state").val(data.id);
					}
				}
		  }).getCombo();
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
			        {year_Code:"",year_Name:"全部"},
					{year_Code:""+parseInt(currentYear+1)+"",year_Name:""+parseInt(currentYear+1)+""},
					{year_Code:""+parseInt(currentYear)+"",year_Name:""+parseInt(currentYear)+""},
					{year_Code:""+parseInt(currentYear-1)+"",year_Name:""+parseInt(currentYear-1)+""},
					{year_Code:""+parseInt(currentYear-2)+"",year_Name:""+parseInt(currentYear-2)+""},
					{year_Code:""+parseInt(currentYear-3)+"",year_Name:""+parseInt(currentYear-3)+""},
					{year_Code:""+parseInt(currentYear-4)+"",year_Name:""+parseInt(currentYear-4)+""},
					{year_Code:""+parseInt(currentYear-5)+"",year_Name:""+parseInt(currentYear-5)+""},
					{year_Code:""+parseInt(currentYear-6)+"",year_Name:""+parseInt(currentYear-6)+""},
					{year_Code:""+parseInt(currentYear-7)+"",year_Name:""+parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'year_Code',
			text: 'year_Name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.year_Code);
				}
			}
		}).getCombo();
	  },
  initDict:function(){
	  seasonCombo = $('#span_pd_season_Code').combo({
			value: 'dtl_code',
			text: 'dtl_name',
			width :206,
			height: 80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					if(data.dtl_name == "全部"){
						$("#pd_season").val("");
					}else{
						$("#pd_season").val(data.dtl_name);
					}
				}
			}
		}).getCombo(); 
		$.ajax({
			type:"POST",
			url:config.BASEPATH +"base/dict/listByProduct",
			data:"",
			cache:false,
			dataType:"json",
			success:function(data){
				seasonInfo = data.data.seasonitemsquery;
				seasonCombo.loadData(seasonInfo);
			}
		});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
            {label:'单据',name: 'shl_number',index:'shl_number',width:130,align:'left'},
			{label:'货号',name: 'pd_no',index:'pd_no',width:90,align:'left'},
			{label:'名称',name: 'pd_name',index:'pd_name',width:100, align:'left'},
			{label:'品牌',name: 'bd_name',index:'bd_name',width:60, align:'left'},
			{label:'类别',name: 'tp_name',index:'tp_name',width:60, align:'left'},
			{label:'单位',name: 'pd_unit',index:'pd_unit',width:40,align:'center'},		    	
			{label:'颜色',name: 'cr_name',index:'cr_name',width:50, align:'center'},
			{label:'尺码',name: 'sz_name',index:'sz_name',width:45,align:'center'},
			{label:'杯型',name: 'bs_name',index:'bs_name',width:40, align:'center'},
			{label:'数量',name: 'shl_amount',index:'shl_amount',width:40,align:'right'},
			{label:'数量',name: 'shl_cost_price',index:'shl_cost_price',hidden:true},
			{label:'零售价',name: 'shl_sell_price',index:'shl_sell_price',width:60,align:'right',formatter: PriceLimit.formatBySell},
			{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后价',name: 'shl_price',index:'shl_price',width:60,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后金额',name: 'shl_money',index:'shl_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'成本',name: 'shl_cost_money',index:'shl_cost_money',width:70,align:'right',formatter: PriceLimit.formatByCost},
			{label:'折扣率',name: 'shl_rate',index:'shl_rate',width:60,align:'right',formatter: handle.formatRate},
			{label:'毛利',name: 'shl_profit',index:'shl_profit',width:70,align:'right',formatter: PriceLimit.formatByProfit},
			{label:'会员',name: 'vm_name',index:'vm_name',width:70},
			{label:'收银员',name: 'em_name',index:'em_name',width:70},
	    	{label:'主导购',name: 'main_name',index:'main_name',width:70},
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
				if(undefined != data && undefined != data.data){
					var _data = data.data.list;
					for(key in _data){
						var state = _data[key].shl_state;
						if(state == 1){
							$("#grid").jqGrid('setRowData',_data[key].shl_id,false,{color:'#eb3f5f'});
						}
						if(state == 2){
							$("#grid").jqGrid('setRowData',_data[key].shl_id,false,{color:'#3286b4'});
						}
					}
				}
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&shl_shop_code='+$("#shl_shop_code").val();
		params += '&shl_main='+$("#shl_main").val();
		params += '&pd_no='+$("#pd_no").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&tp_code='+$("#pd_tp_code").val();
		params += '&bd_code='+$("#pd_bd_code").val();
		params += '&shl_state='+$("#shl_state").val();
		params += '&minrate='+$("#minrate").val();
		params += '&maxrate='+$("#maxrate").val();
		return params;
	},
	reset:function(){
		$("#em_name").val("");
		$("#shl_main").val("");
		$("#shop_name").val("");
		$("#shl_shop_code").val("");
		$("#pd_no").val("");
		$("#tp_name").val("");
		$("#bd_name").val("");
		$("#pd_tp_code").val("");
		$("#pd_bd_code").val("");
		yearCombo.selectByIndex(0);
		seasonCombo.selectByIndex(0);
		stateInfo.selectByIndex(0);
		$("#minrate").val("");
		$("#maxrate").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
