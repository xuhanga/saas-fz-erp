var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/listPage';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true,jmd:THISPAGE.$_jmd.chkVal().join()? 1 : 0},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#sh_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择导购员',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false,em_type:0},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#sh_em_code").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#sh_em_code").val(selected.em_code);
				}
			},
			cancel:true
		});
	},doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var pd_tp_code = [];
					var pd_tp_name = [];
					var pd_tp_upcode = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
						pd_tp_upcode.push(selected[i].tp_upcode);
					}
					$("#pd_tp_code").val(pd_tp_code.join(","));
					$("#pd_tp_name").val(pd_tp_name.join(","));
					$("#pd_tp_upcode").val(pd_tp_upcode.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var pd_bd_code = [];
				var pd_bd_name = [];
				for(var i=0;i<selected.length;i++){
					pd_bd_code.push(selected[i].bd_code);
					pd_bd_name.push(selected[i].bd_name);
				}
				$("#pd_bd_code").val(pd_bd_code.join(","));
				$("#pd_bd_name").val(pd_bd_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var pd_bd_code = [];
					var pd_bd_name = [];
					for(var i=0;i<selected.length;i++){
						pd_bd_code.push(selected[i].bd_code);
						pd_bd_name.push(selected[i].bd_name);
					}
					$("#pd_bd_code").val(pd_bd_code.join(","));
					$("#pd_bd_name").val(pd_bd_name.join(","));
				}
			},
			cancel:true
		});
	},
	doSupply:function(){
		commonDia = $.dialog({
			title : '选择供货厂商',
			content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#supply_code").val(selected.sp_code);
				$("#supply_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#supply_code").val(selected.sp_code);
					$("#supply_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doProduct : function(){
		commonDia = $.dialog({
			title : '选择商品',
			content : 'url:'+config.BASEPATH+'base/product/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#pd_code").val(selected.pd_code);
				$("#pd_name").val(selected.pd_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#pd_code").val(selected.pd_code);
					$("#pd_name").val(selected.pd_name);
				}
			},
			cancel:true
		});
	},
	doVip : function(){
		commonDia = $.dialog({
			title : '选择会员',
			content : 'url:'+config.BASEPATH+'vip/member/to_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#vip_code").val(selected.vm_code);
				$("#vip_name").val(selected.vm_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#vip_code").val(selected.vm_code);
					$("#vip_name").val(selected.vm_name);
				}
			},
			cancel:true
		});
	},
	initYear:function(){
		var tim = new Date();
		var currentYear = tim.getFullYear();
		yearInfo = {
			data:{items:[
			        {code:"",name:"全部"},
					{code:""+parseInt(currentYear+1)+"",name:""+parseInt(currentYear+1)+""},
					{code:""+parseInt(currentYear)+"",name:""+parseInt(currentYear)+""},
					{code:""+parseInt(currentYear-1)+"",name:""+parseInt(currentYear-1)+""},
					{code:""+parseInt(currentYear-2)+"",name:""+parseInt(currentYear-2)+""},
					{code:""+parseInt(currentYear-3)+"",name:""+parseInt(currentYear-3)+""},
					{code:""+parseInt(currentYear-4)+"",name:""+parseInt(currentYear-4)+""},
					{code:""+parseInt(currentYear-5)+"",name:""+parseInt(currentYear-5)+""},
					{code:""+parseInt(currentYear-6)+"",name:""+parseInt(currentYear-6)+""},
					{code:""+parseInt(currentYear-7)+"",name:""+parseInt(currentYear-7)+""}
				]}
		}
		yearCombo = $('#span_year').combo({
			data:yearInfo.data.items,
			value: 'code',
			text: 'name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.code);
				}
			}
		}).getCombo();
	  },
	  initState:function(){
		  stateInfo = [
				        {id:"",name:"全部"},
						{id:"0",name:"零售"},
						{id:"1",name:"退货"},
						{id:"2",name:"换货"},
					  ];
		  stateCombo = $('#span_state').combo({
				data:stateInfo,
				value: 'id',
				text: 'name',
				width :206,
				height:80,
				listId:'',
				defaultSelected: 0,
				editable: true,
				callback:{
					onChange: function(data){
						$("#shl_state").val(data.id);
					}
				}
		  }).getCombo();
	  },
	  initDict:function(){
		  seasonCombo = $('#span_pd_season_Code').combo({
				value: 'dtl_code',
				text: 'dtl_name',
				width :206,
				height: 80,
				listId:'',
				defaultSelected: 0,
				editable: true,
				callback:{
					onChange: function(data){
						if(data.dtl_name == "全部"){
							$("#pd_season").val("");
						}else{
							$("#pd_season").val(data.dtl_name);
						}
					}
				}
			}).getCombo(); 
			$.ajax({
				type:"POST",
				url:config.BASEPATH +"base/dict/listByProduct",
				data:"",
				cache:false,
				dataType:"json",
				success:function(data){
					seasonInfo = data.data.seasonitemsquery;
					seasonCombo.loadData(seasonInfo);
				}
			});
		}
};

var handle = {
	viewList: function(id){//收银明细
		var rowData = $("#grid").jqGrid("getRowData", id);
		$.dialog({
			title : '收银明细',
			content : 'url:'+config.BASEPATH+"sell/report/to_report_shoplist",
			data: {number:rowData.sh_number},
			width:750,
			height:500,
			fixed:false,
			drag: true,
			resize:false,
			cache : false,
			lock: true
		});
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-img" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatState:function(val, opt, row){
		if(val == 0){
			return "零售";
		}else if(val == 1){
			return "退货";
		}else if(val == 2){
			return "换货 ";
		}
		return '';
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		Utils.initYear();
		Utils.initDict();
		Utils.initState();
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_jmd = $("#jmd").cssCheckbox({callback:function($_obj){
			$("#shop_name").val("");
			$("#sh_shop_code").val("");
			$('#btn-search').click();
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'店铺名称',name: 'shop_name',index:'shop_name',width:100},	
	    	{label:'收银流水号',name: 'shl_number',index:'shl_number',width:130,align:'center',sortable:false},
        	{label:'收银日期',name: 'shl_sysdate',index:'shl_sysdate',width:160},	
	    	{label:'状态',name: 'shl_state',index:'shl_state',width:45,align:'left',formatter:handle.formatState},
	    	{label:'商品编号',name: 'shl_pd_code',index:'shl_pd_code',hidden:true},
	    	{label:'商品货号',name: 'pd_no',index:'pd_no',width:80, align:'left'},
	    	{label:'商品名称',name: 'pd_name',index:'pd_name',width:70, align:'left'},
	    	{label:'品牌',name: 'bd_name',index:'bd_name',width:70, align:'left'},
	    	{label:'类别',name: 'tp_name',index:'tp_name',width:70,align:'left'},
	    	{label:'颜色',name: 'cr_name',index:'cr_name',width:70,align:'left'},
	    	{label:'尺码',name: 'sz_name',index:'sz_name',width:60,align:'left'},   	
	    	{label:'杯型',name: 'br_name',index:'br_name',width:40,align:'center'},
	    	{label:'面料',name: 'pd_fabric',index:'pd_fabric',width:70,align:'left'},
	    	{label:'季节',name: 'pd_season',index:'pd_season',width:50,align:'center'},
	    	{label:'年份',name: 'pd_year',index:'pd_year',width:50,align:'center'},
	    	{label:'数量',name: 'shl_amount',index:'shl_amount',width:50,align:'right'},
	    	{label:'零售价',name: 'shl_sell_price',index:'shl_sell_price',width:70,align:'right',formatter: PriceLimit.formatMoney},   	
	    	{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'折扣价',name: 'shl_price',index:'shl_price',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'折扣率',name: 'shl_price_rate',index:'shl_price_rate',width:50,align:'right',formatter: PriceLimit.formatMoney,sortable:false},
	    	{label:'折扣金额',name: 'shl_money',index:'shl_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'标牌价',name: 'pd_sign_price',index:'pd_sign_price',width:60,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'会员信息',name: 'vm_name',index:'vm_name',width:140,align:'right'}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
				userdata:'data.data',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
				if(undefined != data && undefined != data.data){
					var _data = data.data.list;
					for(key in _data){
						var state = _data[key].sh_state;
						if(state == 1){
							$("#grid").jqGrid('setRowData',_data[key].sh_id,false,{color:'#eb3f5f'});
						}
						if(state == 2){
							$("#grid").jqGrid('setRowData',_data[key].sh_id,false,{color:'#3286b4'});
						}
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&pd_code='+$("#pd_code").val();
		params += '&sh_shop_code='+$("#sh_shop_code").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&pd_season='+Public.encodeURI($("#pd_season").val());
		params += '&pd_tp_code='+$("#pd_tp_code").val();
		params += '&pd_bd_code='+$("#pd_bd_code").val();
		params += '&supply_code='+$("#supply_code").val();
		params += '&vip_code='+$("#vip_code").val();
		params += '&shl_state='+$("#shl_state").val();
		params += '&sh_em_code='+$("#sh_em_code").val();
		params += '&minmoney='+$("#minmoney").val();
		params += '&maxmoney='+$("#maxmoney").val();
		params += '&jmd='+(THISPAGE.$_jmd.chkVal().join()? 1 : 0);
//		params += '&pd_tp_upcode='+$("#pd_tp_upcode").val();
//		params += '&sh_number='+Public.encodeURI($("#sh_number").val());
		return params;
	},
	reset:function(){
		THISPAGE.$_date.setValue(0);
		$("#em_name").val("");
		$("#sh_em_code").val("");
		$("#shop_name").val("");
		$("#sh_shop_code").val("");
		$("#pd_tp_name").val("");
		$("#pd_tp_code").val("");
//		$("#pd_tp_upcode").val("");
		$("#pd_bd_name").val("");
		$("#pd_bd_code").val("");
		$("#pd_name").val("");
		$("#pd_code").val("");
		$("#supply_name").val("");
		$("#supply_code").val("");
		$("#span_year").getCombo().selectByValue("");
		$("#span_pd_season_Code").getCombo().selectByValue("");
		$("#vip_name").val("");
		$("#vip_code").val("");
		$("#minmoney").val("");
		$("#maxmoney").val("");
		$("#span_state").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//收银明细
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.viewList(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#grid').on('click', '.operating .ui-icon-img', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			var pdata = $("#grid").jqGrid("getRowData", id);
			viewProductImg(pdata.shl_pd_code);
		});
	}
}
THISPAGE.init();