var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL08;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/pageShop';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true,jmd:THISPAGE.$_jmd.chkVal().join()? 1 : 0},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#sh_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#em_name").val(selected.em_name);
				$("#sh_em_code").val(selected.em_code);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#em_name").val(selected.em_name);
					$("#sh_em_code").val(selected.em_code);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	viewList: function(id){//收银明细
		var rowData = $("#grid").jqGrid("getRowData", id);
		$.dialog({
			title : '收银明细',
			content : 'url:'+config.BASEPATH+"sell/report/to_report_shoplist",
			data: {number:rowData.sh_number},
			width:750,
			height:500,
			fixed:false,
			drag: true,
			resize:false,
			cache : false,
			lock: true
		});
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="收银明细">&#xe64f;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatState:function(val, opt, row){
		if(val == 0){
			return "零售";
		}else if(val == 1){
			return "退货";
		}else if(val == 2){
			return "换货 ";
		}
		return '';
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initState();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_jmd = $("#jmd").cssCheckbox({callback:function($_obj){
			$("#shop_name").val("");
			$("#sh_shop_code").val("");
			$('#btn-search').click();
		}});
	},
	initState:function(){
		  stateInfo = [
				        {id:"",name:"全部"},
						{id:"0",name:"零售"},
						{id:"1",name:"退货"},
						{id:"2",name:"换货"},
					  ];
		  stateCombo = $('#span_state').combo({
				data:stateInfo,
				value: 'id',
				text: 'name',
				width :206,
				height:80,
				listId:'',
				defaultSelected: 0,
				editable: true,
				callback:{
					onChange: function(data){
						$("#sh_state").val(data.id);
					}
				}
		  }).getCombo();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'店铺名称',name: 'shop_name',index:'shop_name',width:100},	
	    	{label:'类型',name: 'sh_state',index:'sh_state',formatter:handle.formatState,width:60,align:'center',sortable:false},
        	{label:'收银员',name: 'em_name',index:'em_name',width:70},	
	    	{label:'单号',name: 'sh_number',index:'sh_number',width:140,align:'left'},
	    	{label:'日期',name: 'sh_date',index:'sh_date',width:80, align:'left'},
	    	{label:'金额',name: 'sh_money',index:'sh_money',width:70, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'代金券抵用后金额',name: 'sh_vc_after_money',index:'sh_vc_after_money',width:80, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'现金',name: 'sh_cash',index:'sh_cash',width:70, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'支付宝',name: 'sh_ali_money',index:'sh_ali_money',width:70, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'微支付',name: 'sh_wx_money',index:'sh_wx_money',width:70, align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'银行卡',name: 'sh_bank_money',index:'sh_bank_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'抹零',name: 'sh_lost_money',index:'sh_lost_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'优惠券',name: 'sh_ec_money',index:'sh_ec_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'代金券',name: 'sh_vc_money',index:'sh_vc_money',width:70,align:'right',formatter: PriceLimit.formatMoney},   	
	    	{label:'储值卡',name: 'sh_cd_money',index:'sh_cd_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'商场卡',name: 'sh_mall_money',index:'sh_mall_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'积分抵现',name: 'sh_point_money',index:'sh_point_money',width:60,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'订金',name: 'sh_sd_money',index:'sh_sd_money',width:70,align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-55,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'sh_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&sh_shop_code='+$("#sh_shop_code").val();
		params += '&sh_em_code='+$("#sh_em_code").val();
		params += '&sh_number='+Public.encodeURI($("#sh_number").val());
		params += '&jmd='+(THISPAGE.$_jmd.chkVal().join()? 1 : 0);
		return params;
	},
	reset:function(){
		$("#sh_number").val("");
		$("#em_name").val("");
		$("#sh_em_code").val("");
		$("#shop_name").val("");
		$("#sh_shop_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//收银明细
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.viewList(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();