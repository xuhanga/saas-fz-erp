var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL20;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/report/shopRank';
var handle = {
	formatRate:function(val,opt,row){
		return PriceLimit.formatMoney(row.sh_price/row.sh_sell_price);
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true,jmd:THISPAGE.$_jmd.chkVal().join()? 1 : 0},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#sh_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	},
	formatRate:function(val, opt, row){
		if(null != row.da_come && "0" != row.da_come){
			return parseFloat(row.sh_count/row.da_come*100).toFixed(2);
		}else if(row.sh_count == 0){
			return "0.00";
		}else{
			return "100.00";
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_jmd = $("#jmd").cssCheckbox({callback:function($_obj){
			$("#shop_name").val("");
			$("#sh_shop_code").val("");
			$('#btn-search').click();
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'店铺',name: 'shop_name',index:'shop_name',width:100, align:'left'},
			{label:'进店数',name: 'da_come',index:'da_come',width:60,align:'right'},
			{label:'接待数',name: 'da_receive',index:'da_receive',width:60,align:'right'},
			{label:'试穿数',name: 'da_try',index:'da_try',width:60,align:'right'},
			{label:'单据数',name: 'sh_count',index:'sh_count',width:60,align:'right'},
			{label:'购买率',name: 'sh_rate',index:'sh_rate',width:80, align:'right',formatter:Utils.formatRate},
			{label:'数量',name: 'sh_amount',index:'sh_amount',width:40,align:'right'},
			{label:'零售金额',name: 'sh_sell_money',index:'sh_sell_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'折后金额',name: 'sh_money',index:'sh_money',width:70,align:'right',formatter: PriceLimit.formatBySell},
			{label:'代金券抵用后金额',name: 'sh_vc_after_money',index:'sh_vc_after_money',width:90,align:'right',formatter: PriceLimit.formatBySell},
			{label:'销售占比',name: 'sh_rate',index:'sh_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'成本',name: 'sh_cost_money',index:'sh_cost_money',width:70,align:'right',formatter: PriceLimit.formatByCost},
			{label:'毛利',name: 'sh_profit',index:'sh_profit',width:70,align:'right',formatter: PriceLimit.formatByProfit},
			{label:'毛利率',name: 'sh_profit_rate',index:'sh_profit_rate',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false},
			{label:'毛利占比',name: 'sh_profit_ratio',index:'sh_profit_ratio',width:70,align:'right',formatter: PriceLimit.formatByProfit,sortable:false}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
				repeatitems : false,
				id: 'sh_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&sh_shop_code='+$("#sh_shop_code").val();
		params += '&jmd='+(THISPAGE.$_jmd.chkVal().join()? 1 : 0);
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#sh_shop_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
