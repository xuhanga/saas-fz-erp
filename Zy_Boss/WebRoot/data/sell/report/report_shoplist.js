var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'sell/report/listShopList/'+api.data.number;
var _height = api.config.height-92,_width = api.config.width-34;//获取弹出框弹出宽、高

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'货号',name: 'pd_no',index:'pd_no',width:110,align:'left'},
			{label:'名称',name: 'pd_name',index:'pd_name',width:100, align:'left'},
			{label:'品牌',name: 'bd_name',index:'bd_name',width:60, align:'left'},
			{label:'类别',name: 'tp_name',index:'tp_name',width:60, align:'left'},
			{label:'单位',name: 'pd_unit',index:'pd_unit',width:60,align:'center'},		    	
			{label:'颜色',name: 'cr_name',index:'cr_name',width:50, align:'center'},
			{label:'尺码',name: 'sz_name',index:'sz_name',width:60,align:'center'},
			{label:'杯型',name: 'bs_name',index:'bs_name',width:50, align:'center'},
			{label:'数量',name: 'shl_amount',index:'shl_amount',width:50,align:'right'},
			{label:'零售价',name: 'shl_sell_price',index:'shl_sell_price',width:70,align:'right',formatter: PriceLimit.formatMoney},
			{label:'零售金额',name: 'shl_sell_money',index:'shl_sell_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
			{label:'折后价',name: 'shl_price',index:'shl_price',width:70,align:'right',formatter: PriceLimit.formatMoney},
			{label:'折后金额',name: 'shl_money',index:'shl_money',width:70,align:'right',formatter: PriceLimit.formatMoney},
			{label:'会员',name: 'vm_name',index:'vm_name',width:100},
			{label:'收银员',name: 'em_name',index:'em_name',width:90},
	    	{label:'主导购',name: 'main_name',index:'main_name',width:70},
	    	{label:'副导购',name: 'slave_name',index:'slave_name',width:70}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'shl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){
			}
	    });
	}
}
THISPAGE.init();