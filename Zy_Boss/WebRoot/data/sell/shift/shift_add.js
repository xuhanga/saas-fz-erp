var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var Utils = {
	doQueryShop : function(){
		commonDia = W.$.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#st_shop_code").val(sp_code.join(","));
				$("#sp_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#st_shop_code").val(sp_code.join(","));
					$("#sp_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	}
};
var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sell/shift/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.close()",300);
					/*THISPAGE.initDom();*/
					W.isRefresh = true;
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		/*pdata.id=data.st_id;
		pdata.st_code=data.st_code;
		pdata.st_name=data.st_name;
		pdata.st_begintime=data.st_begintime;
		pdata.st_endtime=data.st_endtime;
		pdata.shop_name=api.data.shop_name;*/
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#sp_name").val("").focus();
		$("#st_shop_code").val("");
		$("#st_name").val("");
		$("#st_begintime").val("");
		$("#st_endtime").val("");
		$("#st_shop_code").val(api.data.shop_code);
		$("#shop_name").val(api.data.shop_name);
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
	}
};
THISPAGE.init();