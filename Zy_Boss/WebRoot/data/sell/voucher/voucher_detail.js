var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api;
var _height = api.config.height-92,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'sell/voucher/listDetail?vc_code='+api.data.vc_code;

var Utils = {
	formatType:function(val, opt, row){//0：期初  1：消费
		if(val == "0"){
			return "期初";
		}else if(val == "1"){
			return "消费";
		}else{
			return val;
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initEvent();	
	},
	initGrid:function(){
		var colModel = [
			{name: 'vcl_cardcode',label:'代金券号',index: 'vcl_cardcode',width:100},
			{name: 'vcl_type',label:'类型',index: 'vcl_type',width:60,formatter:Utils.formatType},
			{name: 'vcl_date',label:'操作日期',index: 'vcl_date',width:135},
			{name: 'vcl_money',label:'发放金额',index: 'vcl_money',width:70,align:'right'},
			{name: 'vcl_cash',label:'实收现金',index: 'vcl_cash',width:70,align:'right'},
			{name: 'shop_name',label:'发放门店',index: 'vcl_shop_code',width:100},
			{name: 'bank_name',label:'银行账户',index: 'vcl_ba_code',width:100},
			{name: 'vcl_manager',label:'操作人',index: 'vcl_manager',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			viewrecords: true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'vcl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initEvent:function(){
	}
};

THISPAGE.init();