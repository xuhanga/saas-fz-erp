var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SELL06;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'sell/voucher/page';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#vc_shop_code").val(sp_code.join(","));
				$("#sp_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#vc_shop_code").val(sp_code.join(","));
					$("#sp_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		var height = 420;
		var width = 645;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"sell/voucher/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			height = 320;
			width = 345;
			title = '修改';
			url = config.BASEPATH+"sell/voucher/to_update?vc_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	detail : function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		$.dialog({
			title : '代金券消费明细',
			content : 'url:'+config.BASEPATH+"sell/voucher/to_detail",
			data: {vc_code:rowData.vc_code},
			width : 845,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true
		});
	},
	del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sell/voucher/del',
					data:{"vc_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    updateState : function(obj,id,vc_state){
    	var rowData = $("#grid").jqGrid('getRowData', id);
    	var tip = null;
    	if(vc_state==0){
    		tip = '确认要启用代金券吗？';
    	}else{
    		tip = '确认要停用代金券吗？';
    	}
    	$.dialog.confirm(tip, function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'sell/voucher/updateState',
				data:{"vc_id":id,"vc_state":vc_state},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						$("#grid").jqGrid("setRowData", id,{vc_state:vc_state});
						if(vc_state==0){
			    			$(obj).html("&#xe60d;").removeClass("ui-icon-begin").addClass("ui-icon-stop").attr("title","停用");
			    		}else{
			    			$(obj).html("&#xe63c;").removeClass("ui-icon-stop").addClass("ui-icon-begin").attr("title","启用");
			    		}
						Public.tips({type: 3, content : "状态修改成功!"});
					}else{
						Public.tips({type: 1, content : "状态修改失败!"});
					}
				}
			});
		});
    },
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		if(row.vc_state == 0){
			html_con += '<i class="iconfont i-hand ui-icon-stop" title="停用">&#xe60d;</i>';
		}else{
			html_con += '<i class="iconfont i-hand ui-icon-begin" title="启用">&#xe63c;</i>';
		}
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="明细">&#xe608;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatLeftMoney : function(val, opt, row){
		return row.vc_money - row.vc_used_money;
	},
	formatState:function(val, opt, row){//0:正常 1:停用
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "停用";
		}else{
			return val;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'operate',label:'操作',width: 90, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'vc_code',label:'编号',index: 'vc_code',width:80,hidden: true},
	    	{name: 'vc_cardcode',label:'代金券号',index: 'vc_cardcode',width:120},
	    	{name: 'vc_name',label:'姓名',index: 'vc_name',width:100},
	    	{name: 'vc_mobile',label:'手机号码',index: 'vc_mobile',width:100},
	    	{name: 'shop_name',label:'发放门店',index: 'shop_name',width:100},
	    	{name: 'vc_grantdate',label:'发放日期',index: 'vc_grantdate',width:90},
	    	{name: 'vc_enddate',label:'有效日期',index: 'vc_enddate',width:90},
	    	{name: 'vc_money',label:'发放金额',index: 'vc_money',width:70},
	    	{name: 'vc_realcash',label:'实收现金',index: 'vc_realcash',width:70},
	    	{name: 'vc_used_money',label:'已用金额',index: 'vc_used_money',width:70},
	    	{name: 'left_money',label:'余额',index: 'vc_money',width:70,formatter: handle.formatLeftMoney},
	    	{name: 'vc_state',label:'状态',index: 'vc_state',width:50,formatter:handle.formatState},
	    	{name: 'vc_cashrate',label:'现金率',index: 'vc_cashrate',width:60},
	    	{name: 'vc_manager',label:'经办人',index: 'vc_manager',width:80}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'vc_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vc_shop_code='+$("#vc_shop_code").val();
		params += '&vc_cardcode='+Public.encodeURI($("#vc_cardcode").val());
		params += '&vc_name='+Public.encodeURI($("#vc_name").val());
		params += '&vc_mobile='+Public.encodeURI($("#vc_mobile").val());
		params += '&minmoney='+$("#minmoney").val()+'&maxmoney='+$("#maxmoney").val();
		return params;
	},
	reset:function(){
		$("#vc_shop_code").val("");
		$("#vc_cardcode").val("");
		$("#vc_name").val("");
		$("#vc_mobile").val("");
		$("#sp_name").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
		$("#minmoney").val("");
		$("#maxmoney").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		$('#grid').on('click', '.operating .ui-icon-begin', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.updateState(this, id,0);
		});
		$('#grid').on('click', '.operating .ui-icon-stop', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.updateState(this, id,1);
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.detail(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
	}
}
THISPAGE.init();