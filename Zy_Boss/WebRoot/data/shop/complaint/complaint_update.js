var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	countPoint:function(obj,countId){
		var haveBean = obj.value.length;//已用字数
		document.getElementById(countId).value = 150 - haveBean;
		if(haveBean>150){
			W.Public.tips({type: 2, content : "输入字数不能超过150！"});
			obj.value = obj.value.substr(0,150);
		}
	},
	update:function(){
		var sc_processinfo = $("#sc_processinfo").val();
		var sc_id = $("#sc_id").val();
		$.dialog.confirm("确认处理?", function(){
			var url="";
			url="?sc_processinfo="+sc_processinfo+"&sc_id="+sc_id;
			url = encodeURI(encodeURI(url)); 
			$("#btn-save").attr("disabled",true);
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"shop/complaint/update"+url,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.Public.tips({type: 3, content : data.message});
						setTimeout("api.zindex()",500);
						W.isRefresh = true;
						api.close();
					}else{
						W.Public.tips({type: 1, content : data.message});
					}
					$("#btn-save").attr("disabled",false);
				}
			});
		}, function(){
		   return;
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
	},
	initEvent:function(){
		$("#btn-save").click(function(){
			handle.update();
		});
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();