var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var queryurl = config.BASEPATH+'shop/gift/temp_list';
var querysumurl = config.BASEPATH+'shop/gift/temp_sum';
var querysizeurl = config.BASEPATH + 'shop/gift/temp_size';
var _height = $(parent).height()-300,_width = $(parent).width()-30;
var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式

var Utils = {
	buildParams:function(){
		var params = '';
		params += 'gil_shop_code='+$("#gil_shop_code").val();
		return params;
	},
	saveGift:function(){
		var gi_shop_code = $("#gil_shop_code").val();
		var gi_begindate = $("#gi_begindate").val();
		var gi_enddate = $("#gi_enddate").val();
		if(gi_begindate == null || gi_begindate ==""){
			Public.tips({type: 2, content : '请选择兑换日期！'});
			return;
		}
		if(gi_enddate == null || gi_enddate ==""){
			Public.tips({type: 2, content : '请选择兑换日期！'});
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '明细不存在，请选择货号！'});
			return;
		}
		var params = {};
		params.gi_shop_code = gi_shop_code;
		params.gi_begindate = gi_begindate;
		params.gi_enddate = gi_enddate;
		$('#btnSave').attr('disabled','disabled');
		$.ajax({
			type:"post",
			url:config.BASEPATH+"shop/gift/save",
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$('#btnSave').removeAttr('disabled');
			}
		});
	},
	doQueryShop : function(){
		var src_gil_shop_code = $("#gil_shop_code").val();
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#gil_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
				var gil_shop_code = $("#gil_shop_code").val();
            	if(src_gil_shop_code != gil_shop_code){
            		THISPAGE.reloadGridData();
                	needListRefresh = false;
            	}
			},
			cancel:true
		});
	},
	selectProduct:function(){
		var gil_shop_code = $("#gil_shop_code").val();
		if(gil_shop_code == ''){
			Public.tips({type: 2, content : '请选择发布店铺！'});
			return;
		}
		$.dialog({
			title : '商品录入',
			content : 'url:'+config.BASEPATH+'shop/gift/to_select_product/',
			data : {gil_shop_code:gil_shop_code},
			//设置最大化最小化按钮(false：隐藏 true:关闭)
		   	max: false,
		   	min: false,
		   	lock:true,
		   	//设置长宽
		   	width:1130,
		   	height:440,
		   	fixed:false,
		   	//设置是否拖拽(false:禁止，true可以移动)
		   	drag: true,
		   	resize:false,
			close:function(){
				if(isRefresh){
					Utils.refreshCurrentPageDate();
					isRefresh = false;
				}
			}
		});
	},
	refreshCurrentPageDate:function(){
		var currentMode = $("#CurrentMode").val();
		if(currentMode == '0'){//列表模式
			needListRefresh = true;
			Utils.ajaxGetGiftTempList();
        	needListRefresh = false;
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		Utils.ajaxGetGiftTempTitles();
    		needListRefresh = true;
        	needSizeRefresh = false;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		Utils.ajaxGetGiftTempSumList();
    		needListRefresh = true;
        	needSizeRefresh = true;
        	needSumRefresh = false;
    	}
	},
	ajaxGetGiftTempTitles: function() {//尺码模式刷新数据
    	if(!needSizeRefresh){
			return;
		}
    	$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/gift/temp_size_title',
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGroupGrid').GridUnload();
                    THISPAGE.addEvent();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
    },
	ajaxGetGiftTempSumList: function() {//汇总模式刷新数据
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
    },
    ajaxGetGiftTempList:function(){//列表模式刷新数据
    	if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
    }
};

var handle = {
	edit:function(rowid,pd_code){
		var gil_shop_code = $("#gil_shop_code").val();
		if(gil_shop_code == ''){
			Public.tips({type: 2, content : '请选择发布店铺！'});
			return;
		}
		if (rowid!=null){
			var params = "pd_code="+pd_code;
			params += "&needVaildateStock=1";
			$.dialog({
			   	title:'修改商品',
			   	content:'url:'+config.BASEPATH+'shop/gift/to_temp_update?'+params,
			   	data:{gil_shop_code:gil_shop_code},
			   	max: false,
			   	min: false,
			   	lock:true,
			   	width:800,
			   	height:480,
			   	fixed:false,
			   	drag: true,
			   	resize:false,
			   	close: function () {
			   		if(isRefresh){
			   			Utils.refreshCurrentPageDate();
			   			isRefresh = false;
			   		}
			   	}
			});
			
		}
	},
	updateAmountById : function(rowid,amount,srcAmount){
    	var rowData = $("#grid").jqGrid("getRowData", rowid);
    	var saveUrl = config.BASEPATH+'shop/gift/temp_updateAmountById';
    	var params = {gil_id:rowid,amount:amount};
    	$.ajax({
			type:"POST",
			url:saveUrl,
			data:params,
			async:false,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功!'});
					needSizeRefresh = true;
		        	needSumRefresh = true;
			   		var footerData = $("#grid").footerData("get");
			   		footerData.gil_totalamount = parseInt(footerData.gil_totalamount) + amount - parseInt(srcAmount);
			   		$("#grid").footerData('set',{gil_totalamount:footerData.gil_totalamount});	
            	}else{
            		Public.tips({type: 1, content : data.message});
            	}
			}
		});
    },
    updatePointByPdCode : function(rowid,pd_code,point){
    	var saveUrl = config.BASEPATH+'shop/gift/temp_updatePointByPdcode';
    	var params = {pd_code:pd_code,point:point};
    	$.ajax({
			type:"POST",
			url:saveUrl,
			data:params,
			async:false,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功!'});
				}else{
            		Public.tips({type: 1, content : data.message});
            	}
			}
		});
    },
	delByPdCode:function(rowId,type){
		if (rowId != null && rowId != '') {
    		var params = "";
    		var rowData;
    		if(type == '1'){
    			rowData = $("#sizeGroupGrid").jqGrid("getRowData", rowId);
    			params += "pd_code="+rowData.pd_code;
    			params += "&cr_code="+rowData.cr_code;
    			if(rowData.br_code != undefined){
    				params += "&br_code="+rowData.br_code;
    			}
    		}else if(type == '2'){
    			rowData = $("#sumGrid").jqGrid("getRowData", rowId);
    			params += "pd_code="+rowData.pd_code;
    		}
            $.dialog.confirm('数据删除后无法恢复，确定要删除吗？', function () {
                $.ajax({
                    type: "POST",
                    url: config.BASEPATH + "shop/gift/temp_delByPiCode",
                    data: params,
                    cache: false,
                    dataType: "json",
                    success: function (data) {
                    	if(undefined != data && data.stat == 200){
                    		Public.tips({type: 3, content : '删除成功!'});
                    		Utils.refreshCurrentPageDate();
                    	}else{
                    		Public.tips({type: 1, content : data.message});
                    	}
                    }
                });
            });
        } else {
        	Public.tips({type: 1, content : '请选择数据!!!'});
        }
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
			$.dialog.confirm('数据删除后无法恢复，确定要删除吗？',
				function(){
					$.ajax({
						type:"POST",
						url:config.BASEPATH+"shop/gift/temp_del",
						data:"gil_id="+rowIds,
						cache:false,
						dataType:"json",
						success:function(data){
							if(undefined != data && data.stat == 200){
								Public.tips({type: 3, content : '删除成功!'});
								$('#grid').jqGrid('delRowData', rowIds);
								THISPAGE.gridTotal();
								needSizeRefresh = true;
								needSumRefresh = true;
							}else{
								Public.tips({type: 1, content : data.message});
							}
						}
					});
				}, 
				function(){
			   		return;
				}
			);
		}else{
			$.dialog.tips("请选择数据",2,"32X32/hits.png");
		}
	}
};
function operFmatter(val, opt, row) {
	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
	html_con += '<span class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</span>&nbsp;';
	html_con += '<span class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</span>&nbsp;';
	html_con += '<span class="iconfont i-hand ui-icon-image" title="图片预览">&#xe654;</span>';
	html_con += '</div>';
	return html_con;
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		this.addEvent();
	},
	initDom:function(){
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
        /* this.$_LG_PickWay_0 = $("#LG_PickWay_0").cssCheckbox();
		this.$_LG_PickWay_1 = $("#LG_PickWay_1").cssCheckbox();
		this.$_LG_Type_1 = $("#LG_Type_1").cssCheckbox();
		if(SYSTEM.RT_CODE == '3' || SYSTEM.RT_CODE == '4'){
			$("#LG_SI_Code").val(SYSTEM.SI_CODE);
			$("#si_Name").val(SYSTEM.SI_NAME);
		}*/
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var sd_amount=grid.getCol('sd_amount',false,'sum');
    	var gil_totalamount=grid.getCol('gil_totalamount',false,'sum');
    	/*var odl_retailmoney=grid.getCol('odl_retailmoney',false,'sum');*/
    	grid.footerData('set',{sd_amount:sd_amount,gil_totalamount:gil_totalamount});
    	/*$("#od_amount").val(odl_amount);
    	$("#od_money").val(PriceLimit.formatByEnter(odl_unitmoney));*/
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var gil_totalamount=grid.getCol('gil_totalamount',false,'sum');
    	grid.footerData('set',{gil_totalamount:gil_totalamount});
    },
	initGrid:function(){
		var self=this;
		var gridWH = Public.setGrid();//操作
		var colModel = [
	    	{label:'操作',name: 'operate', width: 80, fixed:true, formatter: operFmatter, title: false,sortable:false,align:'center'},
	    	{label:'礼品编号',name: 'pd_code', index: 'pd_code', width: 120, title: false,hidden:true},
	    	{label:'礼品货号',name: 'pd_no', index: 'pd_no', width: 120, title: false,fixed:true},
	    	{label:'礼品名称',name: 'pd_name', index: 'pd_name', width: 120, title: false},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 100, title: false},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 80, title: false,align:'center'},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 80, title: false,align:'center'},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 80, title: false,align:'center'},
	    	{label:'库存数量',name: 'sd_amount', index: 'sd_amount', width:80, title: false,align:'right'},
	    	{label:'发布数量',name: 'gil_totalamount', index: 'gil_totalamount', width:80, title: false,align:'right',editable:true},
	    	{label:'兑换积分',name: 'score', index: 'score', width:80, title: false,align:'right',hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl+'?'+Utils.buildParams(),
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'gil_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			gridComplete:function(){ 
				var ids = $("#grid").jqGrid('getDataIDs');
            	var hasBra = false;
				for(var i=0;i < ids.length;i++){
					var cl = ids[i];
					var rowData = $("#grid").jqGrid("getRowData", cl);
					if(rowData.br_name != null && rowData.br_name != ''){
						hasBra = true;
						break;
					}
				}
				if(!hasBra){
					$("#grid").hideCol("br_name");
				}else{
					$("#grid").showCol("br_name");
				}
			},
			loadError: function(xhr, status, error){
				Public.tips({type: 1, content : '操作失败了哦，请检查您的网络链接！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				var rowData = $("#grid").jqGrid("getRowData", rowid);
            	handle.edit(rowid, rowData.pd_code);
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
            	if(cellname == 'gil_totalamount' && self.selectRow.value!=value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					}
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					if(parseInt(value) > parseInt(rowData.sd_amount)){
						Public.tips({type: 2, content : '库存不足！'});
						return self.selectRow.value;
					}
					handle.updateAmountById(rowid, parseInt(value),self.selectRow.value);
					return parseInt(value);
				}
				return value;
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
        
		var gridWH = Public.setGrid();//操作
		var colModel = [
			{label:'',name: 'sizeGroupCode', hidden: true},
		   	{label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
		    {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
		    {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: operFmatter, sortable:false,align:'center'},
		    {label:'礼品货号',name: 'pd_no', index: 'pd_no', width: 100},
		    {label:'礼品名称',name: 'pd_name', index: 'pd_name', width: 100},
		    {label:'颜色',name: 'cr_name', index: 'cr_name', width: 80},
		    {label:'杯型',name: 'br_name', index: 'br_name', width: 80},
		];
		colModel = colModel.concat(dms);
		colModel = colModel.concat([
			{label:'发布数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false}
		]);
		$('#sizeGroupGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
			height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				Public.resizeSpecifyGrid('sizeGroupGrid', 70 + (dyns.length * 32), 32);
			},
			gridComplete:function(){
				
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				var rowData = $("#sizeGroupGrid").jqGrid("getRowData", rowid);
				handle.edit(rowid, rowData.pd_code);
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGroupGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            },
            onCellSelect: function(rowid,iCol,cellcontent,e){
            	
            }
	    });
		
		if (dySize >= 2 || (dySize == 1 && dyns[0].length >0)) {
            $('#sizeGroupGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGroupGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            
            for (var key in sizeCodeMap) {
                $('#sizeGroupGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        } else {
        	$('#sizeGroupGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
        }
    },
    initSumGrid:function(){
    	var self=this;
		var gridWH = Public.setGrid();//操作
		var colModel = [
	    	{label:'操作',name: 'operate', width: 80, fixed:true, formatter: operFmatter, title: false,sortable:false},
	    	{label:'礼品编号',name: 'pd_code', index: 'pd_code', width: 120, title: false,hidden:true},
	    	{label:'礼品货号',name: 'pd_no', index: 'pd_no', width: 120, title: false,fixed:true},
	    	{label:'礼品名称',name: 'pd_name', index: 'pd_name', width: 120, title: false},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 80, title: false,align:'center'},
	    	{label:'发布数量',name: 'gil_totalamount', index: 'gil_totalamount', width:80, title: false,align:'right'},
	    	{label:'兑换积分',name: 'gil_point', index: 'gil_point', width:80, title: false,align:'right',editable:true}
	    ];
		$('#sumGrid').jqGrid({
			url: querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'gil_id'  //图标ID
			},
			loadComplete: function(data) {
				THISPAGE.sumGridTotal();
			},
			gridComplete:function(){ 
				
			},
			loadError: function(xhr, status, error) {
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
            	handle.edit(rowid, rowData.pd_code);
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
				if(cellname == 'gil_point' && self.selectRow.value!=value){
					if(value == '' || isNaN(value) || parseInt(value) < 0){
						return self.selectRow.value;
					}
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.updatePointByPdCode(rowid,rowData.pd_code,parseInt(value));
					return parseInt(value);
				}
				return value;
			}
	    });
    },
    reloadGridData: function () {
       $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl+'?'+Utils.buildParams()}).trigger("reloadGrid");
    },
	initEvent:function(){
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",110,32);
		});
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#grid').off('click','.operating .ui-icon-pencil');
		$('#grid').off('click','.operating .ui-icon-trash');
		$('#grid').off('click','.operating .ui-icon-image');
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-trash');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGroupGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGroupGrid').off('click','.operating .ui-icon-trash');
		$('#sizeGroupGrid').off('click','.operating .ui-icon-image');
		
		//修改--列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            if (id == null || id == '') {
                $.dialog.tips("你没有选中单据,请重新选择!", 2, "32X32/hits.png");
            } else {
            	var rowData = $("#grid").jqGrid("getRowData", id);
            	handle.edit(id, rowData.pd_code);
            }
        });
        $('#sizeGroupGrid').on('click', '.operating .ui-icon-pencil', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            if (id == null || id == '') {
                $.dialog.tips("你没有选中单据,请重新选择!", 2, "32X32/hits.png");
            } else {
            	var rowData = $("#sizeGroupGrid").jqGrid("getRowData", id);
            	handle.edit(id, rowData.pd_code);
            }
        });
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            if (id == null || id == '') {
                $.dialog.tips("你没有选中单据,请重新选择!", 2, "32X32/hits.png");
            } else {
            	var rowData = $("#grid").jqGrid("getRowData", id);
            	handle.edit(id, rowData.pd_code);
            }
        });
		//删除
        $('#grid').on('click', '.operating .ui-icon-trash', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            handle.del(id);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-trash', function (e) {
             e.preventDefault();
             var id = $(this).parent().data('id');
             handle.delByPdCode(id,'2');
        });
        $('#sizeGroupGrid').on('click', '.operating .ui-icon-trash', function (e) {
             e.preventDefault();
             var id = $(this).parent().data('id');
             handle.delByPdCode(id,'1');
        });
		//查看图片
        $('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	var id = $(this).parent().data('id');
  			var row=$('#grid').getRowData(id);
  			viewProductImg(row.pd_code);
        });
        $('#sizeGroupGrid').on('click', '.operating .ui-icon-image', function (e) {
        	var id = $(this).parent().data('id');
  			var row=$('#sizeGroupGrid').getRowData(id);
  			viewProductImg(row.pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	var id = $(this).parent().data('id');
  			var row=$('#sumGrid').getRowData(id);
  			viewProductImg(row.pd_code);
        });
	}
}
THISPAGE.init();
