var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP09;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/gift/report_list';
var DIALOG = {
	commonDialog:function(id,url){
		$.dialog({ 
		   	id:id,
		   	title:false,
		   	max: false,
		   	min: false,
		   	cache : false,
		   	fixed:false,
		   	drag:false,
		   	resize:false,
		   	lock:false,
		   	content:'url:'+url
	    }).max();
	}
};
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#gs_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};


var handle = {
	stateFmatter:function(val,opt,row){
		var _name = "未发放";
		if(row.gi_state != 1){
			_name = "已发放";
		}
		return _name;
	},
	addImage: function(pd_code){//新增图片
		if(pd_code != undefined && pd_code != ''){
			$.dialog({ 
				id: 'id',
				title: '商品图片',
				max: false,
				min: false,
				width: '830px',
				height: '400px',
				fixed:false,
				drag: true,
				content:'url:'+config.BASEPATH+'base/product/to_img_add?pd_code='+pd_code
			});
		}else{
			Public.tips({type: 1, content : '数据出错，请联系管理员!!!'});
		}
    },
    formatType:function(val, opt, row){
		if(null != row.br_name && "" != row.br_name){
			return row.cr_name+"/"+row.sz_name+"/"+row.br_name;
		}else{
			return row.cr_name+"/"+row.sz_name;
		}
	}
};
		
var format = {
	money: function(val,opt,row){
		var val = Public.numToCurrency(val);
		return val || '&#160;';
	},
	quantity: function(val,opt,row){
		return val || '&#160;';
	}
};
  
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satae = $('#satae');
	 	this.$_satae.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#gi_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'发放店铺',name: 'shop_name', index: 'shop_name', width: 140, title: false},
			{label:'货号',name: 'pd_no', index: 'pd_no', width: 100, title: false,fixed:true},
	    	{label:'名称',name: 'pd_name', index: 'pd_name', width: 140, title: false},
	    	{label:'规格',name: 'pd_name', index: 'pd_name', width: 140, title: false,formatter:handle.formatStype},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', hidden:true},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', hidden:true},
	    	{label:'杯型',name: 'br_name', index: 'br_name', hidden:true},
	    	{label:'状态',name: 'gs_state', index: 'gs_state', width:80, title: false,align:'center',formatter:handle.stateFmatter,},
	    	{label:'领取数量',name: 'gs_amount', index: 'gs_amount', width: 70, title: false,align:'right'},
	    	{label:'发放数量',name: 'gs_amount', index: 'gs_amount', width: 70, title: false,align:'right'},
	    	{label:'领取日期',name: 'gs_date', index: 'gs_date', width:100, title: false,align:'center'},
	    	{label:'会员名称',name: 'vm_name', index: 'vm_name', width:80}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-34,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'gi_id'
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var gs_shop_code = $("#gs_shop_code").val();
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&gs_shop_code='+gs_shop_code;
		return params;
	},
	reset:function(){
		$("#shop_name").val("")
		$("#gs_shop_code").val("")
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
