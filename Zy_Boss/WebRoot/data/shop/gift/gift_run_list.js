var api = frameElement.api, oper = api.opener;
var config=parent.parent.CONFIG;
var system=parent.parent.SYSTEM;

//ajaxQueryLineGiftRun
var queryurl = config.BASEPATH+'shop/gift/list_run';
var queryConditions = {
		searchContent: ''
	};

var Utils = {
	buildParams:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&gir_pd_code='+$("#gir_pd_code").val();
		params += '&gir_shop_code='+$("#gir_shop_code").val();
		params += '&gir_number='+$("#gir_number").val();
		return params;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		var start = document.getElementById("begindate").value;
		var end = document.getElementById("enddate").value;
		this.$_date = $('#rqxz').cssRadio({
			callback : function($_obj) { //$_obj是radio外面的lable
				dateRedioClick($_obj.find("input").attr("id"));
			}
		});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var gir_totalamount=grid.getCol('gir_totalamount',false,'sum');
    	grid.footerData('set',{gir_totalamount:'合计',gir_totalamount:gir_totalamount});	
    },
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'流水号',name: 'gir_number', index: 'gir_number', width: 100, title: false,fixed:true},
			{label:'礼品货号',name: 'pd_no', index: 'pd_no', width: 100, title: false},
	    	{label:'礼品名称',name: 'pd_name', index: 'pd_name', width: 140, title: false},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 80, title: false},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 80, title: false},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 80, title: false},
	    	{label:'发布数量',name: 'gir_totalamount', index: 'gir_totalamount', width:80, title: false,align:'right'},
	    	{label:'开始日期',name: 'gir_begindate', index: 'gir_begindate', width:130, title: false,align:'left',hidden:true},
	    	{label:'结束日期',name: 'gir_enddate', index: 'gir_enddate', width:130, title: false,align:'left',hidden:true},
	    	{label:'发布日期',name: 'gir_sysdate', index: 'gir_sysdate', width:90, title: false,align:'left'},
	    	{label:'发布人',name: 'user_name', index: 'user_name', width:90, title: false,align:'left'}
	    ];
		$('#grid').jqGrid({
			url:queryurl+'?'+Utils.buildParams(),
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			autowidth: true,
			height: gridWH.h,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'gir_id'
			},
			loadComplete: function(data){
				Public.resizeSpecifyGrid("grid",110,32);
				THISPAGE.gridTotal();
				var ids = $("#grid").jqGrid('getDataIDs');
            	var hasBra = false;
				for(var i=0;i < ids.length;i++){
					var rowData = $("#grid").jqGrid("getRowData", ids[i]);
					if(rowData.br_name != null && rowData.br_name != ''){
						hasBra = true;
						break;
					}
				}
				if(!hasBra){
					$("#grid").hideCol("br_name");
				}else{
					$("#grid").showCol("br_name");
				}
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新!'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				
			}
	    });
	},
	reloadData:function(){
		var params = Utils.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page: 1,url:queryurl+'?'+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		//返回
		$('#btn-back').on('click', function(e){
			e.preventDefault();
			api.close();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			$("#theDate").click();
			$("#begindate").val("");
			$("#enddate").val("");
			$("#gir_number").val("");
		});
		
		/*//刷新
		$('#btn-flush').on('click', function(e){
			e.preventDefault();
			$("#btn_reset").click();
			THISPAGE.reloadData();
		});*/
		
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",110,32);
		});
	}
}

THISPAGE.init();
