var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var queryurl = config.BASEPATH+'shop/gift/temp_list';
var querysizeurl = config.BASEPATH + 'shop/gift/temp_size';
var _height = $(parent).height()-300,_width = $(parent).width()-30;
var needHeaderSumRefresh = false;//表头汇总-总数量
var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式

var Utils = {
	buildParams:function(){
		var params = '';
		params += 'gil_shop_code='+$("#gi_shop_code").val();
		return params;
	},
	saveUpdateGift:function(){
		var gi_pd_code = $("#gi_pd_code").val();
		var gi_shop_code = $("#gi_shop_code").val();
		var gi_begindate = $("#gi_begindate").val();
		var gi_enddate = $("#gi_enddate").val();
		var gi_point = $("#gi_point").val();
		if(gi_point == '' || isNaN(gi_point)){
			Public.tips({type: 2, content : '请输入兑换积分！'});
			return;
		}
		if(gi_enddate == null || gi_enddate ==""){
			Public.tips({type: 2, content : '请选择兑换日期！'});
			return;
		}
		var params = {};
		params.gi_pd_code = gi_pd_code;
		params.gi_shop_code = gi_shop_code;
		params.gi_begindate = gi_begindate;
		params.gi_enddate = gi_enddate;
		params.gi_point = gi_point;
		$('#btnSave').attr('disabled','disabled');
		$.ajax({
			type:"post",
			url:config.BASEPATH+"shop/gift/update",
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data != undefined){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "保存成功!"});
						parent.childrenFlush();
						setTimeout("doBack()",300);
					}else{
						$('#btnSave').removeAttr('disabled');
						Public.tips({type: 1, content : "保存失败!"});
					}
				}
			}
		});	
	},
	refreshCurrentPageDate:function(refreshHeader){
		var currentMode = $("#CurrentMode").val();
		if(currentMode == '0'){//列表模式
			needListRefresh = true;
			Utils.ajaxGetGiftTempList();
        	needListRefresh = false;
        	needSizeRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		Utils.ajaxGetGiftTempTitles();
    		needListRefresh = true;
        	needSizeRefresh = false;
    	}
		if(refreshHeader != undefined && refreshHeader){//刷新表头数据
			needHeaderSumRefresh = true;
	   		Utils.ajaxGetGiftTempHeaderSum();
		}
	},
	ajaxGetGiftTempHeaderSum: function() {//刷新表头数据总数量
		if(!needHeaderSumRefresh){
			return;
		}
        $.ajax({
            type: "POST",
            async: false,
            url: config.BASEPATH + 'shop/gift/getGiftTempSum',
            cache: false,
            dataType: "json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
            		$("#totalamount").val(data.data);
            	}
            }
        });
        needHeaderSumRefresh = false;
    },
	ajaxGetGiftTempTitles: function() {//尺码模式刷新数据
    	if(!needSizeRefresh){
			return;
		}
    	$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/gift/temp_size_title',
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGroupGrid').GridUnload();
                    THISPAGE.addEvent();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
    },
    ajaxGetGiftTempList:function(){//列表模式刷新数据
    	if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
    }
};

var handle = {
	edit:function(rowid,pd_code){
		if (rowid!=null){
			var gil_shop_code = $("#gi_shop_code").val();
			var params = "pd_code="+pd_code;
			params += "&needVaildateStock=1";
			params += "&fromJsp=modify";
			$.dialog({
			   	title:'修改商品',
			   	content:'url:'+config.BASEPATH+'shop/gift/to_temp_update?'+params,
			   	data:{gil_shop_code:gil_shop_code},
			   	max: false,
			   	min: false,
			   	lock:true,
			   	width:800,
			   	height:480,
			   	fixed:false,
			   	drag: true,
			   	resize:false,
			   	close: function () {
			   		if(isRefresh){
			   			Utils.refreshCurrentPageDate();
			   			isRefresh = false;
			   		}
			   	}
			});
			
		}
	},
    updateAddAmountById : function(rowid,amount,srcAmount){
    	var rowData = $("#grid").jqGrid("getRowData", rowid);
    	var saveUrl = config.BASEPATH+'shop/gift/updateGiftListTempAddAmount';
    	var params = {gil_id:rowid,amount:amount};
    	$.ajax({
			type:"POST",
			url:saveUrl,
			data:params,
			async:false,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					needSizeRefresh = true;
			   		var footerData = $("#grid").footerData("get");
			   		footerData.gil_addamount = parseInt(footerData.gil_addamount) + amount - parseInt(srcAmount);
			   		$("#grid").footerData('set',{gil_addamount:footerData.gil_addamount});
			   		needHeaderSumRefresh = true;
			   		Utils.ajaxGetGiftTempHeaderSum();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    del: function(rowIds){//删除
    	if(rowIds != null && rowIds != ''){
    		var rowData = $("#grid").jqGrid("getRowData", rowIds);
			if(parseInt(rowData.gil_getamount)>0){
				Public.tips({type: 2, content : '已被领取不允许删除!'});
				return;
			}
			$.dialog.confirm('数据删除后无法恢复，确定要删除吗？',
				function(){
					$.ajax({
						type:"POST",
						url:config.BASEPATH+"shop/gift/temp_del",
						data:"gil_id="+rowIds,
						cache:false,
						dataType:"json",
						success:function(data){
							if(undefined != data && data.stat == 200){
								Public.tips({type: 3, content : '删除成功!'});
								$('#grid').jqGrid('delRowData', rowIds);
								THISPAGE.gridTotal();
								needSizeRefresh = true;
							}else{
								Public.tips({type: 1, content : data.message});
							}
						}
					});
				}, 
				function(){
			   		return;
				}
			);
		}else{
			Public.tips({type: 2, content : "请选择数据"});
		}
	}
};


function operFmatter(val, opt, row) {
	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
	html_con += '<span class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</span>&nbsp;';
	html_con += '<span class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</span>&nbsp;';
	html_con += '<span class="iconfont i-hand ui-icon-image" title="图片预览">&#xe654;</span>';
	html_con += '</div>';
	return html_con;
}
function operFmatterNoDel(val, opt, row) {
	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
	html_con += '<span class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</span>&nbsp;';
	html_con += '<span class="iconfont i-hand ui-icon-image" title="图片预览">&#xe654;</span>';
	html_con += '</div>';
	return html_con;
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		this.addEvent();
	},
	initDom:function(){
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        this.selectRow={};
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var sd_amount=grid.getCol('sd_amount',false,'sum');
    	var gil_totalamount=grid.getCol('gil_totalamount',false,'sum');
    	var gil_getamount=grid.getCol('gil_getamount',false,'sum');
    	var gil_addamount=grid.getCol('gil_addamount',false,'sum');
    	grid.footerData('set',{sd_amount:sd_amount,gil_totalamount:gil_totalamount,gil_getamount:gil_getamount,gil_addamount:gil_addamount});
    },
	initGrid:function(){
		var self=this;
		var gridWH = Public.setGrid();//操作
		var colModel = [
			{label:'操作',name: 'operate', width: 80, fixed:true, formatter: operFmatter, title: false,sortable:false,align:'center'},
			{label:'礼品编号',name: 'pd_code', index: 'pd_code', width: 120, title: false,hidden:true},
	    	{label:'礼品货号',name: 'pd_no', index: 'pd_no', width: 120, title: false,fixed:true},
	    	{label:'礼品名称',name: 'pd_name', index: 'pd_name', width: 120, title: false},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 100, title: false},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 80, title: false,align:'center'},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 80, title: false,align:'center'},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 80, title: false,align:'center'},
	    	{label:'库存数量',name: 'sd_amount', index: 'sd_amount', width:80, title: false,align:'right'},
	    	{label:'已发布数量',name: 'gil_totalamount', index: 'gil_totalamount', width:80, title: false,align:'right'},
	    	{label:'已兑换数量',name: 'gil_getamount', index: 'gil_getamount', width:80, title: false,align:'right'},
	    	{label:'本次发布',name: 'gil_addamount', index: 'gil_addamount', width:80, title: false,align:'right',editable:true},
	    	{label:'兑换积分',name: 'pi_score', index: 'pi_score', width:80, title: false,align:'right',hidden:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl+'?'+Utils.buildParams(),
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'gil_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			gridComplete:function(){ 
				var ids = $("#grid").jqGrid('getDataIDs');
            	var hasBra = false;
				for(var i=0;i < ids.length;i++){
					var cl = ids[i];
					var rowData = $("#grid").jqGrid("getRowData", cl);
					if(rowData.br_name != null && rowData.br_name != ''){
						hasBra = true;
						break;
					}
				}
				if(!hasBra){
					$("#grid").hideCol("br_name");
				}else{
					$("#grid").showCol("br_name");
				}
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，请检查您的网络链接！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
            	if(cellname == 'gil_addamount' && self.selectRow.value!=value){
					if(value == '' || isNaN(value)){
						return self.selectRow.value;
					}
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(Math.abs(parseFloat(value)).toFixed(0).length > 9|| Math.abs(parseInt(value)) >1e+9){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					if(parseInt(value) > parseInt(rowData.sd_amount)){
						Public.tips({type: 2, content : '库存不足！'});
						return self.selectRow.value;
					}
					if(parseInt(value) > 0 && parseInt(rowData.gil_totalamount)-parseInt(rowData.gil_getamount)+parseInt(value) > parseInt(rowData.sd_amount)){
						Public.tips({type: 2, content : '库存不足！'});
						return self.selectRow.value;
					}
					if(parseInt(value) < 0 && parseInt(rowData.gil_totalamount)+parseInt(value) < parseInt(rowData.gil_getamount)){
						Public.tips({type: 2, content : '总发布数量不能小于已领取数量！'});
						return self.selectRow.value;
					}
					
					handle.updateAddAmountById(rowid, parseInt(value),self.selectRow.value);
					return parseInt(value);
				}
				return value;
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
        var gridWH = Public.setGrid();//操作
		var colModel = [
			{label:'',name: 'sizeGroupCode', hidden: true},
		   	{label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
		    {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
		    {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: operFmatterNoDel, sortable:false,align:'center'},
		    {label:'礼品货号',name: 'pd_no', index: 'pd_no', width: 100},
		    {label:'礼品名称',name: 'pd_name', index: 'pd_name', width: 100},
		    {label:'颜色',name: 'cr_name', index: 'cr_name', width: 80},
		    {label:'杯型',name: 'br_name', index: 'br_name', width: 80},
		];
		colModel = colModel.concat(dms);
		colModel = colModel.concat([
			{label:'发布数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false}
		]);
        
		$('#sizeGroupGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
			height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				Public.resizeSpecifyGrid('sizeGroupGrid', 80 + (dyns.length * 32), 32);
			},
			gridComplete:function(){
				var ids = $("#grid").jqGrid('getDataIDs');
            	var hasBra = false;
				for(var i=0;i < ids.length;i++){
					var cl = ids[i];
					var rowData = $("#grid").jqGrid("getRowData", cl);
					if(rowData.br_name != null && rowData.br_name != ''){
						hasBra = true;
						break;
					}
				}
				if(!hasBra){
					$("#sizeGroupGrid").hideCol("br_name");
				}else{
					$("#sizeGroupGrid").showCol("br_name");
				}
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				var rowData = $("#sizeGroupGrid").jqGrid("getRowData", rowid);
				handle.edit(rowid, rowData.pd_code);
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
            	var sizeG = $("#sizeGroupGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            },
            onCellSelect: function(rowid,iCol,cellcontent,e){

            }
	    });
		if (dySize >= 2 || (dySize == 1 && dyns[0].length >0)) {
            $('#sizeGroupGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGroupGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            
            for (var key in sizeCodeMap) {
                $('#sizeGroupGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        } else {
        	$('#sizeGroupGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
        }
    },
    reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl+'?'+Utils.buildParams()}).trigger("reloadGrid");
    },
	initEvent:function(){
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",110,32);
		});
	},
	addEvent:function(){
		//修改--列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            if (id == null || id == '') {
                $.dialog.tips("你没有选中单据,请重新选择!", 2, "32X32/hits.png");
            } else {
            	var rowData = $("#grid").jqGrid("getRowData", id);
            	handle.edit(id, rowData.pd_code);
            }
        });
        $('#sizeGroupGrid').on('click', '.operating .ui-icon-pencil', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            if (id == null || id == '') {
                $.dialog.tips("你没有选中单据,请重新选择!", 2, "32X32/hits.png");
            } else {
            	var rowData = $("#sizeGroupGrid").jqGrid("getRowData", id);
            	handle.edit(id, rowData.pd_code);
            }
        });
		//查看图片
        $('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	var id = $(this).parent().data('id');
  			var row=$('#grid').getRowData(id);
  			viewProductImg(row.pd_code);
        });
        $('#sizeGroupGrid').on('click', '.operating .ui-icon-image', function (e) {
        	var id = $(this).parent().data('id');
  			var row=$('#sizeGroupGrid').getRowData(id);
  			viewProductImg(row.pd_code);
        });
      //删除
        $('#grid').on('click', '.operating .ui-icon-trash', function (e) {
            e.preventDefault();
            var id = $(this).parent().data('id');
            handle.del(id);
        });
	}
}
THISPAGE.init();
