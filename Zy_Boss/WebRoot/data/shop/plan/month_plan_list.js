var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SHOP21;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/plan/month_page';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择要货门店',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#mp_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#mp_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'add'){
			url = config.BASEPATH+"shop/plan/to_month_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'view'){
			url = config.BASEPATH+"shop/plan/to_month_view?mp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"shop/plan/to_month_view?mp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	del: function(rowId){//删除
		var rowData = $("#grid").jqGrid("getRowData", rowId);
	 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/plan/month_del',
				data:{"number":rowData.mp_number},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowId);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
    },
    callback: function(data, oper, dialogWin){
		if(oper == "approve") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.mp_ar_state == '2'){//退回
			btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
		}else if (row.mp_ar_state == '1') {//完成
        	btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
        }else if (row.mp_ar_state == '0') {//完成
        	btnHtml += '<input type="button" value="审核" class="btn_sp" onclick="javascript:handle.operate(\'approve\',' + opt.rowId + ');" />';
        }
		btnHtml += '<input type="button" value="流程" class="btn_qh" onclick="javascript:listBillProcess(\''+row.mp_number+'\',\'t_shop_monthplan\');" />';
		return btnHtml;
	},
	formatArState:function(val, opt, row){
		if(val == 0){
			return '未审核';
		}else if(val == 1){
			return '已审核';
		}else if(val == 2){
			return '已退回';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		this.$_ar_state = $("#ar_state").cssRadio({ callback: function($_obj){
			$("#mp_ar_state").val($_obj.find("input").val());
		}});
		this.initYear();
		this.initMonth();
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
		    {Code:"",Name:"全部"},
		    {Code:""+parseInt(currentYear+1)+"",Name:""+parseInt(currentYear+1)+""},
		    {Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""}
		];
		$('#span_year').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :205,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#mp_year").val(data.Code);
				}
			}
		}).getCombo();
	},
	initMonth:function(){
		var data = [
		    {Code:"",Name:"全部"},
		    {Code:"1",Name:"1月份"},
		    {Code:"2",Name:"2月份"},
		    {Code:"3",Name:"3月份"},
		    {Code:"4",Name:"4月份"},
		    {Code:"5",Name:"5月份"},
		    {Code:"6",Name:"6月份"},
		    {Code:"7",Name:"7月份"},
		    {Code:"8",Name:"8月份"},
		    {Code:"9",Name:"9月份"},
		    {Code:"10",Name:"10月份"},
		    {Code:"11",Name:"11月份"},
		    {Code:"12",Name:"12月份"}
		];
		$('#span_month').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :205,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#mp_month").val(data.Code);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'操作',name: 'operate',width: 100, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'',name: 'mp_us_id',index: 'mp_us_id',width:100,hidden:true},
	    	{label:'计划编号',name: 'mp_number',index: 'mp_number',width:150},
	    	{label:'店铺名称',name: 'shop_name',index: 'mp_shop_code',width:150},
	    	{label:'年份',name: 'mp_year',index: 'mp_year',width:80,align:'center'},
	    	{label:'月份',name: 'mp_month',index: 'mp_month',width:80,align:'center'},
	    	{label:'计划金额',name: 'mp_sell_money',index: 'mp_sell_money',width:80,align:'right',formatter: PriceLimit.formatMoney},
	    	{label:'状态',name: 'mp_ar_state',index: 'mp_ar_state',width:80,formatter: handle.formatArState},
	    	{label:'审核日期',name: 'mp_ar_date',index: 'mp_ar_date',width:80},
	    	{label:'制单日期',name: 'mp_sysdate',index: 'mp_sysdate',width:80},
	    	{label:'制单人',name: 'mp_maker',index: 'mp_maker',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'mp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.operate('view',rowid);
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&mp_ar_state='+$("#mp_ar_state").val();
		params += '&mp_shop_code='+$("#mp_shop_code").val();
		params += '&mp_year='+$("#mp_year").val();
		params += '&mp_month='+$("#mp_month").val();
		params += '&mp_number='+Public.encodeURI($("#mp_number").val());
		return params;
	},
	reset:function(){
		$("#mp_shop_code").val("");
		$("#mp_number").val("");
		$("#shop_name").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_ar_state.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
		$('#span_year').getCombo().selectByValue("");
		$('#span_month').getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		$('#btn-del').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			if(rowId == null || rowId == ''){
				Public.tips({type: 1, content : "您未选择任何数据"});
				return;
			}
			var rowData =$("#grid").jqGrid("getRowData", rowId);
			if(rowData.mp_ar_state != "未审核" && rowData.mp_ar_state != "已退回"){
				Public.tips({type: 1, content : "单据已审核通过，不能删除"});
				return;
			}
			handle.del(rowId)
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();