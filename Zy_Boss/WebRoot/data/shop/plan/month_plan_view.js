var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var mp_number = $("#mp_number").val();

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+mp_number;
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"shop/plan/month_approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.mp_id;
		pdata.mp_number=data.mp_number;
		pdata.mp_ar_state=data.mp_ar_state;
		pdata.mp_ar_date=data.mp_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initGrid();
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
	},
    gridTotal:function(){
		var grid=$('#grid');
		var mpd_sell_money_pre=grid.getCol('mpd_sell_money_pre',false,'sum');
		var mpd_sell_money_plan=grid.getCol('mpd_sell_money_plan',false,'sum');
    	grid.footerData('set',{mpd_sell_money_pre:mpd_sell_money_pre,mpd_sell_money_plan:mpd_sell_money_plan});
    },
    initGrid:function(){
		var colModel = [
	    	{label:'日期',name: 'mpd_day', index: 'mpd_day', width: 80, title: false,fixed:true,align:'center'},
	    	{label:'同期销售',name: 'mpd_sell_money_pre', index: 'mpd_sell_money_pre', width: 100, title: false,align:'right'},
	    	{label:'计划金额',name: 'mpd_sell_money_plan', index: 'mpd_sell_money_plan', width: 100, title: false,align:'right'},
	    	{label:'备注',name: 'mpd_remark', index: 'mpd_remark', width: 200, title: false}
	    ];
		$('#grid').jqGrid({
			url:config.BASEPATH+'shop/plan/month_listDay/'+mp_number,
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: $(parent).width()-32,
			height: $(parent).height()-242,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			sortable:false,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'mpd_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			}
	    });
	},
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();