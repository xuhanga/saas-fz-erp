var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

function formatFloat(value){
	if(value == ''){
		return 0;
	}
	if(isNaN(value)){
		return 0;
	}
	return parseFloat(value);
}
function calcPlanMoney_Type_0(){
	//参数设置
	var pl_gross_profit_rate = formatFloat($("#pl_gross_profit_rate").val());
	var pl_sell_grow_percent = formatFloat($("#pl_sell_grow_percent").val());
	var pl_expense_grow_percent = formatFloat($("#pl_expense_grow_percent").val());
	//同期数据
	var pl_sell_money_pre = formatFloat($("#pl_sell_money_pre").val());
	var pl_cost_money_pre = formatFloat($("#pl_cost_money_pre").val());
	var pl_expense_pre = formatFloat($("#pl_expense_pre").val());
	var pl_gross_money_pre = formatFloat($("#pl_gross_money_pre").val());
	var pl_net_money_pre = formatFloat($("#pl_net_money_pre").val());
	var pl_breakeven_pre = formatFloat($("#pl_breakeven_pre").val());
	var pl_gross_profit_rate_pre = formatFloat($("#pl_gross_profit_rate_pre").val());
	//目标计算
	if(pl_gross_profit_rate == 0){
		pl_gross_profit_rate = pl_gross_profit_rate_pre;
	}
	var pl_sell_money = (1+(pl_sell_grow_percent/100))*pl_sell_money_pre;
	var pl_expense = (1+(pl_expense_grow_percent/100))*pl_expense_pre;
	var pl_gross_money = pl_gross_profit_rate*pl_sell_money;
	$("#pl_sell_money").val(Math.round(pl_sell_money));
	$("#pl_expense").val(Math.round(pl_expense));
	$("#pl_gross_money").val(Math.round(pl_gross_money));
	$("#pl_net_money").val(Math.round(pl_gross_money-pl_expense));
	$("#pl_cost_money").val(Math.round(pl_sell_money-pl_gross_money));
	if(pl_gross_profit_rate != 0){
		$("#pl_breakeven").val(Math.round(pl_expense/pl_gross_profit_rate));
	}else{
		$("#pl_breakeven").val(0);
	}
	
}
function calcPlanMoney_Type_1(){
	//参数设置
	var pl_gross_profit_rate = formatFloat($("#pl_gross_profit_rate").val());
	var pl_breakeven_grow_percent = formatFloat($("#pl_breakeven_grow_percent").val());
	if(pl_gross_profit_rate == 0){
		return;
	}
	//费用总计
	var footerData = $("#expensegrid").footerData("get");
	var pl_expense = parseFloat(footerData.money);
	//目标计算
	var pl_breakeven = pl_expense/pl_gross_profit_rate;
	var pl_sell_money = pl_breakeven * (1+pl_breakeven_grow_percent/100);
	var pl_gross_money = pl_sell_money * pl_gross_profit_rate;
	var pl_net_money = pl_gross_money - pl_expense;
	var pl_cost_money = pl_sell_money - pl_gross_money;
	$("#pl_expense_1").val(Math.round(pl_expense));
	$("#pl_breakeven_1").val(Math.round(pl_breakeven));
	$("#pl_sell_money_1").val(Math.round(pl_sell_money));
	$("#pl_gross_money_1").val(Math.round(pl_gross_money));
	$("#pl_net_money_1").val(Math.round(pl_net_money));
	$("#pl_cost_money_1").val(Math.round(pl_cost_money));
}

function initPlan(){
	var pl_type = $("#pl_type").val();
	if(pl_type == "0"){
		Utils.loadPreData();
	}else if(pl_type == "1"){
		
	}else if(pl_type == "2"){
		
	}
	$('#monthdatagrid').GridUnload();
}

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择计划门店',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				Utils.check(selected.sp_code,selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					Utils.check(selected.sp_code,selected.sp_name);
				}
			},
			cancel:true
		});
	},
	check:function(shop_code,shop_name){
		var pl_year = $("#pl_year").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/plan/check/'+shop_code+'/'+pl_year,
			data:{},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == null){
						$("#pl_shop_code").val(shop_code);
						$("#shop_name").val(shop_name);
						
						var pl_type = $("#pl_type").val();
						if(pl_type == "0"){//参考同期数据
							Utils.loadPreData();
						}else if(pl_type == "1"){//费用开支估计
							calcPlanMoney_Type_1();
						}
						
					}else{
						Public.tips({type: 1, content : '该年份计划已经制定！'});
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	loadPreData:function(){
		var pl_shop_code = $("#pl_shop_code").val();
		var pl_year = $("#pl_year").val();
		if(pl_shop_code == "" || pl_year == ""){
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/plan/loadPreData/'+pl_shop_code+'/'+pl_year,
			data:{},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var preData = data.data;
					if(preData == null){
						Public.tips({type: 1, content : '往年同期数据不存在，可以按相关费用估算进行计划制定！'});
						$("#pl_sell_money_pre").val("");
						$("#pl_cost_money_pre").val("");
						$("#pl_gross_money_pre").val("");
						$("#pl_net_money_pre").val("");
						$("#pl_breakeven_pre").val("");
						$("#pl_expense_pre").val("");
						$("#pl_gross_profit_rate_pre").val("");
						calcPlanMoney_Type_0();
						return;
					}
					$("#pl_sell_money_pre").val(PriceLimit.formatMoney(preData.pl_sell_money_pre));
					$("#pl_cost_money_pre").val(PriceLimit.formatMoney(preData.pl_cost_money_pre));
					$("#pl_gross_money_pre").val(PriceLimit.formatMoney(preData.pl_gross_money_pre));
					$("#pl_net_money_pre").val(PriceLimit.formatMoney(preData.pl_net_money_pre));
					$("#pl_breakeven_pre").val(PriceLimit.formatMoney(preData.pl_breakeven_pre));
					$("#pl_expense_pre").val(PriceLimit.formatMoney(preData.pl_expense_pre));
					$("#pl_gross_profit_rate_pre").val(PriceLimit.formatMoney(preData.pl_gross_profit_rate_pre));
					calcPlanMoney_Type_0();
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#wt_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#wt_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	checkParamNumberEmpty:function(value,tip){
		if(value == undefined || value == null || value == '' || isNaN(value)){
			Public.tips({type: 2, content : tip});
			return false;
		}
		return true;
	},
	checkParamEmpty:function(value,tip){
		if(value == undefined || value == null || value == ''){
			Public.tips({type: 2, content : tip});
			return false;
		}
		return true;
	},
	save:function(){
		var params = {};
		var plan = {};
		plan.pl_name = $("#pl_name").val();
		plan.pl_shop_code = $("#pl_shop_code").val();
		plan.pl_date = $("#pl_date").val();
		plan.pl_year = $("#pl_year").val();
		plan.pl_type = $("#pl_type").val();
		plan.pl_remark = $("#pl_remark").val();
		if(!handle.checkParamEmpty(plan.pl_shop_code, '请选择店铺！')){
			return;
		}
		if(!handle.checkParamEmpty(plan.pl_name, '请输入计划名称！')){
			return;
		}
		if(plan.pl_type == '0'){
			plan.pl_gross_profit_rate = $("#pl_gross_profit_rate").val();
			if(!handle.checkParamNumberEmpty(plan.pl_gross_profit_rate, '请输入预计毛利率！')){
				return;
			}
			plan.pl_sell_grow_percent = $("#pl_sell_grow_percent").val();
			plan.pl_expense_grow_percent = $("#pl_expense_grow_percent").val();
			plan.pl_sell_money_pre = $("#pl_sell_money_pre").val();
			plan.pl_cost_money_pre = $("#pl_cost_money_pre").val();
			plan.pl_expense_pre = $("#pl_expense_pre").val();
			plan.pl_gross_money_pre = $("#pl_gross_money_pre").val();
			plan.pl_net_money_pre = $("#pl_net_money_pre").val();
			plan.pl_breakeven_pre = $("#pl_breakeven_pre").val();
			plan.pl_gross_profit_rate_pre = $("#pl_gross_profit_rate_pre").val();
			plan.pl_expense = $("#pl_expense").val();
			plan.pl_breakeven = $("#pl_breakeven").val();
			plan.pl_sell_money = $("#pl_sell_money").val();
			plan.pl_cost_money = $("#pl_cost_money").val();
			plan.pl_gross_money = $("#pl_gross_money").val();
			plan.pl_net_money = $("#pl_net_money").val();
		}else if(plan.pl_type == '1'){
			plan.pl_gross_profit_rate = $("#pl_gross_profit_rate").val();
			if(!handle.checkParamNumberEmpty(plan.pl_gross_profit_rate, '请输入预计毛利率！')){
				return;
			}
			plan.pl_breakeven_grow_percent = $("#pl_breakeven_grow_percent").val();
			plan.pl_expense = $("#pl_expense_1").val();
			plan.pl_breakeven = $("#pl_breakeven_1").val();
			plan.pl_sell_money = $("#pl_sell_money_1").val();
			plan.pl_cost_money = $("#pl_cost_money_1").val();
			plan.pl_gross_money = $("#pl_gross_money_1").val();
			plan.pl_net_money = $("#pl_net_money_1").val();
			var expenses = [];
			var ids = $("#expensegrid").jqGrid('getDataIDs');
			for(var i=0;i < ids.length;i++){
				var rowData = $("#expensegrid").jqGrid("getRowData", ids[i]);
				
				if(formatFloat(rowData.money) != 0){
					var expense = {};
					expense.pe_expensecode = rowData.pp_code;
					expense.pe_money = rowData.money;
					expenses.push(expense);
				}
			}
			if(expenses.length == 0){
				Public.tips({type: 2, content : '请输入费用金额！'});
				return;
			}
			params.expenses = Public.encodeURI(JSON.stringify(expenses));
		}else if(plan.pl_type == '2'){
			plan.pl_sell_money = $("#pl_sell_money_2").val();
		}
		if(!handle.checkParamNumberEmpty(plan.pl_sell_money, '请设置目标金额！')){
			return;
		}
		if(plan.pl_sell_money == 0){
			Public.tips({type: 2, content : '请设置目标金额！'});
			return;
		}
		var monthdatas = [];
		var ids = $("#monthdatagrid").jqGrid('getDataIDs');
		for(var i=0;i < ids.length;i++){
			var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
			var monthdata = {};
			monthdata.pm_month = ids[i];
			monthdata.pm_sell_money_pre = rowData.premoney;
			monthdata.pm_sell_money_plan = rowData.planmoney;
			monthdata.pm_remark = rowData.remark;
			monthdatas.push(monthdata);
		}
		if(monthdatas.length == 0){
			Public.tips({type: 2, content : '请计划分配到月！'});
			return;
		}
		params.monthdatas = Public.encodeURI(JSON.stringify(monthdatas));
		params.plan = Public.encodeURI(JSON.stringify(plan));
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"shop/plan/save",
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.pl_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#pl_date").val(config.TODAY);
		this.$_pl_type = $("#td_pl_type").cssRadio({ callback: function($_obj){
			var pl_type = $_obj.find("input").val();
			$("#pl_type").val(pl_type);
			$("#plan_data_0").hide();
			$("#plan_data_1").hide();
			$("#plan_data_2").hide();
			if(pl_type == '0'){
				$("#grow_set_table").show();
				$("#span_sell_grow_percent").show();
				$("#span_expense_grow_percent").show();
				$("#span_breakeven_grow_percent").hide();
				$("#plan_data_0").show();
			}else if(pl_type == '1'){
				$("#grow_set_table").show();
				$("#span_sell_grow_percent").hide();
				$("#span_expense_grow_percent").hide();
				$("#span_breakeven_grow_percent").show();
				$("#plan_data_1").show();
			}else if(pl_type == '2'){
				$("#grow_set_table").hide();
				$("#plan_data_2").show();
			}
		}});
		this.initYear();
		this.initPlanType();
		this.selectRow={};
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
		    {Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear+1)+"",Name:""+parseInt(currentYear+1)+""}
		];
		$('#span_year').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :158,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#pl_year").val(data.Code);
					$("#pl_name").val(data.Code+"年度目标计划");
					initPlan();
				}
			}
		}).getCombo();
	},
	initPlanType:function(){
		var data = [
		    {Code:"0",Name:"参考同期数据制定"},
		    {Code:"1",Name:"根据预估费用制定"},
		    {Code:"2",Name:"目标销售金额制定"}
		];
		$('#span_pl_type').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :158,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					var pl_type = data.Code;
					$("#pl_type").val(pl_type);
					$("#plan_data_0").hide();
					$("#plan_data_1").hide();
					$("#plan_data_2").hide();
					if(pl_type == '0'){
						$("#grow_set_tr").show();
						$("td[name='type_0']").show();
						$("td[name='type_1']").hide();
						$("#plan_data_0").show();
					}else if(pl_type == '1'){
						$("#grow_set_tr").show();
						$("td[name='type_0']").hide();
						$("td[name='type_1']").show();
						$("#plan_data_1").show();
						THISPAGE.initExpenseGrid();
					}else if(pl_type == '2'){
						$("#grow_set_tr").hide();
						$("#plan_data_2").show();
					}
					initPlan();
				}
			}
		}).getCombo();
	},
	initExpenseGrid:function(){
		var self=this;
		var colModel = [
	    	{label:'编号',name: 'pp_code', index: 'pp_code', width: 80, title: false,fixed:true},
	    	{label:'费用名称',name: 'pp_name', index: 'pp_name', width: 140, title: false},
	    	{label:'金额',name: 'money', index: 'money', width: 100, title: false,editable:true,formatter: PriceLimit.formatMoney}
	    ];
		$('#expensegrid').jqGrid({
			url:config.BASEPATH+'money/property/list/0',
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: 400,
			height: 150,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#expensepage',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			cellEdit: true,
            cellsubmit: 'clientArray',
			//scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				$("#expensegrid").footerData('set',{pp_code:'合计:',money:"0.00"});
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(cellname == 'money'){
					if(self.selectRow.value != value){
						if(value == '' || isNaN(value) || parseFloat(value)<0){
							Public.tips({type: 2, content : '请输入正数！'});
							return self.selectRow.value;
						}
						if(parseFloat(self.selectRow.value) == parseFloat(value)){
							return self.selectRow.value;
						}
						if(parseFloat(value).toFixed(2).length > 11 || Math.abs(parseFloat(value)) > 1e+11){
							return self.selectRow.value;
						}
						return parseFloat(value).toFixed(2);
					}
				}
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'money' && self.selectRow.value != value){
					THISPAGE.gridExpenseTotal();
					calcPlanMoney_Type_1();
				}
				
			}
	    });
	},
	gridExpenseTotal:function(value,srcMoney){
		var grid=$('#expensegrid');
		var money=grid.getCol('money',false,'sum');
    	grid.footerData('set',{money:money});
    },
    gridMonthDataTotal:function(){
		var grid=$('#monthdatagrid');
		var premoney=grid.getCol('premoney',false,'sum');
		var planmoney=grid.getCol('planmoney',false,'sum');
    	grid.footerData('set',{premoney:premoney,planmoney:planmoney});
    },
    initMonthDataGrid:function(){
		var self=this;
		var colModel = [
	    	{label:'月份',name: 'month', index: 'month', width: 80, title: false,fixed:true},
	    	{label:'同期销售',name: 'premoney', index: 'premoney', width: 100, title: false},
	    	{label:'计划金额',name: 'planmoney', index: 'planmoney', width: 100, title: false,editable:true},
	    	{label:'备注',name: 'remark', index: 'remark', width: 200, title: false,editable:true},
	    	{label:'',name: 'ismodify', index: 'ismodify', width: 100, title: false,hidden:true}
	    ];
		var params = '';
		params += 'shop_code='+$("#pl_shop_code").val();
		params += '&year='+$("#pl_year").val();
		params += '&pl_type='+$("#pl_type").val();
		$('#monthdatagrid').jqGrid({
			url:config.BASEPATH+'shop/plan/loadPreDataMonth?'+params,
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: 800,
			height: $(parent).height()-200,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			sortable:false,
			rownumbers: true,//行号
			pager: '#monthdatapage',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:true,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			cellEdit: true,
            cellsubmit: 'clientArray',
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.initPlanSellMoney();
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(self.selectRow.value == value){
					return value;
				}
				if(cellname == 'planmoney'){
					if(value == '' || isNaN(value) || parseFloat(value)<0){
						Public.tips({type: 2, content : '请输入正数！'});
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || Math.abs(parseFloat(value)) > 1e+11){
						return self.selectRow.value;
					}
					if(!THISPAGE.checkInutPlanSellMoney(rowid, value)){
						Public.tips({type: 2, content : '计划金额总和超出范围！'});
						return self.selectRow.value;
					}
					$("#monthdatagrid").jqGrid('setRowData',rowid,{ismodify:'1'});
					return parseInt(value);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'planmoney' && self.selectRow.value != value){
					THISPAGE.reallotPlanSellMoney();//重新分配金额
				}
			}
	    });
	},
	checkInutPlanSellMoney:function(rowid,value){
		var pl_type = $("#pl_type").val();
		var pl_sell_money = null;
		if(pl_type == "0"){
			pl_sell_money = formatFloat($("#pl_sell_money").val());
		}else if(pl_type == "1"){
			pl_sell_money = formatFloat($("#pl_sell_money_1").val());
		}else if(pl_type == "2"){
			pl_sell_money = formatFloat($("#pl_sell_money_2").val());
		}
		var ids = $("#monthdatagrid").jqGrid('getDataIDs');
		var totalModifyMoney = 0;
		for(var i=0;i < ids.length;i++){
			var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
			if(rowData.ismodify == '1' && ids[i] != rowid){
				totalModifyMoney += parseFloat(rowData.planmoney);
			}
		}
		if(totalModifyMoney+parseFloat(value)>pl_sell_money){
			return false;
		}
		return true;
	},
	initPlanSellMoney:function(){
		var pl_type = $("#pl_type").val();
		var pl_sell_money = null;
		if(pl_type == "0"){
			pl_sell_money = formatFloat($("#pl_sell_money").val());
		}else if(pl_type == "1"){
			pl_sell_money = formatFloat($("#pl_sell_money_1").val());
		}else if(pl_type == "2"){
			pl_sell_money = formatFloat($("#pl_sell_money_2").val());
		}
		var money = 0;
		for (var i = 0; i < 12; i++) {
			money = Math.round(pl_sell_money/12);
			$("#monthdatagrid").jqGrid('setRowData',i+1,{ismodify:'0',planmoney:money});
		}
		if(pl_type == '0'){//同期数据参考需要将存在同期数据的月份重新按照比例分配
			var totalPreMoney = 0;
			var totalAllotMoney = 0;
			var ids = $("#monthdatagrid").jqGrid('getDataIDs');
			for(var i=0;i < ids.length;i++){
				var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
				if(rowData.ismodify == '0' && parseFloat(rowData.premoney) > 0){
					totalPreMoney += parseFloat(rowData.premoney);
					totalAllotMoney += parseFloat(rowData.planmoney);
				}
			}
			for(var i=0;i < ids.length;i++){
				var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
				if(rowData.ismodify == '0' && parseFloat(rowData.premoney) > 0){
					money = Math.round(totalAllotMoney*parseFloat(rowData.premoney)/totalPreMoney);
					$("#monthdatagrid").jqGrid('setRowData',ids[i],{planmoney:money});
				}
			}
		}
		THISPAGE.gridMonthDataTotal();
	},
	reallotPlanSellMoney:function(){//重新分配未修改计划金额
		var pl_type = $("#pl_type").val();
		var pl_sell_money = null;
		if(pl_type == "0"){
			pl_sell_money = formatFloat($("#pl_sell_money").val());
		}else if(pl_type == "1"){
			pl_sell_money = formatFloat($("#pl_sell_money_1").val());
		}else if(pl_type == "2"){
			pl_sell_money = formatFloat($("#pl_sell_money_2").val());
		}
		
		var ids = $("#monthdatagrid").jqGrid('getDataIDs');
		var totalModifyMoney = 0;
		var modifyCount = 0;
		for(var i=0;i < ids.length;i++){
			var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
			if(rowData.ismodify == '1'){
				totalModifyMoney += parseFloat(rowData.planmoney);
				modifyCount++;
			}
		}
		//将未修改的月份首先平均分配
		var leftPlanMoney = pl_sell_money-totalModifyMoney;
		var money = 0;
		for(var i=0;i < ids.length;i++){
			var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
			if(rowData.ismodify == '0'){
				money = Math.round(leftPlanMoney/(12-modifyCount));
				$("#monthdatagrid").jqGrid('setRowData',ids[i],{planmoney:money});
			}
		}
		
		//同期数据参考需要将存在同期数据且未修改的月份重新按照比例分配
		if(pl_type == '0'){
			var totalPreMoney = 0;
			var totalAllotMoney = 0;
			for(var i=0;i < ids.length;i++){
				var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
				if(rowData.ismodify == '0' && parseFloat(rowData.premoney) > 0){
					totalPreMoney += parseFloat(rowData.premoney);
					totalAllotMoney += parseFloat(rowData.planmoney);
				}
			}
			for(var i=0;i < ids.length;i++){
				var rowData = $("#monthdatagrid").jqGrid("getRowData", ids[i]);
				if(rowData.ismodify == '0' && parseFloat(rowData.premoney) > 0){
					money = Math.round(totalAllotMoney*parseFloat(rowData.premoney)/totalPreMoney);
					$("#monthdatagrid").jqGrid('setRowData',ids[i],{planmoney:money});
				}
			}
		}
		THISPAGE.gridMonthDataTotal();
	},
	initEvent:function(){
		$("#btn_close").click(function(e){
			e.preventDefault();
			$.dialog.confirm('确定要放弃制定的目标计划吗？',function(){
				api.close();
			});
		});
	}
};

THISPAGE.init();