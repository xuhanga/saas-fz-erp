var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'shop/price/temp_list';

var _height = $(parent).height()-270,_width = $(parent).width()-31;

var Utils = {
	doQueryShop : function(){
		var shopCodeBefore = $("#sp_shop_code").val();
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_price_dialog',
			data : {multiselect:true},
			width : 550,
			height : 390,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var datas = commonDia.content.doSelect();
				if($.isEmptyObject(datas)){
					return false;
				} else {
					var codes = [];
					var names = [];
					for(var i=0;i<datas.length;i++){
						codes.push(datas[i].sp_code);
						names.push(datas[i].sp_name);
					}
					$("#sp_shop_code").val(codes.join(","));
					$("#shop_name").val(names.join(","));
				}
			},
			close:function(){
				if($("#sp_shop_code").val() == ''){
            		return;
            	}
            	if(shopCodeBefore == $("#sp_shop_code").val()){
            		return;
            	}
            	
            	var sp_shop_type = $("#sp_shop_type").val();
				$("#SortPrice").css("display","");
				if(sp_shop_type == '3'){//自营店
					$("#SortPrice").css("display","none");
				}
				
				//合伙店
				if(sp_shop_type == '5'){
					$("#CostPrice").css("display","");
				} else {
					$("#CostPrice").css("display","none");
				}
				
				var ids = $("#grid").jqGrid('getDataIDs');
            	if(ids.length ==0){
            		return;
            	}
            	var discount = $("#discount").val();
            	var sp_shop_code = $("#sp_shop_code").val();
            	$.ajax({
					type:"POST",
					url:config.BASEPATH+"shop/price/shop_change_templist",
					data:"sp_shop_code="+sp_shop_code+"&shopCodeBefore="+shopCodeBefore+"&discount="+discount+"&sp_shop_type="+sp_shop_type,
					cache:false,
					dataType:"json",
					success:function(data){
						if(data.stat == 200){
			        		THISPAGE.reloadGridData();
			        	}
					}
				});
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#sp_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	//打开商品
	openProduct:function (pd_no){
		if($("#sp_shop_code").val()==""){
			Public.tips({type: 2, content: '分店列表不能为空！'});
			return;
		}
		if($("#sp_manager").val()==""){
			Public.tips({type: 2, content: '经办人不能为空！'});
			return;
		}
		var sp_shop_code = $("#sp_shop_code").val();
		var discount = $("#discount").val();
		var sp_shop_type = $("#sp_shop_type").val();
		var url = 'url:'+config.BASEPATH+'base/product/to_list_dialog_price?sp_shop_code='+sp_shop_code+'&discount='+discount;
		if(pd_no != undefined && pd_no !=''){
			url+= '&pd_no='+Public.encodeURI(pd_no);
		}
		url+= '&sp_shop_type='+sp_shop_type;
		$.dialog({ 
		   	title:'选择商品',
		   	data : {multiselect:true},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:560,
		   	height:397,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:url,
		   	ok:function(){
		   		this.content.doSelect();
		   	},
		   	cancel:true,
		   	close:function(){		   		
		   		THISPAGE.reloadGridData();
		   	}
		 });
	},
	queryProductInfo:function(value){
		if(value == undefined || value== null || value == ""){
			Utils.openProduct(value);
			return ;
		}
		if($("#sp_shop_code").val()==""){
			Public.tips({type: 2, content: '分店列表不能为空！'});
			return;
		}
		if($("#sp_manager").val()==""){
			Public.tips({type: 2, content: '经办人不能为空！'});
			return;
		}
		
		var sp_shop_code = $("#sp_shop_code").val();
		var discount = $("#discount").val();
		var sp_is_sellprice = THISPAGE.$_SellPrice.chkVal().join() ? '1' : '0';
		var sp_is_vipprice = THISPAGE.$_VipPrice.chkVal().join() ? '1' : '0';
		var sp_is_sortprice = THISPAGE.$_SortPrice.chkVal().join() ? '1' : '0';
		var sp_is_costprice = THISPAGE.$_CostPrice.chkVal().join() ? '1' : '0';
		var sp_shop_type = $("#sp_shop_type").val();
		var params = {};
		params.sp_is_sellprice = sp_is_sellprice;
		params.sp_is_vipprice = sp_is_vipprice;
		params.sp_is_sortprice = sp_is_sortprice;
		params.sp_is_costprice = sp_is_costprice;
		params.sp_shop_code = sp_shop_code;
		params.sp_shop_type = sp_shop_type;
		params.discount = discount;
		params.value = Public.encodeURI(value);
		
		$.ajax({
			type: "POST",
	        url: config.BASEPATH + "shop/price/save_tempList_Enter",
	        data:params,
	        cache: false,
	        dataType: "json",
	        success: function (data) {
	        	if(data.stat == 200){
	        		THISPAGE.reloadGridData();
	        	}else if(data.stat == 304){
	        		Public.tips({type: 2, content : '您输入的货号已存在！'});
	        	}else{
	        		Utils.openProduct(value);
	        	}
			}
		});
	}
};

function buildUrl(){
	var sp_manager = $("#sp_manager").val();
	var sp_shop_code = $("#sp_shop_code").val();
	var sp_makerdate = $("#sp_makerdate").val();
	var sp_remark = $("#sp_remark").val();
	var sp_shop_type = $("#sp_shop_type").val(); 
	var sp_number = $("#sp_number").val(); 
	var sp_id = $("#sp_id").val(); 
	var url = "sp_manager="+ sp_manager + "&sp_shop_code=" + sp_shop_code +"&sp_makerdate="+sp_makerdate+"&sp_remark="+sp_remark+"&sp_shop_type="+sp_shop_type+"&sp_number="+sp_number;
	if(sp_id != undefined){
		url += "&sp_id="+sp_id;
	}
	url = encodeURI(encodeURI(url));
	return url;
}

var handle = {
	updateUnitPrice:function(pd_code,unitPrice){
		var saveUrl = config.BASEPATH+'shop/price/update_templist_byPdCode';
	    var params = {pd_code:pd_code,unitPrice:unitPrice};
	    $.ajax({
			type:"POST",
			url:saveUrl,
			data:params,
			async:false,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data.stat == 200){
					Public.tips({type: 3, content : '修改成功'});
			   		THISPAGE.reloadGridData();
				}else{
					Public.tips({type: 1, content : '修改失败！'});
				}
			}
	    });
	},
	updatePrice:function(rowid,prices,price_type){
    	var rowData = $("#grid").jqGrid("getRowData", rowid);
    	var saveUrl = config.BASEPATH+'shop/price/savePrice_templist';
    	var params = {prices:prices,pd_code:rowData.spl_pd_code,price_type:price_type};
    	var modifyAll = THISPAGE.$_btnModifySamePinosPriceAll.chkVal().join() ? true : false;
    	if(!modifyAll){
    		params.spl_shop_code = rowData.spl_shop_code;
    	}
    	$.ajax({
			type:"POST",
			url:saveUrl,
			data:params,
			async:false,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data.stat == 200){
					Public.tips({type: 3, content : '修改成功'});
					if(modifyAll){
						THISPAGE.reloadGridData();
					}
				}else{
					Public.tips({type: 1, content : '修改失败！'});
				}
			}
		});
    },
	loadData:function(data){
		var pdata = {};
		pdata.id=data.od_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	save:function(){
		var sp_is_sellprice = THISPAGE.$_SellPrice.chkVal().join() ? '1' : '0';
		var sp_is_vipprice = THISPAGE.$_VipPrice.chkVal().join() ? '1' : '0';
		var sp_is_sortprice = THISPAGE.$_SortPrice.chkVal().join() ? '1' : '0';
		var sp_is_costprice = THISPAGE.$_CostPrice.chkVal().join() ? '1' : '0';
		var sp_shop_code = document.getElementById("sp_shop_code").value;
		var ids = $("#grid").jqGrid('getDataIDs');
		//判断分店信息是否为空
		if(sp_shop_code.length == 0 ){
			Public.tips({type: 2, content : '分店信息不能为空!'});
			return;
		};
		if(ids.length == 0 ){
			Public.tips({type: 2, content : '商品明细信息不存在，请补充完整！'});
			return;
		};
		if(sp_is_sellprice ==0 && sp_is_vipprice ==0 && sp_is_sortprice ==0 && sp_is_costprice ==0 ){
			Public.tips({type: 2, content : '请选择要修改哪些价格!'});
			return;
		}
		var saveUrl = config.BASEPATH+"shop/price/save";
		if($("#sp_id").val() != undefined){
			saveUrl = config.BASEPATH+"shop/price/update";
		}
		$("#btn-save").attr("disabled",true);
		$.dialog.confirm('确定要保存单据吗？', function(){
			$.ajax({
				type:"POST",
				url: saveUrl + "?" + buildUrl(),
				cache:false,
				data:"&sp_is_sellprice="+sp_is_sellprice+"&sp_is_vipprice="+sp_is_vipprice+"&sp_is_sortprice="+sp_is_sortprice+"&sp_is_costprice="+sp_is_costprice,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '保存成功!'});
						W.isRefresh = true;
						setTimeout("api.close()",500);
					}else{
						Public.tips({type: 1, content : '保存失败!'});
					}
					$("#btn-save").attr("disabled",false);
				}
			});
		});
	},
	//清除
	temp_clear:function(){
		$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"post",
				async:false,
				url:config.BASEPATH+'shop/price/temp_clear',
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						setShowPrice();
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		}
	);},
	//删除
	temp_del:function(rowId){
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'shop/price/temp_del',
					data:{"spl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }
	},
	formatPoint:function(val, options, row){
		if(val == null || $.trim(val) == ''){
			return "";
		}
		val = $.trim(val);
		var show="";
		show+="<input type='radio' class='point_yes' name='radio"+row.spl_id+"' "+(val=="1"?"checked":"")+" value='1' onClick='handle.updatePoint("+row.spl_id+",1);'/>是";
		show+="<input type='radio' class='point_no' name='radio"+row.spl_id+"' "+(val=="0"?"checked":"")+" value='0' onClick='handle.updatePoint("+row.spl_id+",0);'/>否";
		return show;
	},
	updatePoint:function(rowid,point_val){
    	var saveUrl = config.BASEPATH+'shop/price/savePoint_templist';
    	var params = {spl_id:rowid,spl_pd_point:point_val};
    	$.ajax({
			type:"POST",
			url:saveUrl,
			data:params,
			async:false,
			cache:false,
			dataType:"json",
			success:function(data){
				if(data.stat == 200){
					Public.tips({type: 3, content : '修改成功'});
				}else{
					Public.tips({type: 1, content : '修改失败！'});
				}
			}
		});
    }
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		this.selectRow={};
	},
	initDom:function(){
		this.$_SellPrice = $("#SellPrice").cssCheckbox();
		this.$_VipPrice = $("#VipPrice").cssCheckbox();
		this.$_SortPrice = $("#SortPrice").cssCheckbox();
		this.$_CostPrice = $("#CostPrice").cssCheckbox();
		this.$_btnModifySamePinosPriceAll = $("#btnModifySamePinosPriceAll").cssCheckbox();
	},
	initGrid:function(){
		var self=this;
		var colModel = [
			{label:'',name: 'spl_id',index: 'spl_id', fixed:true, title: false,sortable:false,hidden:true},
			{label:'',name: 'spl_shop_code', index: 'spl_shop_code',title: false,fixed:true,hidden:true},
			{label:'',name: 'spl_pd_code', index: 'spl_pd_code',title: false,fixed:true,hidden:true},
		    {label:'操作',name:'operate', width: 50, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'店铺名称',name: 'sp_name', index: 'sp_name', width: 100},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'是否积分',name: 'spl_pd_point', index: 'spl_pd_point',align:'center', width: 100,formatter:handle.formatPoint},
	    	{label:'原零售价',name: 'spl_old_sellprice', index: 'spl_old_sellprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell,title: false,hidden:true},
	    	{label:'现零售价',name: 'spl_new_sellprice', index: 'spl_new_sellprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell,title: false,hidden:true,editable:true},
	    	{label:'原会员价',name: 'spl_old_vipprice', index: 'spl_old_vipprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByVip,title: false,hidden:true},
	    	{label:'现会员价',name: 'spl_new_vipprice', index: 'spl_new_vipprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByVip,title: false,hidden:true,editable:true},
	    	{label:'原配送价',name: 'spl_old_sortprice', index: 'spl_old_sortprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByDistribute,title: false,hidden:true},
	    	{label:'现配送价',name: 'spl_new_sortprice', index: 'spl_new_sortprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByDistribute,title: false,hidden:true,editable:true},
	    	{label:'原成本价',name: 'spl_old_costprice', index: 'spl_old_costprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost,title: false,hidden:true},
	    	{label:'现成本价',name: 'spl_new_costprice', index: 'spl_new_costprice',width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost,title: false,hidden:true,editable:true},
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'json',
			width: _width,
			height: _height,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			cellEdit: true,
			cellsubmit: 'clientArray',//可编辑列在编辑完成后会触发保存事件，clientArray表示只保存到表格不提交到服务器
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			//scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'spl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.reloadCheckBox();
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell : function (rowid, cellname, value, iRow, iCol){
				if(cellname == "spl_new_sellprice" && self.selectRow.value!=value){
					if( value == ''  || isNaN(value) || parseFloat(value)<0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					handle.updatePrice(rowid,parseFloat(value).toFixed(2),cellname);
				} else if (cellname == "spl_new_vipprice" && self.selectRow.value!=value){
					if( value == '' || isNaN(value) || parseFloat(value)<0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					handle.updatePrice(rowid,parseFloat(value).toFixed(2),cellname);
				} else if (cellname == "spl_new_sortprice" && self.selectRow.value!=value){
					if( value == '' || isNaN(value) || parseFloat(value)<0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					handle.updatePrice(rowid,parseFloat(value).toFixed(2),cellname);
				} else if (cellname == "spl_new_costprice" && self.selectRow.value!=value){
					if( value == '' || isNaN(value) || parseFloat(value)<0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					handle.updatePrice(rowid,parseFloat(value).toFixed(2),cellname);
				} 
				return parseFloat(value).toFixed(2);
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			}
	    });
	},
	
	reloadGridData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	reloadCheckBox:function(){
		var PAP_IsSellPrice = this.$_SellPrice.chkVal().join() ? true : false;
		var PAP_IsVipPrice = this.$_VipPrice.chkVal().join() ? true : false;
		var PAP_IsSortPrice = this.$_SortPrice.chkVal().join() ? true : false;
		var PAP_IsCostPrice = this.$_CostPrice.chkVal().join() ? true : false;
		
		if($("#SortPrice").css("display") == "none"){
			PAP_IsSortPrice = false;
		}
		if($("#VipPrice").css("display") == "none"){
			PAP_IsVipPrice = false;
		}
		
		if (PAP_IsSellPrice) {
			 $("#grid").setGridParam().showCol("spl_old_sellprice");
			 $("#grid").setGridParam().showCol("spl_new_sellprice");
        } else {
       	 	$("#grid").setGridParam().hideCol("spl_old_sellprice");
       	 	$("#grid").setGridParam().hideCol("spl_new_sellprice");
        }
		if (PAP_IsVipPrice) {
			 $("#grid").setGridParam().showCol("spl_old_vipprice");
			 $("#grid").setGridParam().showCol("spl_new_vipprice");
		} else {
      	 	$("#grid").setGridParam().hideCol("spl_old_vipprice");
      	 	$("#grid").setGridParam().hideCol("spl_new_vipprice");
		}
		if (PAP_IsSortPrice) {
			 $("#grid").setGridParam().showCol("spl_old_sortprice");
			 $("#grid").setGridParam().showCol("spl_new_sortprice");
		} else {
      	 	$("#grid").setGridParam().hideCol("spl_old_sortprice");
      	 	$("#grid").setGridParam().hideCol("spl_new_sortprice");
		}
		if (PAP_IsCostPrice) {
			 $("#grid").setGridParam().showCol("spl_old_costprice");
			 $("#grid").setGridParam().showCol("spl_new_costprice");
		} else {
     	 	$("#grid").setGridParam().hideCol("spl_old_costprice");
     	 	$("#grid").setGridParam().hideCol("spl_new_costprice");
		}
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save(0);
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			exit();
		});
		//删除
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	var id = $(this).parent().data('id');
  			var row=$('#grid').getRowData(id);
  			viewProductImg(row.spl_pd_code);
        });
		//一键修改价格
        $('#btnModifyPriceAll').click(function(){
        	var UnitPrice = $("#UnitPrice").val();
        	if(UnitPrice == '' || isNaN(UnitPrice)){
        		$("#UnitPrice").val('请输入价格').select();
        		return;
        	}
        	$.dialog.confirm('确定要修改所有商品价格吗?',function(){
        		handle.updateUnitPrice('', parseFloat(UnitPrice).toFixed(2));
    		},
    		function(){
    		});
		});
		$('#SellPrice').click(function(){
			var PAP_IsSellPrice = THISPAGE.$_SellPrice.chkVal().join() ? true : false;
			if(PAP_IsSellPrice){
				$("#DiscountTd").css("display","");
				$("#UnitPriceTd").css("display","");
				$("#btnModifyPriceAllTd").css("display","");
			}else{
				$("#DiscountTd").css("display","none");
				$("#UnitPriceTd").css("display","none");
				$("#btnModifyPriceAllTd").css("display","none");
			}
			THISPAGE.reloadCheckBox();
		});
		$('#VipPrice').click(function(){
			THISPAGE.reloadCheckBox();
		});
		$('#SortPrice').click(function(){
			THISPAGE.reloadCheckBox();
		});
		$('#CostPrice').click(function(){
			THISPAGE.reloadCheckBox();
		});
		//弹出商品窗口
		$('#openProduct').click(function(){
			Utils.openProduct();
		});
	}
};

THISPAGE.init();

//退回查询页面
function exit(){
	W.isRefresh = false;
	api.close();
}
//最低折扣check
var zero = false;
function checkzero(){
	if (document.getElementById("discount").value >1 || document.getElementById("discount").value <0){
		document.getElementById("discount").value =1;
		Public.tips({type: 2, content: '你输入的折扣率必须是在0和1之间！'});
		return;
	}
    zero = true;
}