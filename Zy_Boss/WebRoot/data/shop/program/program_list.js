var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP07;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/program/list';

var Utils = {
		doQueryShop : function(){
			commonDia = $.dialog({
				title : '选择店铺',
				content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
				data : {multiselect:false},
				width : 450,
				height : 370,
				max : false,
				min : false,
				cache : false,
				lock: true,
				ok:function(){
					var selected = commonDia.content.doSelect();
					if (!selected) {
						return false;
					}
				},
				close:function(){
					var selected = commonDia.content.doSelect();
					if(selected){
						$("#sp_shop_code").val(selected.sp_code);
						$("#shop_name").val(selected.sp_name);
					}
				},
				cancel:true
			});
		}
}

var handle = {
	//修改、新增
	operate: function(oper, rowId){
		if(oper == 'add'){
			var url = config.BASEPATH+"shop/program/to_add";
			var data = {oper: oper, callback: this.callback};
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
			   	}
		   	}
		}).max();
	},
	toView:function(id){
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		var url = config.BASEPATH+"shop/program/to_view?sp_id="+id;
		var data = {callback: this.callback};
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
			   	}
		   	}
		}).max();
	},
	del: function(rowIds,sp_id,sp_imgpath){//删除
		 $.dialog.confirm('删除后无法恢复，确定删除吗?', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/program/del',
				data:{"sps_id":rowIds,"sp_id":sp_id,"sp_imgpath":sp_imgpath},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){		 		
						$('#grid').jqGrid('delRowData', rowIds);
					}else{
						Public.tips({type: 1, content : "删除失败"});
					}
				}
			});
		});
    },
	operFmatter : function(val, opt, row){
		var btnHtml = '';
        btnHtml += '<input type="button" value="查看" class="btn_sp" onclick="javascript:handle.toView(\'' + row.sp_id + '\');" />';
		return btnHtml;
	},
	sp_state : function(val, opt, row){
		if(val == '0'){
			return '未阅读';
		} else if(val == '1'){
			return '已阅读';
		} else {
			return '';
		}
	}
}
 


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom: function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satae = $('#satae');
	 	this.$_satae.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#sp_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function (){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'sp_id', label:'',index: 'sp_id', hidden:true},
		    {name: 'sps_id', label:'',index: 'sps_id', hidden:true},
		    {name: 'sp_imgpath', label:'',index: 'sp_imgpath', hidden:true},
	    	{name: 'operate', label:'操作', width: 60, fixed:true,title: false,formatter: handle.operFmatter, sortable:false},
	    	{name: 'sp_shop_name', label:'门店名称', index: 'sp_shop_name', width: 140, title: false,fixed:true},
	    	{name: 'sp_title', label:'方案标题', index: 'sp_title', width: 140, title: false},
	    	{name: 'sps_state', label:'状态', index: 'sps_state', width: 80, title: false,align:'center',formatter:handle.sp_state},
	    	{name: 'sp_us_name', label:'发布人', index: 'sp_us_name', width: 100, title: false,align:'left'},
	    	{name: 'sp_sysdate', label:'发布时间', index: 'sp_sysdate', width: 160, title: false,align:'center'},
	    	{name: 'sp_info', label:'方案说明', index: 'sp_info', width: 200, title: false,align:'left'}
	    ];
	    $("#grid").jqGrid({
	    	url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'sps_id'
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var sp_state = $("#sp_state").val();
		var sp_shop_code = $("#sp_shop_code").val();
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&sp_state='+sp_state;
		params += '&sp_shop_code='+sp_shop_code;
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#sp_shop_code").val("");
		$("#sp_stateAll").click();
		$("#sp_state").val("");
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$('#btn-del').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			if(rowId == null || rowId == ''){
				Public.tips({type: 1, content : "您未选择任何数据"});
				return;
			}
			var rowData =$("#grid").jqGrid("getRowData", rowId);
			handle.del(rowId,rowData.sp_id,rowData.sp_imgpath);
		});	 
	}
};
THISPAGE.init();