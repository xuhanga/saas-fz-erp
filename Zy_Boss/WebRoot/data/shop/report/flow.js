var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SELL19;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/report/flow';
var handle = {
	formatAVG:function(val,opt,row){
		if(null != row.sh_amount && row.sh_amount != 0){
			return parseFloat(row.sh_money/row.sh_amount).toFixed(2);
		}else{
			return 0;
		}
	},
	formatRate:function(val,opt,row){
		if(null != row.sh_count && row.sh_count != 0){
			if(null != row.da_come && row.da_come != 0){
				return parseFloat(row.sh_count/row.da_come).toFixed(2);
			}else{
				return 100.00;
			}
		}else{
			return 0.00;
		}
	},
	formatCome:function(val,opt,row){
		if(null != val && '' != val){
			return val;
		}else{
			return 0;
		}
	},
	formatReceive:function(val,opt,row){
		if(null != val && '' != val){
			return val;
		}else{
			return 0;
		}
	},
	formatTry:function(val,opt,row){
		if(null != val && '' != val){
			return val;
		}else{
			return 0;
		}
	},
	formatTryRate:function(val,opt,row){
		var come = row.da_come;
		var trys = row.da_try;
		var rate = 0.00;
		if(null != come && 0 != come){
			if(null != trys && 0 != trys){
				rate = parseFloat(come/trys*100).toFixed(2);
			}
		}else{
			if(null != trys && 0 != trys){
				rate = 100.00;
			}
		}
		return rate;
	}
}
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:true},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#sh_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			close:function(){
				if(commonDia.content.dblClick){
					var selected = commonDia.content.doSelect();
					var sp_code = [];
					var sp_name = [];
					for(var i=0;i<selected.length;i++){
						sp_code.push(selected[i].sp_code);
						sp_name.push(selected[i].sp_name);
					}
					$("#sh_shop_code").val(sp_code.join(","));
					$("#shop_name").val(sp_name.join(","));
				}
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
		this.initChart();
	},
	bulidData:function(){
		var ids = $("#grid").getDataIDs();
		var pdata = {};
		var comeArr = [],recesArr = [], tryArr = [],numberArr = [];
		for(var j = 0;j < 24; j++){
			var come = 0,reces = 0, trys = 0,number = 0;
			for (var i = 0; i < ids.length; i++) {
				var grid = $("#grid").jqGrid("getRowData", ids[i]);
				if(parseInt(grid.da_date) == j){
					if(null != grid.da_come && "" != grid.da_come){
						come = grid.da_come;
					}
					if(null != grid.da_receive && "" != grid.da_receive){
						reces = grid.da_receive;
					}
					if(null != grid.da_try && "" != grid.da_try){
						trys = grid.da_try;
					}
					if(null != grid.sh_count && "" != grid.sh_count){
						number = grid.sh_count;
					}
				}
			}
			comeArr.push(parseInt(come));
			recesArr.push(parseInt(reces));
			tryArr.push(parseInt(trys));
			numberArr.push(parseInt(number));
		}
		pdata.come=comeArr;
		pdata.reces=recesArr;
		pdata.trys=tryArr;
		pdata.number=numberArr;
		return pdata;
	},
	initChart:function(){
		var hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		var pdata = THISPAGE.bulidData();
		var chart = new Highcharts.Chart('container', {
		    title: {
		        text: '门店流量分析',
		        x: -20
		    },
		    xAxis: {
		        categories: hours
		    },
		    yAxis: {
		        title: {
		            text: '数量'
		        },
		        plotLines: [{
		            value: 0,
		            width: 1,
		            color: '#808080'
		        }]
		    },
		    legend: {
		        layout: 'vertical',
		        align: 'right',
		        verticalAlign: 'middle',
		        borderWidth: 0
		    },
		    series: [{
		    	type: 'spline',
		        name: '进店',
		        data: pdata.come
		    }, {
		    	 type: 'spline',
		        name: '接待',
		        data: pdata.reces
		    }, {
		    	type: 'spline',
		        name: '试穿',
		        data: pdata.trys
		    }, {
		    	type: 'column',
		        name: '单数',
		        data: pdata.number
		    }]
		});
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.initType("data");
	},
	initType:function(type){
		$("#tab_type").children("a").each(function(){
			$(this).removeClass("on");
		});
		$("#btn_"+type).addClass("on")
		if(type == "data"){
			$("#container").hide();
			$(".grid-wrap").show();
		}else{
			$("#container").show();
			$(".grid-wrap").hide();
			THISPAGE.initChart();
		}
		$("#type").val(type);
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'时段',name: 'da_date',index:'da_date',width:60, align:'left'},
			{label:'进店数',name: 'da_come',index:'da_come',width:70,align:'right',formatter:handle.formatCome},
			{label:'接待数',name: 'da_receive',index:'da_receive',width:70,align:'right',formatter:handle.formatReceive},
			{label:'试穿数',name: 'da_try',index:'da_try',width:70,align:'right',formatter:handle.formatTry},
			{label:'试穿率',name: 'try_rate',index:'try_rate',width:70,align:'right',sortable:false,formatter:handle.formatTryRate},
			{label:'销售单数',name: 'sh_count',index:'sh_count',width:70,align:'right',sortable:false},
			{label:'成交率',name: 'sh_rate',index:'sh_rate',width:70,align:'right',sortable:false,formatter:handle.formatRate},
			{label:'平均价',name: 'shl_avg_price',index:'shl_avg_price',width:70,align:'right',formatter:handle.formatAVG},
			{label:'销售数量',name: 'sh_amount',index:'sh_amount',width:70,align:'right'},
			{label:'销售金额',name: 'sh_money',index:'sh_money',width:70,align:'right',formatter:PriceLimit.formatBySell}
	    ];
		$('#grid').jqGrid({
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-50,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			page:1,
			pgbuttons:true,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
				repeatitems : false,
				id: 'da_id'  //图标ID
			},
			loadComplete: function(data){
				var type = $("#type").val();
				if(type == "chart"){
					THISPAGE.initChart();
				}
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	initParam:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&sh_shop_code='+$("#sh_shop_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#sh_shop_code").val("");
		THISPAGE.$_date.setValue(0);
	},
	reloadData:function(){
		var param=THISPAGE.initParam();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_data').on('click', function(e){
			e.preventDefault();
			THISPAGE.initType("data");
		});
		$('#btn_chart').on('click', function(e){
			e.preventDefault();
			var ids = $("#grid").getDataIDs();
			if(ids == null || ids.length == 0){
				Public.tips({type: 2, content : "请查询数据！"});
				return;
			}
			THISPAGE.initType("chart");
		});
		
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();
