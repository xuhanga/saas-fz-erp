var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP16;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/sale/list';
var DIALOG = {
	commonDialog:function(id,url){
		$.dialog({ 
		   	id:id,
		   	title:false,
		   	max: false,
		   	min: false,
		   	cache : false,
		   	fixed:false,
		   	drag:false,
		   	resize:false,
		   	lock:false,
		   	content:'url:'+url
	    }).max();
	}
};
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#sss_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryMemberType : function(){
		commonDia = $.dialog({
			title : '会员类别',
			content : 'url:'+config.BASEPATH+'vip/membertype/to_list_dialog_sale',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ss_mt_code").val(selected.mt_code);
					$("#mt_name").val(selected.mt_name);
				}
			},
			cancel:true
		});
	}
};


var handle = {
	//修改、新增
	operate: function(oper, id){
		var url="";
		if(oper == 'add'){ 
			url = config.BASEPATH+"shop/sale/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"shop/sale/to_view?ss_code="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'stop'){
			url = config.BASEPATH+"shop/sale/to_view?ss_code="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'view'){
			url = config.BASEPATH+"shop/sale/to_view?ss_code="+id;
			data = {oper: oper,callback: this.callback};
		}
		$.dialog({
			title : false,
			max : false,
			min : false,
			cache : false,
			lock:false,
			content:'url:'+url,
			data: data,
			close:function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
			   	}
		   	}
		   	}).max();
	},
	del: function(rowId){
		var rowData = $("#grid").jqGrid("getRowData", rowId);
	 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/sale/del',
				data:{"ss_code":rowData.ss_code},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowId);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
    },
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.ss_state == '0') {
            btnHtml += '<input type="button" value="审核" class="btn_sp" onclick="javascript:handle.operate(\'approve\',\'' + row.ss_code + '\');" />';
        }else if (row.ss_state == '1') {//审核通过
        	btnHtml += '<input type="button" value="终止" class="btn_wx" onclick="javascript:handle.operate(\'stop\',\'' + row.ss_code + '\');" />';
        }else{//
        	btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',\'' + row.ss_code + '\');" />';
        }
		btnHtml += '<input type="button" value="流程" class="btn_qh" onclick="javascript:listBillProcess(\''+row.ss_code+'\',\'t_shop_sale\');" />';
		return btnHtml;
	},
	ss_priority : function(val, opt, row){
		if(val == '1'){
			return '最高';
		} else if(val == '2'){
			return '较高';
		} else if(val == '3'){
			return '一般';
		} else if(val == '4'){
			return '较低';
		} else if(val == '5'){
			return '最低';
		} else {
			return '';
		}
	},
	ss_state : function(val, opt, row){
		if(val == '0'){
			return '未审批';
		} else if(val == '1'){
			return '已通过';
		} else if(val == '2'){
			return '已退回';
		} else if(val == '4'){
			return '已终止';
		} else {
			return '';
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satae = $('#satae');
	 	this.$_satae.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#ss_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{name: 'operate', width: 100,label:'操作', fixed:true,formatter: handle.operFmatter, title: false,sortable:false},
			{name: 'ss_code', index: 'ss_code',label:'方案编号',width: 140, title: false, fixed: true, frozen:true},
			{name: 'ss_name', index: 'ss_name',label:'方案名称', width: 120, title: false, fixed: true, frozen:true},
			{name: 'ss_sp_name', index: 'ss_sp_name',label:'促销店铺', width: 100, title: false, fixed: true},
			{name: 'ss_priority', index: 'ss_priority',label:'方案级别', width: 100, title: false, fixed: true, formatter:handle.ss_priority},
			{name: 'ss_begin_date', index: 'ss_begin_date',label:'开始日期', width: 100, title: false, fixed: true},
			{name: 'ss_end_date', index: 'ss_end_date',label:'结束日期', width: 100, title: false, fixed: true},
			{name: 'ss_manager', index: 'ss_manager',label:'操作员', width: 100, title: false, fixed: true},
			{name: 'ss_sysdate', index: 'ss_sysdate',label:'操作日期', width: 100, title: false, fixed: true},
			{name: 'ss_state', index: 'ss_state',label:'状态', width: 100, title: false, fixed: true, formatter:handle.ss_state}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-20,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'ss_id'
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新!'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var ss_code = $("#ss_code").val();
		var sss_shop_code = $("#sss_shop_code").val();
		var ss_mt_code = $("#ss_mt_code").val();
		var ss_state = $("#ss_state").val();
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&ss_code='+ss_code;
		params += '&sss_shop_code='+sss_shop_code;
		params += '&ss_mt_code='+ss_mt_code;
		params += '&ss_state='+ss_state;
		return params;
	},
	reset:function(){
		$("#ss_code").val("");
		$("#shop_name").val("");
		$("#sss_shop_code").val("");
		$("#mt_name").val("");
		$("#ss_mt_code").val("");
		$("#ss_state_all").click();
		$("#ss_state").val("");
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
				return ;
			}
			handle.operate('add');
		});
		//删除
		$('#btn-del').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
				return ;
			}
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			if(rowId == null || rowId == ''){
				Public.tips({type: 1, content : "您未选择任何数据"});
				return;
			}
			var rowData =$("#grid").jqGrid("getRowData", rowId);
			if(rowData.ss_state == "已通过"){
				Public.tips({type: 1, content : "单据已审核，不能删除"});
				return;
			}
			handle.del(rowId)
		});
	}
}
THISPAGE.init();

function childrenFlush(){
	THISPAGE.reloadData();
}