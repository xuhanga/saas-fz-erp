var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.SHOP17;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/sale/totalList';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var bd_code = [];
					var bd_name = [];
					for(var i=0;i<selected.length;i++){
						bd_code.push(selected[i].bd_code);
						bd_name.push(selected[i].bd_name);
					}
					$("#bd_code").val(bd_code.join(","));
					$("#bd_name").val(bd_name.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择商品类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var pd_tp_code = [];
					var pd_tp_name = [];
					var pd_tp_upcode = [];
					for(var i=0;i<selected.length;i++){
						pd_tp_code.push(selected[i].tp_code);
						pd_tp_name.push(selected[i].tp_name);
						pd_tp_upcode.push(selected[i].tp_upcode);
					}
					$("#tp_code").val(pd_tp_code.join(","));
					$("#tp_name").val(pd_tp_name.join(","));
					$("#tp_upcode").val(pd_tp_upcode.join(","));
				}
			},
			cancel:true
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	dateRedioClick("theDate");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
		    
			{label:'店铺名称', name: 'shop_name', index: 'shop_name', width :120, title: false ,fixed: true},
			{label:'数量', name: 'shl_amount', index: 'shl_amount', width :80, title: false ,fixed: true, align:'right'},
			{label:'零售价', name: 'shl_sell_price', index: 'shl_sell_price', width :100, title: false ,fixed: true, align:'right'},
			{label:'零售金额', name: 'shl_sell_money', index: 'shl_sell_money', width :100, title: false ,fixed: true, align:'right'},
			{label:'折扣价', name: 'shl_price', index: 'shl_price', width :100, title: false ,fixed: true, align:'right'},
			{label:'折扣金额', name: 'shl_money', index: 'shl_money', width :100, title: false ,fixed: true, align:'right'},
			{label:'成本', name: 'shl_cost_money', index: 'shl_cost_money', width :100, title: false ,fixed: true, align:'right'},
			{label:'利润', name: 'shl_profits', index: 'shl_profits', width :100, title: false ,fixed: true, align:'right'},
			{label:'让利金额', name: 'shl_discountmonery', index: 'shl_discountmonery', width :100, title: false ,fixed: true, align:'right'}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'local',
			width: gridWH.w-12,
			height: gridWH.h-34,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:false,
			pgtext:false,
			rowNum:1000,//每页条数
			/*rowList:config.BASEROWLIST,*///分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
                userdata: 'data.data',
				repeatitems : false,
				id:'ss_id'
			},
			loadComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
				Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新!'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
			}
	    });
	},
	buildParams:function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&shop_code='+$("#shop_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&pd_no='+Public.encodeURI($("#pd_no").val());
		params += '&pd_name='+Public.encodeURI($("#pd_name").val());
		return params;
	},
	reset:function(){
		$("#pd_name").val("");
		$("#pd_no").val("");
		$("#bd_code").val("");
		$("#bd_name").val("");
		$("#tp_code").val("");
		$("#tp_name").val("");
		$("#tp_upcode").val("");
		$("#shop_code").val("");
		$("#shop_name").val("");
		$("#theDate").click();
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();