var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;

var handle = {
	approve:function(){
		commonDia = $.dialog({
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "code="+$("#ss_code").val();
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
			type: "POST",
	        url: config.BASEPATH+"shop/sale/approve",
	        data: params,
	        cache:false,
			dataType:"json",
	        success: function (data) {
	        	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : data.message});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
	   		}
	    });
	},
		stop:function(){
			commonDia = $.dialog({ 
			   	id:'approve_stop',
			   	title:'终止促销方案',
			   	data:{},
			   	max: false,
			   	min: false,
			   	lock:true,
			   	width:300,
			   	height:200,
			   	drag: true,
			   	resize:false,
			   	content:'url:'+config.BASEPATH+'approve/to_stop',
			   	fixed:false,
			   	close : function(){
			   		var ar_infos = commonDia.content.ar_infos;
			   		if(ar_infos != undefined){
			   			handle.doStop(ar_infos);
			   		}
			   	}
		    });
		},
		doStop:function(ar_infos){
			$("#btn-stop").attr("disabled",true);
			var params = "";
			params += "code="+$("#ss_code").val();
			params += "&ec_stop_cause="+Public.encodeURI(ar_infos.ar_describe);
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"shop/sale/stop",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : data.message});
						W.isRefresh = true;
						setTimeout("api.close()",500);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-stop").attr("disabled",false);
	            }
	        });
		}
	};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
		var ss_state = $("#ss_state").val();
		if(ss_state == 1){//审核通过、未终止
			$("#btn-stop").show();
		}
	},
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-stop').on('click', function(e){
			e.preventDefault();
			handle.stop();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();

function doShowProduct(code,index){
	$(".comm-"+code).each(function(){
		$(this).hide();
	});
	$(".product-"+code+"-"+index).show();
}
function goNext(){
	document.getElementById("sales1").style.display="none";
	document.getElementById("sales2").style.display="";
}
function goUp(){
	document.getElementById("sales1").style.display="";
	document.getElementById("sales2").style.display="none";
}