var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var Utils = {
	doQueryShop : function(){
		var shopCodeBefore = $("#sp_shop_code").val();
		commonDia = W.$.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 550,
			height : 390,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ss_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = W.$.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ss_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
}
var handle = {
	save:function(){
		var ss_tel = $("#ss_tel").val().trim();
		if(ss_tel == ''){
	    	W.Public.tips({type: 2, content : "请输入手机号码！"});
	        $("#ss_tel").select().focus();
	        return;
	    }else{
	    	$("#ss_tel").val(ss_tel);//防止空格
	    }
		
		var regu =/^[1-9][0-9]{10}$/; 
		if (!regu.test(ss_tel)){ 
			W.Public.tips({type: 2, content : "请输入正确的手机号码!"});
			$("#ss_tel").val("");
		    setTimeout("api.zindex()",400);
		    return; 
		}
		
		var ss_name = $("#ss_name").val().trim();
	    if(ss_name == ''){
	    	W.Public.tips({type: 2, content : "请输入顾客姓名!"});
	        $("#ss_name").select().focus();
	        return;
	    }else{
	    	$("#ss_name").val(ss_name);//防止空格
	    }
	    var ss_manager = $("#ss_manager").val();
	    if(ss_manager == ''){
	    	W.Public.tips({type: 2, content : "请选择受理人员!"});
	        $("#ss_manager").select().focus();
	        return;
	    }
	    var ss_pd_no = $("#ss_pd_no").val().trim();
	    if(ss_pd_no == ''){
	    	W.Public.tips({type: 2, content : "请输入商品货号!"});
	        $("#ss_pd_no").select().focus();
	        return;
	    }else{
	    	$("#ss_pd_no").val(ss_pd_no);//防止空格
	    }
	    var ss_shop_code = $("#ss_shop_code").val();
	    if(ss_shop_code == ''){
	    	W.Public.tips({type: 2, content : "请选择维修店铺!"});
	        $("#shop_name").select().focus();
	        return;
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"shop/service/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					setTimeout("api.zindex()",500);
					W.isRefresh = true;
					api.close();
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	checkMobile:function(obj){ //验证手机号码
	    var s = obj.value;
		var regu =/^[1-9][0-9]{10}$/; 
		if (!regu.test(s)){ 
			W.Public.tips({type: 1, content : "请输入正确的手机号码!"});
		    obj.value="";
		    setTimeout("api.zindex()",400);
		    return false; 
		}
    }
};
var THISPAGE = {
	init:function (){
		this.serviceItemCombo;
		/*this.storeCombo,*/
		this.initServiceItem();
		/*this.initShop();*/
		this.initDate();
		this.initDom();
		this.initEvent();
	},
	initServiceItem:function(){
		var seasonInfo = {};
		THISPAGE.serviceItemCombo = $('#span_ss_ssi_id').combo({
			data:config.BASEPATH +"shop/service/list_serviceitem_combo",
			value: 'ssi_id',
			text: 'ssi_name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 0,
			editable: true,
			ajaxOptions:{
				formatData: function(data){
					seasonInfo = data.data.items;
					return data.data.items;
				}
			},
			callback:{
				onChange: function(data){
					$("#ss_ssi_id").val(data.ssi_id);
					$("#ssi_name").val(data.ssi_name);
				}
			}
		}).getCombo(); 
	},
	/*initShop:function(){
		var seasonInfo = {};
		THISPAGE.storeCombo = $('#span_em_shop_Code').combo({
			data:config.BASEPATH +"base/shop/listByEmp",
			value: 'em_shop_code',
			text: 'em_shop_name',
			width :161,
			height: 30,
			listId:'',
			defaultSelected: 0,
			editable: true,
			ajaxOptions:{
				formatData: function(data){
					seasonInfo = data.data.items;
					return data.data.items;
				}
			},
			callback:{
				onChange: function(data){
					$("#em_shop_code").val(data.em_shop_code);
					$("#em_shop_name").val(data.em_shop_name);
				}
			}
		}).getCombo(); 
	},*/
	initDate:function(){
		var mydate = new Date();
		var currentdate = mydate.getFullYear() + "-" + (mydate.getMonth()+1) + "-" + mydate.getDate(); 
		document.getElementById('ss_startdate').value = currentdate;
	},
	initDom:function(){
		$("#ss_tel").val("").focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();