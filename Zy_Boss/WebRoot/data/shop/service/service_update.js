var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	countPoint:function(obj,countId){
		var haveBean = obj.value.length;//已用字数
		document.getElementById(countId).value = 500 - haveBean;
		if(haveBean>500){
			W.Public.tips({type: 2, content : "输入字数不能超过500！"});
			obj.value = obj.value.substr(0,500);
		}
	},
	update:function(state,ss_state,ss_id){
		var ask="";
		if(state=="2"){
			ask="确认受理?";
		}
		if(state=="0"){
			ask="确认维修?";
		}
		if(state=="1"){
			ask="确认取货?";
		}
		$.dialog.confirm(ask, function(){
			var url="";
			if(state=="2"){
				url="?ss_id="+ss_id+"&ss_state="+ss_state;
			}else if(state=="0"){
				var ss_result = document.getElementById("ss_result").value;
				url="?ss_result="+ss_result+"&ss_id="+ss_id+"&ss_state="+ss_state;
			}else{
				if(ss_state=="1"){
					//var ss_satis = document.getElementById("ss_satis").value;
					var ss_satis="";
					url="?ss_satis="+ss_satis+"&ss_id="+ss_id+"&ss_state="+ss_state;
				}else{
					W.Public.tips({type: 2, content : "请确认已经维修！"});
					return;
				}
			}
			url = encodeURI(encodeURI(url)); 
			$("#btn-save").attr("disabled",true);
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"shop/service/update"+url,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.Public.tips({type: 3, content : data.message});
						setTimeout("api.zindex()",500);
						W.isRefresh = true;
						api.close();
					}else{
						W.Public.tips({type: 1, content : data.message});
					}
					$("#btn-save").attr("disabled",false);
				}
			});
		}, function(){
		   return;
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
	},
	initEvent:function(){
		$("#btn_close").click(function(){
			W.isRefresh = false;
			api.close();
		});
	}
};
THISPAGE.init();