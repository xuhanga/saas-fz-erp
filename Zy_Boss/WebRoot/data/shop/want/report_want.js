var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.SHOP11;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'shop/want/pageWantReport';
var _height = $(parent).height()-298,_width = $(parent).width()-192;

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择要货门店',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:true,module:'want'},
			width : 460,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var sp_code = [];
				var sp_name = [];
				for(var i=0;i<selected.length;i++){
					sp_code.push(selected[i].sp_code);
					sp_name.push(selected[i].sp_name);
				}
				$("#wt_shop_code").val(sp_code.join(","));
				$("#shop_name").val(sp_name.join(","));
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#wt_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#wt_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(code,name){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#"+code).val(selected.dp_code);
				$("#"+name).val(selected.dp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#"+code).val(selected.dp_code);
					$("#"+name).val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].bd_code);
					names.push(selected[i].bd_name);
				}
				$("#bd_code").val(codes.join(","));
				$("#bd_name").val(names.join(","));
			},
			close:function(){
			},
			cancel:true
		});
	},
	doQueryType : function(){
		commonDia = $.dialog({
			title : '选择类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 300,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].tp_code);
					names.push(selected[i].tp_name);
				}
				$("#tp_code").val(codes.join(","));
				$("#tp_name").val(names.join(","));
			},
			close:function(){
			},
			cancel:true
		});
	},
	doQueryProduct : function(){
		commonDia = $.dialog({
			title : '选择商品',
			content : 'url:'+config.BASEPATH+'base/product/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#pd_code").val(selected.pd_code);
				$("#pd_name").val(selected.pd_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#pd_code").val(selected.pd_code);
					$("#pd_name").val(selected.pd_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	viewDetail: function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		var params = {};
		params.wt_type = $("#wt_type").val();
		params.wt_ar_state = $("#wt_ar_state").val();
		params.begindate = $("#begindate").val();
		params.enddate = $("#enddate").val();
		params.shop_name = $("#shop_name").val();
		params.wt_shop_code = $("#wt_shop_code").val();
		params.outdepot_name = $("#outdepot_name").val();
		params.wt_outdp_code = $("#wt_outdp_code").val();
		params.indepot_name = $("#indepot_name").val();
		params.wt_indp_code = $("#wt_indp_code").val();
		params.wt_manager = $("#wt_manager").val();
		params.bd_name = $("#bd_name").val();
		params.bd_code = $("#bd_code").val();
		params.tp_name = $("#tp_name").val();
		params.tp_code = $("#tp_code").val();
		params.pd_season = $("#pd_season").val();
		params.pd_year = $("#pd_year").val();
		params.pd_code = $("#pd_code").val();
		params.pd_name = $("#pd_name").val();
		var type = $("#type").val();
		switch(type){
			case 'brand':
				params.bd_code = rowData.code;
				params.bd_name = rowData.name;
				break ;
			case 'type':
				params.tp_code = rowData.code;
				params.tp_name = rowData.name;
				break ;
			case 'shop':
				params.wt_shop_code = rowData.code;
				params.shop_name = rowData.name;
				break ;
			case 'product' :
				params.pd_code = rowData.code;
				params.pd_name = rowData.name;
				break;
			case 'season' :
				params.pd_season = rowData.name;
				break;
			case 'depot' :
				params.outdepot_name = rowData.name;
				params.wt_outdp_code = rowData.code;
				params.indepot_name = rowData.indp_name;
				params.wt_indp_code = rowData.indp_code;
				break;
			case 'manager' : 
				params.wt_manager = rowData.name;
				break;
			default :
				break ;
		}
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+"shop/want/to_report_want_detail",
			data: params,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	operFmatter : function(val, opt, row){
		if(val == "合计"){
			return val;
		}
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-list" title="明细">&#xe608;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theDate");
		this.$_wt_type = $("#td_wt_type").cssRadio({ callback: function($_obj){
			$("#wt_type").val($_obj.find("input").val());
		}});
		this.$_state = $("#td_state").cssRadio({ callback: function($_obj){
			$("#wt_ar_state").val($_obj.find("input").val());
		}});
		this.$_type = $("#td_type").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			$("#type").val(type);
			$('#grid').showCol("code");
			$('#grid').hideCol("pd_no");
			$('#grid').hideCol("indp_name");
			switch(type){
				case 'brand' : 
					$('#grid').setLabel("code","品牌编号");
					$('#grid').setLabel("name","品牌名称");
					break ;
				case 'type' : 
					$('#grid').setLabel("code","类别编号");
					$('#grid').setLabel("name","类别名称");
					break ;
				case 'shop' : 
					$('#grid').setLabel("code","店铺编号");
					$('#grid').setLabel("name","店铺名称");
					break ;
				case 'product' :
					$('#grid').setLabel("code","商品货号");
					$('#grid').setLabel("name","商品名称");
					$('#grid').hideCol("code");
					$('#grid').showCol("pd_no");
					break;
				case 'season' :
					$('#grid').setLabel("code","季节编号");
					$('#grid').setLabel("name","季节名称");
					$('#grid').hideCol("code");
					break;
				case 'depot' :
					$('#grid').setLabel("code","发货仓库编号");
					$('#grid').setLabel("name","发货仓库");
					$('#grid').hideCol("code");
					$('#grid').showCol("indp_name");
					break;
				case 'manager' : 
					$('#grid').setLabel("code","经办人员");
					$('#grid').setLabel("name","经办人员");
					$('#grid').hideCol("code");
					break;
				default :
					break ;
			}	
			THISPAGE.reloadData();
		}});
		this.initSeason();
		this.initYear();
	},
	initSeason:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"base/dict/list?dtl_upcode=KEY_SEASON",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					var seasons = data.data;
					seasons.unshift({dtl_code:"",dtl_name:"全部"});
					$('#span_pd_season').combo({
						value : 'dtl_code',
						text : 'dtl_name',
						width : 207,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								if(data.dtl_code != ""){
									$("#pd_season").val(data.dtl_name);
								}else{
									$("#pd_season").val("");
								}
							}
						}
					}).getCombo().loadData(seasons,["dtl_name",$("#pd_season").val(),0]);
				}
			}
		 });
	},
	initYear:function(){
		var currentYear = new Date().getFullYear();
		var data = [
		    {Code:"",Name:"全部"},
			{Code:""+parseInt(currentYear+1)+"",Name:""+parseInt(currentYear+1)+""},
			{Code:""+parseInt(currentYear)+"",Name:""+parseInt(currentYear)+""},
			{Code:""+parseInt(currentYear-1)+"",Name:""+parseInt(currentYear-1)+""},
			{Code:""+parseInt(currentYear-2)+"",Name:""+parseInt(currentYear-2)+""},
			{Code:""+parseInt(currentYear-3)+"",Name:""+parseInt(currentYear-3)+""},
			{Code:""+parseInt(currentYear-4)+"",Name:""+parseInt(currentYear-4)+""},
			{Code:""+parseInt(currentYear-5)+"",Name:""+parseInt(currentYear-5)+""}
		];
		$('#span_pd_year').combo({
			data:data,
			value: 'Code',
			text: 'Name',
			width :206,
			height:80,
			listId:'',
			defaultSelected: 0,
			editable: false,
			callback:{
				onChange: function(data){
					$("#pd_year").val(data.Code);
				}
			}
		}).getCombo();
	},
	initGrid:function(){
		var colModel = [
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'店铺编号',name: 'code',index:'code',width:100},
	    	{label:'商品货号',name: 'pd_no',index:'pd_no',width:100,hidden:true},
	    	{label:'店铺名称',name: 'name',index:'name',width:100},
	    	{label:'收货仓库编号',name: 'indp_code',index:'indp_code',width:100,hidden:true},
	    	{label:'收货仓库',name: 'indp_name',index:'indp_name',width:100,hidden:true},
        	{label:'补货申请',name: 'apply_amount',index:'apply_amount',width:80, align:'right'},
        	{label:'补货实发',name: 'send_amount',index:'send_amount',width:80, align:'right'},
        	{label:'补货申请金额',name: 'apply_money',index:'apply_money',width:100, align:'right',formatter: PriceLimit.formatMoney},
        	{label:'补货实发金额',name: 'send_money',index:'send_money',width:100, align:'right',formatter: PriceLimit.formatMoney},
        	{label:'补货实发成本',name: 'send_cost',index:'send_cost',width:100, align:'right',formatter: PriceLimit.formatMoney},
        	{label:'退货申请',name: 'apply_amount_th',index:'apply_amount_th',width:80, align:'right'},
        	{label:'退货实发',name: 'send_amount_th',index:'send_amount_th',width:80, align:'right'},
        	{label:'退货申请金额',name: 'apply_money_th',index:'apply_money_th',width:100, align:'right',formatter: PriceLimit.formatMoney},
        	{label:'退货实发金额',name: 'send_money_th',index:'send_money_th',width:100, align:'right',formatter: PriceLimit.formatMoney},
        	{label:'退货实发成本',name: 'send_cost_th',index:'send_cost_th',width:100, align:'right',formatter: PriceLimit.formatMoney}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'local',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				var footer = $('#grid').footerData()
				footer.operate = "合计";
				footer.money_proportion = "合计";
		    	$('#grid').footerData('set',footer);
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'type='+$("#type").val();
		params += '&wt_type='+$("#wt_type").val();
		params += '&wt_ar_state='+$("#wt_ar_state").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&wt_shop_code='+$("#wt_shop_code").val();
		params += '&wt_outdp_code='+$("#wt_outdp_code").val();
		params += '&wt_indp_code='+$("#wt_indp_code").val();
		params += '&bd_code='+$("#bd_code").val();
		params += '&tp_code='+$("#tp_code").val();
		params += '&pd_season='+$("#pd_season").val();
		params += '&pd_year='+$("#pd_year").val();
		params += '&wt_manager='+Public.encodeURI($("#wt_manager").val());
		params += '&pd_code='+$("#pd_code").val();
		return params;
	},
	reset:function(){
		$("#pd_code").val("");
		$("#pd_name").val("");
		$("#wt_manager").val("");
		$("#shop_name").val("");
		$("#wt_shop_code").val("");
		$("#outdepot_name").val("");
		$("#wt_outdp_code").val("");
		$("#indepot_name").val("");
		$("#wt_indp_code").val("");
		$("#bd_name").val("");
		$("#bd_code").val("");
		$("#tp_name").val("");
		$("#tp_code").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_wt_type.setValue(0);
		THISPAGE.$_state.setValue(0);
		$("#span_pd_season").getCombo().selectByValue("");
		$("#span_pd_year").getCombo().selectByValue("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//收银明细
		$('#grid').on('click', '.operating .ui-icon-list', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			var id = $(this).parent().data('id');
			handle.viewDetail(id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();