var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'sort/fee/temp_list';
var _height = $(parent).height()-243,_width = $(parent).width()-2;

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'allot'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#fe_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#fe_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#fe_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	doQueryProperty : function(){
		commonDia = $.dialog({
			title : '选择费用类型',
			content : 'url:'+config.BASEPATH+'money/property/to_list_dialog/0',
			data : {multiselect:true},
			width : 350,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var temps = [];
				for(var i=0;i<selected.length;i++){
					var temp = {};
					temp.fel_mp_code = selected[i].pp_code;
					temp.fel_money = 0;
					temps.push(temp);
				}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sort/fee/temp_save',
					data:{"temps":JSON.stringify(temps)},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '保存成功'});
							THISPAGE.reloadGridData();
							commonDia.close();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
				return false;
			},
			close:function(){
				
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemark:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'sort/fee/temp_updateRemark',
			data:{fel_id:rowid,fel_remark:Public.encodeURI(remark)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateMoney:function(rowid,money){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'sort/fee/temp_updateMoney',
			data:{fel_id:rowid,fel_money:money},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'sort/fee/temp_del',
					data:{"fel_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							var ids = $("#grid").getDataIDs();
							if(ids.length == 0){
								THISPAGE.addEmptyData();
							}
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'sort/fee/temp_clear',
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						THISPAGE.addEmptyData();
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择费用类型！'});
			return;
		}
		$("#btn-save").attr("disabled",true);
		var saveUrl = config.BASEPATH+"sort/fee/save";
		if($("#fe_id").val() != undefined){
			saveUrl = config.BASEPATH+"sort/fee/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.fe_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-plus" title="新增">&#xe639;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#fe_date").val(config.TODAY);
		this.selectRow={};
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var fel_money=grid.getCol('fel_money',false,'sum');
    	grid.footerData('set',{fel_mp_code:'合计：',fel_money:fel_money.toFixed(2)});
    	$("#fe_money").val(fel_money.toFixed(2));
    },
    addEmptyData:function(){
    	var emptyData = {fel_mp_code:'',mp_name:'',fel_money:'',fel_remark:''};
    	$("#grid").addRowData(0, emptyData);
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'编号',name: 'fel_mp_code', index: 'fel_mp_code', width: 100},
	    	{label:'费用名称',name: 'mp_name', index: 'mp_name', width: 100},
	    	{label:'金额',name: 'fel_money', index: 'fel_money', width: 80,align:'right',sorttype: 'float',editable:true},
	    	{label:'备注',name: 'fel_remark', index: 'fel_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'fel_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				var ids = $("#grid").getDataIDs();
				if(ids.length == 0){
					THISPAGE.addEmptyData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(rowid == 0){
					return self.selectRow.value;
				}
				if(cellname == 'fel_money'  && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) == 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					handle.temp_updateMoney(rowid, parseFloat(value).toFixed(2));
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'fel_remark' && self.selectRow.value != value){
					handle.temp_updateRemark(rowid, value);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'fel_money' && self.selectRow.value != value){
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        $('#grid').on('click', '.operating .ui-icon-plus', function (e) {
        	e.preventDefault();
        	Utils.doQueryProperty();
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
	}
};

THISPAGE.init();