var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'allot'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#pp_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
				$("#sp_prepay").val(selected.sp_prepay);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#pp_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
					$("#sp_prepay").val(selected.sp_prepay);
				}
			},
			cancel:true
		});
	},
	doQueryBank : function(){
		commonDia = $.dialog({
			title : '选择银行账户',
			content : 'url:'+config.BASEPATH+'money/bank/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#pp_ba_code").val(selected.ba_code);
				$("#ba_name").val(selected.ba_name);
				$("#ba_balance").val(selected.ba_balance);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#pp_ba_code").val(selected.ba_code);
					$("#ba_name").val(selected.ba_name);
					$("#ba_balance").val(selected.ba_balance);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#pp_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		var saveUrl = config.BASEPATH+"sort/prepay/save";
		if($("#pp_id").val() != undefined){
			saveUrl = config.BASEPATH+"sort/prepay/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.pp_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#pp_date").val(config.TODAY);
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();