var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var queryurl = config.BASEPATH+'stock/allocate/page';
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var Utils = {
	doQueryDepot : function(code,name){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'stock'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#"+code).val(selected.dp_code);
					$("#"+name).val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ac_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var dblClick = false;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var colModel = [
			{name: 'ac_outdp_code',label:'',index: 'ac_outdp_code',hidden:true},
			{name: 'ac_indp_code',label:'',index: 'ac_indp_code',hidden:true},
	    	{name: 'ac_number',label:'单据编号',index: 'ac_number',width:150},
	    	{name: 'ac_date',label:'调拨日期',index: 'ac_date',width:80},
	    	{name: 'outdepot_name',label:'发货仓库',index: 'ac_outdp_code',width:120},
	    	{name: 'indepot_name',label:'收货仓库',index: 'ac_indp_code',width:120},
	    	{name: 'ac_manager',label:'经办人',index: 'ac_manager',width:100},
	    	{name: 'ac_amount',label:'总计数量',index: 'ac_amount',width:80,align:'right'},
	    	{name: 'ac_money',label:'金额',index: 'ac_money',width:80,align:'right',formatter: PriceLimit.formatByCost},
	    	{name: 'ac_remark',label:'备注',index: 'ac_remark',width:100,hidden:false}
	    	
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'ac_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				dblClick = true;
				api.close();
            }
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'ac_isdraft=1';
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ac_outdp_code='+$("#ac_outdp_code").val();
		params += '&ac_indp_code='+$("#ac_indp_code").val();
		params += '&ac_manager='+Public.encodeURI($("#ac_manager").val());
		params += '&ac_number='+Public.encodeURI($("#ac_number").val());
		return params;
	},
	reset:function(){
		$("#outdepot_name").val("");
		$("#indepot_name").val("");
		$("#ac_outdp_code").val("");
		$("#ac_indp_code").val("");
		$("#ac_number").val("");
		$("#ac_manager").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}

function doSelect(){
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		Public.tips({type: 2, content : "请选择草稿！"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	return rowData;
}

THISPAGE.init();