var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	doQueryDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'stock'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ba_dp_code").val(selected.dp_code);
				$("#depot_name").val(selected.dp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ba_dp_code").val(selected.dp_code);
					$("#depot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ba_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ba_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	doQueryType : function(obj){
		commonDia = $.dialog({
			title : '选择类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:true},
			width : 280,
		   	height : 320,
		   	top : 50,
		   	left : 250,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					Public.tips({type: 2, content : '请选择商品类别!'});
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					var codes = [];
					var names = [];
					for(var i=0;i<selected.length;i++){
						codes.push(selected[i].tp_code);
						names.push(selected[i].tp_name);
					}
					$("#ba_scope_code").val(codes.join(","));
					$("#ba_scope_name").val(names.join(","));
					$(obj).prev().val(names.join(","));
				}
			},
			cancel:true
		});
	},
	doQueryBrand : function(obj){
		commonDia = $.dialog({
			title : '选择品牌',
			content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
			data : {multiselect:true},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].bd_code);
					names.push(selected[i].bd_name);
				}
				$("#ba_scope_code").val(codes.join(","));
				$("#ba_scope_name").val(names.join(","));
				$(obj).prev().val(names.join(","));
			},
			cancel:true
		});
	},
	doQueryProduct : function(obj){
		commonDia = $.dialog({
			title : '选择商品',
			content : 'url:'+config.BASEPATH+'base/product/to_list_dialog',
			data : {multiselect:true},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].pd_code);
					names.push(selected[i].pd_name);
				}
				$("#ba_scope_code").val(codes.join(","));
				$("#ba_scope_name").val(names.join(","));
				$(obj).prev().val(names.join(","));
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(isdraft){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"stock/check/batch_save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ba_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		this.initSeasonCombo();
		this.initYearCombo();
		this.initScopeCombo();
	},
	initScopeCombo : function(){
		var data = [ 
                {Code : '',Name : '--请选择--'}, 
                {Code : '0',Name : '全场盘点'}, 
                {Code : '1',Name : '类别盘点'},
                {Code : '2',Name : '品牌盘点'},
                {Code : '3',Name : '单品盘点'},
                {Code : '4',Name : '年份盘点'},
                {Code : '5',Name : '季节盘点'}
              ];
		
		$('#span_scope').combo({
			value : 'Code',
			text : 'Name',
			width : 207,
			listHeight : 300,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#ba_scope").val(data.Code);
					$("#ba_scope_code").val("");
					$("#ba_scope_name").val("");
					$("#typeTd").hide();
					$("#brandTd").hide();
					$("#productTd").hide();
					$("#seasonTd").hide();
					$("#yearTd").hide();
					$("#scopeLabel").html("");
					if(data.Code == '1'){
						$("#scopeLabel").html("类别盘点：");
						$("#typeTd").show();
					}else if(data.Code == '2'){
						$("#scopeLabel").html("品牌盘点：");
						$("#brandTd").show();
					}else if(data.Code == '3'){
						$("#scopeLabel").html("单品盘点：");
						$("#productTd").show();
					}else if(data.Code == '4'){
						$("#scopeLabel").html("年份盘点：");
						$("#yearTd").show();
						$("#ba_scope_code").val($('#span_year').getCombo().getValue());
						$("#ba_scope_name").val($('#span_year').getCombo().getText());
					}else if(data.Code == '5'){
						$("#scopeLabel").html("季节盘点：");
						$("#seasonTd").show();
						$("#ba_scope_code").val($('#span_season').getCombo().getValue());
						$("#ba_scope_name").val($('#span_season').getCombo().getText());
					}
				}
			}
		}).getCombo().loadData(data);
	},
	initSeasonCombo:function(){
		$('#span_season').combo({
            data: config.BASEPATH + "base/dict/list?dtl_upcode=KEY_SEASON",
            value : 'dtl_code',
			text : 'dtl_name',
			width : 207,
			listHeight : 300,
            listId: '',
            defaultSelected: 0,
            editable: false,
            ajaxOptions: {
                formatData: function (data) {
                    return data.data;
                }
            },
            callback: {
                onChange: function (data) {
                	$("#ba_scope_code").val(data.dtl_code);
                	$("#ba_scope_name").val(data.dtl_name);
                }
            }
        }).getCombo();
	},
	initYearCombo : function(){
        var currentYear = new Date().getFullYear();
		var data = [ 
	        {Code: parseInt(currentYear - 3) + "", Name: parseInt(currentYear - 3) + ""},
	        {Code: parseInt(currentYear - 2) + "", Name: parseInt(currentYear - 2) + ""},
	        {Code: parseInt(currentYear - 1) + "", Name: parseInt(currentYear - 1) + ""},
	        {Code: parseInt(currentYear) + "", Name: parseInt(currentYear) + ""},
	        {Code: parseInt(currentYear + 1) + "", Name: parseInt(currentYear + 1) + ""},
	        {Code: parseInt(currentYear + 2) + "", Name: parseInt(currentYear + 2) + ""},
	        {Code: parseInt(currentYear + 3) + "", Name: parseInt(currentYear + 3) + ""}
	      ];
		
		$('#span_year').combo({
			value : 'Code',
			text : 'Name',
			width : 207,
			listHeight : 300,
			listId : '',
			defaultSelected : 3,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#ba_scope_code").val(data.Code);
					$("#ba_scope_name").val(data.Name);
				}
			}
		}).getCombo().loadData(data,['Code','2016']);
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();