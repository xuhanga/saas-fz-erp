var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var ba_number = $("#ba_number").val();
var ckl_isrepair = $("#ckl_isrepair").val();
var ck_dp_code = $("#ck_dp_code").val();
var queryurl = config.BASEPATH+'stock/check/check_temp_list/'+ba_number+'/'+ckl_isrepair+'/'+ck_dp_code;
var querysumurl = config.BASEPATH+'stock/check/check_temp_sum/'+ba_number+'/'+ckl_isrepair+'/'+ck_dp_code;
var querysizeurl = config.BASEPATH+'stock/check/check_temp_size/'+ba_number+'/'+ckl_isrepair+'/'+ck_dp_code;
var querysizetitleurl = config.BASEPATH+'stock/check/check_temp_size_title/'+ba_number+'/'+ckl_isrepair;

var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-320,_width = $(parent).width()-2;

var showMode = {
	display:function(mode){
		var ids = $("#grid").getDataIDs();
		if(ids == undefined || null == ids || ids.length == 0){
			if(mode != 0){
				Public.tips({type: 1, content : "未添加数据，不能切换模式"});
				return;
			}
		}
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:querysizetitleurl,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var Choose = {
	repair:function(){
		isRefresh = false;
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+"stock/check/to_check_addrepair?ba_id="+$("#ba_id").val(),
			data: {},
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
				if(isRefresh){
		   			showMode.refresh();
		   		}
			}
		}).max();
	},
	imports:function(){
		isRefresh = false;
		$.dialog({ 
		   	id:'import',
		   	title:'盘点机导入',
		   	content:'url:'+config.BASEPATH+'common/to_barcodeimport',
		   	data : {saveSuccUrl:config.BASEPATH+"stock/check/check_temp_import/"+ba_number+"/"+ckl_isrepair},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:700,
		   	height:470,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	lock:true,
		   	close: function () {
		   		if(isRefresh){
		   			showMode.refresh();
		   		}
			}
		});
	},
	selectProduct:function(){
		$.dialog({
			title : '商品录入',
			content : 'url:'+config.BASEPATH+'stock/check/to_select_product',
			data : {
				dp_code : ck_dp_code,
				ba_number : ba_number,
				ckl_isrepair : ckl_isrepair,
				ba_scope : $("#ba_scope").val(),
				ba_scope_code : $("#ba_scope_code").val(),
				searchContent:$.trim($("#pd_no").val())
			},
			width : 1080,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	barCode:function(){
		var CurrentMode = $("#CurrentMode").val();
		if(CurrentMode == 1){//若是尺码模式调整到列表模式
			showMode.display(0);
		}
		var barcode = $.trim($("#barcode").val());
		var barcode_amount = $.trim($("#barcode_amount").val());
		if(barcode == ""){
			return;
		}
		if (barcode_amount == "" || isNaN(barcode_amount)) {
			Public.tips({type: 2, content : '请正确输入数量!'});
	        $('#barcode_amount').select();
	        return;
	    }
		var params = "";
		params += "barcode="+barcode;
		params += "&amount="+barcode_amount;
		params += "&ba_scope="+$("#ba_scope").val();
		params += "&ba_scope_code="+$("#ba_scope_code").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"stock/check/check_temp_save_bybarcode/"+ba_number+"/"+ckl_isrepair+'/'+ck_dp_code,
			data:params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data.result == 3){//条码不存在
						Choose.barcodeNotExist(barcode);
						return;
					}
					needSizeRefresh = true;
					$("#barcode").val("");
					var temp = data.data.temp;
					//列表模式
					if(data.data.result == 1){//update
						var rowData = $("#grid").jqGrid("getRowData", temp.ckl_id);
						rowData.ckl_amount = temp.ckl_amount;
						rowData.ckl_unitmoney = rowData.ckl_amount * rowData.ckl_unitprice;
						rowData.ckl_retailmoney = rowData.ckl_amount * rowData.ckl_retailprice;
						$("#grid").delRowData(temp.ckl_id);
						$("#grid").addRowData(temp.ckl_id, rowData, 'first');
						THISPAGE.gridTotal();
					}else if(data.data.result == 2){//add
						$("#grid").addRowData(temp.ckl_id, temp, 'first');
						THISPAGE.gridTotal();
					}
					//汇总模式
			    	var ids = $("#sumGrid").jqGrid('getDataIDs');
	            	var oldSumRowData = null;
	            	var sum_id = null;
					for(var i=0;i < ids.length;i++){
						var sumRowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(sumRowData.ckl_pd_code == temp.ckl_pd_code){
							oldSumRowData = sumRowData;
							sum_id = ids[i];
							break;
						}
					}
					if(oldSumRowData != null){
						oldSumRowData.ckl_amount = parseInt(oldSumRowData.ckl_amount) + parseInt(barcode_amount);
						oldSumRowData.ckl_unitmoney = oldSumRowData.ckl_amount * oldSumRowData.ckl_unitprice;
						oldSumRowData.ckl_retailmoney = oldSumRowData.ckl_amount * oldSumRowData.ckl_retailprice;
						$("#sumGrid").jqGrid('setRowData', sum_id, oldSumRowData);
						THISPAGE.sumGridTotal();
					}else{
						$("#sumGrid").addRowData(temp.ckl_id, temp, 'first');
						THISPAGE.sumGridTotal();
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
	},
	barcodeNotExist:function(barcode){
		$("#barcode").val("无此商品").select();
		var BarCodeCheckNotPass = document.getElementById("BarCodeCheckNotPass");
		if(BarCodeCheckNotPass.style.display == 'none'){
			BarCodeCheckNotPass.style.display = '';
			
			var barCodeDiv=$("#BarCodeCheckNotPass");
			var barCodeDivTop=$("#barcode_amount").offset().top
				,barCodeDivLeft=$("#barcode_amount").offset().left;
			barCodeDiv.css(
			{
				"top":barCodeDivTop-150
				,"left":barCodeDivLeft+30
			}
			);
		}
		var CheckNotPassText = document.getElementById("CheckNotPassText");
		if($("#CheckNotPassTextShow").val().indexOf(barcode+";") > -1){
			return;
		}
		CheckNotPassText.innerHTML = CheckNotPassText.innerHTML + "<p>" + barcode + "</p>";
		$("#CheckNotPassTextShow").val($("#CheckNotPassTextShow").val()+barcode+";");
	}
};

var Utils = {
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ck_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ck_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemarkById:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'stock/check/check_temp_updateRemarkById',
			data:{ckl_id:rowid,ckl_remark:Public.encodeURI(remark),ckl_ba_number:ba_number,ckl_isrepair:ckl_isrepair},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					var ids = $("#sumGrid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(row.ckl_pd_code == rowData.ckl_pd_code){
							$("#sumGrid").jqGrid('setRowData',ids[i],{ckl_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateRemarkByPdCode:function(pd_code,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'stock/check/check_temp_updateRemarkByPdCode',
			data:{ckl_pd_code:pd_code,ckl_remark:Public.encodeURI(remark),ckl_ba_number:ba_number,ckl_isrepair:ckl_isrepair},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var ids = $("#grid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var row = $("#grid").jqGrid("getRowData", ids[i]);
						if(row.ckl_pd_code == pd_code){
							$("#grid").jqGrid('setRowData',ids[i],{ckl_remark:remark});
						}
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateAmount:function(rowid,amount){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'stock/check/check_temp_updateAmount',
			data:{ckl_id:rowid,ckl_amount:amount},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					needSizeRefresh = true;
					needSumRefresh = true;
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_update : function(id,type){
		var params = {ba_number:ba_number,ckl_isrepair:ckl_isrepair};
 		var rowData;
 		if(type == '0'){
 			rowData = $("#grid").jqGrid("getRowData", id);
 			params.pd_code = rowData.ckl_pd_code;
 		}else if(type == '1'){
			rowData = $("#sizeGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.pd_code;
		}else if(type == '2'){
			rowData = $("#sumGrid").jqGrid("getRowData", id);
			params.pd_code = rowData.ckl_pd_code;
		}
 		params.dp_code = ck_dp_code;
		$.dialog({
			title : '修改商品',
			content : 'url:'+config.BASEPATH+'stock/check/to_check_temp_update',
			data : params,
			width : 850,
			height : 440,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				showMode.refresh();
			}
		});
	},
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'stock/check/check_temp_del',
					data:{"ckl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							needSizeRefresh = true;
							needSumRefresh = true;
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_delByPiCode: function(rowId,type){//删除
		if(rowId != null && rowId != ''){
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
		 		var params = {ckl_ba_number:ba_number,ckl_isrepair:ckl_isrepair};
		 		var rowData;
	    		if(type == '1'){
	    			rowData = $("#sizeGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.pd_code;
	    			params.cr_code = rowData.cr_code;
	    			params.br_code = rowData.br_code;
	    		}else if(type == '2'){
	    			rowData = $("#sumGrid").jqGrid("getRowData", rowId);
	    			params.pd_code = rowData.ckl_pd_code;
	    		}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'stock/check/check_temp_delByPiCode',
					data:params,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							showMode.refresh();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'stock/check/check_temp_clear',
				data:{ckl_ba_number:ba_number,ckl_isrepair:ckl_isrepair},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						$("#sumGrid").clearGridData();
						THISPAGE.sumGridTotal();
						if($("#CurrentMode").val() == '1'){
							showMode.refresh();
						}else{
							needSizeRefresh = true;
						}
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择货号！'});
			return;
		}
		$("#btn-save").attr("disabled",true);
		var saveUrl = config.BASEPATH+"stock/check/check_save/"+ba_number;
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=$("#ba_id").val();
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	submitRepair:function(){
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择货号！'});
			return;
		}
		$("#btn-submit-repair").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"stock/check/check_temp_repair/"+ba_number,
			data:{repair_type:$("#repair_type").val()},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					W.isRefresh = true;
					setTimeout("api.close()",500);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-submit-repair").attr("disabled",false);
			}
		});
		
	},
	formatSubAmount :function(val, opt, row){
		return row.ckl_amount-row.ckl_stock_amount;
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#ck_date").val(config.TODAY);
		this.$_repairType = $("#repairType").cssRadio({ callback: function($_obj){
			$("#repair_type").val($_obj.find("input").val());
		}});
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var ckl_amount=grid.getCol('ckl_amount',false,'sum');
		var ckl_stock_amount=grid.getCol('ckl_stock_amount',false,'sum');
    	var ckl_unitmoney=grid.getCol('ckl_unitmoney',false,'sum');
    	var ckl_retailmoney=grid.getCol('ckl_retailmoney',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',ckl_amount:ckl_amount,ckl_stock_amount:ckl_stock_amount,sub_amount:'',ckl_unitmoney:ckl_unitmoney,ckl_retailmoney:ckl_retailmoney});
    	$("#ck_amount").val(ckl_amount);
    	$("#ck_stockamount").val(ckl_stock_amount);
    	$("#sub_amount").val(ckl_amount-ckl_stock_amount);
    	$("#ck_money").val(PriceLimit.formatByCost(ckl_unitmoney));
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var ckl_amount=grid.getCol('ckl_amount',false,'sum');
    	var ckl_stock_amount=grid.getCol('ckl_stock_amount',false,'sum');
    	var ckl_unitmoney=grid.getCol('ckl_unitmoney',false,'sum');
    	var ckl_retailmoney=grid.getCol('ckl_retailmoney',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',ckl_amount:ckl_amount,ckl_stock_amount:ckl_stock_amount,sub_amount:'',ckl_unitmoney:ckl_unitmoney,ckl_retailmoney:ckl_retailmoney});
    	$("#ck_amount").val(ckl_amount);
    	$("#ck_stockamount").val(ckl_stock_amount);
    	$("#sub_amount").val(ckl_amount-ckl_stock_amount);
    	$("#ck_money").val(PriceLimit.formatByCost(ckl_unitmoney));
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'ckl_pd_code', index: 'ckl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'库存数量',name: 'ckl_stock_amount', index: 'ckl_stock_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'录入数量',name: 'ckl_amount', index: 'ckl_amount', width: 70,align:'right',sorttype: 'int',editable:true},
	    	{label:'相差数量',name: 'sub_amount', index: '', width: 70,align:'right',sortable:false,formatter: handle.formatSubAmount},
	    	{label:'零售价',name: 'ckl_retailprice', index: 'ckl_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'ckl_retailmoney', index: 'ckl_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'成本价',name: 'ckl_unitprice', index: 'ckl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'ckl_unitmoney', index: 'ckl_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'备注',name: 'ckl_remark', index: 'ckl_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ckl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'ckl_amount' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					} 
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.temp_updateAmount(rowid, parseInt(value));
					return parseInt(value);
				}else if(cellname == 'ckl_remark' && self.selectRow.value != value){
					handle.temp_updateRemarkById(rowid, value);
				}
				
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'ckl_amount' && self.selectRow.value != value){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					rowData.ckl_unitmoney = rowData.ckl_amount * rowData.ckl_unitprice;
					rowData.ckl_retailmoney = rowData.ckl_amount * rowData.ckl_retailprice;
					$("#grid").jqGrid('setRowData', rowid, rowData);
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	initSumGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'',name: 'ckl_pd_code', index: 'ckl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'库存数量',name: 'ckl_stock_amount', index: 'ckl_stock_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'录入数量',name: 'ckl_amount', index: 'ckl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'相差数量',name: 'sub_amount', index: '', width: 70,align:'right',sortable:false,formatter: handle.formatSubAmount},
	    	{label:'零售价',name: 'ckl_retailprice', index: 'ckl_retailprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'ckl_retailmoney', index: 'ckl_retailmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'成本价',name: 'ckl_unitprice', index: 'ckl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'成本金额',name: 'ckl_unitmoney', index: 'ckl_unitmoney', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'备注',name: 'ckl_remark', index: 'ckl_remark', width: 180,editable:true}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'ckl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '2');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(cellname == 'ckl_remark' && self.selectRow.value != value){
					var rowData = $("#sumGrid").jqGrid("getRowData", rowid);
					handle.temp_updateRemarkByPdCode(rowData.ckl_pd_code, value);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
		var gridWH = Public.setGrid();//操作
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'ckl_cr_code', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'ckl_br_code', width: 60},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'零售价',name: 'retail_price', index: 'retail_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
	    	{label:'零售金额',name: 'retail_money', index: 'retail_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatBySell},
            {label:'成本价',name: 'unit_price', index: 'unit_price', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
            {label:'成本金额',name: 'unit_money', index: 'unit_money', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
            height: gridWH.h-100,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 255 + (dyns.length * 32), 2);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '1');
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-import').on('click', function(e){
			e.preventDefault();
			Choose.imports();
		});
		$('#btn-repair').on('click', function(e){
			e.preventDefault();
			Choose.repair();
		});
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        //修改-列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	handle.temp_update(id, '0');
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
		
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).ckl_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-trash');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGrid').off('click','.operating .ui-icon-trash');
		$('#sizeGrid').off('click','.operating .ui-icon-image');
        //修改-尺码模式
        $('#sizeGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '1');
        });
        //修改-汇总模式
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.temp_update(id, '2');
        });
		
		 //删除-汇总模式
		$('#sumGrid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '2');
		});
		//删除-尺码模式
		$('#sizeGrid').on('click', '.operating .ui-icon-trash',function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_delByPiCode(id, '1');
		});
		
		$('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#sumGrid").jqGrid("getRowData", id).ckl_pd_code);
        });
	}
};

THISPAGE.init();