var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var ba_number = api.data.ba_number;
var queryurl = config.BASEPATH+'stock/check/check_missing/'+ba_number;
queryurl += '?dp_code='+api.data.dp_code;
queryurl += '&ba_scope='+api.data.ba_scope;
queryurl += '&ba_scope_code='+api.data.ba_scope_code;

var _height = api.config.height-202,_width = api.config.width-17;//获取弹出框弹出宽、高

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var sd_amount=grid.getCol('sd_amount',false,'sum');
    	grid.footerData('set',{pd_no:'合计：',sd_amount:sd_amount});
    },
	initGrid:function(){
		var colModel = [
	    	{label:'货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name',  index: 'pd_name',width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 70},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 70},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 70},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 70},
	    	{label:'库存数量',name: 'sd_amount', index: 'sd_amount', width: 80,align:'right'}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			cmTemplate: {sortable:false,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sd_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();