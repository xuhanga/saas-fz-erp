var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var pc_number = $("#pc_number").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'stock/price/detail_list/'+pc_number;

var _height = $(parent).height()-180,_width = $(parent).width()-2;

var handle = {
	approve:function(){
		commonDia = $.dialog({ 
		   	id:'approve_confirm',
		   	title:'审批页面',
		   	data:{},
		   	max: false,
		   	min: false,
		   	lock:true,
		   	width:300,
		   	height:200,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'approve/to_confirm',
		   	fixed:false,
		   	close : function(){
		   		var ar_infos = commonDia.content.ar_infos;
		   		if(ar_infos != undefined){
		   			handle.doApprove(ar_infos);
		   		}
		   	}
	    });
	},
	doApprove:function(ar_infos){
		$("#btn-approve").attr("disabled",true);
		var params = "";
		params += "number="+pc_number;
		params += "&ar_state="+ar_infos.ar_state;
		params += "&ar_describe="+Public.encodeURI(ar_infos.ar_describe);
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"stock/price/approve",
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '审核成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-approve").attr("disabled",false);
            }
        });
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.pc_id;
		pdata.pc_ar_state=data.pc_ar_state;
		pdata.pc_ar_date=data.pc_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	doPrint:function(){
		$.ajax({
            type: "POST",
            url: config.BASEPATH+"stock/price/print",
            data: {'number':pc_number},
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
           		 	var strBodyStyle="<style>table,td {border: 1 solid #000000;border-collapse:collapse;font-size:11pt;height:6mm;text-align: center;}</style>";
                    var bodyHtml = strBodyStyle + data.data.body;
                    LODOP.ADD_PRINT_HTM("2mm","3%","100%","30mm",data.data.header);
                    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
                    LODOP.ADD_PRINT_TABLE("32mm","3%","100%","100%",bodyHtml);
                    LODOP.SET_PRINT_STYLEA(0, "Vorient", 3);
                    LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","Auto-Width");//整宽不变形
                    LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED", 1);
                    LODOP.PREVIEW();
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
        var pc_ar_state = $("#pc_ar_state").val();
        if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
	},
	initGrid:function(){
		var colModel = [
		    {label:'操作',name:'operate', width: 40, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
		    {label:'',name: 'pcl_pd_code', index: 'pcl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'调整前价格',name: 'pcl_oldprice', index: 'pcl_oldprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost},
	    	{label:'调整后价格',name: 'pcl_newprice', index: 'pcl_newprice', width: 80,align:'right',sorttype: 'float',formatter: PriceLimit.formatByCost}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'pcl_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
        $('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	viewProductImg($("#grid").jqGrid("getRowData", id).ajl_pd_code);
        });
	}
};

THISPAGE.init();