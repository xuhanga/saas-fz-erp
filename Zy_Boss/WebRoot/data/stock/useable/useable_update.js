var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.BASE18;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'stock/useable/list';

var handle = {
	formatType:function(val, opt, row){
		if(row.ua_type == "0"){
			return "加";
		}else if(row.ua_type == "1"){
			return "减";
		}else{
			return "加/减";
		}
	},
	formatJoin:function(val, opt, row){
		var html = ""; 
		if(row.ua_join == "1"){
			html = "<input type='checkbox' checked data-id='"+opt.rowId+"' onclick='javascript:handle.selectJoin(this)'/>";
		}else{
			html = "<input type='checkbox' data-id='"+opt.rowId+"' onclick='javascript:handle.selectJoin(this)'/>";
		}
		return html;
	},
	selectJoin:function(obj,rowid){
		var rowid = $(obj).data("id");
		if($(obj).is(":checked")){
			$("#grid").jqGrid('setRowData', rowid, {ua_join:"1"});
		}else{
			$("#grid").jqGrid('setRowData', rowid, {ua_join:"0"});
		}
		this.buildExpression();
	},
	buildExpression:function(){
		var expression = "";
		var ids = $("#grid").jqGrid('getDataIDs');
		for(var i=0;i < ids.length;i++){
			var rowData = $("#grid").jqGrid("getRowData", ids[i]);
			if(rowData.ua_join == "1"){
				if(rowData.ua_type == 0){
					expression += "+";
				}else if(rowData.ua_type == 1){
					expression += "-";
				}else{
					expression += "+/-";
				}
				expression += rowData.ua_name;
			}
		}
		$("#expression").text(expression);
	},
	save:function(){
		if($("#st_useable").val() == ""){
			Public.tips({type: 1, content : "请选择是否启用可用库存！"});
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		var datas = [];
		for(var i=0;i < ids.length;i++){
			var rowData = $("#grid").jqGrid("getRowData", ids[i]);
			var data = {};
			data.ua_id = rowData.ua_id;
			data.ua_code = rowData.ua_code;
			if(rowData.ua_join == ""){
				data.ua_join = 0;
			}else{
				data.ua_join = rowData.ua_join;
			}
			datas.push(data);
		}
		var postData = {};
		postData.datas = datas;
		postData.st_id = $("#st_id").val();
		postData.st_useable = $("#st_useable").val();
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"stock/useable/update",
			data:JSON.stringify(postData),
			cache:false,
			dataType:"json",
			contentType:"application/json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					THISPAGE.reloadData();
					if($("#st_id").val() == ""){
						$("#st_id").val(data.data.st_id);
					}
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		$("#span_st_useable").cssRadio({ callback: function($_obj){
			$("#st_useable").val($_obj.find("input").val());
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{name: 'ua_id',label:'',index: 'ua_id',width:80,hidden:true},
	    	{name: 'ua_code',label:'',index: 'ua_code',width:80,hidden:true},
	    	{name: 'ua_type',index: 'ua_type',width:120,hidden:true},
	    	{name: 'ua_join',index: 'ua_join',width:120,hidden:true},
	    	{name: 'ua_name',label:'模块名称',index: 'ua_name',width:120},
	    	{name: '',label:'计算方式',index: '',width:120,align:'center',formatter:handle.formatType},
	    	{name: 'ua_remark',label:'说明',index: 'ua_remark',width:180},
	    	{name: '',label:'计算参与',index: '',width:120,align:'center',formatter:handle.formatJoin}
	    	
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-2,
			height: gridWH.h-20,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'ua_code'
			},
			loadComplete: function(data){
				handle.buildExpression();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		
	}
};
THISPAGE.init();