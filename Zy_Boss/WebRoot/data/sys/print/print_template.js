var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var pt_type = $("#pt_type").val();
var queryurl = config.BASEPATH+'sys/print/list_template/'+pt_type;
var querylisturl = config.BASEPATH+'sys/print/list';
var _height = $(parent).height()-217,_width = $(parent).width()-2;

var handle = {
	add : function(id){
		var rowData = $("#grid").jqGrid("getRowData", id);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'sys/print/to_print_set/'+rowData.pt_type,
			data:{
				pt_type:pt_type
			},
			max: false,
		   	min: false,
		   	fixed:false,
		   	drag:false,
		   	resize:false,
		   	lock:false,
		}).max();
	},
	edit : function(id){
		var rowid = $('#grid').jqGrid('getGridParam','selrow');
		var rowData = $("#grid").jqGrid("getRowData", rowid);
		$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'sys/print/to_print_set/'+rowData.pt_type,
			data:{
				sp_id:id
			},
			max: false,
		   	min: false,
		   	fixed:false,
		   	drag:false,
		   	resize:false,
		   	lock:false,
		}).max();
	},
	del: function(id){//删除
		$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'sys/print/del/'+id,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#gridList').jqGrid('delRowData', id);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-plus" title="新增">&#xe639;</i>';
		html_con += '</div>';
		return html_con;
	},
	operFmatterList:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatStat : function(val, opt, row){
		if(val == "0"){
			return "有效";
		}else if(val == "1"){
			return "无效";
		}
		return "";
	},
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initListGrid();
		this.initEvent();	
	},
	initDom : function(){
		
	},
	initGrid:function(){
		var colModel = [
	    	{label:'',name: 'pt_id',index: 'pt_id',width:60,hidden:true},
	    	{label:'',name: 'pt_type',index: 'pt_type',width:60,hidden:true},
	    	{label:'操作',name: 'operate',width: 40, formatter: handle.operFmatter,align:'center'},
	    	{label:'单据类型',name: 'pt_name',index: 'pt_name', width: 160, title: true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: 400,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:999,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'pt_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				if(ids.length>0){
					$("#grid tr:eq(1)").trigger("click");
				}else{
					$("#gridList").clearGridData();
				}
			},
			loadError: function(xhr, status, error){		
			},
			onCellSelect:function(rowid,iRow,iCol,e){
				var rowData = $("#grid").jqGrid("getRowData", rowid);
				$("#gridList").jqGrid('setGridParam',{datatype:"json",url:querylisturl+"/"+rowData.pt_type}).trigger("reloadGrid");
			}
	    });
	},
	initListGrid:function(){
		var colModel = [
            {label:'操作',name: 'operate',width: 60, formatter: handle.operFmatterList,align:'center'},
            {label:'模板名称',name: 'sp_name',index: 'sp_name',width:160},
	    ];
		$('#gridList').jqGrid({
			datatype: 'local',
			width: _width-610,
			height: _height,
			altRows:true,
			gridview: true,				
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			multiselect:false,//多选
			viewrecords: true,
			pager: '#pageList',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',
			rowNum:999,//每页条数
			shrinkToFit:false,//表格是否自动填充
			cmTemplate: {sortable:false,title:false},
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			}
	    });
	},
	initEvent:function(){
		$('#grid').on('click', '.operating .ui-icon-plus', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.add(id);
		});
		$('#gridList').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.edit(id);
		});
		$('#gridList').on('click', '.operating .ui-icon-trash', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.del(id);
		});
	}
};
THISPAGE.init();