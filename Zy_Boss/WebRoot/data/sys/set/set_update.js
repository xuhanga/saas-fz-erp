var config = parent.CONFIG;

function isRepeat(arr){
	var hash = {};
    for(var i in arr) {
        if(hash[arr[i]]){
            return true;
        }
        hash[arr[i]] = true;
    }
    return false;
}

var comboOpts = {
	value : 'Code',
	text : 'Name',
	width : 80,
	height : 300,
	listHeight : 300,
	listId : '',
	defaultSelected : 0,
	editable : false
};


function save(){
	var st_subcode_isrule = $("#st_subcode_isrule").val();
	if(st_subcode_isrule == "0"){
		$("#st_subcode_rule").val("编码+颜色+尺码+杯型");
	}
	$("#btn-save").attr("disabled",true);
	$.ajax({
		type:"POST",
		url:config.BASEPATH+"sys/set/update",
		data:$('#form1').serialize(),
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				Public.tips({type: 3, content : "保存成功"});
				if($("#st_id").val() == ""){
					$("#st_id").val(data.data.st_id);
				}
			}else{
				Public.tips({type: 1, content : data.message});
			}
			$("#btn-save").attr("disabled",false);
		}
	});
}

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initCombo();
		this.initEvent();
	},
	initDom:function(){
		$("#td_st_buy_calc_costprice").cssRadio({ callback: function($_obj){
			$("#st_buy_calc_costprice").val($_obj.find("input").val());
		}});
		$("#td_st_buy_os").cssRadio({ callback: function($_obj){
			$("#st_buy_os").val($_obj.find("input").val());
		}});
		$("#td_st_batch_os").cssRadio({ callback: function($_obj){
			$("#st_batch_os").val($_obj.find("input").val());
		}});
		$("#td_st_batch_showrebates").cssRadio({ callback: function($_obj){
			$("#st_batch_showrebates").val($_obj.find("input").val());
		}});
		$("#td_st_ar_print").cssRadio({ callback: function($_obj){
			$("#st_ar_print").val($_obj.find("input").val());
		}});
		$("#td_st_blank").cssRadio({ callback: function($_obj){
			$("#st_blank").val($_obj.find("input").val());
		}});
		$("#td_st_subcode_isrule").cssRadio({ callback: function($_obj){
			var st_subcode_isrule = $_obj.find("input").val();
			$("#st_subcode_isrule").val(st_subcode_isrule);
			if(st_subcode_isrule == 0){//系统配置
				$("#rule_span").text("编码+颜色+尺码+杯型");
				$("#rule_set").hide();
			}else{//规则设置
				$("#rule_span").text($("#st_subcode_rule").val());
				$("#rule_set").show();
			}
		}});
		$("#td_st_allocate_unitprice").cssRadio({ callback: function($_obj){
			$("#st_allocate_unitprice").val($_obj.find("input").val());
		}});
		$("#td_st_force_checkstock").cssRadio({ callback: function($_obj){
			$("#st_force_checkstock").val($_obj.find("input").val());
		}});
		$("#td_st_use_upmoney").cssRadio({ callback: function($_obj){
			$("#st_use_upmoney").val($_obj.find("input").val());
		}});
		$("#chk_st_check_showstock").cssCheckbox({callback : function($_obj) {
			var val = $_obj.find("input").is(":checked") ? 1 : 0;
			$("#st_check_showstock").val(val);
		}});
		$("#chk_st_check_showdiffer").cssCheckbox({callback : function($_obj) {
			var val = $_obj.find("input").is(":checked") ? 1 : 0;
			$("#st_check_showdiffer").val(val);
		}});
	},
	initCombo:function(){
		var data = [ 
				{Code : '编码',Name : '编码'}, 
				{Code : '货号',Name : '货号'}, 
				{Code : '颜色',Name : '颜色'}, 
				{Code : '尺码',Name : '尺码'}, 
				{Code : '杯型',Name : '杯型'} 
              ];
		var opts_one = $.extend(true,{
			callback : {
				onChange : function(data) {
					THISPAGE.changeRule($("#span_rule_one"));
				}
			}
		},comboOpts);
		var opts_two = $.extend(true,{
			callback : {
				onChange : function(data) {
					THISPAGE.changeRule($("#span_rule_two"));
				}
			}
		},comboOpts);
		var opts_three = $.extend(true,{
			callback : {
				onChange : function(data) {
					THISPAGE.changeRule($("#span_rule_three"));
				}
			}
		},comboOpts);
		var opts_four = $.extend(true,{
			callback : {
				onChange : function(data) {
					THISPAGE.changeRule($("#span_rule_four"));
				}
			}
		},comboOpts);
		var st_subcode_rule = $("#st_subcode_rule").val();
		var rules = ["","","",""];
		if(st_subcode_rule != ""){
			rules = st_subcode_rule.split("+");
		}
		$('#span_rule_one').combo(opts_one).getCombo().loadData(data,[ 'Code', rules[0], 0 ]);
		$('#span_rule_two').combo(opts_two).getCombo().loadData(data,[ 'Code', rules[1], 2 ]);
		$('#span_rule_three').combo(opts_three).getCombo().loadData(data,[ 'Code', rules[2], 3 ]);
		$('#span_rule_four').combo(opts_four).getCombo().loadData(data,[ 'Code', rules[3], 4 ]);
	},
	changeRule:function($_obj){
		var $_objs = [ $('#span_rule_one'), $('#span_rule_two'), $('#span_rule_three'), $('#span_rule_four') ];
		if(!$_objs[0].getCombo() || !$_objs[1].getCombo() || !$_objs[2].getCombo() || !$_objs[3].getCombo()){//验证四个combo是否全部加载完成
			return;
		}
		var index = -1;
		var vals = [];
		for (var i = 0; i < $_objs.length; i++) {
			vals.push($_objs[i].getCombo().getValue());
			if($_obj[0] == $_objs[i][0]){//比较jquery的dom对象
				index = i;
			}
		}
		if(!isRepeat(vals)){//不重复
			$("#st_subcode_rule").val(vals.join("+"));
			$("#rule_span").text($("#st_subcode_rule").val());
			return;
		}
		var old_vals = $("#st_subcode_rule").val().split("+");
		var repeatIndex = -1;
		for (var i = 0; i < vals.length; i++) {
			if(i != index && vals[i] == vals[index]){
				repeatIndex = i;
			}
		}
		$_objs[repeatIndex].getCombo().selectByValue(old_vals[index])
	},
	initEvent:function(){
		
	}
};

THISPAGE.init();