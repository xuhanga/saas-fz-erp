var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		var us_name = $("#us_name").val().trim();
	    if(us_name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#us_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#us_name").val(us_name);//防止空格
	    }
		$("#btn-save").attr("disabled",true);
		handle.buildPrice();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"sys/user/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					if(callback && typeof callback == 'function'){
						callback({},oper,window);
					}
					setTimeout("api.close()",500);
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					W.isRefresh = false;
					setTimeout("api.zindex()",500);
				}else{
					W.isRefresh = false;
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	buildPrice:function(){
		var price_limit = "";
		$("input[name='price_limit']").each(function(){
			if($(this).attr("checked")){
				$(this).attr("checked");
				price_limit += $(this).val();
			}
		});
		$("#us_limit").val(price_limit);
	},
	doEmp:function(){
		empData = W.$.dialog({
			title : '选择用户',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = empData.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = empData.content.doSelect();
				if(selected){
					$("#us_account").val(selected.em_code);
					$("#us_name").val(selected.em_name);
				}
				setTimeout("api.zindex()",200);
			},
			cancel:true
		});
	},
	doShop:function(){
		var ro_code = $("#us_ro_code").val();
		var ro_shop_type = $("#ro_shop_type").val();
		if(null != ro_code && "" != ro_code){
			shopData = W.$.dialog({
				title : '选择店铺',
				content : 'url:'+config.BASEPATH+'base/shop/to_sin_list_dialog',
				data : {"shop_type":ro_shop_type},
				width : 450,
				height : 370,
				max : false,
				min : false,
				cache : false,
				lock: true,
				ok:function(){
					var selected = shopData.content.doSelect();
					if (!selected) {
						return false;
					}
				},
				close:function(){
					var selected = shopData.content.doSelect();
					if(selected){
						$("#us_shop_code").val(selected.sp_code)
						$("#sp_name").val(selected.sp_name);
					}
					setTimeout("api.zindex()",200);
				},
				cancel:true
			});
		}else{
			W.Public.tips({type: 1, content : "先选择角色"});
		}
	},
	doRole:function(){
		roleData = W.$.dialog({
			title : '选择',
			content : 'url:'+config.BASEPATH+'sys/role/to_sin_list_dialog',
			data : {},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = roleData.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = roleData.content.doSelect();
				if(selected){
					$("#us_ro_code").val(selected.ro_code)
					$("#ro_name").val(selected.ro_name);
					$("#ro_shop_type").val(selected.ro_shop_type);
				}
				setTimeout("api.zindex()",500);
			},
			cancel:true
		});
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#us_account").select().focus();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$("#btn_emp").on('click',function(e){
			e.preventDefault();
			handle.doEmp();
		});
		$("#btn_shop").on('click',function(e){
			e.preventDefault();
			handle.doShop();
		});
		$("#btn_role").on('click',function(e){
			e.preventDefault();
			handle.doRole();
		});
	}
};
THISPAGE.init();