var api = frameElement.api, oper = api.opener;
var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var callback = api.data.callback;
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH + 'sys/workbench/listSubMenu';

var $_matchCon = null;
var Utils = {
    buildUrl: function() {
        var url = "&rtCodes="+api.data.rtCodes;
        url += "&usIds="+api.data.usIds;
        url += "&mn_upcode="+$("#mn_upcode").val();
        url += "&allParentIds="+allParentIds.join(",");
        return url;
    }
};
var allParentIds = [];
var THISPAGE = {
    init: function () {
        this.initDom();
        this.initUpMenu();
        this.initGrid();
        this.initEvent();
    },
    initDom: function() {
        this.$_TypeLi = $('#td_type').cssRadio({
            callback: function($_obj) {
            	$("#wb_type").val($_obj.find("input").attr("value"));
            	THISPAGE.reloadData();
            }
        });
        var wb_type = api.data.wb_type;
        $("#wb_type").val(wb_type);
        if(wb_type != undefined && wb_type != ""){
        	$("#TypeLi").hide();
        }
    },
    initUpMenu : function() {
    	var rtCodes = api.data.rtCodes;
    	var roCodes = api.data.roCodes;
    	var params = "rtCodes="+rtCodes+"&roCodes="+roCodes;
    	$.ajax({
			type:"POST",
			url:config.BASEPATH+"sys/workbench/listUpMenu?"+params,
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					$('#span_UpMenu').combo({
						value : 'mn_code',
						text : 'mn_name',
						width : 207,
						listHeight : 300,
						listId : '',
						defaultSelected : 0,
						editable : false,
						callback : {
							onChange : function(data) {
								$("#mn_upcode").val(data.mn_code);
								THISPAGE.reloadData();
							}
						}
					}).getCombo().loadData(data.data,["mn_code",$("#mn_upcode").val(),0]);
				}
			}
		 });
	},
    initGrid: function () {
        var colModel = [
            {label:'菜单编号',name: 'mn_code', index: 'mn_code', width: 120},
            {label:'菜单名称',name: 'mn_name', index: 'mn_name', width: 180}
        ];
        $('#grid').jqGrid({
            url: queryurl + '?' + Utils.buildUrl(),
            datatype: 'local',
            width: _width,
			height: _height,
            altRows: true,
            gridview: true,
            onselectrow: false,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#page',//分页
            multiselect: true,//多选
            viewrecords: true,
            cmTemplate: {sortable: true, title: false},
            page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:99999,//每页条数
			recordtext:'{0} - {1} 共 {2} 条',  
            shrinkToFit: false,//ture，则按比例初始化列宽度
            forceFit: false,//调整列宽度会改变表格的宽度  shrinkToFit 为false时，此属性会被忽略
            jsonReader: {
            	root: "data",
				repeatitems : false,
				id: 'mn_id'  //图标ID
            },
            loadComplete: function (data) {
            	
            }
        });
    },
    reloadData: function () {
        var params = Utils.buildUrl();
        $("#grid").jqGrid('setGridParam', {datatype: "json",page:1, url: queryurl + "?" + params}).trigger("reloadGrid");
    },
    initEvent: function () {
       
    }
}
THISPAGE.init();

function ok_Click() {
	var params = {};
	var selectedId =$('#grid').jqGrid('getGridParam','selarrrow');
	if(selectedId !=null && selectedId.length > 0){
		params.ids = selectedId.join(",");
	}else{
		Public.tips({type: 2, content: '请选择菜单！'});
		return false;
	}
	params.wb_type = $("#wb_type").val();
	$.ajax({
		type:"POST",
		url:config.BASEPATH+"sys/workbench/save",
		data:params,
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				if(callback && typeof callback == 'function'){
					callback();
				}
				api.close();
			}else{
				Public.tips({type: 1, content : data.message});
			}
		}
	});
    return false;
}