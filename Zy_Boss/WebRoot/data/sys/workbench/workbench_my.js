var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;

var Utils = {
	callback: function(){
		THISPAGE.loadWorkBenchMenu();
	},
	doAddPage : function(wb_type) {
		$.dialog({
			id : 'workbench_add',
			title : '工作台菜单',
			data : {
				callback: Utils.callback,
				wb_type :wb_type,
				us_id : system.US_ID,
				shop_type : system.SHOP_TYPE,
				us_ro_code : system.US_RO_CODE,
			},
			max : false,
			min : false,
			width: 600,
            height: 425,
			fixed : false,
			drag : false,
			resize : false,
			lock : true,
			content : "url:" + config.BASEPATH + "sys/workbench/to_add",
			ok: function () {
                return this.content.ok_Click();
            },
            cancel: true
		});
	},
	doAddToDoPage : function(wb_type) {
		$.dialog({
			id : 'workbench_add',
			title : '待办事项',
			data : {
				callback: Utils.callback,
				wb_type :wb_type,
				us_id : system.US_ID,
				shop_type : system.SHOP_TYPE,
				us_ro_code : system.US_RO_CODE,
			},
			max : false,
			min : false,
			width: 400,
			height: 425,
			fixed : false,
			drag : false,
			resize : false,
			lock : true,
			content : "url:" + config.BASEPATH + "sys/workbench/to_add_todo",
			ok: function () {
				return this.content.ok_Click();
			},
			cancel: true
		});
	},
	doQueryWorkBench : function() {
		$.dialog({
			id : 'workbench_set',
			title : '工作台设置',
			data : {
				refresh : THISPAGE.refresh
			},
			max : false,
			min : false,
			width: 600,
            height: 425,
			fixed : false,
			drag : false,
			resize : false,
			lock : true,
			content : "url:" + config.BASEPATH + "sys/workbench/to_workbench_set",
			close : function() {
				var refresh = THISPAGE.refresh.val();
				if (refresh) {
					THISPAGE.loadWorkBenchMenu();
				}
			}
		});
	}
};


var THISPAGE = {
	init:function (){
		this.initDom();
		this.loadWorkBenchMenu();
	},
	initDom:function(){
		this.refresh = $('#refresh');
//		this.formatDate(DateToFullDateTimeString(new Date()));
	},
	formatDate : function(date) {
		var ymd = new Array();
		ymd=date.split('-');
		var fullDate = ymd[1]+"月"+ymd[2]+"日";
		$("#currentDate").text(fullDate);
	},
	loadWorkBenchMenu : function() {
		$.ajax({
			type:"get",
			url:config.BASEPATH+"sys/workbench/listByUser",
			data:"",
			cache:true,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					//我的日常工作
					var dailyWork = data.data.dailyWork;
					$("#dailyWorkMenu").empty();
					if(dailyWork != undefined && dailyWork.length > 0){
						var item = null;
						for (var i = 0, length = dailyWork.length; i < length; i++) {
							var li = '';
							item = dailyWork[i];
							li += '<li><a class="last_menu" dataType="iframe" '; 
							li += 'dataLink="'+item.mn_url+'" id="'+item.wb_mn_id+'" jerichotabindex="'+item.wb_mn_id+'" '; 
							li += 'onclick="javascript:addTags(this,\''+item.mn_name+'\');">'; 
							if($.trim(item.mn_icon) != ""){
								li += '<i class="iconfont '+item.mn_style+'">'+item.mn_icon+'</i>'; 
							}else{
								li += '<i class="iconfont bg_green">&#xe62c;</i>'; 
							}
							li += '<span class="last_menu">'+item.mn_name+'</span>'; 
							li += '</a></li>'; 
							$("#dailyWorkMenu").append(li);
						}
					}
					$("#dailyWorkMenu").append('<li><a id="addButton1"><i class="iconfont bg_green">&#xe639;</i><span class="last_menu">新增</span></a></li>');
					//我的日常报表
					var dailySheet = data.data.dailySheet;
					$("#dailySheetMenu").empty();
					if(dailySheet != undefined && dailySheet.length > 0){
						var item = null;
						for (var i = 0, length = dailySheet.length; i < length; i++) {
							var li = '';
							item = dailySheet[i];
							li += '<li><a class="last_menu" dataType="iframe" '; 
							li += 'dataLink="'+item.mn_url+'" id="'+item.wb_mn_id+'" jerichotabindex="'+item.wb_mn_id+'" '; 
							li += 'onclick="javascript:addTags(this,\''+item.mn_name+'\');">'; 
							if($.trim(item.mn_icon) != ""){
								li += '<i class="iconfont '+item.mn_style+'">'+item.mn_icon+'</i>'; 
							}else{
								li += '<i class="iconfont bg_green">&#xe62c;</i>'; 
							}
							li += '<span class="last_menu">'+item.mn_name+'</span>'; 
							li += '</a></li>'; 
							$("#dailySheetMenu").append(li);
						}
					}
					$("#dailySheetMenu").append('<li><a id="addButton2"><i class="iconfont bg_green">&#xe639;</i><span class="last_menu">新增</span></a></li>');
					//待办事项
					var toDo = data.data.toDo;
					$("#todoList").empty();
					var todoids = [];
					if(toDo != undefined && toDo.length > 0){
						var item = null;
						for (var i = 0, length = toDo.length; i < length; i++) {
							var li = '';
							item = toDo[i];
							li += '<li dataType="iframe" dataLink="'+item.mn_url+'" id="todo'+item.wb_mn_id+'" jerichotabindex="2"';
							li += ' onclick="javascript:addTags(this,\''+item.mn_name+'\');">';
							li += '<i class="iconfont '+item.mn_style+'">'+item.mn_icon+'</i> '+item.mn_name+' ';
							li += '<span>0</span></li>';
							$("#todoList").append(li);
							todoids.push(item.wb_mn_id);
						}
						$("#todoids").val(todoids.join(","));
						if(todoids.length > 0){
							THISPAGE.statToDoWarn();
						}
					}
					THISPAGE.initEvent();
				}
			}
	 	});
	},
	statToDoWarn : function(){
		var params = 'todoids='+$("#todoids").val();
		$.ajax({
			type:"get",
			url:config.BASEPATH+"sys/workbench/statToDoWarn",
			data:params,
			cache:true,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var statData = data.data;
					for ( var key in statData) {
						$("#todo"+key).find("span").text(statData[key]);
					}
				}
			}
	 	});
	},
	initEvent:function(){
        $('#addButton1').on('click', function(e) {
            e.preventDefault();
            Utils.doAddPage("0");
        });
        $('#addButton2').on('click', function(e) {
            e.preventDefault();
            Utils.doAddPage("1");
        });
        $('#addToDoButton').on('click', function(e) {
            e.preventDefault();
            Utils.doAddToDoPage();
        });
        $('#refreshToDo').on('click', function(e) {
            e.preventDefault();
            THISPAGE.statToDoWarn();
        });
        $('#modifyButton').on('click', function(e) {
            e.preventDefault();
            Utils.doQueryWorkBench();
        });
	}
};
THISPAGE.init();