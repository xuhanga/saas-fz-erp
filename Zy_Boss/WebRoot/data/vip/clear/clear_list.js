var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.VIP16;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/clear/page';

var Utils = {
	doQueryMemberType : function(){
		commonDia = $.dialog({
			title : '会员类别',
			content : 'url:'+config.BASEPATH+'vip/membertype/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_mt_code").val(selected.mt_code);
					$("#mt_name").val(selected.mt_name);
				}
			},
			cancel:true
		});
	},
	doQueryVipGrade : function(){
		commonDia = $.dialog({
			title : '会员等级',
			content : 'url:'+config.BASEPATH+'vip/set/to_list_dialog',
			data : {multiselect:false},
			width : 520,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#gd_code").val(selected.gd_code);
					$("#gd_name").val(selected.gd_name);
				}
			},
			cancel:true
		});
	},
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_manager_code").val(selected.em_code);
					$("#vm_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operFmatter:function(val, opt, row) {
		var html_con = "<div class='operating' data-id='" + row.vm_id + "'>";
		if(row.vm_id != ""){
			html_con += "<i class='iconfont i-hand' title='回访' onclick=\"handle.consumeVisit('"+row.vm_id+"','"+row.vm_cardcode+"')\" >&#xe69b;</i>";
		}else{
			html_con += "<span class='ui-icon' style='background:none;'></span><span class='ui-icon' style='background:none;'></span>";
		}
		html_con += "</div>";
		return html_con;
	},
	consumeVisit:function(vm_id, vm_cardcode){//消费回访
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		$.dialog({ 
		   	title:'会员回访',
		   	//data:{vi_type:1},//1:会员回访 2:生日回访 3:消费回访
		   	max: false,
		   	min: false,
		   	width:900,height:450,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'vip/member/vip_visit_analysis?vm_id='+vm_id+'&vi_type=1&vm_cardcode='+vm_cardcode,
		   	lock:true,
		   	close: function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
		   			isRefresh = false;
		   		}
		   	}
	    });
	},
	clearPoints: function(rowIds){// 清除积分
		var vals = "";
		if(rowIds != null && rowIds != ''){
			if(rowIds instanceof Array){
				for(var i = rowIds.length-1; i>=0;i--){
					var rowData = $("#grid").jqGrid("getRowData", rowIds[i]);
					vals += rowData.vm_id +",";
				}
			} else {
				var rowData = $("#grid").jqGrid("getRowData", rowIds);
				vals += rowData.vm_id + ",";
			}
			vals = vals.substring(0, vals.length-1);
			$.dialog.confirm('会员积分清除将不能恢复，请确认是否清除？',function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'vip/clear/clear_points?ids='+vals,
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '积分清除成功!!!'});
							THISPAGE.reloadData();
						}else{
							Public.tips({type: 1, content : '积分清除失败,请联系统管理员!!!'});
						}
					}
				});
			});
		} else {
			Public.tips({type: 2, content : '请选择要清除积分的会员！'});
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{label:'操作', name: 'operate', index: 'operate', formatter: handle.operFmatter, width: 40, align:'center'},
			{name: 'vm_id',label:'', index: 'vm_id',width: 120, hidden:true},
	    	{name: 'vm_code',label:'编号',index: 'vm_code',width:0,hidden:true},
	    	{name: 'vm_cardcode',label:'卡号',index: 'vm_cardcode',width:100},
	    	{name: 'vm_name',label:'名称',index: 'vm_name',width:75},
	    	{name: 'vm_sex',label:'性别',index: 'vm_sex',align:'center',width:40},
	    	{name: 'mt_name',label:'类别',index: 'vm_mt_code',width:60},
	    	{name: 'gd_name',label:'等级',index:'gd_name',width: 100, title: false}, 
	    	{name: 'vm_mobile',label:'手机',index: 'vm_mobile',width:96},
	    	{name: 'vm_total_money',label:'累计消费额',index:'vm_total_money',width: 120,align:'right'},
	    	{name: 'vm_points',label:'会员积分',index:'vm_points',width: 70,align:'right'},
	    	{name: 'vm_last_points',label:'剩余积分',index:'vm_last_points',width: 70,align:'right'},
	    	{name: 'vm_date',label:'办卡日期',index: 'vm_date',width:82},
	    	{name: 'vm_manager',label:'发卡人',index:'vm_manager',width: 60},
	    	{name: 'shop_name',label:'发卡门店',index: 'vm_shop_code',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+"?"+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-40,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:true,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'vm_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_mt_code='+$("#vm_mt_code").val();
		params += '&gd_name='+Public.encodeURI($("#gd_name").val());
		params += '&vm_manager_code='+$("#vm_manager_code").val();
		params += '&vm_cardcode='+Public.encodeURI($("#vm_cardcode").val());
		params += '&begin_last_points='+Public.encodeURI($("#begin_last_points").val());
		params += '&end_last_points='+Public.encodeURI($("#end_last_points").val());
		params += '&consumeDay='+$("#consumeDay").val();
		return params;
	},
	reset:function(){
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
		$("#sp_name").val("");
		$("#vm_shop_code").val("");
		$("#mt_name").val("");
		$("#vm_mt_code").val("");
		$("#gd_name").val("");
		$("#gd_code").val("");
		$("#vm_manager").val("");
		$("#vm_manager_code").val("");
		$("#vm_cardcode").val("");
		$("#begin_last_points").val("");
		$("#end_last_points").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_membertype").click(function(){
			Utils.doQueryMemberType();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
		$("#btn_grade").click(function(){
			Utils.doQueryVipGrade();
		});
		//批量清除积分
		$('#btn-clearPoints').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return;
			};
			e.preventDefault();
			var rowIds = $('#grid').jqGrid('getGridParam','selarrrow');
			handle.clearPoints(rowIds);
		});
	}
}
THISPAGE.init();