var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.VIP23;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/member/assign_page';

var Utils = {
	doQueryMemberType : function(){
		commonDia = $.dialog({
			title : '会员类别',
			content : 'url:'+config.BASEPATH+'vip/membertype/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_mt_code").val(selected.mt_code);
					$("#mt_name").val(selected.mt_name);
				}
			},
			cancel:true
		});
	},
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_manager_code").val(selected.em_code);
					$("#vm_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	//分配办卡人员
	update_assign:function(rowIds){
		if(rowIds != null && rowIds != ''){
			commonDia = $.dialog({ 
			   	title:'分配办卡人员',
			   	content:'url:'+config.BASEPATH+'vip/member/to_assign_emp',
			   	data: {rowIds: rowIds,multiselect:true,callback: this.callback},
			   	width : 450,
				height : 370,
			   	max: false,
			   	min: false,
			   	fixed:false,
			   	//设置是否拖拽(false:禁止，true可以移动)
			   	drag: true,
			   	resize:false,
			   	lock:true,
		        ok: function () {
		        	var selected = commonDia.content.doSelect();
		        },
			   	cancel:true
		    });
		}else{
			Public.tips({type: 2, content : '请选择数据!!!'});
		} 
	},
    callback: function(data, oper, dialogWin){
		THISPAGE.reloadData();
	},
	formatState:function(val, opt, row){//0:正常 1:无效 2:挂失
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "无效";
		}else if(val == "2"){
			return "挂失";
		}else{
			return val;
		}
	},
	formatBirthdayType:function(val, opt, row){//0公历生日1为农历生日
		if(val == "0"){
			return "公历";
		}else if(val == "1"){
			return "农历";
		}else{
			return val;
		}
	},
	formatBirthday:function(val, opt, row){//0公历生日1为农历生日
		if(row.vm_birthday_type == "0"){
			return row.vm_birthday;
		}else if(row.vm_birthday_type == "1"){
			return row.vm_lunar_birth;
		}else{
			return '';
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{name: 'id', index: 'vm_id',hidden: true},
	    	{name: 'vm_code',label:'编号',index: 'vm_code',width:0,hidden:true},
	    	{name: 'vm_cardcode',label:'卡号',index: 'vm_cardcode',width:100},
	    	{name: 'vm_name',label:'名称',index: 'vm_name',width:75},
	    	{name: 'mt_name',label:'类别',index: 'vm_mt_code',width:60},
	    	{name: 'vm_state',label:'状态',index: 'vm_state',align:'center',width:40,formatter:handle.formatState},
	    	{name: 'vm_mobile',label:'手机',index: 'vm_mobile',width:96},
	    	{name: 'vm_sex',label:'性别',index: 'vm_sex',align:'center',width:40},
	    	{name: 'vm_manager',label:'发卡人',index:'vm_manager',width: 60},
	    	{name: 'vm_old_manager',label:'原发卡人',index:'vm_old_manager',width: 60},
	    	{name: 'vm_birthday_type',label:'方式',index: 'vm_birthday_type',align:'center',width:60,formatter:handle.formatBirthdayType},
	    	{name: 'vm_birthday',label:'生日',index: 'vm_birthday',width:82,formatter:handle.formatBirthday},
	    	{name: 'vm_lastbuy_date',label:'最后购买时间',index:'vm_lastbuy_date',width: 80, title: false, align:'center'},
	    	{name: 'vm_times',label:'消费次数',index:'vm_times',width: 60,align:'right'},
	    	{name: 'vm_total_money',label:'累计消费额',index:'vm_total_money',width: 120,align:'right'},
	    	{name: 'vm_points',label:'会员积分',index:'vm_points',width: 70,align:'right'},
	    	{name: 'vm_used_points',label:'已用积分',index:'vm_used_points',width: 70,align:'right'},
	    	{name: 'vm_last_points',label:'剩余积分',index:'vm_last_points',width: 70,align:'right'},
	    	{name: 'vm_date',label:'办卡日期',index: 'vm_date',width:82},
	    	{name: 'shop_name',label:'发卡门店',index: 'vm_shop_code',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-40,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:true,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'vm_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_mt_code='+$("#vm_mt_code").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_manager_code='+$("#vm_manager_code").val();
		params += '&vm_cardcode='+Public.encodeURI($("#vm_cardcode").val());
		params += '&vm_name='+Public.encodeURI($("#vm_name").val());
		params += '&vm_mobile='+Public.encodeURI($("#vm_mobile").val());
		params += "&vm_empty_manager="+Public.encodeURI(THISPAGE.getradioVal());//空发卡人
		return params;
	},
	reset:function(){
		$("#vm_mt_code").val("");
		$("#vm_shop_code").val("");
		$("#vm_manager_code").val("");
		$("#vm_cardcode").val("");
		$("#vm_name").val("");
		$("#vm_mobile").val("");
		$("#mt_name").val("");
		$("#sp_name").val("");
		$("#vm_manager").val("");
		THISPAGE.$_date.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
		$("[name='vm_empty_manager']").removeAttr("checked");
		$("#vm_empty_manager").val("empty_manager");
	},
	getradioVal:function(){
	    var val=$('input:checkbox[name="vm_empty_manager"]:checked').val();
	    if(val==null){
	    	val="";
	    } 
	     return val;     
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn-assign').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
				return;
			}
			var rowIds = $('#grid').jqGrid('getGridParam','selarrrow');
			handle.update_assign(rowIds);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_membertype").click(function(){
			Utils.doQueryMemberType();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
	}
}
THISPAGE.init();