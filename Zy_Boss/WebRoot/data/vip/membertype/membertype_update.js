var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;;
var handle = {
	save:function(){
		var name = $("#mt_name").val().trim();
	    if(name == ''){
	    	W.Public.tips({type: 1, content : "请输入名称"});
	        $("#mt_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#mt_name").val(name);//防止空格
	    }
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/membertype/update",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 3, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.zindex()",500);
					THISPAGE.initDom();
				}else if(data.stat == 304){
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",500);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.mt_id;
		pdata.mt_code=data.mt_code;
		pdata.mt_name=data.mt_name;
		pdata.mt_discount=data.mt_discount;
		pdata.mt_isauto_up=data.mt_isauto_up;
		pdata.mt_min_point=data.mt_min_point;
		pdata.mt_money=data.mt_money;
		pdata.mt_point=data.mt_point;
		pdata.mt_point_to_money=data.mt_point_to_money;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
	},
	initDom:function(){
		$("#mt_name").select().focus();
		this.$_auto_up = $("#td_auto_up").cssRadio({ callback: function($_obj){
			$("#mt_isauto_up").val($_obj.find("input").val());
		}});
		this.$_auto_up.setValue(1-$("#mt_isauto_up").val());
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();