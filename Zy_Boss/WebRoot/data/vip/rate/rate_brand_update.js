var config = parent.parent.parent.CONFIG,system=parent.parent.parent.SYSTEM;
var queryurl = config.BASEPATH+'vip/membertype/list';
var api = frameElement.api, W = api.opener;
var callback = api.data.callback;
var handle = {
	update:function(){
		var br_id = $("#br_id").val();
		var br_rate = $("#br_rate").val();
		if(null == br_rate || '' == br_rate){
			W.Public.tips({type: 2, content : "请输入折扣!"});
			return;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/rate/updateBrand",
			data:{"br_id":br_id,"br_rate":br_rate},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					callback();
					setTimeout("api.close()",200);
				}
			}
		});
	}
}
var THISPAGE = {
	init:function (){
		this.initEvent();	
	},
	initEvent:function(){
		$("#btn-save").on('click',function(){
			handle.update();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
}
THISPAGE.init();