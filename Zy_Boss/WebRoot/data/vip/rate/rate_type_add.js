var config = parent.parent.parent.CONFIG,system=parent.parent.parent.SYSTEM;
var queryurl = config.BASEPATH+'vip/membertype/list';
var api = frameElement.api, W = api.opener;
var callback = api.data.callback;
var handle = {
	doBrand:function(){
		brandData = W.$.dialog({
			title : '选择类别',
			content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
			data : {multiselect:false},
			width : 320,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = brandData.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = brandData.content.doSelect();
				if(selected){
					$("#tr_tp_code").val(selected.tp_code);
					$("#tp_name").val(selected.tp_name);
				}
			},
			cancel:true
		});
	},
	save:function(){
		var tp_code = $("#tr_tp_code").val();
		if(null == tp_code || '' == tp_code){
			W.Public.tips({type: 2, content : "请选择品牌!"});
			return;
		}
		var arr = [];
		var grid = $("#grid");
		var ids = grid.getDataIDs();
		for( var i = 0 ; i < ids.length ; i ++ ){
			var rowData = grid.jqGrid("getRowData",ids[i]);
			var pdata = {};
			pdata.tr_mt_code=rowData.mt_code;
			pdata.tr_rate=rowData.tr_rate;
			arr.push(pdata);
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/rate/saveType",
			data:{"tp_code":tp_code,"data":JSON.stringify(arr)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					callback();
					setTimeout("api.close()",200);
				}
			}
		});
	}
}
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
	},
	initGrid:function(){
		var colModel = [
	    	/*{name: 'operate',label:'操作',width: 70, formatter: Public.operFmatter,align:'center', sortable:false},*/
	    	{name: 'mt_name',label:'会员类别',index: 'mt_name',width:120},
	    	{name: 'mt_code',label:'会员类别',index: 'mt_code',hidden:true},
	    	{name: 'mt_discount',label:'会员类别折扣',index: 'mt_discount',width:120,align:'right'},
	    	{name: 'tr_rate',label:'商品类别折扣',index: 'tr_rate',width:120,align:'right',editable:true}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: 580,
			height: 250,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'mt_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").jqGrid('getDataIDs');
            	for( var i = 0 ; i < ids.length ; i ++ ){
            		var rowData = $("#grid").jqGrid("getRowData",ids[i]);
        			$("#grid").jqGrid('setRowData',ids[i],{tr_rate:rowData.mt_discount});
            	}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'tp_code='+$("#tp_code").val();
		params += '&mt_code='+$("#mt_code").val();
		return params;
	},
	reset:function(){
		$("#tp_code").val("");
		$("#mt_code").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$("#btn_brand").on('click',function(){
			handle.doBrand();
		});
		$("#btn-save").on('click',function(){
			handle.save();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
}
THISPAGE.init();