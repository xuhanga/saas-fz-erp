var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP18;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/report/type_analysis_list';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#vm_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#vm_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	vipCountRateFmatter:function(val,opt,row){
		var _val = (100*row.vip_count_rate).toFixed(2) + '%';
		return _val;
	},
	consumerVipRateFmatter:function(val,opt,row){
		var _val = (100*row.consumer_vip_rate).toFixed(2) + '%';
		return _val;
	},
	moneryRateFmatter:function(val,opt,row){
		var _val = (100*row.monery_rate).toFixed(2) + '%';
		return _val;
	},
	profitsRateFmatter:function(val,opt,row){
		var _val = (100*row.profits_rate).toFixed(2) + '%';
		return _val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		dateRedioClick("theMonth");
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
	    	{label:'类别编号', name: 'mt_code', index: 'tp_code', width:100, title: false},
		    {label:'类别名称',name: 'mt_name', index: 'tp_name', width:150, title: false},
		    {label:'会员人数',name: 'vip_count', index: 'vip_count', width:100, title: false,align:'right'},
	    	{label:'人数占比',name: 'vip_count_rate', index: 'vip_count_rate', width:100, title: false,align:'right',sortable:false,formatter:handle.vipCountRateFmatter},
	    	{label:'消费人数',name: 'consumer_count', index: 'consumer_count', width:100, title: false,align:'right',sortable:false},
	    	{label:'消费人数占比',name: 'consumer_vip_rate', index: 'consumer_vip_rate', width: 100, title: false,align:'right',sortable:false,formatter:handle.consumerVipRateFmatter},
	    	{label:'消费金额',name: 'rebate_monery_sum', index: 'rebate_monery_sum', width:100, title: true,align:'right'},
	    	{label:'金额占比',name: 'monery_rate', index: 'monery_rate', width:100, title: false,align:'right',sortable:false,formatter:handle.moneryRateFmatter},
	    	{label:'成本',name: 'cost_monery_sum', index: 'cost_monery_sum', width:100, title: true,align:'right'},
	    	{label:'利润',name: 'profits_sum', index: 'profits_sum', width:100, title: true,align:'right'},
	    	{label:'利润占比',name: 'profits_rate', index: 'profits_rate', width:100, title: true,align:'right',sortable:false,formatter:handle.profitsRateFmatter}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-34,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				userdata: 'data.data',
				repeatitems : false,
				id: 'id'  //图标ID
			},
			loadComplete: function(data){
				/*THISPAGE.gridTotal();*/
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		return params;
	},
	reset:function(){
		$("#shop_name").val("");
		$("#vm_shop_code").val("");
		THISPAGE.$_date.setValue(3);
		dateRedioClick("theMonth");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();