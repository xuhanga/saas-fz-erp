var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var _limitMenu = system.MENULIMIT.VIP25;
var _menuParam = config.OPTIONLIMIT;
var isRefresh = false;
var queryurl = config.BASEPATH+"vip/report/vipage_analysis_list";

var Utils = {
	doQueryMemberType : function(){
		commonDia = $.dialog({
			title : '会员类别',
			content : 'url:'+config.BASEPATH+'vip/membertype/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_mt_code").val(selected.mt_code);
					$("#mt_name").val(selected.mt_name);
				}
			},
			cancel:true
		});
	},
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#vm_shop_code").val(selected.sp_code);
				$("#sp_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#vm_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_manager_code").val(selected.em_code);
					$("#vm_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	doTableModel: function (obj) {//刷新界面
    	if(obj == '0'){//柱形图模式
    		$("#gridDiv").css("display","none");
    		$("#container").css("display","block");
    		$("#CurrentMode").val("0");
    		Utils.ajaxQueryAnalysisColumn();
    	}else if(obj == '1'){//列表模式
    		$("#gridDiv").css("display","block");
    		$("#container").css("display","none");
    		$("#CurrentMode").val("1");
    		var reload=$("#gridReload").val();
    		if(reload!=""){
    		    THISPAGE.reloadData();
    		}else{
	    		THISPAGE.initGrid();
    		}
    	} 	
    },
    ajaxQueryAnalysisColumn:function(){
		$.ajax({
			type:"get",
			url:config.BASEPATH+"vip/report/vipage_analysis_column?" +THISPAGE.buildParams(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					eval("$('#container').highcharts({"+data.data+"})");
				}else{
					Public.tips({type: 1, content : '加载失败,请联系管理员!'});
				}
			}
		});
	}
}

var handle = {
	clickTag:function(ags_beg_age,ags_end_age){
		$("#ags_beg_age").val(ags_beg_age);
		$("#ags_end_age").val(ags_end_age);
		THISPAGE.reloadData();
	},
	formatBirthdayType:function(val, opt, row){//0公历生日1为农历生日
		if(val == "0"){
			return "公历";
		}else if(val == "1"){
			return "农历";
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		/*this.initGrid();*/
		this.initEvent();
	},
	initDom:function(){
		$("#gridReload").val("");
		Utils.doTableModel(0);
	},
	initGrid:function(){
		var gridWH = Public.setGrid();//操作    
		var colModel = [
		    {label:'会员卡号',name:'vm_cardcode',index:'vm_cardcode',align:'left',width:110,title:false,sortable:true}, 
		    {label:'姓名',name:'vm_name',index:'vm_name',align:'left',width:110,title:false,sortable:true}, 
		    {label:'手机号',name:'vm_mobile',index:'vm_mobile',align:'left',width:110,title:false,sortable:true}, 
		    {label:'会员类别',name:'mt_name',index:'mt_name',align:'left',width:110,title:false,sortable:true}, 
			{label:'性别',name:'vm_sex',index:'vm_sex',align:'center',width:50,title:false,sortable:true}, 
			{label:'生日方式',name: 'vm_birthday_type', index: 'vm_birthday_type',align:'center', width:60, title: true,sortable:true,formatter:handle.formatBirthdayType},
			{label:'公历生日',name: 'vm_birthday', index: 'vm_birthday',align:'center', width:100, title: true,sortable:true },
			{label:'农历生日',name: 'vm_lunar_birth', index: 'vm_lunar_birth',align:'center', width:100, title: true,sortable:true },
			{label:'年龄',name: 'vm_age', index: 'vm_age',align:'center', width:50, title: true,sortable:true },
		    {label:'总消费额',name:'vm_total_money',index:'vm_total_money',align:'right',width:110,title:false,sortable:true},
		    {label:'月消费额',name:'sh_sell_money',index:'sh_sell_money',align:'right',width:110,title:false,sortable:true},
		    {label:'发卡人',name:'vm_manager',index:'vm_manager',align:'left',width:80,title:false,sortable:true},
		    {label:'办卡店铺',name:'shop_name',index:'shop_name',align:'left',width:180,title:false,sortable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl +"?"+THISPAGE.buildParams(),
		//	loadonce:true,
			datatype: 'json',
			width: gridWH.w,
			height: gridWH.h - 200,
			altRows:true,
			gridview: true,
			onselectrow: false,
			cellEdit: false,
			colModel: colModel,
			rownumbers: true,//行号
			rownumWidth:50,
			pager: '#page',//分页
			viewrecords: true,
			pgbuttons:true,
			rowNum:100,//每页条数
			rowList:config.DATAROWLIST,//分页条数*/
			shrinkToFit:false,//表格是否自动填充
			//scroll: 1,//是否滚动
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			jsonReader: {
				root: 'data.list',
	            userdata: 'data.data',
				repeatitems : false,
				id:'vm_id'
			},
	        loadComplete: function (xhr) {
	        	$("#gridReload").val("reload");
	            var data=eval(xhr);
	            $("#headHtml").html(data.data.headData);
	            Public.resizeSpecifyGrid("grid", 120, 32);
	        },
	        loadError: function (xhr, status, error) {
	            Public.tips({type: 1, content: '操作失败了哦，服务可能已过期，请按F5刷新！'});
	        },
	        onSelectRow: function (rowId, status) {
	        },
	        onPaging: function (pgButton) {
	        }
		});
	},
	buildParams : function(){
		var params = '';
		params += 'vm_mt_code='+$("#vm_mt_code").val();
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_manager_code='+$("#vm_manager_code").val();
		params += '&ags_beg_age='+$("#ags_beg_age").val();
		params += '&ags_end_age='+$("#ags_end_age").val();
		return params;
	},
	reset:function(){
		$('#mt_name').val('');
		$('#vm_mt_code').val('');
		$('#sp_name').val('');
		$('#vm_shop_code').val('');
		$('#vm_manager').val('');
		$('#vm_manager_code').val('');
	},
	reloadData:function(data){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+params}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn_search').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			var model=$("#CurrentMode").val(); 
			Utils.doTableModel(model);
		});
			//查询
		$('#btn_ok').on('click', function(e){
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			};
			var model=$("#CurrentMode").val(); 
			Utils.doTableModel(model);
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_membertype").click(function(){
			Utils.doQueryMemberType();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid",111,32);
		});
	}
}
THISPAGE.init();
