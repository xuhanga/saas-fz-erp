var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.VIP04;
var _menuParam = config.OPTIONLIMIT;

var Utils = {
	doQueryShop : function(){
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'vipset'},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vb_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
					handle.getShopSmsSet(selected.sp_code);
				}
			},
			cancel:true
		});
	}
};
var regFloat=/^[0-9]+\.{0,1}[0-9]{0,2}$/;

function varlidaFloat(tagId,message){
	var value=$("#"+tagId).val();
	if (!regFloat.exec(value)){
		handle.showMessage(message,tagId,true);
		return false;
	}
	return true;
}
var handle = {
	smsWordNumber:function(){
		var haveNum=$("#vb_sms_content").val();
		$("#total").val(63-haveNum.length);
	},
	getShopSmsSet:function(shop_code){
		//加载对应店铺会员生日短信情况
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/set/getShopSmsSet",
			data:{"shop_code":shop_code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					 var vb_state=data.data.vb_state;
					 var vb_agowarn_day=data.data.vb_agowarn_day;
					 var vb_sms_content=data.data.vb_sms_content;
					 var num= 63 - vb_sms_content.length;
					 //加载成功，更改页面内容 
					 //判断那个选中。
					 if(vb_state==0){
						 $("input[type=radio][name='vb_state'][value=0]").attr("checked","checked");
					 }else{
						 $("input[type=radio][name='vb_state'][value=1]").attr("checked","checked");
					 }
					 $("#vb_agowarn_day").val(vb_agowarn_day); 
					 $("#vb_sms_content").val(vb_sms_content); 
					 $("#total").val(num);
					 if(vb_state==2){
						 Public.tips({type: 2, content : '该店铺未保存生日提醒设置！！！'});
					 }
				}else{
					Public.tips({type: 1, content : '加载错误，请刷新后重试!!!'});
				}
			}
		});
	},
	updateVipSet:function(){
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		var agowarn_day = $("#vb_agowarn_day").val();
		var sms_content = $("#vb_sms_content").val();
		var stateObj = document.getElementsByName("vb_state");
		var shop_code=$("#vb_shop_code").val();
		var stateVal = "";
		for(var i=0;i<stateObj.length;i++){
			if(stateObj[i].checked){
				stateVal = stateObj[i].value;
			}
		}
		if(agowarn_day==""){
			Public.tips({type: 2, content : '请输入提前提醒天数!'});
			$("#vb_agowarn_day").focus();
			return;
		}
		if(isNaN(agowarn_day)){
			Public.tips({type: 2, content : '天数只能为数字!'});
			$("#vb_agowarn_day").focus();
			return;
		}
		if(sms_content==""){
			Public.tips({type: 2, content : '请输入短信内容!'});
			$("#vb_sms_content").focus();
			return;
		}
		var rts_secondsObj = document.getElementsByName("rts_second");
		var rts_seconds = "";
		for(var i=0;i<rts_secondsObj.length;i++){
			rts_seconds += rts_secondsObj[i].value+",";
		}
		if(rts_seconds!=""){
			rts_seconds = rts_seconds.substring(0,rts_seconds.length-1);
		}
		$.dialog.confirm("确定要保存吗？", function(){
			$.ajax({
				type:"post",
				url:config.BASEPATH+"vip/set/update?rts_seconds="+rts_seconds,
				data:encodeURI(encodeURI($('#form1').serialize())),
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '保存成功!!!'});
					}else{
						Public.tips({type: 1, content : '保存失败，请联系管理员!!!'});
					}
				}
			});
		});
	},
	delTableRowNoSave:function(obj){
		if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
			return ;
		}
		var tr=obj.parentNode.parentNode;  
		var tbody=tr.parentNode;
		$.dialog.confirm("确定要删除吗？", function(){
			tbody.removeChild(tr);
		});   
	},
	delTableRow:function(obj,id){
		if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
			return ;
		}
		var tr=obj.parentNode.parentNode;  
		var tbody=tr.parentNode;
		$.dialog.confirm("确定要删除吗？", function(){
			$.ajax({
				async:false,
				type:"post",
				url:config.BASEPATH+"vip/set/delReturnSet",
				data:{"rts_id":id},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						tbody.removeChild(tr);
						window.location.replace(config.BASEPATH+"vip/set/to_update");
					}else{
						Public.tips({type: 1, content : '删除失败,请重试!'});
					}
				}
			});
		});   
	},
	/*设置消费回访，最多支持5次，每次天数不同！*/
	returnSet:function(){
		tempRow=vipReturn.rows.length;
		if(tempRow>=5){
			Public.tips({type: 1, content : '回访最多支持5次！'});
			return;
		}
		//allDaysArray 如果存在相同的日期，则不允许保存
		var allDaysArray=$("input[name='allDaysArray']").val();
		
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		$.dialog({
			title : '添加回访设置',
			content : 'url:'+config.BASEPATH+'vip/set/to_add_returnset',
			data: {callback: this.callback},
			width:350,
			height:140,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				window.location.replace(config.BASEPATH+"vip/set/to_update");
			}
		});
	},
	/*设置会员年龄段，最多支持5次*/
	ageGroupSet:function(){
		tempRow=vipAgeGroup.rows.length;
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		$.dialog({
			title : '添加年龄段设置',
			content : 'url:'+config.BASEPATH+'vip/set/to_add_agegroupset',
			data: {callback: this.callback},
			width:350,
			height:140,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				window.location.replace(config.BASEPATH+"vip/set/to_update");
			}
		});
	},
	updateAgeGroupSet:function(obj,id){
		if (!Business.verifyRight(_limitMenu,_menuParam.UPDATE)) {
			return ;
		}
		$.dialog({
			title : '修改年龄段设置',
			content : 'url:'+config.BASEPATH+'vip/set/to_update_agegroupset?ags_id='+id,
			data: {callback: this.callback},
			width:350,
			height:140,
			max : false,
			min : false,
			cache : false,
			lock: true,
			close:function(){
				window.location.replace(config.BASEPATH+"vip/set/to_update");
			}
		});
	},
	delAgeGroupRow:function(obj,id){
		if (!Business.verifyRight(_limitMenu,_menuParam.DELETE)) {
			return ;
		}
		var tr=obj.parentNode.parentNode;  
		var tbody=tr.parentNode;
		$.dialog.confirm("确定要删除吗？", function(){
			$.ajax({
				async:false,
				type:"post",
				url:config.BASEPATH+"vip/set/delAgeGroupSet",
				data:{"ags_id":id},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						tbody.removeChild(tr);
						/*window.location.replace(config.BASEPATH+"vip/set/to_update");*/
					}else{
						Public.tips({type: 1, content : '删除失败,请重试!'});
					}
				}
			});
		});   
	},
	lowerOnblur:function(lowerId,upperId,remarkId,nameId){
		if (!varlidaFloat(lowerId,"请正确输入消费下限！")){
			return;
		}
		var lower = $("#"+lowerId).val();
		var upper = $("#"+upperId).val();
		var name = $("#"+nameId).val();
		if (parseFloat(lower)>=parseFloat(upper)){
			handle.showMessage("下限不能大于或等于上限值！",upperId,true);
			return;
		}
		$("#"+remarkId).val("周期消费大于或等于"+lower+"小于"+upper+"为"+name+"会员");
	},
	upperOnblur:function(lowerId,upperId,remarkId,nameId){
		if (!varlidaFloat(upperId,"请正确输入消费上限！")){
			return;
		}
		var lower = $("#"+lowerId).val();
		var upper = $("#"+upperId).val();
		var name = $("#"+nameId).val();
		if (parseFloat(lower)>=parseFloat(upper)){
			handle.showMessage("下限不能大于或等于上限值！",upperId,true);
			return;
		}
		$("#"+remarkId).val("周期消费大于或等于"+lower+"小于"+upper+"为"+name+"会员");
	},
	showMessage:function(message,focusId,isSelect){
		if(focusId!=null){
			if (isSelect){
				$("#"+focusId).select();
			}
			$("#"+focusId).focus();
		}
		Public.tips({type: 1, content : message});
	},
	clickColor:function(obj){
		$(obj).soColorPacker({
			size:3
			,colorChange:2
			,callback:function(){
				$(obj).css("color",$(obj).val());
			}
		});
	},
	doCheckNumber:function(obj){
		var discount = obj.value;
	    if(isNaN(discount)){
	    	Public.tips({type: 1, content : '请输入数字!'});
		    obj.value="";
		    obj.focus();
		    return;
	    }
    }
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		
	},
	initEvent:function(){
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
	}
}
THISPAGE.init();