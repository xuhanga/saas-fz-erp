var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.VIP24;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/visit/detail_page';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vi_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vi_manager_code").val(selected.em_code);
					$("#vi_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	initIsArrive:function(){
		isArriveInfo = {
			data:{items:[
			        {is_arrive_code:'',is_arrive_name:'全部'},
					{is_arrive_code:'1',is_arrive_name:'不确定到店'},
					{is_arrive_code:'2',is_arrive_name:'确认到店'},
					{is_arrive_code:'3',is_arrive_name:'确认不到店'}
			]}
		}
		isArriveCombo = $('#span_is_arrive').combo({
			data:isArriveInfo.data.items,
			value: 'is_arrive_code',
			text: 'is_arrive_name',
			width : 204,
			height: 300,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#vi_is_arrive").val(data.is_arrive_code);
				}
			}
		}).getCombo();
	},
	initWay:function(){
		wayInfo = {
			data:{items:[
			        {way_code:'',way_name:'全部'},
					{way_code:'1',way_name:'电话回访'},
					{way_code:'2',way_name:'短信回访'},
					{way_code:'3',way_name:'微信回访'},
					{way_code:'4',way_name:'推送优惠券'}
			]}
		}
		wayCombo = $('#span_way').combo({
			data:wayInfo.data.items,
			value: 'way_code',
			text: 'way_name',
			width : 204,
			height: 300,
			listId:'',
			defaultSelected: 0,
			editable: true,
			callback:{
				onChange: function(data){
					$("#vi_way").val(data.way_code);
				}
			}
		}).getCombo();
	},
	formatType:function(val, opt, row){//1:会员回访 2:生日回访 3:消费回访
		if(val == "1"){
			return "会员回访";
		}else if(val == "2"){
			return "生日回访";
		}else if(val == "3"){
			return "消费回访";
		}else{
			return "";
		}
	},
	formatWay:function(val, opt, row){//1:电话回访 2:短信回访 3:微信回访 4:推送优惠券
		if(val == "1"){
			return "电话回访";
		}else if(val == "2"){
			return "短信回访";
		}else if(val == "3"){
			return "微信回访";
		}else if(val == "4"){
			return "推送优惠券";
		}else{
			return "";
		}
	},
	formatIsArrive:function(val, opt, row){//1:不确定到店 2:确认到店 3:确认不到店
		if(val == "1"){
			return "不确定到店";
		}else if(val == "2"){
			return "确认到店";
		}else if(val == "3"){
			return "确认不到店";
		}else{
			return "";
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		$('#theMonth').click();
		handle.initIsArrive();
		handle.initWay();
	},
	initGrid:function(){
		var gridWH = Public.setGrid();
		var colModel = [
			{name: 'vi_id',label:'', index: 'vi_id',width: 120, hidden:true},
	    	{name: 'vi_vm_cardcode',label:'卡号',index: 'vi_vm_cardcode',width:100},
	    	{name: 'vi_vm_name',label:'姓名',index: 'vi_vm_name',width:75},
	    	{name: 'vi_vm_mobile',label:'手机',index: 'vi_vm_mobile',width:96},
	    	{name: 'vi_manager',label:'回访人',index:'vi_manager',width: 60},
	    	{name: 'vi_type',label:'类型',index: 'vi_type', width:80, title: false,align:'center',sortable:false,formatter:handle.formatType},
	    	{name: 'vi_way',label:'形式', index: 'vi_way', width: 80, title: false,align:'center',formatter:handle.formatWay},
	    	{name: 'vi_visit_date',label:'日期',index: 'vi_visit_date', width:140, title: false,align:'right',formatter:date},
	    	{name: 'vi_content',label:'回访内容',index: 'vi_content', width:400, title: true},
	    	{name: 'vi_is_arrive',label:'是否到店',index: 'vi_is_arrive', width:80, title: true,formatter:handle.formatIsArrive},
	    	{name: 'vi_date',label:'到店日期/联系日期',index: 'vi_date', width:120, title: true,align:'center'},
	    	{name: 'vi_remark',label:'注意事项/问题反馈',index: 'vi_remark', width:200, title: true}
	    ];
		$('#grid').jqGrid({
            url:queryurl+"?"+THISPAGE.buildParams(),			
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-40,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
//                userdata: 'data.data',
				repeatitems : false,
				id: 'vi_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&vm_cardcode='+Public.encodeURI($("#vm_cardcode").val());
		params += '&vm_mobile='+Public.encodeURI($("#vm_mobile").val());
		params += '&vi_shop_code='+$("#vi_shop_code").val();
		params += '&vi_manager_code='+$("#vi_manager_code").val();
		params += '&vi_way='+$("#vi_way").val();
		params += '&vi_is_arrive='+$("#vi_is_arrive").val();
		return params;
	},
	reset:function(){
		THISPAGE.$_date.setValue(0);
		$('#theMonth').click();
		$("#vm_cardcode").val("");
		$("#vm_mobile").val("");
		$("#sp_name").val("");
		$("#vi_shop_code").val("");
		$("#vi_manager").val("");
		$("#vi_manager_code").val("");
		isArriveCombo.loadData(isArriveInfo.data.items,[0]);
		wayCombo.loadData(wayInfo.data.items,[0]);
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
		
	}
}
THISPAGE.init();