var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var isRefresh = false;
var _limitMenu = system.MENULIMIT.VIP21;
var _menuParam = config.OPTIONLIMIT;
var queryurl = config.BASEPATH+'vip/visit/page?';

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
			var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#card_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#card_manager_code").val(selected.em_code);
					$("#card_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	initVisit:function(){
		visitInfo = {
			data:{items:[
				{visit_code:'',visit_name:'全部'},
				{visit_code:'0',visit_name:'未回访'},
				{visit_code:'1',visit_name:'已回访'}
			]}
		}
		visitCombo = $('#span_visit').combo({
			data:visitInfo.data.items,
			value: 'visit_code',
			text: 'visit_name',
			width : 195,
			height: 300,
			listId:'',
			defaultSelected: 1,
			editable: true,
			callback:{
				onChange: function(data){
					$("#visit").val(data.visit_code);
				}
			}
		}).getCombo();
	},
	operFmatter:function(val, opt, row) {
		var html_con = "<div class='operating' data-id='" + row.vm_id + "'>";
		if(row.vm_id != ""){
			html_con += "<i class='iconfont i-hand' title='回访' onclick=\"handle.consumeVisit('"+row.vm_id+"','"+row.vm_cardcode+"')\" >&#xe69b;</i>";
		}else{
			html_con += "<span class='ui-icon' style='background:none;'></span><span class='ui-icon' style='background:none;'></span>";
		}
		html_con += "</div>";
		return html_con;
	},
	doChangeMode:function (obj, day){
		$("#rts_day").val(day);
		$(obj).siblings().removeClass();
		$(obj).siblings().addClass("");
		$(obj).addClass("selted");
		THISPAGE.reloadData();
	},
	consumeVisit:function(vm_id, vm_cardcode){//消费回访
		if (!Business.verifyRight(_limitMenu,_menuParam.INSERT)) {
			return ;
		}
		$.dialog({ 
		   	title:'会员回访',
		   	//data:{vi_type:3},//1:会员回访 2:生日回访 3:消费回访
		   	max: false,
		   	min: false,
		   	width:900,height:450,
		   	fixed:false,
		   	drag: true,
		   	resize:false,
		   	content:'url:'+config.BASEPATH+'vip/member/vip_visit_analysis?vm_id='+vm_id+'&vi_type=3&vm_cardcode='+vm_cardcode,
		   	lock:true,
		   	close: function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
		   			isRefresh = false;
		   		}
		   	}
	    });
	}
	/*// 批量发送短信
	toSendSMSMass: function(rowIds){
		var vals = "";
		var smsTip = "";
		var addMark = 0;
		var totalCount = 0;
		if(rowIds != null && rowIds != ''){
			if(rowIds instanceof Array){
				totalCount = rowIds.length;
				for(var i = rowIds.length-1; i>=0;i--){
					var rowData = $("#grid").jqGrid("getRowData", rowIds[i]);
					if( isMobilePhone(rowData.MI_Mobile) ){
						addMark++;
						vals += rowData.MI_Mobile + ",";
					}
				}
			} else {
				totalCount++;
				var rowData = $("#grid").jqGrid("getRowData", rowIds);
				if( isMobilePhone(rowData.MI_Mobile) ){
					addMark++;
					vals += rowData.MI_Mobile + ",";
				}					
			}
			if( addMark == 0 ){
				$.dialog.tips("您所选择会员的手机号码格式不正确，无法发送短信！",2,"32X32/fail.png");
				return;
			}
			vals = vals.substring(0, vals.length-1);
			smsTip = "（注：您共选择"+totalCount+"条数据，其中"+addMark+"条是手机号码）";
			
			$.dialog({
			   	id:'vipSendSMSMass',
			   	title:'会员短信',
			   	data:{mobileData:vals , smsTip:smsTip},
			   	max: false,
			   	min: false,
			   	lock:true,
			   	width:550,
			   	height:200,
			   	top:100,
			   	left:250,
			   	drag: true,
			   	resize:false,
			   	fixed:false,
			   	content:'url:'+CONFIG.BASEPATH+'pages/vip/SendSMSRetailServiceMass.jsp',
			   	close: function(){
			   		vals="";
			   		smsTip = "";
			   	}
		    });
		} else {
			$.dialog.tips("请选择要发送短信的会员！",2,"32X32/fail.png");
		}
	},
	callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} else {
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
			dialogWin && dialogWin.api.zindex();
		}
	}*/
};

/*
//会员回访
function doVipVisitAnalysis(mi_cardCode){
	$.dialog({ 
	   	id:'doVipVisitAnalysis',
	   	title:'会员回访',
	   	data:{vitype:3},//1:会员回访 2:生日回访 3:消费回访
	   	max: false,
	   	min: false,
	   	width:900,height:450,
	   	fixed:false,
	   	drag: true,
	   	resize:false,
	   	content:'url:'+CONFIG.BASEPATH+'vip/doVipVisitAnalysis.action?mi_cardCode='+mi_cardCode,
	   	lock:true,
	   	close: true
    });
}
*/
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
	    this.initEvent();
	    /*this.initselectVisit();*/
	},
	initDom:function(){
		handle.initVisit();
	},
	initGrid:function(){ 
		var gridWH = Public.setGrid();
		var colModel = [
            {label:'操作', name: 'operate', index: 'operate', formatter: handle.operFmatter, width: 40, align:'center'},
            {label:'', name: 'vm_id', index: 'vm_id',width: 120, hidden:true},
	    	{label:'卡号', name: 'vm_cardcode', index: 'vm_cardcode',width: 120, fixed:true},
	    	{label:'姓名', name: 'vm_name', index: 'vm_name', width: 70,fixed:true ,title: false},
	    	{label:'类别', name: 'mt_name', index: 'mt_name', width: 60, title: false},
	    	{label:'手机', name: 'vm_mobile', index: 'vm_mobile', width: 95,align:'center'},
	    	{label:'消费金额', name: 'sh_sell_money', index: 'sh_sell_money', width: 60,align:'right'},
	    	{label:'消费日期', name: 'sh_sysdate', index: 'sh_sysdate', width: 140,align:'center'},
	    	{label:'总计积分', name: 'vm_points', index: 'vm_points', width: 80,align:'right'} ,
	    	{label:'剩余积分', name: 'vm_last_points', index: 'vm_last_points', width: 80,align:'right'} ,	    	
	    	{label:'最近回访日期', name: 'vi_visit_date', index: 'vi_visit_date', width: 100,align:'center'},
	    	{label:'回访人员', name: 'vi_manager', index: 'vi_manager', width: 80},
	    	{label:'发卡人', name: 'vm_manager', index: 'vm_manager', width: 70},
	    	{label:'发卡店铺', name: 'shop_name', index: 'shop_name', width: 100}
	    ];
		$('#grid').jqGrid({
			url:queryurl+THISPAGE.buildParams(),
			datatype: 'json',
			width: gridWH.w-12,
			height: gridWH.h-40,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:true,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata: 'data.data',
				repeatitems : false,
				id: 'vm_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				
			},
			onPaging: function(pgButton) {
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'card_manager_code='+$("#card_manager_code").val();
		params += '&begin_buy_money='+$("#begin_buy_money").val();
		params += '&end_buy_money='+$("#end_buy_money").val();
		params += '&card_code='+Public.encodeURI($("#card_code").val());
		params += '&rts_day='+$("#rts_day").val();
		params += '&visit='+$("#visit").val();
		params += '&card_shop_code='+$("#card_shop_code").val();
		return params;
	},
	reset:function(){
		$("#sp_name").val("");
		$("#card_shop_code").val("");
		$("#card_manager").val("");
		$("#card_manager_code").val("");
		$("#card_code").val("");
		$("#begin_buy_money").val("");
		$("#end_buy_money").val("");
		$("#visit").val("0");
		visitCombo.loadData(visitInfo.data.items,[1]);
	},
	reloadData:function(){
		var params=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+params}).trigger("reloadGrid");
	},
	
	initEvent:function(){
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn_emp").click(function(){
			Utils.doQueryEmp();
		});
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			if (!Business.verifyRight(_limitMenu,_menuParam.SELECT)) {
				return ;
			}
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		/*//导出Excel
		$('#btn-export').on('click',function(){	
			doExcel();
		});
		//批量发送短信
		$('#btn-sendSMS').on('click', function(e){
			e.preventDefault();
			var rowIds = $('#grid').jqGrid('getGridParam','selarrrow');
			handle.toSendSMSMass(rowIds);
		});
		
		$(window).resize(function(){
			Public.resizeSpecifyGrid("grid", 110, 32);
		})*/
	}
}

/*
1.发送短信
function toSendSMS(vipMobile,vipName){
	if( !isMobilePhone(vipMobile) ){
		$.dialog.tips("["+vipMobile+"]不是手机号码，无法发送短信！",2,"32X32/fail.png");
		return;
	}
	$.dialog({
	   	id:'vipSendSMS',
	   	title:'消费回访短信',
	   	data:{vip_name:vipName,vip_mobile:vipMobile},
	   	max: false,
	   	min: false,
	   	lock:true,
	   	width:550,
	   	height:230,
	   	top:100,
	   	left:250,
	   	drag: true,
	   	resize:false,
	   	fixed:false,
	   	content:'url:'+CONFIG.BASEPATH+'sms/querySMSBalance.action',
	   	close: function(){
	   	}
    });
}
//2.协助分析
function assistAnalysis(mi_cardCode){
	if (!Business.verifyRight($_limitMenu,$_menuParam.SELECT)) {
//		$.dialog.tips("很抱歉您没有此权限，请联系管理员！",2,"32X32/fail.png");
		return;
	};
	$.dialog({ 
		   //id设置用来区别唯一性 防止div重复
		   	id:'vip_AssistAnalysis',
		   	title:'辅助分析',
		   	//设置最大化最小化按钮(false：隐藏 true:关闭)
		   	max: false,
		   	min: false,
		   	//设置长宽
		   	width:900,height:535,
		   	fixed:false,
		   	//设置是否拖拽(false:禁止，true可以移动)
		   	drag: true,
		   	resize:false,
		   	//设置页面加载处理
		   	content:'url:'+CONFIG.BASEPATH+'vip/queryVipAssistAnalysis.action?mi_cardCode='+mi_cardCode,
		   	lock:true,
		   	close: true
	    });
}
导出excel
function doExcel()
{
	if (!Business.verifyRight($_limitMenu,$_menuParam.EXPORT)) {
	//	$.dialog.tips("很抱歉您没有此权限，请联系管理员！",2,"32X32/fail.png");
		return;
	};
	var params = buildUrlParams();
    var sidx = $('#grid').getGridParam("sortname");
    var sord = $('#grid').getGridParam("sortorder");
    params+="&sidx="+sidx+"&sord="+sord;
	window.location.replace(CONFIG.BASEPATH+"vip/vipConsumeVisitExcel.action?"+params);
}*/

THISPAGE.init();