var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var wt_type = $("#wt_type").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	doQueryProductType : function(){
		commonDia = $.dialog({
			title : '选择商品分类',
			content : 'url:'+config.BASEPATH+'wx/producttype/to_tree_dialog',
			data : {multiselect:false},
			width : 360,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					Public.tips({type: 2, content : "请选择商品类别！"});
					return false;
				}
				if (selected.pt_code == "0") {
					Public.tips({type: 2, content : "请选择具体商品类别！"});
					return false;
				}
				$("#wp_pt_code").val(selected.pt_code);
				$("#pt_name").val(selected.pt_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					if (selected.pt_code == "0") {
						Public.tips({type: 2, content : "请选择具体商品类别！"});
						return false;
					}
					$("#wp_pt_code").val(selected.pt_code);
					$("#pt_name").val(selected.pt_name);
				}
			},
			cancel:true
		});
	},
	doQueryProperty : function(){
		commonDia = $.dialog({
			title : '选择商品属性',
			content : 'url:'+config.BASEPATH+'common/dict/to_list_dialog',
			data : {multiselect:true,dt_type:'PRODUCT_PROPERTY'},
			width : 380,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					Public.tips({type: 2, content : "请选择商品属性！"});
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].dt_code);
					names.push(selected[i].dt_name);
				}
				$("#wp_property_code").val(codes.join(","));
				$("#property_name").val(names.join(","));
				return true;
			},
			close:function(){
			},
			cancel:true
		});
	},
	doQueryPromise : function(){
		commonDia = $.dialog({
			title : '选择售后承诺',
			content : 'url:'+config.BASEPATH+'common/dict/to_list_dialog',
			data : {multiselect:true,dt_type:'PRODUCT_PROMISE'},
			width : 380,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					Public.tips({type: 2, content : "请选择商品属性！"});
					return false;
				}
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].dt_code);
					names.push(selected[i].dt_name);
				}
				$("#wp_promise_code").val(codes.join(","));
				$("#promise_name").val(names.join(","));
				return true;
			},
			close:function(){
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
		var product = {};
		product.wp_id = $("#wp_id").val();
		product.wp_number = $("#wp_number").val();
		product.wp_pd_name = $("#wp_pd_name").val();
		product.wp_pt_code = $("#wp_pt_code").val();
		product.wp_property_code = $("#wp_property_code").val();
		product.wp_promise_code = $("#wp_promise_code").val();
		product.wp_type = $("#wp_type").val();
		product.wp_virtual_amount = $("#wp_virtual_amount").val();
		product.wp_buy_limit = $("#wp_buy_limit").val();
		product.wp_weight = $("#wp_weight").val();
		product.wp_sell_price = $("#wp_sell_price").val();
		product.wp_rate_price = $("#wp_rate_price").val();
		product.wp_subtitle = $("#wp_subtitle").val();
		var wp_istry = $("#wp_istry").val();
		var wp_isbuy = $("#wp_isbuy").val();
		var wp_try_way = '';
		if(wp_istry == '1'){//允许试穿-获得试穿取货方式
			if(THISPAGE.$_wp_try_way_0.chkVal().join()){
				wp_try_way += '0,';
			}
			if(THISPAGE.$_wp_try_way_1.chkVal().join()){
				wp_try_way += '1,';
			}
			if(wp_try_way.length > 0){
				wp_try_way = wp_try_way.substring(0, wp_try_way.length-1);
	    	}else{
	    		Public.tips({type: 2, content : '请选择试穿方式！'});
				return;
	    	}
		}
		var wp_buy_way = '';
		var wp_postfree = '';
		if(wp_isbuy == '1'){//允许购买-获得购买取货方式
			if(THISPAGE.$_wp_buy_way_0.chkVal().join()){
				wp_buy_way += '0,';
				if(THISPAGE.$_wp_postfree.chkVal().join()){
					wp_postfree = '1';
				}else{
					wp_postfree = '0';
				}
			}
			if(THISPAGE.$_wp_buy_way_1.chkVal().join()){
				wp_buy_way += '1,';
			}
			if(wp_buy_way.length > 0){
				wp_buy_way = wp_buy_way.substring(0, wp_buy_way.length-1);
	    	}else{
	    		Public.tips({type: 2, content : '请选择购买取货方式！'});
				return;
	    	}
		}
		product.wp_istry = wp_istry;
		product.wp_try_way = wp_try_way;
		product.wp_isbuy = wp_isbuy;
		product.wp_buy_way = wp_buy_way;
		product.wp_postfree = wp_postfree;
		product.wp_desc = wp_desc.getContent();
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"wx/product/update",
			data:{product:Public.encodeURI(JSON.stringify(product))},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '修改成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		if(callback && typeof callback == 'function'){
			callback({},oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		this.selectRow={};
		this.$_wp_type = $("#td_wp_type").cssRadio({ callback: function($_obj){
			$("#wp_type").val($_obj.find("input").val());
		}});
		this.$_wp_istry = $("#td_wp_istry").cssRadio({ callback: function($_obj){
			var wp_istry = $_obj.find("input").val();
			$("#wp_istry").val(wp_istry);
			if(wp_istry == '0'){
				$("#wp_try_way_0").hide();
				$("#wp_try_way_1").hide();
			}else if(wp_istry == '1'){
				$("#wp_try_way_0").show();
				$("#wp_try_way_1").show();
			}
		}});
		this.$_wp_try_way_0 = $("#wp_try_way_0").cssCheckbox({ callback: function($_obj){
        	if($_obj.find("input")[0].checked){//选中
        	}else{//取消
        	}
		}});
		this.$_wp_try_way_1 = $("#wp_try_way_1").cssCheckbox({ callback: function($_obj){
        	if($_obj.find("input")[0].checked){//选中
        	}else{//取消
        	}
		}});
		this.$_wp_isbuy = $("#td_wp_isbuy").cssRadio({ callback: function($_obj){
			var wp_isbuy = $_obj.find("input").val();
			$("#wp_isbuy").val(wp_isbuy);
			if(wp_isbuy == '0'){
				$("#wp_buy_way_0").hide();
				$("#wp_buy_way_1").hide();
				$("#wp_postfree").hide();
			}else if(wp_isbuy == '1'){
				$("#wp_buy_way_0").show();
				$("#wp_buy_way_1").show();
				if(THISPAGE.$_wp_buy_way_0.chkVal().join()){
					$("#wp_postfree").show();
				}
			}
		}});
		this.$_wp_buy_way_0 = $("#wp_buy_way_0").cssCheckbox({ callback: function($_obj){
        	if($_obj.find("input")[0].checked){//选中
        		$("#wp_postfree").show();
        	}else{//取消
        		$("#wp_postfree").hide();
        	}
		}});
		this.$_wp_buy_way_1 = $("#wp_buy_way_1").cssCheckbox({ callback: function($_obj){
        	if($_obj.find("input")[0].checked){//选中
        	}else{//取消
        	}
		}});
		this.$_wp_postfree = $("#wp_postfree").cssCheckbox({ callback: function($_obj){
			if($_obj.find("input")[0].checked){//选中
			}else{//取消
			}
		}});
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();