var nowDayOfWeek = new Date().getDay();         //今天本周的第几天     
var nowDay = new Date().getDate();              //当前日     
var nowMonth = new Date().getMonth();           //当前月     
var nowYear = new Date().getFullYear(); 
function dateRedioClick(redioId){
	var id = redioId;
	if(undefined != document.getElementById(id)){
		var v = document.getElementById(id).value;
		var temp = document.getElementById("searDate");
		if(temp!='null' && temp!=null){
			document.getElementById("searDate").value = v;
		}
	}
	var date=new Date();                 //当前日期     
	if(id=='theDate'){
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(date);
	}
	if(id=='yesterday'){
		date.setDate(date.getDate() - 1);
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(date);
	}
	if(id=='theWeek'){
    	var WeekFirstDay = getWeekStartDate();
    	var WeekLastDay = getWeekEndDate();
    	
		document.getElementById("begindate").value = WeekFirstDay;
		document.getElementById("enddate").value = WeekLastDay;
	}
	if(id=='theMonth'){
        var	star_date = getMonthStartDate();
        var end_Date = getMonthEndDate();
		document.getElementById("begindate").value = star_date;
		document.getElementById("enddate").value = end_Date;
	}
	if(id=='theSeason'){
		var star_season = getQuarterStartDate();
		var end_season = getQuarterEndDate();
		document.getElementById("begindate").value = star_season;
		document.getElementById("enddate").value = end_season;
	}
	if(id=='theYear'){
		document.getElementById("begindate").value = nowYear+"-"+"01-01";
		document.getElementById("enddate").value = nowYear+"-"+"12-31";
	}
	if(id=='lastYear'){
		document.getElementById("begindate").value = (nowYear-1)+ "-"+"01-01";
		document.getElementById("enddate").value = (nowYear-1)+ "-"+"12-31";
	}
	if(id=='oneMonth'){
		var date = new Date();
		date.setMonth(date.getMonth()-1);
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(new Date());
	}
	if(id=='twoMonth'){
		var date = new Date();
		date.setMonth(date.getMonth()-2);
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(new Date());
	}
	if(id=='threeMonth'){
		var date = new Date();
		date.setMonth(date.getMonth()-3);
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(new Date());
	}
	if(id=='sixMonth'){
		var date = new Date();
		date.setMonth(date.getMonth()-6);
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(new Date());
	}
}
function dateRedioClickFromSelect(redioId){
	var id = redioId;
	var date=new Date();                 //当前日期     
	if(id=='theDate'){
		document.getElementById("begindate").value = DateToFullDateTimeString(date);
		document.getElementById("enddate").value = DateToFullDateTimeString(date);
	}
	if(id=='theWeek'){
    	var WeekFirstDay = getWeekStartDate();
    	var WeekLastDay = getWeekEndDate();
    	
		document.getElementById("begindate").value = WeekFirstDay;
		document.getElementById("enddate").value = WeekLastDay;
	}
	if(id=='theMonth'){
        var	star_date = getMonthStartDate();
        var end_Date = getMonthEndDate();
		document.getElementById("begindate").value = star_date;
		document.getElementById("enddate").value = end_Date;
	}
}
//获得本周的结束日期     
function getWeekEndDate() {      
    var weekEndDate = new Date(nowYear, nowMonth, nowDay + (6 - nowDayOfWeek));      
    return DateToFullDateTimeString(weekEndDate);     
}
//获得本周的开始日期     
function getWeekStartDate() {      
    var weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek);      
    return DateToFullDateTimeString(weekStartDate);     
}   
//获得本月的开始日期    
    function getMonthStartDate(){    
        var monthStartDate = new Date(nowYear, nowMonth, 1);     
        return DateToFullDateTimeString(monthStartDate);    
    }    
    //获得本月的结束日期    
    function getMonthEndDate(){    
        var monthEndDate = new Date(nowYear, nowMonth, getMonthDays(nowMonth));     
        return DateToFullDateTimeString(monthEndDate);    
    }     
//获得某月的天数    
function getMonthDays(myMonth){    
    var monthStartDate = new Date(nowYear, myMonth, 1);     
    var monthEndDate = new Date(nowYear, myMonth + 1, 1);     
    var   days   =   (monthEndDate   -   monthStartDate)/(1000   *   60   *   60   *   24);     
    return   days;     
}     
//获得本季度的开始月份     
function getQuarterStartMonth() {  
    var quarterStartMonth = 0;  
    if (nowMonth < 3) {  
        quarterStartMonth = 0;  
    }  
    if (2 < nowMonth && nowMonth < 6) {  
        quarterStartMonth = 3;  
    }  
    if (5 < nowMonth && nowMonth < 9) {  
        quarterStartMonth = 6;  
    }  
    if (nowMonth > 8) {  
        quarterStartMonth = 9;  
    }  
    return quarterStartMonth;   
}     
//获得本季度的开始日期     
function getQuarterStartDate() {  
    var quarterStartDate = new Date(nowYear, getQuarterStartMonth(), 1);  
    return DateToFullDateTimeString(quarterStartDate);  
}     
//获的本季度的结束日期     
function getQuarterEndDate() {  
    var quarterEndMonth = getQuarterStartMonth() + 2;  
    var quarterStartDate = new Date(nowYear, quarterEndMonth, getMonthDays(quarterEndMonth));  
    return DateToFullDateTimeString(quarterStartDate);  
}  
//获得某月的天数     
function getMonthDays(myMonth) {  
    var monthStartDate = new Date(nowYear, myMonth, 1);  
    var monthEndDate = new Date(nowYear, myMonth + 1, 1);  
    var days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24);  
    return days;
}  
//js日期格式化函数
function DateToFullDateTimeString(date)  
{   
    var year = date.getFullYear();  
    var month = date.getMonth();  
    var day = date.getDate();  
    var hour = date.getHours();  
    var minute = date.getMinutes();  
    var second = date.getSeconds();  
    var datestr;  
    if(month >= 9){
    	month+=1;
    }
    if (month <9)  
    {  
        month = '0' + (month + 1);  
    }  
    if (day < 10)  
    {  
        day = '0' + day;  
    }  
    if (hour < 10)  
    {  
        hour = '0' + hour;  
    }  
    if (minute < 10)  
    {  
        minute = '0' + minute;  
    }  
    if (second < 10)  
    {  
        second = '0' + second;  
    }  
    datestr = year + '-' + month + '-' + day;
    return datestr;  
}