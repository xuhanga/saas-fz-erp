var LOCK ={
	isRightBrowser:function(){
		var browser = DetectBrowser();
		if (browser == "Unknown") {
			return false;
		}
        return true;
	},
	hadInstallEncrypt:function(){
		var create = DetectIA300Plugin();//DetectActiveX() 判断IA300Clinet是否安装
		if (!create) {
			return false;
		}
        return true;
	},
	writeContent:function(datas){
		datas = this.fullLengthStr(datas, 18);
		//写用户存储区
		var rtn = IA300_WriteUserStorageEx(1, datas);
        return rtn;
	},
	readContent:function(){
		//读用户存储区
		return IA300_ReadUserStorageEx(1, 18);
	},
	login:function(pwd){
		var retVal = IA300_CheckPassword(pwd);
		if (retVal != 0) {
			return false;
		} else {
			//获取本Key的唯一硬件ID
			var uid = IA300_GetHardwareId();
			if (uid == "") {
				return false;
			}
		}
		return true;
	},
	fullLengthStr:function(str,len){
		if(str.length < len){
			var space="";
			for(i=0;i<len-str.length;i++){
				space += " ";
			}
		}
		return str+space;
	}
}