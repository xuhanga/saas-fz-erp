package zy.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import zy.entity.JsonBean;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;
import zy.util.StringUtil;

public class BaseController {
	protected final int SUCCESS = 200;
	protected final int EXISTED = 304;
	protected final int FAIL = 500;
	protected int pagesize = 10;

	protected JsonBean returnResult(Object data) {
		JsonBean jsonBean = new JsonBean();
		if (null != data) {
			jsonBean.setStat(SUCCESS);
			jsonBean.setData(data);
		} else {
			jsonBean.setStat(FAIL);
			jsonBean.setData("");
		}
		return jsonBean;
	}

	protected JsonBean success(int flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean success(String flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean fail(int flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean fail(String flag) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean result(int flag) {
		JsonBean jsonBean = new JsonBean();
		if (SUCCESS == flag) {
			jsonBean.setStat(SUCCESS);
			jsonBean.setMessage("保存成功!");
		} else if (EXISTED == flag) {
			jsonBean.setStat(EXISTED);
			jsonBean.setMessage("数据已存在!");
		} else {
			jsonBean.setStat(FAIL);
			jsonBean.setMessage("保存失败!");
		}
		jsonBean.setData(flag);
		return jsonBean;
	}

	protected JsonBean result(String flag) {
		JsonBean jsonBean = new JsonBean();
		if (null != flag && "1".equals(flag)) {
			jsonBean.setStat(SUCCESS);
			jsonBean.setMessage("保存成功!");
		} else if ("3".equals(flag)) {
			jsonBean.setStat(EXISTED);
			jsonBean.setMessage("数据已存在!");
		} else {
			jsonBean.setStat(FAIL);
			jsonBean.setMessage("保存失败!");
		}
		jsonBean.setData(flag);
		return jsonBean;
	}

	public Object ajaxSuccess() {
		return ajaxSuccessMessage("");
	}

	public Object ajaxSuccess(Object data) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(data);
		jsonBean.setMessage("操作成功");
		return jsonBean;
	}

	public Object ajaxSuccess(Object data, String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(data);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作成功");
		}
		return jsonBean;
	}

	public Object ajaxSuccessMessage(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作成功");
		}
		return jsonBean;
	}

	public Object ajaxFailMessage(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败");
		}
		return jsonBean;
	}

	public Object ajaxFail() {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setMessage("");
		return jsonBean;
	}

	public Object ajaxFail(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败");
		}
		return jsonBean;
	}

	public Object ajaxFail(Object data, String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(data);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败,请联系管理员!");
		}
		return jsonBean;
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public Object illegalArgumentException(WebRequest request, IllegalArgumentException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail("非法参数:" + exception.getMessage());
		} else {
			return ajaxFail();
		}
	}

	@ExceptionHandler(EmptyResultDataAccessException.class)
	@ResponseBody
	public Object emptyResultDataAccessException(WebRequest request, EmptyResultDataAccessException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail("没有查询到数据:" + exception.getMessage());
		} else {
			return ajaxFail();
		}
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public Object runtimeException(WebRequest request, RuntimeException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail(exception.getMessage());
		} else {
			return ajaxFail("");
		}
	}
	

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Object exception(WebRequest request, Exception exception) {
		return ajaxFail("操作失败!");
	}

	public T_Sys_User getUser(HttpSession session) {
		T_Sys_User user = (T_Sys_User) session.getAttribute(CommonUtil.KEY_USER);
		return user;
	}
	public int getCompanyid(HttpSession session){
		T_Sys_User user = (T_Sys_User) session.getAttribute(CommonUtil.KEY_USER);
		if(user != null){
			return user.getCompanyid();
		}
		return 0;
	}
	public T_Sys_Set getSysSet(HttpSession session){
		T_Sys_Set set = (T_Sys_Set) session.getAttribute(CommonUtil.KEY_SYSSET);
		return set;
	}
	public void bulidParam(Map<String,Object> param,T_Sys_User user){
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, StringUtil.trimString(user.getShoptype()));
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SHOP_NAME, user.getShop_name());
        param.put(CommonUtil.SHOP_UPTYPE, StringUtil.trimString(user.getShop_uptype()));
        param.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
	}
	
	public String download(File file, HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setCharacterEncoding("utf-8");
			response.setContentType("multipart/octet-stream");
			response.setHeader("Content-Disposition", "attachment;fileName=" + new String(file.getName().getBytes(), "ISO-8859-1"));
			InputStream inputStream;
			inputStream = new FileInputStream(file);
			OutputStream os = response.getOutputStream();
			byte[] b = new byte[2048];
			int length;
			while ((length = inputStream.read(b)) > 0) {
				os.write(b, 0, length);
			}
			os.close();
			inputStream.close();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("下载文件错误:" + e.getMessage());
		}

	}
}
