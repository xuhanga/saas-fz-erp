package zy.controller.approve;


import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.approve.ApproveRecordService;

@Controller
@RequestMapping("approve")
public class ApproveRecordController extends BaseController{
	
	@Resource
	private ApproveRecordService approveRecordService; 
	
	@RequestMapping(value = "to_confirm", method = RequestMethod.GET)
	public String to_confirm() {
		return "approve/confirm";
	}
	
	@RequestMapping(value = "to_confirm_batch", method = RequestMethod.GET)
	public String to_confirm_batch() {
		return "approve/confirm_batch";
	}
	
	@RequestMapping(value = "to_stop", method = RequestMethod.GET)
	public String to_stop() {
		return "approve/stop";
	}
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "approve/list";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestParam(required = true) String number,@RequestParam(required = true) String ar_type,HttpSession session) {
		return ajaxSuccess(approveRecordService.list(number, ar_type, getCompanyid(session)));
	}
	
}
