package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.depot.T_Base_Depot;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.base.depot.DepotService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("base/depot")
public class DepotController extends BaseController{
	@Resource
	private DepotService depotService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/depot/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "base/depot/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(Integer dp_id,Model model) {
		T_Base_Depot depot = depotService.queryByID(dp_id);
		model.addAttribute("model", depot);
		return "base/depot/update";
	}
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page(PageForm pageForm, HttpServletRequest request, HttpSession session) {
		Object dp_shop_code = request.getParameter("dp_shop_code");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("name", pageForm.getSearchContent());
        param.put("dp_shop_code", dp_shop_code);
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
		PageData<T_Base_Depot> pageData = depotService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Depot model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 model.setDp_spell(model.getDp_name());
		 model.setDp_default(0);
		 depotService.save(model);
		 return ajaxSuccess();
	}
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Depot model,HttpServletRequest request,HttpSession session) {
		 model.setDp_spell(StringUtil.getSpell(model.getDp_name()));
		 depotService.update(model);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateDefault", method = { RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Object updateDefault(T_Base_Depot model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 depotService.updateDefault(model);
		 return ajaxSuccess();
	}
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Object del(T_Base_Depot model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 depotService.del(model);
		 return ajaxSuccess();
	}
	
	/**
	 * 仓库查询弹出框，按照功能模块
	 */
	@RequestMapping("to_list_dialog_bymodule")
	public String to_list_dialog_bymodule() {
		return "base/depot/list_dialog_bymodule";
	}
	
	@RequestMapping(value = "list4buy", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4buy(PageForm pageForm, HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(depotService.list4buy(param));
	}
	
	@RequestMapping(value = "list4batch", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4batch(PageForm pageForm, HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(depotService.list4batch(param));
	}
	
	@RequestMapping(value = "list4stock", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4stock(PageForm pageForm, HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(depotService.list4stock(param));
	}
	
	@RequestMapping(value = "list4want", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4want(PageForm pageForm, HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(depotService.list4want(param));
	}
	
	@RequestMapping(value = "list4allot", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4allot(PageForm pageForm, HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(depotService.list4allot(param));
	}
	
}
