package zy.controller.base;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.bra.T_Base_Bra;
import zy.entity.base.color.T_Base_Color;
import zy.entity.base.product.T_Base_Barcode;
import zy.entity.base.product.T_Base_Barcode_Set;
import zy.entity.base.product.T_Base_Product;
import zy.entity.base.product.T_Base_Product_Assist;
import zy.entity.base.product.T_Base_Product_Br;
import zy.entity.base.product.T_Base_Product_Color;
import zy.entity.base.product.T_Base_Product_Img;
import zy.entity.base.product.T_Base_Product_Shop_Price;
import zy.entity.base.size.T_Base_SizeList;
import zy.entity.sys.improtinfo.T_Import_Info;
import zy.entity.sys.set.T_Sys_Set;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.product.ProductService;
import zy.service.base.size.SizeService;
import zy.service.report.excel.ReportInterfaceFactory;
import zy.service.report.excel.reporter.ProductReporter;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.ExcelImportUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("base/product")
public class ProductController extends BaseController{
	@Resource
	private ReportInterfaceFactory reportInterfaceFactory;
	@Resource
	private ProductService productService;
	@Resource
	private SizeService sizeService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/product/list";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "base/product/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pd_id,Model model,HttpSession session) {
		T_Base_Product product = productService.queryByID(pd_id);
		if(null != product){
			T_Sys_User user = getUser(session);
			Map<String,Object> param = new HashMap<String, Object>();
		    param.put(CommonUtil.COMPANYID, user.getCompanyid());
		    param.put("pd_code", product.getPd_code());
			List<T_Base_Color> product_colors = productService.colorByProduct(param);
			if(product_colors != null && product_colors.size()>0){
				String pdc_cr_codes = "";
				for(int i=0;i<product_colors.size();i++){
					pdc_cr_codes += product_colors.get(i).getCr_code()+",";
				}
				pdc_cr_codes = pdc_cr_codes.substring(0, pdc_cr_codes.length()-1);
				product.setPdc_cr_codes(pdc_cr_codes);
			}
			model.addAttribute("product_colors",product_colors);
			model.addAttribute("product",product);
		}
		return "base/product/update";
	}
		
	@RequestMapping(value = "/to_barcode_add", method = { RequestMethod.GET})
	public String to_barcode_add(@RequestParam Integer pd_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("pd_id", pd_id);
	    List<T_Base_Product> list = productService.list(param);
	    
	    if (list == null || list.size() <= 0 ) {
			throw new IllegalArgumentException("该商品资料已经不存在！");
		}
		model.addAttribute("product", list.get(0));
		
		return "base/product/barcode_add";
	}
	
	@RequestMapping(value = "/to_img_add", method = { RequestMethod.GET})
	public String to_img_add(@RequestParam String pd_code,Model model) {
		model.addAttribute("pd_code", pd_code);
		return "base/product/img_add";
	}
	
	@RequestMapping(value = "/to_shop_price_list", method = { RequestMethod.GET})
	public String to_shop_price_list(@RequestParam String pd_code,Model model) {
		model.addAttribute("pd_code", pd_code);
		return "base/product/shop_price_list";
	}
	
	@RequestMapping(value = "/to_img_view/{pd_code}", method = { RequestMethod.GET})
	public String to_img_view(@PathVariable String pd_code,Model model,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("pd_code", pd_code);
		model.addAttribute("imgs", productService.productimglist(params));
		return "base/product/img_view";
	}
	
	@RequestMapping(value = "/to_barcode_list", method = RequestMethod.GET)
	public String to_barcode_list() {
		return "base/product/barcode_list";
	}
	
	@RequestMapping(value = "/to_barcode_plan", method = RequestMethod.GET)
	public String to_barcode_plan() {
		return "base/product/barcode_plan";
	}
	
	@RequestMapping(value = "/to_barcode_addPrintSet", method = RequestMethod.GET)
	public String to_barcode_addPrintSet(Integer bs_id,Model model,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("bs_id", bs_id);
		model.addAttribute("barcode_set", productService.getPrintSetByBsid(params));
		return "base/product/barcode_addPrintSet";
	}
	
	@RequestMapping("to_import_product")
	public String to_import_product() {
		return "base/product/import_product";
	}
	
	@RequestMapping("to_import_barcode")
	public String to_import_barcode() {
		return "base/product/import_barcode";
	}
	
	@RequestMapping(value = "/to_product_assist_template", method = RequestMethod.GET)
	public String to_product_assist_template() {
		return "base/product/product_assist_template";
	}
	
	@RequestMapping(value = "ajax_barcode_printset", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object ajax_barcode_printset(HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		T_Base_Barcode_Set barcode_set = productService.getPrintSetByState(params);
		return ajaxSuccess(barcode_set);
	}
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/product/list_dialog";
	}
	
	/**
	 * 商品调价选择商品
	 * @return
	 */
	@RequestMapping("to_list_dialog_price")
	public String to_list_dialog_price(HttpServletRequest request,Model model) {
		String sp_shop_code = request.getParameter("sp_shop_code");
		String discount = request.getParameter("discount");
		String sp_shop_type = request.getParameter("sp_shop_type");
		String pd_no = request.getParameter("pd_no");
		model.addAttribute("sp_shop_code", sp_shop_code);
		model.addAttribute("discount", discount);
		model.addAttribute("sp_shop_type", sp_shop_type);
		model.addAttribute("pd_no", pd_no);
		return "base/product/list_dialog_price";
	}
	
	/**
	 * 促销方案商品选择
	 * @return
	 */
	@RequestMapping("to_list_dialog_sale")
	public String to_list_dialog_sale() {
		return "base/product/list_dialog_sale";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String searchContent = request.getParameter("searchContent");
        String pd_no = request.getParameter("pd_no");
        String pd_name = request.getParameter("pd_name");
        String pd_spell = request.getParameter("pd_spell");
        String pd_year = request.getParameter("pd_year");
        String pd_season = request.getParameter("pd_season");
        String pd_tp_code = request.getParameter("pd_tp_code");
        String pd_bd_code = request.getParameter("pd_bd_code");
        String pd_sp_code = request.getParameter("pd_sp_code");
        String pd_number = request.getParameter("pd_number");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("pd_no", pd_no);
        param.put("pd_name", StringUtil.decodeString(pd_name));
        param.put("pd_spell", StringUtil.decodeString(pd_spell));
        param.put("pd_year", pd_year);
        param.put("pd_season", StringUtil.decodeString(pd_season));
        param.put("pd_tp_code", pd_tp_code);
        param.put("pd_bd_code", pd_bd_code);
        param.put("pd_sp_code", pd_sp_code);
        param.put("pd_number", pd_number);
		PageData<T_Base_Product> pageData = productService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "listReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String listReport(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String searchContent = request.getParameter("searchContent");
        String pd_no = request.getParameter("pd_no");
        String pd_name = request.getParameter("pd_name");
        String pd_spell = request.getParameter("pd_spell");
        String pd_year = request.getParameter("pd_year");
        String pd_season = request.getParameter("pd_season");
        String pd_tp_code = request.getParameter("pd_tp_code");
        String pd_bd_code = request.getParameter("pd_bd_code");
        String pd_sp_code = request.getParameter("pd_sp_code");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("pd_no", pd_no);
        param.put("pd_name", StringUtil.decodeString(pd_name));
        param.put("pd_spell", StringUtil.decodeString(pd_spell));
        param.put("pd_year", pd_year);
        param.put("pd_season", StringUtil.decodeString(pd_season));
        param.put("pd_tp_code", pd_tp_code);
        param.put("pd_bd_code", pd_bd_code);
        param.put("pd_sp_code", pd_sp_code);
        
        File file = reportInterfaceFactory.newInstance(ProductReporter.class, param).getFile();
        return download(file, request, response);
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Base_Product product,HttpSession session) {
		product.setCompanyid(getCompanyid(session));
		product.setPd_add_manager(getUser(session).getUs_name());
		productService.save(product);
		return ajaxSuccess(product);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Product product,HttpSession session) {
		product.setCompanyid(getCompanyid(session));
		product.setPd_mod_manager(getUser(session).getUs_name());
		productService.update(product);
		return ajaxSuccess(product);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer pd_id,@RequestParam String pd_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("pd_id",pd_id);
	    param.put("pd_code", pd_code);
	    List<T_Base_Product_Img> imgList = productService.productimglist(param);//查询出图片路径
		productService.del(param);//删除表数据
		
		//删除图片
		if(imgList != null && imgList.size()>0){
			String realpath = CommonUtil.CHECK_BASE;
			for(int i=0;i<imgList.size();i++){
				File _temp = new File(realpath + imgList.get(i).getPdm_img_path());
				if (_temp.exists()) {
					_temp.delete();
				}
			}
		}
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "pdNoIfExist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pdNoIfExist(HttpServletRequest request,HttpSession session) {
		T_Base_Product product = new T_Base_Product();
		String pd_no = request.getParameter("pd_no");
		product.setPd_no(pd_no);
		product.setCompanyid(getCompanyid(session));
		Integer id = productService.pdNoIfExit(product);
		if (null != id && id > 0) {
			return ajaxSuccess("exist");
		}else {
			return ajaxSuccess();
		}
	}
	
	@RequestMapping(value = "checkOptionChange", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object checkOptionChange(HttpServletRequest request,HttpSession session) {
		String codes = request.getParameter("codes");
		String pd_code = request.getParameter("pd_code");
		String optionType = request.getParameter("optionType");
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("codes",codes);
        param.put("pd_code", pd_code);
        boolean res = false;
        if(optionType != null && "color".equals(optionType)){//颜色
        	res = productService.queryColorIfEmploy(param);
        }else if(optionType != null && "size".equals(optionType)){//尺码
        	param.put("szl_szg_code",codes);
        	res = productService.querySizeIfEmploy(param);
        }else if(optionType != null && "bra".equals(optionType)){//杯型
        	res = productService.queryBraIfEmploy(param);
        }else if(optionType != null && "product".equals(optionType)){//商品
        	res = productService.queryProductIfEmploy(param);
		}
        
        if(res){
        	return ajaxSuccess("sucess");
		}else{
			return ajaxSuccess("fail");
		}
	}
	
	@RequestMapping(value = "barcodelist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object barcodelist(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String pd_code = request.getParameter("pd_code");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("pd_code", pd_code);
		PageData<T_Base_Barcode> pageData = productService.barcodelist(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "shop_price_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shop_price_list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String pd_code = request.getParameter("pd_code");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("pd_code", pd_code);
		PageData<T_Base_Product_Shop_Price> pageData = productService.shop_price_list(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "barcodepage", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object barcodepage(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String pd_no = request.getParameter("pd_no");
        String pd_name = request.getParameter("pd_name");
        String pd_tp_code = request.getParameter("pd_tp_code");
        String pd_bd_code = request.getParameter("pd_bd_code");
        String bc_barcode = request.getParameter("bc_barcode");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("pd_no", pd_no);
        param.put("pd_name", StringUtil.decodeString(pd_name));
        param.put("pd_tp_code", pd_tp_code);
        param.put("pd_bd_code", pd_bd_code);
        param.put("bc_barcode", StringUtil.decodeString(bc_barcode));
		PageData<T_Base_Barcode> pageData = productService.barcodepage(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	/**
	 * 条形码打印设置查询页面
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "barcodeplan", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object barcodeplan(HttpServletRequest request,HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
		List<T_Base_Barcode_Set> setList = productService.barcodeplan(param);
		return ajaxSuccess(setList, "查询成功, 共" +setList.size() + "条数据");
	}
	
	/**
	 * 条形码打印设置 启动
	 * @param bs_id
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "enable_plan", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object enable_plan(@RequestParam Integer bs_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("bs_id",bs_id);
		productService.enable_plan(param);
		return ajaxSuccess();
	}
	
	/**
	 * 删除条形码打印设置
	 * @param bs_id
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "del_bar_plan", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del_bar_plan(@RequestParam Integer bs_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("bs_id",bs_id);
		productService.del_bar_plan(param);
		return ajaxSuccess();
	}
	
	/**
	 * 条形码打印保存
	 */
	@RequestMapping(value = "save_barcode_printset", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_barcode_printset(T_Base_Barcode_Set barcode_Set,HttpSession session) {
		barcode_Set.setCompanyid(getCompanyid(session));
		productService.save_barcode_printset(barcode_Set);
		return ajaxSuccess(barcode_Set);
	}
	
	/**
	 * 当前商品颜色
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "colorByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void colorByProduct(@RequestParam String pd_code,HttpSession session,HttpServletResponse response) {
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("pd_code", pd_code);
	        List<T_Base_Color> list = productService.colorByProduct(param);
	        
	        StringBuffer colorList = new StringBuffer();
	        colorList.append("{\"data\":{");
	        colorList.append("\"items\":[");
	        for (int i=0;i<list.size();i++){
	        	colorList.append("{\"cr_code\":\"").append(list.get(i).getCr_code())
	        				.append("\",\"cr_name\":\"").append(list.get(i).getCr_name()).append("\"}");
	        	if(i<list.size()-1){
	        		colorList.append(",");
	        	}
			}
	        colorList.append("]");
	        colorList.append("}}");
			out = response.getWriter();
			out.print(colorList.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(out != null){
				out.close();
			}
		}
	}
	
	/**
	 * 当前商品图片颜色
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "colorByProductImg", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void colorByProductImg(@RequestParam String pd_code,HttpSession session,HttpServletResponse response) {
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("pd_code", pd_code);
	        List<T_Base_Color> list = productService.colorByProduct(param);
	        
	        StringBuffer colorList = new StringBuffer();
	        colorList.append("{\"data\":{");
	        colorList.append("\"items\":[");
	        colorList.append("{");
	        colorList.append("\"cr_code\":\"\",");
	        colorList.append("\"cr_name\":\"全部\"");
	        colorList.append("},");
	        colorList.append("{");
	        colorList.append("\"cr_code\":\"0\",");
	        colorList.append("\"cr_name\":\"默认\"");
	        colorList.append("},");
	        for (int i=0;i<list.size();i++){
	        	colorList.append("{\"cr_code\":\"").append(list.get(i).getCr_code())
	        				.append("\",\"cr_name\":\"").append(list.get(i).getCr_name()).append("\"}");
	        	if(i<list.size()-1){
	        		colorList.append(",");
	        	}
			}
	        colorList.append("]");
	        colorList.append("}}");
			out = response.getWriter();
			out.print(colorList.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(out != null){
				out.close();
			}
		}
	}
	
	/**
	 * 当前商品尺码
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "sizeByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void sizeByProduct(@RequestParam String szg_code,HttpSession session,HttpServletResponse response) {
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("szl_szg_code", szg_code);
	        List<T_Base_SizeList> list = sizeService.listByID(param);
	        
	        StringBuffer sizeList = new StringBuffer();
	        sizeList.append("{\"data\":{");
	        sizeList.append("\"items\":[");
	        for (int i=0;i<list.size();i++){
	        	sizeList.append("{\"sz_code\":\"").append(list.get(i).getSzl_sz_code())
	        				.append("\",\"sz_name\":\"").append(list.get(i).getSz_name()).append("\"}");
	        	if(i<list.size()-1){
	        		sizeList.append(",");
	        	}
			}
	        sizeList.append("]");
	        sizeList.append("}}");
			out = response.getWriter();
			out.print(sizeList.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(out != null){
				out.close();
			}
		}
	}
	
	/**
	 * 当前商品杯型
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "braByProduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void braByProduct(@RequestParam String pd_code,HttpSession session,HttpServletResponse response) {
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("pd_code", pd_code);
	        List<T_Base_Bra> list = productService.braByProduct(param);
	        
	        StringBuffer braList = new StringBuffer();
	        braList.append("{\"data\":{");
	        braList.append("\"items\":[");
	        for (int i=0;i<list.size();i++){
	        	braList.append("{\"br_code\":\"").append(list.get(i).getBr_code())
	        				.append("\",\"br_name\":\"").append(list.get(i).getBr_name()).append("\"}");
	        	if(i<list.size()-1){
	        		braList.append(",");
	        	}
			}
	        braList.append("]");
	        braList.append("}}");
			out = response.getWriter();
			out.print(braList.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(out != null){
				out.close();
			}
		}
	}
	
	/**
	 * 查询商品图片
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "productimglist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void productimglist(@RequestParam String pd_code,@RequestParam String cr_code,
			HttpSession session,HttpServletResponse response) {
		PrintWriter out=null;
		try {
	        T_Sys_User user = getUser(session);
	        Map<String,Object> param = new HashMap<String, Object>();
	        param.put(CommonUtil.COMPANYID, user.getCompanyid());
	        param.put("pd_code", pd_code);
	        param.put("cr_code", cr_code);
	        List<T_Base_Product_Img> list = productService.productimglist(param);
	        
	        StringBuffer imgList = new StringBuffer();
	        imgList.append("{\"data\":{");
	        imgList.append("\"items\":[");
	        if(list != null && list.size()>0){
		        for (int i=0;i<list.size();i++){
		        	imgList.append("{");
		        	imgList.append("\"pdm_id\":\"" + StringUtil.trimString(list.get(i).getPdm_id()) + "\",");
		        	imgList.append("\"pdm_img_path\":\"" + StringUtil.trimString(list.get(i).getPdm_img_path()) + "\",");
		        	imgList.append("\"cr_name\":\"" + (StringUtil.trimString(list.get(i).getCr_name()).equals("")?"默认":list.get(i).getCr_name()) + "\"");
		        	imgList.append("},");
				}
		        imgList.deleteCharAt(imgList.length() - 1);
	        }
	        imgList.append("]");
	        imgList.append("}}");
			out = response.getWriter();
			out.print(imgList.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(out != null){
				out.close();
			}
		}
	}
	
	/**
	 * 图片保存
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "saveimg", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object saveimg(String pd_code, String cr_code,
			MultipartFile uploadify, String serverPath, HttpSession session) throws Exception{
		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		if (uploadify == null || uploadify.getSize() <= 0) {
			throw new IllegalArgumentException("请上传照片");
		}
		//获取图片大小
		long picSize = (uploadify.getSize()/1024) + 1;
		//图片不能大于200K
		if(picSize > 200){
			throw new IllegalArgumentException("上传的图片不能大于200K!");
		}
		
		String realpath = CommonUtil.CHECK_BASE;
		String uploadFileName = uploadify.getOriginalFilename();
		String suffix = uploadFileName.substring(uploadFileName
				.lastIndexOf("."));
		String img_name = new Date().getTime() + "" + new Random().nextInt(9999) + suffix;
		String img_path = CommonUtil.PRODUCT_IMG_PATH + "/" + img_name;
		String all_path = serverPath + img_path;
		
		if (pd_code == null || "".equals(pd_code)) {
			throw new IllegalArgumentException("商品编号不能为空！");
		}
		param.put("pd_code", pd_code);
		
		if (cr_code == null) {
			throw new IllegalArgumentException("颜色编号不能为空！");
		}
		if("".equals(cr_code)){
			cr_code = "0";
		}
		param.put("cr_code", cr_code);
		param.put("img_name", img_name);
		param.put("all_path", all_path);
		param.put("img_path", img_path);

		productService.saveimg(param);

		File savefile = new File(realpath, img_path);
		if (!savefile.getParentFile().exists()) {
			savefile.getParentFile().mkdirs();
		}
		uploadify.transferTo(savefile);
		return ajaxSuccess("保存成功");
	}
	
	/**
	 * 图片删除
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "deleteimg", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object deleteimg(@RequestParam Integer pdm_id,@RequestParam String pdm_img_path, 
			HttpSession session) throws Exception{
		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		if (pdm_id == null || 0 == pdm_id) {
			throw new IllegalArgumentException("图片id不能为空！");
		}
		if (pdm_img_path == null || "".equals(pdm_img_path)) {
			throw new IllegalArgumentException("图片路径不能为空！");
		}
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put("pdm_id", pdm_id);
		productService.deleteimg(param);
		String realpath = CommonUtil.CHECK_BASE;
		File _temp = new File(realpath + pdm_img_path);
		if (_temp.exists()) {
			_temp.delete();
		}
		return ajaxSuccess("删除成功");
	}
	
	/**
	 * 删除条码
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "deleteBarcode", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object deleteBarcode(@RequestParam Integer bc_id,HttpSession session) throws Exception{
		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		if (bc_id == null || 0 == bc_id) {
			throw new IllegalArgumentException("条码id不能为空！");
		}
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put("bc_id", bc_id);
		productService.deleteBarcode(param);
		return ajaxSuccess("删除成功");
	}
	
	/**
	 * 批量删除条码
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "delBulkBarcode", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object delBulkBarcode(@RequestParam String bc_ids,HttpSession session) throws Exception{
		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		if (StringUtil.isEmpty(bc_ids)) {
			throw new IllegalArgumentException("条码id不能为空！");
		}
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put("bc_ids", bc_ids);
		productService.delBulkBarcode(param);
		return ajaxSuccess("删除成功");
	}
	
	/**
	 * 清空条码
	 * @param session
	 * @param response
	 */
	@RequestMapping(value = "delAllBarcode", method = { RequestMethod.GET,RequestMethod.POST })
	@ResponseBody
	public Object delAllBarcode(HttpSession session) throws Exception{
		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		productService.delAllBarcode(param);
		return ajaxSuccess("删除成功");
	}
	
	/**
	 * 保存单个条形码
	 * @param session
	 * @param request
	 */
	@RequestMapping(value = "saveOneBarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveOneBarcode(HttpSession session,HttpServletRequest request,HttpServletResponse response) {
		String bc_barcode = request.getParameter("bc_barcode");
		String bc_sys_barcode = request.getParameter("bc_sys_barcode");
		String pd_code = request.getParameter("pd_code");
		String pd_no = request.getParameter("pd_no");
		String cr_code = request.getParameter("cr_code");
		String sz_code = request.getParameter("sz_code");
		String br_code = request.getParameter("br_code");
		String bc_subcode = request.getParameter("bc_subcode");
		String bc_id = request.getParameter("bc_id");
		T_Sys_User user = getUser(session);
	    Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			return ajaxFail(FAIL,"连接超时，请重新登录!");
		}
	    param.put("bc_barcode", bc_barcode);
	    if(bc_barcode == null || "".equals(bc_barcode)){
			return ajaxFail(FAIL,"条形码不能为空");
		}
	    param.put("bc_sys_barcode", bc_sys_barcode);
	    param.put("pd_code", pd_code);
	    param.put("pd_no", pd_no);
	    param.put("cr_code", cr_code);
	    param.put("sz_code", sz_code);
	    param.put("br_code", br_code);
	    param.put("bc_subcode", bc_subcode);
	    param.put("bc_id", bc_id);
	    List<String> barcodes = productService.queryBarCodeIfExit(param);
		if (null != barcodes) {
			if(barcodes.size()>0){
				String barcode = "";
				for(int i=0;i<barcodes.size();i++){
					barcode += barcodes.get(i) + ",";
				}
				barcode = barcode.substring(0,barcode.length()-1);
				
				return ajaxFail(FAIL,"条形码["+barcode+"]已经存在");
			}
		}
		if(bc_id!=null && !"0".equals(bc_id) && !StringUtil.trimString(bc_barcode).equals("")){
			 productService.updateOneBarcode(param);
		}else {
			if (bc_sys_barcode == null || "".equals(bc_sys_barcode)) {
				return ajaxFail(FAIL,"参数bc_sys_barcode不能为null");
			}
			if (pd_code == null || "".equals(pd_code)) {
				return ajaxFail(FAIL,"参数pd_code不能为null");
			}
			if (pd_no == null || "".equals(pd_no)) {
				return ajaxFail(FAIL,"参数pd_no不能为null");
			}
			if (cr_code == null || "".equals(cr_code)) {
				return ajaxFail(FAIL,"参数cr_code不能为null");
			}
			if (sz_code == null || "".equals(sz_code)) {
				return ajaxFail(FAIL,"参数sz_code不能为null");
			}
			if (bc_subcode == null || "".equals(bc_subcode)) {
				return ajaxFail(FAIL,"参数bc_subcode不能为null");
			}
		    productService.saveOneBarcode(param);
		}
		return ajaxSuccess();
	}
	
	/**
	 * 保存多个条形码
	 * @param session
	 * @param request
	 */
	@RequestMapping(value = "saveAllBarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveAllBarcode(HttpSession session,HttpServletRequest request,HttpServletResponse response) {
		String bc_barcodes = request.getParameter("bc_barcodes");
		String bc_ids = request.getParameter("bc_ids");
		T_Sys_User user = getUser(session);
	    Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    Object companyid = param.get(CommonUtil.COMPANYID);
		if (companyid == null) {
			return ajaxFail(FAIL,"连接超时，请重新登录!");
		}
	    param.put("bc_barcode", bc_barcodes);
	    if(bc_barcodes == null || "".equals(bc_barcodes)){
			return ajaxFail(FAIL,"条形码不能为空");
		}
	    param.put("bc_id", bc_ids);
	    List<String> barcodes = productService.queryBarCodeIfExit(param);
		if (null != barcodes) {
			if(barcodes.size()>0){
				String barcode = "";
				for(int i=0;i<barcodes.size();i++){
					barcode += barcodes.get(i) + ",";
				}
				barcode = barcode.substring(0,barcode.length()-1);
				
				return ajaxFail(FAIL,"条形码["+barcode+"]已经存在");
			}
		}
		if(bc_ids!=null && !"0".equals(bc_ids) && !StringUtil.trimString(bc_barcodes).equals("")){
			productService.updateAllBarcode(param);
		}else {
			String pd_nos = request.getParameter("pd_nos");
			String pd_codes = request.getParameter("pd_codes");
			String cr_codes = request.getParameter("cr_codes");
			String sz_codes = request.getParameter("sz_codes");
			String br_codes = request.getParameter("br_codes");
			String bc_subcodes = request.getParameter("bc_subcodes");
			String bc_sys_barcodes = request.getParameter("bc_sys_barcodes");
			
			param.put("pd_no", pd_nos);
			param.put("pd_code", pd_codes);
			param.put("cr_code", cr_codes);
			param.put("sz_code", sz_codes);
			param.put("br_code", br_codes);
			param.put("bc_subcode", bc_subcodes);
			param.put("bc_sys_barcode", bc_sys_barcodes);
			
			productService.saveAllBarcode(param);
		}
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save_import_product", method = RequestMethod.POST,produces = "text/html; charset=utf-8")
	@ResponseBody
	public Object save_import_product(MultipartFile excel,HttpServletRequest request,HttpSession session)throws IOException {
		if (excel == null || excel.getSize() <= 0) {
			return ajaxFail("请选择需要导入的文件！"); 
		}
		Map<String,Object> param = new HashMap<String, Object>();
		Workbook sourceWorkBook = null;
		Workbook wb_error = null;
		WritableWorkbook wb = null;
		
		try {
			T_Sys_User user = getUser(session);
			param.put("companyid", user.getCompanyid());
			param.put("us_id", user.getUs_id());
			//根据商家编号、用户编号及当前时间生成文件名
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = "product_" + user.getCompanyid() + (user.getUs_id() + format.format(new Date())) + ".xls";
			
			// 复制客户端文件到服务器临时文件夹进行处理,处理完以后删除
			String temp_uploaddir = CommonUtil.CHECK_BASE + CommonUtil.EXCEL_PATH;
			String toFilePath = temp_uploaddir + File.separator + fileName;
			File savefile = new File(toFilePath);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			excel.transferTo(savefile);
			
			// Excel 模板文件路径，把模板文件复制，然后作为错误文件进行数据追加
			String relPath = session.getServletContext().getRealPath("resources");
			String excel_member_template = relPath + File.separator + "excel" + File.separator + CommonUtil.PRODUCTLEADING;
			
			// 错误文件名称
			String errFileName = "fail" + fileName;
			String errorFilePath = temp_uploaddir + File.separator + errFileName;
			File excel_template = new File(excel_member_template);// 原有Excel 模板文件
			File errorFile = new File(errorFilePath);// 生成的错误文件路径
			// 把模板文件复制给错误文件
			FileUtils.copyFile(excel_template, errorFile);
			
			sourceWorkBook = Workbook.getWorkbook(savefile);// 要导入的Excel
			wb_error = Workbook.getWorkbook(errorFile);
			wb = Workbook.createWorkbook(errorFile, wb_error);
			Map<String, Object> result = productService.parseSavingProductExcel(sourceWorkBook, wb, user);
			
			ExcelImportUtil.deleteFile(savefile);
			int success = (Integer)(result.get("success"));// 导入成功条数
			int fail = (Integer)(result.get("fail"));// 导入失败条数
			param.put("productList", (List<T_Base_Product>)result.get("productList"));//导入成功商品资料
			param.put("colorMap", (Map<String, List<T_Base_Product_Color>>)result.get("colorMap"));//导入成功商品资料对应的颜色
			param.put("brMap", (Map<String, List<T_Base_Product_Br>>)result.get("brMap"));//导入成功商品资料对应的杯型
			
			T_Import_Info t_Import_Info = new T_Import_Info();
			t_Import_Info.setIi_success(success);
			t_Import_Info.setIi_fail(fail);
			t_Import_Info.setIi_total(success+fail);
			t_Import_Info.setIi_filename(excel.getOriginalFilename());
			t_Import_Info.setIi_error_filename(errFileName);
			t_Import_Info.setIi_us_id(user.getUs_id());
			t_Import_Info.setIi_sysdate(DateUtil.getCurrentTime());
			t_Import_Info.setIi_type(CommonUtil.IMPROT_PRODUCTTYPE);
			t_Import_Info.setCompanyid(user.getCompanyid());
			param.put("t_Import_Info", t_Import_Info);
			param.put("ii_type", CommonUtil.IMPROT_PRODUCTTYPE);
			
			productService.save(param);
			wb.write();
		}  catch (Exception e){
			e.printStackTrace();
			return ajaxFail("导入失败，请联系管理员！");
		} finally {
			if (sourceWorkBook != null)
				sourceWorkBook.close();
			if (wb_error != null)
				wb_error.close();
			try {
				if (wb != null)
					wb.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save_import_barcode", method = RequestMethod.POST,produces = "text/html; charset=utf-8")
	@ResponseBody
	public Object save_import_barcode(MultipartFile excel,HttpServletRequest request,HttpSession session)throws IOException {
		if (excel == null || excel.getSize() <= 0) {
			return ajaxFail("请选择需要导入的文件！"); 
		}
		Map<String,Object> param = new HashMap<String, Object>();
		Workbook sourceWorkBook = null;
		Workbook wb_error = null;
		WritableWorkbook wb = null;
		
		try {
			T_Sys_Set set = getSysSet(session);
			T_Sys_User user = getUser(session);
			param.put("companyid", user.getCompanyid());
			param.put("us_id", user.getUs_id());
			//根据商家编号、用户编号及当前时间生成文件名
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = "barcode_" + user.getCompanyid() + (user.getUs_id() + format.format(new Date())) + ".xls";
			
			// 复制客户端文件到服务器临时文件夹进行处理,处理完以后删除
			String temp_uploaddir = CommonUtil.CHECK_BASE + CommonUtil.EXCEL_PATH;
			String toFilePath = temp_uploaddir + File.separator + fileName;
			File savefile = new File(toFilePath);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			excel.transferTo(savefile);
			
			// Excel 模板文件路径，把模板文件复制，然后作为错误文件进行数据追加
			String relPath = session.getServletContext().getRealPath("resources");
			String excel_member_template = relPath + File.separator + "excel" + File.separator + CommonUtil.BARCODELEADING;
			
			// 错误文件名称
			String errFileName = "fail" + fileName;
			String errorFilePath = temp_uploaddir + File.separator + errFileName;
			File excel_template = new File(excel_member_template);// 原有Excel 模板文件
			File errorFile = new File(errorFilePath);// 生成的错误文件路径
			// 把模板文件复制给错误文件
			FileUtils.copyFile(excel_template, errorFile);
			
			sourceWorkBook = Workbook.getWorkbook(savefile);// 要导入的Excel
			wb_error = Workbook.getWorkbook(errorFile);
			wb = Workbook.createWorkbook(errorFile, wb_error);
			Map<String, Object> result = productService.parseSavingBarcodeExcel(sourceWorkBook, wb, user,set);
			wb.write();
			
			ExcelImportUtil.deleteFile(savefile);
			int success = (Integer)(result.get("success"));// 导入成功条数
			int fail = (Integer)(result.get("fail"));// 导入失败条数
			param.put("barcodeList", (List<T_Base_Barcode>)result.get("barcodeList"));//导入成功条形码
			
			T_Import_Info t_Import_Info = new T_Import_Info();
			t_Import_Info.setIi_success(success);
			t_Import_Info.setIi_fail(fail);
			t_Import_Info.setIi_total(success+fail);
			t_Import_Info.setIi_filename(excel.getOriginalFilename());
			t_Import_Info.setIi_error_filename(errFileName);
			t_Import_Info.setIi_us_id(user.getUs_id());
			t_Import_Info.setIi_sysdate(DateUtil.getCurrentTime());
			t_Import_Info.setIi_type(CommonUtil.IMPROT_BARCODETYPE);
			t_Import_Info.setCompanyid(user.getCompanyid());
			param.put("t_Import_Info", t_Import_Info);
			param.put("ii_type", CommonUtil.IMPROT_BARCODETYPE);
			
			productService.save_import_barcode(param);
		}  catch (Exception e){
			e.printStackTrace();
			return ajaxFail("导入失败，请联系管理员！");
		} finally {
			if (sourceWorkBook != null)
				sourceWorkBook.close();
			if (wb_error != null)
				wb_error.close();
			try {
				if (wb != null)
					wb.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ajaxSuccess();
	}
	
	/**
	 * 商品资料辅助属性模板查询页面
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "assist_template", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object assist_template(HttpServletRequest request,HttpSession session) {
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
		List<T_Base_Product_Assist> setList = productService.assist_template(param);
		return ajaxSuccess(setList, "查询成功, 共" +setList.size() + "条数据");
	}
	
	/**
	 * 根据id查询商品资料辅助属性
	 * @param bs_id
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "get_assist_byid", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object get_assist_byid(@RequestParam Integer pda_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("pda_id",pda_id);
		T_Base_Product_Assist product_Assist = productService.get_assist_byid(param);
		return ajaxSuccess(product_Assist);
	}
	
	@RequestMapping(value = "/to_add_product_assist", method = RequestMethod.GET)
	public String to_add_product_assist(Integer pda_id,Model model,HttpSession session) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("pda_id", pda_id);
		model.addAttribute("product_Assist", productService.get_assist_byid(params));
		return "base/product/add_product_assist";
	}
	
	@RequestMapping(value = "save_product_assist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_product_assist(T_Base_Product_Assist product_Assist,HttpSession session) {
		product_Assist.setCompanyid(getCompanyid(session));
		productService.save_product_assist(product_Assist);
		return ajaxSuccess(product_Assist);
	}
	
	@RequestMapping(value = "update_product_assist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update_product_assist(T_Base_Product_Assist product_Assist,HttpSession session) {
		product_Assist.setCompanyid(getCompanyid(session));
		productService.update_product_assist(product_Assist);
		return ajaxSuccess(product_Assist);
	}
	
	@RequestMapping(value = "del_product_assist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del_product_assist(@RequestParam Integer pda_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("pda_id",pda_id);
		productService.del_product_assist(param);//删除表数据
		return ajaxSuccess();
	}
}
