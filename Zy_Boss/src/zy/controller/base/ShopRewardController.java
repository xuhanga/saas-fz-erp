package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.base.shop.ShopRewardService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("base/shopreward")
public class ShopRewardController extends BaseController{
	@Resource
	private ShopRewardService shopRewardService;
	
	@RequestMapping("to_stat")
	public String to_stat() {
		return "base/shopreward/stat";
	}
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "base/shopreward/list";
	}
	
	@RequestMapping(value = "statByShop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statByShop(PageForm pageForm,@RequestParam(required = true) String sp_code,HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("sr_sp_code", sp_code);
		return ajaxSuccess(shopRewardService.statByShop(params));
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String sr_sp_code,
			@RequestParam(required = false) String sr_rw_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", StringUtil.isEmpty(enddate)?"":enddate+" 23:59:59");
        params.put("sr_sp_code", sr_sp_code);
        params.put("sr_rw_code", sr_rw_code);
		return ajaxSuccess(shopRewardService.page(params));
	}
	
}
