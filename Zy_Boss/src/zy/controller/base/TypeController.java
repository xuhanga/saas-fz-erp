package zy.controller.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.base.type.T_Base_Type;
import zy.entity.sys.user.T_Sys_User;
import zy.service.base.type.TypeService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.base.BaseVO;

@Controller
@RequestMapping("base/type")
public class TypeController extends BaseController{
	@Resource
	private TypeService typeService;
	
	@RequestMapping(value = "/to_all")
	public String to_all() {
		return "base/type/all";
	}
	
	@RequestMapping(value = "/to_tree")
	public String to_tree() {
		return "base/type/tree";
	}
	
	@RequestMapping(value = "/to_list")
	public String to_list() {
		return "base/type/list";
	}
	
	@RequestMapping(value = "/to_add")
	public String to_add() {
		return "base/type/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer tp_id,Model model) {
		T_Base_Type type = typeService.queryByID(tp_id);
		if(null != type){
			model.addAttribute("type",type);
		}
		return "base/type/update";
	}
	@RequestMapping("to_tree_dialog")
	public String to_tree_dialog() {
		return "base/type/tree_dialog";
	}
	@RequestMapping(value = "/tree", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tree(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
		List<T_Base_Type> list = typeService.list(param);
		String tree = BaseVO.buildBaseTypeTree(list);
		return ajaxSuccess(tree, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "/list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpSession session, String searchContent, String tp_upcode) {
		T_Sys_User user = getUser(session);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		if (searchContent != null && !searchContent.equals("")) {
			param.put("searchContent", StringUtil.decodeString(searchContent));
		}
		if (tp_upcode != null && !"".equals(tp_upcode)) {
			param.put("tp_upcode", tp_upcode);
		}
		List<T_Base_Type> list = typeService.list(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "/save",method = {RequestMethod.POST,RequestMethod.GET})
	public @ResponseBody Object save(T_Base_Type t_Base_Type,HttpServletRequest request,HttpSession session){
		T_Sys_User user = getUser(session);
		t_Base_Type.setCompanyid(user.getCompanyid());
		t_Base_Type.setTp_spell(StringUtil.getSpell(t_Base_Type.getTp_name()));
		Integer id = typeService.queryByName(t_Base_Type);
		if(null != id && id > 0){
			return result(EXISTED);
		}else{
			typeService.save(t_Base_Type);
			return ajaxSuccess(t_Base_Type);
		}
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer tp_id,HttpServletRequest request,HttpSession session) {
		typeService.del(tp_id);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Base_Type type,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		type.setCompanyid(user.getCompanyid());
		Integer id = typeService.queryByName(type);
		if(null != id && id > 0){
			return result(EXISTED);
		}else{
			typeService.update(type);
			return ajaxSuccess(type);
		}
	}
	
}
