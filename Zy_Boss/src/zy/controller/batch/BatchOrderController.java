package zy.controller.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.batch.order.T_Batch_Order;
import zy.entity.batch.order.T_Batch_OrderList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.batch.order.BatchOrderService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.batch.BatchOrderVO;

@Controller
@RequestMapping("batch/order")
public class BatchOrderController extends BaseController{
	@Resource
	private BatchOrderService batchOrderService;
	
	@RequestMapping(value = "to_list/{od_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "batch/order/list";
	}
	@RequestMapping(value = "to_list_draft_dialog/{od_type}", method = RequestMethod.GET)
	public String to_list_draft_dialog(@PathVariable Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "batch/order/list_draft_dialog";
	}
	@RequestMapping(value = "to_add/{od_type}", method = RequestMethod.GET)
	public String to_add(@PathVariable Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "batch/order/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer od_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Batch_Order order = batchOrderService.load(od_id);
		if(null != order){
			batchOrderService.initUpdate(order.getOd_number(), order.getOd_type(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("order",order);
		}
		return "batch/order/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer od_id,Model model) {
		T_Batch_Order order = batchOrderService.load(od_id);
		if(null != order){
			model.addAttribute("order",order);
		}
		return "batch/order/view";
	}
	@RequestMapping(value = "to_select_product/{od_type}", method = RequestMethod.GET)
	public String to_select_product(@PathVariable Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "batch/order/select_product";
	}
	@RequestMapping(value = "to_temp_update/{od_type}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "batch/order/temp_update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer od_type,
			@RequestParam(required = true) Integer od_isdraft,
			@RequestParam(required = false) Integer od_ar_state,
			@RequestParam(required = false) String od_client_code,
			@RequestParam(required = false) String od_depot_code,
			@RequestParam(required = false) String od_manager,
			@RequestParam(required = false) String od_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("od_type", od_type);
        param.put("od_isdraft", od_isdraft);
        param.put("od_ar_state", od_ar_state);
        param.put("od_client_code", od_client_code);
        param.put("od_depot_code", od_depot_code);
        param.put("od_manager", StringUtil.decodeString(od_manager));
        param.put("od_number", StringUtil.decodeString(od_number));
		return ajaxSuccess(batchOrderService.page(param));
	}
	
	@RequestMapping(value = "page4Sell", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page4Sell(PageForm pageForm,
			@RequestParam(required = true) Integer od_type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String od_client_code,
			@RequestParam(required = false) String od_depot_code,
			@RequestParam(required = false) String od_manager,
			@RequestParam(required = false) String od_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("od_type", od_type);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("od_client_code", od_client_code);
        param.put("od_depot_code", od_depot_code);
        param.put("od_manager", StringUtil.decodeString(od_manager));
        param.put("od_number", StringUtil.decodeString(od_number));
		return ajaxSuccess(batchOrderService.page4Sell(param));
	}
	
	
	@RequestMapping(value = "detail_list/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchOrderService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchOrderService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchOrderService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(batchOrderService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_list(@PathVariable Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_type", od_type);
		params.put("odl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchOrderService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_sum(@PathVariable Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(batchOrderService.temp_sum(od_type,user.getUs_id(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_size_title/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size_title(@PathVariable Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_type", od_type);
		params.put("odl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchOrderService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size(@PathVariable Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_type", od_type);
		params.put("odl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(batchOrderService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(@PathVariable Integer od_type,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String ci_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) String batch_price,
			@RequestParam(required = false) Double ci_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("ci_code", ci_code);
		params.put("priceType", priceType);
		params.put("ci_rate", ci_rate);
		params.put("batch_price", batch_price);
		params.put("user", user);
		Map<String, Object> resultMap = batchOrderService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page_product(PageForm pageForm,
			@RequestParam(required = true) String od_type,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("od_type", od_type);
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(batchOrderService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_loadproduct(@PathVariable Integer od_type,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String gift,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String ci_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) String batch_price,
			@RequestParam(required = false) Double ci_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("od_type", od_type);
		params.put("pd_code", pd_code);
		params.put("odl_pi_type", gift);
		params.put("exist", exist);
		params.put("priceType", priceType);
		params.put("ci_rate", ci_rate);
		params.put("batch_price", batch_price);
		params.put("dp_code", dp_code);
		params.put("ci_code", ci_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(batchOrderService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@PathVariable Integer od_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Batch_OrderList> temps = BatchOrderVO.convertMap2Model(data, od_type, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		batchOrderService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer odl_id) {
		batchOrderService.temp_del(odl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Batch_OrderList temp) {
		batchOrderService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(@PathVariable Integer od_type,T_Batch_OrderList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setOdl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setOdl_type(od_type);
		batchOrderService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Batch_OrderList temp) {
		temp.setOdl_remark(StringUtil.decodeString(temp.getOdl_remark()));
		batchOrderService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(@PathVariable Integer od_type,T_Batch_OrderList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setOdl_remark(StringUtil.decodeString(temp.getOdl_remark()));
		temp.setOdl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setOdl_type(od_type);
		batchOrderService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@PathVariable Integer od_type,
			@RequestParam String pd_code,@RequestParam Integer odl_pi_type,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Batch_OrderList temp = new T_Batch_OrderList();
		temp.setOdl_pd_code(pd_code);
		temp.setOdl_cr_code(cr_code);
		temp.setOdl_br_code(br_code);
		temp.setOdl_pi_type(odl_pi_type);
		temp.setOdl_type(od_type);
		temp.setOdl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		batchOrderService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(@PathVariable Integer od_type, HttpSession session) {
		T_Sys_User user = getUser(session);
		batchOrderService.temp_clear(od_type, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@PathVariable Integer od_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = BatchOrderVO.convertMap2Model_import(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("datas", datas);
		params.put("user", user);
		params.put("ci_code", data.get("ci_code"));
		params.put("priceType", data.get("priceType"));
		params.put("ci_rate", data.get("ci_rate"));
		params.put("batch_price", data.get("batch_price"));
		batchOrderService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@PathVariable Integer od_type,@RequestParam String od_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("od_number", od_number);
		params.put("user", user);
		batchOrderService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Batch_Order order,HttpSession session) {
		batchOrderService.save(order, getUser(session));
		return ajaxSuccess(order);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Batch_Order order,HttpSession session) {
		batchOrderService.update(order, getUser(session));
		return ajaxSuccess(order);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Batch_Order order = batchOrderService.approve(number, record, getUser(session));
		return ajaxSuccess(order);
	}
	
	@RequestMapping(value = "stop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object stop(String number,HttpSession session) {
		return ajaxSuccess(batchOrderService.stop(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		batchOrderService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = batchOrderService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = BatchOrderVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
