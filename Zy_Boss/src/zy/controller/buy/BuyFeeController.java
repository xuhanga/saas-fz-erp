package zy.controller.buy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.fee.T_Buy_Fee;
import zy.entity.buy.fee.T_Buy_FeeList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.buy.fee.BuyFeeService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.buy.BuyFeeVO;

@Controller
@RequestMapping("buy/fee")
public class BuyFeeController extends BaseController{
	@Resource
	private BuyFeeService buyFeeService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "buy/fee/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "buy/fee/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer fe_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Buy_Fee fee = buyFeeService.load(fe_id);
		if(null != fee){
			buyFeeService.initUpdate(fee.getFe_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("fee",fee);
		}
		return "buy/fee/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer fe_id,Model model) {
		T_Buy_Fee fee = buyFeeService.load(fe_id);
		if(null != fee){
			model.addAttribute("fee",fee);
		}
		return "buy/fee/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Buy_Fee fee = buyFeeService.load(number,getCompanyid(session));
		if(null != fee){
			model.addAttribute("fee",fee);
		}
		return "buy/fee/view";
	}
	
	@RequestMapping(value = "to_report", method = RequestMethod.GET)
	public String to_report() {
		return "buy/fee/report";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer fe_ar_state,
			@RequestParam(required = false) String fe_supply_code,
			@RequestParam(required = false) String fe_manager,
			@RequestParam(required = false) String fe_number,
			HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("fe_ar_state", fe_ar_state);
        param.put("fe_supply_code", fe_supply_code);
        param.put("fe_manager", StringUtil.decodeString(fe_manager));
        param.put("fe_number", StringUtil.decodeString(fe_number));
		return ajaxSuccess(buyFeeService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{fe_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String fe_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("fel_number", fe_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(buyFeeService.detail_list(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("fel_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(buyFeeService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestParam String temps,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Buy_FeeList> feeLists = JSON.parseArray(temps, T_Buy_FeeList.class);
		buyFeeService.temp_save(feeLists, user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateMoney(T_Buy_FeeList temp) {
		buyFeeService.temp_updateMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Buy_FeeList temp) {
		buyFeeService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer fel_id) {
		buyFeeService.temp_del(fel_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		buyFeeService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Buy_Fee fee,HttpSession session) {
		buyFeeService.save(fee, getUser(session));
		return ajaxSuccess(fee);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Buy_Fee fee,HttpSession session) {
		buyFeeService.update(fee, getUser(session));
		return ajaxSuccess(fee);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(buyFeeService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(buyFeeService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		buyFeeService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,HttpSession session) {
		Map<String, Object> resultMap = buyFeeService.loadPrintData(number, sp_id, getUser(session));
		resultMap.put("user", getUser(session));
		return ajaxSuccess(BuyFeeVO.buildPrintJson(resultMap));
	}
	
	@RequestMapping(value = "listReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listReport(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String fe_supply_code,
			@RequestParam(required = false) Integer fe_ar_state,
			@RequestParam(required = false) Integer fe_pay_state,
			@RequestParam(required = false) String timeType,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("type", type);
        param.put("fe_supply_code", fe_supply_code);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("fe_ar_state", fe_ar_state);
        param.put("fe_pay_state", fe_pay_state);
        param.put("timeType", timeType);
		return ajaxSuccess(buyFeeService.listReport(param));
	}
	
	@RequestMapping(value = "listReportDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listReportDetail(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String fe_supply_code,
			@RequestParam(required = false) String fel_mp_code,
			@RequestParam(required = false) Integer fe_ar_state,
			@RequestParam(required = false) Integer fe_pay_state,
			@RequestParam(required = false) String timeType,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("type", type);
        param.put("fe_supply_code", fe_supply_code);
        param.put("fel_mp_code", fel_mp_code);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("fe_ar_state", fe_ar_state);
        param.put("fe_pay_state", fe_pay_state);
        param.put("timeType", timeType);
		return ajaxSuccess(buyFeeService.listReportDetail(param));
	}
	
}
