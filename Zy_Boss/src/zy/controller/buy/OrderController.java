package zy.controller.buy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.buy.order.T_Buy_Order;
import zy.entity.buy.order.T_Buy_OrderList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.buy.order.OrderService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.buy.BuyVO;

@Controller
@RequestMapping("buy/order")
public class OrderController extends BaseController{
	@Resource
	private OrderService orderService;
	
	@RequestMapping(value = "to_list/{od_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable(value = "od_type")Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "buy/order/list";
	}
	@RequestMapping(value = "to_list_draft_dialog/{od_type}", method = RequestMethod.GET)
	public String to_list_draft_dialog(@PathVariable(value = "od_type")Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "buy/order/list_draft_dialog";
	}
	@RequestMapping(value = "to_add/{od_type}", method = RequestMethod.GET)
	public String to_add(@PathVariable(value = "od_type")Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "buy/order/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer od_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Buy_Order order = orderService.load(od_id);
		if(null != order){
			orderService.initUpdate(order.getOd_number(), order.getOd_type(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("order",order);
		}
		return "buy/order/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer od_id,Model model) {
		T_Buy_Order order = orderService.load(od_id);
		if(null != order){
			model.addAttribute("order",order);
		}
		return "buy/order/view";
	}
	@RequestMapping(value = "to_select_product/{od_type}", method = RequestMethod.GET)
	public String to_select_product(@PathVariable(value = "od_type")Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "buy/order/select_product";
	}
	@RequestMapping(value = "to_temp_update/{od_type}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable(value = "od_type")Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "buy/order/temp_update";
	}
	@RequestMapping(value = "to_import/{od_type}", method = RequestMethod.GET)
	public String to_import(@PathVariable(value = "od_type")Integer od_type,Model model) {
		model.addAttribute("od_type", od_type);
		return "buy/order/import";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer od_type,
			@RequestParam(required = true) Integer od_isdraft,
			@RequestParam(required = false) Integer od_ar_state,
			@RequestParam(required = false) String od_supply_code,
			@RequestParam(required = false) String od_depot_code,
			@RequestParam(required = false) String od_manager,
			@RequestParam(required = false) String od_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("od_type", od_type);
        param.put("od_isdraft", od_isdraft);
        param.put("od_ar_state", od_ar_state);
        param.put("od_supply_code", od_supply_code);
        param.put("od_depot_code", od_depot_code);
        param.put("od_manager", StringUtil.decodeString(od_manager));
        param.put("od_number", StringUtil.decodeString(od_number));
		return ajaxSuccess(orderService.page(param));
	}
	
	@RequestMapping(value = "page4Enter", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page4Enter(PageForm pageForm,
			@RequestParam(required = true) Integer od_type,
			@RequestParam(required = false) String od_supply_code,
			@RequestParam(required = false) String od_depot_code,
			@RequestParam(required = false) String od_manager,
			@RequestParam(required = false) String od_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("od_type", od_type);
        param.put("od_supply_code", od_supply_code);
        param.put("od_depot_code", od_depot_code);
        param.put("od_manager", StringUtil.decodeString(od_manager));
        param.put("od_number", StringUtil.decodeString(od_number));
		return ajaxSuccess(orderService.page4Enter(param));
	}
	
	@RequestMapping(value = "detail_list/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable(value = "od_number")String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(orderService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable(value = "od_number")String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(orderService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable(value = "od_number")String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(orderService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{od_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable(value = "od_number")String od_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_number", od_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(orderService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_list(@PathVariable(value = "od_type")Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_type", od_type);
		params.put("odl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(orderService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_sum(@PathVariable(value = "od_type")Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(orderService.temp_sum(od_type,user.getUs_id(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_size_title/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size_title(@PathVariable(value = "od_type")Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_type", od_type);
		params.put("odl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(orderService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size(@PathVariable(value = "od_type")Integer od_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("odl_type", od_type);
		params.put("odl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(orderService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(@PathVariable(value = "od_type")Integer od_type,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = true) Integer priceType,
			@RequestParam(required = true) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("user", user);
		Map<String, Object> resultMap = orderService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page_product(PageForm pageForm,
			@RequestParam(required = true) String od_type,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("od_type", od_type);
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(orderService.page_product(param));
	}

	@RequestMapping(value = "temp_loadproduct/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_loadproduct(@PathVariable(value = "od_type")Integer od_type,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String gift,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("od_type", od_type);
		params.put("pd_code", pd_code);
		params.put("odl_pi_type", gift);
		params.put("exist", exist);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(orderService.temp_loadproduct(params));
	}

	@RequestMapping(value = "temp_save/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@PathVariable(value = "od_type")Integer od_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Buy_OrderList> temps = BuyVO.convertMap2Model_Order(data, od_type, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		orderService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer odl_id) {
		orderService.temp_del(odl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Buy_OrderList temp) {
		orderService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(@PathVariable(value = "od_type")Integer od_type,T_Buy_OrderList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setOdl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setOdl_type(od_type);
		orderService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Buy_OrderList temp) {
		temp.setOdl_remark(StringUtil.decodeString(temp.getOdl_remark()));
		orderService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(@PathVariable(value = "od_type")Integer od_type,T_Buy_OrderList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setOdl_remark(StringUtil.decodeString(temp.getOdl_remark()));
		temp.setOdl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setOdl_type(od_type);
		orderService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@PathVariable(value = "od_type")Integer od_type,
			@RequestParam String pd_code,@RequestParam Integer odl_pi_type,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Buy_OrderList temp = new T_Buy_OrderList();
		temp.setOdl_pd_code(pd_code);
		temp.setOdl_cr_code(cr_code);
		temp.setOdl_br_code(br_code);
		temp.setOdl_pi_type(odl_pi_type);
		temp.setOdl_type(od_type);
		temp.setOdl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		orderService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(@PathVariable(value = "od_type") Integer od_type, HttpSession session) {
		T_Sys_User user = getUser(session);
		orderService.temp_clear(od_type, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Buy_Order order,HttpSession session) {
		orderService.save(order, getUser(session));
		return ajaxSuccess(order);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Buy_Order order,HttpSession session) {
		orderService.update(order, getUser(session));
		return ajaxSuccess(order);
	}
	
	@RequestMapping(value = "temp_import/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@PathVariable(value = "od_type")Integer od_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = BuyVO.convertMap2Model_import(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("datas", datas);
		params.put("user", user);
		params.put("priceType", data.get("priceType"));
		params.put("sp_rate", data.get("sp_rate"));
		orderService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft/{od_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@PathVariable(value = "od_type")Integer od_type,@RequestParam String od_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("od_type", od_type);
		params.put("od_number", od_number);
		params.put("user", user);
		orderService.temp_import_draft(params);
		return ajaxSuccess();
	}

	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Buy_Order order = orderService.approve(number, record, getUser(session));
		return ajaxSuccess(order);
	}
	
	@RequestMapping(value = "stop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object stop(String number,HttpSession session) {
		return ajaxSuccess(orderService.stop(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		orderService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}

	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = orderService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = BuyVO.buildPrintJson_Order(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
