package zy.controller.buy;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.buy.supply.T_Buy_Supply;
import zy.form.PageForm;
import zy.service.buy.supply.SupplyService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("buy/supply")
public class SupplyController extends BaseController{
	
	@Resource
	private SupplyService supplyService;
	
	@RequestMapping("to_all")
	public String to_all() {
		return "buy/supply/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "buy/supply/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "buy/supply/list";
	}
	@RequestMapping(value = "to_money", method = RequestMethod.GET)
	public String to_money() {
		return "buy/supply/money";
	}
	@RequestMapping(value = "to_money_details", method = RequestMethod.GET)
	public String to_money_details() {
		return "buy/supply/money_details";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "buy/supply/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer sp_id,Model model) {
		T_Buy_Supply supply = supplyService.queryByID(sp_id);
		if(null != supply){
			model.addAttribute("supply",supply);
		}
		return "buy/supply/update";
	}
	/**
	 * 查询供货单位弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "buy/supply/list_dialog";
	}
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(String searchContent, String ar_upcode,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("ar_upcode", ar_upcode);
		return ajaxSuccess(supplyService.list(param));
	}
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Buy_Supply supply,HttpSession session) {
		supply.setCompanyid(getCompanyid(session));
		supply.setSp_spell(StringUtil.getSpell(supply.getSp_name()));
		supplyService.save(supply);
		return ajaxSuccess(supply);
	}
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Buy_Supply supply,HttpSession session) {
		supply.setCompanyid(getCompanyid(session));
		supply.setSp_spell(StringUtil.getSpell(supply.getSp_name()));
		supplyService.update(supply);
		return ajaxSuccess(supply);
	}
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer sp_id,@RequestParam String sp_code,HttpSession session) {
		Integer companyid = getCompanyid(session);
		supplyService.del(sp_id,sp_code,companyid);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "pageMoneyDetails", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageMoneyDetails(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String type,
			@RequestParam(required = false) String pay_state,
			@RequestParam(required = false) String supply_code,
			@RequestParam(required = false) String manager,
			@RequestParam(required = false) String number,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("type", type);
        params.put("pay_state", pay_state);
        params.put("supply_code", supply_code);
        params.put("manager", StringUtil.decodeString(manager));
        params.put("number", StringUtil.decodeString(number));
		return ajaxSuccess(supplyService.pageMoneyDetails(params));
	}
	
}
