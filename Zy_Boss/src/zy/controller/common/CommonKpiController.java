package zy.controller.common;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.common.kpi.CommonKpiService;
import zy.util.CommonUtil;

@Controller
@RequestMapping(value="common/kpi")
public class CommonKpiController extends BaseController{

	@Resource
	private CommonKpiService commonKpiService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(commonKpiService.list(params));
	}
	
	
}
