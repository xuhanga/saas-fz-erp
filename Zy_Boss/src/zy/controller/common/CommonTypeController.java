package zy.controller.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.common.type.Common_Type;
import zy.entity.sys.user.T_Sys_User;
import zy.service.common.type.CommonTypeService;
import zy.util.CommonUtil;

@Controller
@RequestMapping(value="/common/type")
public class CommonTypeController extends BaseController{
	@Resource
	private CommonTypeService commonTypeService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.VERSION, user.getCo_ver());
		List<Common_Type> sizeList = commonTypeService.list(param);
		return ajaxSuccess(sizeList, "查询成功, 共" + sizeList.size() + "条数据");
	}
	@RequestMapping(value = "sub_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object sub_list(HttpServletRequest request,HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.VERSION, user.getCo_ver());
		List<Common_Type> sizeList = commonTypeService.sub_list(param);
		return ajaxSuccess(sizeList, "查询成功, 共" + sizeList.size() + "条数据");
	}
	
}
