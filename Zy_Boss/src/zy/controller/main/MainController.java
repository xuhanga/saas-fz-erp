package zy.controller.main;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.interceptor.CookieHandler;
import zy.interceptor.SessionHandler;
import zy.service.common.log.CommonLogService;
import zy.service.main.MainService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;

@Controller
public class MainController extends BaseController{
	@Autowired
	private MainService mainService;
	@Resource
	private CommonLogService commonLogService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	@RequestMapping(value = "/toIndex", method = RequestMethod.GET)
	public String toIndex() {
		return "to_index";
	}
	/**
	 * 管理员主页面
	 * */
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String main(HttpServletRequest request) {
		request.setAttribute("today", DateUtil.getYearMonthDate());
		return "index/main";
	}
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public String info() {
		return "index/info";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("logs", commonLogService.list());
		return "index/home";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody Object login(HttpServletResponse response,HttpServletRequest request,HttpSession session) {
		String us_account = request.getParameter("us_account");
		String us_pass = request.getParameter("us_pass");
		String co_code = request.getParameter("co_code");
		String remember = request.getParameter("remember");
		Map<String,Object> paramMap = new HashMap<String,Object>(3);
		paramMap.put("us_account", us_account);
		paramMap.put("us_pass", MD5.encryptMd5(us_pass));
		paramMap.put("co_code", co_code);
		mainService.login(paramMap);
		T_Sys_User user = (T_Sys_User)paramMap.get(CommonUtil.KEY_USER);
		session.setAttribute(CommonUtil.KEY_USER, user);
		if(null != user){
			session.setAttribute(CommonUtil.KEY_MENU, paramMap.get(CommonUtil.KEY_MENU));
			session.setAttribute(CommonUtil.KEY_MENULIMIT, paramMap.get(CommonUtil.KEY_MENULIMIT));
			session.setAttribute(CommonUtil.KEY_SYSSET, paramMap.get(CommonUtil.KEY_SYSSET));
			SessionHandler.kickUser(user.getCompanyid()+""+user.getUs_id(), session);
			paramMap.remove(CommonUtil.KEY_MENU);
			paramMap.remove(CommonUtil.KEY_MENULIMIT);
			paramMap.remove(CommonUtil.KEY_SYSSET);
			paramMap.remove(CommonUtil.KEY_USER);
		}
		Map<String,String> cookie = CookieHandler.getCookies(request);
		if(null == cookie || null == cookie.get("ca_remember")){
			cookie = new HashMap<String,String>();
			cookie.put("us_co_code",co_code);
			cookie.put("us_account",us_account);
			cookie.put("us_remember",remember);
			CookieHandler.createCookie(response,cookie,10);
		}else{
			cookie.put("us_co_code", co_code);
			cookie.put("us_account", us_account);
			cookie.put("us_remember",remember);
			CookieHandler.createCookie(response,cookie,10);
		}
		return returnResult(user);
	}
	@RequestMapping(value = "/logout", method = {RequestMethod.GET,RequestMethod.POST})
	public String logout(HttpSession session) {
		T_Sys_User user = (T_Sys_User)session.getAttribute(CommonUtil.KEY_USER);
		if(user != null){
			SessionHandler.delUserFromSession(user.getCompanyid()+""+user.getUs_id());
		}
		Enumeration<?> keys = session.getAttributeNames();
		String key;
		for (;keys.hasMoreElements();){
			key = (String)keys.nextElement();
			session.removeAttribute(key);
		}
		if(session != null)session.invalidate();
		return "redirect:/index";
	}
}
