package zy.controller.money;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.access.T_Money_Access;
import zy.entity.money.access.T_Money_AccessList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.money.access.AccessService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.money.AccessVO;

@Controller
@RequestMapping("money/access")
public class AccessController extends BaseController{
	@Resource
	private AccessService accessService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "money/access/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "money/access/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ac_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Money_Access access = accessService.load(ac_id);
		if(null != access){
			accessService.initUpdate(access.getAc_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("access",access);
		}
		return "money/access/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ac_id,Model model) {
		T_Money_Access access = accessService.load(ac_id);
		if(null != access){
			model.addAttribute("access",access);
		}
		return "money/access/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer ac_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ac_manager,
			@RequestParam(required = false) String ac_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put("ac_ar_state", ac_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ac_manager", StringUtil.decodeString(ac_manager));
        param.put("ac_number", StringUtil.decodeString(ac_number));
		return ajaxSuccess(accessService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{ac_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String ac_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_number", ac_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(accessService.detail_list(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("acl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(accessService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestParam String temps,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Money_AccessList> accessLists = JSON.parseArray(temps, T_Money_AccessList.class);
		accessService.temp_save(accessLists, user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateMoney(T_Money_AccessList temp) {
		accessService.temp_updateMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateType(T_Money_AccessList temp) {
		accessService.temp_updateType(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Money_AccessList temp) {
		accessService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer acl_id) {
		accessService.temp_del(acl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("acl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		accessService.temp_clear(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Money_Access access,HttpSession session) {
		accessService.save(access, getUser(session));
		return ajaxSuccess(access);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Money_Access access,HttpSession session) {
		accessService.update(access, getUser(session));
		return ajaxSuccess(access);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(accessService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(accessService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		accessService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,HttpSession session) {
		Map<String, Object> resultMap = accessService.loadPrintData(number, sp_id, getUser(session));
		resultMap.put("user", getUser(session));
		return ajaxSuccess(AccessVO.buildPrintJson(resultMap));
	}
	
	
}
