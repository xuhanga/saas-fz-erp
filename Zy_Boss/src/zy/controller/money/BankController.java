package zy.controller.money;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.money.bank.T_Money_Bank;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.money.bank.BankService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("money/bank")
public class BankController extends BaseController{
	
	@Resource
	private BankService bankService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "money/bank/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "money/bank/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ba_id,Model model,HttpSession session) {
		T_Money_Bank bank = bankService.queryByID(ba_id);
		if(null != bank){
			model.addAttribute("bank",bank);
		}
		model.addAttribute("sp_init", getUser(session).getSp_init());
		return "money/bank/update";
	}
	/**
	 * 查询银行账户弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list_dialog")
	public String to_list_sub_dialog() {
		return "money/bank/list_dialog";
	}
	@RequestMapping(value = "to_run", method = RequestMethod.GET)
	public String to_run() {
		return "money/bank/run";
	}
	@RequestMapping(value = "to_report", method = RequestMethod.GET)
	public String to_report() {
		return "money/bank/report";
	}
	@RequestMapping(value = "to_report_sum", method = RequestMethod.GET)
	public String to_report_sum() {
		return "money/bank/report_sum";
	}
	@RequestMapping(value = "to_runs", method = RequestMethod.GET)
	public String to_runs() {
		return "money/bank/runs";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(String searchContent, HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("searchContent", StringUtil.decodeString(searchContent));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(bankService.list(param));
	}
	
	@RequestMapping(value = "pageRun", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageRun(PageForm pageForm,
			@RequestParam(required = false) String ba_code,
			@RequestParam(required = false) String br_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put("ba_code", ba_code);
        param.put("br_number", StringUtil.decodeString(br_number));
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
		return ajaxSuccess(bankService.pageRun(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Money_Bank bank,HttpSession session) {
		T_Sys_User user = getUser(session);
		bank.setCompanyid(getCompanyid(session));
		bank.setBa_spell(StringUtil.getSpell(bank.getBa_name()));
		bank.setBa_balance(0d);
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){
			bank.setBa_shop_code(user.getUs_shop_code());
		}else {
			bank.setBa_shop_code(user.getShop_upcode());
		}
		bankService.save(bank);
		return ajaxSuccess(bank);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Money_Bank bank,HttpSession session) {
		bank.setCompanyid(getCompanyid(session));
		bank.setBa_spell(StringUtil.getSpell(bank.getBa_name()));
		bankService.update(bank);
		return ajaxSuccess(bank);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ba_id) {
		bankService.del(ba_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "report_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object report_sum(PageForm pageForm,
			@RequestParam(required = false) String ba_code,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String type,
			@RequestParam(required = false) String br_manager,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        param.put("ba_code", ba_code);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("type", type);
        param.put("br_manager", StringUtil.decodeString(br_manager));
		return ajaxSuccess(bankService.report_sum(param));
	}
	
}
