package zy.controller.money;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.expense.T_Money_Expense;
import zy.entity.money.expense.T_Money_ExpenseList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.money.expense.ExpenseService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.money.ExpenseVO;

@Controller
@RequestMapping("money/expense")
public class ExpenseController extends BaseController{
	@Resource
	private ExpenseService expenseService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "money/expense/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "money/expense/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ep_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Money_Expense expense = expenseService.load(ep_id);
		if(null != expense){
			expenseService.initUpdate(expense.getEp_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("expense",expense);
		}
		return "money/expense/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ep_id,Model model) {
		T_Money_Expense expense = expenseService.load(ep_id);
		if(null != expense){
			model.addAttribute("expense",expense);
		}
		return "money/expense/view";
	}
	@RequestMapping(value = "to_report", method = RequestMethod.GET)
	public String to_report() {
		return "money/expense/report";
	}
	
	@RequestMapping(value = "to_report_expense/{type}", method = RequestMethod.GET)
	public String to_report_expense(@PathVariable String type,Model model) {
		model.addAttribute("type", type);
		return "money/expense/report_expense";
	}
	@RequestMapping(value = "to_report_expense_department", method = RequestMethod.GET)
	public String to_report_expense_department() {
		return "money/expense/report_expense_department";
	}
	
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer ep_ar_state,
			@RequestParam(required = false) String ep_ba_code,
			@RequestParam(required = false) String ep_shop_code,
			@RequestParam(required = false) String ep_manager,
			@RequestParam(required = false) String ep_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("ep_ar_state", ep_ar_state);
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("ep_ba_code", ep_ba_code);
        param.put("ep_shop_code", ep_shop_code);
        param.put("ep_manager", StringUtil.decodeString(ep_manager));
        param.put("ep_number", StringUtil.decodeString(ep_number));
		return ajaxSuccess(expenseService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{ep_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String ep_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("epl_number", ep_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(expenseService.detail_list(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("epl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(expenseService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestParam String temps,HttpSession session) {
		T_Sys_User user = getUser(session);
		expenseService.temp_save(JSON.parseArray(temps, T_Money_ExpenseList.class), user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateMoney(T_Money_ExpenseList temp) {
		expenseService.temp_updateMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateSharemonth", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateSharemonth(T_Money_ExpenseList temp) {
		expenseService.temp_updateSharemonth(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Money_ExpenseList temp) {
		expenseService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer epl_id) {
		expenseService.temp_del(epl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		expenseService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Money_Expense expense,HttpSession session) {
		expenseService.save(expense, getUser(session));
		return ajaxSuccess(expense);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Money_Expense expense,HttpSession session) {
		expenseService.update(expense, getUser(session));
		return ajaxSuccess(expense);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(expenseService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "approveBatch", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approveBatch(T_Approve_Record record,String numbers,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		expenseService.approveBatch(numbers, record, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(expenseService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String numbers,HttpSession session) {
		expenseService.del(numbers, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,HttpSession session) {
		Map<String, Object> resultMap = expenseService.loadPrintData(number, sp_id, getUser(session));
		resultMap.put("user", getUser(session));
		return ajaxSuccess(ExpenseVO.buildPrintJson(resultMap));
	}
	
	@RequestMapping(value = "listReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listReport(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String ep_shop_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("type", type);
        param.put("ep_shop_code", ep_shop_code);
		return ajaxSuccess(expenseService.listReport(param));
	}
	
	@RequestMapping(value = "listReportDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listReportDetail(PageForm pageForm,
			@RequestParam(required = false) String month,
			@RequestParam(required = false) String epl_mp_code,
			@RequestParam(required = false) String ep_shop_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", pageForm.getBegindate());
        param.put("enddate", pageForm.getEnddate());
        param.put("month", month);
        param.put("epl_mp_code", epl_mp_code);
        param.put("ep_shop_code", ep_shop_code);
		return ajaxSuccess(expenseService.listReportDetail(param));
	}
	
	@RequestMapping(value = "expense_head", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object expense_head(@RequestParam(required = true) Integer year,
			HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("year", year);
        params.put("begindate", year+"-01-01");
        params.put("enddate", year+"-12-31 23:59:59");
		return ajaxSuccess(expenseService.expense_head(params));
	}
	
	@RequestMapping(value = "expense_shop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object expense_shop(@RequestParam(required = true) Integer year,
			@RequestParam(required = false) String shopCode,
			HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("shopCode", shopCode);
        params.put("year", year);
        params.put("begindate", year+"-01-01");
        params.put("enddate", year+"-12-31 23:59:59");
		return ajaxSuccess(expenseService.expense_shop(params));
	}
	
	@RequestMapping(value = "expense_department", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object expense_department(
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String mp_code,
			@RequestParam(required = false) String dm_code,
			HttpSession session){
		T_Sys_User user = getUser(session);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("mp_code", mp_code);
        params.put("dm_code", dm_code);
		return ajaxSuccess(expenseService.expense_department(params));
	}
	
}
