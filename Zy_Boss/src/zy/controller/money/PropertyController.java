package zy.controller.money;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.money.property.T_Money_Property;
import zy.entity.sys.user.T_Sys_User;
import zy.service.money.property.PropertyService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("money/property")
public class PropertyController extends BaseController{
	@Resource
	private PropertyService propertyService;
	
	@RequestMapping(value = "to_list/{pp_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer pp_type,Model model) {
		model.addAttribute("pp_type", pp_type);
		return "money/property/list";
	}
	@RequestMapping(value = "/to_add/{pp_type}", method = RequestMethod.GET)
	public String to_add(@PathVariable Integer pp_type,Model model) {
		model.addAttribute("pp_type", pp_type);
		return "money/property/add";
	}
	
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pp_id,Model model) {
		T_Money_Property property = propertyService.queryByID(pp_id);
		if(null != property){
			model.addAttribute("property",property);
		}
		return "money/property/update";
	}
	
	@RequestMapping("to_list_dialog/{pp_type}")
	public String to_list_dialog(@PathVariable Integer pp_type,Model model) {//类型:0-费用类型，1-其他收入
		model.addAttribute("pp_type", pp_type);
		return "money/property/list_dialog";
	}
	
	@RequestMapping(value = "list/{pp_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(@PathVariable(value = "pp_type")Integer pp_type,String searchContent, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put("pp_type", pp_type);
		return ajaxSuccess(propertyService.list(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Money_Property property,HttpSession session) {
		T_Sys_User user = getUser(session);
		property.setCompanyid(getCompanyid(session));
		property.setPp_spell(StringUtil.getSpell(property.getPp_name()));
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){
			property.setPp_shop_code(user.getUs_shop_code());
		}else {
			property.setPp_shop_code(user.getShop_upcode());
		}
		propertyService.save(property);
		return ajaxSuccess(property);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Money_Property property,HttpSession session) {
		property.setCompanyid(getCompanyid(session));
		property.setPp_spell(StringUtil.getSpell(property.getPp_name()));
		propertyService.update(property);
		return ajaxSuccess(property);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer pp_id) {
		propertyService.del(pp_id);
		return ajaxSuccess();
	}
}
