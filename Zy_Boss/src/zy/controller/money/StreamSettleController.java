package zy.controller.money;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.money.stream.T_Money_StreamSettle;
import zy.entity.money.stream.T_Money_StreamSettleList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.money.stream.StreamSettleService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("money/streamsettle")
public class StreamSettleController extends BaseController{
	@Resource
	private StreamSettleService streamSettleService;
	
	@RequestMapping(value = "to_stream", method = RequestMethod.GET)
	public String to_stream() {
		return "money/streamsettle/stream";
	}
	@RequestMapping(value = "to_bill", method = RequestMethod.GET)
	public String to_bill() {
		return "money/streamsettle/bill";
	}
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "money/streamsettle/list";
	}
	@RequestMapping(value = "to_list_detail", method = RequestMethod.GET)
	public String to_list_detail() {
		return "money/streamsettle/list_detail";
	}
	@RequestMapping(value = "to_add/{se_code}", method = RequestMethod.GET)
	public String to_add(@PathVariable String se_code,Model model,HttpSession session) {
		model.addAttribute("stream",streamSettleService.loadStream(se_code, getCompanyid(session)));
		return "money/streamsettle/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer st_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Money_StreamSettle settle = streamSettleService.load(st_id);
		if(null != settle){
			streamSettleService.initUpdate(settle.getSt_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("settle",settle);
			model.addAttribute("stream",streamSettleService.loadStream(settle.getSt_stream_code(), settle.getCompanyid()));
		}
		return "money/streamsettle/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer st_id,Model model) {
		T_Money_StreamSettle settle = streamSettleService.load(st_id);
		if(null != settle){
			model.addAttribute("settle",settle);
			model.addAttribute("stream",streamSettleService.loadStream(settle.getSt_stream_code(), settle.getCompanyid()));
		}
		return "money/streamsettle/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Money_StreamSettle settle = streamSettleService.load(number, getCompanyid(session));
		if(null != settle){
			model.addAttribute("settle",settle);
			model.addAttribute("stream",streamSettleService.loadStream(settle.getSt_stream_code(), settle.getCompanyid()));
		}
		return "money/streamsettle/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer st_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String st_stream_code,
			@RequestParam(required = false) String st_manager,
			@RequestParam(required = false) String st_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("st_ar_state", st_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("st_stream_code", st_stream_code);
        param.put("st_manager", StringUtil.decodeString(st_manager));
        param.put("st_number", StringUtil.decodeString(st_number));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(streamSettleService.page(param));
	}
	
	@RequestMapping(value = "pageBill", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageBill(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String seb_se_code,
			@RequestParam(required = false) String seb_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("seb_se_code", seb_se_code);
        param.put("seb_number", StringUtil.decodeString(seb_number));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype())){//总公司、分公司
        	param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、加盟、合伙
			param.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(streamSettleService.pageBill(param));
	}
	
	@RequestMapping(value = "detail_list/{st_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String st_number,HttpSession session) {
		return ajaxSuccess(streamSettleService.detail_list(st_number,getCompanyid(session)));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("stl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(streamSettleService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestParam String se_code,HttpSession session) {
		streamSettleService.temp_save(se_code, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateDiscountMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateDiscountMoney(T_Money_StreamSettleList temp) {
		streamSettleService.temp_updateDiscountMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRealMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRealMoney(T_Money_StreamSettleList temp) {
		streamSettleService.temp_updateRealMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Money_StreamSettleList temp) {
		streamSettleService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_entire", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_entire(Double discount_money,Double paid,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("discount_money", discount_money);
		params.put("paid", paid);
		params.put("user", getUser(session));
		streamSettleService.temp_entire(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Money_StreamSettle settle,HttpSession session) {
		streamSettleService.save(settle, getUser(session));
		return ajaxSuccess(settle);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Money_StreamSettle settle,HttpSession session) {
		streamSettleService.update(settle, getUser(session));
		return ajaxSuccess(settle);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(streamSettleService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(streamSettleService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		streamSettleService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
}
