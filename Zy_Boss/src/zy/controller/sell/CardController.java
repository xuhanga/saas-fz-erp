package zy.controller.sell;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.card.T_Sell_Card;
import zy.entity.sell.card.T_Sell_CardList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sell.card.CardService;
import zy.util.CommonUtil;
import zy.util.MD5;
import zy.util.StringUtil;

@Controller
@RequestMapping("sell/card")
public class CardController extends BaseController{
	@Resource
	private CardService cardService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sell/card/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sell/card/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer cd_id,Model model) {
		T_Sell_Card card = cardService.queryByID(cd_id);
		if(null != card){
			model.addAttribute("card",card);
		}
		return "sell/card/update";
	}
	@RequestMapping(value = "/to_recharge", method = RequestMethod.GET)
	public String to_recharge(@RequestParam Integer cd_id,Model model) {
		T_Sell_Card card = cardService.queryByID(cd_id);
		if(null != card){
			model.addAttribute("card",card);
		}
		return "sell/card/recharge";
	}
	@RequestMapping(value = "/to_detail", method = RequestMethod.GET)
	public String to_detail() {
		return "sell/card/detail";
	}
	@RequestMapping(value = "/to_details", method = RequestMethod.GET)
	public String to_details() {
		return "sell/card/details";
	}
	
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String cd_shop_code,
			@RequestParam(required = false) String cd_cardcode,
			@RequestParam(required = false) String cd_name,
			@RequestParam(required = false) String cd_mobile,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("cd_shop_code", cd_shop_code);
        param.put("cd_cardcode", StringUtil.decodeString(cd_cardcode));
        param.put("cd_name", StringUtil.decodeString(cd_name));
        param.put("cd_mobile", StringUtil.decodeString(cd_mobile));
		return ajaxSuccess(cardService.page(param));
	}
	
	@RequestMapping(value = "pageDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageDetail(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String cdl_shop_code,
			@RequestParam(required = false) String cdl_cardcode,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("cdl_shop_code", cdl_shop_code);
        param.put("cdl_cardcode", StringUtil.decodeString(cdl_cardcode));
		return ajaxSuccess(cardService.pageDetail(param));
	}
	
	@RequestMapping(value = "listDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listDetail(@RequestParam(required = true) String cd_code,HttpSession session) {
		return ajaxSuccess(cardService.listDetail(cd_code, getCompanyid(session)));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Card card,String start_no,Integer add_count,String mark,Integer prefix,
			String cdl_ba_code,String cdl_bank_code,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		card.setCompanyid(getCompanyid(session));
		card.setCd_state(0);
		card.setCd_used_money(0d);
		card.setCd_cashrate((card.getCd_realcash()+card.getCd_bankmoney())/card.getCd_money());
		card.setCd_pass(MD5.encryptMd5(CommonUtil.INIT_PWD));
		
		params.put("card", card);
		params.put("start_no", start_no);
		params.put("add_count", add_count);
		params.put("mark", mark);
		params.put("prefix", prefix);
		params.put("cdl_ba_code", cdl_ba_code);
		params.put("cdl_bank_code", cdl_bank_code);
		cardService.save(params);
		return ajaxSuccess(card);
	}
	
	
	@RequestMapping(value = "updateState", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateState(T_Sell_Card card) {
		cardService.updateState(card);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_Card card) {
		cardService.update(card);
		return ajaxSuccess(card);
	}
	
	@RequestMapping(value = "recharge", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object recharge(T_Sell_CardList cardList,Integer cd_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		cardList.setCompanyid(user.getCompanyid());
		cardList.setCdl_manager(user.getUs_name());
		cardList.setCdl_shop_code(user.getUs_shop_code());
		return ajaxSuccess(cardService.recharge(cardList, cd_id));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer cd_id) {
		cardService.del(cd_id);
		return ajaxSuccess();
	}
}
