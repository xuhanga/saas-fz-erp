package zy.controller.sell;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.displayarea.T_Sell_DisplayArea;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sell.displayarea.DisplayAreaService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("sell/displayarea")
public class DisplayAreaController extends BaseController{
	
	@Resource
	private DisplayAreaService displayAreaService;
	
	@RequestMapping("to_all")
	public String to_all() {
		return "sell/displayarea/all";
	}
	@RequestMapping("to_tree")
	public String to_tree() {
		return "sell/displayarea/tree";
	}
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sell/displayarea/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sell/displayarea/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer da_id,Model model) {
		T_Sell_DisplayArea displayArea = displayAreaService.queryByID(da_id);
		if(null != displayArea){
			model.addAttribute("displayArea",displayArea);
		}
		return "sell/displayarea/update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,String sp_code,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("sp_code", sp_code);
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
		return ajaxSuccess(displayAreaService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_DisplayArea displayArea,HttpSession session) {
		displayArea.setCompanyid(getCompanyid(session));
		displayAreaService.save(displayArea);
		return ajaxSuccess(displayArea);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_DisplayArea displayArea,HttpSession session) {
		displayArea.setCompanyid(getCompanyid(session));
		displayAreaService.update(displayArea);
		return ajaxSuccess(displayArea);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer da_id) {
		displayAreaService.del(da_id);
		return ajaxSuccess();
	}
	
}
