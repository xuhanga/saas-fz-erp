package zy.controller.sell;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sell.ecoupon.T_Sell_ECoupon;
import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sell.ecoupon.ECouponService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sell/ecoupon")
public class ECouponController extends BaseController{
	
	@Resource
	private ECouponService eCouponService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sell/ecoupon/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sell/ecoupon/add";
	}
	@RequestMapping(value = "/to_report", method = RequestMethod.GET)
	public String to_report() {
		return "sell/ecoupon/report";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ec_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Sell_ECoupon eCoupon = eCouponService.load(ec_id);
		if(null != eCoupon){
			eCouponService.initUpdate(eCoupon.getEc_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("eCoupon",eCoupon);
		}
		return "sell/ecoupon/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ec_id,Model model) {
		T_Sell_ECoupon eCoupon = eCouponService.load(ec_id);
		if(null != eCoupon){
			model.addAttribute("eCoupon",eCoupon);
		}
		return "sell/ecoupon/view";
	}
	@RequestMapping(value = "to_temp_add/{ec_type}", method = RequestMethod.GET)
	public String to_temp_add(@PathVariable Integer ec_type,Model model) {
		model.addAttribute("ec_type",ec_type);
		return "sell/ecoupon/temp_add";
	}
	@RequestMapping(value = "to_temp_update/{ec_type}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable Integer ec_type,@RequestParam Integer ecl_id,Model model) {
		T_Sell_ECouponList temp = eCouponService.temp_load(ecl_id);
		if(null != temp){
			model.addAttribute("temp",temp);
		}
		model.addAttribute("ec_type",ec_type);
		return "sell/ecoupon/temp_update";
	}
	
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) Integer ec_ar_state,
			@RequestParam(required = false) String ec_manager,
			@RequestParam(required = false) String ec_name,
			@RequestParam(required = false) String ec_number,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ec_ar_state", ec_ar_state);
        param.put("ec_manager", StringUtil.decodeString(ec_manager));
        param.put("ec_name", StringUtil.decodeString(ec_name));
        param.put("ec_number", StringUtil.decodeString(ec_number));
		return ajaxSuccess(eCouponService.page(param));
	}
	
	@RequestMapping(value = "listDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listDetail(@RequestParam(required = true) String number,HttpSession session) {
		return ajaxSuccess(eCouponService.listDetail(number, getCompanyid(session)));
	}
	
	@RequestMapping(value = "temp", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp(HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(eCouponService.temp(user.getUs_id(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(T_Sell_ECouponList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setCompanyid(user.getCompanyid());
		temp.setEcl_us_id(user.getUs_id());
		eCouponService.temp_save(temp);
		return ajaxSuccess(temp);
	}
	
	@RequestMapping(value = "temp_update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_update(T_Sell_ECouponList temp) {
		eCouponService.temp_update(temp);
		return ajaxSuccess(temp);
	}

	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer ecl_id) {
		eCouponService.temp_del(ecl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_ECoupon eCoupon,HttpSession session) {
		eCoupon.setCompanyid(getCompanyid(session));
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("user", user);
		eCouponService.save(eCoupon, params);
		return ajaxSuccess(eCoupon);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_ECoupon eCoupon,HttpSession session) {
		eCoupon.setCompanyid(getCompanyid(session));
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("user", user);
		eCouponService.update(eCoupon, params);
		return ajaxSuccess(eCoupon);
	}

	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Sell_ECoupon eCoupon = eCouponService.approve(number, record, getUser(session));
		return ajaxSuccess(eCoupon);
	}
	
	@RequestMapping(value = "stop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object stop(String number,String stop_cause,HttpSession session) {
		T_Sell_ECoupon eCoupon = eCouponService.stop(number, StringUtil.decodeString(stop_cause), getUser(session));
		return ajaxSuccess(eCoupon);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ec_id) {
		eCouponService.del(ec_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "report", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object report(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) Integer ecu_state,
			@RequestParam(required = false) Integer ecu_name,
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getUser(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ecu_state", ecu_state);
        param.put("ecu_name", ecu_name);
		return ajaxSuccess(eCouponService.report(param));
	}
	
}
