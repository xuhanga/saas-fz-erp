package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.shop.complaint.T_Shop_Complaint;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.complaint.ComplaintService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/complaint")
public class ComplaintController  extends BaseController{
	
	@Resource
	private ComplaintService complaintService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/complaint/list";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer sc_id,Model model,HttpSession session) {
		T_Shop_Complaint complaint = complaintService.load(sc_id);
		if(null != complaint){
			model.addAttribute("complaint",complaint);
		}
		return "shop/complaint/update";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String sc_satis = request.getParameter("sc_satis");
        String sc_state = request.getParameter("sc_state");
        String sc_type = request.getParameter("sc_type");
        String sc_username = request.getParameter("sc_username");
        String sc_processname = request.getParameter("sc_processname");
        String sc_shop_code = request.getParameter("sc_shop_code");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sc_satis", sc_satis);
        param.put("sc_state", sc_state);
        param.put("sc_type", sc_type);
        param.put("sc_username", StringUtil.decodeString(sc_username));
        param.put("sc_processname", StringUtil.decodeString(sc_processname));
        param.put("sc_shop_code", sc_shop_code);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Shop_Complaint> pageData = complaintService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Shop_Complaint complaint,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		complaintService.update(complaint,user);
		return ajaxSuccess();
	}
}
