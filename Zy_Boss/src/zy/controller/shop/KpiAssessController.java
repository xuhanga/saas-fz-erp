package zy.controller.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.shop.kpiassess.T_Shop_KpiAssess;
import zy.entity.shop.kpiassess.T_Shop_KpiAssessList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.kpiassess.KpiAssessService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/kpiassess")
public class KpiAssessController extends BaseController{
	
	@Resource
	private KpiAssessService kpiAssessService;
	
	@RequestMapping(value = "to_kpi_menu", method = RequestMethod.GET)
	public String to_kpi_menu() {
		return "shop/kpiassess/kpi_menu";
	}
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/kpiassess/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "shop/kpiassess/add";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ka_id,Model model) {
		T_Shop_KpiAssess kpiAssess = kpiAssessService.load(ka_id);
		if(null != kpiAssess){
			model.addAttribute("kpiAssess",kpiAssess);
		}
		return "shop/kpiassess/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) Integer ka_type,
			@RequestParam(required = false) Integer ka_state,
			@RequestParam(required = false) String ka_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", StringUtil.isEmpty(enddate)?"":enddate+" 23:59:59");
        param.put("ka_type", ka_type);
        param.put("ka_state", ka_state);
        param.put("ka_number", StringUtil.decodeString(ka_number));
		return ajaxSuccess(kpiAssessService.page(param));
	}
	
	@RequestMapping(value = "loadDetail/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadDetail(@PathVariable String number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("number", number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(kpiAssessService.loadDetail(params));
	}
	
	@RequestMapping(value = "statDetail/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statDetail(@PathVariable String number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("number", number);
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		return ajaxSuccess(kpiAssessService.statDetail(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(String kpiassess,String details,String kpis,HttpSession session) {
		kpiassess = StringUtil.decodeString(kpiassess);
		details = StringUtil.decodeString(details);
		kpis = StringUtil.trimString(kpis);
		T_Shop_KpiAssess kpiAssess = JSON.parseObject(kpiassess,T_Shop_KpiAssess.class);
		List<T_Shop_KpiAssessList> kpiDetails = JSON.parseArray(details,T_Shop_KpiAssessList.class);
		String[] kiCodes = kpis.split(",");
		List<T_Shop_KpiAssessList> kpiAssessLists = new ArrayList<T_Shop_KpiAssessList>();
		for (T_Shop_KpiAssessList item : kpiDetails) {
			for (String ki_code : kiCodes) {
				T_Shop_KpiAssessList assessList = new T_Shop_KpiAssessList();
				assessList.setKal_code(item.getKal_code());
				assessList.setKal_name(item.getKal_name());
				assessList.setKal_ki_code(ki_code);
				kpiAssessLists.add(assessList);
			}
		}
		kpiAssessService.save(kpiAssess, kpiAssessLists, getUser(session));
		return ajaxSuccess(kpiAssess);
	}
	
	@RequestMapping(value = "complete", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object complete(String kpiassess,String details,HttpSession session) {
		kpiassess = StringUtil.decodeString(kpiassess);
		details = StringUtil.decodeString(details);
		T_Shop_KpiAssess kpiAssess = JSON.parseObject(kpiassess,T_Shop_KpiAssess.class);
		List<T_Shop_KpiAssessList> kpiAssessLists = JSON.parseArray(details,T_Shop_KpiAssessList.class);
		return ajaxSuccess(kpiAssessService.complete(kpiAssess, kpiAssessLists));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(Integer ka_id,HttpSession session) {
		kpiAssessService.del(ka_id);
		return ajaxSuccess();
	}
	
}
