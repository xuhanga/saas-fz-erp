package zy.controller.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.shop.kpipk.T_Shop_KpiPk;
import zy.entity.shop.kpipk.T_Shop_KpiPkList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.kpipk.KpiPkService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/kpipk")
public class KpiPkController extends BaseController{

	@Resource
	private KpiPkService kpiPkService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/kpipk/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "shop/kpipk/add";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer kp_id,Model model) {
		T_Shop_KpiPk kpiPk = kpiPkService.load(kp_id);
		if(null != kpiPk){
			model.addAttribute("kpiPk",kpiPk);
		}
		return "shop/kpipk/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) Integer kp_type,
			@RequestParam(required = false) Integer kp_state,
			@RequestParam(required = false) String kp_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("begindate", begindate);
        param.put("enddate", StringUtil.isEmpty(enddate)?"":enddate+" 23:59:59");
        param.put("kp_type", kp_type);
        param.put("kp_state", kp_state);
        param.put("kp_number", StringUtil.decodeString(kp_number));
		return ajaxSuccess(kpiPkService.page(param));
	}
	
	@RequestMapping(value = "loadDetail/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadDetail(@PathVariable String number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("number", number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(kpiPkService.loadDetail(params));
	}
	
	@RequestMapping(value = "statDetail/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statDetail(@PathVariable String number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("number", number);
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		return ajaxSuccess(kpiPkService.statDetail(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(String kpipk,String details,String kpis,HttpSession session) {
		kpipk = StringUtil.decodeString(kpipk);
		details = StringUtil.decodeString(details);
		kpis = StringUtil.trimString(kpis);
		T_Shop_KpiPk kpiPk = JSON.parseObject(kpipk,T_Shop_KpiPk.class);
		List<T_Shop_KpiPkList> kpiDetails = JSON.parseArray(details,T_Shop_KpiPkList.class);
		String[] kiCodes = kpis.split(",");
		List<T_Shop_KpiPkList> kpiPkLists = new ArrayList<T_Shop_KpiPkList>();
		for (T_Shop_KpiPkList item : kpiDetails) {
			for (String ki_code : kiCodes) {
				T_Shop_KpiPkList pkList = new T_Shop_KpiPkList();
				pkList.setKpl_code(item.getKpl_code());
				pkList.setKpl_name(item.getKpl_name());
				pkList.setKpl_ki_code(ki_code);
				kpiPkLists.add(pkList);
			}
		}
		kpiPkService.save(kpiPk, kpiPkLists, getUser(session));
		return ajaxSuccess(kpiPk);
	}
	
	@RequestMapping(value = "complete", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object complete(String kpipk,String details,HttpSession session) {
		kpipk = StringUtil.decodeString(kpipk);
		details = StringUtil.decodeString(details);
		T_Shop_KpiPk kpiPk = JSON.parseObject(kpipk,T_Shop_KpiPk.class);
		List<T_Shop_KpiPkList> kpiPkLists = JSON.parseArray(details,T_Shop_KpiPkList.class);
		return ajaxSuccess(kpiPkService.complete(kpiPk, kpiPkLists));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(Integer kp_id,HttpSession session) {
		kpiPkService.del(kp_id);
		return ajaxSuccess();
	}
	
}
