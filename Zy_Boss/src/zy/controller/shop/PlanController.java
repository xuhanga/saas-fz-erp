package zy.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.plan.T_Shop_MonthPlan;
import zy.entity.shop.plan.T_Shop_MonthPlan_Day;
import zy.entity.shop.plan.T_Shop_Plan;
import zy.entity.shop.plan.T_Shop_Plan_Expense;
import zy.entity.shop.plan.T_Shop_Plan_Month;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.plan.MonthPlanService;
import zy.service.shop.plan.PlanService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.shop.PlanVO;

@Controller
@RequestMapping("shop/plan")
public class PlanController extends BaseController{
	@Resource
	private PlanService planService;
	@Resource
	private MonthPlanService monthPlanService;
	
	@RequestMapping(value = "to_plan_menu", method = RequestMethod.GET)
	public String to_plan_menu() {
		return "shop/plan/plan_menu";
	}
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/plan/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "shop/plan/add";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer pl_id,Model model) {
		T_Shop_Plan plan = planService.load(pl_id);
		if(null != plan){
			model.addAttribute("plan",plan);
		}
		return "shop/plan/view";
	}
	@RequestMapping(value = "to_chart", method = RequestMethod.GET)
	public String to_chart() {
		return "shop/plan/chart";
	}
	@RequestMapping(value = "to_month_list", method = RequestMethod.GET)
	public String to_month_list() {
		return "shop/plan/month_list";
	}
	@RequestMapping(value = "to_month_add", method = RequestMethod.GET)
	public String to_month_add() {
		return "shop/plan/month_add";
	}
	@RequestMapping(value = "to_month_view", method = RequestMethod.GET)
	public String to_month_view(@RequestParam Integer mp_id,Model model) {
		T_Shop_MonthPlan plan = monthPlanService.load(mp_id);
		if(null != plan){
			model.addAttribute("plan",plan);
		}
		return "shop/plan/month_view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer pl_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String pl_shop_code,
			@RequestParam(required = false) String pl_year,
			@RequestParam(required = false) String pl_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("pl_ar_state", pl_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("pl_shop_code", pl_shop_code);
        param.put("pl_year", pl_year);
        param.put("pl_number", StringUtil.decodeString(pl_number));
		return ajaxSuccess(planService.page(param));
	}
	
	@RequestMapping(value = "listExpense/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listExpense(@PathVariable String number,HttpSession session) {
		return ajaxSuccess(planService.listExpense(number, getCompanyid(session)));
	}
	
	@RequestMapping(value = "listMonth/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listMonth(@PathVariable String number,HttpSession session) {
		return ajaxSuccess(planService.listMonth(number, getCompanyid(session)));
	}
	
	@RequestMapping(value = "check/{shop_code}/{year}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object check(@PathVariable String shop_code,@PathVariable Integer year,HttpSession session) {
		return ajaxSuccess(planService.check(shop_code,year,getCompanyid(session)));
	}
	
	@RequestMapping(value = "loadPreData/{shop_code}/{year}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadPreData(@PathVariable String shop_code,@PathVariable Integer year,HttpSession session) {
		return ajaxSuccess(planService.loadPreData(shop_code,year-1,getCompanyid(session)));
	}
	
	@RequestMapping(value = "loadPreDataMonth", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadPreDataMonth(String shop_code,Integer year,String pl_type,HttpSession session) {
		Map<String, Object> monthData = new HashMap<String,Object>();
		if("0".equals(pl_type)){
			List<Map<String, Object>> list = planService.loadPreDataMonth(shop_code, year-1, getCompanyid(session));
			for (Map<String, Object> item : list) {
				monthData.put(StringUtil.trimString(item.get("month")), StringUtil.trimString(item.get("money")));
			}
		}
		return ajaxSuccess(PlanVO.buildPreDataMonth(monthData));
	}
	
	@RequestMapping(value = "loadPreDataDay", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadPreDataDay(String shop_code,Integer year,Integer month,HttpSession session) {
		return ajaxSuccess(planService.loadPreDataDay(shop_code, year-1,month, getCompanyid(session)));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(String plan,String monthdatas,String expenses,HttpSession session) {
		plan = StringUtil.decodeString(plan);
		monthdatas = StringUtil.decodeString(monthdatas);
		expenses = StringUtil.decodeString(expenses);
		T_Shop_Plan shopPlan = JSON.parseObject(plan,T_Shop_Plan.class);
		List<T_Shop_Plan_Month> planMonths = JSON.parseArray(monthdatas,T_Shop_Plan_Month.class);
		List<T_Shop_Plan_Expense> planExpenses = null;
		if(StringUtil.isNotEmpty(expenses)){
			planExpenses = JSON.parseArray(expenses,T_Shop_Plan_Expense.class);
		}
		planService.save(shopPlan, planMonths, planExpenses, getUser(session));
		return ajaxSuccess(shopPlan);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(planService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		planService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "chartByShop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object chartByShop(PageForm pageForm,
			@RequestParam(required = false) String pl_shop_code,
			@RequestParam(required = false) String pl_year,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("pl_shop_code", pl_shop_code);
        param.put("pl_year", pl_year);
		return ajaxSuccess(planService.chartByShop(param));
	}
	
	@RequestMapping(value = "chartByMonth", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object chartByMonth(PageForm pageForm,
			@RequestParam(required = false) String pl_shop_code,
			@RequestParam(required = false) String pl_year,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("pl_shop_code", pl_shop_code);
		param.put("pl_year", pl_year);
		return ajaxSuccess(planService.chartByMonth(param));
	}
	
	@RequestMapping(value = "statByYearMonth", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statByYearMonth(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		param.put("year", DateUtil.getYear());
		param.put("month", DateUtil.getMonth(DateUtil.getCurrentDate()));
		return ajaxSuccess(planService.statByYearMonth(param));
	}
	
	@RequestMapping(value = "month_page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month_page(PageForm pageForm,
			@RequestParam(required = false) Integer mp_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String mp_shop_code,
			@RequestParam(required = false) String mp_year,
			@RequestParam(required = false) String mp_month,
			@RequestParam(required = false) String mp_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("mp_ar_state", mp_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("mp_shop_code", mp_shop_code);
        param.put("mp_year", mp_year);
        param.put("mp_month", mp_month);
        param.put("mp_number", StringUtil.decodeString(mp_number));
		return ajaxSuccess(monthPlanService.page(param));
	}
	
	@RequestMapping(value = "month_check/{shop_code}/{year}/{month}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month_check(@PathVariable String shop_code,@PathVariable Integer year,@PathVariable Integer month,HttpSession session) {
		T_Shop_MonthPlan monthPlan = monthPlanService.check(shop_code,year,month,getCompanyid(session));
		T_Shop_Plan_Month planMonth = planService.loadPlanMonth(shop_code,year, month, getCompanyid(session));
		Map<String, Object> resultMap = new HashMap<String,Object>();
		resultMap.put("monthPlan", monthPlan);
		resultMap.put("planMonth", planMonth);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "month_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month_save(String plan,String daydatas,HttpSession session) {
		plan = StringUtil.decodeString(plan);
		daydatas = StringUtil.decodeString(daydatas);
		T_Shop_MonthPlan monthPlan = JSON.parseObject(plan,T_Shop_MonthPlan.class);
		List<T_Shop_MonthPlan_Day> days = JSON.parseArray(daydatas,T_Shop_MonthPlan_Day.class);
		monthPlanService.save(monthPlan, days, getUser(session));
		return ajaxSuccess(monthPlan);
	}
	
	@RequestMapping(value = "month_listDay/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month_listDay(@PathVariable String number,HttpSession session) {
		return ajaxSuccess(monthPlanService.listDay(number, getCompanyid(session)));
	}
	
	@RequestMapping(value = "month_approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month_approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(monthPlanService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "month_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object month_del(String number,HttpSession session) {
		monthPlanService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
}
