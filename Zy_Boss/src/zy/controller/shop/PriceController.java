package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.price.T_Shop_Price;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.price.PriceService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/price")
public class PriceController extends BaseController{
	
	@Resource
	private PriceService priceService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/price/list";
	}
	
	@RequestMapping(value = "/to_modifyprice_list", method = RequestMethod.GET)
	public String to_modifyprice_list() {
		return "shop/price/modifyprice_list";
	}
	
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add(Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		model.addAttribute("shoptype", user.getShoptype());
		model.addAttribute("sp_makerdate",DateUtil.getYearMonthDate());
		return "shop/price/add";
	}
	
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer sp_id,Model model) {
		T_Shop_Price price = priceService.load(sp_id);
		if(null != price){
			model.addAttribute("price",price);
		}
		return "shop/price/view";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer sp_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Shop_Price price = priceService.load(sp_id);
		if(null != price){
			priceService.initUpdate(price.getSp_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("price",price);
		}
		return "shop/price/update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer sp_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String sp_manager,
			@RequestParam(required = false) String sp_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("sp_state", sp_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sp_manager", StringUtil.decodeString(sp_manager));
        param.put("sp_number", StringUtil.decodeString(sp_number));
		return ajaxSuccess(priceService.page(param));
	}
	
	@RequestMapping(value = "price_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object price_list(HttpSession session,@RequestParam(required = false) String sp_number) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("sp_number", sp_number);
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(priceService.price_list(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("spl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(priceService.temp_list(params));
	}
	
	
	@RequestMapping(value = "save_temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_temp_list(HttpSession session,
			@RequestParam(required = false) String pd_code,
			@RequestParam(required = false) String sp_shop_code,
			@RequestParam(required = false) Double discount,
			@RequestParam(required = false) Integer sp_shop_type) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("spl_us_id", user.getUs_id());
		params.put("pd_code", pd_code);
		params.put("sp_shop_code", sp_shop_code);
		params.put("discount", discount);
		params.put("sp_shop_type", sp_shop_type);
		params.put("companyid", user.getCompanyid());
		priceService.save_temp_list(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save_tempList_Enter", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_tempList_Enter(HttpSession session,
			@RequestParam(required = false) String sp_is_sellprice,
			@RequestParam(required = false) String sp_is_vipprice,
			@RequestParam(required = false) String sp_is_sortprice,
			@RequestParam(required = false) String sp_is_costprice,
			@RequestParam(required = false) String value,
			@RequestParam(required = false) String sp_shop_code,
			@RequestParam(required = false) Double discount,
			@RequestParam(required = false) Integer sp_shop_type) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pd_no", StringUtil.decodeString(value));
		params.put("sp_shop_code", sp_shop_code);
		params.put("discount", discount);
		params.put("sp_shop_type", sp_shop_type);
		params.put("sp_is_sellprice", sp_is_sellprice);
		params.put("sp_is_vipprice", sp_is_vipprice);
		params.put("sp_is_sortprice", sp_is_sortprice);
		params.put("sp_is_costprice", sp_is_costprice);
		params.put("spl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		String state = "0";
		state = priceService.save_tempList_Enter(params);
		return result(state);
	}
	
	@RequestMapping(value = "shop_change_templist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object shop_change_templist(HttpSession session,
			@RequestParam(required = false) String shopCodeBefore,
			@RequestParam(required = false) String sp_shop_code,
			@RequestParam(required = false) Double discount,
			@RequestParam(required = false) Integer sp_shop_type) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("shopCodeBefore", shopCodeBefore);
		params.put("sp_shop_code", sp_shop_code);
		params.put("discount", discount);
		params.put("sp_shop_type", sp_shop_type);
		params.put("spl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		
		priceService.shop_change_templist(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "update_templist_byPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update_templist_byPdCode(HttpSession session,
			@RequestParam(required = false) String pd_code,
			@RequestParam(required = false) Double unitPrice) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("spl_us_id", user.getUs_id());
		params.put("spl_pd_code", pd_code);
		params.put("unitPrice", unitPrice);
		params.put("companyid", user.getCompanyid());
		priceService.update_templist_byPdCode(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "savePrice_templist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object savePrice_templist(HttpSession session,
			@RequestParam(required = false) String pd_code,
			@RequestParam(required = false) Double prices,
			@RequestParam(required = false) String spl_shop_code,
			@RequestParam(required = false) String price_type) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("spl_us_id", user.getUs_id());
		params.put("prices", prices);
		params.put("spl_pd_code", pd_code);
		params.put("spl_shop_code", spl_shop_code);
		params.put("price_type", price_type);
		params.put("companyid", user.getCompanyid());
		priceService.savePrice_templist(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "savePoint_templist", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object savePoint_templist(HttpSession session,
			@RequestParam(required = false) Integer spl_id,
			@RequestParam(required = false) Integer spl_pd_point) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("spl_id", spl_id);
		params.put("spl_pd_point", spl_pd_point);
		params.put("companyid", user.getCompanyid());
		priceService.savePoint_templist(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(HttpSession session,T_Shop_Price shop_Price) {
		T_Sys_User user = getUser(session);
		priceService.save(shop_Price,user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(HttpSession session,T_Shop_Price shop_Price) {
		T_Sys_User user = getUser(session);
		priceService.update(shop_Price,user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		priceService.temp_clear(user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer spl_id) {
		priceService.temp_del(spl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Shop_Price price = priceService.approve(number, record, getUser(session));
		return ajaxSuccess(price);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		priceService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
}
