package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.sale.T_Shop_Sale;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.sale.SaleService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/sale")
public class SaleController extends BaseController{
	@Resource
	private SaleService saleService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/sale/list";
	}
	
	@RequestMapping(value = "/to_total_list", method = RequestMethod.GET)
	public String to_total_list() {
		return "shop/sale/total_list";
	}
	
	@RequestMapping(value = "/to_rate_list", method = RequestMethod.GET)
	public String to_rate_list() {
		return "shop/sale/rate_list";
	}
	
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "shop/sale/add";
	}
	
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam String ss_code,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put("ss_code", ss_code);
		// 根据促销编号查询明细信息
		Map<String,Object> saleMap = saleService.load(param);
		if(null != saleMap){
			model.addAttribute("saleMap",saleMap);
		}
		return "shop/sale/view";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String sss_shop_code = request.getParameter("sss_shop_code");
        String ss_code = request.getParameter("ss_code");
        String ss_mt_code = request.getParameter("ss_mt_code");
        String ss_state = request.getParameter("ss_state");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sss_shop_code", sss_shop_code);
        param.put("ss_code", ss_code);
        param.put("ss_mt_code", ss_mt_code);
        param.put("ss_state", ss_state);
		PageData<T_Shop_Sale> pageData = saleService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	
	@RequestMapping(value = "/queryPriority", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryPriority(@RequestParam String ss_priority,
			@RequestParam String ss_shop_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		//获取当前日期
		String curdate = DateUtil.getYearMonthDate();
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
	    param.put("ss_shop_code", ss_shop_code);
	    param.put("ss_priority", ss_priority);
		param.put("curdate", curdate);
		Integer ss_id = 0;
		ss_id = saleService.queryPriority(param);
		if (ss_id > 0) {
			return ajaxSuccessMessage("sucess");
		} else {
			return ajaxSuccessMessage("fail");
		}
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Shop_Sale sale,HttpServletRequest request,HttpSession session) {
		
		String ss_shop_codes = request.getParameter("ss_shop_code");//促销店铺
		String ifUpdatePriority = request.getParameter("ifUpdatePriority");
		String ssa_discount111 = request.getParameter("ssa_discount111");
		String[] ssa_buy_fulls112 = request.getParameterValues("ssa_buy_full112");
		String[] ssa_discounts112 = request.getParameterValues("ssa_discount112");
		String[] ssa_buy_number113 = request.getParameterValues("ssa_buy_number113");
		String[] ssa_discount113 = request.getParameterValues("ssa_discount113");
		String[] sst_tp_code121 = request.getParameterValues("sst_tp_code121");
		String[] sst_discount121 = request.getParameterValues("sst_discount121");
		String[] sst_tp_code122 = request.getParameterValues("sst_tp_code122");
		String[] sst_buy_full122 = request.getParameterValues("sst_buy_full122");
		String[] sst_discount122 = request.getParameterValues("sst_discount122");
		String[] sst_tp_code123 = request.getParameterValues("sst_tp_code123");
		String[] sst_buy_number123 = request.getParameterValues("sst_buy_number123");
		String[] sst_discount123 = request.getParameterValues("sst_discount123");
		
		String[] ssb_bd_code131 = request.getParameterValues("ssb_bd_code131");
		String[] ssb_discount131 = request.getParameterValues("ssb_discount131");
		String[] ssb_bd_code132 = request.getParameterValues("ssb_bd_code132");
		String[] ssb_buy_full132 = request.getParameterValues("ssb_buy_full132");
		String[] ssb_discount132 = request.getParameterValues("ssb_discount132");
		
		String[] ssb_bd_code133 = request.getParameterValues("ssb_bd_code133");
		String[] ssb_buy_number133 = request.getParameterValues("ssb_buy_number133");
		String[] ssb_discount133 = request.getParameterValues("ssb_discount133");
		
		String[] ssp_pd_code141 = request.getParameterValues("ssp_pd_code141");
		String[] ssp_discount141 = request.getParameterValues("ssp_discount141");
		
		String[] ssp_pd_code142 = request.getParameterValues("ssp_pd_code142");
		String[] ssp_buy_full142 = request.getParameterValues("ssp_buy_full142");
		String[] ssp_discount142 = request.getParameterValues("ssp_discount142");
		
		String[] ssp_pd_code143 = request.getParameterValues("ssp_pd_code143");
		String[] ssp_buy_number143 = request.getParameterValues("ssp_buy_number143");
		String[] ssp_discount143 = request.getParameterValues("ssp_discount143");
		
		String[] ssp_pd_code241 = request.getParameterValues("ssp_pd_code241");
		String[] ssp_special_offer241 = request.getParameterValues("ssp_special_offer241");
		
		String[] ssp_pd_code242 = request.getParameterValues("ssp_pd_code242");
		String[] ssp_buy_number242 = request.getParameterValues("ssp_buy_number242");
		String[] ssp_special_offer242 = request.getParameterValues("ssp_special_offer242");
		
		/*String[] buyFulls311 = request.getParameterValues("SA_Buy_Full311");
		String[] donationAmounts311 = request.getParameterValues("SA_Donation_Amount311");*/
		String[] ssa_buy_number311 = request.getParameterValues("ssa_buy_number311");
		String[] ssa_donation_number311 = request.getParameterValues("ssa_donation_number311");
		
		String[] ssa_buy_full312 = request.getParameterValues("ssa_buy_full312");
		String[] ssa_reduce_amount312 = request.getParameterValues("ssa_reduce_amount312");
		
		String[] ssa_buy_full313 = request.getParameterValues("ssa_buy_full313");
		String[] ssa_increased_amount313 = request.getParameterValues("ssa_increased_amount313");
		Map<String,Object> productMap = new HashMap<String, Object>();
		if(ssa_buy_full313 != null && ssa_buy_full313.length > 0){
			for(int i=0;i<ssa_buy_full313.length;i++){
				productMap.put("ssp_subcode313_"+(i+1), request.getParameterValues("ssp_subcode313_"+(i+1)));
				productMap.put("ssp_count313_"+(i+1), request.getParameterValues("ssp_count313_"+(i+1)));
				productMap.put("ssp_pd_code313_"+(i+1), request.getParameterValues("ssp_pd_code313_"+(i+1)));
				productMap.put("ssp_cr_code313_"+(i+1), request.getParameterValues("ssp_cr_code313_"+(i+1)));
				productMap.put("ssp_sz_code313_"+(i+1), request.getParameterValues("ssp_sz_code313_"+(i+1)));
				productMap.put("ssp_br_code313_"+(i+1), request.getParameterValues("ssp_br_code313_"+(i+1)));
			}
		}
		String[] ssa_buy_number314 = request.getParameterValues("ssa_buy_number314");
		if(ssa_buy_number314 != null && ssa_buy_number314.length > 0){
			for(int i=0;i<ssa_buy_number314.length;i++){
				productMap.put("ssp_subcode314_"+(i+1), request.getParameterValues("ssp_subcode314_"+(i+1)));
				productMap.put("ssp_count314_"+(i+1), request.getParameterValues("ssp_count314_"+(i+1)));
				productMap.put("ssp_pd_code314_"+(i+1), request.getParameterValues("ssp_pd_code314_"+(i+1)));
				productMap.put("ssp_cr_code314_"+(i+1), request.getParameterValues("ssp_cr_code314_"+(i+1)));
				productMap.put("ssp_sz_code314_"+(i+1), request.getParameterValues("ssp_sz_code314_"+(i+1)));
				productMap.put("ssp_br_code314_"+(i+1), request.getParameterValues("ssp_br_code314_"+(i+1)));
			}
		}
		String[] ssa_buy_full315 = request.getParameterValues("ssa_buy_full315");
		if(ssa_buy_full315 != null && ssa_buy_full315.length > 0){
			for(int i=0;i<ssa_buy_full315.length;i++){
				productMap.put("ssp_subcode315_"+(i+1), request.getParameterValues("ssp_subcode315_"+(i+1)));
				productMap.put("ssp_count315_"+(i+1), request.getParameterValues("ssp_count315_"+(i+1)));
				productMap.put("ssp_pd_code315_"+(i+1), request.getParameterValues("ssp_pd_code315_"+(i+1)));
				productMap.put("ssp_cr_code315_"+(i+1), request.getParameterValues("ssp_cr_code315_"+(i+1)));
				productMap.put("ssp_sz_code315_"+(i+1), request.getParameterValues("ssp_sz_code315_"+(i+1)));
				productMap.put("ssp_br_code315_"+(i+1), request.getParameterValues("ssp_br_code315_"+(i+1)));
			}
		}
		
		/*String[] buyFulls321 = request.getParameterValues("ST_Buy_Full321");
		String[] donationAmounts321 = request.getParameterValues("ST_Donation_Amount321");
		String[] ctCode321 = request.getParameterValues("ST_Ct_Code321");*/
		
		String[] sst_buy_full322 = request.getParameterValues("sst_buy_full322");
		String[] sst_reduce_amount322 = request.getParameterValues("sst_reduce_amount322");
		String[] sst_tp_code322 = request.getParameterValues("sst_tp_code322");
		
		String[] sst_buy_full323 = request.getParameterValues("sst_buy_full323");
		String[] sst_increased_amount323 = request.getParameterValues("sst_increased_amount323");
		String[] sst_tp_code323 = request.getParameterValues("sst_tp_code323");
		if(sst_tp_code323 != null && sst_tp_code323.length > 0){
			for(int i=0;i<sst_tp_code323.length;i++){
				productMap.put("ssp_subcode323_"+(i+1), request.getParameterValues("ssp_subcode323_"+(i+1)));
				productMap.put("ssp_count323_"+(i+1), request.getParameterValues("ssp_count323_"+(i+1)));
				productMap.put("ssp_pd_code323_"+(i+1), request.getParameterValues("ssp_pd_code323_"+(i+1)));
				productMap.put("ssp_cr_code323_"+(i+1), request.getParameterValues("ssp_cr_code323_"+(i+1)));
				productMap.put("ssp_sz_code323_"+(i+1), request.getParameterValues("ssp_sz_code323_"+(i+1)));
				productMap.put("ssp_br_code323_"+(i+1), request.getParameterValues("ssp_br_code323_"+(i+1)));
			}
		}
		
		String[] sst_buy_number324 = request.getParameterValues("sst_buy_number324");
		String[] sst_tp_code324 = request.getParameterValues("sst_tp_code324");
		if(sst_tp_code324 != null && sst_tp_code324.length > 0){
			for(int i=0;i<sst_tp_code324.length;i++){
				productMap.put("ssp_subcode324_"+(i+1), request.getParameterValues("ssp_subcode324_"+(i+1)));
				productMap.put("ssp_count324_"+(i+1), request.getParameterValues("ssp_count324_"+(i+1)));
				productMap.put("ssp_pd_code324_"+(i+1), request.getParameterValues("ssp_pd_code324_"+(i+1)));
				productMap.put("ssp_cr_code324_"+(i+1), request.getParameterValues("ssp_cr_code324_"+(i+1)));
				productMap.put("ssp_sz_code324_"+(i+1), request.getParameterValues("ssp_sz_code324_"+(i+1)));
				productMap.put("ssp_br_code324_"+(i+1), request.getParameterValues("ssp_br_code324_"+(i+1)));
			}
		}
		
		String[] sst_buy_full325 = request.getParameterValues("sst_buy_full325");
		String[] sst_tp_code325 = request.getParameterValues("sst_tp_code325");
		if(sst_tp_code325 != null && sst_tp_code325.length > 0){
			for(int i=0;i<sst_tp_code325.length;i++){
				productMap.put("ssp_subcode325_"+(i+1), request.getParameterValues("ssp_subcode325_"+(i+1)));
				productMap.put("ssp_count325_"+(i+1), request.getParameterValues("ssp_count325_"+(i+1)));
				productMap.put("ssp_pd_code325_"+(i+1), request.getParameterValues("ssp_pd_code325_"+(i+1)));
				productMap.put("ssp_cr_code325_"+(i+1), request.getParameterValues("ssp_cr_code325_"+(i+1)));
				productMap.put("ssp_sz_code325_"+(i+1), request.getParameterValues("ssp_sz_code325_"+(i+1)));
				productMap.put("ssp_br_code325_"+(i+1), request.getParameterValues("ssp_br_code325_"+(i+1)));
			}
		}
		
		/*String[] bdCodes331 = request.getParameterValues("SB_Bd_Code331");
		String[] donationAmounts331 = request.getParameterValues("SB_Donation_Amount331");
		String[] buyFulls331 = request.getParameterValues("SB_Buy_Full331");*/
		
		String[] ssb_bd_code332 = request.getParameterValues("ssb_bd_code332");
		String[] ssb_reduce_amount332 = request.getParameterValues("ssb_reduce_amount332");
		String[] ssb_buy_full332 = request.getParameterValues("ssb_buy_full332");
		
		String[] ssb_buy_full333 = request.getParameterValues("ssb_buy_full333");
		String[] ssb_increased_amount333 = request.getParameterValues("ssb_increased_amount333");
		String[] ssb_bd_code333 = request.getParameterValues("ssb_bd_code333");
		if(ssb_bd_code333 != null && ssb_bd_code333.length > 0){
			for(int i=0;i<ssb_bd_code333.length;i++){
				productMap.put("ssp_subcode333_"+(i+1), request.getParameterValues("ssp_subcode333_"+(i+1)));
				productMap.put("ssp_count333_"+(i+1), request.getParameterValues("ssp_count333_"+(i+1)));
				productMap.put("ssp_pd_code333_"+(i+1), request.getParameterValues("ssp_pd_code333_"+(i+1)));
				productMap.put("ssp_cr_code333_"+(i+1), request.getParameterValues("ssp_cr_code333_"+(i+1)));
				productMap.put("ssp_sz_code333_"+(i+1), request.getParameterValues("ssp_sz_code333_"+(i+1)));
				productMap.put("ssp_br_code333_"+(i+1), request.getParameterValues("ssp_br_code333_"+(i+1)));
			}
		}
		
		String[] ssb_buy_number334 = request.getParameterValues("ssb_buy_number334");
		String[] ssb_bd_code334 = request.getParameterValues("ssb_bd_code334");
		if(ssb_bd_code334 != null && ssb_bd_code334.length > 0){
			for(int i=0;i<ssb_bd_code334.length;i++){
				productMap.put("ssp_subcode334_"+(i+1), request.getParameterValues("ssp_subcode334_"+(i+1)));
				productMap.put("ssp_count334_"+(i+1), request.getParameterValues("ssp_count334_"+(i+1)));
				productMap.put("ssp_pd_code334_"+(i+1), request.getParameterValues("ssp_pd_code334_"+(i+1)));
				productMap.put("ssp_cr_code334_"+(i+1), request.getParameterValues("ssp_cr_code334_"+(i+1)));
				productMap.put("ssp_sz_code334_"+(i+1), request.getParameterValues("ssp_sz_code334_"+(i+1)));
				productMap.put("ssp_br_code334_"+(i+1), request.getParameterValues("ssp_br_code334_"+(i+1)));
			}
		}
		
		String[] ssb_buy_full335 = request.getParameterValues("ssb_buy_full335");
		String[] ssb_bd_code335 = request.getParameterValues("ssb_bd_code335");
		if(ssb_bd_code335 != null && ssb_bd_code335.length > 0){
			for(int i=0;i<ssb_bd_code335.length;i++){
				productMap.put("ssp_subcode335_"+(i+1), request.getParameterValues("ssp_subcode335_"+(i+1)));
				productMap.put("ssp_count335_"+(i+1), request.getParameterValues("ssp_count335_"+(i+1)));
				productMap.put("ssp_pd_code335_"+(i+1), request.getParameterValues("ssp_pd_code335_"+(i+1)));
				productMap.put("ssp_cr_code335_"+(i+1), request.getParameterValues("ssp_cr_code335_"+(i+1)));
				productMap.put("ssp_sz_code335_"+(i+1), request.getParameterValues("ssp_sz_code335_"+(i+1)));
				productMap.put("ssp_br_code335_"+(i+1), request.getParameterValues("ssp_br_code335_"+(i+1)));
			}
		}
		
		/*String[] piNos341 = request.getParameterValues("SP_Pi_No341");
		String[] donationAmounts341 = request.getParameterValues("SP_Donation_Amount341");
		String[] buyFulls341 = request.getParameterValues("SP_Buy_Full341");*/
		
		String[] ssp_pd_code342 = request.getParameterValues("ssp_pd_code342");
		String[] ssp_reduce_amount342 = request.getParameterValues("ssp_reduce_amount342");
		String[] ssp_buy_full342 = request.getParameterValues("ssp_buy_full342");
		
		String[] ssp_pd_code343 = request.getParameterValues("ssp_pd_code343");
		String[] ssp_increased_amount343 = request.getParameterValues("ssp_increased_amount343");
		String[] ssp_buy_full343 = request.getParameterValues("ssp_buy_full343");
		if(ssp_pd_code343 != null && ssp_pd_code343.length > 0){
			for(int i=0;i<ssp_pd_code343.length;i++){
				productMap.put("ssp_subcode343_"+(i+1), request.getParameterValues("ssp_subcode343_"+(i+1)));
				productMap.put("ssp_count343_"+(i+1), request.getParameterValues("ssp_count343_"+(i+1)));
				productMap.put("ssp_pd_code343_"+(i+1), request.getParameterValues("ssp_pd_code343_"+(i+1)));
				productMap.put("ssp_cr_code343_"+(i+1), request.getParameterValues("ssp_cr_code343_"+(i+1)));
				productMap.put("ssp_sz_code343_"+(i+1), request.getParameterValues("ssp_sz_code343_"+(i+1)));
				productMap.put("ssp_br_code343_"+(i+1), request.getParameterValues("ssp_br_code343_"+(i+1)));
			}
		}
		
		String[] ssp_pd_code344 = request.getParameterValues("ssp_pd_code344");
		String[] ssp_buy_number344 = request.getParameterValues("ssp_buy_number344");
		if(ssp_pd_code344 != null && ssp_pd_code344.length > 0){
			for(int i=0;i<ssp_pd_code344.length;i++){
				productMap.put("ssp_subcode344_"+(i+1), request.getParameterValues("ssp_subcode344_"+(i+1)));
				productMap.put("ssp_count344_"+(i+1), request.getParameterValues("ssp_count344_"+(i+1)));
				productMap.put("ssp_pd_code344_"+(i+1), request.getParameterValues("ssp_pd_code344_"+(i+1)));
				productMap.put("ssp_cr_code344_"+(i+1), request.getParameterValues("ssp_cr_code344_"+(i+1)));
				productMap.put("ssp_sz_code344_"+(i+1), request.getParameterValues("ssp_sz_code344_"+(i+1)));
				productMap.put("ssp_br_code344_"+(i+1), request.getParameterValues("ssp_br_code344_"+(i+1)));
			}
		}
		
		String[] ssp_pd_code345 = request.getParameterValues("ssp_pd_code345");
		String[] ssp_buy_full345 = request.getParameterValues("ssp_buy_full345");
		if(ssp_pd_code345 != null && ssp_pd_code345.length > 0){
			for(int i=0;i<ssp_pd_code345.length;i++){
				productMap.put("ssp_subcode345_"+(i+1), request.getParameterValues("ssp_subcode345_"+(i+1)));
				productMap.put("ssp_count345_"+(i+1), request.getParameterValues("ssp_count345_"+(i+1)));
				productMap.put("ssp_pd_code345_"+(i+1), request.getParameterValues("ssp_pd_code345_"+(i+1)));
				productMap.put("ssp_cr_code345_"+(i+1), request.getParameterValues("ssp_cr_code345_"+(i+1)));
				productMap.put("ssp_sz_code345_"+(i+1), request.getParameterValues("ssp_sz_code345_"+(i+1)));
				productMap.put("ssp_br_code345_"+(i+1), request.getParameterValues("ssp_br_code345_"+(i+1)));
			}
		}
		T_Sys_User user = getUser(session);
		sale.setSs_current_spcode(user.getUs_shop_code());//保存创建人当前店铺编号
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put("us_name", user.getUs_name());
		param.put("t_shop_sale", sale);
		param.put("ss_shop_codes", ss_shop_codes);
		param.put("ifUpdatePriority", ifUpdatePriority);
		param.put("ssa_discount111", ssa_discount111);
		
		param.put("ssa_buy_fulls112", ssa_buy_fulls112);
		param.put("ssa_discounts112", ssa_discounts112);
		
		param.put("ssa_discount113", ssa_discount113);
		param.put("ssa_buy_number113", ssa_buy_number113);
		
		param.put("sst_tp_code121", sst_tp_code121);
		param.put("sst_discount121", sst_discount121);
		
		param.put("sst_tp_code122", sst_tp_code122);
		param.put("sst_buy_full122", sst_buy_full122);
		param.put("sst_discount122", sst_discount122);
		
		param.put("sst_tp_code123", sst_tp_code123);
		param.put("sst_buy_number123", sst_buy_number123);
		param.put("sst_discount123", sst_discount123);
		
		param.put("ssb_bd_code131", ssb_bd_code131);
		param.put("ssb_discount131", ssb_discount131);
		
		param.put("ssb_bd_code132", ssb_bd_code132);
		param.put("ssb_buy_full132", ssb_buy_full132);
		param.put("ssb_discount132", ssb_discount132);
		
		param.put("ssb_bd_code133", ssb_bd_code133);
		param.put("ssb_buy_number133", ssb_buy_number133);
		param.put("ssb_discount133", ssb_discount133);
		
		param.put("ssp_pd_code141", ssp_pd_code141);
		param.put("ssp_discount141", ssp_discount141);
		
		param.put("ssp_pd_code142", ssp_pd_code142);
		param.put("ssp_buy_full142", ssp_buy_full142);
		param.put("ssp_discount142", ssp_discount142);
		
		param.put("ssp_pd_code143", ssp_pd_code143);
		param.put("ssp_buy_number143", ssp_buy_number143);
		param.put("ssp_discount143", ssp_discount143);
		
		param.put("ssp_pd_code241", ssp_pd_code241);
		param.put("ssp_special_offer241", ssp_special_offer241);
		
		param.put("ssp_pd_code242", ssp_pd_code242);
		param.put("ssp_buy_number242", ssp_buy_number242);
		param.put("ssp_special_offer242", ssp_special_offer242);
		
		/*param.put("buyFulls311", buyFulls311);
		param.put("donationAmounts311", donationAmounts311);*/
		
		param.put("ssa_buy_number311", ssa_buy_number311);
		param.put("ssa_donation_number311", ssa_donation_number311);
		
		param.put("ssa_buy_full312", ssa_buy_full312);
		param.put("ssa_reduce_amount312", ssa_reduce_amount312);
		
		param.put("ssa_buy_full313", ssa_buy_full313);
		param.put("ssa_increased_amount313", ssa_increased_amount313);
		
		param.put("productMap", productMap);
		
		param.put("ssa_buy_number314", ssa_buy_number314);
		
		param.put("ssa_buy_full315", ssa_buy_full315);
		
		/*param.put("buyFulls321", buyFulls321);
		param.put("donationAmounts321", donationAmounts321);
		param.put("ctCode321", ctCode321);*/
		
		param.put("sst_buy_full322", sst_buy_full322);
		param.put("sst_reduce_amount322", sst_reduce_amount322);
		param.put("sst_tp_code322", sst_tp_code322);
		
		param.put("sst_buy_full323", sst_buy_full323);
		param.put("sst_increased_amount323", sst_increased_amount323);
		param.put("sst_tp_code323", sst_tp_code323);
		
		param.put("sst_buy_number324", sst_buy_number324);
		param.put("sst_tp_code324", sst_tp_code324);
		
		param.put("sst_buy_full325", sst_buy_full325);
		param.put("sst_tp_code325", sst_tp_code325);
		
		/*param.put("bdCodes331", bdCodes331);
		param.put("donationAmounts331", donationAmounts331);
		param.put("buyFulls331", buyFulls331);*/
		
		param.put("ssb_bd_code332", ssb_bd_code332);
		param.put("ssb_reduce_amount332", ssb_reduce_amount332);
		param.put("ssb_buy_full332", ssb_buy_full332);
		
		param.put("ssb_buy_full333", ssb_buy_full333);
		param.put("ssb_increased_amount333", ssb_increased_amount333);
		param.put("ssb_bd_code333", ssb_bd_code333);
		
		param.put("ssb_bd_code334", ssb_bd_code334);
		param.put("ssb_buy_number334", ssb_buy_number334);
		
		param.put("ssb_buy_full335", ssb_buy_full335);
		param.put("ssb_bd_code335", ssb_bd_code335);
		
		/*param.put("piNos341", piNos341);
		param.put("donationAmounts341", donationAmounts341);
		param.put("buyFulls341", buyFulls341);*/
		
		param.put("ssp_pd_code342", ssp_pd_code342);
		param.put("ssp_reduce_amount342", ssp_reduce_amount342);
		param.put("ssp_buy_full342", ssp_buy_full342);
		
		param.put("ssp_pd_code343", ssp_pd_code343);
		param.put("ssp_increased_amount343", ssp_increased_amount343);
		param.put("ssp_buy_full343", ssp_buy_full343);

		param.put("ssp_pd_code344", ssp_pd_code344);
		param.put("ssp_buy_number344", ssp_buy_number344);
		
		param.put("ssp_pd_code345", ssp_pd_code345);
		param.put("ssp_buy_full345", ssp_buy_full345);
		
		saleService.save(param);
		return ajaxSuccess(sale);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String code,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Shop_Sale sale = saleService.approve(code, record, getUser(session));
		return ajaxSuccess(sale);
	}
	
	@RequestMapping(value = "stop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object stop(String code,String ec_stop_cause,HttpSession session) {
		T_Shop_Sale sale = saleService.stop(code, StringUtil.decodeString(ec_stop_cause), getUser(session));
		return ajaxSuccess(sale);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String ss_code,HttpSession session) {
		saleService.del(ss_code, getCompanyid(session));
		return ajaxSuccess();
	}
	
	
	@RequestMapping(value = "rateList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object rateList(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String shop_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String pd_no,
			@RequestParam(required = false) String pd_name,
			@RequestParam(required = false) String ss_code,
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("shl_shop_code", shop_code);
        param.put("tp_code", tp_code);
        param.put("bd_code", bd_code);
        param.put("pd_no", StringUtil.decodeString(pd_no));
        param.put("pd_name", StringUtil.decodeString(pd_name));
        param.put("ss_code", StringUtil.decodeString(ss_code));
		return ajaxSuccess(saleService.rateList(param));
	}
	
	@RequestMapping(value = "totalList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object totalList(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String shop_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String pd_no,
			@RequestParam(required = false) String pd_name,
			HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("shl_shop_code", shop_code);
        param.put("tp_code", tp_code);
        param.put("bd_code", bd_code);
        param.put("pd_no", StringUtil.decodeString(pd_no));
        param.put("pd_name", StringUtil.decodeString(pd_name));
		return ajaxSuccess(saleService.totalList(param));
	}
}
