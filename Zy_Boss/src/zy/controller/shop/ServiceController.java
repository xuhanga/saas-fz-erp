package zy.controller.shop;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.shop.service.T_Shop_Service;
import zy.entity.shop.service.T_Shop_Service_Item;
import zy.entity.sys.user.T_Sys_User;
import zy.service.shop.service.ServiceService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/service")
public class ServiceController extends BaseController{
	@Resource
	private ServiceService serviceService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/service/list";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "shop/service/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer ss_id,Model model,HttpSession session) {
		T_Shop_Service service = serviceService.load(ss_id);
		if(null != service){
			model.addAttribute("service",service);
		}
		return "shop/service/update";
	}
	
	@RequestMapping(value = "/to_list_item", method = RequestMethod.GET)
	public String to_list_item() {
		return "shop/service/list_item";
	}
	
	@RequestMapping("to_add_item")
	public String to_add_item() {
		return "shop/service/add_item";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String ss_state = request.getParameter("ss_state");
        String ss_name = request.getParameter("ss_name");
        String ss_tel = request.getParameter("ss_tel");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ss_state", ss_state);
        param.put("ss_name", StringUtil.decodeString(ss_name));
        param.put("ss_tel", ss_tel);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Shop_Service> pageData = serviceService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Shop_Service service,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 serviceService.save(service,user);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Shop_Service service,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		serviceService.update(service,user);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ss_id,HttpServletRequest request,HttpSession session) {
		serviceService.del(ss_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "/to_update_item", method = RequestMethod.GET)
	public String to_update_item(@RequestParam Integer ssi_id,Model model) {
		T_Shop_Service_Item service_Item = serviceService.queryServiceItemByID(ssi_id);
		if(null != service_Item){
			model.addAttribute("service_Item",service_Item);
		}
		return "shop/service/update_item";
	}
	
	@RequestMapping(value = "list_serviceitem", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list_serviceitem(HttpServletRequest request,HttpSession session) {
		String name = request.getParameter("searchContent");
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", StringUtil.decodeString(name));
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Shop_Service_Item> pageData = serviceService.list_serviceitem(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "list_serviceitem_combo", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void list_serviceitem_combo(HttpServletRequest request,HttpSession session,HttpServletResponse response) {
		try {
			PrintWriter out=null;
			T_Sys_User user = getUser(session);
			Map<String,Object> param = new HashMap<String, Object>();
			param.put(CommonUtil.COMPANYID, user.getCompanyid());
			String ssi_list="{\"data\":{\"items\":[";
			List<T_Shop_Service_Item> list = serviceService.list_serviceitem_combo(param);
			for (int i=0;i<list.size();i++){
				ssi_list+="{\"ssi_id\":\""+list.get(i).getSsi_id()+"\",\"ssi_name\":\""+list.get(i).getSsi_name()+"\"},";
			}
			ssi_list=ssi_list.substring(0,ssi_list.length()-1);
			ssi_list+="]}}";
			out = response.getWriter();
			out.print(ssi_list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "save_serviceitem", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save_serviceitem(T_Shop_Service_Item service_Item,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 service_Item.setCompanyid(user.getCompanyid());
		 Integer id = serviceService.queryItemByName(service_Item);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 serviceService.save_serviceitem(service_Item);
			 return ajaxSuccess(service_Item);
		 }
	}
	
	@RequestMapping(value = "update_serviceitem", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update_serviceitem(T_Shop_Service_Item service_Item,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		service_Item.setCompanyid(user.getCompanyid());
		Integer id = serviceService.queryItemByName(service_Item);
		if(null != id && id > 0){
			return result(EXISTED);
		}else{
			serviceService.update_serviceitem(service_Item);
			return ajaxSuccess(service_Item);
		}
	}
	
	@RequestMapping(value = "del_serviceitem", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del_serviceitem(@RequestParam Integer ssi_id,HttpServletRequest request,HttpSession session) {
		serviceService.del_serviceitem(ssi_id);
		 return ajaxSuccess();
	}
}
