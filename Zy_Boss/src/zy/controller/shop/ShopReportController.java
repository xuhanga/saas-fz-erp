package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sell.day.T_Sell_Day;
import zy.form.PageForm;
import zy.service.shop.report.ShopReportService;
import zy.util.CommonUtil;
@Controller
@RequestMapping("shop/report")
public class ShopReportController extends BaseController{
	@Resource
	private ShopReportService shopReportService;
	@RequestMapping(value = "to_flow", method = RequestMethod.GET)
	public String to_report_want() {
		return "shop/report/flow";
	}
	
	@RequestMapping(value = "flow", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object flow(PageForm pageForm,String sh_shop_code,
			String begindate,String enddate,
			HttpSession session) {
        Map<String,Object> params = new HashMap<String, Object>();
        bulidParam(params, getUser(session));
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("sh_shop_code", sh_shop_code);
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        PageData<T_Sell_Day> pageData = shopReportService.flow(params);
		return ajaxSuccess(pageData);
	}
}
