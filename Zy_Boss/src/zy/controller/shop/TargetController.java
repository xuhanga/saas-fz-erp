package zy.controller.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.shop.target.T_Shop_Target;
import zy.entity.shop.target.T_Shop_Target_Detail;
import zy.entity.shop.target.T_Shop_Target_DetailList;
import zy.entity.sys.user.T_Sys_User;
//import zy.service.shop.target.TargetService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;
import zy.vo.shop.TargetVO;

@Controller
@RequestMapping("shop/target")
public class TargetController extends BaseController{
	
//	@Resource
//	private TargetService targetService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/target/list";
	}
	
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "shop/target/add";
	}
	
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(HttpServletRequest request,HttpSession session,Model model) {
		/*T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, user.getCompanyid());
		String ta_number= request.getParameter("ta_number");
		param.put("ta_number", ta_number);
		T_Shop_Target t_Shop_Target = targetService.queryTargetByNumber(param);
		if(t_Shop_Target != null){
			model.addAttribute("t_Shop_Target", t_Shop_Target);
		}else {
			model.addAttribute("t_Shop_Target", new T_Shop_Target());
		}*/
		return "shop/target/update";
	}
	
	@RequestMapping(value = "/to_update_detaillist", method = { RequestMethod.GET, RequestMethod.POST })
	public String to_update_detaillist(HttpServletRequest request,Model model) {
		String ta_number = request.getParameter("ta_number");
		String ta_state = request.getParameter("ta_state");
		String tad_em_code = request.getParameter("tad_em_code");
		String tad_em_name = request.getParameter("tad_em_name");
		String tad_target_money = request.getParameter("tad_target_money");
		String tad_project_type = request.getParameter("tad_project_type");
		model.addAttribute("ta_number", ta_number);
		model.addAttribute("ta_state", ta_state);
		model.addAttribute("tad_em_code", tad_em_code);
		model.addAttribute("tad_em_name", StringUtil.decodeString(tad_em_name));
		model.addAttribute("tad_target_money", tad_target_money);
		model.addAttribute("tad_project_type", tad_project_type);
		return "shop/target/update_detaillist";
	}
	
	@RequestMapping(value = "/to_add_detaillist", method = RequestMethod.GET)
	public String to_add_detaillist(HttpServletRequest request,Model model) {
		String tad_em_code= request.getParameter("tad_em_code");
		String tad_em_name= request.getParameter("tad_em_name");
		String tad_target_money= request.getParameter("tad_target_money");
		String tad_project_type= request.getParameter("tad_project_type");
		String rowid = request.getParameter("rowid");
		model.addAttribute("tad_em_code", tad_em_code);
		model.addAttribute("tad_em_name", StringUtil.decodeString(tad_em_name));
		model.addAttribute("tad_target_money", tad_target_money);
		model.addAttribute("tad_project_type", tad_project_type);
		model.addAttribute("rowid", rowid);
		return "shop/target/add_detaillist";
	}
	
	
	/*@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String ta_type = request.getParameter("ta_type");
        String ta_state = request.getParameter("ta_state");
        String ta_shop_code = request.getParameter("ta_shop_code");
        String ta_number = request.getParameter("ta_number");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("ta_type", ta_type);
        param.put("ta_state", ta_state);
        param.put("ta_shop_code", ta_shop_code);
        param.put("ta_number", StringUtil.decodeString(ta_number));
		PageData<T_Shop_Target> pageData = targetService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "emp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object emp_list(HttpServletRequest request,HttpSession session) {
		String ta_shop_code = request.getParameter("ta_shop_code");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("ta_shop_code", ta_shop_code);
        //根据店铺编号查询员工
		return ajaxSuccess(targetService.emp_list(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Shop_Target t_Shop_Target = new T_Shop_Target();
		t_Shop_Target.setTa_shop_code(StringUtil.trimString(request.getParameter("ta_shop_code")));
		t_Shop_Target.setTa_type(Integer.parseInt(StringUtil.trimString(request.getParameter("ta_type"))));
		t_Shop_Target.setTa_begintime(StringUtil.trimString(request.getParameter("begindate")));
		t_Shop_Target.setTa_endtime(StringUtil.trimString(request.getParameter("enddate")));
		t_Shop_Target.setTa_money(Double.parseDouble(StringUtil.trimString(request.getParameter("ta_money"))));
		t_Shop_Target.setTa_vipcount(Integer.parseInt(StringUtil.trimString(request.getParameter("ta_vipcount"))));
		t_Shop_Target.setTa_summary(StringUtil.decodeString(request.getParameter("ta_summary")));
		t_Shop_Target.setTa_createtime(DateUtil.getCurrentTime());
		t_Shop_Target.setTa_state(0);
		t_Shop_Target.setCompanyid(user.getCompanyid());
		
		String planDetails = StringUtil.decodeString(request.getParameter("planDetails"));
		String detailListStr = StringUtil.decodeString(request.getParameter("detailList"));
		 
		List<T_Shop_Target_Detail> target_Details = null;
		if(!"".equals(StringUtil.trimString(planDetails))){
			target_Details = TargetVO.convertJsonToTargetDetail(planDetails, user);
		}
		
		List<T_Shop_Target_DetailList> detailList = null;
		if(!"".equals(StringUtil.trimString(detailListStr))){
			detailList = TargetVO.convertJsonToTargetDetailList(detailListStr, user);
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user", user);
		params.put("t_Shop_Target", t_Shop_Target);
		params.put("target_Details", target_Details);
		params.put("detailList", detailList);
		
		targetService.save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam String ids,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("ids", ids);
		targetService.del(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "queryTargetDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryTargetDetail(HttpServletRequest request,HttpSession session) {
        String ta_number = request.getParameter("ta_number");
        String ta_state = request.getParameter("ta_state");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("ta_number", ta_number);
        param.put("ta_state", ta_state);
		return ajaxSuccess(targetService.queryTargetDetail(param));
	}
	
	@RequestMapping(value = "queryTargetDetailList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryTargetDetailList(HttpServletRequest request,HttpSession session) {
        String ta_number = request.getParameter("ta_number");
        String ta_state = request.getParameter("ta_state");
        String tad_em_code = request.getParameter("tad_em_code");
        String tad_project_type = request.getParameter("tad_project_type");
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("ta_number", ta_number);
        param.put("ta_state", ta_state);
        param.put("tad_em_code", tad_em_code);
        param.put("tad_project_type", tad_project_type);
        List<T_Shop_Target_DetailList> target_DetailLists = null;
        if("".equals(StringUtil.trimString(tad_project_type)) 
        		||  "0".equals(StringUtil.trimString(tad_project_type))){
        	target_DetailLists = new ArrayList<T_Shop_Target_DetailList>();
        }else {
        	target_DetailLists = targetService.queryTargetDetailList(param);
		}
		return ajaxSuccess(target_DetailLists);
	}
	
	@RequestMapping(value = "updateTargetDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateTargetDetail(HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		String ta_number = StringUtil.trimString(request.getParameter("ta_number"));
		String ta_shop_code = StringUtil.trimString(request.getParameter("ta_shop_code"));
		String ta_complete = StringUtil.decodeString(request.getParameter("ta_complete"));
		String targetDetailMedals = StringUtil.decodeString(request.getParameter("targetDetailMedals"));
		 
		List<T_Shop_Target_Detail> target_Details = null;
		if(!"".equals(StringUtil.trimString(targetDetailMedals))){
			target_Details = TargetVO.convertJsonToTargetDetailUpdate(targetDetailMedals, user, ta_number);
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user", user);
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("ta_number", ta_number);
		params.put("ta_shop_code", ta_shop_code);
		params.put("ta_complete", ta_complete);
		params.put("target_Details", target_Details);
		targetService.updateTargetDetail(params);
		return ajaxSuccess();
	}*/
}
