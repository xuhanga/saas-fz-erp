package zy.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.shop.want.T_Shop_Want;
import zy.entity.shop.want.T_Shop_WantList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.want.WantService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.shop.WantVO;

@Controller
@RequestMapping("shop/want")
public class WantController extends BaseController{
	@Resource
	private WantService wantService;
	
	@RequestMapping(value = "to_list/{wt_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer wt_type,Model model) {
		model.addAttribute("wt_type", wt_type);
		return "shop/want/list";
	}
	@RequestMapping(value = "to_list_warn/{wt_type}", method = RequestMethod.GET)
	public String to_list_warn(@PathVariable Integer wt_type,Model model) {
		model.addAttribute("wt_type", wt_type);
		return "shop/want/list_warn";
	}
	@RequestMapping(value = "to_list_copy_dialog", method = RequestMethod.GET)
	public String to_list_copy_dialog() {
		return "shop/want/list_copy_dialog";
	}
	@RequestMapping(value = "to_add/{wt_type}", method = RequestMethod.GET)
	public String to_add(@PathVariable Integer wt_type,Model model) {
		model.addAttribute("wt_type", wt_type);
		return "shop/want/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer wt_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Shop_Want want = wantService.load(wt_id);
		if(null != want){
			wantService.initUpdate(want.getWt_number(), want.getWt_type(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("want",want);
		}
		return "shop/want/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer wt_id,Model model,
			@RequestParam(required = false) String oper) {
		T_Shop_Want want = wantService.load(wt_id);
		if(null != want){
			model.addAttribute("want",want);
		}
		if(StringUtil.isNotEmpty(oper)){
			model.addAttribute("oper",oper);
		}
		return "shop/want/view";
	}
	@RequestMapping(value = "to_send", method = RequestMethod.GET)
	public String to_send(@RequestParam Integer wt_id,Model model) {
		T_Shop_Want want = wantService.load(wt_id);
		if(null != want){
			model.addAttribute("want",want);
		}
		return "shop/want/send";
	}
	@RequestMapping(value = "to_select_product/{wt_type}", method = RequestMethod.GET)
	public String to_select_product(@PathVariable Integer wt_type,Model model) {
		model.addAttribute("wt_type", wt_type);
		return "shop/want/select_product";
	}
	@RequestMapping(value = "to_temp_update/{wt_type}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable Integer wt_type,Model model) {
		model.addAttribute("wt_type", wt_type);
		return "shop/want/temp_update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer wt_type,
			@RequestParam(required = true) Integer wt_isdraft,
			@RequestParam(required = false) Integer wt_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String wt_shop_code,
			@RequestParam(required = false) String wt_outdp_code,
			@RequestParam(required = false) String wt_indp_code,
			@RequestParam(required = false) String wt_manager,
			@RequestParam(required = false) String wt_number,
			@RequestParam(required = false) String fromJsp,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("us_id", user.getUs_id());
        param.put("wt_type", wt_type);
        param.put("wt_isdraft", wt_isdraft);
        param.put("wt_ar_state", wt_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("wt_shop_code", wt_shop_code);
        param.put("wt_outdp_code", wt_outdp_code);
        param.put("wt_indp_code", wt_indp_code);
        param.put("wt_manager", StringUtil.decodeString(wt_manager));
        param.put("wt_number", StringUtil.decodeString(wt_number));
        param.put("fromJsp", fromJsp);
		return ajaxSuccess(wantService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String wt_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String wt_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String wt_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String wt_number,
			@RequestParam Integer wt_ar_state,@RequestParam Integer isFHD,@RequestParam Integer isSend,
			HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("wt_ar_state", wt_ar_state);
		params.put("isFHD", isFHD);
		params.put("isSend", isSend);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_list(@PathVariable Integer wt_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_type", wt_type);
		params.put("wtl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(wantService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_sum(@PathVariable Integer wt_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(wantService.temp_sum(wt_type,user.getUs_id(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_size_title/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size_title(@PathVariable Integer wt_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_type", wt_type);
		params.put("wtl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(wantService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size(@PathVariable Integer wt_type,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_type", wt_type);
		params.put("wtl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(wantService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(@PathVariable Integer wt_type,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String sp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wt_type", wt_type);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("sp_code", sp_code);
		params.put("user", user);
		Map<String, Object> resultMap = wantService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "detail_save_bybarcode/{wt_type}/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_save_bybarcode(@PathVariable Integer wt_type,@PathVariable String wt_number,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String sp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wt_type", wt_type);
		params.put("wt_number", wt_number);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("sp_code", sp_code);
		params.put("user", user);
		Map<String, Object> resultMap = wantService.detail_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page_product(PageForm pageForm,
			@RequestParam(required = true) String wt_type,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			@RequestParam(required = false) String wt_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("wt_type", wt_type);
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
        param.put("wt_number", wt_number);
		return ajaxSuccess(wantService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_loadproduct(@PathVariable Integer wt_type,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = true) String sp_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("wt_type", wt_type);
		params.put("pd_code", pd_code);
		params.put("sp_code", sp_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(wantService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "send_loadproduct/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object send_loadproduct(@PathVariable String wt_number,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = true) String sp_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("wt_number", wt_number);
		params.put("pd_code", pd_code);
		params.put("sp_code", sp_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(wantService.send_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@PathVariable Integer wt_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Shop_WantList> temps = WantVO.convertMap2Model(data, wt_type, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wt_type", wt_type);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		wantService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "send_save_detail/{wt_type}/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object send_save_detail(@PathVariable Integer wt_type,@PathVariable String wt_number,
			@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Shop_WantList> temps = WantVO.convertMap2Model_Send(data, wt_type,wt_number, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wt_number", wt_number);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		wantService.send_save_detail(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer wtl_id) {
		wantService.temp_del(wtl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Shop_WantList temp) {
		wantService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(@PathVariable Integer wt_type,T_Shop_WantList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setWtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setWtl_type(wt_type);
		wantService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Shop_WantList temp) {
		temp.setWtl_remark(StringUtil.decodeString(temp.getWtl_remark()));
		wantService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(@PathVariable Integer wt_type,T_Shop_WantList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setWtl_remark(StringUtil.decodeString(temp.getWtl_remark()));
		temp.setWtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setWtl_type(wt_type);
		wantService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@PathVariable Integer wt_type,
			@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Shop_WantList temp = new T_Shop_WantList();
		temp.setWtl_pd_code(pd_code);
		temp.setWtl_cr_code(cr_code);
		temp.setWtl_br_code(br_code);
		temp.setWtl_type(wt_type);
		temp.setWtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		wantService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(@PathVariable Integer wt_type, HttpSession session) {
		T_Sys_User user = getUser(session);
		wantService.temp_clear(wt_type, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@PathVariable Integer wt_type,@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = WantVO.convertMap2Model_import(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wt_type", wt_type);
		params.put("datas", datas);
		params.put("user", user);
		params.put("sp_code", data.get("sp_code"));
		wantService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_copy/{wt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_copy(@PathVariable Integer wt_type,@RequestParam String ids,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wt_type", wt_type);
		params.put("ids", ids);
		params.put("user", user);
		wantService.temp_copy(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_updateAmount(T_Shop_WantList temp) {
		wantService.detail_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_updatePrice", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_updatePrice(T_Shop_WantList temp,HttpSession session) {
		temp.setCompanyid(getCompanyid(session));
		wantService.detail_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_automatch", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_automatch(String number,HttpSession session) {
		wantService.detail_automatch(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_del(@RequestParam Integer wtl_id) {
		wantService.detail_del(wtl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Shop_Want want,HttpSession session) {
		wantService.save(want, getUser(session));
		return ajaxSuccess(want);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Shop_Want want,HttpSession session) {
		wantService.update(want, getUser(session));
		return ajaxSuccess(want);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Shop_Want want = wantService.approve(number, record, getUser(session));
		return ajaxSuccess(want);
	}
	
	@RequestMapping(value = "send", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object send(String number,String wt_outdp_code,HttpSession session) {
		return ajaxSuccess(wantService.send(number, wt_outdp_code, getUser(session)));
	}
	
	@RequestMapping(value = "receive", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object receive(String number,String wt_indp_code,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("number", number);
		params.put("wt_indp_code", wt_indp_code);
		params.put("us_name", user.getUs_name());
		params.put("us_id", user.getUs_id());
		return ajaxSuccess(wantService.receive(params));
	}
	
	@RequestMapping(value = "reject", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reject(String number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("number", number);
		params.put("us_name", user.getUs_name());
		return ajaxSuccess(wantService.reject(params));
	}
	
	@RequestMapping(value = "rejectconfirm", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object rejectconfirm(String number,HttpSession session) {
		return ajaxSuccess(wantService.rejectconfirm(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		wantService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,
			@RequestParam Integer isSend, HttpSession session) {
		Map<String, Object> resultMap = wantService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		resultMap.put("isSend", isSend);
		Map<String, Object> temp = WantVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
