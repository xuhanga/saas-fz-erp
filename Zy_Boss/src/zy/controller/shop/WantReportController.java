package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.shop.want.WantReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/want")
public class WantReportController extends BaseController{
	@Resource
	private WantReportService wantReportService;
	
	@RequestMapping(value = "to_report_want", method = RequestMethod.GET)
	public String to_report_want() {
		return "shop/want/report_want";
	}
	
	@RequestMapping(value = "to_report_want_detail", method = RequestMethod.GET)
	public String to_report_want_detail() {
		return "shop/want/report_want_detail";
	}
	
	@RequestMapping(value = "pageWantReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageWantReport(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String wt_type,
			@RequestParam(required = false) String wt_ar_state,
			@RequestParam(required = false) String wt_shop_code,
			@RequestParam(required = false) String wt_outdp_code,
			@RequestParam(required = false) String wt_indp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String wt_manager,
			@RequestParam(required = false) String pd_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("wt_type", wt_type);
        params.put("wt_ar_state", wt_ar_state);
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("wt_shop_code", wt_shop_code);
        params.put("wt_outdp_code", wt_outdp_code);
        params.put("wt_indp_code", wt_indp_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("pd_season", pd_season);
        params.put("pd_year", pd_year);
        params.put("wt_manager", StringUtil.decodeString(wt_manager));
        params.put("pd_code", pd_code);
		return ajaxSuccess(wantReportService.pageWantReport(params));
	}
	
	@RequestMapping(value = "pageWantDetailReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageWantDetailReport(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String wt_type,
			@RequestParam(required = false) String wt_ar_state,
			@RequestParam(required = false) String wt_shop_code,
			@RequestParam(required = false) String wt_outdp_code,
			@RequestParam(required = false) String wt_indp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String wt_manager,
			@RequestParam(required = false) String pd_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("wt_type", wt_type);
        params.put("wt_ar_state", wt_ar_state);
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("wt_shop_code", wt_shop_code);
        params.put("wt_outdp_code", wt_outdp_code);
        params.put("wt_indp_code", wt_indp_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("pd_season", pd_season);
        params.put("pd_year", pd_year);
        params.put("wt_manager", StringUtil.decodeString(wt_manager));
        params.put("pd_code", pd_code);
		return ajaxSuccess(wantReportService.pageWantDetailReport(params));
	}
	
}
