package zy.controller.sort;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sort.allot.T_Sort_Allot;
import zy.entity.sort.allot.T_Sort_AllotList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.report.excel.ReportInterfaceFactory;
import zy.service.report.excel.reporter.AllotReporter;
import zy.service.sort.allot.AllotService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.sort.AllotVO;

@Controller
@RequestMapping("sort/allot")
public class AllotController extends BaseController {
	@Resource
	private AllotService allotService;
	@Resource
	private ReportInterfaceFactory reportInterfaceFactory;
	
	@RequestMapping(value = "to_list/{at_type}/{allot_up}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer at_type,@PathVariable Integer allot_up,Model model) {
		model.addAttribute("at_type", at_type);
		model.addAttribute("allot_up", allot_up);//此字段针对分公司有用，0-配货发货单、退货确认单，1-配货申请单、退货申请单
		return "sort/allot/list";
	}
	@RequestMapping(value = "to_list_warn/{at_type}/{allot_up}", method = RequestMethod.GET)
	public String to_list_warn(@PathVariable Integer at_type,@PathVariable Integer allot_up,Model model) {
		model.addAttribute("at_type", at_type);
		model.addAttribute("allot_up", allot_up);//此字段针对分公司有用，0-配货发货单、退货确认单，1-配货申请单、退货申请单
		return "sort/allot/list_warn";
	}
	@RequestMapping(value = "to_list_copy_dialog/{allot_up}", method = RequestMethod.GET)
	public String to_list_copy_dialog(@PathVariable Integer allot_up,Model model) {
		model.addAttribute("allot_up", allot_up);
		return "sort/allot/list_copy_dialog";
	}
	@RequestMapping(value = "to_add/{at_type}/{allot_up}", method = RequestMethod.GET)
	public String to_add(@PathVariable Integer at_type,@PathVariable Integer allot_up,Model model) {
		model.addAttribute("at_type", at_type);
		model.addAttribute("allot_up", allot_up);
		return "sort/allot/add";
	}
	@RequestMapping(value = "to_update/{allot_up}", method = RequestMethod.GET)
	public String to_update(@PathVariable Integer allot_up,@RequestParam Integer at_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Sort_Allot allot = allotService.load(at_id);
		if(null != allot){
			allotService.initUpdate(allot.getAt_number(), allot.getAt_type(),allot_up, user.getUs_id(), user.getCompanyid());
			model.addAttribute("allot",allot);
		}
		return "sort/allot/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer at_id,Model model,
			@RequestParam(required = false) String oper) {
		T_Sort_Allot allot = allotService.load(at_id);
		if(null != allot){
			model.addAttribute("allot",allot);
		}
		if(StringUtil.isNotEmpty(oper)){
			model.addAttribute("oper",oper);
		}
		return "sort/allot/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Sort_Allot allot = allotService.load(number,getCompanyid(session));
		if(null != allot){
			model.addAttribute("allot",allot);
		}
		return "sort/allot/view";
	}
	@RequestMapping(value = "to_send/{allot_up}", method = RequestMethod.GET)
	public String to_send(@PathVariable Integer allot_up,@RequestParam Integer at_id,Model model) {
		T_Sort_Allot allot = allotService.load(at_id);
		if(null != allot){
			model.addAttribute("allot",allot);
		}
		model.addAttribute("allot_up", allot_up);
		return "sort/allot/send";
	}
	@RequestMapping(value = "to_select_product/{at_type}/{allot_up}", method = RequestMethod.GET)
	public String to_select_product(@PathVariable Integer at_type,@PathVariable Integer allot_up,Model model) {
		model.addAttribute("at_type", at_type);
		model.addAttribute("allot_up", allot_up);
		return "sort/allot/select_product";
	}
	@RequestMapping(value = "to_temp_update/{at_type}/{allot_up}", method = RequestMethod.GET)
	public String to_temp_update(@PathVariable Integer at_type,@PathVariable Integer allot_up,Model model) {
		model.addAttribute("at_type", at_type);
		model.addAttribute("allot_up", allot_up);
		return "sort/allot/temp_update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer at_type,
			@RequestParam(required = true) Integer allot_up,
			@RequestParam(required = true) Integer at_isdraft,
			@RequestParam(required = false) Integer at_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String at_shop_code,
			@RequestParam(required = false) String at_outdp_code,
			@RequestParam(required = false) String at_indp_code,
			@RequestParam(required = false) String at_manager,
			@RequestParam(required = false) String at_number,
			@RequestParam(required = false) String fromJsp,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("at_type", at_type);
        param.put("allot_up", allot_up);
        param.put("at_isdraft", at_isdraft);
        param.put("at_ar_state", at_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("at_shop_code", at_shop_code);
        param.put("at_outdp_code", at_outdp_code);
        param.put("at_indp_code", at_indp_code);
        param.put("at_manager", StringUtil.decodeString(at_manager));
        param.put("at_number", StringUtil.decodeString(at_number));
        param.put("fromJsp", fromJsp);
		return ajaxSuccess(allotService.page(param));
	}
	
	@RequestMapping(value = "report", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String report(PageForm pageForm,
			@RequestParam(required = true) Integer at_type,
			@RequestParam(required = true) Integer allot_up,
			@RequestParam(required = true) Integer at_isdraft,
			@RequestParam(required = false) Integer at_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String at_shop_code,
			@RequestParam(required = false) String at_outdp_code,
			@RequestParam(required = false) String at_indp_code,
			@RequestParam(required = false) String at_manager,
			@RequestParam(required = false) String at_number,
			@RequestParam(required = false) String fromJsp,
			HttpSession session,
			HttpServletRequest request,HttpServletResponse response) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("at_type", at_type);
        param.put("allot_up", allot_up);
        param.put("at_isdraft", at_isdraft);
        param.put("at_ar_state", at_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("at_shop_code", at_shop_code);
        param.put("at_outdp_code", at_outdp_code);
        param.put("at_indp_code", at_indp_code);
        param.put("at_manager", StringUtil.decodeString(at_manager));
        param.put("at_number", StringUtil.decodeString(at_number));
        param.put("fromJsp", fromJsp);
        
        File file = reportInterfaceFactory.newInstance(AllotReporter.class, param).getFile();
        return download(file, request, response);
	}
	
	@RequestMapping(value = "detail_list/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String at_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_number", at_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allotService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String at_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_number", at_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allotService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String at_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_number", at_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allotService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String at_number,
			@RequestParam Integer at_ar_state,@RequestParam Integer isFHD,@RequestParam Integer isSend,
			HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_number", at_number);
		params.put("at_ar_state", at_ar_state);
		params.put("isFHD", isFHD);
		params.put("isSend", isSend);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(allotService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_list(@PathVariable Integer at_type,@PathVariable Integer allot_up,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_type", at_type);
		params.put("atl_up", allot_up);
		params.put("atl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allotService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_sum(@PathVariable Integer at_type,@PathVariable Integer allot_up,HttpSession session) {
		T_Sys_User user = getUser(session);
		return ajaxSuccess(allotService.temp_sum(at_type,allot_up,user.getUs_id(), user.getCompanyid()));
	}
	
	@RequestMapping(value = "temp_size_title/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size_title(@PathVariable Integer at_type,@PathVariable Integer allot_up,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_type", at_type);
		params.put("atl_up", allot_up);
		params.put("atl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allotService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_size(@PathVariable Integer at_type,@PathVariable Integer allot_up,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("atl_type", at_type);
		params.put("atl_up", allot_up);
		params.put("atl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(allotService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(@PathVariable Integer at_type,@PathVariable Integer allot_up,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String sp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("at_type", at_type);
		params.put("atl_up", allot_up);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("sp_code", sp_code);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("user", user);
		Map<String, Object> resultMap = allotService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "detail_save_bybarcode/{at_type}/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_save_bybarcode(@PathVariable Integer at_type,@PathVariable String at_number,HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount,
			@RequestParam(required = false) String sp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("at_type", at_type);
		params.put("at_number", at_number);
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("sp_code", sp_code);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("user", user);
		Map<String, Object> resultMap = allotService.detail_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object page_product(PageForm pageForm,
			@RequestParam(required = true) String at_type,
			@RequestParam(required = true) String allot_up,
			@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			@RequestParam(required = false) String at_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("at_type", at_type);
        param.put("atl_up", allot_up);
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
        param.put("at_number", at_number);
		return ajaxSuccess(allotService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object temp_loadproduct(@PathVariable Integer at_type,@PathVariable Integer allot_up,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = true) String sp_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("at_type", at_type);
		params.put("atl_up", allot_up);
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("dp_code", dp_code);
		params.put("sp_code", sp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(allotService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "send_loadproduct/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object send_loadproduct(@PathVariable String at_number,HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = true) String sp_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String priceType,
			@RequestParam(required = false) Double sp_rate) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("at_number", at_number);
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("priceType", priceType);
		params.put("sp_rate", sp_rate);
		params.put("dp_code", dp_code);
		params.put("sp_code", sp_code);
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(allotService.send_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@PathVariable Integer at_type,@PathVariable Integer allot_up,
			@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Sort_AllotList> temps = AllotVO.convertMap2Model(data, at_type,allot_up, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("at_type", at_type);
		params.put("atl_up", allot_up);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		allotService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "send_save_detail/{at_type}/{at_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object send_save_detail(@PathVariable Integer at_type,@PathVariable String at_number,
			@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Sort_AllotList> temps = AllotVO.convertMap2Model_Send(data, at_type,at_number, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("at_number", at_number);
		params.put("temps", temps);
		params.put("user", user);
		params.put("pd_code", data.get("pd_code"));
		params.put("unitPrice", data.get("unitPrice"));
		allotService.send_save_detail(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer atl_id) {
		allotService.temp_del(atl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Sort_AllotList temp) {
		allotService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrice/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrice(@PathVariable Integer at_type,@PathVariable Integer allot_up,T_Sort_AllotList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setAtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setAtl_type(at_type);
		temp.setAtl_up(allot_up);
		allotService.temp_updatePrice(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Sort_AllotList temp) {
		temp.setAtl_remark(StringUtil.decodeString(temp.getAtl_remark()));
		allotService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(@PathVariable Integer at_type,@PathVariable Integer allot_up,
			T_Sort_AllotList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setAtl_remark(StringUtil.decodeString(temp.getAtl_remark()));
		temp.setAtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		temp.setAtl_type(at_type);
		temp.setAtl_up(allot_up);
		allotService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@PathVariable Integer at_type,@PathVariable Integer allot_up,
			@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Sort_AllotList temp = new T_Sort_AllotList();
		temp.setAtl_pd_code(pd_code);
		temp.setAtl_cr_code(cr_code);
		temp.setAtl_br_code(br_code);
		temp.setAtl_type(at_type);
		temp.setAtl_up(allot_up);
		temp.setAtl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		allotService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(@PathVariable Integer at_type,@PathVariable Integer allot_up, HttpSession session) {
		T_Sys_User user = getUser(session);
		allotService.temp_clear(at_type,allot_up, user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@PathVariable Integer at_type,@PathVariable Integer allot_up,
			@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = AllotVO.convertMap2Model_import(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("at_type", at_type);
		params.put("atl_up", allot_up);
		params.put("datas", datas);
		params.put("user", user);
		params.put("sp_code", data.get("sp_code"));
		params.put("priceType", data.get("priceType"));
		params.put("sp_rate", data.get("sp_rate"));
		allotService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_copy/{at_type}/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_copy(@PathVariable Integer at_type,@PathVariable Integer allot_up,
			@RequestParam String ids,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("at_type", at_type);
		params.put("atl_up", allot_up);
		params.put("ids", ids);
		params.put("user", user);
		allotService.temp_copy(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_updateAmount(T_Sort_AllotList temp) {
		allotService.detail_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_automatch", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_automatch(String number,HttpSession session) {
		allotService.detail_automatch(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "detail_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_del(@RequestParam Integer atl_id) {
		allotService.detail_del(atl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(@PathVariable Integer allot_up,T_Sort_Allot allot,HttpSession session) {
		allotService.save(allot, getUser(session), allot_up);
		return ajaxSuccess(allot);
	}
	
	@RequestMapping(value = "update/{allot_up}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(@PathVariable Integer allot_up,T_Sort_Allot allot,HttpSession session) {
		allotService.update(allot, getUser(session), allot_up);
		return ajaxSuccess(allot);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Sort_Allot allot = allotService.approve(number, record, getUser(session));
		return ajaxSuccess(allot);
	}
	
	@RequestMapping(value = "send", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object send(String number,String at_outdp_code,HttpSession session) {
		return ajaxSuccess(allotService.send(number, at_outdp_code, getUser(session)));
	}
	
	@RequestMapping(value = "receive", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object receive(String number,String at_indp_code,HttpSession session) {
		return ajaxSuccess(allotService.receive(number, at_indp_code, getUser(session)));
	}
	
	@RequestMapping(value = "reject", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reject(String number,HttpSession session) {
		return ajaxSuccess(allotService.reject(number, getUser(session)));
	}
	
	@RequestMapping(value = "rejectconfirm", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object rejectconfirm(String number,HttpSession session) {
		return ajaxSuccess(allotService.rejectconfirm(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		allotService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,
			@RequestParam Integer isSend,HttpSession session) {
		Map<String, Object> resultMap = allotService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		resultMap.put("isSend", isSend);
		Map<String, Object> temp = AllotVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
