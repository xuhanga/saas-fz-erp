package zy.controller.sort;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sort.allot.AllotReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sort/allot")
public class AllotReportController extends BaseController{
	@Resource
	private AllotReportService allotReportService;
	
	@RequestMapping(value = "to_report_allot", method = RequestMethod.GET)
	public String to_report_allot() {
		return "sort/allot/report_allot";
	}
	
	@RequestMapping(value = "to_report_allot_detail", method = RequestMethod.GET)
	public String to_report_allot_detail() {
		return "sort/allot/report_allot_detail";
	}
	
	@RequestMapping(value = "pageAllotReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageAllotReport(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String at_type,
			@RequestParam(required = false) String at_ar_state,
			@RequestParam(required = false) String at_shop_code,
			@RequestParam(required = false) String at_outdp_code,
			@RequestParam(required = false) String at_indp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String at_manager,
			@RequestParam(required = false) String pd_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("at_type", at_type);
        params.put("at_ar_state", at_ar_state);
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("at_shop_code", at_shop_code);
        params.put("at_outdp_code", at_outdp_code);
        params.put("at_indp_code", at_indp_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("pd_season", pd_season);
        params.put("pd_year", pd_year);
        params.put("at_manager", StringUtil.decodeString(at_manager));
        params.put("pd_code", pd_code);
		return ajaxSuccess(allotReportService.pageAllotReport(params));
	}
	
	@RequestMapping(value = "pageAllotDetailReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageAllotDetailReport(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String at_type,
			@RequestParam(required = false) String at_ar_state,
			@RequestParam(required = false) String at_shop_code,
			@RequestParam(required = false) String at_outdp_code,
			@RequestParam(required = false) String at_indp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String at_manager,
			@RequestParam(required = false) String pd_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("at_type", at_type);
        params.put("at_ar_state", at_ar_state);
        params.put("begindate", begindate);
        params.put("enddate", enddate);
        params.put("at_shop_code", at_shop_code);
        params.put("at_outdp_code", at_outdp_code);
        params.put("at_indp_code", at_indp_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("pd_season", pd_season);
        params.put("pd_year", pd_year);
        params.put("at_manager", StringUtil.decodeString(at_manager));
        params.put("pd_code", pd_code);
		return ajaxSuccess(allotReportService.pageAllotDetailReport(params));
	}
	
	
}
