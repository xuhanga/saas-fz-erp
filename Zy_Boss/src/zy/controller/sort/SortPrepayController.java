package zy.controller.sort;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sort.prepay.T_Sort_Prepay;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sort.prepay.SortPrepayService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sort/prepay")
public class SortPrepayController extends BaseController{
	@Resource
	private SortPrepayService sortPrepayService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sort/prepay/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sort/prepay/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer pp_id,Model model) {
		T_Sort_Prepay prepay = sortPrepayService.load(pp_id);
		if(null != prepay){
			model.addAttribute("prepay",prepay);
			model.addAttribute("shop",sortPrepayService.loadShop(prepay.getPp_shop_code(), prepay.getCompanyid()));
			model.addAttribute("bank",sortPrepayService.loadBank(prepay.getPp_ba_code(), prepay.getCompanyid()));
		}
		return "sort/prepay/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer pp_id,Model model) {
		T_Sort_Prepay prepay = sortPrepayService.load(pp_id);
		if(null != prepay){
			model.addAttribute("prepay",prepay);
		}
		return "sort/prepay/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Sort_Prepay prepay = sortPrepayService.load(number, getCompanyid(session));
		if(null != prepay){
			model.addAttribute("prepay",prepay);
		}
		return "sort/prepay/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer pp_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String pp_shop_code,
			@RequestParam(required = false) String pp_ba_code,
			@RequestParam(required = false) String pp_manager,
			@RequestParam(required = false) String pp_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("pp_ar_state", pp_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("pp_shop_code", pp_shop_code);
        param.put("pp_ba_code", pp_ba_code);
        param.put("pp_manager", StringUtil.decodeString(pp_manager));
        param.put("pp_number", StringUtil.decodeString(pp_number));
		return ajaxSuccess(sortPrepayService.page(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sort_Prepay prepay,HttpSession session) {
		sortPrepayService.save(prepay, getUser(session));
		return ajaxSuccess(prepay);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sort_Prepay prepay,HttpSession session) {
		sortPrepayService.update(prepay, getUser(session));
		return ajaxSuccess(prepay);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(sortPrepayService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(sortPrepayService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		sortPrepayService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
}
