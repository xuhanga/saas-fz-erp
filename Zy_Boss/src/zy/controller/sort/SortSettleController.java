package zy.controller.sort;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.sort.settle.T_Sort_Settle;
import zy.entity.sort.settle.T_Sort_SettleList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sort.settle.SortSettleService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.sort.SortSettleVO;

@Controller
@RequestMapping("sort/settle")
public class SortSettleController extends BaseController{
	@Resource
	private SortSettleService sortSettleService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sort/settle/list";
	}
	@RequestMapping(value = "to_list_detail", method = RequestMethod.GET)
	public String to_list_detail() {
		return "sort/settle/list_detail";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sort/settle/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer st_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Sort_Settle settle = sortSettleService.load(st_id);
		if(null != settle){
			sortSettleService.initUpdate(settle.getSt_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("settle",settle);
			model.addAttribute("shop",sortSettleService.loadShop(settle.getSt_shop_code(), settle.getCompanyid()));
		}
		return "sort/settle/update";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer st_id,Model model) {
		T_Sort_Settle settle = sortSettleService.load(st_id);
		if(null != settle){
			model.addAttribute("settle",settle);
			model.addAttribute("shop",sortSettleService.loadShop(settle.getSt_shop_code(), settle.getCompanyid()));
		}
		return "sort/settle/view";
	}
	@RequestMapping(value = "to_view/{number}", method = RequestMethod.GET)
	public String to_view(@PathVariable String number,Model model,HttpSession session) {
		T_Sort_Settle settle = sortSettleService.load(number, getCompanyid(session));
		if(null != settle){
			model.addAttribute("settle",settle);
			model.addAttribute("shop",sortSettleService.loadShop(settle.getSt_shop_code(), settle.getCompanyid()));
		}
		return "sort/settle/view";
	}
	
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) Integer st_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String st_shop_code,
			@RequestParam(required = false) String st_manager,
			@RequestParam(required = false) String st_number,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("st_ar_state", st_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("st_shop_code", st_shop_code);
        param.put("st_manager", StringUtil.decodeString(st_manager));
        param.put("st_number", StringUtil.decodeString(st_number));
		return ajaxSuccess(sortSettleService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{st_number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object detail_list(@PathVariable String st_number,HttpSession session) {
		return ajaxSuccess(sortSettleService.detail_list(st_number,getCompanyid(session)));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("stl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(sortSettleService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(String sp_code,HttpSession session) {
		sortSettleService.temp_save(sp_code, getUser(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateDiscountMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateDiscountMoney(T_Sort_SettleList temp) {
		sortSettleService.temp_updateDiscountMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updatePrepay", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updatePrepay(T_Sort_SettleList temp) {
		sortSettleService.temp_updatePrepay(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRealMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRealMoney(T_Sort_SettleList temp) {
		sortSettleService.temp_updateRealMoney(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemark", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemark(T_Sort_SettleList temp) {
		sortSettleService.temp_updateRemark(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateJoin", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateJoin(String ids,String stl_join) {
		sortSettleService.temp_updateJoin(ids, stl_join);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_entire", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_entire(Double discount_money,Double prepay,Double received,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("discount_money", discount_money);
		params.put("prepay", prepay);
		params.put("received", received);
		params.put("user", getUser(session));
		return ajaxSuccess(sortSettleService.temp_entire(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sort_Settle settle,HttpSession session) {
		sortSettleService.save(settle, getUser(session));
		return ajaxSuccess(settle);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sort_Settle settle,HttpSession session) {
		sortSettleService.update(settle, getUser(session));
		return ajaxSuccess(settle);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		return ajaxSuccess(sortSettleService.approve(number, record, getUser(session)));
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(sortSettleService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		sortSettleService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,HttpSession session) {
		Map<String, Object> resultMap = sortSettleService.loadPrintData(number, sp_id, getUser(session));
		resultMap.put("user", getUser(session));
		return ajaxSuccess(SortSettleVO.buildPrintJson(resultMap));
	}
	
}
