package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.adjust.T_Stock_Adjust;
import zy.entity.stock.adjust.T_Stock_AdjustList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.adjust.AdjustService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.stock.AdjustVO;

@Controller
@RequestMapping("stock/adjust")
public class AdjustController extends BaseController{
	@Resource
	private AdjustService adjustService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "stock/adjust/list";
	}
	@RequestMapping(value = "to_list_draft_dialog", method = RequestMethod.GET)
	public String to_list_draft_dialog() {
		return "stock/adjust/list_draft_dialog";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "stock/adjust/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer aj_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_Adjust adjust = adjustService.load(aj_id);
		if(null != adjust){
			adjustService.initUpdate(adjust.getAj_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("adjust",adjust);
		}
		return "stock/adjust/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer aj_id,Model model) {
		T_Stock_Adjust adjust = adjustService.load(aj_id);
		if(null != adjust){
			model.addAttribute("adjust",adjust);
		}
		return "stock/adjust/view";
	}
	@RequestMapping(value = "to_select_product", method = RequestMethod.GET)
	public String to_select_product() {
		return "stock/adjust/select_product";
	}
	@RequestMapping(value = "to_temp_update", method = RequestMethod.GET)
	public String to_temp_update() {
		return "stock/adjust/temp_update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer aj_isdraft,
			@RequestParam(required = false) Integer aj_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String aj_dp_code,
			@RequestParam(required = false) String aj_manager,
			@RequestParam(required = false) String aj_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("aj_isdraft", aj_isdraft);
        param.put("aj_ar_state", aj_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("aj_dp_code", aj_dp_code);
        param.put("aj_manager", StringUtil.decodeString(aj_manager));
        param.put("aj_number", StringUtil.decodeString(aj_number));
		return ajaxSuccess(adjustService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{aj_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String aj_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_number", aj_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(adjustService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{aj_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String aj_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_number", aj_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(adjustService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{aj_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String aj_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_number", aj_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(adjustService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{aj_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String aj_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_number", aj_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(adjustService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(adjustService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_sum(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(adjustService.temp_sum(params));
	}
	
	@RequestMapping(value = "temp_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size_title(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(adjustService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("ajl_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(adjustService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("user", user);
		Map<String, Object> resultMap = adjustService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(adjustService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_loadproduct(HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(adjustService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Stock_AdjustList> temps = AdjustVO.convertMap2Model(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("temps", temps);
		params.put("user", user);
		adjustService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer ajl_id) {
		adjustService.temp_del(ajl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Stock_AdjustList temp) {
		adjustService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Stock_AdjustList temp) {
		temp.setAjl_remark(StringUtil.decodeString(temp.getAjl_remark()));
		adjustService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(T_Stock_AdjustList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setAjl_remark(StringUtil.decodeString(temp.getAjl_remark()));
		temp.setAjl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		adjustService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_AdjustList temp = new T_Stock_AdjustList();
		temp.setAjl_pd_code(pd_code);
		temp.setAjl_cr_code(cr_code);
		temp.setAjl_br_code(br_code);
		temp.setAjl_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		adjustService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		adjustService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}

	@RequestMapping(value = "temp_import", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = AdjustVO.convertMap2Model_import(data);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("datas", datas);
		params.put("user", user);
		adjustService.temp_import(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import_draft", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@RequestParam String aj_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("aj_number", aj_number);
		params.put("user", user);
		adjustService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Stock_Adjust adjust,HttpSession session) {
		adjustService.save(adjust, getUser(session));
		return ajaxSuccess(adjust);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Stock_Adjust adjust,HttpSession session) {
		adjustService.update(adjust, getUser(session));
		return ajaxSuccess(adjust);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Stock_Adjust adjust = adjustService.approve(number, record, getUser(session));
		return ajaxSuccess(adjust);
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(adjustService.reverse(number, getUser(session)));
	}

	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		adjustService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = adjustService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = AdjustVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
