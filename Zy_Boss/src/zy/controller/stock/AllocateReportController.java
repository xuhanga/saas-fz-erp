package zy.controller.stock;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.allocate.StockAllocateReportService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("stock/allocate")
public class AllocateReportController extends BaseController{
	@Resource
	private StockAllocateReportService stockAllocateReportService;
	
	@RequestMapping(value = "to_report_allocate", method = RequestMethod.GET)
	public String to_report_allocate() {
		return "stock/allocate/report_allocate";
	}
	@RequestMapping(value = "to_report_allocate_detail", method = RequestMethod.GET)
	public String to_report_allocate_detail() {
		return "stock/allocate/report_allocate_detail";
	}
	
	@RequestMapping(value = "pageAllocateReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageAllocateReport(PageForm pageForm,
			@RequestParam(required = true) String type,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String dp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String ac_manager,
			@RequestParam(required = false) String pd_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("type", type);
        params.put("begindate", begindate);
        params.put("enddate", StringUtil.isEmpty(enddate) ? null : enddate + " 23:59:59");
        params.put("dp_code", dp_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("pd_season", pd_season);
        params.put("pd_year", pd_year);
        params.put("pd_code", pd_code);
        params.put("ac_manager", StringUtil.decodeString(ac_manager));
		return ajaxSuccess(stockAllocateReportService.pageAllocateReport(params));
	}
	
	@RequestMapping(value = "pageAllocateDetailReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageAllocateDetailReport(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String ac_outdp_code,
			@RequestParam(required = false) String ac_indp_code,
			@RequestParam(required = false) String bd_code,
			@RequestParam(required = false) String tp_code,
			@RequestParam(required = false) String pd_season,
			@RequestParam(required = false) String pd_year,
			@RequestParam(required = false) String ac_manager,
			@RequestParam(required = false) String pd_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        params.put("begindate", begindate);
        params.put("enddate", StringUtil.isEmpty(enddate) ? null : enddate + " 23:59:59");
        params.put("ac_outdp_code", ac_outdp_code);
        params.put("ac_indp_code", ac_indp_code);
        params.put("bd_code", bd_code);
        params.put("tp_code", tp_code);
        params.put("pd_season", pd_season);
        params.put("pd_year", pd_year);
        params.put("pd_code", pd_code);
        params.put("ac_manager", StringUtil.decodeString(ac_manager));
		return ajaxSuccess(stockAllocateReportService.pageAllocateDetailReport(params));
	}
	
}
