package zy.controller.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.approve.T_Approve_Record;
import zy.entity.stock.loss.T_Stock_Loss;
import zy.entity.stock.loss.T_Stock_LossList;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.stock.loss.LossService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.stock.LossVO;

@Controller
@RequestMapping("stock/loss")
public class LossController extends BaseController{
	
	@Resource
	private LossService lossService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "stock/loss/list";
	}
	@RequestMapping(value = "to_list_draft_dialog", method = RequestMethod.GET)
	public String to_list_draft_dialog() {
		return "stock/loss/list_draft_dialog";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "stock/loss/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer lo_id,Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_Loss loss = lossService.load(lo_id);
		if(null != loss){
			lossService.initUpdate(loss.getLo_number(), user.getUs_id(), user.getCompanyid());
			model.addAttribute("loss",loss);
		}
		return "stock/loss/update";
	}
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer lo_id,Model model) {
		T_Stock_Loss loss = lossService.load(lo_id);
		if(null != loss){
			model.addAttribute("loss",loss);
		}
		return "stock/loss/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer lo_isdraft,
			@RequestParam(required = false) Integer lo_ar_state,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String lo_dp_code,
			@RequestParam(required = false) String lo_manager,
			@RequestParam(required = false) String lo_number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("lo_isdraft", lo_isdraft);
        param.put("lo_ar_state", lo_ar_state);
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("lo_dp_code", lo_dp_code);
        param.put("lo_manager", StringUtil.decodeString(lo_manager));
        param.put("lo_number", StringUtil.decodeString(lo_number));
		return ajaxSuccess(lossService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{lo_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String lo_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_number", lo_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(lossService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{lo_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String lo_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_number", lo_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(lossService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{lo_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String lo_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_number", lo_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(lossService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{lo_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String lo_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_number", lo_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(lossService.detail_size(params));
	}
	
	@RequestMapping(value = "temp_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_list(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(lossService.temp_list(params));
	}
	
	@RequestMapping(value = "temp_sum", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_sum(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(lossService.temp_sum(params));
	}
	
	@RequestMapping(value = "temp_size_title", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size_title(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(lossService.temp_size_title(params));
	}
	
	@RequestMapping(value = "temp_size", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_size(HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lol_us_id", user.getUs_id());
		params.put("companyid", user.getCompanyid());
		return ajaxSuccess(lossService.temp_size(params));
	}
	
	@RequestMapping(value = "temp_save_bybarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save_bybarcode(HttpSession session,
			@RequestParam(required = true) String barcode,
			@RequestParam(required = true) Integer amount) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("barcode", barcode);
		params.put("amount", amount);
		params.put("user", user);
		Map<String, Object> resultMap = lossService.temp_save_bybarcode(params);
		return ajaxSuccess(resultMap);
	}
	
	@RequestMapping(value = "page_product", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page_product(PageForm pageForm,@RequestParam(required = false) String alreadyExist,
			@RequestParam(required = false) String exactQuery,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("us_id", user.getUs_id());
        param.put("alreadyExist", alreadyExist);
        param.put("exactQuery", exactQuery);
		return ajaxSuccess(lossService.page_product(param));
	}
	
	@RequestMapping(value = "temp_loadproduct", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_loadproduct(HttpSession session,
			@RequestParam(required = true) String pd_code,
			@RequestParam(required = false) String exist,
			@RequestParam(required = false) String dp_code) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put("pd_code", pd_code);
		params.put("exist", exist);
		params.put("dp_code", dp_code);
		params.put("us_id", user.getUs_id());
		params.put(CommonUtil.KEY_SYSSET, getSysSet(session));
		return ajaxSuccess(lossService.temp_loadproduct(params));
	}
	
	@RequestMapping(value = "temp_save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_save(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<T_Stock_LossList> temps = LossVO.convertMap2Model(data, user);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("temps", temps);
		params.put("user", user);
		lossService.temp_save(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_del(@RequestParam Integer lol_id) {
		lossService.temp_del(lol_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateAmount(T_Stock_LossList temp) {
		lossService.temp_updateAmount(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkById(T_Stock_LossList temp) {
		temp.setLol_remark(StringUtil.decodeString(temp.getLol_remark()));
		lossService.temp_updateRemarkById(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_updateRemarkByPdCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_updateRemarkByPdCode(T_Stock_LossList temp,HttpSession session) {
		T_Sys_User user = getUser(session);
		temp.setLol_remark(StringUtil.decodeString(temp.getLol_remark()));
		temp.setLol_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		lossService.temp_updateRemarkByPdCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_delByPiCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_delByPiCode(@RequestParam String pd_code,
			@RequestParam(required = false) String cr_code,
			@RequestParam(required = false) String br_code,
			HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Stock_LossList temp = new T_Stock_LossList();
		temp.setLol_pd_code(pd_code);
		temp.setLol_cr_code(cr_code);
		temp.setLol_br_code(br_code);
		temp.setLol_us_id(user.getUs_id());
		temp.setCompanyid(user.getCompanyid());
		lossService.temp_delByPiCode(temp);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_clear", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_clear(HttpSession session) {
		T_Sys_User user = getUser(session);
		lossService.temp_clear(user.getUs_id(), user.getCompanyid());
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "temp_import", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import(@RequestBody Map<String, Object> data,HttpSession session) {
		T_Sys_User user = getUser(session);
		List<String[]> datas = LossVO.convertMap2Model_import(data);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("datas", datas);
		params.put("user", user);
		lossService.temp_import(params);
		return ajaxSuccess();
	}

	@RequestMapping(value = "temp_import_draft", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object temp_import_draft(@RequestParam String lo_number,HttpSession session) {
		T_Sys_User user = getUser(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("lo_number", lo_number);
		params.put("user", user);
		lossService.temp_import_draft(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Stock_Loss loss,HttpSession session) {
		lossService.save(loss, getUser(session));
		return ajaxSuccess(loss);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Stock_Loss loss,HttpSession session) {
		lossService.update(loss, getUser(session));
		return ajaxSuccess(loss);
	}
	
	@RequestMapping(value = "approve", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object approve(T_Approve_Record record,String number,HttpSession session) {
		record.setAr_describe(StringUtil.decodeString(record.getAr_describe()));
		T_Stock_Loss loss = lossService.approve(number, record, getUser(session));
		return ajaxSuccess(loss);
	}
	
	@RequestMapping(value = "reverse", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reverse(String number,HttpSession session) {
		return ajaxSuccess(lossService.reverse(number, getUser(session)));
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(String number,HttpSession session) {
		lossService.del(number, getCompanyid(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(String number,Integer sp_id,Integer displayMode,HttpSession session) {
		Map<String, Object> resultMap = lossService.loadPrintData(number, sp_id, displayMode, getUser(session));
		resultMap.put("user", getUser(session));
		Map<String, Object> temp = LossVO.buildPrintJson(resultMap, displayMode);
		return ajaxSuccess(temp);
	}
	
}
