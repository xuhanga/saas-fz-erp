package zy.controller.stock;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.stock.useable.T_Stock_Useable;
import zy.entity.sys.set.T_Sys_Set;
import zy.service.stock.useable.UseableService;
import zy.service.sys.set.SetService;
import zy.util.StringUtil;
import zy.vo.stock.UseableVO;

@Controller
@RequestMapping("stock/useable")
public class UseableController extends BaseController{
	@Resource
	private SetService setService;
	@Resource
	private UseableService useableService;
	
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(HttpSession session,Model model) {
		model.addAttribute("set", setService.load(getCompanyid(session)));
		return "stock/useable/update";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(HttpSession session) {
		return ajaxSuccess(useableService.list(getUser(session)));
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(@RequestBody Map<String, Object> data,HttpSession session) {
		List<T_Stock_Useable> datas = UseableVO.convertMap2Model(data, getCompanyid(session));
		T_Sys_Set set = new T_Sys_Set();
		if(StringUtil.isNotEmpty(data.get("st_id"))){
			set.setSt_id(Integer.parseInt(data.get("st_id").toString()));
		}
		set.setSt_useable(Integer.parseInt(data.get("st_useable").toString()));
		set.setCompanyid(getCompanyid(session));
		useableService.update(datas, set);
		return ajaxSuccess(set);
	}
}
