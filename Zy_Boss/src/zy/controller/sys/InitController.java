package zy.controller.sys;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.init.InitService;

@Controller
@RequestMapping(value="sys/init")
public class InitController extends BaseController{
	
	@Resource
	private InitService initService;
	
	@RequestMapping(value = "to_account_enable", method = RequestMethod.GET)
	public String to_list() {
		return "sys/init/account_enable";
	}
	
	@RequestMapping(value = "enableAccount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object enableAccount(HttpSession session) {
		T_Sys_User user = getUser(session);
		initService.enableAccount(user);
		return ajaxSuccess();
	}
	
}
