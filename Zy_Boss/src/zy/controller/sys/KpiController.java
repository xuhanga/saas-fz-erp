package zy.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.sys.kpi.T_Sys_Kpi;
import zy.entity.sys.kpi.T_Sys_KpiScore;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sys.kpi.KpiService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("sys/kpi")
public class KpiController extends BaseController{
	@Resource
	private KpiService kpiService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sys/kpi/list";
	}
	@RequestMapping("to_add")
	public String to_add() {
		return "sys/kpi/add";
	}
	@RequestMapping("to_update")
	public String to_update() {
		return "sys/kpi/update";
	}
	@RequestMapping("to_view")
	public String to_view() {
		return "sys/kpi/view";
	}
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "sys/kpi/list_dialog";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(kpiService.list(params));
	}
	
	@RequestMapping(value = "listScores", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listScores(@RequestParam(required = true) String ki_code,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
        params.put("ki_code", ki_code);
		return ajaxSuccess(kpiService.listScores(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(String ki_code,String kpiscores,HttpSession session) {
		T_Sys_User user = getUser(session);
		kpiscores = StringUtil.decodeString(kpiscores);
		T_Sys_Kpi kpi = new T_Sys_Kpi();
		kpi.setKi_code(ki_code);
		kpi.setCompanyid(user.getCompanyid());
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
        	kpi.setKi_shop_code(user.getUs_shop_code());
		}else{//自营、合伙
			kpi.setKi_shop_code(user.getShop_upcode());
		}
		List<T_Sys_KpiScore> scores = JSON.parseArray(kpiscores,T_Sys_KpiScore.class);
		kpiService.save(kpi, scores);
		return ajaxSuccess(kpi);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(String ki_code,String kpiscores,HttpSession session) {
		T_Sys_User user = getUser(session);
		kpiscores = StringUtil.decodeString(kpiscores);
		T_Sys_Kpi kpi = new T_Sys_Kpi();
		kpi.setKi_code(ki_code);
		kpi.setCompanyid(user.getCompanyid());
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
        	kpi.setKi_shop_code(user.getUs_shop_code());
		}else{//自营、合伙
			kpi.setKi_shop_code(user.getShop_upcode());
		}
		List<T_Sys_KpiScore> scores = JSON.parseArray(kpiscores,T_Sys_KpiScore.class);
		kpiService.update(kpi, scores);
		return ajaxSuccess(kpi);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam String ki_code,HttpSession session) {
		String shop_code = null;
		T_Sys_User user = getUser(session);
		if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
			shop_code = user.getUs_shop_code();
		}else{//自营、合伙
			shop_code = user.getShop_upcode();
		}
		kpiService.del(ki_code, shop_code, user.getCompanyid());
		return ajaxSuccess();
	}
	
	
}
