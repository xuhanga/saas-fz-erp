package zy.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.reward.T_Sys_Reward;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.sys.reward.RewardService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("sys/reward")
public class RewardController extends BaseController{
	@Resource
	private RewardService rewardService;
	
	@RequestMapping(value = "to_reward_menu", method = RequestMethod.GET)
	public String to_reward_menu() {
		return "sys/reward/reward_menu";
	}
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sys/reward/list";
	}
	@RequestMapping(value = "to_add", method = RequestMethod.GET)
	public String to_add() {
		return "sys/reward/add";
	}
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer rw_id,Model model) {
		T_Sys_Reward reward = rewardService.load(rw_id);
		if(null != reward){
			model.addAttribute("reward",reward);
		}
		return "sys/reward/update";
	}
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "sys/reward/list_dialog";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> params = new HashMap<String, Object>();
        params.put(CommonUtil.COMPANYID, getCompanyid(session));
        params.put(CommonUtil.PAGESIZE, pageForm.getRows());
        params.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        params.put(CommonUtil.SIDX, pageForm.getSidx());
        params.put(CommonUtil.SORD, pageForm.getSord());
        params.put("searchContent", pageForm.getSearchContent());
        if(CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())){//总公司、分公司、加盟
        	params.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		}else{//自营、合伙
			params.put(CommonUtil.SHOP_CODE, user.getShop_upcode());
		}
		return ajaxSuccess(rewardService.page(params));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sys_Reward reward,HttpSession session) {
		rewardService.save(reward, getUser(session));
		return ajaxSuccess(reward);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sys_Reward reward,HttpSession session) {
		System.out.println(reward.getRw_code());
		System.out.println(reward.getRw_icon());
		rewardService.update(reward, getUser(session));
		return ajaxSuccess(reward);
	}
	
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer rw_id,HttpSession session) {
		rewardService.del(rw_id);
		return ajaxSuccess();
	}
	
	
}
