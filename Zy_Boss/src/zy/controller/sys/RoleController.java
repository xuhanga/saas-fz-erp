package zy.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.sys.menu.T_Sys_Menu;
import zy.entity.sys.role.T_Sys_Role;
import zy.entity.sys.role.T_Sys_RoleMenu;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.role.RoleService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.vo.sys.MainVO;
import zy.vo.sys.RoleMenuVO;

@Controller
@RequestMapping(value="/sys/role")
public class RoleController extends BaseController{
	@Resource
	private RoleService roleService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sys/role/list";
	}
	@RequestMapping(value = "to_all", method = RequestMethod.GET)
	public String to_all() {
		return "sys/menu/all";
	}
	
	@RequestMapping(value = "to_tree", method = RequestMethod.GET)
	public String to_tree() {
		return "sys/menu/tree";
	}
	@RequestMapping(value = "to_menu", method = RequestMethod.GET)
	public String to_menu() {
		return "sys/menu/list";
	}
	@RequestMapping(value = "to_sin_list_dialog", method = RequestMethod.GET)
	public String to_sin_list_dialog() {
		return "sys/role/sin_list_dialog";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "sys/role/add";
	}
	
	@RequestMapping("to_update")
	public String to_update(@RequestParam Integer ro_id,Model model,HttpSession session) {
		T_Sys_Role role = roleService.queryByID(ro_id);
		model.addAttribute("model", role);
		return "sys/role/update";
	}
	
	@RequestMapping(value = "to_rolemenu_tree/{ro_id}", method = RequestMethod.GET)
	public String to_rolemenu_tree(@PathVariable Integer ro_id,Model model) {
		model.addAttribute("ro_id",ro_id);
		return "sys/role/rolemenu_tree";
	}
	
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session){
		Object name = request.getParameter("searchContent");
		Object type = request.getParameter("shop_type");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", name);
        param.put(CommonUtil.SHOP_TYPE, type);
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		List<T_Sys_Role> list = roleService.list(param);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	
	@RequestMapping(value = "tree", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object tree(HttpServletRequest request,HttpSession session){
		Object type = request.getParameter("shop_type");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, type);
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		List<T_Sys_Role> list = roleService.list(param);
		String tree = MainVO.buildRoleTree(list);
		return ajaxSuccess(tree);
	}
	@RequestMapping(value = "listMenu", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listMenu(HttpServletRequest request,HttpSession session){
		Object ro_code = request.getParameter("ro_code");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("ro_code", ro_code);
		List<T_Sys_Menu> list = roleService.listMenu(param);
		return ajaxSuccess(list);
	}
	
	@RequestMapping(value = "dia_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object dia_list(HttpServletRequest request,HttpSession session){
		Object name = request.getParameter("searchContent");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>(2);
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put("name", name);
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		List<T_Sys_Role> sizeList = roleService.dia_list(param);
		return ajaxSuccess(sizeList, "查询成功, 共" + sizeList.size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sys_Role model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 model.setRo_date(DateUtil.getYearMonthDate());
		 model.setRo_shop_code(user.getUs_shop_code());
		 Integer id = roleService.queryByName(model);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 roleService.save(model);
			 return ajaxSuccess(model);
		 }
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sys_Role model,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		model.setCompanyid(user.getCompanyid());
		Integer id = roleService.queryByName(model);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 roleService.update(model);
			 return ajaxSuccess(model);
		 }
	}
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer ro_id,HttpServletRequest request,HttpSession session) {
		roleService.del(ro_id);
		 return ajaxSuccess();
	}
	
	
	@RequestMapping(value = "updateMenu",method = RequestMethod.POST)
	public @ResponseBody Object updateMenu(T_Sys_Menu menu,
			HttpServletRequest request,HttpSession session){
		roleService.updateMenu(menu);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateMenus",method = RequestMethod.POST)
	public @ResponseBody Object updateMenus(
			HttpServletRequest request,HttpSession session){
		String data = request.getParameter("data");
		List<T_Sys_Menu> menuList = MainVO.buildRoleMenu(data);
		roleService.updateMenus(menuList);
		return ajaxSuccess();
	}

	@RequestMapping(value = "rolemenu_tree", method = RequestMethod.POST)
	public @ResponseBody Object rolemenu_tree(@RequestParam(required = true) Integer ro_id,HttpSession session) {
		T_Sys_User user = getUser(session);
		T_Sys_Role role = roleService.queryByID(ro_id);
		List<T_Sys_Menu> allMenu = roleService.listAllMenu(String.valueOf(user.getCo_ver()),role.getRo_shop_type());
		List<T_Sys_RoleMenu> existMenu = roleService.listRoleMenu(role.getRo_code(), user.getCompanyid());
		return ajaxSuccess(RoleMenuVO.buildMenuTree(allMenu,existMenu));
	}
	
	@RequestMapping(value = "rolemenu_save",method = RequestMethod.POST)
	public @ResponseBody Object rolemenu_save(@RequestParam(required = true) String ro_code,
			@RequestParam(required = true) String menus,
			HttpServletRequest request,HttpSession session){
		List<T_Sys_RoleMenu> roleMenus = JSON.parseArray(menus,T_Sys_RoleMenu.class);
		roleService.rolemenu_save(roleMenus, ro_code, getCompanyid(session));
		return ajaxSuccess();
	}
	
}
