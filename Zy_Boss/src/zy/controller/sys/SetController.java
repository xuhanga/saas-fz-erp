package zy.controller.sys;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.set.T_Sys_Set;
import zy.service.sys.set.SetService;

@Controller
@RequestMapping("sys/set")
public class SetController extends BaseController{
	
	@Resource
	private SetService setService;
	
	@RequestMapping(value = "to_update", method = RequestMethod.GET)
	public String to_update(HttpSession session,Model model) {
		model.addAttribute("set", setService.load(getCompanyid(session)));
		return "sys/set/update";
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sys_Set set,HttpSession session) {
		set.setCompanyid(getCompanyid(session));
		setService.update(set);
		return ajaxSuccess(set);
	}
}
