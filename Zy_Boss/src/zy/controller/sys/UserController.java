package zy.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sys.user.T_Sys_User;
import zy.service.sys.user.UserService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;

@Controller
@RequestMapping(value="/sys/user")
public class UserController extends BaseController{
	@Resource
	private UserService userService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "sys/user/list";
	}
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "sys/user/list_dialog";
	}
	
	@RequestMapping("to_add")
	public String to_add() {
		return "sys/user/add";
	}
	
	@RequestMapping("to_update")
	public String to_update(@RequestParam Integer us_id,Model model,HttpSession session) {
		T_Sys_User role = userService.queryByID(us_id);
		model.addAttribute("model", role);
		return "sys/user/update";
	}
	
	@RequestMapping(value = "/to_change_password", method = RequestMethod.GET)
	public String to_change_password() {
		return "sys/user/change_password";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(HttpServletRequest request,HttpSession session){
		Object name = request.getParameter("searchContent");
		Object ty_id = request.getParameter("shop_type");
		Object sp_code = request.getParameter("sp_code");
		Object ro_code = request.getParameter("ro_code");
       
        String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShoptype());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put("name", name);
        param.put("ty_id", ty_id);
        param.put("sp_code", sp_code);
        param.put("ro_code", ro_code);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Sys_User> pageData = userService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sys_User model,HttpServletRequest request,HttpSession session) {
		 T_Sys_User user = getUser(session);
		 model.setCompanyid(user.getCompanyid());
		 model.setUs_end(DateUtil.getDateAddDays(365));
		 model.setUs_pay(0);
		 model.setUs_default(0);
		 model.setUs_pass(MD5.encryptMd5(model.getUs_pass()));
		 model.setUs_date(DateUtil.getCurrentTime());
		 Integer id = userService.queryByName(model);//验证帐号是否存在
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 userService.save(model);
			 return ajaxSuccess(model);
		 }
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sys_User model,HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
		model.setCompanyid(user.getCompanyid());
		Integer id = userService.queryByName(model);
		 if(null != id && id > 0){
			return result(EXISTED);
		 }else{
			 userService.update(model);
			 return ajaxSuccess(model);
		 }
	}
	@RequestMapping(value = "del", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@RequestParam Integer us_id,HttpServletRequest request,HttpSession session) {
		userService.del(us_id);
		 return ajaxSuccess();
	}
	@RequestMapping(value = "initPwd", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object initPwd(@RequestParam Integer us_id,HttpServletRequest request,HttpSession session) {
		userService.initPwd(us_id);
		 return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateState", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateState(T_Sys_User model,HttpServletRequest request,HttpSession session) {
		userService.updateState(model);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "changePassword", method = {RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object changePassword(HttpSession session, String old_pwd,String us_pwd) {
		T_Sys_User user = getUser(session);
		userService.changePassword(user.getUs_id(), old_pwd, us_pwd);
		return ajaxSuccess("更改成功");
	}
}
