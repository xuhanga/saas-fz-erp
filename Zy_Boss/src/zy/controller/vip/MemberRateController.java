package zy.controller.vip;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.vip.rate.T_Vip_Brand_Rate;
import zy.entity.vip.rate.T_Vip_Type_Rate;
import zy.form.PageForm;
import zy.service.vip.rate.MemberRateService;
import zy.util.CommonUtil;
@Controller
@RequestMapping("vip/rate")
public class MemberRateController extends BaseController {
	@Resource
	private MemberRateService memberRateService;
	
	@RequestMapping(value = "/to_list_brand", method = RequestMethod.GET)
	public String to_list_brand() {
		return "vip/rate/brand_list";
	}
	
	@RequestMapping(value = "/to_list_type", method = RequestMethod.GET)
	public String to_list_type() {
		return "vip/rate/type_list";
	}
	
	@RequestMapping(value = "/to_add_brand", method = RequestMethod.GET)
	public String to_add_brand() {
		return "vip/rate/brand_add";
	}
	
	@RequestMapping(value = "/to_add_type", method = RequestMethod.GET)
	public String to_add_type() {
		return "vip/rate/type_add";
	}
	
	@RequestMapping(value = "/to_update_type", method = RequestMethod.GET)
	public String to_update_type(Integer tr_id,Model model) {
		 T_Vip_Type_Rate type_rate = memberRateService.typeByID(tr_id);
		 model.addAttribute("typeRate", type_rate);
		return "vip/rate/type_update";
	}
	
	@RequestMapping(value = "/to_update_brand", method = RequestMethod.GET)
	public String to_update_brand(Integer br_id,Model model) {
		T_Vip_Brand_Rate brand_rate = memberRateService.brandByID(br_id);
		model.addAttribute("brandRate", brand_rate);
		return "vip/rate/brand_update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String mt_code,
			@RequestParam(required = false) String br_bd_code,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("mt_code", mt_code);
        param.put("br_bd_code", br_bd_code);
		return ajaxSuccess(memberRateService.page(param));
	}
	
	@RequestMapping(value = "pageType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageType(PageForm pageForm,
			@RequestParam(required = false) String mt_code,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("mt_code", mt_code);
		return ajaxSuccess(memberRateService.pageType(param));
	}
	
	@RequestMapping(value = "saveBrand", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveBrand(HttpServletRequest request,HttpSession session) {
		String bd_code = request.getParameter("bd_code");
		String data = request.getParameter("data");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("bd_code", bd_code);
		param.put("data", data);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		memberRateService.saveBrand(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "saveType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveType(HttpServletRequest request,HttpSession session) {
		String tp_code = request.getParameter("tp_code");
		String data = request.getParameter("data");
		T_Sys_User user = getUser(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("tp_code", tp_code);
		param.put("data", data);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getUs_shop_code());
		memberRateService.saveType(param);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "delBrand", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delBrand(HttpServletRequest request,HttpSession session) {
		String br_id = request.getParameter("br_id");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("br_id", br_id);
		memberRateService.delBrand(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "delType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object delType(HttpServletRequest request,HttpSession session) {
		String tr_id = request.getParameter("tr_id");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("tr_id", tr_id);
		memberRateService.delType(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "updateBrand", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateBrand(Integer br_id,Double br_rate) {
		Map<String,Object> param = new HashMap<String, Object>(2);
		param.put("br_id", br_id);
		param.put("br_rate", br_rate);
		memberRateService.updateBrand(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "updateType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateType(Integer tr_id,Double tr_rate) {
		Map<String,Object> param = new HashMap<String, Object>(2);
		param.put("tr_id", tr_id);
		param.put("tr_rate", tr_rate);
		memberRateService.updateType(param);
		return ajaxSuccess();
	}
}
