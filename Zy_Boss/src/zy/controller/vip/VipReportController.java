package zy.controller.vip;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.vip.set.T_Vip_Setup;
import zy.form.PageForm;
import zy.service.vip.report.VipReportService;
import zy.service.vip.set.VipSetService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("vip/report")
public class VipReportController extends BaseController{
	@Resource
	private VipReportService vipReportService;
	@Resource
	private VipSetService vipSetService;
	
	@RequestMapping(value = "to_point_list", method = RequestMethod.GET)
	public String to_point_list() {
		return "vip/report/point_list";
	}
	
	@RequestMapping(value = "to_type_analysis_list", method = RequestMethod.GET)
	public String to_type_analysis_list() {
		return "vip/report/type_analysis_list";
	}
	
	@RequestMapping(value = "to_consume_detail_list", method = RequestMethod.GET)
	public String to_consume_detail_list() {
		return "vip/report/consume_detail_list";
	}
	
	@RequestMapping(value = "to_activevip_analysis_list", method = RequestMethod.GET)
	public String to_activevip_analysis_list() {
		return "vip/report/activevip_analysis_list";
	}
	
	@RequestMapping(value = "to_noshopvip_analysis_list", method = RequestMethod.GET)
	public String to_noshopvip_analysis_list() {
		return "vip/report/noshopvip_analysis_list";
	}
	
	@RequestMapping(value = "to_brand_analysis_list", method = RequestMethod.GET)
	public String to_brand_analysis_list() {
		return "vip/report/brand_analysis_list";
	}
	
	@RequestMapping(value = "to_vipconsume_month_compare", method = RequestMethod.GET)
	public String to_vipconsume_month_compare() {
		return "vip/report/vipconsume_month_compare";
	}
	
	@RequestMapping(value = "to_vipage_analysis_list", method = RequestMethod.GET)
	public String to_vipage_analysis_list() {
		return "vip/report/vipage_analysis_list";
	}
	
	@RequestMapping(value = "to_vipquota_analysis_list", method = RequestMethod.GET)
	public String to_vipquota_analysis_list(HttpServletRequest request,HttpSession session,Model model) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getUser(session));
		int lossDay = 180;//会员流失天数
		int consumeDay = 90;//会员默认消费周期
		T_Vip_Setup t_Vip_Setup = vipSetService.loadSetUp(param);
		if(t_Vip_Setup!=null){
			lossDay = t_Vip_Setup.getVs_loss_day();
			consumeDay = t_Vip_Setup.getVs_consume_day();
		}
		Calendar cl = Calendar.getInstance();
		cl.add(Calendar.DATE, -lossDay);
		
		Calendar c2 = Calendar.getInstance();
		c2.add(Calendar.DATE, -consumeDay);
		
		model.addAttribute("begindate", DateUtil.getYearMonthDate(cl.getTime()));
		model.addAttribute("enddate", DateUtil.getYearMonthDate());
		model.addAttribute("lossDay", lossDay);//会员流失天数
		model.addAttribute("consumeDay", consumeDay);
		model.addAttribute("consumeDayDate", DateUtil.getYearMonthDate(c2.getTime()));
		return "vip/report/vipquota_analysis_list";
	}
	
	@RequestMapping(value = "point_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object point_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_mobile,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_cardcode", StringUtil.decodeString(vm_cardcode));
        param.put("vm_mobile", StringUtil.decodeString(vm_mobile));
		return ajaxSuccess(vipReportService.point_list(param));
	}
	
	@RequestMapping(value = "type_analysis_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object type_analysis_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
		return ajaxSuccess(vipReportService.type_analysis_list(param));
	}
	
	@RequestMapping(value = "consume_detail_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object consume_detail_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_mobile,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_cardcode", vm_cardcode);
        param.put("vm_mobile", vm_mobile);
		return ajaxSuccess(vipReportService.consume_detail_list(param));
	}
	
	@RequestMapping(value = "activevip_analysis_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object activevip_analysis_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) Integer number,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
        param.put("number", number);
		return ajaxSuccess(vipReportService.activevip_analysis_list(param));
	}
	
	@RequestMapping(value = "noshopvip_analysis_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object noshopvip_analysis_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_mt_code,
			@RequestParam(required = false) String vm_manager_code,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_mt_code", vm_mt_code);
        param.put("vm_manager_code", vm_manager_code);
		return ajaxSuccess(vipReportService.noshopvip_analysis_list(param));
	}
	
	@RequestMapping(value = "member_lose_data", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object member_lose_data(HttpServletRequest request,HttpSession session,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) Integer vs_loss_day,
			@RequestParam(required = false) String consumeDayDate,
			@RequestParam(required = false) Integer consumeDay) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getUser(session));
		param.put("begindate", begindate);
		param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
		param.put("vs_loss_day", vs_loss_day);
		param.put("consumeDayDate", consumeDayDate);
		param.put("consumeDay", consumeDay);
		
		return ajaxSuccess(vipReportService.member_lose_data(param));
	}
	
	@RequestMapping(value = "vipquota_analysis_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object vipquota_analysis_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) Integer vs_loss_day,
			@RequestParam(required = false) String consumeDayDate,
			@RequestParam(required = false) Integer consumeDay,
			@RequestParam(required = false) String query_type,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vs_loss_day", vs_loss_day);
        param.put("consumeDayDate", consumeDayDate);
        param.put("consumeDay", consumeDay);
        param.put("query_type", query_type);
		return ajaxSuccess(vipReportService.vipquota_analysis_list(param));
	}
	
	@RequestMapping(value = "brand_analysis_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object brand_analysis_list(PageForm pageForm,
			@RequestParam(required = false) String begindate,
			@RequestParam(required = false) String enddate,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_cardcode,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_cardcode", vm_cardcode);
		return ajaxSuccess(vipReportService.brand_analysis_list(param));
	}
	
	@RequestMapping(value = "vipconsume_month_compare", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object vipconsume_month_compare(PageForm pageForm,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_year,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_year", vm_year);
		return ajaxSuccess(vipReportService.vipconsume_month_compare(param));
	}
	
	@RequestMapping(value = "vipconsume_month_compare_column", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object vipconsume_month_compare_column(PageForm pageForm,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_year,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_year", vm_year);
		return ajaxSuccess(vipReportService.vipconsume_month_compare_column(param));
	}
	
	@RequestMapping(value = "vipage_analysis_column", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object vipage_analysis_column(PageForm pageForm,
			@RequestParam(required = false) String vm_mt_code,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_manager_code,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getUser(session));
        param.put("vm_mt_code", vm_mt_code);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_manager_code", vm_manager_code);
		return ajaxSuccess(vipReportService.vipage_analysis_column(param));
	}
	
	@RequestMapping(value = "vipage_analysis_list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object vipage_analysis_list(PageForm pageForm,
			@RequestParam(required = false) String vm_mt_code,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_manager_code,
			@RequestParam(required = false) Integer ags_beg_age,
			@RequestParam(required = false) Integer ags_end_age,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        bulidParam(param, getUser(session));
        param.put("vm_mt_code", vm_mt_code);
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_manager_code", vm_manager_code);
        param.put("ags_beg_age", ags_beg_age);
        param.put("ags_end_age", ags_end_age);
		return ajaxSuccess(vipReportService.vipage_analysis_list(param));
	}
}
