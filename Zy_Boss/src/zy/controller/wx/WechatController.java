package zy.controller.wx;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.entity.wx.wechat.T_Wx_WechatInfo;
import zy.entity.wx.wechat.T_Wx_WechatSet;
import zy.service.wx.wechat.WechatInfoService;
import zy.service.wx.wechat.WechatSetService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.util.security.ThreeDESUtil;

@Controller
@RequestMapping("wx/wechat")
public class WechatController extends BaseController{
	@Resource
	private WechatInfoService wechatInfoService;
	@Resource
	private WechatSetService wechatSetService;
	
	@RequestMapping("to_wechat")
	public String to_wechat() {
		return "wx/wechat/wechat";
	}
	@RequestMapping("to_wechat_set")
	public String to_wechat_set(Model model,HttpSession session) {
		T_Sys_User user = getUser(session);
		if (CommonUtil.ONE.equals(user.getShoptype())
				|| CommonUtil.TWO.equals(user.getShoptype())
				|| CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
			model.addAttribute("wechatInfo",wechatInfoService.load(user.getUs_shop_code(), user.getCompanyid()));
			model.addAttribute("wechatSet",wechatSetService.load(user.getUs_shop_code(), user.getCompanyid()));
		}else{//自营、合伙
			model.addAttribute("wechatInfo",wechatInfoService.load(user.getShop_upcode(), user.getCompanyid()));
			model.addAttribute("wechatSet",wechatSetService.load(user.getShop_upcode(), user.getCompanyid()));
		}
		return "wx/wechat/wechat_set";
	}
	
	@RequestMapping(value = "updateWechatInfo", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	@ResponseBody
	public String updateWechatInfo(T_Wx_WechatInfo wechatInfo, MultipartFile mchCer,HttpSession session) throws IOException, Exception {
		T_Sys_User user = getUser(session);
		if(StringUtil.isNotEmpty(wechatInfo.getWx_mch_id())){
			wechatInfo.setWx_mch_id(ThreeDESUtil.des3EncodeCBC(wechatInfo.getWx_mch_id().getBytes("UTF-8")));
		}
		if (StringUtil.isNotEmpty(wechatInfo.getWx_mch_key()) && wechatInfo.getWx_mch_key().indexOf("*") == -1) {
			wechatInfo.setWx_mch_key(ThreeDESUtil.des3EncodeCBC(wechatInfo.getWx_mch_key().getBytes("UTF-8")));
		}
		if(StringUtil.isNotEmpty(wechatInfo.getWx_mch_pwd())){
			wechatInfo.setWx_mch_pwd(ThreeDESUtil.des3EncodeCBC(wechatInfo.getWx_mch_pwd().getBytes("UTF-8")));
		}
		if(mchCer != null && StringUtil.isNotEmpty(mchCer.getOriginalFilename())){
			String shop_code;
			if (CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
	        	shop_code = user.getUs_shop_code();
			}else{//自营、合伙
				shop_code = user.getShop_upcode();
			}
			String uploadFileName = mchCer.getOriginalFilename();
			String suffix = uploadFileName.substring(uploadFileName.lastIndexOf("."));
			String saveFileName = CommonUtil.WX_CER_PATH + "/" + user.getCo_code()+"_"+shop_code + suffix;
			File savefile = new File(CommonUtil.CHECK_BASE, saveFileName);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			mchCer.transferTo(savefile);
			wechatInfo.setWx_mch_cer_path(saveFileName);
		}
		wechatInfoService.update(wechatInfo, getUser(session));
		return JSON.toJSONString(ajaxSuccess(wechatInfo));
	}
	
	@RequestMapping(value = "updateWechatSet", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	@ResponseBody
	public String updateWechatSet(T_Wx_WechatSet wechatSet, MultipartFile wx_bg,HttpSession session) throws IOException, Exception {
		T_Sys_User user = getUser(session);
		if(wx_bg != null && StringUtil.isNotEmpty(wx_bg.getOriginalFilename())){
			String shop_code;
			if (CommonUtil.ONE.equals(user.getShoptype()) || CommonUtil.TWO.equals(user.getShoptype()) || CommonUtil.FOUR.equals(user.getShoptype())) {// 总公司、分公司、加盟
	        	shop_code = user.getUs_shop_code();
			}else{//自营、合伙
				shop_code = user.getShop_upcode();
			}
			String uploadFileName = wx_bg.getOriginalFilename();
			String suffix = uploadFileName.substring(uploadFileName.lastIndexOf("."));
			String saveFileName = CommonUtil.WX_BG_PATH + "/bg_" + user.getCo_code()+"_"+shop_code + suffix;
			File savefile = new File(CommonUtil.CHECK_BASE, saveFileName);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			wx_bg.transferTo(savefile);
			wechatSet.setWs_bgimg_path(saveFileName);
		}
		wechatSetService.update(wechatSet, getUser(session));
		return JSON.toJSONString(ajaxSuccess(wechatSet));
	}
	
	
}
