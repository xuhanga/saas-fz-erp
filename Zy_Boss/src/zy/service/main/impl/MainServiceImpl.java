package zy.service.main.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zy.dao.sys.MainDAO;
import zy.service.main.MainService;
@Service
public class MainServiceImpl implements MainService{
	@Resource
	private MainDAO mainDAO;
	@Override
	public void login(Map<String, Object> paramMap) {
		mainDAO.login(paramMap);
	}

}
