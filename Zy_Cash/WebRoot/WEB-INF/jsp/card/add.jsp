<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px"><b>*</b>储值卡号：</td>
					<td width="130px">
						<input class="main_Input w146" type="text" name="cd_cardcode" id="cd_cardcode" value=""/>
					</td>
					<td align="right" width="80px"><b>*</b>发放日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate w146"  name="cd_grantdate" id="cd_grantdate" onclick="WdatePicker()" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" ><b>*</b>输入密码：</td>
					<td>
						<input class="main_Input w146" type="password" name="cd_pass" id="cd_pass" value=""/>
					</td>
					<td align="right"><b>*</b>确认密码：</td>
					<td>
						<input class="main_Input w146" type="password" name="cd_repass" id="cd_repass" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">有效日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate w146"  name="cd_enddate" id="cd_enddate" onclick="WdatePicker()" value=""/>
					</td>
					<td align="right">发卡店铺：</td>
					<td>
						<input class="main_Input w146" type="text" readonly="readonly" name="shop_name" id="shop_name" value=""/>
						<input type="hidden" name="cd_shop_code" id="cd_shop_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>发卡金额：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_money" id="cd_money" value="0"/>
					</td>
					<td align="right">经办人员：</td>
					<td>
						<input class="main_Input w146" type="text" readonly="readonly" name="cd_manager" id="cd_manager" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>实收现金：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_realcash" id="cd_realcash" value="0"/>
					</td>
					<td align="right"><b>*</b>实收刷卡：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_bankmoney" id="cd_bankmoney" value="0"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>现金账户：</td>
					<td>
						<span class="ui-combo-wrap" id="spanBank" ></span>
						<input type="hidden" name="cdl_ba_code" id="cdl_ba_code" value=""/>
					</td>
					<td align="right"><b>*</b>刷卡账户：</td>
					<td>
						<span class="ui-combo-wrap" id="spanBanks" ></span>
						<input type="hidden" name="cdl_bank_code" id="cdl_bank_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">用户姓名：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_name" id="cd_name" value=""/>
					</td>
					<td align="right">手机号码：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_mobile" id="cd_mobile" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">身份证号：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_idcard" id="cd_idcard" value=""/>
					</td>
					<td align="right">备注：</td>
					<td>
						<input class="main_Input w146" type="text" name="cd_remark" id="cd_remark" value=""/>
					</td>
				</tr>
				<tr class="list last">
					<td align="right"></td>
					<td id="errorTip" class="errorTip" colspan="3"></td>
				</tr>
			</table>
</div>
<div class="footdiv">
	<a id="ischeck" class="fl">
		<label class="chk">
			<input type="checkbox"/>打印
		</label>
	</a>
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script src="<%=printPath%>/CLodopfuncs.js"></script>
<script src="<%=basePath%>data/card/card_add.js"></script>
<script language="javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
</script>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				cd_cardcode : {
					required : true
				},
				cd_pass : {
					rangelength:[6,16]
				},
				cd_repass : {
					equalTo:"#cd_pass"
				},
				cd_money : {
					required : true
				},
				cd_realcash : {
					required : true
				},
				cd_bankmoney : {
					required : true
				},
				cdl_ba_code : {
					required : true
				},
				cdl_bank_code : {
					required : true
				},
				cd_mobile : {
					isMobile : true
				}
			},
			messages : {
				cd_cardcode : "请输入卡号",
				cd_pass : "密码长度必须在6到16之间",
				cd_repass : "两次密码输入不一致",
				cd_money : "请输入金额",
				cd_realcash : "请输入实收现金",
				cd_bankmoney : "请输入实收刷卡",
				cdl_ba_code : "请选择现金账户",
				cdl_bank_code : "请选择刷卡账户",
				cd_mobile : "请输入正确的手机号码"
			}
		});
	});
</script>
</body>
</html>