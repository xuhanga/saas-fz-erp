<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form name="form1" method="post" action="" id="form1">
<input type="hidden" id="hand_type" name="hand_type" value=""/>
<div class="shop_box">
	<table class="table_add" width="100%">
		<tr>
			<td align="left">
				<input name="hand_radio" id="rate_radio" type="radio" value="1" onclick="javascript:$('#hand_type').val(1);$('#rate_value').focus();handle.clear();"/>&nbsp;输入折扣：
			</td>
			<td align="left">
				<input class="main_Input w146" onkeyup="javascript:vadiUtil.vadiDouble(this);" onfocus="javascript:handle.valFocus(1);" type="text" name="rate_value" id="rate_value" maxlength="10" value=""/>
			</td>
		</tr>
		<tr>
			<td align="left" >
 				<input name="hand_radio" id="plus_radio" type="radio" value="2" onclick="javascript:$('#hand_type').val(2);$('#plus_value').focus();handle.clear();"/>&nbsp;让利金额：
			</td>
			<td align="left">
				<input class="main_Input w146" onkeyup="javascript:vadiUtil.vadiNumber(this);" onfocus="javascript:handle.valFocus(2);" type="text" name="plus_value" id="plus_value" value="" />
			</td>
		</tr>
		<tr>
			<td align="left" >
				<input name="hand_radio" id="real_radio" type="radio" value="3" onclick="javascript:$('#hand_type').val(3);$('#real_value').focus();handle.clear();"/>&nbsp;实际金额：
			</td>
			<td align="left">
				<input class="main_Input w146" onkeyup="javascript:vadiUtil.vadiNumber(this);" onfocus="javascript:handle.valFocus(3);" type="text" name="real_value" id="real_value" value="" />
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2">
				零售金额：<b id="sell_money"></b>
			</td>
		</tr>
	</table>
</div>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script src="<%=basePath%>data/cash/all_rate.js"></script>
</body>
</html>