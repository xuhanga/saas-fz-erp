<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
</script>
<style>
</style>
</head>
<body>
<form name="form1" method="post" action="" id="form1" >
<div class="wrapper">
	<div class="warn_info"><b style="color:red;">注：收银金额小于0时，储值卡输入负数.回车（Enter）加入使用列表</b></div>
	<div class="mod-search cf" style="margin-left:10px;margin-top:5px;">
	    <div class="fl" style="width:100%">
	      	<ul class="ul-inline">
		        <li>卡号：
					<input type="text" class="main_Input w158" id="cd_cardcode" name="cd_cardcode" value="" maxlength="18" onkeyup="if(event.keyCode == 13){$('#cd_pass').focus();}"/>
					<input type="text" style="display:none;"/>
					<input type="hidden" id="cd_id" name="cd_id" value=""/>
					<input type="hidden" id="used_code" name="used_code" value=""/>
					<input type="hidden" id="cd_cashrate" name="cd_cashrate" value=""/>
		        </li>
		        <li>
		        	&nbsp;密码：
					<input type="password" class="main_Input w158" id="cd_pass" name="cd_pass" value="" maxlength="18"/>
					<input type="text" style="display:none;"/>
		        </li>
	      	</ul>
			<ul class="ul-inline" style="float:left;margin-top:2px;">
	      		<li>余额：
					<input type="text" class="main_Input w158" id="cd_money" name="cd_money" value="" readonly="readonly"/>
		        </li>
		        <li>&nbsp;使用：
					<input type="text" class="main_Input w158" id="cd_usemoney" name="cd_usemoney" value="" maxlength="6"/>
					<input type="text" style="display:none;"/>
		        </li>
	     	</ul>
		</div>
	</div>
	<div class="grid-wrap" >
		<table id="grid">
		</table>
		<div id="page"></div>
	</div>
</div>
</form>
<script type="text/javascript" src="<%=basePath%>data/cash/card.js?v=${time}"></script>
</body>
</html>