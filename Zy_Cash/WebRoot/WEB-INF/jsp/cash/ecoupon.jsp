<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
</script>
<style>
</style>
</head>
<body>
<form name="form1" method="post" action="" id="form1" >
<div class="wrapper">
	<div class="mod-search cf" style="margin-left:5px;margin-top:5px;">
	    <div class="fl" style="width:100%">
	      	<ul class="ul-inline">
		        <li>手机：
					<input type="text" class="main_Input w120" id="ecu_tel" name="ecu_tel" value="" maxlength="18"/>
					<input type="text" style="display:none;"/>
		        </li>
		        <li>&nbsp;
	        		<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
	        		<span>注：使用前请查看短信或微信，输入6位验证码</span>
		        </li>
	     	</ul>
		</div>
	</div>
	<div class="grid-wrap" >
		<table id="grid">
		</table>
		<div id="page"></div>
	</div>
</div>
</form>
<script type="text/javascript" src="<%=basePath%>data/cash/ecoupon.js"></script>
</body>
</html>