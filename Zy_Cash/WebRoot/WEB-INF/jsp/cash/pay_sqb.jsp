<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<style>
.ui-input-ph{width:250px;}
.tabnav{height:40px;position:absolute;top:0;right:10px;left:10px;}
.tabnav li{display:inline;float:left;height:30px;margin:10px 10px 0 0;cursor:pointer;line-height:30px;text-align:center;width:80px;}
.tabnav li.select{background:#38d;color:#fff;font-weight:700;}
.contbox{background:#fff;border-top:#ddd solid 1px;position:absolute;top:40px;bottom:0;right:0;left:0;}
</style>
</head>
<body>
<form name="form1" method="post" action="" id="form1" >
<div class="tabnav">
	<ul>
		<li id="pay-menu" class="pay-tab select" onclick="UTIL.tab(this)">付款中</li>
		<li id="pay-query" class="pay-tab menu" onclick="UTIL.tab(this)">付款查询</li>
	</ul>
</div>
<div id="pay-menu-div" class="contbox">
	<div class="con">
		<ul style="margin: 5px;padding:5px;">
			<li style="margin:5px;">
				订单编号：
				<input type="text" class="main_Input" id="pt_number" name="pt_number" value="自动生成" readonly="readonly"/>
			</li>
			<li style="margin:5px;">
				付款金额：
				<input type="text" class="main_Input" id="pt_money" name="pt_money" value="" />
			</li>
			<li style="margin:5px;">
				付款条码：
				<input type="text" class="main_Input" id="pt_paycode" name="pt_paycode" value="" maxlength="32"/>
				<input type="text" style="display:none;"/>
			</li>
		</ul>
	</div>
</div>
<div id="pay-query-div" class="contbox" style="display:none;">
	<!-- <div  style="margin-top:10px;margin-right: 8px;">
      <ul>
	        <li class="fl">
		        <span class="ui-combo-wrap" id="span_pay_result"></span>
				<input type="hidden"  name="pay_state" id="pay_state" value=""/>
				&nbsp;<a class="ui-btn mrb" id="btn_search" >查询</a> 
	        </li>
      </ul>
    </div> -->
	<div class="grid-wrap fl" style="margin-top:8px;">
		<table id="grid">
		</table>
		<div id="page"></div>
	</div>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn_ok" name="btn_ok" type="button" value="确认支付"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
document.write("<scr"+"ipt src=\"<%=basePath%>data/cash/pay_sqb.js\"></sc"+"ript>");
</script>
</body>
</html>