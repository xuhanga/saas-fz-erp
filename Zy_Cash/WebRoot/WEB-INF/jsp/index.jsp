<%@ page contentType="text/html; charset=utf-8" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>智慧数收银平台</title>
<link href="<%=basePath%>resources/css/index.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
var basepath = '${static_path}';
document.write("<scr"+"ipt src=\""+basepath+"/resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\""+basepath+"/resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\""+basepath+"/resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
</script>
<script type="text/javascript">
$(function(){
	if($.trim($("#co_code").val()) != ""){
		if($.trim($("#us_account").val()) != ""){
			$("#us_pass").select().focus();
		}else{
			$("#us_account").focus();
		}
	}else{
		$("#co_code").focus();	
	}
	$("#co_code").on("keyup",function(event){
		if(event.keyCode == 13){
			$("#us_account").select().focus();
		}
	});
	$("#us_account").on("keyup",function(event){
		if(event.keyCode == 13){
			$("#us_pass").select().focus();
		}
	});
	$("#us_pass").on("keyup",function(event){
		if(event.keyCode == 13){
			doLogin();
		}
	});
});
function doLogin(){
	var us_pass=$.trim($("#us_pass").val());
	var co_code=$.trim($("#co_code").val());
	var us_account=$.trim($("#us_account").val());
	if(co_code == ''){//后台密码
		$.dialog.tips("请输入商家编号!",1,"32X32/hits.png");
		$("#co_code").focus();
		return;
	}
	if(us_account == ''){
		$.dialog.tips("请输入用户编号!",1,"32X32/hits.png");
		$("#us_account").focus();
		return;
	}
	if(us_pass == ''){//后台密码
		$.dialog.tips("请输入商家密码!",1,"32X32/hits.png");
		$("#us_pass").focus();
		return;
	}
	var url = basepath+"login";
	$.dialog.tips("正在登录中...",5,"loading.gif");
	$.ajax({
		type:"POST",
		data:{"us_account":us_account,"us_pass":us_pass,"co_code":co_code},
		url:url,
		cache:false,
		dataType:"json",
		success:function(data){
			if(undefined != data && data.stat == 200){
				if(undefined != data.data.st_code && "" != data.data.st_code){
					window.location.replace(basepath+"main");
				}else{
					handle.doShift();
				}
			}else{
				$.dialog.tips("帐户或密码有误!",1,"32X32/hits.png");
			}
		}
	 });
}
var handle = {
	doShift:function(){
		shiftData = $.dialog({
			id:"OnePageID",
			title : '选择班次',
			content : 'url:'+basepath+'to_shift',
			data:{callback:this.callback},
			width : 395,
			height : 380,
			max : false,
			min : false,
			cache : false,
			lock: false,
			ok:false
		});
	},
	saveDayEnd:function(pdata){
		$.ajax({
			type:"POST",
			data:pdata,
			url:basepath+"dayend/save",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					window.location.replace(basepath+"main");
				}else{
					$.dialog.tips("登录有误!",1,"32X32/hits.png");
				}
			}
		 });
	},
	callback: function(data){
		if(data){
			handle.saveDayEnd(data);
		}
	}
};
function doRemember(obj){
	if(obj.checked){
		$("#remember").val(1);
	}else{
		$("#remember").val(0);
	}
}
</script>
</head>
<body>
	<div class="big-bg">
    	<div class="login">
        	<div class="login-n">
        		<span class="login_title">
        			<h3>收银平台</h3>
        			<b></b>
        		</span>
        		<div>
                	<label>编号：</label><input class="login-user" type="text" id="co_code" name="co_code" value=""/>
                </div>
                <div>
                	<label>帐号：</label><input class="login-user" type="text" id="us_account" name="us_account" value=""/>
                </div>
                <div>
                	<label>密码：</label><input class="login-user" type="password" id="us_pass" name="us_pass" value=""/>
                </div>
                <div style="margin-left: 35px;">
                	<input id="remember" type="hidden" name="remember" value="0"/>
					<input type="checkbox" onclick="javascript:doRemember(this);"/> 记住帐号
                </div>
                <p>
                	<input type="button" class="btn_submit" onclick="javascript:doLogin();" value="登 录" /> 
                </p>
            </div>        	
        </div>
    </div>
</body>
</html>
