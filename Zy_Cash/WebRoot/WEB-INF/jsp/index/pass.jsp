<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/default.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<table class="t_add" width="100%" style="padding:10px;">
	<tr style="line-height:45px; ">
		<td align="right" >旧密码：</td>
		<td align="left">
		 	<input type="password" class="main_Input w146" id="old_pass" name="old_pass" value=""/>
		</td>
	</tr>
	<tr style="line-height:45px; ">
		<td align="right" >新密码：</td>
		<td align="left">
		 	<input type="password" class="main_Input w146" id="new_pass" name="new_pass" value="" maxlength="18"/>
		</td>
	</tr>
	<tr style="line-height:45px; ">
		<td align="right" >确认密码：</td>
		<td align="left">
		 	<input type="password" class="main_Input w146" id="two_pass" name="two_pass" value="" maxlength="18"/>
		</td>
	</tr>
</table>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script>
var config = parent.CONFIG,system=parent.SYSTEM;
var api = frameElement.api, W = api.opener;
$(function(){
	$("#btn-save").on('click',function(){
		var old_pass = $("#old_pass").val();
		var new_pass = $("#new_pass").val();
		var two_pass = $("#two_pass").val();
		if(null == new_pass || '' == new_pass){
			W.Public.tips({type: 1, content : "请输入新密码"});
			$("#new_pass").focus();
			return;
		}else{
			if(old_pass == new_pass){
				W.Public.tips({type: 1, content : "密码不能与原密码一样"});
				$("#new_pass").select().focus();
				return;
			}
			if(new_pass.length < 6){
				W.Public.tips({type: 1, content : "密码不能小于6位数"});
				$("#new_pass").select().focus();
				return;
			}
		}
		
		if(null == two_pass || '' == two_pass){
			Public.tips({type: 1, content : "请输入新密码"});
			$("#new_pass").focus();
			return;
		}else{
			if(new_pass != two_pass){
				W.Public.tips({type: 1, content : "两次密码不一样"});
				$("#two_pass").select().focus();
				return;
			}
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"index/editPass",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 0, content : data.message});
					setTimeout(function(){
						window.parent.location.replace(config.BASEPATH+"logout");
					},800);
				}else{
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",300);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	});
	$("#btn_close").click(function(){
		api.close();
	});
});
</script>
</body>
</html>