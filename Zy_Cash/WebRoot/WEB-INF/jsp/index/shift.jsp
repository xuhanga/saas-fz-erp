<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.span_title{width:80px;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<title>班次选择</title>
</head>
<body>
<div class="wrapper">
	<div class="mod-search cf">
		<div class="fl">
			<ul class="ul-inline">
				<li>
					<span class="span_title">交&nbsp;班&nbsp;人：</span>
					<span class="ui-combo-wrap" id="spanEmp"></span>
					<input type="hidden" id="de_man" name="de_man" value=""/>
				</li>
				<li>
					<span class="span_title">接&nbsp;班&nbsp;人：</span>
					<input type="text" id="emp_name" name="emp_name" readonly="readonly" class="main_Input w146" value="${sessionScope.cashier.em_name}"/>
				</li>
				<li>
					<span class="span_title">上班结余：</span>
					<input type="text" id="de_last_money" name="de_last_money" class="main_Input w146" value="0"
						onmouseup="this.select();" onkeyup="javascript:vadiUtil.vadiNumber(this);"/>					
				</li>
				<li>
					<span class="span_title">备&nbsp;用&nbsp;金：</span>
					<input type="text" id="de_petty_cash" name="de_petty_cash" class="main_Input w146" value="0"
						onmouseup="this.select();" onkeyup="javascript:vadiUtil.vadiNumber(this);"/>					
				</li>
			</ul>
		</div>
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="确定"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="关闭"/>
</div>
<script src="<%=basePath%>data/index/shift.js"></script>
</body>
</html>