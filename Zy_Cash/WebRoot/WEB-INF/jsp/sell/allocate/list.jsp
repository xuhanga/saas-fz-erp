<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
</script> 
</head>
<body id="main_content">
<form name="form1" method="post" action="" id="form1">
<input type="hidden" name="flushFlag" id="flushFlag" value=""/>
<div class="wrapper">
	<div class="mod-search cf">
	   <div class="fl">
	    	<div id="filter-menu" class="ui-btn-menu fl">
	    		 <span style="float:left" class="ui-btn menu-btn">
		     		 <strong>查询条件</strong>
		     			 <input type="text" class="main_Input Wdate-select" readonly="readonly" id="begindate" name="begindate" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'enddate\',{d:0})}'})"  value="" />
							-
	                     <input type="text" class="main_Input Wdate-select" readonly="readonly" id="enddate" name="enddate" onclick="WdatePicker({minDate:'#F{$dp.$D(\'begindate\',{d:0})}'})"   value="" />
					 <b></b>
			  	 </span>
		 		<div class="con" >
					<ul class="ul-inline">
						<li>
							<table class="searchbar">
								<tr>     
									<td align="right">日期选择：</td>
									<td id="date">
			                            <label class="radio" id="date_default" style="width:30px">
							  	 			<input name="redio1" type="radio" id="theDate" value="1" checked="checked"/>今天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="yesterday" value="2" />昨天
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theWeek" value="3" />本周
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theMonth" value="4" />本月
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theSeason" value="5" />本季
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="theYear" value="6" />本年
								  	 	</label>
								  	 	<label class="radio" id="label" style="width:30px">
								  	 		<input name="redio1" type="radio" id="lastYear" value="7" />去年
								  	 	</label>
							  	 	</td>
								 </tr>
								 <tr>     
									<td align="right">确认状态：</td>
									<td id="ar_state">
			                            <label class="radio" style="width:30px">
							  	 			<input name="radio_ar_state" type="radio" value="" checked="checked"/>全部
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="0" />暂存
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="1" />已调出
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="2" />完成
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_ar_state" type="radio" value="3" />已拒收
								  	 	</label>
								  	 	<input type="hidden" id="ac_state" name="ac_state" value=""/>
							  	 	</td>
								 </tr>
								 <tr>     
									<td id="td_out_in" colspan="2">
			                            <label class="radio" style="width:30px">
							  	 			<input name="radio_out_in" type="radio" value="all" checked="checked"/>全部
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_out_in" type="radio" value="out" />调出
								  	 	</label>
								  	 	<label class="radio" style="width:30px">
								  	 		<input name="radio_out_in" type="radio" value="in" />调入
								  	 	</label>
							  	 	</td>
								 </tr>
								  <tr>
									  <td align="right">调出店铺：</td>
										<td>
										  	<input class="main_Input" type="text" id="outshop_name" name="outshop_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" id="btn_out_shop" value="" class="btn_select" onclick="javascript:Utils.doQueryShop('out','ac_out_shop','outshop_name');"/>
										     <input type="hidden" name="ac_out_shop" id="ac_out_shop" value=""/>
								  		</td>
								  </tr>
								  <tr>
										<td align="right">调入店铺：</td>
										<td>
										  	<input class="main_Input" type="text" id="inshop_name" name="inshop_name" value="" readonly="readonly" style="width:170px; " />
										     <input type="button" id="btn_in_shop" value="" class="btn_select" onclick="javascript:Utils.doQueryShop('in','ac_in_shop','inshop_name');"/>
										     <input type="hidden" name="ac_in_shop" id="ac_in_shop" value=""/>
								  		</td>
									</tr>
									<tr>
										<td align="right">经办人员：</td>
										<td>
										  	<input class="main_Input" type="text" readonly="readonly" name="ac_man" id="ac_man" value="" style="width:170px;"/>
											<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
								  		</td>
									</tr>
									<tr>
										<td align="right">单据编号：</td>
										<td>
											<input type="text" id="ac_number" name="ac_number" class="main_Input" value=""/>
										</td>
									</tr>
							</table>
						</li>
						<li style="float:right;">
							<a class="ui-btn ui-btn-sp mrb" id="btn_ok">确认</a>
		       				<a class="ui-btn mrb" id="btn_reset">重置</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	    <div class="fl-m">
	    	<a class="ui-btn mrb" id="btn-search" name="btn-search">查询</a>
		</div>
	    <div class="fr">
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-report" >调拨明细</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-add" >新增</a>
	    	<a href="javascript:void(0);" class="ui-btn ui-btn-sp mrb" id="btn-del" >删除</a>
	    </div> 
	 </div>
	<div class="grid-wrap">
	    <table id="grid">
	    </table>
    	<div id="page"></div>
	</div>
</div>
</form>
<script src="<%=basePath%>data/sell/allocate/allocate_list.js"></script>
</body>
</html>