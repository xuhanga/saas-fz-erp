<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.notpassbarcode {
	background: url(<%=basePath%>resources/grid/images/poptxt1.png);
	width: 198px;
	height: 143px;
	position: absolute;
	font-size: 12px;
}
.notcheckbody {
	float: left;
	width: 155px;
	margin: 15px 0 0 10px;
	height: 110px;
	overflow-x: hidden;
	overflow-y: auto;
	SCROLLBAR-HIGHLIGHT-COLOR: #fee7cf;
	SCROLLBAR-ARROW-COLOR: #fee7cf;
}
.notcheckbody p {
	float: left;
	width: 155px;
	height: 16px;
	overflow: hidden;
	padding-left: 5px;
}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/search.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
<script>
//控制控件只能输入正整数 
function onlyPositiveNumber(e){ 
	var key = window.event ? e.keyCode : e.which;  
    var keychar = String.fromCharCode(key);   
    var result = (Validator.Number).test(keychar);   
    if(!result){      
	     return false;  
	 }else{     
	     return true;
	 } 
}
function valNumber(obj){
	var num = obj.value; 
	var k=window.event.keyCode;
	if(k == 109 || k == 189){
    	obj.value=obj.value.replace("-","");
    	return;
    }
	if(num.length > 0){
		if(num.match(Validator.Number) == null){
			obj.value="";
			Public.tips({type: 2, content : '请正确输入数量!'});
			$("#barcode_amount").focus()
			return false;
		}
	}
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="ac_state" name="ac_state" value="0"/>
<input type="hidden" id="ac_id" name="ac_id" value="${allocate.ac_id }"/>
<input type="hidden" id="ac_number" name="ac_number" value="${allocate.ac_number }"/>
<div class="border">
	<table width="100%" cellspacing="0" cellpadding="0">
	    <tr>
	      <td class="top-b border-b">
	        <input id="btn-save" class="t-btn btn-red iconfont" type="button" value="暂存单据"/>
	        <input id="btn-out" class="t-btn btn-cblue" type="button" value="调出"/>
	        <input id="btn-copy" class="t-btn btn-bblue" type="button" value="复制单据"/>
	        <input id="btn_close" class="t-btn iconfont" type="button" value="返回"/>
	      </td>
	    </tr>
    </table>
</div>
<div class="grid-wrap">
    <div id="list-grid" style="display:'';width: 100%;">
        <table id="grid"></table>
        <div id="page"></div>
    </div>
    <div id="size-grid" style="display:none; width: 100%;">
        <table id="sizeGrid"></table>
        <div id="sizePage"></div>
    </div>
    <div id="sum-grid" style="display:none;width: 100%;">
       <table id="sumGrid"></table>
       <div id="sumPage"></div>
   </div>
</div>
<div>
	<table cellpadding="0" cellspacing="0"  class="top-b border-t" width="100%">
		<tr>
			<td width="220">
				货号查询：<input name="pd_no" type="text" id="pd_no" class="ui-input ui-input-ph w120" onkeyup="javascript:if (event.keyCode==13){Choose.selectProduct();};"/>
               <input type="button" value=" " class="btn_select" onclick="javascript:Choose.selectProduct();"/>
			</td>
			<td width="280">
				扫描条码：<input name="text" type="text" id="barcode" onkeyup="javascript:if (event.keyCode==13){Choose.barCode()};" class="ui-input ui-input-ph w146"/>
				<input id="barcode_amount" class="ui-input ui-input-ph" value="1" style="width:20px" maxlength="4"
						onkeypress="return onlyPositiveNumber(event)"
						onkeyup="javascript:valNumber(this);"/>
			</td>
			<td width="270">
			</td>
			<td>
				<input type="hidden" id="CurrentMode" value="0"/>
				<a id="sumBtn" name="mode-btn" class="t-btn btn-bl"  onclick="showMode.display(2);"><i></i>汇总</a>
			    <a id="listBtn" name="mode-btn" class="t-btn btn-blc on"  onclick="showMode.display(0);"><i></i>列表</a>
			    <a id="sizeBtn" name="mode-btn" class="t-btn btn-bc" onclick="showMode.display(1);"><i></i>尺码</a>
			    <a class="t-btn btn-red" id="btnClear">清除</a>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr class="list first">
			<td align="right" width="60px">本店名称：</td>
			<td width="220px">
				<input class="main_Input" type="text" readonly="readonly" name="outshop_name" id="outshop_name" value="${allocate.outshop_name }"/>
				<input type="hidden" name="ac_out_shop" id="ac_out_shop" value="${allocate.ac_out_shop }" />
			</td>
			<td align="right" width="60px">目标店铺：</td>
			<td width="220px">
				<input class="main_Input" type="text" readonly="readonly" name="inshop_name" id="inshop_name" value="${allocate.inshop_name }" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryInShop();"/>
				<input type="hidden" name="ac_in_shop" id="ac_in_shop" value="${allocate.ac_in_shop }" />
			</td>
			<td align="right" width="60px">经办人员：</td>
			<td width="220px">
				<input class="main_Input" type="text" readonly="readonly" name="ac_man" id="ac_man" value="${allocate.ac_man }" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
			</td>
			<td align="right" width="60px">调拨日期：</td>
			<td>
				<input readonly type="text" class="main_Input Wdate"  name="ac_date" id="ac_date" onclick="WdatePicker()" value="${allocate.ac_date }" style="width:198px;"/>
			</td>
		</tr>
		<tr class="list last">
			<td align="right">发货仓库：</td>
			<td>
				<input class="main_Input" type="text" readonly="readonly" name="outdepot_name" id="outdepot_name" value="${allocate.outdepot_name }" style="width:170px;"/>
				<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryOutDepot();"/>
				<input type="hidden" name="ac_out_dp" id="ac_out_dp" value="${allocate.ac_out_dp }" />
			</td>
			<td align="right">总计数量：</td>
			<td>
				<input class="main_Input" type="text" name="ac_amount" id="ac_amount" value="" readonly="readonly"/>
			</td>
			<td align="right">总计金额：</td>
			<td>
				<input class="main_Input" type="text" id="ac_sell_money" value="" readonly="readonly"/>
			</td>
			<td align="right">备注：</td>
			<td>
				<input class="main_Input" type="text" name="ac_remark" id="ac_remark" value="${allocate.ac_remark }"/>
			</td>
		</tr>
	</table>
</div>
<input type="hidden" id="CheckNotPassTextShow" value=""/>
<div id="BarCodeCheckNotPass" class="notpassbarcode" title="双击关闭错误提示" style="display:none;" ondblclick="this.style.display='none'">
 	<span class="notcheckbody" id="CheckNotPassText"></span>
</div>
</form>
<script src="<%=basePath%>data/sell/allocate/allocate_add.js"></script>
</body>
</html>