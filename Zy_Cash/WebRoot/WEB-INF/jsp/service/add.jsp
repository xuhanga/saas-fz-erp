<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	.list td{line-height:35px; }
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<div class="border">
	<table style="width:100%;height:360;"  class="pad_t20" border="0">
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right"><font color="red">*</font>手机号码 :</div>
   			</td>
    		<td width="155px">
    			<input class="main_Input" type="text" name="ss_tel" id="ss_tel" maxlength ="11" value="" style="width:150px"/>
    		</td>
    		<td width="65px">
    			<div align="right"><font color="red">*</font>顾客姓名:</div>
    		</td>
    		<td>
    			<input class="main_Input" type="text"  style="width:150px" id="ss_name" name="ss_name" value="" maxlength=6/>
    		</td>
  		</tr>
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right"><font color="red">*</font>受理人员:</div>
   			</td>
    		<td width="155px">
    			 <input type="text" id="ss_manager" name="ss_manager" readonly="readonly" class="main_Input atfer_select w120" value="" />
    			 <input type="button" value=" " class="btn_select" onclick="javascript:Utils.doQueryEmp();" />
    		</td>
    		<td width="65px">
    			<div align="right"><font color="red">*</font>接受日期:</div>
    		</td>
    		<td>
    			<input readonly type="text" class="main_Input" name="ss_startdate" id="ss_startdate"  value="" style="width:150px">
    		</td>
  		</tr>
  		<tr>
   			<td width="80px" style="padding-top:3px;">
   				<div align="right"><font color="red">*</font>商品货号:</div>
   			</td>
    		<td width="155px">
    			<input class="main_Input" type="text" name="ss_pd_no" id="ss_pd_no" value="" style="width:150px;"/>
    		</td>
    		<td width="65px">
    			<div align="right">维修项目:</div>
    		</td>
    		<td>
				<span class="ui-combo-wrap" id="span_ss_ssi_id"></span>
    			<input type="hidden" id="ssi_name" name="ssi_name" value=""/>
				<input type="hidden" name="ss_ssi_id" id="ss_ssi_id"/>
    		</td>
  		</tr>
  		<tr>
    		<td>
    			<div align="right">存在问题:</div>
    		</td>
    		<td colspan="3">
				<textarea style="width:400px;height:50px;resize:none;" id="ss_question"  name="ss_question"></textarea>
    		</td>
  		</tr>
</table>
</div>
</form>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/service/service_add.js"></script>
</body>
</html>