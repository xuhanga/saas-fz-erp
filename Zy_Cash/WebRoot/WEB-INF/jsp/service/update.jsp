<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/css/base.css" rel="stylesheet"  type="text/css" />
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/forbidBackSpace.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script> 
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" id="showConditionflg" value="1"/>
<div class="mainwra" style="overflow:auto;">
  <div class="border" style="border-bottom:#ddd solid 1px;padding-bottom:10px;">
      <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
              <td colspan="6" class="top-b border-b" >
              <a onclick="javascript:goBack();" class="t-btn" id="btn_close">返回</a>
              <c:choose>
              <c:when test="${service.ss_state == 1 }">
                	<a class="t-btn btn-red" id="btn-save" onclick="javascript:handle.update('1','${service.ss_state }','${service.ss_id }');">顾客取货</a>
               </c:when>
               </c:choose>
              </td>
          </tr>
          <tr class="list first">
              <td align="right" width="100">顾客姓名：</td>
              <td width="150">${service.ss_name }</td>
              <td align="right" width="100">手机号码：</td>
              <td width="150">${service.ss_tel }</td>
              <td align="right" width="80">状态：</td>
              <td>
	              <font style="color:red;">
	              <c:choose>
				  <c:when test="${service.ss_state == 0 }">
	             	 待维修
	              </c:when>
	              <c:when test="${service.ss_state == 1 }">
	            	  待取走
	           	  </c:when>
	           	  <c:when test="${service.ss_state == 2 }">
	             	 已完成
	              </c:when>
	              <c:when test="${service.ss_state == 3 }">
	             	 待受理
	              </c:when>
	              </c:choose>
	              </font>
              </td>
          </tr>
          <tr class="list ">
            <td align="right">商品货号：</td>
            <td>${service.ss_pd_no }</td>
            <td align="right">项目：</td>
            <td>${service.ssi_name }</td>
            <td align="right">预约时间：</td>
            <td>${service.ss_sysdate }</td>
          </tr>
          <c:if test="${!empty service.ss_manager}">
          <tr class="list ">
	      	<td align="right">接收受理人员：</td>
	        <td>${service.ss_manager }</td>
	        <td align="right">接受日期：</td>
	        <td>${service.ss_startdate }</td>
	        <td>&nbsp;</td>
	        <td>&nbsp;</td>
	      </tr>
          </c:if>
          <c:if test="${!empty service.ss_pickupname}">
          <tr class="list ">
              <td align="right">取货受理人员：</td>
              <td>${service.ss_pickupname }</td>
              <td align="right">顾客取货日期：</td>
              <td>${service.ss_enddate }</td>
              <c:if test="${!empty service.ss_satis}">
	              <td align="right">满意度：</td>
		          <td>${service.ss_satis }</td>
		      </c:if>
          </tr>
          </c:if>
         <c:if test="${!empty service.ss_servicedate}">
         <tr class="list ">
        	  <td align="right">维修完成日期：</td>
              <td>${service.ss_servicedate }</td>
              <td align="right">维修结果：</td>
              <td colspan="3"><div style="border:0px;word-break: break-all; word-wrap:break-word;overflow:auto;">${service.ss_result }</div></td>
         </tr>
         </c:if>
         <tr class="list last">
              <td align="right">存在问题：</td>
              <td colspan="5" >${service.ss_question }</td>
         </tr>
    </table>
      <table cellpadding="0" cellspacing="0" style="width:90%;margin:10px 0 0 20px;">
        <tr>
            <td>
                <c:if test="${service.ss_state == 1}">
                	顾客取货评价：
                	<span class="ui-combo-wrap" id="span_satis">
					</span>
                	<input type="hidden" id="ss_satis" name="ss_satis" value=""/>
                </c:if>
            </td>
        </tr>
    </table>
  </div>
</div>
</form>
<script src="<%=basePath%>data/service/service_update.js"></script>
</body>
</html>