<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style>
.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 396px;
	    width: 96%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.span_value{margin:7px;padding:5px;float: left;}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post">
	<input type="hidden" id="sp_id" name="sp_id" value=""/>
	<input type="hidden" id="sp_code" name="sp_code" value=""/>
	<div class="border">
		<div class="mutab">
			<ul id="menu_tab">
				<li id="menu1" class="select" onclick="THISPAGE.changeType(1)">基本设置</li>
				<li id="menu2" class="menu" onclick="THISPAGE.changeType(2)" >打印格式</li>
				<li id="menu3" class="menu" onclick="THISPAGE.changeType(3)" >数据设置</li>
			</ul>
			<span style="color:red;margin-left: 20px;height:30px;font: 12px Microsoft Yahei;" id="errorTip"></span>
		</div>
		<div class="content" id="content1">
			<table class="t_add" width="100%">
				<tr>
					<td>
						<span class="span_value fl">
							<a id="isprint" class="">
								<label class="chk">
									<input type="checkbox"/>结算打印小票
								</label>
							</a>
						</span>
					</td>
					<td>
						<span class="span_value fl">
							<a id="autoprint" class="">
								<label class="chk">
									<input type="checkbox"/>小票自动打印
								</label>
							</a>
						</span>
					</td>
					<td>
						<span class="span_value fl">
							<a id="show_logo" class="">
								<label class="chk">
									<input type="checkbox"/>显示店铺LOGO
								</label>
							</a>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span class="span_value fl">
							小票打印份数：
							<input type="text" class="ui-input" style="width:20px;" id="print_times" name="print_times" value=""/>
						</span>
					</td>
					<td>
						<span class="span_value fl">
							纸张宽度：
							<input type="text" class="ui-input" style="width:30px;" id="page_width" name="page_width" value=""/>
						</span>
					</td>
					<td>
						<span class="span_value fl">
							纸张高度：
							<input type="text" class="ui-input" style="width:30px;" id="page_height" name="page_height" value=""/>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<span class="span_value fl">
							小票标题：
							<input type="text" class="ui-input" style="width:403px;" id="sp_title" name="sp_title" value=""/>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<span class="span_value fl">
							二&nbsp;&nbsp;维&nbsp;&nbsp;码：
							<input type="text" class="ui-input" style="width:403px;" id="sp_qrcode" name="sp_qrcode" value=""/>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<span class="span_value fl">
							<span style="">备注信息：</span>
							<textarea id="sp_remark" name="sp_remark" rows="7" cols="62"></textarea>
						</span>
					</td>
				</tr>
			</table>
		</div>
		<div class="content" id="content2" style="display: none;">
			<div class="grid-wrap">
		      <table id="grid">
		      </table>
		    </div>
		</div>
		<div class="content" id="content3" style="display: none;">
			<div class="grid-wrap">
		      <table id="gridData">
		      </table>
		    </div>
		</div>
	</div>
</form>
<div class="footdiv">
	<input class="btn_confirm" type="button" id="btn-save" name="btn-save" value="保存"/>
	<input class="btn_close" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
<script src="<%=basePath%>data/set/print_set.js"></script>
</body>
</html>