<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/reward.css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.celledit.js\"></sc" + "ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.size.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/dateselect.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/validator.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
</script>
<style type="text/css">
	#nav_pkjg,#nav_pkmx{cursor:pointer;padding:5px 10px;margin-left:10px;}
	.mainwra{position:fixed;left:0;top:0;bottom:10px;right:0;overflow-x:hidden;overflow-y:auto;}
	.baseinfo{float:left;width:98%;margin:15px 1% 0;}
	.baseinfo h3{float:left;height:60px;width:60px;margin:15px 0 0 15px;border:#ddd solid 5px;background:#fff;overflow:hidden;border-radius:50%}
	.baseinfo dl{}
	.baseinfo dl dd{float:left;width:160px;margin-top:5px;line-height:40px;padding-left:10px;}
	.mapinfo{float:left;border:#ddd solid 1px;height:300px;width:98%;margin:0 1%;border-top:0;}
	.tab_nav{float:left;width:98%;height:40px;margin:15px 1% 0;}
	.tab_nav span{display:inline;float:left;height:35px;line-height:35px;width:80px;background:#f4f4f4;border:#ddd solid 1px;margin-right:15px;padding:0 10px;border-radius:3px;cursor:pointer;}
	.tab_nav span.selted{background:#f60;border:#f50;color:#fff;}
	.gridinfo{float:left;width:98%;margin:10px 1% 0;}
	.zongjie{float:left;height:150px;border:#ddd solid 1px;width:99%;}
</style>
</head>

<body>
<div class="mainwra">
<input type="hidden" id="kp_id" name="kp_id" value="${ kpiPk.kp_id}"/>
<input type="hidden" id="kp_number" name="kp_number" value="${ kpiPk.kp_number}"/>
<input type="hidden" id="kp_type" name="kp_type" value="${ kpiPk.kp_type}"/>
<input type="hidden" id="kp_state" name="kp_state" value="${ kpiPk.kp_state}"/>
<input type="hidden" id="kp_score" name="kp_score" value="${ kpiPk.kp_score}"/>
	<div class="baseinfo">
		<dl>
			<dd>
				<span>考核范围：</span>
				<span>
					<c:choose>
						<c:when test="${ kpiPk.kp_type eq 0}">
							店铺
						</c:when>
						<c:when test="${ kpiPk.kp_type eq 1}">
							员工组
						</c:when>
						<c:when test="${ kpiPk.kp_type eq 2}">
							员工
						</c:when>
					</c:choose>
				</span>
			</dd>
			<dd>
				<span>单据编号：</span>
				<span>${kpiPk.kp_number }</span>
			</dd>
			<dd style="width:220px;">
				<span>时间范围：</span>
				<span>${kpiPk.kp_begin }</span>~<span>${kpiPk.kp_end }</span>
			</dd>
			<dd>
				<span>标准分数：</span>
				<span>${kpiPk.kp_score }</span>
			</dd>
			<dd>
				<span>奖励：</span>
				<span id="reward_names">${kpiPk.reward_names }</span>
			</dd>
			
			<dd>
				<span>当前获胜者：</span>
				<span id="winner"></span>
			</dd>
			<dd>
				<span>当前最高分：</span>
				<span id="maxScore"></span>
			</dd>
			<dd >
				<span>摘要信息：</span>
				<span>${kpiPk.kp_remark }</span>
			</dd>
		</dl>	
	</div>
	<div class="border">
		<table cellpadding="0" cellspacing="0" class="top-b border-t" width="100%">
			<tr>
				<td>
					<span id="nav_pkjg" class="selted"><i class="iconfont">&#xe62c;</i> PK图例</span>
					<span id="nav_pkmx" class="navr"><i class="iconfont">&#xe608;</i> PK明细</span>
					<span id="span_kpi_label" style="margin-left: 30px;">图例指标：</span>
					<span class="ui-combo-wrap" id="span_kpi" style="width:100px;"></span>
					<input type="hidden" name="chart_kpi" id="chart_kpi"/>
					<span style="float:right;">
					<a class="t-btn" id="btn_close" >返回</a>
					</span>
				</td>
			</tr>
		</table>
	</div>
	<div id="ffxz" class="grid-wrap">
		<table id="grid">
		</table>
	  	<div id="page"></div>
	</div>
	<div id="container"></div>
</div>
<script src="<%=basePath%>resources/chart/highcharts.4.0.3.js"></script>
<script src="<%=basePath%>data/shop/kpipk/kpipk_view.js?v=${time}"></script>
<script type="text/javascript">
$(function(){
	$("#nav_pkjg").click(function(){
		$(this).css("color","#f30");
		$("#nav_pkmx").css("color","#666");
		$("#ffxz").css("display","none");
		$("#container").show();
		$("#span_kpi_label").show();
		$("#span_kpi").show();
	});
	$("#nav_pkmx").click(function(){
		$(this).css("color","#f30");
		$("#nav_pkjg").css("color","#666");
		$("#ffxz").css("display","");
		$("#container").hide();
		$("#span_kpi_label").hide();
		$("#span_kpi").hide();
	});
	$("#nav_pkjg").click();
}) 
</script>
</body>
</html>