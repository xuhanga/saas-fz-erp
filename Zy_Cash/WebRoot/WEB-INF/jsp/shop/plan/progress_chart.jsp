<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base target="_self"/>
<meta http-equiv="Content-Type" content="text/html;charset=gbk"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=true&skin=mac\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.5.0.12.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts-more.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/modules/solid-gauge.js\"></sc"+"ript>");
</script>
<style>
body {font-family:"微软雅黑"; height:100%; overflow:hidden; background:#fff;}
</style>
</head>
<body>
<div class="Smartplan bwt">
    <div class="b_tit">
        <b>计划进度</b>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th scope="col" width="60">&nbsp;</th>
        <th scope="col">完成</th>
        <th scope="col">计划</th>
      </tr>
      <tr class="color_g">
        <th scope="row">本月</th>
        <td><span id="realmoney_month"></span></td>
        <td><span id="planmoney_month"></span></td>
      </tr>
      <tr class="color_b">
        <th scope="row">全年</th>
        <td><span id="realmoney_year"></span></td>
        <td><span id="planmoney_year"></span></td>
      </tr>
    </table>
    <div class="chart">
    	<div id="container-month" style="width: 50%; height: 200px; float: left"></div>
        <div id="container-year" style="width: 50%; height: 200px; float: left"></div>
        <!-- <div class="chart_b color_g" id="monthChart">
            <span style="height:0px;" id="finishMonthChart"></span>
            <b>本 月</b>
            <i id="monthPercent"></i>
        </div>
        <div class="chart_b color_b" id="yearChart">
            <span style="height:0px;" id="finishYearChart"></span>
            <b>全 年</b>
            <i id="yearPercent"></i>
        </div> -->
    </div>
</div>
<script>
var config = Public.getDefaultPage().CONFIG;

var gaugeOptions = {
    chart: {
        type: 'solidgauge'
    },
    title: null,
    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },
    tooltip: {
        enabled: false
    },
    // the value axis
    yAxis: {
        stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickPixelInterval: 400,
        tickWidth: 0,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },
    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};


var THISPAGE = {
	init:function(){
		this.initDom();
		this.initPlanProgress();
	},
	initDom:function(){
	},
	initPlanProgress:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'shop/plan/statByYearMonth',
			data:{},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					$("#planmoney_month").text(data.data.planmoney_month);
					$("#realmoney_month").text(data.data.realmoney_month);
					$("#planmoney_year").text(data.data.planmoney_year);
					$("#realmoney_year").text(data.data.realmoney_year);
					THISPAGE.buildMonthProgress(data.data.planmoney_month,data.data.realmoney_month);
					THISPAGE.buildYearProgress(data.data.planmoney_year,data.data.realmoney_year);
				}
			}
		});
	},
	buildMonthProgress:function(planMoney,realMoney){
		var monthPercent = 0;
		if(planMoney != 0){
			monthPercent = parseInt(realMoney*100/planMoney);
		}
		$('#container-month').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 100,
	            title: {
	                text: '本月完成情况'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        series: [{
	            name: '本月完成',
	            data: [monthPercent],
	            dataLabels: {
	                format: '<div style="text-align:center"><br/><span style="font-size:18px;color:' +
	                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span></div>'
	            },
	            tooltip: {
	                valueSuffix: ''
	            }
	        }]
	    }));
	},
	buildYearProgress:function(planMoney,realMoney){
		var yearPercent = 0;
		if(planMoney != 0){
			yearPercent = parseInt(realMoney*100/planMoney);
		}
		$('#container-year').highcharts(Highcharts.merge(gaugeOptions, {
	        yAxis: {
	            min: 0,
	            max: 100,
	            title: {
	                text: '本年完成情况'
	            }
	        },
	        series: [{
	            name: '本年完成',
	            data: [yearPercent],
	            dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:18px;color:' +
	                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span></div>'
	            },
	            tooltip: {
	                valueSuffix: '%'
	            }
	        }]
	    }));
	}
};
$(function(){
	THISPAGE.init();
});
</script>
</body>
</html>
