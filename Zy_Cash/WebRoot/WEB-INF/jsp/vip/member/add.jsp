<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
	.main_Input{width: 130px; height: 17px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 360px;
	    width: 96%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.error{
		color:red;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.extend.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.validate.messages_zh.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/My97DatePicker/WdatePicker.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/date-util.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jquery.combo.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/json2.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/util/lundarDate.js\"></sc"+"ript>");
</script>
<script>
var api = frameElement.api, W = api.opener;
var _callback = api.data.callback;
function doClose(){
	api.close();
}
function selContent(showId){
	$("#menu1").removeClass().addClass("menu");
	$("#menu2").removeClass().addClass("menu");
	$("#menu3").removeClass().addClass("menu");
	$("#menu"+showId).removeClass().addClass("select");
	$("#tagContent1").hide();
	$("#tagContent2").hide();
	$("#tagContent3").hide();
	$("#tagContent"+showId).show();
}
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="vm_state" id="vm_state" value="0"/>
<div class="border">
	<div class="mutab">
		<ul>
			<li id="menu1" class="select" onclick="selContent('1')">基本资料</li>
			<li id="menu2" class="menu" onclick="selContent('2')" >生活属性</li>
			<li id="menu3" class="menu" onclick="selContent('3')" >穿衣属性</li>
		</ul>
		<span style="color:red;margin-left: 20px;height:30px;font: 12px Microsoft Yahei;" id="errorTip"></span>
	</div>
	<div>
		<div class="content" id="tagContent1">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px"><b>*</b>手机：</td>
					<td width="130px">
						<input class="main_Input required" type="text" name="vm_mobile" id="vm_mobile" value=""
								onkeyup="javascript:$('#vm_cardcode').val($(this).val());"/>
					</td>
					<td align="right" width="80px"><b>*</b>卡号：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="vm_cardcode" id="vm_cardcode" value=""/>
					</td>
					<td align="right" width="80px"><b>*</b>类别：</td>
					<td width="130px">
						<span class="ui-combo-wrap" id="span_membertype">
						</span>
						<input type="hidden" name="vm_mt_code" id="vm_mt_code" value=""/>
					</td>
					<td></td>
				</tr>
				<tr class="list">
					<td align="right"><b>*</b>姓名：</td>
					<td>
						<input class="main_Input" type="text" name="vm_name" id="vm_name" value=""/>
					</td>
					<td align="right">店铺：</td>
					<td>
						<input readonly="readonly" class="main_Input" type="text" name="shop_name" id="shop_name" value=""/>
						<input type="hidden" name="vm_shop_code" id="vm_shop_code" value=""/>
					</td>
					<td align="right">初始积分：</td>
					<td>
						<input class="main_Input" type="text" name="vm_init_points" id="vm_init_points" value="0"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">公历生日：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate"  name="vm_birthday" id="vm_birthday" 
							onFocus="WdatePicker({onpicked:Utils.onchangeVm_Birthday(this)})" value=""/>
					</td>
					<td align="right">农历生日：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate" name="vm_lunar_birth" id="vm_lunar_birth"  
							onFocus="WdatePicker({onpicked:Utils.onchangeVm_LunarBirthday(this)})" value="" style="width:99px;"/>
						<input readonly type="text" class="main_Input" name="vmi_anima" id="vmi_anima" value="" style="width:15px;"/>
					</td>
					<td align="right">生日方式：</td>
					<td>
						<span class="ui-combo-wrap" id="span_birthdaytype">
						</span>
						<input type="hidden" name="vm_birthday_type" id="vm_birthday_type"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">办卡日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate"  name="vm_date" id="vm_date" onclick="WdatePicker()" value=""/>
					</td>
					<td align="right">有效日期：</td>
					<td>
						<input readonly type="text" class="main_Input Wdate" name="vm_enddate" id="vm_enddate" onclick="WdatePicker()" value=""/>
					</td>
					<td align="right">性别：</td>
					<td>
						<span class="ui-combo-wrap" id="span_sex">
						</span>
						<input type="hidden" name="vm_sex" id="vm_sex"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">婚姻状况：</td>
					<td>
						<span class="ui-combo-wrap" id="span_marriage_state">
						</span>
						<input type="hidden" name="vmi_marriage_state" id="vmi_marriage_state"/>
					</td>
					<td align="right">民族：</td>
					<td>
						<span class="ui-combo-wrap" id="span_nation">
						</span>
						<input type="hidden" name="vmi_nation" id="vmi_nation"/>
					</td>
					<td align="right">血型：</td>
					<td>
						<span class="ui-combo-wrap" id="span_bloodtype">
						</span>
						<input type="hidden" name="vmi_bloodtype" id="vmi_bloodtype"/>
					</td>
				</tr>
				
				<tr class="list">
					<td align="right">证件选择：</td>
					<td>
						<span class="ui-combo-wrap" id="span_idcard_type">
						</span>
						<input type="hidden" name="vmi_idcard_type" id="vmi_idcard_type"/>
					</td>
					<td align="right">证件号：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_idcard" id="vmi_idcard" value=""/>
					</td>
					<td align="right">工作单位：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_work_unit" id="vmi_work_unit" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">QQ：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_qq" id="vmi_qq" value=""/>
					</td>
					<td align="right">微信：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_wechat" id="vmi_wechat" value=""/>
					</td>
					<td align="right">E-Mail：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_email" id="vmi_email" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">职业选择：</td>
					<td>
						<span class="ui-combo-wrap" id="span_job">
						</span>
						<input type="hidden" name="vmi_job" id="vmi_job"/>
					</td>
					<td align="right">宗教信仰：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_faith" id="vmi_faith" value=""/>
					</td>
					<td align="right">经办人：</td>
					<td>
						<input readonly="readonly" class="main_Input" style="width:105px;" type="text" name="vm_manager" id="vm_manager" value=""/>
						<input type="button" value="" class="btn_select" onclick="javascript:Utils.doQueryEmp();"/>
						<input class="main_Input" type="hidden" name="vm_manager_code" id="vm_manager_code" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">推荐人：</td>
					<td>
						<input class="main_Input" style="width:105px;" type="text" id="referee_name" name="referee_name" value="" readonly="readonly" /> 
						<input type="button" id="btn_referee" name="btn_referee" value="" class="btn_select" /> 
						<input type="hidden" name="vm_referee" id="vm_referee" value="" />
					</td>
					<td align="right">地址：</td>
					<td colspan="3">
						<input class="main_Input" type="text" name="vmi_address" id="vmi_address" value="" style="width:377px;"/>
					</td>
				</tr>
			</table>
		</div>
		<div class="content" id="tagContent2" style="display:none;overflow-y:auto;">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">文化：</td>
					<td width="130px">
						<span class="ui-combo-wrap" id="span_culture">
						</span>
						<input type="hidden" name="vmi_culture" id="vmi_culture"/>
					</td>
					<td align="right" width="80px">收入水平：</td>
					<td width="130px">
						<span class="ui-combo-wrap" id="span_income_level">
						</span>
						<input type="hidden" name="vmi_income_level" id="vmi_income_level"/>
					</td>
					<td align="right" width="80px">家庭信息：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_family" id="vmi_family" value=""/>
					</td>
					
				</tr>
				<tr class="list">
					<td align="right">口语习惯：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_oral_habit" id="vmi_oral_habit" value=""/>
					</td>
					<td align="right">喜欢话题：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_topic" id="vmi_like_topic" value=""/>
					</td>
					<td align="right">禁忌话题：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_taboo_topic" id="vmi_taboo_topic" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">交通工具：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_transport" id="vmi_transport" value=""/>
					</td>
					<td align="right">抽烟与否：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_smoke" id="vmi_smoke" value=""/>
					</td>
					<td align="right">饮食偏好：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_food" id="vmi_like_food" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">家庭节日：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_family_festival" id="vmi_family_festival" value=""/>
					</td>
					<td align="right">兴趣：</td>
					<td colspan="3">
						<input class="main_Input" style="width:373px;" type="text" name="vmi_hobby" id="vmi_hobby" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">性格特点：</td>
					<td colspan="3">
						<input class="main_Input" style="width:373px;" type="text" name="vmi_character" id="vmi_character" value=""/>
					</td>
				</tr>
			</table>
		</div>
		<div class="content" id="tagContent3" style="display:none;overflow-y:auto;">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">身高(CM)：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="vmi_height" id="vmi_height" value=""/>
					</td>
					<td align="right" width="80px">体重(KG)：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="vmi_weight" id="vmi_weight" value=""/>
					</td>
					<td align="right" width="80px">肤色：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_skin" id="vmi_skin" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">胸围(CM)：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_bust" id="vmi_bust" value=""/>
					</td>
					<td align="right">腰围(CM)：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_waist" id="vmi_waist" value=""/>
					</td>
					<td align="right">臀围(CM)：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_hips" id="vmi_hips" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">上装尺码：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_top" id="vmi_size_top" value=""/>
					</td>
					<td align="right">下装尺码：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_lower" id="vmi_size_lower" value=""/>
					</td>
					<td align="right">鞋子尺码：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_shoes" id="vmi_size_shoes" value=""/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">内衣尺寸：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_bra" id="vmi_size_bra" value=""/>
					</td>
					<td align="right">裤长：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_trousers_length" id="vmi_trousers_length" value=""/>
					</td>
					<td align="right">脚型：</td>
					<td>
						<span class="ui-combo-wrap" id="span_foottype">
						</span>
						<input type="hidden" name="vmi_foottype" id="vmi_foottype"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">钟意颜色：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_color" id="vmi_like_color" value=""/>
					</td>
					<td align="right">常穿品牌：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_brand" id="vmi_like_brand" value=""/>
					</td>
					<td align="right">穿着偏好：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_wear_prefer" id="vmi_wear_prefer" value=""/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="footdiv">
	<input class="ui-btn ui-btn-sp mrb" id="btn-save" name="btn-save" type="button" value="确定"/>
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="取消"/>
</div>
</form>
<script>
	$().ready(function() {
		$("#form1").validate({
			ignore: "",
			onfocusout : function(element) {
				$(element).valid();
			},
			errorPlacement: function(error, element) { //错误信息位置设置方法
				element.focus();
				$("#errorTip").empty();
				error.appendTo( $("#errorTip") );
			},
			rules : {
				vm_cardcode : "required",
				vm_mobile : {
					required : true,
					isMobile : true
				},
				vm_tel : {
					isPhone : true
				},
				vm_name : {
					required : true,
					maxlength : 15
				}
			},
			messages : {
				vm_cardcode : "请输入会员卡号",
				vm_mobile : "请输入正确的手机号码",
				vm_tel : "请输入正确的电话号码",
				vm_name : {
					required : "请输入会员姓名",
					maxlength : "姓名太长"
				}
			}
		});
	});
</script>
<script src="<%=basePath%>data/vip/member/member_add.js"></script>
</body>
</html>