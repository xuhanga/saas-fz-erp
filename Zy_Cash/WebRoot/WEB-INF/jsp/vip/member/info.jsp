<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0" />
<title></title>
<link href="<%=basePath%>resources/grid/css/common.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.base.css" rel="stylesheet" type="text/css"/>
<link href="<%=basePath%>resources/grid/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	table tr td b {color:red;}
	.list td{line-height:32px; }
	.main_Input{width: 130px; height: 17px;}
	.width100{width:100px;}
	.mutab {
	    height: 32px;
	    margin: 10px 0 0 11px;
	}
	ul {
	    list-style: none;
	}
	.mutab li {
	    cursor: pointer;
	    display: inline;
	    float: left;
	    margin-right: 5px;
	}
	.select {
	    background: #fff;
	    border-style: solid;
	    border-width: 2px 1px 0;
	    border-color: #08c #ddd #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    color: #08c;
	    font-weight: 700;
	    cursor: hand;
	}
	.menu {
	    background: #f1f1f1;
	    border-style: solid;
	    border-width: 1px 1px 0;
	    border-color: #ddd;
	    height: 30px;
	    text-align: center;
	    line-height: 30px;
	    width: 120px;
	    cursor: hand;
	}
	.content {
	    border: #ddd 1px solid;
	    height: 420px;
	    width: 98%;
	    background-color: #FFFFFF;
	    margin: 0 0 0 10px;
	}
	.error{
		color:red;
	}
</style>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/common.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.local.cn.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.base.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/grid/js/jqgrid.custom.js\"></sc"+"ript>");
</script>
</head>
<body>
<form action="" id="form1" name="form1" method="post" >
<input type="hidden" name="vm_code" id="vm_code" value="${model.vm_code}"/>
<div class="border">
	<div class="mutab">
		<ul>
			<li id="menu1" class="select" onclick="Tab('1')">会员资料</li>
			<li id="menu2" class="menu" onclick="Tab('2')" >消费记录</li>
			<li id="menu3" class="menu" onclick="Tab('3')" >积分记录</li>
		</ul>
		<span style="color:red;margin-left: 20px;height:30px;font: 12px Microsoft Yahei;" id="errorTip"></span>
	</div>
	<div>
		<div class="content" id="tagContent1">
			<table width="100%">
				<tr class="list first">
					<td align="right" width="80px">手机：</td>
					<td width="130px">
						<input class="main_Input" readonly="readonly" type="text" name="vm_mobile" id="vm_mobile" value="${model.vm_mobile}"/>
					</td>
					<td align="right" width="80px">卡号：</td>
					<td width="130px">
						<input class="main_Input" readonly="readonly" type="text" name="vm_cardcode" id="vm_cardcode" value="${model.vm_cardcode}"/>
					</td>
					<td align="right" width="80px">类别：</td>
					<td width="130px">
						<input class="main_Input" readonly="readonly" type="text" name="mt_name" id="mt_name" value="${model.mt_name}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">姓名：</td>
					<td>
						<input class="main_Input"  readonly="readonly" type="text" name="vm_name" id="vm_name" value="${model.vm_name}"/>
					</td>
					<td align="right">店铺：</td>
					<td>
						<input readonly="readonly" class="main_Input" type="text" name="shop_name" id="shop_name" value="${model.shop_name}"/>
					</td>
					<td align="right">状态：</td>
					<td >
						<input readonly="readonly" class="main_Input" type="text" name="_vm_state" id="_vm_state" value=""/>
						<input type="hidden" name="vm_state" id="vm_state" value="${model.vm_state}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">阳历生日：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input"  name="vm_birthday" id="vm_birthday" 
							value="${model.vm_birthday}"/>
					</td>
					<td align="right">阴历生日：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input" name="vm_lunar_birth" id="vm_lunar_birth"  
							value="${model.vm_lunar_birth}"/>
					</td>
					<td align="right">有效期：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input" name="vm_enddate" id="vm_enddate"  
							value="${model.vm_enddate}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">累计消费：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input"  name="vm_total_money" id="vm_total_money" 
							value="${model.vm_total_money}"/>
					</td>
					<td align="right">累计积分：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input" name="vm_total_point" id="vm_total_point"  
							value="${model.vm_total_point}"/>
					</td>
					<td align="right">最新消费：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input" name="vm_lastbuy_date" id="vm_lastbuy_date"  
							value="${model.vm_lastbuy_date}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">已用积分：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input"  name="vm_used_points" id="vm_used_points" 
							value="${model.vm_used_points}"/>
					</td>
					<td align="right">剩余积分：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input" name="vm_points" id="vm_points"  
							value="${model.vm_points}"/>
					</td>
					<td align="right">消费次数：</td>
					<td>
						<input type="text" readonly="readonly" class="main_Input" name="vm_times" id="vm_times"  
							value="${model.vm_times}"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right" width="80px">身高(CM)：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="vmi_height" id="vmi_height" value="${memberinfo.vmi_height }"/>
					</td>
					<td align="right" width="80px">体重(KG)：</td>
					<td width="130px">
						<input class="main_Input" type="text" name="vmi_weight" id="vmi_weight" value="${memberinfo.vmi_weight }"/>
					</td>
					<td align="right" width="80px">肤色：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_skin" id="vmi_skin" value="${memberinfo.vmi_skin }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">胸围(CM)：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_bust" id="vmi_bust" value="${memberinfo.vmi_bust }"/>
					</td>
					<td align="right">腰围(CM)：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_waist" id="vmi_waist" value="${memberinfo.vmi_waist }"/>
					</td>
					<td align="right">臀围(CM)：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_hips" id="vmi_hips" value="${memberinfo.vmi_hips }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">上装尺码：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_top" id="vmi_size_top" value="${memberinfo.vmi_size_top }"/>
					</td>
					<td align="right">下装尺码：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_lower" id="vmi_size_lower" value="${memberinfo.vmi_size_lower }"/>
					</td>
					<td align="right">鞋子尺码：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_shoes" id="vmi_size_shoes" value="${memberinfo.vmi_size_shoes }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">内衣尺寸：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_size_bra" id="vmi_size_bra" value="${memberinfo.vmi_size_bra }"/>
					</td>
					<td align="right">裤长：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_trousers_length" id="vmi_trousers_length" value="${memberinfo.vmi_trousers_length }"/>
					</td>
					<td align="right">脚型：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_foottype" id="vmi_foottype" value="${memberinfo.vmi_foottype }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">钟意颜色：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_color" id="vmi_like_color" value="${memberinfo.vmi_like_color }"/>
					</td>
					<td align="right">常穿品牌：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_brand" id="vmi_like_brand" value="${memberinfo.vmi_like_brand }"/>
					</td>
					<td align="right">穿着偏好：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_wear_prefer" id="vmi_wear_prefer" value="${memberinfo.vmi_wear_prefer }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">口语习惯：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_oral_habit" id="vmi_oral_habit" value="${memberinfo.vmi_oral_habit }"/>
					</td>
					<td align="right">喜欢话题：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_topic" id="vmi_like_topic" value="${memberinfo.vmi_like_topic }"/>
					</td>
					<td align="right">禁忌话题：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_taboo_topic" id="vmi_taboo_topic" value="${memberinfo.vmi_taboo_topic }"/>
					</td>
				</tr>
				<tr class="list">
					<td align="right">交通工具：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_transport" id="vmi_transport" value="${memberinfo.vmi_transport }"/>
					</td>
					<td align="right">抽烟与否：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_smoke" id="vmi_smoke" value="${memberinfo.vmi_smoke }"/>
					</td>
					<td align="right">饮食偏好：</td>
					<td>
						<input class="main_Input" type="text" name="vmi_like_food" id="vmi_like_food" value="${memberinfo.vmi_like_food }"/>
					</td>
				</tr>
			</table>
		</div>
		<div class="content" id="tagContent2" style="display:none;overflow-y:auto;">
			<div class="grid-wrap">
		      <table id="grid">
		      </table>
		    </div>
		</div>
		<div class="content" id="tagContent3" style="display:none;overflow-y:auto;">
			<div class="grid-wrap">
		      <table id="gridData">
		      </table>
		    </div>
		</div>
	</div>
</div>
<div class="footdiv">
	<input class="ui-btn mrb" type="button" id="btn_close" name="btn_close" value="关闭"/>
</div>
</form>
<script src="<%=basePath%>data/vip/member/member_info.js"></script>
</body>
</html>