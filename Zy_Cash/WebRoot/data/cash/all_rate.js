var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var maxmoney=system.MAXMONEY,minrate=system.MIN_RATE;
var sell_money = api.data.sell_money;
var handle = {
	clear:function(){
		$("#rate_value").val("");
		$("#plus_value").val("");
		$("#real_value").val("");
	},
	setFocus:function(){
		var type = $("#hand_type").val();
		if(1 == type){
			$("#rate_value").focus();
		}
		if(2 == type){
			$("#plus_value").focus();
		}
		if(3 == type){
			$("#real_value").focus();
		}
	},
	valFocus:function(type){
		$("input:radio").each(function(){
			if(type == $(this).val()){
				$(this).prop("checked",true);
				$('#hand_type').val(type);
			}
		});
	},
	getValue:function(type){
		var val = 0;
		if(1 == type){
			val = $("#rate_value").val();
		}
		if(2 == type){
			val = $("#plus_value").val();
		}
		if(3 == type){
			val = $("#real_value").val();
		}
		return val;
	},
	handRate:function(){
		var type = $("#hand_type").val();
		var sell_type = W.$("#sell_type").val();
		var val = handle.getValue(type);
		if(null == val || "" == val){
			W.Public.tips({type:1,content:'请输入折扣!'});
			return false;
		}
		if(1 == type){
			if(val < parseFloat(minrate)){
				W.Public.tips({type: 1, content : "折扣超过权限"});
				$("#rate_value").select().focus();
				return;
			}
		}
		if(2 == type){
			if(val > parseFloat(maxmoney)){
				W.Public.tips({type: 1, content : "让利超过权限"});
				$("#plus_value").select().focus();
				return;
			}
			if((parseFloat(sell_money) - val) < 0){
				W.Public.tips({type: 1, content : "让利不能超过零售金额"});
				$("#plus_value").select().focus();
				return;
			}
		}
		if(3 == type){
			if(val < 0){
				W.Public.tips({type: 1, content : "实际金额不能为负"});
				$("#real_value").select().focus();
				return;
			}
			if((parseFloat(sell_money)-val) > parseFloat(maxmoney)){
				W.Public.tips({type: 1, content : "让利超过权限"});
				$("#real_value").select().focus();
				return;
			}
		}
		
		var mdata = {};
		mdata.sell_type=sell_type;
		mdata.hand_type=type;
		mdata.hand_value=val;
		mdata.total_money=sell_money;
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/allRate",
			data:mdata,
			cache:false,
			dataType:"json",
			success:function(data){
				if (data.stat == 200){
					W.flag = true;
					setTimeout("api.close()",200);
				}else{
					W.flag = false;
					W.Public.tips({type:1,content:'打折失败!'});
				}
			}
		});
	}
};
var vadiUtil={
	vadiDouble:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入数字"});
			$(obj).val("").focus();
			return;
		}
		if (data<0 || data>1) {
			W.Public.tips({type: 1, content : "数字必须0-1之间"});
			$(obj).val("").focus();
			return;
		}
	},
	vadiNumber:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入数字"});
			$(obj).val("").focus();
			return;
		}
		if(parseFloat(data) < 0){
			W.Public.tips({type: 1, content : "让利不能为负"});
			$(obj).val("").focus();
			return;
		}
	}
};
var THISPAGE = {
	init:function(){
		this.initDom();
		this.initEvent();
	},
	initDom:function(){
		$("#sell_money").text(sell_money);
		_self = this;
		_self.$_many_rate = $("#many_rate").cssCheckbox();
		_self.$_no_point = $("#no_point").cssCheckbox();
		var rate_type = system.RATE_TYPE;
		if(null != rate_type && "" != rate_type){
			$("#hand_type").val(rate_type);
			handle.valFocus(rate_type);
			setTimeout("handle.setFocus()",300);
		}else{
			$('input:radio:first').prop('checked', true);
			$("#rate_value").focus();;
			$("#hand_type").val("1");
		}
	},
	initEvent:function(){
		$("#rate_value").on('keyup',function(event){
			if(event.keyCode == 13){
				$("#btn-save").click();
			}
		});
		$("#plus_value").on('keyup',function(event){
			if(event.keyCode == 13){
				$("#btn-save").click();
			}
		});
		$("#real_value").on('keyup',function(event){
			if(event.keyCode == 13){
				$("#btn-save").click();
			}
		});
		$("#btn-save").on('click',function(e){
			e.preventDefault();
			handle.handRate();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
};
THISPAGE.init();