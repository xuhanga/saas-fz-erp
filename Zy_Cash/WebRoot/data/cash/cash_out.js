var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var erase = system.ERASE,sell_type = api.data.sell_type;
var total_money = api.data.total_money;
var vip_id = api.data.vip_id;
var sd_deposit = api.data.sd_deposit,sd_number = api.data.sd_number,isvip=api.data.isvip;
var erg = /^(-)?||[1-9]*\d+$/;
var handle = {
	cashOut:function(){
		var sell_money = $("#sell_money").val();
		var sh_cash = $("#sh_cash").val();
		var change_money = $("#change_money").val();
		if(change_money != null && change_money != "" && change_money.length > 0){
			change_money = parseFloat(change_money).toFixed(2);
		}else{
			change_money = 0;
		}
//		sh_cash = parseFloat(sh_cash)-parseFloat(change_money);//现金获取，不用再减去找零金额，那样现金就很大了
		var sh_lost_money = $("#sh_lost_money").val();
		var sh_bank_money = $("#sh_bank_money").val();
		var sh_mall_money = $("#sh_mall_money").val();
		var sh_vc_money = $("#sh_vc_money").val();
		var sh_ec_money = $("#sh_ec_money").val();
		var sh_cd_money = $("#sh_cd_money").val();
		var sh_point_money = $("#sh_point_money").val();
		var sh_sd_money = $("#sh_sd_money").val();
		var sh_wx_money = $("#sh_wx_money").val();
		var sh_ali_money = $("#sh_ali_money").val();
		var sh_third_money = $("#sh_third_money").val();
		var real_money = 0;
		real_money += parseFloat(sh_cash) + parseFloat(sh_lost_money);
		real_money += parseFloat(sh_bank_money) + parseFloat(sh_mall_money);
		real_money += parseFloat(sh_vc_money) + parseFloat(sh_ec_money);
		real_money += parseFloat(sh_cd_money)+ parseFloat(sh_third_money);// parseFloat(sh_sc_money) + 
		real_money += parseFloat(sh_point_money) + parseFloat(sh_sd_money);
		real_money += parseFloat(sh_wx_money) + parseFloat(sh_ali_money);
		if(parseFloat(real_money) < parseFloat(sell_money)){
			W.Public.tips({type: 1, content : "支付金额不够"});
			return;
		}
		if(parseFloat(change_money) < 0){
			W.Public.tips({type: 1, content : "支付金额不够"});
			return;
		}
		W.$.dialog.confirm('确定收银吗?', function(){
			handle.sellCashOut();
		});
	},
	sendEcoupon:function(param){
		sendData = W.$.dialog({ 
		   	title: '优惠券发放',
		   	max: false,
		   	min: false,
		   	data:param,
		   	width:300,
		   	height:200,
		   	drag: false,
		   	resize:false,
		   	lock:false,
		   	content:"url:"+config.BASEPATH+"cash/to_send_ecoupon",
		   	close: function(){
				setTimeout("api.close()",100);
		    },
		    ok:false,
		    cancel:false
	    });
	},
	sellCashOut:function(){
		var change_money = $("#change_money").val();
		if(change_money != null && change_money != "" && change_money.length > 0){
			change_money = parseFloat(change_money).toFixed(2);
		}else{
			change_money = 0;
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'cash/sellCashOut',
			data:encodeURI(encodeURI($('#form1').serialize())),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.flag = true;
					W.$("#sh_id").val(data.data.sh_id);
					W.$("#change_money").val(change_money);
					if(undefined != data.data.user && null != data.data.user && "" != data.data.user){
						handle.sendEcoupon(data.data.user);
					}else{
						setTimeout("api.close()",100);
					}
				}else{
					W.$("#sh_id").val("");
					W.flag = false;
					W.Public.tips({type: 1, content : "收银失败"});
				}
			}
		});
	},
	doVoucher:function(){
		vcData = W.$.dialog({ 
		   	title: '代金券使用',
		   	max: false,
		   	min: false,
		   	width:500,
		   	height: 370,
		   	drag: false,
		   	resize:false,
		   	lock:false,
		   	content:"url:"+config.BASEPATH+"cash/to_voucher",
		   	close: function(){
		   		/*var pdata = vcData.content.submit();
				if(pdata){
					$("#vc_ids").val(pdata.vc_ids);
					$("#vc_money").val(pdata.vc_money);
					$("#vc_rate").val(pdata.vc_rate);
					$("#sh_vc_money").val(pdata.sh_vc_money);
					THISPAGE.buildCash();
				}
		   		 */
				setTimeout("api.zindex()",200);
		    },
		    ok:function(){
		    	var pdata = vcData.content.submit();
				if (!pdata) {
					return false;
				}else{
					$("#vc_ids").val(pdata.vc_ids);
					$("#vc_money").val(pdata.vc_money);
					$("#vc_rate").val(pdata.vc_rate);
					$("#sh_vc_money").val(pdata.sh_vc_money);
					THISPAGE.buildCash();
				}
				setTimeout("api.zindex()",200);
		    },
		    cancel:true
	   });
	},
	doEcoupon:function(){
		couponData = W.$.dialog({ 
		   	title: '优惠券使用',
		   	max: false,
		   	min: false,
		   	width:600,
		   	height: 350,
		   	drag: false,
		   	lock:false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_ecoupon",
		   	close: function(){
		   		var ecu_id = $("#ecu_id").val();
		   		if(null != ecu_id && "" != ecu_id){
		   			THISPAGE.buildCash();
		   		}
				setTimeout("api.zindex()",200);
		    },
		    ok:false,
		    cancel:true
	   });
	},
	doThird:function(){
		thirdData = W.$.dialog({ 
		   	title: '收钱吧使用',
		   	max: false,
		   	min: false,
		   	data:{"total_money":total_money,callback: this.callback},
		   	width:400,
		   	height: 250,
		   	drag: false,
		   	lock:false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_third",
		   	close: function(){
				setTimeout("api.zindex()",200);
		    },
		    ok:false,
		    cancel:false
	   });
	},
	callback: function(data, oper, dialogWin){
		if(undefined != data && data.flag == 1){
			$("#sh_third_money").val(data.third_money);
			THISPAGE.buildCash();
		}
	},
	doCard:function(){
		cardData = W.$.dialog({ 
		   	title: '储值卡使用',
		   	max: false,
		   	min: false,
		   	width:500,
		   	height: 400,
		   	drag: false,
		   	lock:false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_card",
		   	close: function(){
		   		/*var pdata = cardData.content.submit();
				if(pdata){
					$("#cd_codes").val(pdata.cd_codes);
					$("#cd_money").val(pdata.cd_money);
					$("#cd_rate").val(pdata.cd_rate);
					$("#sh_cd_money").val(pdata.sh_cd_money);
					THISPAGE.buildCash();
				}
				*/
		   		setTimeout("api.zindex()",200);
		    },
		    ok:function(){
		    	var pdata = cardData.content.submit();
				if (!pdata) {
					return false;
				}else{
					$("#cd_ids").val(pdata.cd_ids);
					$("#cd_money").val(pdata.cd_money);
					$("#cd_rate").val(pdata.cd_rate);
					$("#sh_cd_money").val(pdata.sh_cd_money);
					THISPAGE.buildCash();
				}
				setTimeout("api.zindex()",200);
		    },
		    cancel:true
	   });
	},
	doVip:function(){
		pointData = W.$.dialog({ 
		   	title: '会员用积分',
		   	max: false,
		   	min: false,
		   	width:280,
		   	height: 300,
		   	drag: false,
		   	lock:false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"vip/member/to_vip_point?vip_id="+vip_id,
		   	close: function(){
		   		var pdata = pointData.content.submit();
				if(null != pdata.vip_point && "" != pdata.vip_point && 0 != pdata.vip_point){
					$("#vip_point").val(pdata.vip_point);
					$("#sh_point_money").val(pdata.sh_point_money);
					THISPAGE.buildCash();
					if(pdata.isprint == 1){
						setTimeout(function(){handle.printPoint(pdata);},200);
					}
				}
				setTimeout("api.zindex()",200);
		    },
		    ok:false,
		    cancel:false
	   });
	},
	vadiDouble:function(obj) {
		var data = $(obj).val();
		if(!erg.test(data)){
			$(obj).val(0).select();
			return false;
		}
		return true;
	},
	printPoint:function(pdata){
		var footer = "<table><tr><td align='center'>&nbsp;</td></tr></table>";
		LODOP.SET_LICENSES(config.PRINT_NAME,config.PRINT_KEY,"","");
		LODOP.SET_PRINT_STYLE("FontSize","10");
		LODOP.SET_PRINT_PAGESIZE(3,"76mm",'2mm', '');
		LODOP.ADD_PRINT_TEXT("3mm",'2mm','76mm','15mm','会员积分抵现单');
		LODOP.ADD_PRINT_TEXT("9mm",'2mm','76mm','15mm','------------------------------');
		LODOP.ADD_PRINT_TEXT("15mm",'2mm','76mm','15mm','卡号：'+pdata.vm_cardcode);
		LODOP.ADD_PRINT_TEXT("21mm",'2mm','76mm','15mm','姓名：'+pdata.vm_name);
		LODOP.ADD_PRINT_TEXT("27mm",'2mm','76mm','15mm','积分：'+pdata.vm_points);
		LODOP.ADD_PRINT_TEXT("33mm",'2mm','76mm','15mm','使用：'+pdata.vip_point);
		LODOP.ADD_PRINT_TEXT("39mm",'2mm','76mm','15mm','剩余：'+pdata.vm_last_point);
		LODOP.ADD_PRINT_TEXT("45mm",'2mm','76mm','15mm','抵现：'+pdata.sh_point_money);
		LODOP.ADD_PRINT_TEXT("57mm",'2mm','76mm','15mm','------------------------------');
		LODOP.ADD_PRINT_TEXT("63mm",'2mm','76mm','15mm','收银人员：'+system.EM_NAME);
		LODOP.ADD_PRINT_TEXT("69mm",'2mm','76mm','15mm','兑换时间：'+config.TODAY);
		LODOP.ADD_PRINT_TEXT("75mm",'2mm','76mm','15mm','兑换时间：'+system.SHOP_NAME);
		LODOP.ADD_PRINT_HTML("84mm", "2mm",'100%',"100%", footer);
		LODOP.PRINT();
//		LODOP.PREVIEW();
	}
};
var THISPAGE = {
	init:function(){
		this.initCom();
		this.initParam();
		this.initMoney();
		this.initEvent();
	},
	initParam:function(){
		
	},
	initCom:function(){
		$("#sell_type").val(sell_type);
		if(null != sd_number && "" != sd_number){
			if(null != sd_deposit && parseFloat(sd_deposit) > 0){
				$("#sh_sd_money").val(sd_deposit);
				$("#sd_number").val(sd_number);
			}
		}
		$("#sh_lost_money").select().focus();
	},
	initMoney:function(){
		$("#sell_money").val(total_money);
		var sh_sd_money = $("#sh_sd_money").val();
		var sh_cash = (total_money-sh_sd_money);
		$("#sh_cash").val(sh_cash);
	},
	vadiMoney:function(sell_money,sh_cash){
		var sh_lost_money = $("#sh_lost_money").val();
		var sh_bank_money = $("#sh_bank_money").val();
		var sh_mall_money = $("#sh_mall_money").val();
		var sh_vc_money = $("#sh_vc_money").val();
		var sh_ec_money = $("#sh_ec_money").val();
//		var sh_sc_money = $("#sh_sc_money").val();
		var sh_third_money = $("#sh_third_money").val();
		var sh_cd_money = $("#sh_cd_money").val();
		var sh_point_money = $("#sh_point_money").val();
		var sh_wx_money = $("#sh_wx_money").val();
		var sh_ali_money = $("#sh_ali_money").val();
		var sh_sd_money = 0;
		if(!erg.test(sh_cash)){
			sh_cash = 0;
			$("#sh_cash").val(0).focus().select();
		}
		if(null != sd_number && "" != sd_number){
			if(erg.test(sd_deposit)){
				sh_sd_money = sd_deposit;
			}
		}
		if(!erg.test(sh_lost_money)){
			sh_lost_money = 0;
			$("#sh_lost_money").val(0).focus().select();
		}else{
			if (Math.abs(sh_lost_money) > Math.abs(sell_money)){
				sh_lost_money = 0;
				W.Public.tips({type: 1, content : "抹零不能大于商品总额"});
				$("#sh_lost_money").val(0).focus().select();
				setTimeout("api.zindex()",300);
			}
			if (Math.abs(sh_lost_money) > Math.abs(erase)){
				sh_lost_money = 0;
				W.Public.tips({type: 1, content : "抹零不能超过"+erase});
				$("#sh_lost_money").val(0).focus().select();
				setTimeout("api.zindex()",300);
			}
		}
		if(!erg.test(sh_bank_money)){
			sh_bank_money = 0;
			$("#sh_bank_money").val(0).focus().select();
		}else{
			if (Math.abs(sh_bank_money) > Math.abs(sell_money)){
				sh_bank_money = 0;
				W.Public.tips({type: 1, content : "不能大于商品总额"});
				$("#sh_bank_money").val(0).focus().select();
				setTimeout("api.zindex()",300);
			}
		}
		if(!erg.test(sh_mall_money)){
			sh_mall_money = 0;
			$("#sh_mall_money").val(0).focus().select();
		}else{
			if (Math.abs(sh_mall_money) > Math.abs(sell_money)){
				sh_mall_money = 0;
				W.Public.tips({type: 1, content : "不能大于商品总额"});
				$("#sh_mall_money").val(0).focus().select();
				setTimeout("api.zindex()",300);
			}
		}
		
		var temp_money = 0;
		temp_money += parseFloat(sh_lost_money) + parseFloat(sh_bank_money);
		temp_money += parseFloat(sh_mall_money) + parseFloat(sh_vc_money);
		temp_money += parseFloat(sh_sd_money) + parseFloat(sh_ec_money);
		temp_money += parseFloat(sh_cd_money) + parseFloat(sh_point_money);
		temp_money += parseFloat(sh_wx_money) + parseFloat(sh_ali_money);
		temp_money += parseFloat(sh_third_money);
		return temp_money;
	},
	buildCash:function(){
		var sell_money = $("#sell_money").val();
		var sh_cash = $("#sh_cash").val();
		var temp_money = THISPAGE.vadiMoney(sell_money,sh_cash);
		var cash_money = parseFloat(sell_money-temp_money).toFixed(2);
		if(isNaN(cash_money)){
			cash_money = 0.0;
		}
		if(isNaN(temp_money)){
			temp_money = 0.0;
		}
		if(Math.abs(cash_money) > 0){
			$("#sh_cash").val(cash_money);
			var _money = parseFloat(sell_money-cash_money-temp_money).toFixed(2);
			$("#change_money").val(_money);
		}else{
			$("#sh_cash").val(0);
			$("#change_money").val(0);
		}
	},
	changeMoney:function(){
		var sell_money = $("#sell_money").val();
		var sh_cash = $("#sh_cash").val();
		var temp_money = THISPAGE.vadiMoney(sell_money,sh_cash);
		if(isNaN(temp_money)){
			temp_money = 0.0;
		}
		var change_money = parseFloat(parseFloat(sh_cash)+parseFloat(temp_money)-parseFloat(sell_money)).toFixed(2);
		if(isNaN(change_money)){
			change_money = 0.0;
		}
		$("#change_money").val(change_money);
	},
	initEvent:function(){
		$("#btn-save").on('click',function(e){
			handle.cashOut();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
		//使用代金券
		$("#use_voucher").on('click',function(){
			if(0 == sell_type){
				handle.doVoucher();
			}else{
				W.Public.tips({type: 1, content : "零售才能用"});
			}
		});
		//使用优惠券
		$("#use_ecoupon").on('click',function(){
			if(0 == sell_type){
				handle.doEcoupon();
			}else{
				W.Public.tips({type: 1, content : "零售才能用"});
			}
		});
		//使用收钱吧
		$("#use_third").on('click',function(){
			if(0 == sell_type){
				handle.doThird();
			}else{
				W.Public.tips({type: 1, content : "零售才能用"});
			}
		});
		//使用储值卡
		$("#use_card").on('click',function(){
			handle.doCard();
		});
		//使用会员积分
		$("#use_vip").on('click',function(){
			if(1 == isvip){
				handle.doVip();
			}else{
				W.Public.tips({type: 1, content : "无会员不能用积分"});
			}
		});
	}
};
THISPAGE.init();