var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
var queryurl = config.BASEPATH+'cash/pageBack';
var api = frameElement.api, W = api.opener;
var sell_type=api.data.sell_type,backIds = backIds || [],changeIds = changeIds || [];
var isnumber = api.data.isnumber,number = api.data.code;
var handle = {
	formatBack:function (val, opt, row) {
    	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
    	html_con += '<input class="btn_back" id="re_'+opt.rowId+'" name="re_'+opt.rowId+'" type="checkbox" value="'+opt.rowId+'"/>';
    	html_con += '</div>';
    	return html_con;
    },
    formatChange:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<input class="btn_change" id="ch_'+opt.rowId+'" name="ch_'+opt.rowId+'" type="checkbox" value="'+opt.rowId+'"/>';
		html_con += '</div>';
		return html_con;
	},
	styleFmatter:function(val,opt,row){
		var br_name = row.br_name;
		var style = row.cr_name+"/"+row.sz_name;
		if(null != br_name && "null" != br_name && "" != br_name){
			style += "/"+br_name;
		}
		return style;
	},
	buildJson:function(grid,amount){
		var pdata = {};
		pdata.sht_sub_code= grid.sht_sub_code;
		pdata.sht_pd_code= grid.sht_pd_code;
		pdata.sht_ishand= grid.sht_ishand;
		pdata.sht_isvip= grid.sht_isvip;
		pdata.sht_ispoint= grid.sht_ispoint;
		pdata.sht_isgift= grid.sht_isgift;
		pdata.sht_cost_price= grid.sht_cost_price;
		pdata.sht_upcost_price= grid.sht_upcost_price;
		pdata.sht_vip_price= grid.sht_vip_price;
		pdata.sht_bd_code= grid.sht_bd_code;
		pdata.sht_tp_code= grid.sht_tp_code;
		pdata.sht_cr_code=grid.sht_cr_code;
		pdata.sht_sz_code= grid.sht_sz_code;
		pdata.sht_br_code= grid.sht_br_code;
		pdata.sht_sell_price= grid.sht_sell_price;
		pdata.sht_sign_price= grid.sht_sign_price;
		pdata.sht_amount=amount;
		pdata.pd_no=grid.pd_no;
		pdata.pd_name=grid.pd_name;
		pdata.cr_name=grid.cr_name;
		pdata.sz_name=grid.sz_name;
		pdata.br_name=grid.br_name;
		return pdata;
	},
	buildData:function(){
		var dataArr = [];
		var grids = $("#grid");
		$(".btn_back").each(function(){
			if($(this)[0].checked){
				var grid = grids.jqGrid("getRowData", $(this).val());
				dataArr.push(handle.buildJson(grid,-1));
			}
		});
		$(".btn_change").each(function(){
			if($(this)[0].checked){
				var grid = grids.jqGrid("getRowData", $(this).val());
				dataArr.push(handle.buildJson(grid,1));
			}
		});
		return dataArr;
	},
	saveTemp:function(){
		var pdata = handle.buildData();
		if(undefined != pdata && null != pdata && pdata.length > 0){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/saveBackTemp",
				data:{"sell_type":sell_type,"data":JSON.stringify(pdata)},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						W.flag = true;
						setTimeout("api.close()",200);
					}else{
						W.flag = false;
						W.Public.tips({type:1,content:'商品添加失败!'});
					}
				}
			});
		}else{
			W.Public.tips({type:1,content:'未选择商品!'});
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		this.$_search = $('#pd_no');
		this.$_isquery = $('#isquery');
		this.$_isall = $('#isall');
		if(api.data != null){
			$(this.$_search).val(api.data.code);
		}
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'state_back',label:'退',index: 'state_back',width: 50, formatter: handle.formatBack,align:'center', sortable:false},
	    	{name: 'state_change',label:'换',index: 'state_change',width: 50, formatter: handle.formatChange,align:'center', sortable:false},
	    	{name: 'sht_sub_code',label:'子码',index: 'sht_sub_code',hidden:true},
	    	{name: 'sht_pd_code',label:'编号',index: 'sht_pd_code',hidden:true},
	    	{name: 'sht_ishand',label:'是否手动打折',index: 'sht_ishand',hidden:true},
	    	{name: 'sht_isvip',label:'是否会员打折',index: 'sht_isvip',hidden:true},
	    	{name: 'sht_ispoint',label:'是否会员积分',index: 'sht_ispoint',hidden:true},
	    	{name: 'sht_isgift',label:'是否赠品',index: 'sht_isgift',hidden:true},
	    	{name: 'sht_cost_price',label:'成本价',index: 'sht_cost_price',hidden:true},
	    	{name: 'sht_upcost_price',label:'总部成本价',index: 'sht_upcost_price',hidden:true},
	    	{name: 'sht_vip_price',label:'会员价',index: 'sht_vip_price',hidden:true},
	    	{name: 'sht_bd_code',label:'品牌',index: 'sht_bd_code',hidden:true},
	    	{name: 'sht_tp_code',label:'类别',index: 'sht_tp_code',hidden:true},
	    	{name: 'sht_cr_code',label:'颜色',index: 'sht_cr_code',hidden:true},
	    	{name: 'sht_sz_code',label:'尺码',index: 'sht_sz_code',hidden:true},
	    	{name: 'sht_br_code',label:'怀型',index: 'sht_br_code',hidden:true},
	    	{name: 'cr_name',label:'颜色',index: 'cr_name',hidden:true},
	    	{name: 'sz_name',label:'尺码',index: 'sz_name',hidden:true},
	    	{name: 'br_name',label:'怀型',index: 'br_name',hidden:true},
	    	{name: 'pd_no',label:'货号',index: 'pd_no',width:100},
	    	{name: 'pd_name',label:'名称',index: 'pd_name',width:140},
	    	{name: 'style',label:'规格',index: 'style',width:140,formatter:handle.styleFmatter},
	    	{name: 'pd_unit',label:'单位',index: 'pd_unit',align:'center', width:85},
	    	{name: 'sht_sell_price',label:'零售价',index: 'sht_sell_price',align:'right',width:85},
	    	{name: 'sht_sign_price',label:'标牌价',index: 'sht_sign_price',align:'right',width:85},
	    	{name: 'sht_amount',label:'数量',index: 'sht_amount',align:'right',width:85}
	    ];
		$('#grid').jqGrid({
			url:queryurl + '?' +THISPAGE.buildParam(),
			datatype: 'json',
			width: 730,
			height: 330,
			altRows:true,
	        colModel: colModel,
	        rownumbers:true,
            rowNum:  config.BASEROWNUM,
	        rowList: config.BASEROWLIST,
	        multiselect:false,
	        gridview:true,
	        pager: '#page',
	        shrinkToFit:true,
	        forceFit:false,
	        viewrecords: true,
            pgbuttons: false,
            scroll: 1,
            recordtext:'{0} - {1} 共 {2} 条',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
                userdata:'data.data',
                repeatitems : false,
				id: 'sht_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParam:function(){
		var code = $.trim(this.$_search.val());
		if(null == code || "" == code){
			W.Public.tips({type: 2, content : "请输入货号!"});
			return;
		}
		var isquery = this.$_isquery.val();
		var isall = this.$_isall.val();
		var params = 'code='+Public.encodeURI(code);
		params += "&isall="+isall;
		params += "&isquery="+isquery;
		params += "&isnumber="+isnumber;
		params += "&sell_type="+sell_type;
		return params;
	},
	reset:function(){
		$("#pd_no").val("");
	},
	reloadData:function(){
		var code = $.trim(this.$_search.val());
		if(null == code || "" == code){
			W.Public.tips({type: 2, content : "请输入货号!"});
			return;
		}
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+THISPAGE.buildParam()}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$("#pd_no").keyup(function(e){
			e.preventDefault();
			if(e.keyCode ==13){
				$('#btn-search').click();
			}
		});
		$('#sumbit_ok').on('click', function(e){
			e.preventDefault();
			handle.saveTemp();
		});
		$('#btn_close').on('click', function(e){
			e.preventDefault();
			api.close();
		});
	}
};
THISPAGE.init();