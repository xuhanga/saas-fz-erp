var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
$(function(){
	$("#vc_cardcode").focus();
	THISPAGE.init();
});
var UTIL = {
	operFormater:function(val, opt, row){
    	var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-dele" title="删除">&#xe60e;</i>';
		html_con += '</div>';
    	return html_con;
	}
};
var THISPAGE = {
	init:function(){
		this.initGrid();
		this.addEvent();
	},
	initGrid:function(){
		var colModel = [
            {name: 'opera', index: '',label:'操作',width:50, title: false,formatter: UTIL.operFormater},
            {name: 'vc_id', index: 'vc_id',label:'ID',hidden:true},
	    	{name: 'vc_cardcode', index: 'vc_cardcode',label:'券号', width:185, title: false},	    
	    	{name: 'vc_money', index: 'vc_money',label:'余额', width:80,align:'right',title: false},
	    	{name: 'vc_used_money', index:'vc_used_money',label:'使用金额', width:80,align:'right', title: false,editable:true},
	    	{name: 'vc_cashrate', index:'vc_cashrate',label:'现金率',hidden:true, title: false}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: 465,
			height:200,
			gridview: true,
			colModel: colModel,
			rownumbers: true,
			multiselect:false,
			viewrecords: true,
			rowNum:999,
			shrinkToFit:false,
			autoScroll: true,
			footerrow :true,
			userDataOnFooter : true,
			cellEdit: true,
			cellsubmit: 'clientArray',
			jsonReader: {
				repeatitems : false,
				userData:"userData",
				id:'vc_id'
			},
			loadComplete: function(data){
			},
			gridComplete:function(data){
				THISPAGE.buildMoney();
			},
			beforeSaveCell:function(rowid, cellname, value, iRow, iCol){		
				var vc_money = $("#grid").jqGrid("getRowData", rowid).vc_money;
				if(parseFloat(value)<=parseFloat(vc_money)){
					return value;
				}else{
					W.RXDialog.commonTip('manyMoney','使用金额超过余额!','320','60','1');
					setTimeout("api.zindex()",500);
					return vc_money;
				}
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				THISPAGE.buildMoney();
			}
	    });
	},
	buildMoney:function(){
		var grid=$('#grid'),data=grid.getRowData();
		var totalMoney = 0;
		for(var i=0;i<data.length;i++){
			totalMoney += parseFloat(data[i].vc_used_money);
		}
		var userData = {};
		userData.vc_cardcode = "合计";
		userData.vc_used_money=totalMoney;
		$("#grid").footerData("set", userData);
	},
	addEvent:function(){
		$("#vc_cardcode").keyup(function(event){
			if(event.keyCode== 13){
				THISPAGE.query();
			}
		});
		$("#vc_usemoney").keyup(function(event){
			if(event.keyCode== 13){
				THISPAGE.add();
			}
		});
		$('#grid').on('click', '.operating .ui-icon-dele', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			THISPAGE.del(id);
		});
		$("#btn-save").click(function(){
			THISPAGE.submit();
		});
	},
	query:function(){
		var card_code = $("#vc_cardcode").val();
		if(null != card_code && "" != card_code){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"voucher/queryByCode",
				data:"vc_cardcode="+card_code,
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						var pdata = data.data;
						if(pdata.vc_money != undefined && pdata.vc_money != null){
							$("#vc_money").val(pdata.vc_money);
							$("#vc_cashrate").val(pdata.vc_cashrate);
							$("#vc_id").val(pdata.vc_id);
							$("#used_code").val(card_code);
							$("#vc_usemoney").focus().select();
						}else{
							W.Public.tips({type: 2, content : "代金券异常!"});
							setTimeout("api.zindex()",500);
							$("#used_code").val("");
							$("#vc_id").val("");
							$("#vc_cardcode").focus().select();
							return;
						}
					}else{
						W.Public.tips({type: 2, content : "代金券异常!"});
						setTimeout("api.zindex()",500);
						$("#vc_cardcode").focus().select();
						$("#used_code").val("");
						$("#vc_id").val("");
						return;
					}
					
				}
			});
		}else{
			W.Public.tips({type: 2, content : "代金券异常!"});
			setTimeout("api.zindex()",500);
			return;
		}
	},
	flush:function(){
		$("#vc_money").val("");
		$("#vc_usemoney").val("");
		$("#vc_id").val("");
		$("#vc_cardcode").val("").focus();
	},
	del:function(rowId){
		$('#grid').jqGrid('delRowData', rowId);
	},
	add:function(){
		var cardData = $("#grid").getRowData();
		if(cardData.length == 5){
			W.Public.tips({type: 2, content : "同时只能用5张券!"});
			setTimeout("api.zindex()",500);
			return;
		}
		var vc_id = $("#vc_id").val();
		var vc_cardcode = $.trim($("#vc_cardcode").val()); 
		var vc_usemoney = $("#vc_usemoney").val();
		var vc_money = $.trim($("#vc_money").val());
		var vc_cashrate = $("#vc_cashrate").val();
		if (vc_cardcode.length == 0 ){
			W.Public.tips({type: 2, content : "券号不能为空!"});
			setTimeout("api.zindex()",500);
			$("#vc_cardcode").focus().select();
	        return;
		}
		if(vc_money == null || vc_money == ""){
			W.Public.tips({type: 2, content : "回车查询余额!"});
			setTimeout("api.zindex()",500);
			$("#vc_cardcode").focus().select();
			return;
		}
		if (!config.REG_DOUBLE.test(vc_usemoney)){
			W.Public.tips({type: 2, content : "余额输入错误!"});
			setTimeout("api.zindex()",500);
		    $("#vc_usemoney").focus().select();
	        return;
		}
		if(isNaN(vc_usemoney)){
			W.Public.tips({type: 2, content : "请输入余额!"});
			setTimeout("api.zindex()",500);
	        $("#vc_usemoney").focus().select();
	        return;
	    }
		if (parseFloat(vc_money) < parseFloat(vc_usemoney)){
			W.Public.tips({type: 2, content : "余额不足!"});
			setTimeout("api.zindex()",500);
		    $("#vc_usemoney").focus().select();
	        return;
		}
		for(var i=0;i<cardData.length;i++){
			var _vc_cardcode = cardData[i].vc_cardcode;
			if(_vc_cardcode == vc_cardcode){
				W.Public.tips({type: 2, content : "同一张券不能重复使用!"});
				setTimeout("api.zindex()",500);
	      		$("#vc_cardcode").focus().select();
				return;
			}
		}
		var cardjson = {vc_id:vc_id,vc_cardcode:vc_cardcode,vc_money:vc_money,vc_used_money:vc_usemoney,vc_cashrate:vc_cashrate};
		$("#grid").jqGrid('addRowData', cardjson.vc_id, cardjson, 'first');
		THISPAGE.flush();
		$("#used_code").val("");
	}
};
function submit(){
	var grid=$('#grid'),rows=grid.getRowData();
	var totalMoney = 0,cardcodes="",ids="",vc_money="",cashrate = 0,total_cash = 0;
	if(null != rows && rows.length > 0){
		for(var i=0;i<rows.length;i++){
			totalMoney = parseFloat(parseFloat(totalMoney)+parseFloat(rows[i].vc_used_money)).toFixed(2);
			total_cash = parseFloat(parseFloat(total_cash)+parseFloat(rows[i].vc_used_money)*parseFloat(rows[i].vc_cashrate)).toFixed(2);
			cardcodes += rows[i].vc_cardcode+",";
			vc_money += rows[i].vc_used_money+",";
			ids += rows[i].vc_id+",";
		}
		cashrate = parseFloat(total_cash/totalMoney).toFixed(2);
		var pdata={};
		pdata.vc_codes=cardcodes;
		pdata.vc_money=vc_money;
		pdata.vc_rate=cashrate;
		pdata.sh_vc_money=totalMoney;
		pdata.vc_ids = ids;
		return pdata;
	}else{
		W.Public.tips({type: 2, content : "代金券未加入使用列表，请加入!"});
		return false;
	}
}