var config = parent.CONFIG,system=parent.SYSTEM;
var isRefresh = false;
var queryurl = config.BASEPATH+'sell/gift/page';
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	operate: function(oper, id){//修改、新增
		var height = 300;
		var width = 538;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"voucher/to_add";
			data = {oper: oper, callback:this.callback};
		}
		W.$.dialog({
			id:"twoPageID",
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			drag: false,
			lock: false,
			close:function(){
				try{setTimeout("api.zindex()",300);}catch(e){}
			}
		});
	},
    callback: function(data, oper, dialogWin){
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	formatType:function(val, opt, row){
		if(null != row.br_name && "" != row.br_name){
			return row.cr_name+"/"+row.sz_name+"/"+row.br_name;
		}else{
			return row.cr_name+"/"+row.sz_name;
		}
	},
	formatAmount:function(val, opt, row){
		if(null != row.gil_totalamount && "" != row.gil_totalamount){
			return row.gil_totalamount-row.gil_getamount;
		}
	},
	doVip:function(){
		var vip_code = $("#vm_cardcode").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/vipByCode",
			data:{"cardcode":Public.encodeURI(vip_code)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					$("#vm_name").val(data.data.vm_name);
					$("#vm_id").val(data.data.vm_id);
					$("#vm_point").val(data.data.vm_points);
					$("#vm_code").val(data.data.vm_code);
				}else{
				}
			}
		});
	},
	changeGift:function(){
		var vm_point = $("#vm_point").val();
		var total_point = 0;
		var vm_id = $("#vm_id").val();
		var vm_code = $("#vm_code").val();
		var ids = $("#grid").jqGrid('getGridParam', 'selarrrow');
		var arr = [];
		if(null != ids && ids.length > 0 && vm_code != ""){
			for(key in ids){
				var id = ids[key];
				var amount = $("#grid").jqGrid("getRowData", id).gil_amount;
				var point = $("#grid").jqGrid("getRowData", id).gi_point;
				total_point += parseFloat(point*amount);
				var json = {"gil_id":id,"gil_amount":amount};
				arr.push(json);
			}
			alert(total_point+":"+vm_code);
			if(vm_point >= total_point){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+"sell/gift/send",
					data:{"vm_id":vm_id,"vm_code":vm_code,"point":total_point,"json":JSON.stringify(arr)},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							api.close();
						}else{
							W.Public.tips({type: 2, content : "兑换失败，请联系管理员!"});
						}
					}
				});
			}else{
				W.Public.tips({type: 2, content : "剩余积分不足!"});
				return;
			}
		}else{
			W.Public.tips({type: 2, content : "请选择兑换的礼品!"});
			return;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		$("#vm_mobile").focus();
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'gil_amount',label:'数量',index: 'gil_amount',align:'center', width:60,editable:true},
	    	{name: 'pd_name',label:'名称',index: 'pd_name',width:120},
	    	{name: 'cr_name',label:'颜色',index: 'cr_name',hidden:true},
	    	{name: 'sz_name',label:'颜色',index: 'sz_name',hidden:true},
	    	{name: 'br_name',label:'颜色',index: 'br_name',hidden:true},
	    	{name: 'gil_totalamount',label:'总数量',index: 'gil_totalamount',hidden:true},
	    	{name: 'gil_getamount',label:'已领取',index: 'gil_getamount',hidden:true},
	    	{name: 'pd_style',label:'规格',index: 'pd_style',width:120,formatter:handle.formatType},
	    	{name: 'gi_point',label:'需要积分',index: 'gi_point',width:80},
	    	{name: 'gil_lastamount',label:'剩余数量',index: 'gil_lastamount',width:80,formatter:handle.formatAmount},
	    	{name: 'gi_enddate',label:'截至日期',index: 'gi_enddate',width:120}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			datatype: 'json',
			width: 800,
			height: 350,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pgbuttons:false,
			recordtext:'{0} - {1} 共 {2} 条',
			multiselect:true,//多选
			viewrecords: true,
			rowNum:15,
	        rowList:config.BASEROWLIST,
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
			scroll: 1,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
				repeatitems : false,
				id: 'gil_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		var code = $.trim($("#SearchContent").val());
		params += '&vc_cardcode='+Public.encodeURI(code);
		return params;
	},
	reset:function(){
		$("#vc_cardcode").val("");
	},
	reloadData:function(){
		var code = $.trim($("#SearchContent").val());
		if(null == code || '' == code){
			W.Public.tips({type: 2, content : "请输入卡号!"});
			return;
		}
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$("#vm_cardcode").keyup(function(event){
			event.preventDefault();
			if(event.keyCode == 13){
				handle.doVip();
			}
		});
		$("#btn-change").click(function(event){
			event.preventDefault();
			handle.changeGift();
		});
	}
};
THISPAGE.init();