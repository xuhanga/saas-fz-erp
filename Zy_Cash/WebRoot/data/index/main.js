var config = parent.CONFIG,system=parent.SYSTEM;
var height = $(parent).height()-236,width = $(parent).width()-280;
var paramData = {id:"FIRSTID",title:"",width:"",height:"",url:"",data:{}};
var stateData = [{id:"0",name:"零售"},{id:"1",name:"退货"},{id:"2",name:"换货"}];
var empData = empData || {},areaData = areaData || {};
var comboFlag = false,flag = false;//comboFlag 只用于刷新下拉框的设置，不能用于其他，flag 是选择商品后的刷新
var handle = {
	vipByMobile:function(){
		var mobile = $("#mobile").val();
		if(null != mobile && "" != mobile){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"vip/member/mobile",
				data:{"mobile":mobile},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						alert(JSON.stringify(data))
						var vipData = data.data;
						if(undefined != vipData){
							var vipInfo = "名称："+vipData.vip.vm_name+"  类别："+vipData.vip.mt_name+"  折扣："+vipData.vip.mt_discount;
							$("#vip_data").html(vipInfo);
						}else{
							$("#vip_data").html("无会员信息!");
						}
						if(undefined != vipData.card.cd_cardcode){
							$("#card_moeny").html("储值卡"+vipData.card.cardcode+" 余额："+vipData.card.card_moeny);
						}
					}else{
					}
				}
			});
		}
	},		
	doVip:function(){
		if(1 != system.VIP_QUERY){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
	    $.dialog({ 
		   	id: 'FIRSTID',
		   	title: '会员管理',
		   	data:{vipInfo:handle.buildVipInfo},
		   	max: false,
		   	min: false,
		   	width:965,
		   	height: 532,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"vip/member/to_list",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	buildVipInfo:function(data){
		if(undefined != data && null != data){
			var info = "姓名:"+data.vm_name;
			info += " 卡号:"+data.vm_cardcode;
			info += " 手机:"+data.vm_mobile;
			info += " 积分:"+data.vm_points;
			$("#vip_info").val(info);
			$("#vip_id").val(data.vm_id);
		}
	},
	doVipInfo:function(){
		var vip_id = $("#vip_id").val();
		if(null == vip_id || "" == vip_id){
			Public.tips({type: 2, content : "会员信息出错!"});
			return;
		}
		paramData.title = "会员分析";
		paramData.width = "765";
		paramData.height = "532";
		paramData.url = config.BASEPATH+"vip/member/to_info?vip_id="+vip_id;
		paramData.data={vip_id:vip_id};
	    Public.openDialog(paramData);
	},
	doVipRate:function(){
		var ids = $("#grid").jqGrid('getDataIDs');
		if(null == ids || "" == ids || ids.length == 0){
			Public.tips({type: 2, content : "请添加商品"});
			return;
		}
	    $.dialog({ 
		   	id: 'FIRSTID',
		   	title: '会员打折',
		   	data:{vipInfo:handle.buildVipInfo},
		   	max: false,
		   	min: false,
		   	width:300,
		   	height: 200,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_vip_rate",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	doHandRate:function(id){
		if(1 != system.HAND_RATE){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
		if(null == id || "" == id){
			id = $("#grid").jqGrid('getGridParam', 'selrow');
		}
		if(null == id || "" == id){
			var ids = $("#grid").jqGrid('getDataIDs');
			if(ids.length > 0){
				id = ids[0];
			}
		}
		if(null == id || "" == id){
			Public.tips({type: 2, content : "请添加商品"});
			return;
		}
		var pdata = $("#grid").jqGrid("getRowData", id);
		if(pdata.sht_ishand != 1){
			Public.tips({type:1,content:'商品设置为不打折!'});
			return;
		}
		if(null != pdata.sht_sale_model && "" != pdata.sht_sale_model){
			Public.tips({type:1,content:'促销商品不能打折!'});
			return;
		}
		if(null != pdata.sht_isgift && 1 == pdata.sht_isgift){
			Public.tips({type:1,content:'赠品不能打折!'});
			return;
		}
		
		var line = $('#' + id)[0].rowIndex;
		var cdata = {};
		cdata.line = line;
		cdata.sht_id=pdata.sht_id;
		cdata.pd_no=pdata.pd_no;
		cdata.sht_sale_model=pdata.sht_sale_model;
		cdata.sht_ishand=pdata.sht_ishand;
		cdata.sht_isgift=pdata.sht_isgift;
		cdata.sht_price=pdata.sht_price;
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '手动打折',
		   	data:{pdata:cdata},
		   	max: false,
		   	min: false,
		   	width:300,
		   	height: 280,
		   	drag: true,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_hand_rate",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	doAllRate:function(){
		if(1 != system.HAND_RATE){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
		var ids = $("#grid").jqGrid('getDataIDs');
		if(null == ids || "" == ids || ids.length == 0){
			Public.tips({type: 2, content : "请添加商品"});
			return;
		}
		var sell_money = 0;
		for(id in ids){
			var pdata = $("#grid").jqGrid("getRowData", ids[id]);
			sell_money += parseFloat(pdata.sht_sell_price)*parseFloat(pdata.sht_amount);
		}
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '全场打折',
		   	max: false,
		   	min: false,
		   	data:{"sell_money":parseFloat(sell_money).toFixed(2)},
		   	width:300,
		   	height: 200,
		   	drag: true,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_all_rate",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	doVoucher:function(){
		if(1 != system.VOUCHER_QUERY){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
		paramData.title = "代金券管理";
		paramData.width = "965";
		paramData.height = "532";
		paramData.url = config.BASEPATH+"voucher/to_list";
	    Public.openDialog(paramData);
	},
	doCard:function(){
		if(1 != system.CARD_QUERY){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
		paramData.title = "储值卡管理";
		paramData.width = "965";
		paramData.height = "532";
		paramData.url = config.BASEPATH+"cash/card/to_list";
	    Public.openDialog(paramData);
	},
	doVipPoint:function(){
		if(1 != system.VIP_POINT){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
		paramData.title = "积分管理";
		paramData.width = "285";
		paramData.height = "292";
		paramData.url = config.BASEPATH+"vip/member/to_point";
	    Public.openDialog(paramData);
	},
	changeGift:function(){
		paramData.title = "积分兑换";
		paramData.width = "830";
		paramData.height = "465";
		paramData.url = config.BASEPATH+"sell/gift/to_list";
	    Public.openDialog(paramData);
	},
	doShopService:function(){
		paramData.title = "售后服务";
		paramData.width = "885";
		paramData.height = "492";
		paramData.url = config.BASEPATH+"service/to_list";
	    Public.openDialog(paramData);
	},
	doShopProgram:function(){
		paramData.title = "搭配方案";
		paramData.width = "885";
		paramData.height = "492";
		paramData.url = config.BASEPATH+"shop/program/to_list";
	    Public.openDialog(paramData);
	},
	doCome:function(){
		var da_id = $("#da_id").val();
		var come = $("#da_come").text();
		$("#da_come").text(parseInt(come)+1);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'day/come',
			data:{"da_id":da_id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){		 		
				}else{
					Public.tips({type: 1, content : "删除失败"});
				}
			}
		});
	},
	doReceive:function(){
	    empDia = $.dialog({
			id:"FIRSTID",
			title : '选择接待人',
			content : 'url:'+config.BASEPATH+'cash/emp/to_emp_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: false,
			ok:function(){
				var selected = empDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = empDia.content.doSelect();
				if(selected){
					var da_id = $("#da_id").val();
					var receive = $("#da_receive").text();
					$("#da_receive").text(parseInt(receive)+1);
					$.ajax({
						type:"POST",
						url:config.BASEPATH+'day/receive',
						data:{"da_id":da_id,"em_code":selected.em_code},
						cache:false,
						dataType:"json",
						success:function(data){
							if(undefined != data && data.stat == 200){		 		
							}else{
								Public.tips({type: 1, content : "接待失败"});
							}
						}
					});
				}
			},
			cancel:true
		});
	},
	doTry:function(){
		empDia = $.dialog({
			id:"FIRSTID",
			title : '选择接待人',
			content : 'url:'+config.BASEPATH+'cash/emp/to_emp_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: false,
			ok:function(){
				var selected = empDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = empDia.content.doSelect();
				if(selected){
					var da_id = $("#da_id").val();
					var da_try = $("#da_try").text();
					$("#da_try").text(parseInt(da_try)+1);
					$.ajax({
						type:"POST",
						url:config.BASEPATH+'day/doTry',
						data:{"da_id":da_id,"em_code":selected.em_code},
						cache:false,
						dataType:"json",
						success:function(data){
							if(undefined != data && data.stat == 200){		 		
							}else{
								Public.tips({type: 1, content : "接待失败"});
							}
						}
					});
				}
			},
			cancel:true
		});
	},
	doEditPass:function(){
		if(1 != system.EDIT_PASS){
			Public.tips({type: 2, content : "无权限"});
			return;
		}
		paramData.title = "密码修改";
		paramData.width = "265";
		paramData.height = "232";
		paramData.url = config.BASEPATH+"index/to_pass";
	    Public.openDialog(paramData);
	},
	doWeather:function(){
		paramData.title = "今日天气";
		paramData.width = "265";
		paramData.height = "262";
		paramData.url = config.BASEPATH+"day/to_weather";
	    Public.openDialog(paramData);
	},
	doPrintSet:function(){
		paramData.title = "打印设置";
		paramData.width = "565";
		paramData.height = "500";
		paramData.url = config.BASEPATH+"cash/set/to_print";
	    Public.openDialog(paramData);
	},
	cashSet:function(){
		paramData.title = "收银设置";
		paramData.width = "660";
		paramData.height = "390";
		paramData.url = config.BASEPATH+"cash/set/to_cash";
	    Public.openDialog(paramData);
	},
	doAllocateSet:function(){
		paramData.title = "调拨设置";
		paramData.width = "565";
		paramData.height = "500";
		paramData.data = {from:"set"};
		paramData.url = config.BASEPATH+"set/print/to_list/19";
	    Public.openDialog(paramData);
	},
	del: function(rowIds){//删除
		if(rowIds != null && rowIds != ''){
		 	$.dialog.confirm('确定删除吗?', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'cash/delTemp',
					data:{"sht_id":rowIds},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){		 		
							$('#grid').jqGrid('delRowData', rowIds);
							THISPAGE.buildTotal();
						}else{
							Public.tips({type: 1, content : "删除失败"});
						}
					}
				});
			});
	   }else{
	      $.dialog.tips("请选择数据!!!",1,"32X32/fail.png");
	   }
	},
	updateArea:function(data,rowid){
		var line = $('#' + rowid)[0].rowIndex;
		if(1 == line){//是第一行，如果默认下面全选，则进行更新
			if(1 == system.SELECT_ALL){
				$('.ui-combo-wrap.area').each(function(){
					$(this).getCombo().selectByValue(data.code,false);
					$(this).next().val(data.name);
				});
			}
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/updateArea?"+Math.random(),
			data:{"line":line,sht_id:rowid,"da_code":data.code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}
			}
		});
	},
	updateEmp:function(data,rowid,type){
		var line = $('#' + rowid)[0].rowIndex;
		if(1 == line){//是第一行，如果默认下面全选，则进行更新
			if(1 == system.SELECT_ALL){
				$('.ui-combo-wrap.'+type).each(function(){
					$(this).getCombo().selectByValue(data.code,false);
					$(this).next().val(data.name);
				});
			}
		}
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/updateEmp?"+Math.random(),
			data:{"line":line,sht_id:rowid,"type":type,"em_code":data.code,"em_name":data.name},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}
			}
		});
	},
	doCash:function(){
		if(1 != system.CASHOUT){
			Public.tips({type:1,content:"无收银权限"});
			return;
		}
		var grid = $("#grid");
		var ids = grid.jqGrid("getDataIDs");
		if(null == ids || ids.length == 0){
			Public.tips({type: 1, content : "无数据，无法结算"});
			return;
		}
		var _main = 0,_salve = 0,_area = 0;
		if(1 == system.EMP_MAIN){
			$(".main").each(function(){
				var main = $(this).next().val();
				if(null == main || "null" == main || "" == main){
					_main = 1;
					return false;
				}
			});
			if(_main == 1){
				Public.tips({type: 1, content : "主导购必选!"});
				return;
			}
		}
		if(1 == system.EMP_SALVE){
			$(".salve").each(function(){
				var salve = $(this).next().val();
				if(null == salve || "null" == salve || "" == salve){
					_salve = 1;
					return false;
				}
			});
			if(_salve == 1){
				Public.tips({type: 1, content : "次导购必选!"});
				return;
			}
		}
		if(1 == system.AREA){
			$(".area").each(function(){
				var area = $(this).next().val();
				if(null == area || "null" == area || "" == area){
					_area = 1;
					return false;
				}
			});
			if(_area == 1){
				Public.tips({type: 1, content : "位置必选!"});
				return;
			}
		}
		$("#change_money").val(0);//弹出收银时，把上次记的找零初始化
		var total_money = $("#total_money").text();
		var sd_deposit = $("#sd_deposit").val();
		var sd_number = $("#sd_number").val();
		var sell_type = $("#sell_type").val();
		var vip_id = $("#vip_id").val();
		var pdata = {};
		pdata.total_money = total_money;
		pdata.sd_deposit = sd_deposit;
		pdata.sd_number = sd_number;
		pdata.sell_type = sell_type;
		pdata.vip_id=vip_id;
		if(null != vip_id && "" != vip_id){
			pdata.isvip = 1;
		}else{
			pdata.isvip = 0;
		}
		cashData = $.dialog({ 
		   	id: 'FIRSTID',
		   	title: '收银结算',
		   	data:pdata,
		   	max: false,
		   	min: false,
		   	width:570,
		   	height:480,
		   	drag: true,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_cash",
		   	close: function(){
		   		if (flag){
		   			var sh_id = $("#sh_id").val();
		   			var change_money = $("#change_money").val();
		   			if(null != sh_id && "" != sh_id){
		   				setTimeout(function(){
		   					handle.print(sh_id,change_money);
		   				},200);
		   			}
		   			THISPAGE.reset();
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	print:function(id,change_money){
		if(SYSTEM.PRINT == 1){
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"sell/print",
	            data: {sh_id:id,change_money:change_money},
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
	            		var unit = "mm";
						var html = data.data.html;
						var page_height = data.data.page_height+unit;
						var page_width = data.data.page_width+unit;
						var page_title = data.data.page_title;
						var sh_number = data.data.number;
						LODOP.SET_LICENSES(config.PRINT_NAME,config.PRINT_KEY,"","");
						LODOP.PRINT_INIT(page_title);
						LODOP.SET_PRINT_PAGESIZE(0,page_width,page_height,"");
						LODOP.ADD_PRINT_BARCODE("5mm","5mm","60mm","10mm","128Auto",sh_number);
						LODOP.SET_PRINT_STYLEA(0,"ShowBarText",1);
						LODOP.ADD_PRINT_HTM("20mm","0mm","100%","100%",html);
						LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","Auto-Width");//整宽不变形
						LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED", 1);
						LODOP.SET_PRINT_COPIES(data.data.print_times);//打印份数
						if(SYSTEM.AUTO_PRINT == 1){
							LODOP.PRINT();
						}else{
							LODOP.PREVIEW();
						}
					}
	            }
	        });
		}
	},
	doSellList:function(){
		paramData.title = "零售查询";
		paramData.url = config.BASEPATH+"sell/to_list";
		paramData.data = empData;
		Public.openMax(paramData);
	},
	doDayEnd:function(){
		if(1 != system.DAYEND){
			Public.tips({type:1,content:"无交班权限"});
			return;
		}
		var grid = $("#grid");
		var ids = grid.jqGrid("getDataIDs");
		if(null != ids && ids.length > 0){
			Public.tips({type: 1, content : "有零售数据，无法交班"});
			return;
		}
		paramData.title = "交班处理";
		paramData.width = "560";
		paramData.height = "500";
		paramData.url = config.BASEPATH+"dayend/to_dayend";
	    Public.openDialog(paramData);
	},
	doAllocate:function(){
		paramData.title = "前台调拨";
		paramData.url = config.BASEPATH+"sell/allocate/to_list";
		paramData.data = empData;
		Public.openMax(paramData);
	},
	doWantReceive:function(){
		paramData.title = "收货管理";
		paramData.url = config.BASEPATH+"shop/want/to_list/0";
		paramData.data = empData;
		Public.openMax(paramData);
	},
	doMoneyAccess:function(){
		paramData.title = "银行收支";
		paramData.url = config.BASEPATH+"money/access/to_list";
		paramData.data = empData;
		Public.openMax(paramData);
	},
	doShopPlan:function(){
		paramData.title = "智能计划";
		paramData.width = "600";
		paramData.height = "320";
		paramData.url = config.BASEPATH+"shop/plan/to_progress_chart";
	    Public.openDialog(paramData);
	},
	doShopKpiAssess:function(){
		paramData.title = "KPI考核";
		paramData.width = "800";
		paramData.height = "540";
		paramData.url = config.BASEPATH+"shop/kpiassess/to_list";
		Public.openDialog(paramData);
	},
	doShopKpiPk:function(){
		paramData.title = "PK工具";
		paramData.width = "900";
		paramData.height = "540";
		paramData.url = config.BASEPATH+"shop/kpipk/to_list";
		Public.openDialog(paramData);
	},
	doShopTarget:function(){
		paramData.title = "目标管理";
		paramData.width = "900";
		paramData.height = "540";
		paramData.url = config.BASEPATH+"shop/target/to_list";
		Public.openDialog(paramData);
	}
};
var formatUtil = {
	operFmatter:function (val, opt, row) {
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-img" title="图片">&#xe654;</i><i class="iconfont i-hand ui-icon-dele" title="删除">&#xe60e;</i>';
		html_con += '</div>';
		return html_con;
	},
	styleFmatter:function(val,opt,row){
		var br_name = row.br_name;
		var style = row.cr_name+"/"+row.sz_name;
		if(null != br_name && "null" != br_name && "" != br_name){
			style += "/"+br_name;
		}
		return style;
	},
	typeFmatter:function(val,opt,row){
		var sell_type = row.sht_state;
		var type = "售",color = "";
		if(sell_type == 1){
			type="退";
			color = "red";
		}
		if(sell_type == 2){
			if(row.sht_amount < 0){
				type = "退";
				color = "red";
			}else{
				type="换";
			}
		}
		if(null != row.sht_sale_model && row.sht_sale_model != ''){
			type="促";	
			color = "blue";
		}
		if(row.sht_isgift == 1){
			type="赠";
			color = "red";
		}
		if(row.sht_isgift == 2){
			type="送";
			color = "red";
		}
		return "<span style='color:"+color+";'>"+type+"</span>";
	},
	formatMain:function(val, opt, row){
		var html = '<span class="ui-combo-wrap main" id="'+row.sht_id+'"></span><input type="hidden" id="main_'+row.sht_id+'" value="'+row.main_name+'"/>';
		return html;
	},
	formatSlave:function(val, opt, row){
		var html = '<span class="ui-combo-wrap slave" id="'+row.sht_id+'"></span><input type="hidden" id="slave_'+row.sht_id+'" value="'+row.slave_name+'"/>';
		return html;
	},
	formatArea:function(val, opt, row){
		var html = '<span class="ui-combo-wrap area" id="'+row.sht_id+'"></span><input type="hidden" id="area_'+row.sht_id+'" value="'+row.da_name+'"/>';
		return html;
	},
	vipRate:function(row){
		var type = row.sht_viptype,_value = row.sht_vipvalue,title="";
		if(null != type && "" != type || "0" != type){
			if(type == "4"){
				title="会员价"+_value;
			}else{
				title="会员折"+_value;
			}
			return title+" ";
		}else{
			return "";
		}
	},
	handRate:function(row){
		var type = row.sht_handtype,_value = row.sht_handvalue,title="";
		if(null != type && "" != type || "0" != type){
			if(type == "1"){
				title="手动折"+_value;
			}
			if(type == "2"){
				title="让利"+_value;
			}
			if(type == "3"){
				title="实际金额"+_value;
			}
			return title+" ";
		}else{
			return "";
		}
	},
	remark:function(val, opt, row){
		var saleRate = row.sht_sale_money;
		var info = "";
		info += formatUtil.vipRate(row);
		info += formatUtil.handRate(row);
		if(null != saleRate && "" != saleRate){
			info += "促销让利"+saleRate;
		}
		return info;
	}
};
var productUtil = {
	doProduct:function(){
		var sell_type = $("#sell_type").val();
		var pd_no = $.trim($("#pd_no").val());
		var size = _self.$_open_type.chkVal().join()?1:0;
		if(pd_no == null || "" == pd_no){
			Public.tips({type: 1, content : "请输入货号!"});
			setTimeout(function(){
				$("#pd_no").focus();
			},300);
			return;
		}
		if(sell_type == 0){
			if(size == 1){
				productUtil.sellSize(pd_no,sell_type);
			}else{
				productUtil.sellByCode(pd_no,sell_type);
			}
		}else if(sell_type == 1){
			if(size == 1){
				productUtil.sellSize(pd_no,sell_type);
			}else{
				productUtil.backByCode(pd_no,sell_type);
			}
		}else{
			productUtil.changeByCode(pd_no,sell_type);
		}
	},
	sellSize:function(pd_no,sell_type){
		var random = Math.random();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/product?"+random,
			data:{"pd_no":pd_no,"sell_type":sell_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					$.dialog({
					   	id: 'FIRSTID',
					   	title: '商品尺码查询',
					   	data:{product:data.data.product,sell_type:sell_type},
					   	max: false,
					   	min: false,
					   	width:765,
					   	height: 532,
					   	drag: false,
					   	resize:false,
					   	content:"url:"+config.BASEPATH+"cash/to_size",
					   	close: function(){
					   		if (flag){
					        	THISPAGE.loadData();
					        	flag = false;
					        }
					    }
				   });
				}else if(data.stat == 404){
					Public.tips({type: 1, content :"货号不存在"});
				}else{
					Public.tips({type: 1, content :"查询失败"});
				}
			}
		});
		
	},
	sellAll:function(pd_no,sell_type){
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '商品零售查询',
		   	data:{code:pd_no,sell_type:sell_type},
		   	max: false,
		   	min: false,
		   	width:765,
		   	height: 532,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_product_sell",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	sellByCode:function(pd_no,sell_type){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/subCode?"+Math.random(),
			data:{"barcode":pd_no,"sell_type":sell_type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					THISPAGE.loadData();
				}else if(data.stat == 404){
					productUtil.sellAll(pd_no,sell_type);
				}
			}
		});
	},
	backByCode:function(code,type){
		if(1 != system.ISBACK){
			Public.tips({type:1,content:"无退货权限"});
			return;
		}
		//先查询是不是已售单据编号
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/queryNumber?"+Math.random(),
			data:{"code":code,"sell_type":type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					if(data.data == 1){//是条码，则直接刷新表单，后台已经插入临时表
						THISPAGE.loadData();
					}else{
						productUtil.backNumber(code,type);
					}
				}else if(data.stat == 404){
					if(system.KEY_BACK_NUMBER == 1){
						Public.tips({type:1,content:"权限设置只能小票退货!"});
						return;
					}else{
						productUtil.backAll(code,type);
					}
				}
			}
		});
	},
	backAll:function(code,type){
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '商品零售查询',
		   	data:{"isnumber":0,"code":code,"sell_type":type},
		   	max: false,
		   	min: false,
		   	width:765,
		   	height: 532,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_product_back",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	backNumber:function(number,type){
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '商品退货查询',
		   	data:{"isnumber":2,"code":number,"sell_type":type},
		   	max: false,
		   	min: false,
		   	width:765,
		   	height: 532,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_product_back",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	changeByCode:function(code,type){
		if(1 != system.ISCHANGE){
			Public.tips({type:1,content:"无换货权限"});
			return;
		}
		//先查询是不是已售单据编号
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/queryNumber?"+Math.random(),
			data:{"code":code,"sell_type":type},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					productUtil.changeCode(data.data,code,type);
				}else if(data.stat == 404){
					productUtil.changeAll(code,type);
				}
			}
		});
	},
	changeCode:function(flag,number,type){
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '商品退货查询',
		   	data:{"isnumber":flag,"code":number,"sell_type":type},
		   	max: false,
		   	min: false,
		   	width:765,
		   	height: 532,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_product_change",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	changeAll:function(code,type){
		$.dialog({ 
		   	id: 'FIRSTID',
		   	title: '商品换货查询',
		   	data:{"isnumber":0,"code":code,"sell_type":type},
		   	max: false,
		   	min: false,
		   	width:765,
		   	height: 532,
		   	drag: false,
		   	resize:false,
		   	content:"url:"+config.BASEPATH+"cash/to_product_change",
		   	close: function(){
		   		if (flag){
		        	THISPAGE.loadData();
		        	flag = false;
		        }
		    }
	   });
	},
	updateTemp:function(id,val){
		var sell_type = $("#sell_type").val();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"cash/updateTemp?"+Math.random(),
			data:{"sht_id":id,"sell_type":sell_type,"sht_amount":val},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					THISPAGE.loadData();
				}else{
					Public.tips({type: 1, content : "修改失败!"});
				}
			}
		});
	},
	delById:function(id){
		$.dialog.confirm('确定删除?', function(){
			var sell_type = $("#sell_type").val();
			var grid = $("#grid");
			var pdata = grid.jqGrid("getRowData", id);
			var data = grid.getDataIDs();
			var size = data.length;
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/delTemp?"+Math.random(),
				data:{"sht_id":id,"sell_type":sell_type,"isgift":pdata.sht_isgift,"temp_size":size},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						if(size == 1){
							grid.jqGrid('delRowData', id);
							THISPAGE.reset();
						}else{
							THISPAGE.loadData();
						}
					}else{
						Public.tips({type: 1, content : "删除失败!"});
					}
				}
			});
		});
	},
	clearTemp:function(){
		$.dialog.confirm('确定清空数据?', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/clearTemp?"+Math.random(),
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						$("#grid").jqGrid('clearGridData');
						THISPAGE.reset();
					}else{
						Public.tips({type: 1, content : "清空失败!"});
					}
				}
			});
		});
	},
	doPutdown:function(code){
		$.dialog.confirm('提取挂单会清空当前数据，确定提取?', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/putDown?"+Math.random(),
				data:{"put_code":code},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						var sell_type = data.data.sell_type;
						THISPAGE.reset();//把原来数据清除
						handle.buildVipInfo(data.data.vip_info);//更改会员信息
//						typeCombo.loadData(stateData,['id',sell_type,0]);//更改销售状态
						THISPAGE.initType(sell_type);
						setTimeout(function(){
							THISPAGE.loadData();
							productUtil.buildPutdown(sell_type,code);//提取挂单更改挂单状态
						},200);
					}else{
						Public.tips({type: 1, content : "提取挂单失败!"});
					}
				}
			});
		});
	},
	doPutup:function(code){
		$.dialog.confirm('确定挂单?', function(){
			var sell_type = $("#sell_type").val();
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/putUp?"+Math.random(),
				data:{"put_code":code},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						$("#grid").jqGrid('clearGridData');
						THISPAGE.reset();
						productUtil.buildPutup(sell_type, code);
					}else{
						Public.tips({type: 1, content : "挂单失败!"});
					}
				}
			});
		});
	},
	buildPutup:function(state,number){
		var color = "bg_gray";
		if(1 == state){
			color = "bg_red";
		}
		if(2 == state){
			color = "bg_yellow";
		}
		$("#putup_"+number).addClass(color).removeClass("bg_white");
		$("#putup_"+number).prop("code",number);
	},
	buildPutdown:function(state,number){
		var color = "bg_gray";
		if(1 == state){
			color = "bg_red";
		}
		if(2 == state){
			color = "bg_yellow";
		}
		$("#putup_"+number).addClass("bg_white").removeClass(color);
		$("#putup_"+number).prop("code","");
	},
	payDeposit:function(){
		var sell_type = $("#sell_type").val();
		var ids = $("#grid").jqGrid("getDataIDs");
		if(null != ids && ids.length > 0){
			if(0 == sell_type){
				paramData.title = "预付订金";
				paramData.width = "300";
				paramData.height = "300";
				paramData.url = config.BASEPATH+"deposit/to_pay";
				paramData.data = {callback:this.callback};
				Public.openDialog(paramData);
			}else{
				Public.tips({type:1,content:"只有零售才能预付"});
				return;
			}
		}else{
			Public.tips({type:1,content:"无数据!"});
			return;
		}
	},
	getDeposit:function(){
		paramData.title = "提取订金";
		paramData.width = "640";
		paramData.height = "415";
		paramData.url = config.BASEPATH+"deposit/to_get";
		paramData.data = {callback:this.callback};
	    Public.openDialog(paramData);
	},
	doOtherStock:function(){
		paramData.title = "各店库存";
		paramData.width = "965";
		paramData.height = "500";
		paramData.url = config.BASEPATH+"stock/to_other";
	    Public.openDialog(paramData);
	},
	doProductStock:function(){
		paramData.title = "商品库存";
		paramData.width = "965";
		paramData.height = "500";
		paramData.url = config.BASEPATH+"stock/to_stock";
	    Public.openDialog(paramData);
	},
	callback:function(){
		if (flag){
        	THISPAGE.loadData();
        	flag = false;
        }
	}
};
var ComboUtil = {
	main:function(data){
		$('.ui-combo-wrap.main').each(function(){
			var defaultValue = $(this).next().val();
			var _self = this;
			$(_self).combo({
	            value: 'code',
	            text: 'name',
	            width : 66,
	            listId:'',
	            defaultSelected: 0,
	            editable: false,
	            callback:{
	                onChange: function(data){
	                	$(_self).next().val(data.name);
	                	var rowid = $(_self).attr("id");
	                	if(comboFlag){
	                		handle.updateEmp(data, rowid,"main");
	                	}
	                }
	            }
	        }).getCombo().loadData(data,[ 'name', defaultValue, -1]);
		});
	},
	slave:function(data){
		$('.ui-combo-wrap.slave').each(function(){
			var defaultValue = $(this).next().val();
			var _self = this;
			$(_self).combo({
	            value: 'code',
	            text: 'name',
	            width : 66,
	            listId:'',
	            defaultSelected: 0,
	            editable: false,
	            callback:{
	                onChange: function(data){
	                	$(_self).next().val(data.name);
	                	var rowid = $(_self).attr("id");
	                	if(comboFlag){
	                		handle.updateEmp(data, rowid,"slave");
	                	}
	                }
	            }
	        }).getCombo().loadData(data,[ 'name', defaultValue, -1]);;
		});
	},
	area:function(data){
		$('.ui-combo-wrap.area').each(function(){
			var defaultValue = $(this).next().val();
			var _self = this;
			$(_self).combo({
	            value: 'code',
	            text: 'name',
	            width : 98,
	            listId:'',
	            defaultSelected: 0,
	            editable: false,
	            callback:{
	                onChange: function(data){
	                	$(_self).next().val(data.name);
	                	var rowid = $(_self).attr("id");
	                	if(comboFlag){
	                		handle.updateArea(data, rowid);
	                	}
	                }
	            }
	        }).getCombo().loadData(data,[ 'name', defaultValue, -1]);;
		});
		comboFlag = true;
	}
};
var queryurl = config.BASEPATH+"cash/listTemp";
var THISPAGE = {
	init:function (){
//		this.initCombo();
		this.initGrid();
		this.initEvent();
		this.initReset();
		this.initDom();
		this.initData();
		this.loadWarnCount();
//		setTimeout("THISPAGE.initWeather()",5000);
	},
	initWeather:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"day/weather",
			cache:false,
			dataType:"json",
			success:function(data){
			}
		});
	},
	initDom:function(){
		$("#pd_no").focus();
		_self = this;
		_self.$_open_type = $("#open_type").cssCheckbox({callback:function(){
			$("#pd_no").focus();
		}});
		this.SelRow = {};
		var id = $("#vip_id").val();
		if(null != id && "" != id){
			var info = "名称："+$("#vm_name").val();
			info += "卡号："+$("#vm_cardcode").val();
			info += "手机:"+$("#vm_mobile").val();
			info += "积分："+$("#vm_points").val();
			$("#vip_info").val(info);
		}
	},
	initData:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"init?"+Math.random(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var pdata = data.data;
					empData = pdata.emp;
					areaData = pdata.area;
					THISPAGE.loadData();
					setTimeout(function(){
						THISPAGE.initPutUp(pdata.putup);
					},200);
					setTimeout(function(){
						THISPAGE.initDay(pdata.day);
					},200);
				}
			}
		});
	},
	initType:function(_type){
		$("#sell_btn").children("a").each(function(){
			$(this).removeClass("in");				
		});
		if(_type == 0){
			$(".btn-sell").addClass("in");
		}
		if(_type == 1){
			$(".btn-back").addClass("in");
		}
		if(_type == 2){
			$(".btn-change").addClass("in");
		}
		$("#sell_type").val(_type);
		setTimeout('$("#pd_no").focus()',200);
	},
	initDay:function(data){
		$("#da_id").val(data.da_id);
		$("#da_come").text(data.da_come);
		$("#da_receive").text(data.da_receive);
		$("#da_try").text(data.da_try);
	},
	initPutUp:function(data){
		if(undefined != data && data.length > 0){
			for(i in data){
				productUtil.buildPutup(data[i].code,data[i].name);
			}
		}
	},
	initGrid:function(){
		var colModel = [	    	
            {name: 'oper',label:'操作',width: 50, fixed:true,align:'center', formatter: formatUtil.operFmatter, title: false,sortable:false},
            {name: 'sht_id',label:'id',index: 'sht_id',hidden:true, title: false},
            {name: 'sht_state',label:'零售状态',index: 'sht_state',hidden:true, title: false},
	    	{name: 'sht_type',label:'状态',index: 'sht_type',align:'center',width:40,formatter:formatUtil.typeFmatter ,title: false},
	    	{name: 'sht_ishand',label:'商品资料里是否手动打折',index: 'sht_ishand',hidden:true, title: false},
	    	{name: 'sht_isvip',label:'商品资料里是否会员打折',index: 'sht_isvip',hidden:true, title: false},
	    	{name: 'sht_ispoint',label:'会员是否积分',index: 'sht_ispoint',hidden:true, title: false},
	    	{name: 'sht_isgift',label:'赠品状态',index: 'sht_isgift',hidden:true,width:40,title: false},
	    	{name: 'sht_sale_model',label:'促销方式',index: 'sht_sale_model',hidden:true, title: false},
	    	{name: 'sht_pd_code',label:'商品编号',index: 'sht_pd_code',hidden:true, title: false},
	    	{name: 'sht_sub_code',label:'商品子码',index: 'sht_sub_code',hidden:true, title: false},
	    	{name: 'sht_viptype',label:'会员打折类别',index: 'sht_viptype',hidden:true, title: false},
	    	{name: 'sht_vipvalue',label:'会员打折值',index: 'sht_vipvalue',hidden:true, title: false},
	    	{name: 'sht_handtype',label:'手动类别',index: 'sht_handtype',hidden:true, title: false},
	    	{name: 'sht_handvalue',label:'手动折值',index: 'sht_handvalue',hidden:true, title: false},
	    	{name: 'pd_no',label:'货号',index: 'pd_no',width:100, title: false},
	    	{name: 'pd_name',label:'名称',index: 'pd_name',width:120,title: true},
	    	{name: 'sht_style',label:'规格',index: 'sht_style',width:110, title: true,formatter:formatUtil.styleFmatter},
	    	{name: 'cr_name',label:'颜色',index: 'cr_name',hidden:true, title: false},
	    	{name: 'sz_name',label:'尺码',index: 'sz_name',hidden:true, title: false},
	    	{name: 'br_name',label:'杯型',index: 'br_name',hidden:true, title: false},
	    	{name: 'sht_amount',label:'数量',index: 'sht_amount',width:50,align:'center', title: false,editable:true},
	    	{name: 'sht_sell_price',label:'零售价',index: 'sht_sell_price',width:70,align:'right', title: false},
	    	{name: 'sht_price',label:'折后价',index: 'sht_price',align:'right',width:60, title: false},
	    	{name: 'sht_sale_money',label:'促销折',index: 'sht_sale_money',align:'right',width:60,hidden:true, title: false},
	    	{name: 'sht_vip_money',label:'会员折',index: 'sht_vip_money',align:'right',width:60, hidden:true,title: false},
	    	{name: 'sht_hand_money',label:'手动折',index: 'sht_hand_money',align:'right',width:60,hidden:true, title: false},
	    	{name: 'sht_money',label:'小计',index: 'sht_money',width:80,align:'right',title: false},
	    	{name: 'main_combo',label:'主导',index: 'main_combo',width:60,align:'center',title: false,shrinkToFit:false
	    			,formatter:formatUtil.formatMain},
	    	{name: 'main_name',label:'主导名称',index: 'main_name',hidden:true,align:'center',title: false},
	    	{name: 'slave_combo',label:'副导',index: 'slave_combo',width:60,align:'center',title: false,shrinkToFit:false
	    			,formatter:formatUtil.formatSlave},
			{name: 'slave_name',label:'主导名称',index: 'slave_name',hidden:true,align:'center',title: false},
			{name: 'da_name',label:'位置名称',index: 'da_name',hidden:true,align:'center',title: false},
	    	{name: 'area_combo',label:'陈列位置',index: 'area_combo',width:90,align:'center',title: false,shrinkToFit:false
	    			,formatter:formatUtil.formatArea},
			{name: 'sht_remark',label:'折扣情况',index: 'sht_remark',width:150,formatter:formatUtil.remark,title: true}
	    ];
		var recordHtml = '<a class="record_vip" onclick="javascript:handle.doVipInfo();"></a>';
		$('#grid').jqGrid({
			datatype: 'lcoal',
			width: width,
			height: height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordpos:'right',
			recordtext:recordHtml,  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:999,//每页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:true,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'sht_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.buildTotal();
				ComboUtil.main(empData);
				ComboUtil.slave(empData);
				ComboUtil.area(areaData);
				var vip_info = $("#vip_info").val();
				$(".record_vip").html(vip_info);
				$("#pd_no").select().focus();
			},
			gridComplete:function(data){
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow: function (rowid, iRow, iCol, e) {
				handle.doHandRate(rowid);
            },
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	THISPAGE.SelRow.id=rowid;
            	THISPAGE.SelRow.value=value;
				return value;
			},
			beforeSaveCell:function(rowid, cellname, value, iRow, iCol){
				if(value == '' || isNaN(value) ){
					return value;
				}
				if(value == 0){
					return THISPAGE.SelRow.value;
				}else{
					productUtil.updateTemp(rowid,value);
					return value;
				}
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
			}
	    });
	},
	initParam:function(){
		var random = Math.random();
		var param = random;
		return param;
	},
	loadData:function(){
		comboFlag = false;
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+"?"+THISPAGE.initParam()}).trigger("reloadGrid");
	},
	buildTotal:function(){
		var grid = $("#grid");
		var ids = grid.getDataIDs();
		var amount_total = 0,money_total = 0,sell_type = -1;
		for(key in ids){
			var data = grid.jqGrid('getRowData', ids[key]);
			amount_total += parseFloat(data.sht_amount);
			money_total += parseFloat(data.sht_money);
			sell_type = data.sht_state;
		}
		$("#total_money").text(money_total.toFixed(2));
		grid.footerData("set",{'sht_amount':amount_total,'sht_money':money_total.toFixed(2)});
		setTimeout(function(){
			if(sell_type != -1){
				$("#sell_type").val(sell_type);
				THISPAGE.initType(sell_type);
			}
		},50);
	},
	reset:function(){
		$("#vip_id").val("");
		$("#vip_info").val("");
		$("#sh_id").val("");
		$("#record_vip").text("");
		THISPAGE.buildTotal();
//		$("#last_give_money").text(0.00);
	},
	initReset:function(){
		/*if(1 != system.SHOWSALVE){
			$("#grid").hideCol("alt_salve").trigger("reloadGrid");
		}
		if(1 != system.SHOWAREA){
			$("#grid").hideCol("alt_ar_code").trigger("reloadGrid");
		}*/
		//防止刷新功能 
		var id = $("#vip_id").val();
		if(id == null || "" == id){
			$("#vip_info").val("");
			$("#vip_id").val("");
			$("#record_vip").text("");
		}
//		$("#last_give_money").text(0.00);
		THISPAGE.buildTotal();
	},
	loadWarnCount:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"shop/warn/loadWarnCount",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var warnCount = data.data;
					var allMsgCount = 0;
					allMsgCount += parseInt(warnCount.wantReceiveCount);
					$("#btn_want_receive").find("em").html(warnCount.wantReceiveCount);
					$("#allMsgCount").find("b").html(allMsgCount);
					
				}
			}
		});
	},
	initEvent:function(){
		$("#mobile").on('keyup',function(event){//货号查询
			if(event.keyCode == 13){
				handle.vipByMobile();
			}
		});
		$("#vip_manage").on('click',function(){//会员管理
			handle.doVip();
		});
		$("#edit_pass").on('click',function(){//修改密码
			handle.doEditPass();
		});
		$("#voucher_manage").on('click',function(){//代金券管理
			handle.doVoucher();
		});
		$("#card_manage").on('click',function(){//储值卡管理
			handle.doCard();
		});
		$("#vip_point").on('click',function(){//积分管理
			handle.doVipPoint();
		});
		$("#gift_point").on('click',function(e){
			e.preventDefault();
			handle.changeGift();
		});
		$("#shop_service").on('click',function(){//维修服务
			handle.doShopService();
		});
		$("#shop_program").on('click',function(){//搭配方案
			handle.doShopProgram();
		});
		$("#money_access").on('click',function(){//银行收支
			handle.doMoneyAccess();
		});
		/*************打折处理*****************/
		$("#vip_rate").on('click',function(){//会员折扣
			handle.doVipRate();
		});
		$("#hand_rate").on('click',function(){//手动折扣
			handle.doHandRate();
		});
		$("#all_rate").on('click',function(){//整单打折
			handle.doAllRate();
		});
		/*************打折处理*****************/
		$("#btn_come").on('click',function(){//进店人数
			handle.doCome();
		});
		$("#btn_receive").on('click',function(){//接待人数
			handle.doReceive();
		});
		$("#btn_try").on('click',function(){//试穿人数
			handle.doTry();
		});
		$("#btn_weather").on('click',function(){//店铺天气
			handle.doWeather();
		});
		$("#btn_print_set").on('click',function(e){//打印设置
			handle.doPrintSet();
		});
		$("#btn_cash_set").on('click',function(e){//收银设置
			handle.cashSet();
		});
		$("#btn_allocate_set").on('click',function(e){//调拨打印设置
			handle.doAllocateSet();
		});
		$("#pd_no").on('keyup',function(event){//货号查询
			if(event.keyCode == 13){
				productUtil.doProduct();
			}
		});
		$("#dayend").on('click',function(e){
			handle.doDayEnd();
		});
		$("#btn-search").on('click',function(){//查询按钮
			productUtil.doProduct();
		});
		$(".putup").on('click',function(){
			var code = $(this).prop("code");
			var id = $(this).prop("id");
			id = id.replace("putup_","");
			if(null != code && "" != code){
				productUtil.doPutdown(id);
			}else{
				var ids = $("#grid").jqGrid("getDataIDs");
				if(null != ids && ids.length > 0){
					productUtil.doPutup(id);
				}else{
					Public.tips({type:1,content:"未添加数据，不能挂单"});
				}
			}
		});
		$("#other_stock").on('click',function(e){//商品查询
			e.preventDefault();
			productUtil.doOtherStock();
		});
		//删除单个商品
		$('#grid').on('click', '.operating .ui-icon-dele', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			productUtil.delById(id);
		});
		//清空商品
		$("#btn-clear").on('click',function(e){
			e.preventDefault();
			productUtil.clearTemp();
		});
		//查看商品图片
		$('#grid').on('click', '.operating .ui-icon-img', function(e){
			e.preventDefault();
			var id = $(this).parent().data('id');
			var pdata = $("#grid").jqGrid("getRowData", id);
			Public.openImg(pdata.sht_pd_code);
		});
		//收银结算
		$("#btn-cash").on('click',function(e){
			e.preventDefault();
			handle.doCash();
		});
		$("#sell_list").on('click',function(e){//零售查询
			e.preventDefault();
			handle.doSellList();
		});
		//预付订金
		$("#btn-pay").on('click',function(e){
			e.preventDefault();
			productUtil.payDeposit();
		});
		//提取订金
		$("#btn-get").on('click',function(e){
			e.preventDefault();
			productUtil.getDeposit();
		});
		//前台调拨
		$("#btn_allocate").on('click',function(e){
			e.preventDefault();
			handle.doAllocate();
		});
		//收货管理
		$("#btn_want_receive").on('click',function(e){
			e.preventDefault();
			handle.doWantReceive();
		});
		//智能计划
		$("#btn_shop_plan").on('click',function(e){
			e.preventDefault();
			handle.doShopPlan();
		});
		//KPI考核
		$("#btn_shop_kpiassess").on('click',function(e){
			e.preventDefault();
			handle.doShopKpiAssess();
		});
		//PK工具
		$("#btn_shop_kpipk").on('click',function(e){
			e.preventDefault();
			handle.doShopKpiPk();
		});
		//目标管理
		$("#btn_shop_target").on('click',function(e){
			e.preventDefault();
			handle.doShopTarget();
		});
		//零售状态切换
		$(".btn-sell").on('click',function(){
			var ids = $("#grid").getDataIDs();
			if(ids != null && ids.length > 0){
				Public.tips({type:1,content:"有零售数据,无法切换!"});
				return;
			}
			THISPAGE.initType(0);
		});
		$(".btn-back").on('click',function(){
			var ids = $("#grid").getDataIDs();
			if(ids != null && ids.length > 0){
				Public.tips({type:1,content:"有零售数据,无法切换!"});
				return;
			}
			THISPAGE.initType(1);
		});
		$(".btn-change").on('click',function(){
			var ids = $("#grid").getDataIDs();
			if(ids != null && ids.length > 0){
				Public.tips({type:1,content:"有零售数据,无法切换!"});
				return;
			}
			THISPAGE.initType(2);
		});
	}
};
THISPAGE.init();