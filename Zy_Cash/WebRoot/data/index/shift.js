var api = frameElement.api, W = api.opener;
var queryurl = W.basepath+'shift';
var callback = api.data.callback;
var basePath = $("#basePath").val();
var vadiUtil={
	vadiNumber:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入数字"});
			$(obj).val(0).focus().select();
			return;
		}
		if(parseFloat(data) < 0){
			W.Public.tips({type: 1, content : "请输入正数"});
			$(obj).val(0).focus().select();
			return;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initCombo();
		this.initGrid();
		this.initEvent();	
	},
	initCombo:function(){
		empCombo = $('#spanEmp').combo({
	        value: 'code',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#de_man").val(data.name);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:W.basepath+"cash/emp/listCash?"+Math.random(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					empCombo.loadData(data.data,-1);
				}
			}
		});
	},
	initGrid:function(){
		var colModel = [
			{name: 'st_code', label:'编号',index: 'st_code', width: 50, title: false},
	    	{name: 'st_name', label:'姓名',index: 'st_name', width: 75, title: false},
	    	{name: 'st_begintime', label:'开始',index: 'st_begintime', width: 75, title: false},
	    	{name: 'st_endtime', label:'结束',index: 'st_endtime', width: 75, title: false}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
            datatype: 'json',
			width: 358,
			height: 140,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:-1,//每页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'st_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
            }
	    });
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		$("#btn-save").on('click',function(){
			doSelect();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
};
function doSelect(){
	var de_last_money = $("#de_last_money").val();
	var de_petty_cash = $("#de_petty_cash").val();
	if (isNaN(de_last_money)) {
		W.Public.tips({type: 1, content : "请输入数字"});
		$("#de_last_money").val(0).focus().select();
		return;
	}
	if (isNaN(de_petty_cash)) {
		W.Public.tips({type: 1, content : "请输入数字"});
		$("#de_petty_cash").val(0).focus().select();
		return;
	}
	var selectedId = $("#grid").jqGrid('getGridParam', 'selrow');
	if(selectedId == null || selectedId == ""){
		W.Public.tips({type: 2, content : "请选择班次！"});
		return false;
	}
	var rowData = $("#grid").jqGrid("getRowData", selectedId);
	rowData.de_petty_cash = de_petty_cash;
	rowData.de_last_money = de_last_money;
	if(callback && typeof callback == 'function'){
		callback(rowData);
	}
}
THISPAGE.init();