var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'money/access/temp_list';
var _height = $(parent).height()-243,_width = $(parent).width()-2;

var Utils = {
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#ac_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	},
	doQueryBank : function(){
		commonDia = $.dialog({
			title : '选择银行账户',
			content : 'url:'+config.BASEPATH+'money/bank/to_list_dialog',
			data : {multiselect:true},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				var temps = [];
				for(var i=0;i<selected.length;i++){
					var temp = {};
					temp.acl_ba_code = selected[i].ba_code;
					temp.acl_money = 0;
					temps.push(temp);
				}
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'money/access/temp_save',
					data:{"temps":JSON.stringify(temps)},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '保存成功'});
							THISPAGE.reloadGridData();
							commonDia.close();
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
				return false;
			},
			close:function(){
				
			},
			cancel:true
		});
	}
};

var handle = {
	temp_updateRemark:function(rowid,remark){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/access/temp_updateRemark',
			data:{acl_id:rowid,acl_remark:Public.encodeURI(remark)},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
	temp_updateMoney:function(rowid,money){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/access/temp_updateMoney',
			data:{acl_id:rowid,acl_money:money},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
				}else{
					Public.tips({type: 1, content : data.message});
				}
			}
		});
    },
    temp_updateType:function(rowid,acl_type){
    	$.ajax({
    		type:"POST",
    		url:config.BASEPATH+'money/access/temp_updateType',
    		data:{acl_id:rowid,acl_type:acl_type},
    		cache:false,
    		dataType:"json",
    		success:function(data){
    			if(undefined != data && data.stat == 200){
    				$("#grid").jqGrid('setRowData', rowid, {acl_type:acl_type});
    				THISPAGE.gridTotal();
            		if(acl_type == '1'){
            			$("#grid").jqGrid('setRowData', rowid, false, { color: '#FF0000' });
            		}else{
            			$("#grid").jqGrid('setRowData', rowid, false, { color: '#696969' });
            		}
    			}else{
    				Public.tips({type: 1, content : data.message});
    			}
    		}
    	});
    },
	temp_del: function(rowId){//删除
		if(rowId != null && rowId != ''){
			var rowData = $("#grid").jqGrid("getRowData", rowId);
		 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
				$.ajax({
					type:"POST",
					url:config.BASEPATH+'money/access/temp_del',
					data:{"acl_id":rowId},
					cache:false,
					dataType:"json",
					success:function(data){
						if(undefined != data && data.stat == 200){
							Public.tips({type: 3, content : '删除成功!'});
							$('#grid').jqGrid('delRowData', rowId);
							THISPAGE.gridTotal();
							var ids = $("#grid").getDataIDs();
							if(ids.length == 0){
								THISPAGE.addEmptyData();
							}
						}else{
							Public.tips({type: 1, content : data.message});
						}
					}
				});
			});
	   }else{
		   Public.tips({type: 1, content : '请选择数据!'});
	   }	
    },
    temp_clear:function(){
    	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'money/access/temp_clear',
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$("#grid").clearGridData();
						THISPAGE.gridTotal();
						THISPAGE.addEmptyData();
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	save:function(){
		var ids = $("#grid").jqGrid('getDataIDs');
		if (ids.length == 0){
			Public.tips({type: 2, content : '单据明细不存在，请选择收入类型！'});
			return;
		}
		$("#btn-save").attr("disabled",true);
		var saveUrl = config.BASEPATH+"money/access/save";
		if($("#ac_id").val() != undefined){
			saveUrl = config.BASEPATH+"money/access/update";
		}
		$.ajax({
			type:"POST",
			url:saveUrl,
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					Public.tips({type: 3, content : '保存成功'});
					handle.loadData(data.data);
				}else{
					Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.ac_id;
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	formatType :function(val, opt, row){
		if(row.acl_type == null || $.trim(row.acl_type) == ''){
			return "";
		}
		var show="";
		show+="<input type='radio' onclick='javascript:handle.temp_updateType("+row.acl_id+",0);' name='radio"+row.acl_id+"' "+(row.acl_type=="0"?"checked":"")+" value='0'/>存款";
		show+="<input type='radio' onclick='javascript:handle.temp_updateType("+row.acl_id+",1);' name='radio"+row.acl_id+"' "+(row.acl_type=="1"?"checked":"")+" value='1'/>取款";
		return show;
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		html_con += '<i class="iconfont i-hand ui-icon-plus" title="新增">&#xe639;</i>';
		html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		html_con += '</div>';
		return html_con;
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		$("#ac_date").val(config.TODAY);
		this.selectRow={};
	},
	gridTotal:function(){
    	var gridData = $("#grid").jqGrid('getRowData');
		var rowData = null;
    	var totalMoney = 0;
    	for(key in gridData){
    		rowData = gridData[key];
    		if(rowData.acl_type == '0'){//存款
    			totalMoney += parseFloat(rowData.acl_money);
    		}else if(rowData.acl_type == '1'){//取款
    			totalMoney -= parseFloat(rowData.acl_money);
    		}
    	}
    	$("#ac_money").val(totalMoney.toFixed(2));
		$("#grid").footerData("set", {acl_money:totalMoney});
    },
    addEmptyData:function(){
    	var emptyData = {acl_pp_code:'',pp_name:'',acl_money:'',acl_remark:''};
    	$("#grid").addRowData(0, emptyData);
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'银行账户',name: 'ba_name', index: 'ba_name', width: 140},
	    	{label:'',name: 'acl_type', index: 'acl_type', width: 100,hidden:true},
	    	{label:'存取类型',name: 'typeHtml', index: 'acl_type', width: 140,align:'center',formatter: handle.formatType},
	    	{label:'金额',name: 'acl_money', index: 'acl_money', width: 80,align:'right',sorttype: 'float',editable:true,formatter: Public.formatMoney},
	    	{label:'备注',name: 'acl_remark', index: 'acl_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				var ids = $("#grid").getDataIDs();
				if(ids.length == 0){
					THISPAGE.addEmptyData();
				}else{
					for(var i=0;i < ids.length;i++){
						var rowData = $("#grid").jqGrid("getRowData", ids[i]);
						if(rowData.acl_type == '1'){
							$("#grid").jqGrid('setRowData', ids[i], false, { color: '#FF0000' });
						}
					}
				}
				
			},
			loadError: function(xhr, status, error){		
			},
			ondblClickRow:function(rowid,iRow,iCol,e){
				handle.temp_update(rowid, '0');
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if(rowid == 0){
					return self.selectRow.value;
				}
				if(cellname == 'acl_money'  && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) == 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					handle.temp_updateMoney(rowid, parseFloat(value).toFixed(2));
					return parseFloat(value).toFixed(2);
				}else if(cellname == 'acl_remark' && self.selectRow.value != value){
					handle.temp_updateRemark(rowid, value);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'acl_money' && self.selectRow.value != value){
					THISPAGE.gridTotal();
				}
				
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//清除
        $('#btnClear').click(function(e){
        	e.preventDefault();
        	handle.temp_clear();
		});
        $('#grid').on('click', '.operating .ui-icon-plus', function (e) {
        	e.preventDefault();
        	Utils.doQueryBank();
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.temp_del(id);
		});
	}
};

THISPAGE.init();