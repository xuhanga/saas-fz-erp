var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var ac_number = $("#ac_number").val();
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'money/access/detail_list/'+ac_number;
var _height = $(parent).height()-243,_width = $(parent).width()-2;

var handle = {
	formatType :function(val, opt, row){
		if(row.acl_money > 0){
			return '存款';
		}else if(row.acl_money < 0){
			return '取款';
		}
		return '';
	}
};

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		var ac_ar_state = $("#ac_ar_state").val();
        if('approve' == api.data.oper){
			$("#btn-approve").show();
		}
        if('view' == api.data.oper && ac_ar_state == '1'){
        	$("#btn-reverse").show();
        }
	},
	gridTotal:function(){
		var grid=$('#grid');
		var acl_money=grid.getCol('acl_money',false,'sum');
    	grid.footerData('set',{ba_name:'合计：',acl_money:acl_money});
    	$("#ac_money").val(acl_money.toFixed(2));
    },
	initGrid:function(){
		var self=this;
		var colModel = [
	    	{label:'银行账户',name: 'ba_name', index: 'ba_name', width: 140},
	    	{label:'',name: 'acl_type', index: 'acl_type', width: 100,hidden:true},
	    	{label:'存取类型',name: 'typeHtml', index: 'acl_type', width: 140,align:'center',formatter: handle.formatType},
	    	{label:'金额',name: 'acl_money', index: 'acl_money', width: 80,align:'right',sorttype: 'float',editable:true,formatter: Public.formatMoney},
	    	{label:'备注',name: 'acl_remark', index: 'acl_remark', width: 180,editable:true}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				var ids = $("#grid").getDataIDs();
				for(var i=0;i < ids.length;i++){
					var rowData = $("#grid").jqGrid("getRowData", ids[i]);
					if(rowData.acl_money < 0){
						$("#grid").jqGrid('setRowData', ids[i], false, { color: '#FF0000' });
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();