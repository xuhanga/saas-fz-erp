var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'sell/allocate/pageReport';
var _height = $(parent).height()-172,_width = $(parent).width()-34;

var Utils = {
	doQueryShop : function(type,code,name){
		var title = '';
		if(type == 'out'){
			title = '选择调出店铺';
		}else if(type == 'in'){
			title = '选择调入店铺';
		}
		commonDia = $.dialog({
			title : title,
			content : 'url:'+config.BASEPATH+'base/shop/to_list_dialog_bymodule',
			data : {multiselect:false,module:'sell_allocate'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#"+code).val(selected.sp_code);
				$("#"+name).val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#"+code).val(selected.sp_code);
					$("#"+name).val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'cash/emp/to_emp_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#ac_man").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#ac_man").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	formatArState:function(val, opt, row){
		if(val == 0){
			return '暂存';
		}else if(val == 1){
			return '已调出';
		}else if(val == 2){
			return '完成';
		}else if(val == 3){
			return '已拒收';
		}
		return val;
	},
	formatSellMoney :function(val, opt, row){
		if($.trim(val) != ""){
			return val;
		}
		return Public.formatMoney(row.acl_amount*row.acl_sell_price);
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_date = $("#date").cssRadio({ callback: function($_obj){
			dateRedioClick($_obj.find("input").attr("id"));
		}});
		this.$_ar_state = $("#ar_state").cssRadio({ callback: function($_obj){
			$("#ac_state").val($_obj.find("input").val());
		}});
		this.$_out_in = $("#td_out_in").cssRadio({ callback: function($_obj){
			var type = $_obj.find("input").val();
			if(type == "out"){//调出
				$("#outshop_name").val(system.SHOP_NAME);
				$("#ac_out_shop").val(system.SHOP_CODE);
				$("#inshop_name").val("");
				$("#ac_in_shop").val("");
				$("#btn_out_shop").attr("disabled",true);
				$("#outshop_name").attr("disabled",true);
				$("#btn_in_shop").attr("disabled",false);
				$("#inshop_name").attr("disabled",false);
				
			}else if(type == "in"){//调入
				$("#inshop_name").val(system.SHOP_NAME);
				$("#ac_in_shop").val(system.SHOP_CODE);
				$("#outshop_name").val("");
				$("#ac_out_shop").val("");
				$("#btn_in_shop").attr("disabled",true);
				$("#inshop_name").attr("disabled",true);
				$("#btn_out_shop").attr("disabled",false);
				$("#outshop_name").attr("disabled",false);
			}else{
				$("#outshop_name").val("");
				$("#ac_out_shop").val("");
				$("#inshop_name").val("");
				$("#ac_in_shop").val("");
				$("#btn_in_shop").attr("disabled",false);
				$("#outshop_name").attr("disabled",false);
				$("#btn_out_shop").attr("disabled",false);
				$("#inshop_name").attr("disabled",false);
			}
		}});
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var acl_amount=grid.getCol('acl_amount',false,'sum');
    	var acl_sell_money=grid.getCol('acl_sell_money',false,'sum');
    	grid.footerData('set',{ac_number:'合计：',acl_amount:acl_amount,acl_sell_money:Public.formatMoney(acl_sell_money)});
    },
	initGrid:function(){
		var colModel = [
	    	{label:'',name: 'ac_out_shop',index: 'ac_out_shop',width:100,hidden:true},
	    	{label:'',name: 'ac_in_shop',index: 'ac_in_shop',width:100,hidden:true},
	    	{label:'单据编号',name: 'ac_number',index: 'ac_number',width:150},
	    	{label:'调拨日期',name: 'ac_date',index: 'ac_date',width:80},
	    	{label:'经办人',name: 'ac_man',index: 'ac_man',width:100},
	    	{label:'调出店铺',name: 'outshop_name',index: 'ac_out_shop',width:120},
	    	{label:'调入店铺',name: 'inshop_name',index: 'ac_in_shop',width:120},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'数量',name: 'acl_amount', index: 'acl_amount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'零售价',name: 'acl_sell_price', index: 'acl_sell_price', width: 80,align:'right',formatter: Public.formatMoney},
	    	{label:'零售额',name: 'acl_sell_money', index: 'acl_sell_money', width: 80,align:'right',formatter: handle.formatSellMoney,sortable:false},
	    	{label:'状态',name: 'ac_state',index: 'ac_state',width:80,formatter: handle.formatArState,align:'center'},
	    	{label:'接收人',name: 'ac_receiver',index: 'ac_receiver',width:100},
	    	{label:'接收日期',name: 'ac_recedate',index: 'ac_recedate',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.DATAROWNUM,//每页条数
			rowList:config.DATAROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'acl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'ac_state='+$("#ac_state").val();
		params += '&begindate='+$("#begindate").val();
		params += '&enddate='+$("#enddate").val();
		params += '&ac_out_shop='+$("#ac_out_shop").val();
		params += '&ac_in_shop='+$("#ac_in_shop").val();
		params += '&ac_man='+Public.encodeURI($("#ac_man").val());
		params += '&ac_number='+Public.encodeURI($("#ac_number").val());
		return params;
	},
	reset:function(){
		$("#outshop_name").val("");
		$("#inshop_name").val("");
		$("#ac_out_shop").val("");
		$("#ac_in_shop").val("");
		$("#ac_number").val("");
		$("#ac_man").val("");
		THISPAGE.$_date.setValue(0);
		THISPAGE.$_ar_state.setValue(0);
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
}
THISPAGE.init();