var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var isRefresh = false;
var api = frameElement.api, W = api.opener;
var queryurl = config.BASEPATH+'service/list';
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高
var handle = {
	//修改、新增
	operate: function(oper, rowId){
		var height = 300;
		var width = 520;
		if(oper == 'add'){
			var title = '新增售后维修';
			var url = config.BASEPATH+"service/to_add";
			var data = {oper: oper, callback: this.callback};
		}
		$.dialog({
			id:"twoPageID",
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			drag: false,
			lock: false,
			close:function(){
				if(isRefresh){
					THISPAGE.reloadData();
					isRefresh = false;
				}
			}
		});
	},
	toUpdate:function(oper, id){
		var url = config.BASEPATH+"service/to_update?ss_id="+id;
		var data = {oper: oper, callback: this.callback};
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false,
			close:function(){
		   		if(isRefresh){
		   			THISPAGE.reloadData();
			   	}
		   	}
		}).max();
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if (row.ss_state == '0') {
            btnHtml += '维修中...';
        }else if (row.ss_state == '1') {
        	btnHtml += '<input type="button" value="取货" class="btn_wx" onclick="javascript:handle.toUpdate(\'pick\',\'' + row.ss_id + '\');" />';
        }else if (row.ss_state == '2') {
        	btnHtml += '<input type="button" value="明细" class="btn_ck" onclick="javascript:handle.toUpdate(\'detail\',\'' + row.ss_id + '\');" />';
        }else if (row.ss_state == '3') {
        	btnHtml += '<input type="button" value="受理" class="btn_qh" onclick="javascript:handle.toUpdate(\'processed\',\'' + row.ss_id + '\');" />';
        }
		return btnHtml;
	},
	ss_state : function(val, opt, row){
		if(val == '0'){
			return '待维修';
		} else if(val == '1'){
			return '待取货';
		} else if(val == '2'){
			return '已完成';
		} else if(val == '3'){
			return '待受理';
		} else {
			return '';
		}
	}
}
 


var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom: function(){
		this.$_date = $('#date');
	 	this.$_date.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			dateRedioClick($_obj.find("input").attr("id"));
		 		}
		 	}
	 	);
	 	this.$_satae = $('#satae');
	 	this.$_satae.cssRadio(
		 	{callback:
		 		function($_obj){  //$_obj是radio外面的lable
		 			$("#ss_state").val($($_obj).find('input').val());
		 		}
		 	}
	 	);
	},
	initGrid:function (){
		var gridWH = Public.setGrid();
		var colModel = [
		    {name: 'ss_id', label:'',index: 'ss_id', hidden:true},
	    	{name: 'operate', label:'操作', width: 60, fixed:true,title: false,formatter: handle.operFmatter, sortable:false},
	    	/*{name: 'send_message',label:'短信提醒',  width: 90, fixed:true,   title: false,sortable:false},*/
	    	{name: 'ss_state', label:'状态',index: 'ss_state', width: 60, title: false,align:'center',formatter:handle.ss_state},
	    	{name: 'ss_sms_count',label:'短信发送次数', index: 'ss_sms_count', width: 60, title: false,align:'center'},
	    	{name: 'ss_name', label:'顾客姓名',index: 'ss_name', width: 60,fixed:true ,title: false,align:'left'},
	    	{name: 'ss_tel', label:'手机号码',index: 'ss_tel', width: 90},
	    	{name: 'ss_pd_no',label:'商品货号', index: 'ss_pd_no', width: 80},
	    	{name: 'ssi_name', label:'维修项目',index: 'ssi_name', width: 80},
	    	{name: 'ss_sysdate',label:'预约日期', index: 'ss_sysdate', width: 80,align:'center'},
	    	{name: 'ss_startdate',label:'受理日期', index: 'ss_startdate', width: 80,align:'center'},
	    	{name: 'ss_manager', label:'受理人员',index: 'ss_manager', width: 80},
	    	{name: 'ss_servicedate',label:'维修日期', index: 'ss_servicedate', width: 80},
	    	{name: 'ss_enddate',label:'取货日期', index: 'ss_enddate', width: 80,align:'center'},
	    	{name: 'ss_satis', label:'满意度',index: 'ss_satis', width: 80,align:'center'},
	    	{name: 'ss_question', label:'存在问题',width: 180,title:true}
	    ];
	    $("#grid").jqGrid({
	    	url:queryurl,
			//loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'json',
			width: _width,
			height: _height,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			rowNum:30,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id:'ss_id'
			}
	    });
	},
	buildParams:function(){
		var begindate = $("#begindate").val();//开始日期		
		var enddate = $("#enddate").val();//结束日期		
		var ss_state = $("#ss_state").val();
		var ss_name = Public.encodeURI($("#ss_name").val());
		var ss_tel = $("#ss_tel").val();
		var params = '';
		params += 'begindate='+begindate;
		params += '&enddate='+enddate;
		params += '&ss_state='+ss_state;
		params += '&ss_name='+ss_name;
		params += '&ss_tel='+ss_tel;
		return params;
	},
	reset:function(){
		$("#ss_name").val("");
		$("#ss_tel").val("");
		$("#ss_state5").click();
		$("#ss_state").val("");
		$("#theDate").click();
		$("#begindate").val("");
		$("#enddate").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.operate('add');
		});
		//重置
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
	}
};
THISPAGE.init();