var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var sp_type = $("#sp_type").val();
var queryurl = config.BASEPATH+'set/print/list/'+sp_type;

var api = frameElement.api,W = api.opener;
var _height = api.config.height-132,_width = api.config.width-34;//获取弹出框弹出宽、高

var handle = {
	formatOperate : function(val, opt, row){
		if(api.data.from == "print"){
			return "<input type='button' class='btn_sp' onclick='javascript:handle.doPrint("+opt.rowId+");' value='打印'/>";
		}else if(api.data.from == "set"){
			return "<input type='button' class='btn_xg' onclick='javascript:handle.edit("+opt.rowId+");' value='修改'/>";
		}
		return "";
	},
	add:function(){
		W.$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'set/print/to_print_update/'+sp_type,
			data:{
				sp_type:sp_type,
				callback:this.callback
			},
			max: false,
		   	min: false,
		   	fixed:false,
		   	drag:false,
		   	resize:false,
		   	lock:false,
		   	close:function(){
		   		api.zindex();
		   	}
		}).max();
	},
	edit : function(id){
		W.$.dialog({
			title : false,
			content : 'url:'+config.BASEPATH+'set/print/to_print_update/'+sp_type,
			data:{
				sp_type:sp_type,
				sp_id:id,
				callback:this.callback
			},
			max: false,
		   	min: false,
		   	fixed:false,
		   	drag:false,
		   	resize:false,
		   	lock:false,
		   	close:function(){
		   		api.zindex();
		   	}
		}).max();
	},
	callback: function(){
		THISPAGE.reloadData();
	},
	del: function(id){//删除
		$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'set/print/del/'+id,
				data:{},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', id);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});
    },
	doPrint:function(sp_id){
		var params = "";
		params += "number="+api.data.number;
		if(api.data.displayMode != undefined){
			params += "&displayMode="+api.data.displayMode;
		}
		params += "&sp_id="+sp_id;
		if(api.data.extraParams != undefined){
			params += api.data.extraParams;
		}
		var printUrl = '';
		if(sp_type == '19'){//前台调拨单
			printUrl = config.BASEPATH+"sell/allocate/print";
		}
		if(printUrl == ''){
			Public.tips({type: 1, content : '打印模板暂未开放，请联系管理员！'});
			return;
		}
		$.ajax({
            type: "POST",
            url: printUrl,
            data: params,
            cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
            		var unit = "mm";
					var html = data.data.styleHtml+data.data.listHtml;
					var headHtml = data.data.styleHtml+data.data.headHtml
					var page_height = data.data.page_height+unit;
					var page_width = data.data.page_width+unit;
					var totalHeadHeight = data.data.totalHeadHeight +unit;
					var page_top = data.data.page_top + unit;
					var page_left = data.data.page_left + unit;
					var page_direction = data.data.page_direction;
					var page_title = data.data.page_title;
					//var LODOP=getLodop(document.getElementById('LODOP_OB'),document.getElementById('LODOP_EM'));
					LODOP.PRINT_INIT(page_title);
					LODOP.SET_PRINT_PAGESIZE(page_direction,page_width,page_height,"");
					LODOP.ADD_PRINT_TABLE(totalHeadHeight,"0mm","100%","100%",html);
					//LODOP.SET_PRINT_STYLEA(0,"TableHeightScope",1);
					LODOP.ADD_PRINT_HTM("0mm","0mm","100%",totalHeadHeight,headHtml);
					LODOP.SET_PRINT_STYLEA(0,"ItemType",1);// ItemType的值：数字型，0--普通项 1--页眉页脚 2--页号项 3--页数项 4--多页项
					LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","100%");//整宽不变形
					LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED", 1);
					LODOP.PREVIEW();
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
		
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
		    {label:'操作',name: 'operate', width: 80,align:'center',formatter: handle.formatOperate},
	    	{label:'模板名称',name: 'sp_name',index: 'sp_name',width:160}
	    ];
		$('#grid').jqGrid({
            url:queryurl,			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'sp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	reloadData:function(){
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			handle.add();
		});
		$('#btn-del').on('click', function(e){
			e.preventDefault();
			var rowId = $('#grid').jqGrid('getGridParam','selrow');
			if(rowId == null || rowId == ''){
				Public.tips({type: 1, content : "您未选择任何数据"});
				return;
			}
			handle.del(rowId)
		});
	}
}
THISPAGE.init();