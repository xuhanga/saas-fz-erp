var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var queryurl = config.BASEPATH+'shop/kpipk/list';
var api = frameElement.api;
var _height = api.config.height-142,_width = api.config.width-34;//获取弹出框弹出宽、高

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'view'){
			url = config.BASEPATH+"shop/kpipk/to_view?kp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"shop/kpipk/to_view?kp_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
    callback: function(data, oper, dialogWin){
		if(oper == "approve") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "add"){
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		btnHtml += '<input type="button" value="查看" class="btn_ck" onclick="javascript:handle.operate(\'view\',' + opt.rowId + ');" />';
		return btnHtml;
	},
	formatType:function(val, opt, row){
		if(val == 0){
			return '店铺';
		}else if(val == 1){
			return '员工组';
		}else if(val == 2){
			return '员工';
		}
		return val;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		this.$_kp_type = $("#td_kp_type").cssRadio({ callback: function($_obj){
			$("#kp_type").val($_obj.find("input").val());
			THISPAGE.reloadData();
		}});
	},
	initGrid:function(){
		var colModel = [
	    	{label:'操作',name: 'operate',width: 60, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{label:'',name: 'kp_us_id',index: 'kp_us_id',width:100,hidden:true},
	    	{label:'单据编号',name: 'kp_number',index: 'kp_number',width:110},
	    	{label:'考核范围',name: 'kp_type',index: 'kp_type',width:70,formatter: handle.formatType,align:'center'},
	    	{label:'考核对象',name: 'kpl_name',index: '',width:180,title:true},
	    	{label:'开始时间',name: 'kp_begin',index: 'kp_begin',width:80},
	    	{label:'结束时间',name: 'kp_end',index: 'kp_end',width:80},
	    	{label:'标准分数',name: 'kp_score',index: 'kp_score',width:70,align:'right'},
	    	{label:'奖励',name: 'reward_names',index: '',width:140}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:false,title:false},
			shrinkToFit:false,//表格是否自动填充
			scroll: 1,//是否滚动
			jsonReader: {
				root: 'data',
				repeatitems : false,
				id: 'kp_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'kp_type='+$("#kp_type").val();
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
	}
}
THISPAGE.init();