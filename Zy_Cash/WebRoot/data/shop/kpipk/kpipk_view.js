var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var _height = $(parent).height()-192,_width = $(parent).width()-2;

var kp_state = $("#kp_state").val();
var kp_number = $("#kp_number").val();
var queryurl = config.BASEPATH+"shop/kpipk/statDetail/"+kp_number;

var Utils = {
	doViewKpiScore:function(obj,rowid,realComplete){
		var rowData = $('#grid').jqGrid('getRowData', rowid);
		$.dialog({
			title : 'KPI指标分数查询',
			content : 'url:'+config.BASEPATH+"sys/kpi/to_view",
			data: {ki_code:rowData.kpl_ki_code,realComplete:realComplete},
			width : 500,
			height : 360,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:true,
			cancel:true
		});
	}
};

var handle = {
	formatComplete :function(val, opt, row){
		var html_con = '';
		html_con += '<a href="javascript:void(0);" onclick="javascript:Utils.doViewKpiScore(this,'+opt.rowId+',\''+$.trim(val)+'\');">';
		html_con += $.trim(val);
		html_con += '</a>';
		return html_con;
	}
};

var $_columns = null;
var $_details = null;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.loadDetailDatas();
		this.initEvent();
	},
	initDom:function(){
	},
	loadDetailDatas:function(){
		$.ajax({
			type:"POST",
			async:false,
			url:queryurl,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					var detailData = data.data;
					$_columns = detailData.header;
					$_details = detailData.details;
					THISPAGE.initGrid(detailData.header,detailData.details);
					THISPAGE.buildKpiCombo();
				}
			}
		});
	},
	buildKpiCombo:function(){
		var data = [];
		data.push({Code:"",Name:"总分"});
		for(var i=0;i<$_details.length;i++){
			data.push({Code:$_details[i].kpl_ki_code,Name:$_details[i].ki_name});
		}
    	$('#span_kpi').combo({
    		value: 'Code',
    		text: 'Name',
    		width :100,
    		height:80,
    		listId:'',
    		defaultSelected: 0,
    		editable: false,
    		callback:{
    			onChange: function(data){
    				$("#chart_kpi").val(data.Code);
    				if(data.Code == ""){
    					THISPAGE.buildChart();
    				}else{
    					THISPAGE.buildChartByKpi(data.Code);
    				}
    			}
    		}
    	}).getCombo().loadData(data);
	},
	gridTotal:function(){
		var grid=$('#grid');
		var scoreMap = {};
		var footer = {};
		footer.ki_name = "总计：";
		var maxScore = 0;
		for ( var i = 0; i < $_columns.length; i++) {
			var score = parseInt(grid.getCol("score"+$_columns[i].kpl_code,false,'sum'));
			footer["score"+$_columns[i].kpl_code] = score;
			scoreMap[$_columns[i].kpl_code] = score;
			if (score > maxScore) {
				maxScore = score;
			}
		}
    	grid.footerData('set',footer);
    	var winnerCodes = [];
		var winnerNames = [];
		var kp_score = $("#kp_score").val();
		if(maxScore > 0 && maxScore >= kp_score){//遍历获得获胜者
			for ( var i = 0; i < $_columns.length; i++) {
				var score = scoreMap[$_columns[i].kpl_code];
				if(maxScore == score){
					winnerCodes.push($_columns[i].kpl_code);
					winnerNames.push($_columns[i].kpl_name);
				}
			}
		}
    	$("#winner").text(winnerNames.join(","));
    	$("#maxScore").text(maxScore);
	},
	buildChart:function(){
		var ids = $("#grid").getDataIDs();
    	var chartjs = '';
    	chartjs += '{';
    	chartjs += '"chart":{"type":"column","margin":75,"options3d":{"enabled":true,"alpha":45,"beta":0,"depth":50,"viewDistance":25}},';
    	chartjs += '"title":{"text":"';
    	var kp_type = $("#kp_type").val();
		switch(kp_type){
			case '0' : 
				chartjs += '店铺考核图表';
				break ;
			case '1' : 
				chartjs += '员工组考核图表';
				break ;
			case '2' : 
				chartjs += '员工考核图表';
				break ;
			default :
				break ;
		}
    	chartjs += '"},';
    	chartjs += '"xAxis":{"title":{"text":"';
    	switch(kp_type){
			case '0' : 
				chartjs += '店铺';
				break ;
			case '1' : 
				chartjs += '员工组';
				break ;
			case '2' : 
				chartjs += '员工';
				break ;
			default :
				break ;
		}
    	chartjs += '"},"categories": [';
		for ( var i = 0; i < $_columns.length; i++) {
			if(i == $_columns.length - 1){
				chartjs += '"'+$_columns[i].kpl_name+'"';
			}else{
				chartjs += '"'+$_columns[i].kpl_name+'",';
			}
		}
		chartjs += ']},';
		chartjs += '"yAxis": {"min": 0,"title": {"text": "总分"}},';
		chartjs += '"tooltip": {';
		chartjs += '"headerFormat": "<span style=\'font-size:13px;font-weight:bold\'>{point.key}</br></span>",';
		chartjs += '"pointFormat": "<b>{point.y:.0f}分</b>"},';
		chartjs += '"plotOptions": {"column": {"pointPadding": 0.2,"borderWidth": 0}},';
		chartjs += '"series": [';
		chartjs += '{';
		chartjs += '"name": "总分",';
		chartjs += '"data":[';
		for ( var i = 0; i < $_columns.length; i++) {
			var score = $('#grid').getCol("score"+$_columns[i].kpl_code,false,'sum')
			if(i == $_columns.length - 1){
				chartjs += score;
			}else{
				chartjs += score+',';
			}
		}
		chartjs += '],';
		chartjs += '"dataLabels": { ';
		chartjs += '"enabled": false,"rotation": -45,"color": "#FFFFFF","align": "right","x": 4,"y": 10,';
		chartjs += '"style": {"fontSize": "10px","fontFamily": "Verdana, sans-serif","textShadow": "0 0 3px black"}';
		chartjs += '}}]';
		chartjs += '}';
		document.getElementById("container").style.height="350px";
    	eval("$('#container').highcharts("+chartjs+")");
	},
	buildChartByKpi:function(ki_code){
		var ids = $("#grid").getDataIDs();
    	var chartjs = '';
    	chartjs += '{';
    	chartjs += '"chart":{"type":"column","margin":75,"options3d":{"enabled":true,"alpha":45,"beta":0,"depth":50,"viewDistance":25}},';
    	chartjs += '"title":{"text":"';
    	var kp_type = $("#kp_type").val();
		switch(kp_type){
			case '0' : 
				chartjs += '店铺考核图表';
				break ;
			case '1' : 
				chartjs += '员工组考核图表';
				break ;
			case '2' : 
				chartjs += '员工考核图表';
				break ;
			default :
				break ;
		}
    	chartjs += '"},';
    	chartjs += '"xAxis":{"title":{"text":"';
    	switch(kp_type){
			case '0' : 
				chartjs += '店铺';
				break ;
			case '1' : 
				chartjs += '员工组';
				break ;
			case '2' : 
				chartjs += '员工';
				break ;
			default :
				break ;
		}
    	chartjs += '"},"categories": [';
    	for ( var i = 0; i < $_columns.length; i++) {
			if(i == $_columns.length - 1){
				chartjs += '"'+$_columns[i].kpl_name+'"';
			}else{
				chartjs += '"'+$_columns[i].kpl_name+'",';
			}
		}
    	
    	var rowData = null;
    	for(var j=0;j<$_details.length;j++){
    		if($_details[j].kpl_ki_code == ki_code){
    			rowData = $_details[j];
    		}
    	}
    	
    	
		chartjs += ']},';
		chartjs += '"yAxis": {"min": 0,"title": {"text": "'+rowData.ki_name+'"}},';
		chartjs += '"tooltip": {';
		chartjs += '"headerFormat": "<span style=\'font-size:13px;font-weight:bold\'>{point.key}</br></span>",';
		chartjs += '"pointFormat": "<b>{point.y:.0f}</b>"},';
		chartjs += '"plotOptions": {"column": {"pointPadding": 0.2,"borderWidth": 0}},';
		chartjs += '"series": [';
		chartjs += '{';
		chartjs += '"name": "'+rowData.ki_name+'",';
		chartjs += '"data":[';
		for ( var i = 0; i < $_columns.length; i++) {
			var value = rowData["complete"+$_columns[i].kpl_code]
			if(i == $_columns.length - 1){
				chartjs += value;
			}else{
				chartjs += value+',';
			}
		}
		chartjs += '],';
		chartjs += '"dataLabels": { ';
		chartjs += '"enabled": false,"rotation": -45,"color": "#FFFFFF","align": "right","x": 4,"y": 10,';
		chartjs += '"style": {"fontSize": "10px","fontFamily": "Verdana, sans-serif","textShadow": "0 0 3px black"}';
		chartjs += '}}]';
		chartjs += '}';
		document.getElementById("container").style.height="350px";
    	eval("$('#container').highcharts("+chartjs+")");
	},
	initGrid:function(columns,rows){
		var colModel = [
	    	{label:'指标编号',name: 'kpl_ki_code', index: 'kpl_ki_code', width: 140, hidden: false},
	    	{label:'指标名称',name: 'ki_name', index: 'ki_name', width: 160, title: false}
	    ];
		if (columns != null && columns.length > 0) {
			for ( var i = 0; i < columns.length; i++) {
				colModel.push({label:'完成情况',name: "complete"+columns[i].kpl_code, width: 80,align:'right',formatter: handle.formatComplete});
				colModel.push({label:'分数',name: "score"+columns[i].kpl_code, width: 80,align:'right'});
				colModel.push({label:'',name: "id"+columns[i].kpl_code, width: 100,hidden:true});
			}	
		}
		$('#grid').jqGrid({
			loadonce:true, //只从后台加载一次数据，datatype被修改为local,如果弹出框想要查询则再开启为json
			datatype: 'local',
			width:_width,
			height: 220,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			multiselect:false,//多选
			viewrecords: true,
			cmTemplate: {sortable:false,title:false},
			page: 1, //只有一页
			pgbuttons:false,
			pgtext:false,
			rowNum:999,//每页条数
			shrinkToFit:false,//表格是否自动填充
			footerrow: true,
			jsonReader: {
				root: "data",
				repeatitems : false	,
				id: 'kpl_ki_code'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.gridTotal();
				THISPAGE.buildChart();
			},
			loadError: function(xhr, status, error){		
			}
	    });
		$("#grid").jqGrid('setGridParam', {datatype : 'local',data : rows}).trigger("reloadGrid");
		var groupHeaders = [];
		if (columns != null && columns.length > 0) {
			for ( var i = 0; i < columns.length; i++) {
				groupHeaders.push({startColumnName : "complete"+columns[i].kpl_code,numberOfColumns : 2, titleText : columns[i].kpl_name });
			}
		}
		$("#grid").jqGrid('setGroupHeaders', { 
			useColSpanStyle : true, // 没有表头的列是否与表头列位置的空单元格合并 
			groupHeaders : groupHeaders
		});
	},
	initEvent:function(){
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
	}
};

THISPAGE.init();