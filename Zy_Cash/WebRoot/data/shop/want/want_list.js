var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var wt_type = $("#wt_type").val();
var queryurl = config.BASEPATH+'shop/want/page';
var _height = $(parent).height()-152,_width = $(parent).width()-34;

var Utils = {
	doQueryShop : function(){
		commonDia = $.dialog({
			title : '选择要货门店',
			content : 'url:'+config.BASEPATH+'base/shop/list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#wt_shop_code").val(selected.sp_code);
				$("#shop_name").val(selected.sp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#wt_shop_code").val(selected.sp_code);
					$("#shop_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doQueryDepot : function(code,name){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#"+code).val(selected.dp_code);
				$("#"+name).val(selected.dp_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#"+code).val(selected.dp_code);
					$("#"+name).val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryEmp : function(){
		commonDia = $.dialog({
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
			data : {multiselect:false},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
				$("#wt_manager").val(selected.em_name);
			},
			close:function(){
				if(commonDia.content.dblClick){//双击关闭
					var selected = commonDia.content.doSelect();
					$("#wt_manager").val(selected.em_name);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		if(oper == 'add'){
			url = config.BASEPATH+"shop/want/to_add/"+wt_type;
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			url = config.BASEPATH+"shop/want/to_update?wt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'view'){
			url = config.BASEPATH+"shop/want/to_view?wt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'approve'){
			url = config.BASEPATH+"shop/want/to_view?wt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'send'){
			url = config.BASEPATH+"shop/want/to_send?wt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'receive'){
			url = config.BASEPATH+"shop/want/to_view?oper=receive&wt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else if(oper == 'rejectconfirm'){
			url = config.BASEPATH+"shop/want/to_view?wt_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		$.dialog({
			title : false,
			content : 'url:'+url,
			data: data,
			max : false,
			min : false,
			cache : false,
			lock: false
		}).max();
	},
	del: function(rowId){//删除
		var rowData = $("#grid").jqGrid("getRowData", rowId);
	 	$.dialog.confirm('数据删除后无法恢复，确定要清除吗？', function(){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+'shop/want/del',
				data:{"number":rowData.wt_number},
				cache:false,
				dataType:"json",
				success:function(data){
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : '删除成功!'});
						$('#grid').jqGrid('delRowData', rowId);
					}else{
						Public.tips({type: 1, content : data.message});
					}
				}
			});
		});	
    },
    callback: function(data, oper, dialogWin){
		if(oper == "send") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "receive") {
			THISPAGE.reloadData();
			dialogWin && dialogWin.api.close();
		}
		if(oper == "rejectconfirm") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
		if(oper == "view") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		}
	},
	operFmatter : function(val, opt, row){
		var btnHtml = '';
		if(wt_type == '0'){//补货
			btnHtml += '<input type="button" value="接收" class="btn_qh" onclick="javascript:handle.operate(\'receive\',' + opt.rowId + ');" />';
		}else if(wt_type == '1'){//退货
			btnHtml += '<input type="button" value="退货" class="btn_qh" onclick="javascript:handle.operate(\'send\',' + opt.rowId + ');" />';
		}
		return btnHtml;
	},
	formatArState:function(val, opt, row){//状态 0 待审批 1 已审核/待发货 2 已退回 3 已发货/在途 4 完成 5 拒收
		if(val == 0){
			return '未审核';
		}else if(val == 1){
			return '已审核';
		}else if(val == 2){
			return '已退回';
		}else if(val == 3){
			return '在途';
		}else if(val == 4){
			return '完成';
		}else if(val == 5){
			return '拒收';
		}
		return val;
	},
	formatGapAmount:function(val, opt, row){
		return row.wt_applyamount-row.wt_sendamount;
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'operate',label:'操作',width: 60, formatter: handle.operFmatter,align:'center', sortable:false},
	    	{name: 'wt_us_id',label:'',index: 'wt_us_id',width:100,hidden:true},
	    	{name: 'wt_ar_usid',label:'',index: 'wt_ar_usid',width:100,hidden:true},
	    	{name: 'wt_number',label:'单据编号',index: 'wt_number',width:150},
	    	{name: 'wt_date',label:'日期',index: 'wt_date',width:80},
	    	{name: 'shop_name',label:'店铺',index: 'wt_shop_code',width:120},
	    	{name: 'outdepot_name',label:'发货仓库',index: 'wt_outdp_code',width:120},
	    	{name: 'indepot_name',label:'收货仓库',index: 'wt_indp_code',width:120},
	    	{name: 'wt_ar_state',label:'状态',index: 'wt_ar_state',width:80,formatter: handle.formatArState},
	    	{name: 'wt_manager',label:'经办人',index: 'wt_manager',width:100},
	    	{name: 'wt_applyamount',label:'申请',index: 'wt_applyamount',width:80,align:'right'},
	    	{name: 'wt_sendamount',label:'实发',index: 'wt_sendamount',width:80,align:'right'},
	    	{name: 'sub_amount',label:'相差',index: 'wt_applyamount-wt_sendamount',width:80,align:'right',formatter: handle.formatGapAmount},
	    	{name: 'wt_applymoney',label:'申请金额',index: 'wt_applymoney',width:80,align:'right',formatter: Public.formatMoney},
	    	{name: 'wt_sendmoney',label:'实发金额',index: 'wt_sendmoney',width:80,align:'right',formatter: Public.formatMoney},
	    	{name: 'wt_sendcostmoney',label:'实发成本',index: 'wt_sendcostmoney',width:80,align:'right',formatter: Public.formatMoney},
	    	{name: 'wt_ar_date',label:'审核日期',index: 'wt_ar_date',width:80},
	    	{name: 'wt_maker',label:'制单人',index: 'wt_maker',width:100}
	    ];
		$('#grid').jqGrid({
            url:queryurl+'?'+THISPAGE.buildParams(),			
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:config.BASEROWNUM,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'wt_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += 'wt_type='+$("#wt_type").val();
		params += '&wt_isdraft=0';
		params += '&wt_ar_state=3';
		return params;
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		
	}
}
THISPAGE.init();