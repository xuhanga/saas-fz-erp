var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var isRefresh = false;
var wt_number = $("#wt_number").val();
var wt_type = $("#wt_type").val();
var wt_ar_state = $("#wt_ar_state").val();
var isFHD = parseInt($("#wt_applyamount").val())==0;
var isSend = $("#isSend").val() == "1";
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var queryurl = config.BASEPATH+'shop/want/detail_list/'+wt_number;
var querysumurl = config.BASEPATH+'shop/want/detail_sum/'+wt_number;
var querysizeurl = config.BASEPATH+'shop/want/detail_size/'+wt_number;
querysizeurl += '?wt_ar_state='+wt_ar_state+'&isFHD='+(isFHD?'1':'0')+'&isSend='+(isSend ?'1':'0');
var querysizetitleurl = config.BASEPATH+'shop/want/detail_size_title/'+wt_number;
var needListRefresh = false;//列表模式
var needSizeRefresh = false;//尺码模式
var needSumRefresh = false;//汇总模式
var _height = $(parent).height()-353,_width = $(parent).width()-2;
var showMode = {
	display:function(mode){
		var ids = $("#grid").getDataIDs();
		if(ids == undefined || null == ids || ids.length == 0){
			if(mode != 0){
				Public.tips({type: 1, content : "未添加数据，不能切换模式"});
				return;
			}
		}
		$("#CurrentMode").val(mode);
		$("a[name='mode-btn']").removeClass("on");
		$("#list-grid").hide();
		$("#size-grid").hide();
		$("#sum-grid").hide();
		if(mode == 0){
			$("#listBtn").addClass("on");
			$("#list-grid").show();
			showMode.refreshList();
		}else if(mode == 1){
			$("#sizeBtn").addClass("on");
			$("#size-grid").show();
			showMode.refreshSize();
		}else if(mode == 2){
			$("#sumBtn").addClass("on");
			$("#sum-grid").show();
			showMode.refreshSum();
		}
	},
	refreshList:function(){//列表模式刷新数据
		if(!needListRefresh){
    		return;
    	}
    	THISPAGE.reloadGridData();
    	needListRefresh = false;
	},
	refreshSum:function(){
		if(!needSumRefresh){
			return;
		}
		$('#sumGrid').GridUnload();
		THISPAGE.addEvent();
		THISPAGE.initSumGrid();
		needSumRefresh = false;
	},
	refreshSize:function(){
		if(!needSizeRefresh){
			return;
		}
		$.ajax({
			type:"POST",
			url:querysizetitleurl,
			cache:false,
			dataType:"json",
            success: function (data) {
            	if(undefined != data && data.stat == 200){
                    var titles = data.data.titles;
                    var sizeCodeMap = data.data.sizeCodeMap;
                    $('#sizeGrid').GridUnload();
                    THISPAGE.initSizeGroupGrid(titles, sizeCodeMap);
                    THISPAGE.addEvent();
                    needSizeRefresh = false;
				}else{
					Public.tips({type: 1, content : data.message});
				}
            }
        });
	},
	refresh:function(){
		var currentMode = $("#CurrentMode").val();
    	if(currentMode == '0'){//列表模式
    		needListRefresh = true;
    		showMode.refreshList();
        	needSizeRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '1'){//尺码模式
    		needSizeRefresh = true;
    		showMode.refreshSize();
    		needListRefresh = true;
        	needSumRefresh = true;
    	}else if(currentMode == '2'){//汇总模式
    		needSumRefresh = true;
    		showMode.refreshSum();
    		needListRefresh = true;
        	needSizeRefresh = true;
    	}
	}
};

var handle = {
	doReceive:function(){
		$.dialog.confirm('接收后将修改库存，确定接收吗？', function(){
			$("#btn-receive").attr("disabled",true);
			$("#btn-reject").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			params += "&wt_indp_code="+$("#wt_indp_code").val();
			$.ajax({
	            type: "POST",
	            url: config.BASEPATH+"shop/want/receive",
	            data: params,
	            cache:false,
				dataType:"json",
	            success: function (data) {
	            	if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "接收成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-receive").attr("disabled",false);
					$("#btn-reject").attr("disabled",false);
	            }
	        });
		});
	},
	doReject:function(){
		$.dialog.confirm('确定拒收吗？', function(){
			$("#btn-receive").attr("disabled",true);
			$("#btn-reject").attr("disabled",true);
			var params = "";
			params += "number="+wt_number;
			$.ajax({
				type: "POST",
				url: config.BASEPATH+"shop/want/reject",
				data: params,
				cache:false,
				dataType:"json",
				success: function (data) {
					if(undefined != data && data.stat == 200){
						Public.tips({type: 3, content : "拒收成功"});
						handle.loadData(data.data);
					}else{
						Public.tips({type: 1, content : data.message});
					}
					$("#btn-receive").attr("disabled",false);
					$("#btn-reject").attr("disabled",false);
				}
			});
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.wt_id;
		pdata.wt_number=data.wt_number;
		pdata.wt_ar_state=data.wt_ar_state;
		pdata.wt_ar_date=data.wt_ar_date;
		pdata.operate='';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	},
	operFmatter :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		if(isSend){
			html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
		}
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	operFmatter_List :function(val, opt, row){
		var html_con = '<div class="operating" data-id="' +opt.rowId+ '">';
		if(isSend){
			html_con += '<i class="iconfont i-hand ui-icon-pencil" title="修改">&#xe60c;</i>';
			html_con += '<i class="iconfont i-hand ui-icon-trash" title="删除">&#xe60e;</i>';
		}
		html_con += '<i class="iconfont i-hand ui-icon-image" title="图片">&#xe654;</i>';
		html_con += '</div>';
		return html_con;
	},
	formatGapAmount:function(val, opt, row){
		return row.wtl_applyamount-row.wtl_sendamount;
	},
	formatApplyMoney:function(val, opt, row){
		if(row.wtl_unitprice == undefined){
			return Public.formatMoney(val);
		}
		return Public.formatMoney(row.wtl_unitprice * row.wtl_applyamount);
	},
	formatSendMoney:function(val, opt, row){
		if(row.wtl_unitprice == undefined){
			return Public.formatMoney(val);
		}
		return Public.formatMoney(row.wtl_unitprice * row.wtl_sendamount);
	}
};

var Utils = {
	doQueryOutDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#wt_outdp_code").val(selected.dp_code);
					$("#outdepot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	},
	doQueryInDepot : function(){
		commonDia = $.dialog({
			title : '选择仓库',
			content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
			data : {multiselect:false,module:'want'},
			width : 450,
			height : 320,
			max : false,
			min : false,
			cache : false,
			lock: true,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#wt_indp_code").val(selected.dp_code);
					$("#indepot_name").val(selected.dp_name);
				}
			},
			cancel:true
		});
	}
}


var THISPAGE = {
	init:function (){
		this.initDom();
		this.addEvent();
		this.initGrid();
		this.initEvent();
	},
	initDom:function(){
		needListRefresh = false;//列表模式
        needSizeRefresh = true;//尺码模式
        needSumRefresh = true;//汇总模式
        this.selectRow={};
        var wt_ar_state = $("#wt_ar_state").val();
        if('receive' == api.data.oper){
			$("#btn-receive").show();
			$("#btn-reject").show();
		}
	},
	gridTotal:function(){
    	var grid=$('#grid');
		var wtl_applyamount=grid.getCol('wtl_applyamount',false,'sum');
		var wtl_sendamount=grid.getCol('wtl_sendamount',false,'sum');
    	var wtl_applymoney=grid.getCol('wtl_applymoney',false,'sum');
    	var wtl_sendmoney=grid.getCol('wtl_sendmoney',false,'sum');
    	grid.footerData('set',{
    		wtl_applyamount:wtl_applyamount,
    		wtl_sendamount:wtl_sendamount,
    		wtl_applymoney:wtl_applymoney,
    		wtl_sendmoney:wtl_sendmoney});
    	$("#wt_applyamount").val(wtl_applyamount);
    	$("#wt_sendamount").val(wtl_sendamount);
    	$("#wt_applymoney").val(Public.formatMoney(wtl_applymoney));
    	$("#wt_sendmoney").val(Public.formatMoney(wtl_sendmoney));
    },
    sumGridTotal:function(){
    	var grid=$('#sumGrid');
    	var wtl_applyamount=grid.getCol('wtl_applyamount',false,'sum');
		var wtl_sendamount=grid.getCol('wtl_sendamount',false,'sum');
    	var wtl_applymoney=grid.getCol('wtl_applymoney',false,'sum');
    	var wtl_sendmoney=grid.getCol('wtl_sendmoney',false,'sum');
    	grid.footerData('set',{
    		wtl_applyamount:wtl_applyamount,
    		wtl_sendamount:wtl_sendamount,
    		wtl_applymoney:wtl_applymoney,
    		wtl_sendmoney:wtl_sendmoney});
    	$("#wt_applyamount").val(wtl_applyamount);
    	$("#wt_sendamount").val(wtl_sendamount);
    	$("#wt_applymoney").val(Public.formatMoney(wtl_applymoney));
    	$("#wt_sendmoney").val(Public.formatMoney(wtl_sendmoney));
    },
	initGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter_List, sortable:false,align:'center'},
		    {label:'',name: 'wtl_pd_code', index: 'wtl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'cr_name', width: 60},
	    	{label:'尺码',name: 'sz_name', index: 'sz_name', width: 60},
	    	{label:'杯型',name: 'br_name', index: 'br_name', width: 60},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'申请数量',name: 'wtl_applyamount', index: 'wtl_applyamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'实发数量',name: 'wtl_sendamount', index: 'wtl_sendamount', width: 70,align:'right',sorttype: 'int',editable:isSend},
	    	{label:'相差数量',name: 'subamount', index: 'subamount', width: 70,align:'right',sorttype: 'int',formatter: handle.formatGapAmount},
	    	{label:'零售价',name: 'wtl_unitprice', index: 'wtl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney,editable:isSend},
	    	{label:'申请金额',name: 'wtl_applymoney', index: 'wtl_applymoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatApplyMoney},
	    	{label:'实发金额',name: 'wtl_sendmoney', index: 'wtl_sendmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSendMoney},
	    	{label:'备注',name: 'wtl_remark', index: 'wtl_remark', width: 180}
	    ];
		$('#grid').jqGrid({
			url:queryurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
			cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'wtl_id'  //图标ID
			},
			loadComplete: function(data){
				var ids = $("#grid").getDataIDs();
				THISPAGE.gridTotal();
				if((wt_ar_state == 0 || wt_ar_state == 1 || wt_ar_state == 2) && !isSend){
					if(isFHD){
						$("#grid").setGridParam().hideCol("wtl_applyamount").hideCol("subamount").hideCol("wtl_applymoney");
					}else{
						$("#grid").setGridParam().hideCol("wtl_sendamount").hideCol("subamount").hideCol("wtl_sendmoney");
					}
				}else{
					for(var i=0;i < ids.length;i++){
						var rowData = $("#grid").jqGrid("getRowData", ids[i]);
						if(rowData.subamount > 0){
							$("#grid").jqGrid('setRowData', ids[i], false, { color: '#FF0000' });
						}else if(rowData.subamount < 0){
							$("#grid").jqGrid('setRowData', ids[i], false, { color: 'blue' });
						}
					}
				}
			},
			loadError: function(xhr, status, error){		
			},
			formatCell:function (rowid, cellname, value, iRow, iCol){
            	self.selectRow.value=value;
				return value;
			},
			beforeSaveCell :function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'wtl_sendamount' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseInt(value) <= 0){
						return self.selectRow.value;
					} 
					if(parseInt(self.selectRow.value) == parseInt(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(0).length > 9|| parseInt(value) >1e+9){
						return self.selectRow.value;
					}
					handle.detail_updateAmount(rowid, parseInt(value));
					return parseInt(value);
				}else if(cellname == 'wtl_unitprice' && self.selectRow.value != value){
					if(value == '' || isNaN(value) || parseFloat(value) < 0){
						return self.selectRow.value;
					}
					if(parseFloat(self.selectRow.value) == parseFloat(value)){
						return self.selectRow.value;
					}
					if(parseFloat(value).toFixed(2).length > 11 || parseFloat(value) >1e+11){
						return self.selectRow.value;
					}
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					if(cellname == 'wtl_unitprice'){
						handle.detail_updatePrice(rowData.wtl_pd_code, parseFloat(value).toFixed(2));
					}
					return parseFloat(value).toFixed(2);
				}
				return value;
			},
			afterSaveCell:function(rowid, cellname, value, iRow, iCol){
				if (cellname == 'wtl_sendamount' && self.selectRow.value != value){
					var rowData = $("#grid").jqGrid("getRowData", rowid);
					rowData.wtl_sendmoney = rowData.wtl_sendamount * rowData.wtl_unitprice;
					rowData.subamount = rowData.wtl_applyamount - rowData.wtl_sendamount;
					$("#grid").jqGrid('setRowData', rowid, rowData);
					THISPAGE.gridTotal();
					if(rowData.subamount > 0){
						$("#grid").jqGrid('setRowData', rowid, false, { color: '#FF0000' });
					}else if(rowData.subamount < 0){
						$("#grid").jqGrid('setRowData', rowid, false, { color: 'blue' });
					}else{
						$("#grid").jqGrid('setRowData', rowid, false, { color: '#696969' });
					}
				}
				
			}
	    });
	},
	initSumGrid:function(){
		var self=this;
		var colModel = [
		    {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
	    	{label:'',name: 'wtl_pd_code', index: 'wtl_pd_code', hidden:true},
	    	{label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'品牌',name: 'bd_name', index: 'pd_bd_code', width: 80},
	    	{label:'类别',name: 'tp_name', index: 'pd_tp_code', width: 80},
	    	{label:'单位',name: 'pd_unit', index: 'pd_unit', width: 60},
	    	{label:'申请数量',name: 'wtl_applyamount', index: 'wtl_applyamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'实发数量',name: 'wtl_sendamount', index: 'wtl_sendamount', width: 70,align:'right',sorttype: 'int'},
	    	{label:'相差数量',name: 'subamount', index: 'subamount', width: 70,align:'right',sorttype: 'int',formatter: handle.formatGapAmount},
	    	{label:'零售价',name: 'wtl_unitprice', index: 'wtl_unitprice', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney},
	    	{label:'申请金额',name: 'wtl_applymoney', index: 'wtl_applymoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatApplyMoney},
	    	{label:'实发金额',name: 'wtl_sendmoney', index: 'wtl_sendmoney', width: 80,align:'right',sorttype: 'float',formatter: handle.formatSendMoney},
	    	{label:'备注',name: 'wtl_remark', index: 'wtl_remark', width: 180}
	    ];
		$('#sumGrid').jqGrid({
			url:querysumurl,
			loadonce:true,
			datatype: 'json',
			width: _width,
			height: _height,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#sumPage',//分页
			pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
			multiselect:false,//多选
			viewrecords: true,
			rowNum:9999,//每页条数
			cmTemplate: {sortable:true,title:true},
			shrinkToFit:false,//表格是否自动填充
            footerrow: true,
			jsonReader: {
				root: 'data', 
				repeatitems : false,
				id: 'wtl_id'  //图标ID
			},
			loadComplete: function(data){
				THISPAGE.sumGridTotal();
				var ids = $("#sumGrid").getDataIDs();
				if((wt_ar_state == 0 || wt_ar_state == 1 || wt_ar_state == 2) && !isSend){
					if(isFHD){
						$("#sumGrid").setGridParam().hideCol("wtl_applyamount").hideCol("subamount").hideCol("wtl_applymoney");
					}else{
						$("#sumGrid").setGridParam().hideCol("wtl_sendamount").hideCol("subamount").hideCol("wtl_sendmoney");
					}
				}else{
					for(var i=0;i < ids.length;i++){
						var rowData = $("#sumGrid").jqGrid("getRowData", ids[i]);
						if(rowData.subamount > 0){
							$("#sumGrid").jqGrid('setRowData', ids[i], false, { color: '#FF0000' });
						}else if(rowData.subamount < 0){
							$("#sumGrid").jqGrid('setRowData', ids[i], false, { color: 'blue' });
						}
					}
				}
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	initSizeGroupGrid:function(dyColNames, sizeCodeMap) {
		var self=this;
        var headers = [];
        var dyns = dyColNames;
        var dySize = dyns.length;
        var dms = [];
        for (var i = 0; i < dyns.length; i++) {
            var dys = dyns[i];
            for (var j = 0; j < dys.length; j++) {
                var dyss = null;
                dyss = dys[j].split('_AND_');
                if (i == 0 && j == 0) {
                    dyns[i][j] = dyss[0];
                }
                headers.push(dyss[1]);
                dms.push({label:dyss[1],name: dyss[0], index: dyss[0], width: 45, title: false, align: 'right', sorttype: 'int', sortable: false});
            }
            break;
        }
		var colModel = [
            {label:'',name: 'sizeGroupCode', hidden: true},
            {label:'',name: 'pd_code', index: 'pd_code', width: 100,hidden:true},
            {label:'',name: 'cr_code', index: 'cr_code', width: 100,hidden:true},
            {label:'',name: 'br_code', index: 'br_code', width: 100,hidden:true},
            {label:'操作',name:'operate', width: 70, fixed:true, formatter: handle.operFmatter, sortable:false,align:'center'},
            {label:'商品货号',name: 'pd_no', index: 'pd_no', width: 100},
	    	{label:'商品名称',name: 'pd_name', index: 'pd_name', width: 100},
	    	{label:'颜色',name: 'cr_name', index: 'wtl_cr_code', width: 80},
	    	{label:'杯型',name: 'br_name', index: 'wtl_br_code', width: 80},
        ];
        colModel = colModel.concat(dms);
        colModel = colModel.concat([
            {label:'数量',name: 'Amount_Total',	index:'Amount_Total',	width: 70, align: 'right', sorttype: 'int', sortable: false},
            {label:'零售价',name: 'unit_price', index: 'unit_price', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney},
	    	{label:'零售金额',name: 'unit_money', index: 'unit_money', width: 80,align:'right',sorttype: 'float',formatter: Public.formatMoney}
	    ]);
        $('#sizeGrid').jqGrid({
			url: querysizeurl,
			loadonce:true,
            datatype: 'json',
            autowidth: true,
			height: _height,
            gridview: true,
            colModel: colModel,
            rownumbers: true,//行号
            pager: '#sizePage',//分页
            pgbuttons:false,
			page:1,
			pgtext:false,
			recordtext:'共 {2} 条', 
            viewrecords: true,
            rowNum: 9999,//每页条数
            shrinkToFit: false,//表格是否自动填充
            cellEdit: true,
            cellsubmit: 'clientArray',
            footerrow: true,
            userDataOnFooter: true,
			jsonReader: {
				root: 'data.data',
				userdata: 'data.userData',
				repeatitems : false,
				id:'id'
			},
			loadComplete: function(data) {
				
			},
			gridComplete:function(){
				Public.resizeSpecifyGrid('sizeGrid', 290 + (dyns.length * 32),2);
			},
			loadError: function(xhr, status, error) {
                Public.tips({type: 1, content : '操作失败了哦，服务可能已过期，请按F5刷新！'});
			},
			onPaging: function(pgButton) {
				
			},
            onSelectRow: function (rowId, status) {
                var sizeG = $("#sizeGrid").jqGrid("getRowData", rowId).sizeGroupCode;
                var bg = $('[sizeGCode=' + sizeG + ']').css("background");
                if (bg == 'none') {
                    return;
                }
                $("[sizeGCode]").css("background-color", '#eee');
                $('[sizeGCode=' + sizeG + ']').css({ "background": "none", "background-color": "#F8FF94"});
            }
	    });
        if (dySize >= 2) {
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders:[]
            });
            $('#sizeGrid').jqGrid("setComplexGroupHeaders", {
                complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                rowspan: dySize,
                topTitle:true
            });
            for (var key in sizeCodeMap) {
                $('#sizeGrid').jqGrid("setComplexGroupHeaders",{
                    complexGroupHeaders: {startColumnName: dyns[0][0], numberOfColumns: dyns[0].length},
                    rowspan: dySize,
                    sizeGroupCode: key,
                    titleTextGroup:sizeCodeMap[key],
                    topTitle:false
                });
            }
        }else{
        	$('#sizeGrid').jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [{startColumnName: dyns[0][0], numberOfColumns: dyns[0].length, titleText: '尺码'}]
            });
        }
    },
	reloadGridData: function () {
        $("#grid").jqGrid('setGridParam', {datatype: 'json', page: 1, url: queryurl}).trigger("reloadGrid");
    },
	initEvent:function(){
		$('#btn-approve').on('click', function(e){
			e.preventDefault();
			handle.approve();
		});
		$('#btn-send').on('click', function(e){
			e.preventDefault();
			handle.doSend();
		});
		$('#btn-receive').on('click', function(e){
			e.preventDefault();
			handle.doReceive();
		});
		$('#btn-reject').on('click', function(e){
			e.preventDefault();
			handle.doReject();
		});
		$('#btn-reject-confirm').on('click', function(e){
			e.preventDefault();
			handle.doRejectConfirm();
		});
		$('#btn-reverse').on('click', function(e){
			e.preventDefault();
			handle.doReverse();
		});
		$("#btn_close").click(function(e){
			e.preventDefault();
			api.close();
		});
		//修改-列表模式
        $('#grid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	handle.detail_update(id, '0');
        });
        
        //删除-列表模式
		$('#grid').on('click', '.operating .ui-icon-trash', function(e) {
			e.preventDefault();
			var id = $(this).parent().data('id');
			handle.detail_del(id);
		});
		$('#grid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	Public.openImg($("#grid").jqGrid("getRowData", id).wtl_pd_code);
        });
	},
	addEvent:function(){
		//移除事件，防止重复绑定
		$('#sumGrid').off('click','.operating .ui-icon-pencil');
		$('#sizeGrid').off('click','.operating .ui-icon-pencil');
		$('#sumGrid').off('click','.operating .ui-icon-image');
		$('#sizeGrid').off('click','.operating .ui-icon-image');
		
		//修改-尺码模式
        $('#sizeGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.detail_update(id, '1');
        });
        //修改-汇总模式
        $('#sumGrid').on('click', '.operating .ui-icon-pencil', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
			handle.detail_update(id, '2');
        });
		$('#sizeGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	Public.openImg($("#sizeGrid").jqGrid("getRowData", id).pd_code);
        });
        $('#sumGrid').on('click', '.operating .ui-icon-image', function (e) {
        	e.preventDefault();
        	var id = $(this).parent().data('id');
        	Public.openImg($("#sumGrid").jqGrid("getRowData", id).wtl_pd_code);
        });
	}
};

THISPAGE.init();