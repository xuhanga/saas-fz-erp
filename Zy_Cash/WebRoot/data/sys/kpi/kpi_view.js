var defaultPage = Public.getDefaultPage();
var config = defaultPage.CONFIG;
var system = defaultPage.SYSTEM;
var api = frameElement.api;
var _height = api.config.height-112,_width = api.config.width-34;//获取弹出框弹出宽、高
var queryurl = config.BASEPATH+'sys/kpi/listScores?ki_code='+api.data.ki_code;

var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGridList();
		this.initEvent();
	},
	initDom:function(){
		
	},
	initGridList: function () {
        var colModel = [
            {label:'区间下限',name: 'ks_min',index: 'ks_min',width: 70,align:'right',formatter: Public.formatMoney},
            {label:'区间上限',name: 'ks_max',index: 'ks_max',width: 70,align:'right',formatter: Public.formatMoney},
            {label:'实际完成',name: 'realComplete',index: '',width: 70,align:'right'},
            {label:'分数',name: 'ks_score',index: 'ks_score',width: 70,align:'right'},
            {label:'奖励',name: 'reward_name',index: 'reward_name',width: 100,title:true},
            {label:'',name: 'ks_rw_code',index: 'ks_rw_code',width: 0,hidden:true}
        ];
        $("#gridDetail").jqGrid({
        	url: queryurl,
            datatype: 'json',
            height: _height,
			width:_width,
            colModel: colModel,
            rownumbers: true,
            rowNum: 999,
            pgbuttons:true,
            pager: '#pageDetail',
            recordtext:'{0} - {1} 共 {2} 条',
            gridview: true,//false构造一行数据后添加到grid中
            multiselect:false,//多选
            shrinkToFit: false,//ture，则按比例初始化列宽度
            forceFit: false,//调整列宽度会改变表格的宽度  shrinkToFit 为false时，此属性会被忽略
            viewrecords: true,//是否要显示总记录数信息  
            cmTemplate: {sortable:false,title:false},
            scroll:1,
            jsonReader: {
            	root: 'data',
				repeatitems : false
            },
            loadComplete: function (data) {
            	var ids = $("#gridDetail").jqGrid('getDataIDs');
            	var rowData = $('#gridDetail').jqGrid('getRowData', ids[0]);
            	var realComplete = api.data.realComplete;
        		if(parseFloat(realComplete) < parseFloat(rowData.ks_min)){
        			return;
        		}
        		rowData = $('#gridDetail').jqGrid('getRowData', ids[ids.length-1]);
        		if(parseFloat(realComplete) >= parseFloat(rowData.ks_max)){
        			$("#gridDetail").setRowData(ids[ids.length-1], {realComplete:realComplete},{ color: '#FF0000' });
        			return;
        		}
        		for(var i=0;i<ids.length;i++){
        			rowData = $('#gridDetail').jqGrid('getRowData', ids[i]);
        			if(parseFloat(realComplete) >= parseFloat(rowData.ks_min) && parseFloat(realComplete) < parseFloat(rowData.ks_max)){
        				$("#gridDetail").setRowData(ids[i], {realComplete:realComplete},{ color: '#FF0000' });
            			return;
        			}
        		}
            },
            loadError: function (xhr, st, err) {

            }
        });
    },
	initEvent:function(){
		
	}
};

THISPAGE.init();