var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;

var Utils = {
	calcAnima:function(){
		var vm_lunar_birth = $("#vm_lunar_birth").val();
		if(vm_lunar_birth == ''){
			return;
		}
		$("#vmi_anima").val(getAnima(vm_lunar_birth));
	},
	onchangeVm_Birthday:function(){
		var vm_birthday=$("#vm_birthday").val();
		var vm_lunar_birth = CalConvertStr(vm_birthday,true);
		if(vm_lunar_birth != undefined){
			vm_lunar_birth = DateToFullDateTimeString(new Date(Date.parse(vm_lunar_birth.replace(/-/g,"/"))));
			$("#vm_lunar_birth").val(vm_lunar_birth);
			
		}
		Utils.calcAnima();
	},
	onchangeVm_LunarBirthday:function(){
		var vm_lunar_birth=$("#vm_lunar_birth").val();
		var vm_birthday=CalConvertStr(vm_lunar_birth,false);
		if (vm_birthday == false) {
			W.Public.tips({type: 1, content : "农历日期输入不正确"});
			Utils.onchangeVm_Birthday();
			return false;
		}
		if(vm_birthday != undefined){
			vm_birthday = DateToFullDateTimeString(new Date(Date.parse(vm_birthday.replace(/-/g,"/"))));
			$("#vm_birthday").val(vm_birthday);
		}
		Utils.calcAnima();
	},
	doQueryEmp : function(){
		empDia = W.$.dialog({
			id:"threePageID",
			title : '选择经办人',
			content : 'url:'+config.BASEPATH+'cash/emp/to_emp_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: false,
			ok:function(){
				var selected = empDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = empDia.content.doSelect();
				if(selected){
					$("#vm_manager_code").val(selected.em_code);
					$("#vm_manager").val(selected.em_name);
				}
				try{setTimeout("api.zindex()",300);}catch(e){}
			},
			cancel:true
		});
	},
	doVip:function(){
		commonDia = W.$.dialog({
			id:"threePageID",
			title : "推荐人查询",
			content : 'url:'+config.BASEPATH+"vip/member/to_referee",
			width : 420,
			height : 280,
			max : false,
			min : false,
			cache : false,
			lock: false,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_referee").val(selected.vm_code);
					$("#referee_name").val(selected.vm_name);
					setTimeout("api.zindex()",300);
				}
			},
			cancel:true
		});
	}
};

var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 0, content : data.message});
					handle.loadData(data.data);
					setTimeout("api.close()",300);
					THISPAGE.initDom();
				}else{
					W.Public.tips({type: 1, content : data.message});
					if(data.message.indexOf("手机") > 0){
						$("#vm_mobile").select().focus();
					}
					setTimeout("api.zindex()",300);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.vm_id;
		pdata.vm_code=data.vm_code;
		pdata.vm_cardcode=data.vm_cardcode;
		pdata.vm_name=data.vm_name;
		pdata.vm_mt_code=data.vm_mt_code;
		pdata.vm_state=data.vm_state;
		pdata.vm_mobile=data.vm_mobile;
		pdata.vm_sex=data.vm_sex;
		pdata.vm_birthday_type=data.vm_birthday_type;
		pdata.vm_birthday=data.vm_birthday;
		pdata.vm_date=data.vm_date;
		pdata.vm_manager=$("#vm_manager").val();
		pdata.vm_shop_code=data.vm_shop_code;
		pdata.mt_name= $('#span_membertype').getCombo().getText();
		pdata.shop_name= $('#shop_name').val();
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var comboOpts = {
	value : 'Code',
	text : 'Name',
	width : 142,
	height : 300,
	listHeight : 300,
	listId : '',
	defaultSelected : 0,
	editable : false
};

var ComboUtil = {
	membertypeCombo : function(){
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vm_mt_code").val(data.mt_code);
				}
			}
		},comboOpts,{value : 'mt_code',text : 'mt_name'});
		var memType = $('#span_membertype').combo(opts).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/listType",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					memType.loadData(data.data);
				}
			}
		 });
	},
	sexCombo : function() {
		var data = [ 
                {Code : '男',Name : '男'}, 
                {Code : '女',Name : '女'} 
              ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vm_sex").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_sex').combo(opts).getCombo().loadData(data);
	},
	birthdayTypeCombo : function(){
		var data = [ 
                {Code : '0',Name : '公历'}, 
                {Code : '1',Name : '农历'} 
              ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vm_birthday_type").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_birthdaytype').combo(opts).getCombo().loadData(data);
	},
	stateCombo : function(){
		/*var data = [ 
                {Code : '0',Name : '正常'}, 
                {Code : '1',Name : '无效'},
                {Code : '2',Name : '挂失'}
              ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vm_state").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_state').combo(opts).getCombo().loadData(data);*/
	},
	idcardTypeCombo : function(){
		var data = [ 
                 {Code : '0',Name : '身份证'}, 
                 {Code : '1',Name : '军官证'},
                 {Code : '2',Name : '驾照'},
                 {Code : '3',Name : '护照'},
                 {Code : '4',Name : '学生证'},
                 {Code : '5',Name : '其他'}
                 ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_idcard_type").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_idcard_type').combo(opts).getCombo().loadData(data);
	},
	footTypeCombo : function(){
		var data = [ 
              	{Code:'正常',Name:'正常'},
				{Code:'偏胖',Name:'偏胖'},
				{Code:'偏瘦',Name:'偏瘦'},
				{Code:'脚面扁平',Name:'脚面扁平 '},
				{Code:'脚面偏高',Name:'脚面偏高'}
                ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_foottype").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_foottype').combo(opts).getCombo().loadData(data);
	},
	marriageStateCombo : function(){
		var data = [ 
                {Code:'0',Name:'未婚'},
                {Code:'1',Name:'已婚'},
                {Code:'2',Name:'曾婚'}
                ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_marriage_state").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_marriage_state').combo(opts).getCombo().loadData(data);
	},
	nationCombo : function(){
		var data = [ 
				{Code:'汉族',Name:'汉族'},{Code:'蒙古族',Name:'蒙古族'},{Code:'回族',Name:'回族'},{Code:'藏族',Name:'藏族'},{Code:'维吾尔族',Name:'维吾尔族'},
				{Code:'苗族',Name:'苗族'},{Code:'彝族',Name:'彝族'},{Code:'壮族',Name:'壮族'},{Code:'布依族',Name:'布依族'},{Code:'朝鲜族',Name:'朝鲜族'},
				{Code:'满族',Name:'满族'},{Code:'侗族',Name:'侗族'},{Code:'瑶族',Name:'瑶族'},{Code:'白族',Name:'白族'},{Code:'土家族',Name:'土家族'},
				{Code:'哈尼族',Name:'哈尼族'},{Code:'哈萨克族',Name:'哈萨克族'},{Code:'傣族',Name:'傣族'},{Code:'黎族',Name:'黎族'},{Code:'傈僳族',Name:'傈僳族'},
				{Code:'佤族',Name:'佤族'},{Code:'畲族',Name:'畲族'},{Code:'高山族',Name:'高山族'},{Code:'拉祜族',Name:'拉祜族'},{Code:'水族',Name:'水族'},
				{Code:'东乡族',Name:'东乡族'},{Code:'纳西族',Name:'纳西族'},{Code:'景颇族',Name:'景颇族'},{Code:'柯尔克孜族',Name:'柯尔克孜族'},{Code:'土族',Name:'土族'},
				{Code:'达斡尔族',Name:'达斡尔族'},{Code:'仫佬族',Name:'仫佬族'},{Code:'羌族',Name:'羌族'},{Code:'布朗族',Name:'布朗族'},{Code:'撒拉族',Name:'撒拉族'},
				{Code:'毛难族',Name:'毛难族'},{Code:'仡佬族',Name:'仡佬族'},{Code:'锡伯族',Name:'锡伯族'},{Code:'阿昌族',Name:'阿昌族'},{Code:'普米族',Name:'普米族'},
				{Code:'塔吉克族',Name:'塔吉克族'},{Code:'怒族',Name:'怒族'},{Code:'乌孜别克族',Name:'乌孜别克族'},{Code:'俄罗斯族',Name:'俄罗斯族'},{Code:'鄂温克族',Name:'鄂温克族'},
				{Code:'崩龙族',Name:'崩龙族'},{Code:'保安族',Name:'保安族'},{Code:'裕固族',Name:'裕固族'},{Code:'京族',Name:'京族'},
				{Code:'塔塔尔族',Name:'塔塔尔族'},{Code:'独龙族',Name:'独龙族'},{Code:'鄂伦春族',Name:'鄂伦春族'},{Code:'赫哲族',Name:'赫哲族'},
				{Code:'门巴族',Name:'门巴族'},{Code:'珞巴族',Name:'珞巴族'},{Code:'基诺族',Name:'基诺族'}
                ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_nation").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_nation').combo(opts).getCombo().loadData(data);
	},
	jobCombo : function(){
		var data = [ 
				{Code:'老板',Name:'老板'},
				{Code:'企业管理者',Name:'企业管理者'},
				{Code:'公务员',Name:'公务员'},
				{Code:'教师',Name:'教师'},
				{Code:'职员',Name:'职员'},
				{Code:'军人',Name:'军人'},
				{Code:'学生',Name:'学生'},
				{Code:'医生',Name:'医生'},
				{Code:'其他',Name:'其他'}
		        ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_job").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_job').combo(opts).getCombo().loadData(data);
	},
	cultureCombo : function(){
		var data = [ 
				{Code:'高中及以下',Name:'高中及以下'},
				{Code:'大专',Name:'大专'},
				{Code:'本科',Name:'本科'},
				{Code:'研究生',Name:'研究生'},
				{Code:'博士及以上',Name:'博士及以上'},
				{Code:'其他',Name:'其他'}
		        ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_culture").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_culture').combo(opts).getCombo().loadData(data);
	},
	bloodtypeCombo : function(){
		var data = [ 
				{Code:'A',Name:'A'},
				{Code:'B',Name:'B'},
				{Code:'AB',Name:'AB'},
				{Code:'O',Name:'O'}
		        ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_bloodtype").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_bloodtype').combo(opts).getCombo().loadData(data);
	},
	incomeLevelCombo : function(){
		var data = [ 
				{Code:'保密',Name:'保密'},
				{Code:'1000以下',Name:'1000以下'},								
				{Code:'1000-3000',Name:'1000-3000'},
				{Code:'3000-5000',Name:'3000-5000'},
				{Code:'5000-8000',Name:'5000-8000'},
				{Code:'10000以上',Name:'10000以上'}
		        ];
		var opts = $.extend(true,{
			callback : {
				onChange : function(data) {
					$("#vmi_income_level").val(data.Code);
				}
			}
		},comboOpts);
		$('#span_income_level').combo(opts).getCombo().loadData(data);
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
		$("#vm_manager_code").val(system.EM_CODE);
		$("#vm_manager").val(system.EM_NAME);
		$("#vm_shop_code").val(system.SHOP_CODE);
		$("#shop_name").val(system.SHOP_NAME);
		$("#vm_mobile").focus();
	},
	initDom:function(){
		var currentDate = new Date();
		$("#vm_date").val(DateToFullDateTimeString(currentDate));
		currentDate.setYear(currentDate.getFullYear() + 20); 
		$("#vm_enddate").val(DateToFullDateTimeString(currentDate));
		$("#vm_mobile").val("").focus();
		ComboUtil.membertypeCombo();
		ComboUtil.sexCombo();
		ComboUtil.birthdayTypeCombo();
//		ComboUtil.stateCombo();//发卡里，状态基本都是正常的
		ComboUtil.idcardTypeCombo();
		ComboUtil.footTypeCombo();
		ComboUtil.marriageStateCombo();
		ComboUtil.nationCombo();
		ComboUtil.jobCombo();
		ComboUtil.cultureCombo();
		ComboUtil.bloodtypeCombo();
		ComboUtil.incomeLevelCombo();
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$("#btn_referee").on('click',function(){
			Utils.doVip();
		});
	}
};
THISPAGE.init();