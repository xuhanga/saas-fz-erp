var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var vip_id = api.data.vip_id;
var comboOpts = {
	value : 'Code',
	text : 'Name',
	width : 142,
	height : 300,
	listHeight : 300,
	listId : '',
	defaultSelected : 0,
	editable : false
};
var Format = {
	styleFormat:function(val, opt, row){
		var html =  row.cr_name+"/"+row.sz_name;
		if(null != row.br_name && "" != row.br_name){
			html += "/"+row.br_name;
		}
		return html;
	}
};
var Tab = function(showId){
	$("#menu1").removeClass().addClass("menu");
	$("#menu2").removeClass().addClass("menu");
	$("#menu3").removeClass().addClass("menu");
	$("#menu"+showId).removeClass().addClass("select");
	$("#tagContent1").hide();
	$("#tagContent2").hide();
	$("#tagContent3").hide();
	$("#tagContent"+showId).show();
	if(showId == 2){
		THISPAGE.loadSell();
	}
	if(showId == 3){
		THISPAGE.loadVip();
	}
};
var THISPAGE = {
	init:function (){
		this.initParam();
		this.initDom();
		this.initSellGrid();
		this.initVipGrid();
		this.initEvent();
	},
	initParam:function(){
		$("input[type='text']").each(function(){
			$(this).prop("disabled",true);
		});
	},
	initDom:function(){
		var vm_state = $("#vm_state").val();
		if(vm_state == 1){
			$("#_vm_state").val("停用");
		}else{
			$("#_vm_state").val("正常");
		}
	},
	initSellGrid:function(){
		var colModel = [
            {name: 'shop_name',label:'店铺',index: 'shop_name', width: 100},
            {name: 'main_name',label:'导购',index: 'main_name',width: 70},
        	{name: 'pd_no',label:'货号',index: 'pd_no',width:80},            
	    	{name: 'pd_name',label:'名称',index: 'pd_name', width:100, title: false},
	    	{name: 'pd_style',label:'规格',index: 'pd_style', width: 130,title: false,formatter:Format.styleFormat},
	    	{name: 'cr_name',label:'颜色',index: 'cr_name', hidden:true},
	    	{name: 'sz_name',label:'尺码',index: 'sz_name', hidden:true},
	    	{name: 'br_name',label:'杯型',index: 'br_name', hidden:true},
	    	{name: 'shl_price',label:'折后价',index: 'shl_price',align:'right', width: 60,title: false},
	    	{name: 'shl_amount',label:'数量',index: 'shl_amount',align:'right', width: 50,title: false},
	    	{name: 'shl_money',label:'金额',index: 'shl_money',align:'right', width: 60, title: false}
	    ];
		$('#grid').jqGrid({
			datatype: 'lcoal',
			width: 746,
			height: 381,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rowNum:99,
			rownumbers: true,//行号
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root:'data',
	            repeatitems: false,
	            id: "shl_id"
            },
			gridComplete: function(data){
				
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	loadSell:function(){
		var url = config.BASEPATH+"vip/member/listSell";
		var param = "?vip_code="+$("#vm_code").val();
		$("#grid").jqGrid('setGridParam',{datatype:"json",url:url+param}).trigger("reloadGrid");
	},
	initVipGrid:function(){
		var colModel = [
            {name: 'vpl_date',label:'日期',index: 'vpl_date', width: 150,formatter:Public.dateFormat},
            {name: 'vpl_manager',label:'经办人',index: 'vpl_manager',width: 60},
            {name: 'vpl_shop_name',label:'店铺',index: 'vpl_shop_name', width: 120,title: false},
        	{name: 'vpl_point',label:'积分',index: 'vpl_point',width:80},            
	    	{name: 'dt_name',label:'类别',index: 'dt_name', width:80, title: false},
	    	{name: 'vpl_remark',label:'备注',index: 'vpl_remark',width: 160,title:true}
	    ];
		$('#gridData').jqGrid({
			datatype: 'local',
			width: 746,
			height: 381,
			altRows:true,
			gridview: true,
			onselectrow: false,
			colModel: colModel,
			rowNum:99,
			rownumbers: true,//行号
			multiselect:false,//多选
			viewrecords: true,
			pgbuttons:true,
			shrinkToFit:false,//表格是否自动填充
			autoScroll: true,
			jsonReader: {
				root:'data',
	            repeatitems: false,
	            id: "vpl_id"
            },
			gridComplete: function(data){
				
			},
			loadError: function(xhr, status, error){
			}
	    });
	},
	loadVip:function(){
		var url = config.BASEPATH+"vip/member/listPoint";
		var param = "?vip_code="+$("#vm_code").val();
		$("#gridData").jqGrid('setGridParam',{datatype:"json",url:url+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
		$("#btn_referee").on('click',function(){
			Utils.doVip();
		});
	}
};
THISPAGE.init();