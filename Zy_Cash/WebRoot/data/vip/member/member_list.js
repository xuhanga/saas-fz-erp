var config = parent.CONFIG,system=parent.SYSTEM;
var isRefresh = false;
var queryurl = config.BASEPATH+'vip/member/page';
var api = frameElement.api, W = api.opener;
var vipInfo = api.data.vipInfo;
var Utils = {
	doQueryShop : function(){
		commonDia = W.$.dialog({
			title : '选择店铺',
			content : 'url:'+config.BASEPATH+'base/shop/to_list_sub_dialog',
			data : {multiselect:false},
			width : 450,
			height : 370,
			max : false,
			min : false,
			cache : false,
			lock: false,
			ok:function(){
				var selected = commonDia.content.doSelect();
				if (!selected) {
					return false;
				}
			},
			close:function(){
				var selected = commonDia.content.doSelect();
				if(selected){
					$("#vm_shop_code").val(selected.sp_code);
					$("#sp_name").val(selected.sp_name);
				}
			},
			cancel:true
		});
	},
	doVipInfo:function(vip_id){
		if(null == vip_id || "" == vip_id){
			W.Public.tips({type: 2, content : "会员信息出错!"});
			return;
		}
		var paramData = {};
		paramData.title = "会员分析";
		paramData.width = "765";
		paramData.height = "532";
		paramData.url = config.BASEPATH+"vip/member/to_info?vip_id="+vip_id;
		paramData.data={vip_id:vip_id};
		paramData.lock=false;
	    W.Public.openDialog(paramData);
	}
};

var handle = {
	operate: function(oper, id){//修改、新增
		var height = 450;
		var width = 780;
		if(oper == 'add'){
			title = '新增';
			url = config.BASEPATH+"vip/member/to_add";
			data = {oper: oper, callback:this.callback};
		}else if(oper == 'edit'){
			title = '修改';
			url = config.BASEPATH+"vip/member/to_update?vm_id="+id;
			data = {oper: oper,callback: this.callback};
		}else{
			return false;
		}
		W.$.dialog({
			id:"twoPageID",
			title : title,
			content : 'url:'+url,
			data: data,
			width : width,
			height : height,
			max : false,
			min : false,
			drag: false,
			lock: false,
			close:function(){
				try{setTimeout("api.zindex()",300);}catch(e){}
			}
		});
	},
    callback: function(data, oper, dialogWin){
		var gridData = $("#grid").data('gridData');
		if(!gridData){
			gridData = {};
			$("#grid").data('gridData', gridData);
		}
		gridData[data.id] = data;
		if(oper == "edit") {
			$("#grid").jqGrid('setRowData', data.id, data);
			dialogWin && dialogWin.api.close();
		} 
		if(oper == "add"){
			$("#grid").jqGrid('addRowData', data.id, data, 'first');
		}
	},
	formatState:function(val, opt, row){//0:正常 1:无效 2:挂失
		if(val == "0"){
			return "正常";
		}else if(val == "1"){
			return "无效";
		}else if(val == "2"){
			return "挂失";
		}else{
			return val;
		}
	},
	formatBirthdayType:function(val, opt, row){//0公历生日1为农历生日
		if(val == "0"){
			return "公历";
		}else if(val == "1"){
			return "农历";
		}else{
			return val;
		}
	},
	formatBirthday:function(val, opt, row){//0公历生日1为农历生日
		if(row.vm_birthday_type == "0"){
			return row.vm_birthday;
		}else if(row.vm_birthday_type == "1"){
			return row.vm_lunar_birth;
		}else{
			return '';
		}
	},
	doVipRate:function(id){
		if(null == id && "" == id){
			W.Public.tips({type: 2, content : "请选择会员!"});
			return;
		}
		var data = $("#grid").jqGrid('getRowData', id);
		var vip_code = data.vm_cardcode;
		if(null == vip_code || '' == vip_code){
			W.Public.tips({type: 2, content : "请选择会员!"});
			return;
		}
		var vip_other = _self.$_vip_other.chkVal().join()?1:0;
		var sell_type = W.$("#sell_type").val();
		var ids = W.$("#grid").jqGrid('getDataIDs');
		if(ids.length > 0){
			$.ajax({
				type:"POST",
				url:config.BASEPATH+"cash/vipRate",
				data:{"vip_code":vip_code,"vip_other":vip_other,"sell_type":sell_type},
				cache:false,
				dataType:"json",
				success:function(data){
					if (data.stat == 200){
						W.flag = true;
						if(vipInfo && typeof vipInfo == 'function'){
							vipInfo(data.data);
						}
						setTimeout("api.close()",200);
					}else{
						W.flag = false;
						W.Public.tips({type:1,content:'打折失败!'});
					}
				}
			});
		}else{
			W.Public.tips({type:1,content:'请选择商品!'});
			return;
		}
	}
};
var THISPAGE = {
	init:function (){
		this.initDom();
		this.initGrid();
		this.initEvent();	
	},
	initDom:function(){
		_self = this;
		_self.$_vip_other = $("#vip_other").cssCheckbox();
	},
	initGrid:function(){
		var colModel = [
	    	{name: 'operate',label:'操作',width:35, formatter: Public.operEditFmatter,align:'center', sortable:false},
	    	{name: 'vm_code',label:'编号',index: 'vm_code',width:0,hidden:true},
	    	{name: 'vm_cardcode',label:'卡号',index: 'vm_cardcode',width:120},
	    	{name: 'vm_name',label:'名称',index: 'vm_name',width:75},
	    	{name: 'mt_name',label:'类别',index: 'vm_mt_code',width:60},
	    	{name: 'vm_state',label:'状态',index: 'vm_state',align:'center',width:50,formatter:handle.formatState},
	    	{name: 'vm_mobile',label:'手机',index: 'vm_mobile',width:96},
	    	{name: 'vm_sex',label:'性别',index: 'vm_sex',align:'center',width:40},
	    	{name: 'vm_birthday_type',label:'方式',index: 'vm_birthday_type',align:'center',width:60,formatter:handle.formatBirthdayType},
	    	{name: 'vm_birthday',label:'生日',index: 'vm_birthday',width:82,formatter:handle.formatBirthday},
	    	{name: 'vm_date',label:'办卡日期',index: 'vm_date',width:82},
	    	{name: 'shop_name',label:'发卡门店',index: 'vm_shop_code',width:120}
	    ];
		$('#grid').jqGrid({
			datatype: 'json',
			width: 933,
			height: 388,
			gridview: true,
			colModel: colModel,
			rownumbers: true,//行号
			pager: '#page',//分页
			pgbuttons:true,
			recordtext:'{0} - {1} 共 {2} 条',  
			multiselect:false,//多选
			viewrecords: true,
			rowNum:99,//每页条数
			rowList:config.BASEROWLIST,//分页条数
			cmTemplate: {sortable:true,title:false},
			shrinkToFit:false,//表格是否自动填充
			jsonReader: {
				root: 'data.list',
				total: 'data.pageInfo.totalPage',
                page: 'data.pageInfo.currentPage',
                records: 'data.pageInfo.totalRow',
				repeatitems : false,
				id: 'vm_id'  //图标ID
			},
			loadComplete: function(data){
			},
			loadError: function(xhr, status, error){		
			}
	    });
	},
	buildParams : function(){
		var params = '';
		params += '&vm_shop_code='+$("#vm_shop_code").val();
		params += '&vm_cardcode='+Public.encodeURI($("#vm_cardcode").val());
		params += '&vm_name='+Public.encodeURI($("#vm_name").val());
		return params;
	},
	reset:function(){
		$("#vm_shop_code").val("");
		$("#vm_cardcode").val("");
		$("#vm_name").val("");
		$("#sp_name").val("");
	},
	reloadData:function(){
		var param=THISPAGE.buildParams();
		$("#grid").jqGrid('setGridParam',{datatype:"json",page:1,url:queryurl+'?'+param}).trigger("reloadGrid");
	},
	initEvent:function(){
		//查询
		$('#btn-search').on('click', function(e){
			e.preventDefault();
			THISPAGE.reloadData();
		});
		$('#btn_ok').on('click', function(e){
			e.preventDefault();
			Public.hideConstion();
			THISPAGE.reloadData();
		});
		//新增
		$('#btn-add').on('click', function(e){
			e.preventDefault();
			if(1 != system.VIP_ADD){
				Public.tips({type: 1, content : "无新增权限!"});
				return;
			}
			handle.operate('add');
		});
		//修改
		$('#grid').on('click', '.operating .ui-icon-pencil', function(e){
			e.preventDefault();
			if(1 != system.VIP_EDIT){
				Public.tips({type: 1, content : "无修改权限!"});
				return;
			}
			var id = $(this).parent().data('id');
			handle.operate('edit', id);
		});
		$('#btn_reset').on('click', function(e){
			e.preventDefault();
			THISPAGE.reset();
		});
		$("#btn-report").on('click',function(e){
			e.preventDefault();
			var id = $("#grid").jqGrid('getGridParam', 'selrow');
			Utils.doVipInfo(id);
		});
		$("#btn_shop").click(function(){
			Utils.doQueryShop();
		});
		$("#btn-rate").click(function(e){
			e.preventDefault();
			var id = $("#grid").jqGrid('getGridParam', 'selrow');
			handle.doVipRate(id);
		});
	}
};
THISPAGE.init();