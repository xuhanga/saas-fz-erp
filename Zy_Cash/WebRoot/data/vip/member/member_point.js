var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	queryVip:function(code){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'vip/member/vipByCode',
			data:{"cardcode":code},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					$("#vm_id").val(data.data.vm_id);
					$("#vm_name").val(data.data.vm_name);
					$("#vm_points").val(data.data.vm_points);
					$("#vm_mobile").val(data.data.vm_mobile);
					$("#vm_cardcode").val(data.data.vm_cardcode);
					$("#vm_code").val(data.data.vm_code);
					$("#point").select().focus();
				}else{
					Public.tips({type: 1, content : "查询失败"});
				}
			}
		});
	},
	save:function(id){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"vip/member/updatePoint",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.Public.tips({type: 0, content : data.message});
					setTimeout("api.close()",300);
				}else{
					W.Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",300);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	print:function(){
		var print = _self.$_ischeck.chkVal().join()?1:0;
		if(print == 1){//选中打印
			var temp = "<table><tr><td align='center'>.</td></tr></table>";
			LODOP.SET_PRINT_STYLE("FontSize","10");
			LODOP.ADD_PRINT_TEXT("3mm",'2mm','50mm','15mm','单据类型：积分管理');
			LODOP.ADD_PRINT_TEXT("8mm",'2mm','50mm','15mm','会员卡号：'+$("#vm_cardcode").val());
			LODOP.ADD_PRINT_TEXT("13mm",'2mm','50mm','15mm','会员姓名：'+$("#vm_name").val());
			LODOP.ADD_PRINT_TEXT("18mm",'2mm','50mm','15mm','手机号：'+$("#vm_mobile").val());
			var vm_points = $("#vm_points").val();
			LODOP.ADD_PRINT_TEXT("23mm",'2mm','50mm','15mm','原有积分：'+vm_points);
			var totalpoint = 0;
			var point = $("#point").val();
			if($("#option").val() != "1"){
				LODOP.ADD_PRINT_TEXT("28mm",'2mm','50mm','15mm','增加积分：'+point);
				totalpoint = parseFloat(vm_points)+parseFloat(point);
			}else{
				LODOP.ADD_PRINT_TEXT("28mm",'2mm','50mm','15mm','减少积分：'+point);
				totalpoint = parseFloat(vm_points)-parseFloat(point);
			}
			
			LODOP.ADD_PRINT_TEXT("33mm",'2mm','50mm','15mm','现有积分：'+totalpoint);
			LODOP.ADD_PRINT_TEXT("38mm",'2mm','50mm','15mm','操作日期：'+config.TODAY);
			LODOP.ADD_PRINT_TEXT("43mm",'2mm','50mm','15mm','操作店铺：'+system.SHOP_NAME);
			LODOP.ADD_PRINT_TEXT("48mm",'2mm','50mm','15mm','备注：'+$("#vpl_remark").val());
			LODOP.ADD_PRINT_HTML("53mm", "2mm",'100%',"100%", temp);
			LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT", "Auto-Width");//整宽不变形
			LODOP.PREVIEW();
		}
	}
};
var THISPAGE = {
	init:function(){
		this.initParam();
		this.initCombo();
		this.initEvent();
	},
	initParam:function(){
		$("#vm_cardcode").focus();
		this.$_ischeck = $("#ischeck").cssCheckbox();
		_self = this;
	},
	initCombo:function(){
		var data = [ 
            {id : '0',name : '加'}, 
            {id : '1',name : '减'} 
          ];
		operCombo = $('#spanOper').combo({
			data:data,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#option").val(data.id);
				}
			}
		}).getCombo();
	},
	initEvent:function(){
		$("#cardcode").keyup(function(e){
			if(e.keyCode==13){
				var code = $.trim($(this).val());
				handle.queryVip(code);
			}
		});
		$("#btn-save").on('click',function(){
			handle.save();
		});
		$("#btn_close").on('click',function(){
			api.close();
		});
	}
};
THISPAGE.init();