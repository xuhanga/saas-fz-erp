var config = parent.parent.CONFIG,system=parent.parent.SYSTEM;
if(!config){
	config = parent.parent.parent.CONFIG;
	system = parent.parent.parent.SYSTEM;
}
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var handle = {
	save:function(){
		if (!$("#form1").valid()) {
			return;
		}
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"voucher/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					handle.loadData(data.data);
					setTimeout("api.close()",200);
					setTimeout("handle.print()",300);
				}else{
					Public.tips({type: 1, content : data.message});
					setTimeout("api.zindex()",100);
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	print:function(){
		var print = _self.$_ischeck.chkVal().join()?1:0;
		if(print == 1){//选中打印
			var temp = "<table><tr><td align='center'>.</td></tr></table>";
			LODOP.SET_PRINT_STYLE("FontSize","10");
			LODOP.SET_PRINT_PAGESIZE(3,"60mm",'2mm', '');
			LODOP.ADD_PRINT_TEXT("3mm",'2mm','50mm','15mm','类型：发代金券');
			LODOP.ADD_PRINT_TEXT("8mm",'2mm','50mm','15mm','代金券号：'+$.trim($("#vc_cardcode").val()));
			LODOP.ADD_PRINT_TEXT("13mm",'2mm','50mm','15mm','发放日期：'+$.trim($("#vc_grantdate").val()));
			LODOP.ADD_PRINT_TEXT("18mm",'2mm','50mm','15mm','有效日期：'+$.trim($("#vc_enddate").val()));
			LODOP.ADD_PRINT_TEXT("23mm",'2mm','50mm','15mm','面    值：'+$.trim($("#vc_money").val()));
			LODOP.ADD_PRINT_TEXT("28mm",'2mm','50mm','15mm','实收现金：'+$.trim($("#vc_realcash").val()));
			LODOP.ADD_PRINT_TEXT("33mm",'2mm','50mm','15mm','发卡店铺：'+$.trim($("#sp_name").val()));
			LODOP.ADD_PRINT_TEXT("38mm",'2mm','50mm','15mm','经办人员：'+$.trim($("#vc_manager").val()));
			LODOP.ADD_PRINT_HTML("43mm", "2mm",'100%',"100%", temp);
			LODOP.PREVIEW();
		}
	},
	loadData:function(data){
		var pdata = {};
		pdata.id=data.vm_id;
		pdata = data;
		pdata.shop_name = $("#shop_name").val();
		pdata.left_money = '';
		if(callback && typeof callback == 'function'){
			callback(pdata,oper,window);
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initCombo();
		this.initDom();
		this.initEvent();
	},
	initParam:function(){
		$("#vc_cardcode").focus();
		$("#vc_manager").val(system.EM_NAME);
		$("#vc_shop_code").val(system.SHOP_CODE);
		$("#shop_name").val(system.SHOP_NAME);
		this.$_ischeck = $("#ischeck").cssCheckbox();
		_self = this;
	},
	initCombo:function(){
		bankCombo = $('#spanBank').combo({
	        value: 'ba_code',
	        text: 'ba_name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#vcl_ba_code").val(data.ba_code);
				}
			}
		}).getCombo();
		$.ajax({
			type:"POST",
			url:config.BASEPATH+'money/bank/list',
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					bankCombo.loadData(data.data);
				}
			}
		});
	},
	initDom:function(){
		var currentDate = new Date();
		$("#vc_grantdate").val(DateToFullDateTimeString(currentDate));
		currentDate.setYear(currentDate.getFullYear() + 2); 
		$("#vc_enddate").val(DateToFullDateTimeString(currentDate));
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();