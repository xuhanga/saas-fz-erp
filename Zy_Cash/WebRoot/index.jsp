<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<% 
	request.getRequestDispatcher("/index").forward(request, response);
%>


<table cellpadding="4" cellspacing="0" class="data-print">
	<thead>
		<tr>
			<th><div style="width='18mm'">货号名称</div></th>
			<th><div style="width='15mm'">规格</div></th>
			<th><div style="width='6mm'">数量</div></th>
			<th><div style="width='8mm'">零售价</div></th>
			<th><div style="width='8mm'">折后价</div></th>
			<th><div style="width='10mm'">折后金额</div></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style='text-align:left;min-width:18mm;'>10002#鞋子2017</td>
			<td style='text-align:center;min-width:15mm;'>绿黄色/XL/B</td>
			<td style='text-align:right;min-width:6mm;'>1</td>
			<td style='text-align:right;min-width:8mm;'>222.0</td>
			<td style='text-align:right;min-width:8mm;'>222.0</td>
			<td style='text-align:right;min-width:10mm;'>222.0</td>
		</tr>
		<tr>
			<td style='text-align:left;min-width:18mm;'>1001#太平鸟男装</td>
			<td style='text-align:center;min-width:15mm;'>花色/2XL</td>
			<td style='text-align:right;min-width:6mm;'>1</td>
			<td style='text-align:right;min-width:8mm;'>199.0</td>
			<td style='text-align:right;min-width:8mm;'>199.0</td>
			<td style='text-align:right;min-width:10mm;'>199.0</td>
		</tr>
		<tr>
			<td style='text-align:left;min-width:18mm;'>LP001#礼品1</td>
			<td style='text-align:center;min-width:15mm;'>花色/2XL/A</td>
			<td style='text-align:right;min-width:6mm;'>1</td>
			<td style='text-align:right;min-width:8mm;'>222.0</td>
			<td style='text-align:right;min-width:8mm;'>222.0</td>
			<td style='text-align:right;min-width:10mm;'>222.0</td>
		</tr>
	</tbody>
	<tfoot class="foot">
		<tr>
			<td></td>
			<td></td>
			<td  tdata="Sum" format="#.##">#####</td>
			<td></td>
			<td></td>
			<td  tdata="AllSum" format="#,##0.00">###</td>
		</tr>
	</tfoot>
</table>