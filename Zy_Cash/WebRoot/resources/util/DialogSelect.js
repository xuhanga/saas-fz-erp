var DialogSelect = {};

DialogSelect.doQuerySupply = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择供货厂商',
		content : 'url:'+config.BASEPATH+'buy/supply/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].sp_code);
					names.push(selected[i].sp_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.sp_code);
				$("#"+name).val(selected.sp_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.sp_code);
				$("#"+name).val(selected.sp_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryClient = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择批发客户',
		content : 'url:'+config.BASEPATH+'batch/client/to_list_dialog',
		data : {multiselect:multiselect},
		width : 650,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].ci_code);
					names.push(selected[i].ci_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.ci_code);
				$("#"+name).val(selected.ci_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.ci_code);
				$("#"+name).val(selected.ci_name);
			}
		},
		cancel:true
	});
}


DialogSelect.doQueryEmp = function(multiselect,name){
	commonDia = $.dialog({
		title : '选择经办人',
		content : 'url:'+config.BASEPATH+'base/emp/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var names = [];
				for(var i=0;i<selected.length;i++){
					names.push(selected[i].em_name);
				}
				$("#"+name).val(names.join(","));
			}else{
				$("#"+name).val(selected.em_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+name).val(selected.em_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryDepot = function(multiselect,code,name,module){
	commonDia = $.dialog({
		title : '选择仓库',
		content : 'url:'+config.BASEPATH+'base/depot/to_list_dialog_bymodule',
		data : {multiselect:multiselect,module:module},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].dp_code);
					names.push(selected[i].dp_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.dp_code);
				$("#"+name).val(selected.dp_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.dp_code);
				$("#"+name).val(selected.dp_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryBrand = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择品牌',
		content : 'url:'+config.BASEPATH+'base/brand/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].bd_code);
					names.push(selected[i].bd_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.bd_code);
				$("#"+name).val(selected.bd_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.bd_code);
				$("#"+name).val(selected.bd_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryType = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择类别',
		content : 'url:'+config.BASEPATH+'base/type/to_tree_dialog',
		data : {multiselect:multiselect},
		width : 300,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].tp_code);
					names.push(selected[i].tp_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.tp_code);
				$("#"+name).val(selected.tp_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.tp_code);
				$("#"+name).val(selected.tp_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryProduct = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择商品',
		content : 'url:'+config.BASEPATH+'base/product/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].pd_code);
					names.push(selected[i].pd_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.pd_code);
				$("#"+name).val(selected.pd_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.pd_code);
				$("#"+name).val(selected.pd_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryColor = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择颜色',
		content : 'url:'+config.BASEPATH+'base/color/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].cr_code);
					names.push(selected[i].cr_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.cr_code);
				$("#"+name).val(selected.cr_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.cr_code);
				$("#"+name).val(selected.cr_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQuerySize = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择尺码',
		content : 'url:'+config.BASEPATH+'base/size/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].sz_code);
					names.push(selected[i].sz_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.sz_code);
				$("#"+name).val(selected.sz_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.sz_code);
				$("#"+name).val(selected.sz_name);
			}
		},
		cancel:true
	});
}

DialogSelect.doQueryBra = function(multiselect,code,name){
	commonDia = $.dialog({
		title : '选择杯型',
		content : 'url:'+config.BASEPATH+'base/bra/to_list_dialog',
		data : {multiselect:multiselect},
		width : 450,
		height : 320,
		max : false,
		min : false,
		cache : false,
		lock: true,
		ok:function(){
			var selected = commonDia.content.doSelect();
			if (!selected) {
				return false;
			}
			if(multiselect){//多选
				var codes = [];
				var names = [];
				for(var i=0;i<selected.length;i++){
					codes.push(selected[i].br_code);
					names.push(selected[i].br_name);
				}
				$("#"+code).val(codes.join(","));
				$("#"+name).val(names.join(","));
			}else{
				$("#"+code).val(selected.br_code);
				$("#"+name).val(selected.br_name);
			}
		},
		close:function(){
			if(!multiselect && commonDia.content.dblClick){//双击关闭
				var selected = commonDia.content.doSelect();
				$("#"+code).val(selected.br_code);
				$("#"+name).val(selected.br_name);
			}
		},
		cancel:true
	});
}




