$(function(){
	$('.ui-btn-menu .menu-btn').on('mouseenter.menuEvent',function(e){
		if($(this).hasClass("ui-btn-dis")){
			return false;
		}
		$(this).parent().addClass('ui-btn-menu-cur');
		$(this).blur();
		e.preventDefault();
	});
	$(document).on('click.menu',function(e){
		var target = e.target || e.srcElement;
		$('.ui-btn-menu').each(function(){
			var menu = $(this);
			if($(target).closest(menu).length == 0 && $('.con',menu).is(':visible')){
				menu.removeClass('ui-btn-menu-cur');
			}
		})
	});
});