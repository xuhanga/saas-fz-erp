package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.brand.T_Base_Brand;
import zy.form.PageForm;
import zy.service.base.brand.BrandService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("base/brand")
public class BrandController extends BaseController{
	@Resource
	private BrandService brandService;
	
	@RequestMapping("to_list_dialog")
	public String to_list_dialog() {
		return "base/brand/list_dialog";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
		PageData<T_Base_Brand> pageData = brandService.page(param);
		return ajaxSuccess(pageData);
	}
	
}
