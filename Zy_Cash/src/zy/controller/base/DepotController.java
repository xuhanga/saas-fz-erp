package zy.controller.base;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.form.PageForm;
import zy.service.base.depot.DepotService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("base/depot")
public class DepotController extends BaseController{
	@Resource
	private DepotService depotService;
	/**
	 * 仓库查询弹出框，按照功能模块
	 */
	@RequestMapping("to_list_dialog_bymodule")
	public String to_list_dialog_bymodule() {
		return "base/depot/list_dialog_bymodule";
	}
	
	@RequestMapping(value = "list4sellallocate", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4sellallocate(PageForm pageForm,String sp_code, HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
        param.put(CommonUtil.SHOP_CODE, sp_code);
		return ajaxSuccess(depotService.list4sellallocate(param));
	}
	
	@RequestMapping(value = "list4want", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list4want(PageForm pageForm, HttpSession session) {
        T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", pageForm.getSearchContent());
		return ajaxSuccess(depotService.list4want(param));
	}
	
}
