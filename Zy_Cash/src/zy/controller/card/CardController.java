package zy.controller.card;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.card.T_Sell_Card;
import zy.entity.sell.card.T_Sell_CardList;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.sell.card.CardService;
import zy.util.CommonUtil;
import zy.util.MD5;
import zy.util.StringUtil;
@Controller
@RequestMapping("cash/card")
public class CardController extends BaseController {
	@Resource
	private CardService cardService;
	/**
	 * 查询员工弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_list")
	public String to_list() {
		return "card/list";
	}
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "card/add";
	}
	@RequestMapping(value = "/to_pass", method = RequestMethod.GET)
	public String to_pass() {
		return "card/pass";
	}
	
	@RequestMapping(value = "/to_recharge/{cdl_type}", method = RequestMethod.GET)
	public String to_recharge(@PathVariable Integer cdl_type,@RequestParam Integer cd_id,Model model) {
		T_Sell_Card card = cardService.queryByID(cd_id);
		if(null != card){
			model.addAttribute("card",card);
		}
		model.addAttribute("cdl_type",cdl_type);
		return "card/recharge";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(
			@RequestParam(required = false) String cd_cardcode,
			HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, user.getCompanyid());
        param.put(CommonUtil.SHOP_TYPE, user.getShop_type());
        param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
        param.put(CommonUtil.SHOP_UPTYPE, user.getShop_uptype());
        param.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
        param.put("cd_cardcode", StringUtil.decodeString(cd_cardcode));
		return ajaxSuccess(cardService.listShop(param));
	}
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sell_Card card,HttpSession session) {
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("card", card);
		bulidParam(param, getCashier(session));
		cardService.saveShop(param);
		return ajaxSuccess(card);
	}
	
	@RequestMapping(value = "recharge", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object recharge(T_Sell_CardList cardList,Integer cd_id,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		cardList.setCompanyid(cashier.getCompanyid());
		cardList.setCdl_manager(cashier.getEm_name());
		cardList.setCdl_shop_code(cashier.getCa_shop_code());
		return ajaxSuccess(cardService.recharge(cardList, cd_id));
	}
	
	@RequestMapping(value = "editPass", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object editPass(Integer cd_id,HttpServletRequest request,HttpSession session) {
		String old_pass = request.getParameter("old_pass");
		String new_pass = request.getParameter("new_pass");
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put("old_pass", MD5.encryptMd5(old_pass));
		param.put("new_pass", MD5.encryptMd5(new_pass));
		param.put("cd_id", cd_id);
		cardService.editPass(param);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "queryByCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryByCode(HttpServletRequest request,HttpSession session) {
		String cd_cardcode = request.getParameter("cd_cardcode");
		String cd_pass = request.getParameter("cd_pass");
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("cd_cardcode", cd_cardcode);
		param.put("cd_pass", MD5.encryptMd5(cd_pass));
		param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		param.put(CommonUtil.SHOP_UPTYPE, cashier.getShop_uptype());
        param.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
        T_Sell_Card card = cardService.queryByCode(param);
        if(null != card){
        	return ajaxSuccess(card);
        }else{
        	return ajaxFail();
        }
	}
	
}
