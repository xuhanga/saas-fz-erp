package zy.controller.day;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.sell.day.DayService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
@Controller
@RequestMapping("day")
public class DayController extends BaseController{
	
	

	@RequestMapping(value = "/to_weather", method = RequestMethod.GET)
	public String to_weather() {
		return "day/weather";
	}
	@Resource
	private DayService dayService;
	@RequestMapping(value = "weather", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object weather(HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
//		String city = RequestUtil.getCityByIP(request);
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
		param.put("today", DateUtil.getYearMonthDate());
//		param.put("_city", city);
		
		return ajaxSuccess(dayService.weather(param));
	}
	
	/*@RequestMapping(value = "queryDay", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object queryDay(HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
		param.put("today", DateUtil.getYearMonthDate());
		return ajaxSuccess(dayService.queryDay(param));
	}*/
	@RequestMapping(value = "come", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object come(Integer da_id,HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
		param.put("today", DateUtil.getYearMonthDate());
		param.put("da_id", da_id);
		dayService.come(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "receive", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object receive(Integer da_id,HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		Object em_code = request.getParameter("em_code");
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
		param.put("today", DateUtil.getYearMonthDate());
		param.put("da_id", da_id);
		param.put("em_code", em_code);
		dayService.receive(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "doTry", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object doTry(Integer da_id,HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier user = (T_Sell_Cashier)session.getAttribute(CommonUtil.KEY_CASHIER);
		Object em_code = request.getParameter("em_code");
		Map<String,Object> param = new HashMap<String, Object>(3);
		param.put(CommonUtil.COMPANYID, user.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, user.getCa_shop_code());
		param.put("today", DateUtil.getYearMonthDate());
		param.put("da_id", da_id);
		param.put("em_code", em_code);
		dayService.doTry(param);
		return ajaxSuccess();
	}
	
}
