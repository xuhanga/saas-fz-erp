package zy.controller.dayend;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.dayend.T_Sell_DayEnd;
import zy.service.sell.dayend.DayEndService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.vo.sell.EndVO;

@Controller
@RequestMapping("dayend")
public class DayEndController extends BaseController{
	
	@Resource
	private DayEndService dayEndService;
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(HttpServletRequest request,HttpSession session){
		String st_code = request.getParameter("st_code");
		String st_name = request.getParameter("st_name");
		String de_man = request.getParameter("de_man");
		Object de_last_money = request.getParameter("de_last_money");
		Object de_petty_cash = request.getParameter("de_petty_cash");
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, cashier);
		param.put("de_petty_cash",de_petty_cash);
		param.put("de_last_money",de_last_money);
		param.put("sysdate", DateUtil.getCurrentTime());
		param.put("st_code", st_code);
		param.put("de_man", de_man);
		dayEndService.save(param);
		cashier.setSt_code(st_code);
		cashier.setSt_name(st_name);
		session.setAttribute(CommonUtil.KEY_CASHIER, cashier);
		return ajaxSuccess();
	}
	@RequestMapping(value = "/to_dayend", method = RequestMethod.GET)
	public String to_dayend(HttpSession session,Model model) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		T_Sell_DayEnd dayEnd = dayEndService.query(param);
		model.addAttribute("model",dayEnd);
		return "day/end";
	}
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Sell_DayEnd dayend,HttpServletRequest request,HttpSession session){
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		dayEndService.update(dayend,param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "list", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object list(HttpServletRequest request,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>(3);
		bulidParam(param, getCashier(session));
		return ajaxSuccess(dayEndService.listShop(param));
	}
	@RequestMapping(value = "endByID", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object endByID(Integer de_id,HttpServletRequest request,HttpSession session) {
		return ajaxSuccess(EndVO.buildEnd(dayEndService.endByID(de_id)));
	}
	
}
