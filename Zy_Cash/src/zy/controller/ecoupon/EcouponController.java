package zy.controller.ecoupon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.service.sell.ecoupon.ECouponService;
@Controller
@RequestMapping("ecoupon")
public class EcouponController extends BaseController {
	@Resource
	private ECouponService ecouponService;
	/**
	 * 查询顾客已经有的优惠券
	 * */
	@RequestMapping(value = "queryByCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryByCode(HttpServletRequest request,HttpSession session) {
		String ecu_tel = request.getParameter("ecu_tel");
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("ecu_tel", ecu_tel);
        bulidParam(param, getCashier(session));
        List<T_Sell_Ecoupon_User> card = ecouponService.queryByCode(param);
        if(null != card){
        	return ajaxSuccess(card);
        }else{
        	return ajaxFail();
        }
	}
}
