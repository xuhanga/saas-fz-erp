package zy.controller.emp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.base.emp.T_Base_Emp;
import zy.form.PageForm;
import zy.form.StringForm;
import zy.service.base.emp.EmpService;
import zy.service.sell.cashier.CashierService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("cash/emp")
public class EmpController extends BaseController{
	@Resource
	private EmpService empService;
	@Resource
	private CashierService cashierService;
	/**
	 * 查询员工弹出框页面，支持单选和多选
	 */
	@RequestMapping("to_emp_dialog")
	public String to_emp_dialog() {
		return "emp/list_dialog";
	}
	
	@RequestMapping(value = "listEmp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listEmp(PageForm pageForm,HttpServletRequest request,HttpSession session) {
        String searchContent = request.getParameter("searchContent");
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("searchContent", StringUtil.decodeString(searchContent));
		PageData<T_Base_Emp> pageData = empService.pageShop(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
	
	@RequestMapping(value = "listCash", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listCash(HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
		List<StringForm> list = cashierService.listCash(param);
		return ajaxSuccess(list);
	}
}
