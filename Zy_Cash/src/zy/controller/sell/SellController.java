package zy.controller.sell;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.form.ProductForm;
import zy.service.sell.sell.SellService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
@Controller
@RequestMapping("sell")
public class SellController extends BaseController{
	@Resource
	private SellService sellService;
	
	@RequestMapping("to_list")
	public String to_list() {
		return "sell/list";
	}
	
	@RequestMapping(value = "pageShop", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object pageShop(ProductForm productForm,HttpServletRequest request,HttpSession session) {
		Object code = request.getParameter("code");
		Object em_code = request.getParameter("em_code");
		Object type = request.getParameter("type");//0收银查询1明细查询
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.PAGESIZE, productForm.getRows());
        param.put(CommonUtil.PAGEINDEX, productForm.getPage());
        param.put("code", StringUtil.getSpell(""+StringUtil.decodeString(code)));
        param.put("begindate", productForm.getBegindate());
        param.put("enddate", productForm.getEnddate());
        param.put("em_code", em_code);
        param.put("bd_code", productForm.getBd_code());
        param.put("pd_code", productForm.getPd_code());
        if("0".equals(StringUtil.trimString(type))){
        	PageData<T_Sell_Shop> pageData = sellService.pageShop(param);
        	return ajaxSuccess(pageData);
        }else{
        	PageData<T_Sell_ShopList> pageData = sellService.pageShopList(param);
        	return ajaxSuccess(pageData);
        }
	}
	
	
	@RequestMapping(value = "print", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object print(Integer sh_id,Double change_money,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put("pt_id", CommonUtil.PRINT_TYPE_CASH);
        param.put("sh_id", sh_id);
        param.put("change_money", change_money);
		Map<String, Object> html = sellService.print(param);
		return ajaxSuccess(html);
	}
}
