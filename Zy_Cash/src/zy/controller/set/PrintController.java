package zy.controller.set;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sys.print.T_Sys_Print;
import zy.entity.sys.print.T_Sys_PrintData;
import zy.entity.sys.print.T_Sys_PrintField;
import zy.entity.sys.print.T_Sys_PrintSet;
import zy.service.sys.print.PrintService;

@Controller
@RequestMapping("set/print")
public class PrintController extends BaseController{
	@Resource
	private PrintService printService;
	
	@RequestMapping(value = "to_list/{sp_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable String sp_type, Model model) {
		model.addAttribute("sp_type", sp_type);
		return "set/print/list";
	}
	@RequestMapping(value = "to_print_update/{sp_type}", method = RequestMethod.GET)
	public String to_print_update(@PathVariable String sp_type, Model model) {
		model.addAttribute("sp_type", sp_type);
		return "set/print/print_update";
	}
	
	@RequestMapping(value = "list/{sp_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object list(@PathVariable Integer sp_type,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		return ajaxSuccess(printService.list(sp_type, cashier.getCa_shop_code(), cashier.getCompanyid()));
	}
	@RequestMapping(value = "loadPrint/{sp_id}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object loadPrint(@PathVariable Integer sp_id) {
		return ajaxSuccess(printService.loadPrint(sp_id));
	}
	@RequestMapping(value = "loadDefaultPrint/{pt_type}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object loadDefaultPrint(@PathVariable Integer pt_type) {
		return ajaxSuccess(printService.loadDefaultPrint(pt_type));
	}
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object save(String postData,HttpSession session) {
		JSONObject postJson = JSON.parseObject(postData);
		T_Sys_Print print = JSON.parseObject(postJson.getString("print"), T_Sys_Print.class);
		List<T_Sys_PrintSet> printSets = JSON.parseArray(postJson.getString("printSets"), T_Sys_PrintSet.class);
		List<T_Sys_PrintData> printDatas = JSON.parseArray(postJson.getString("printDatas"), T_Sys_PrintData.class);
		List<T_Sys_PrintField> printFields = JSON.parseArray(postJson.getString("printFields"), T_Sys_PrintField.class);
		printService.save(print, printFields, printDatas, printSets, getCashier(session));
		return ajaxSuccess();
	}
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object update(String postData) {
		JSONObject postJson = JSON.parseObject(postData);
		T_Sys_Print print = JSON.parseObject(postJson.getString("print"), T_Sys_Print.class);
		List<T_Sys_PrintSet> printSets = JSON.parseArray(postJson.getString("printSets"), T_Sys_PrintSet.class);
		List<T_Sys_PrintData> printDatas = JSON.parseArray(postJson.getString("printDatas"), T_Sys_PrintData.class);
		List<T_Sys_PrintField> printFields = JSON.parseArray(postJson.getString("printFields"), T_Sys_PrintField.class);
		printService.update(print, printFields, printDatas, printSets);
		return ajaxSuccess();
	}
	@RequestMapping(value = "del/{sp_id}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object del(@PathVariable Integer sp_id) {
		printService.del(sp_id);
		return ajaxSuccess();
	}
}
