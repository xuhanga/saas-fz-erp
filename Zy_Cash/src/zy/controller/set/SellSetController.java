package zy.controller.set;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.sell.set.T_Sell_Print;
import zy.entity.sell.set.T_Sell_PrintData;
import zy.entity.sell.set.T_Sell_PrintField;
import zy.entity.sell.set.T_Sell_PrintSet;
import zy.entity.sell.set.T_Sell_Set;
import zy.service.set.SellSetService;
import zy.service.sys.print.PrintService;
import zy.util.CommonUtil;
import zy.vo.sell.SellVO;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
@Controller
@RequestMapping("cash/set")
public class SellSetController extends BaseController{
	
	@Resource
	private SellSetService sellSetService;
	@Resource
	private PrintService printService;
	
	@RequestMapping(value = "/to_print", method = RequestMethod.GET)
	public String to_print(HttpSession session,Model model) {
		return "set/print_set";
	}
	
	
	@RequestMapping(value = "/to_cash", method = RequestMethod.GET)
	public String to_cash(HttpSession session,Model model) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String,Object>();
		param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
		param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		sellSetService.cashSet(param);
		model.addAttribute("model", param);
		return "set/cash_set";
	}
	@RequestMapping(value = "updateCash", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object updateCash(Integer cd_id,HttpServletRequest request,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		String data = request.getParameter("data");
		Map<String,Object> param = new HashMap<String, Object>();
		List<T_Sell_Set> list = SellVO.buildSellMap(data,cashier);  
		bulidParam(param, cashier);
		param.put("setlist", list);
		sellSetService.updateCash(param);
		return ajaxSuccess();
	}
	@RequestMapping(value = "/print", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody Object print(Integer pt_id,HttpSession session,Model model) {
		Map<String,Object> param = new HashMap<String, Object>();
		bulidParam(param, getCashier(session));
		param.put("pt_id", pt_id);
		Map<String,Object> map = sellSetService.print(param);
		return ajaxSuccess(map);
	}
	
	@RequestMapping(value = "savePrint", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object savePrint(String postData,HttpSession session) {
		JSONObject postJson = JSON.parseObject(postData);
		T_Sell_Print print = JSON.parseObject(postJson.getString("print"), T_Sell_Print.class);
		List<T_Sell_PrintSet> printSets = JSON.parseArray(postJson.getString("printSets"), T_Sell_PrintSet.class);
		List<T_Sell_PrintData> printDatas = JSON.parseArray(postJson.getString("printDatas"), T_Sell_PrintData.class);
		List<T_Sell_PrintField> printFields = JSON.parseArray(postJson.getString("printFields"), T_Sell_PrintField.class);
		sellSetService.savePrint(print, printFields, printDatas, printSets, getCashier(session));
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updatePrint", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object updatePrint(String postData,HttpSession session) {
		JSONObject postJson = JSON.parseObject(postData);
		T_Sell_Print print = JSON.parseObject(postJson.getString("print"), T_Sell_Print.class);
		List<T_Sell_PrintSet> printSets = JSON.parseArray(postJson.getString("printSets"), T_Sell_PrintSet.class);
		List<T_Sell_PrintData> printDatas = JSON.parseArray(postJson.getString("printDatas"), T_Sell_PrintData.class);
		List<T_Sell_PrintField> printFields = JSON.parseArray(postJson.getString("printFields"), T_Sell_PrintField.class);
		sellSetService.updatePrint(print, printFields, printDatas, printSets, getCashier(session));
		return ajaxSuccess();
	}
}
