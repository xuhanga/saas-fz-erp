package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.kpiassess.T_Shop_KpiAssess;
import zy.form.PageForm;
import zy.service.shop.kpiassess.KpiAssessService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("shop/kpiassess")
public class KpiAssessController extends BaseController{
	@Resource
	private KpiAssessService kpiAssessService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/kpiassess/list";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer ka_id,Model model) {
		T_Shop_KpiAssess kpiAssess = kpiAssessService.load(ka_id);
		if(null != kpiAssess){
			model.addAttribute("kpiAssess",kpiAssess);
		}
		return "shop/kpiassess/view";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(PageForm pageForm,@RequestParam(required = false) Integer ka_type,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("ka_type", ka_type);
		return ajaxSuccess(kpiAssessService.list4cashier(param));
	}
	
	@RequestMapping(value = "statDetail/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statDetail(@PathVariable String number,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("number", number);
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
		params.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		params.put(CommonUtil.SHOP_UPCODE, cashier.getShop_upcode());
		return ajaxSuccess(kpiAssessService.statDetail(params));
	}
	
}
