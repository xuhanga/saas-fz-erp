package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.PageData;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.program.T_Shop_Program;
import zy.service.shop.program.ProgramService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("shop/program")
public class ProgramController extends BaseController{
	
	@Resource
	private ProgramService programService;
	
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/program/list";
	}
	
	@RequestMapping(value = "/to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer sps_id,@RequestParam Integer sp_id,Model model,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
		param.put("sps_id", sps_id);
		param.put("sp_id", sp_id);
		T_Shop_Program program = programService.load_cash(param);
		if(null != program){
			model.addAttribute("program",program);
		}
		return "shop/program/view";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(HttpServletRequest request,HttpSession session) {
		String page = request.getParameter("page"); // 页码
        String rows = request.getParameter("rows"); // 每页条数
        String sidx = request.getParameter("sidx");
        String sord = request.getParameter("sord");
        String begindate = request.getParameter("begindate");
        String enddate = request.getParameter("enddate");
        String sp_state = request.getParameter("sp_state");
        
        int pageIndex = 1;
        int pageSize = super.pagesize;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(page)) {
        	pageIndex = Integer.parseInt(page);
        	pageIndex = pageIndex == 0 ? 1 : pageIndex;
        }
        if (org.apache.commons.lang.StringUtils.isNotEmpty(rows)) {
        	pageSize = Integer.parseInt(rows);
        }
        T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("begindate", begindate);
        param.put("enddate", enddate);
        param.put("sp_state", sp_state);
        param.put(CommonUtil.PAGESIZE, pageSize);
        param.put(CommonUtil.PAGEINDEX, pageIndex);
        param.put(CommonUtil.SIDX, sidx);
        param.put(CommonUtil.SORD, sord);
		PageData<T_Shop_Program> pageData = programService.page(param);
		return ajaxSuccess(pageData, "查询成功, 共" + pageData.getList().size() + "条数据");
	}
}
