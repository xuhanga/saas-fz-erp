package zy.controller.shop;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.target.T_Shop_Target;
import zy.entity.shop.target.T_Shop_Target_Detail;
import zy.form.PageForm;
//import zy.service.shop.target.TargetService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("shop/target")
public class TargetController extends BaseController{
	/*@Resource
	private TargetService targetService;
	
	@RequestMapping(value = "to_list", method = RequestMethod.GET)
	public String to_list() {
		return "shop/target/list";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam String ta_number,Model model,HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
	    param.put(CommonUtil.COMPANYID, getCompanyid(session));
		param.put("ta_number", ta_number);
		T_Shop_Target target = targetService.queryTargetByNumber(param);
		model.addAttribute("target", target);
		return "shop/target/view";
	}
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(PageForm pageForm,@RequestParam(required = false) Integer ta_type,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("ta_type", ta_type);
		return ajaxSuccess(targetService.list4cashier(param));
	}
	
	@RequestMapping(value = "statDetail/{number}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object statDetail(@PathVariable String number,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("ta_number", number);
        param.put("ta_state", "0");
        List<T_Shop_Target_Detail> details = targetService.queryTargetDetail(param);
        Collections.sort(details, new Comparator<T_Shop_Target_Detail>() {
            @Override
            public int compare(T_Shop_Target_Detail o1, T_Shop_Target_Detail o2) {
                int c = 0;
                c = o1.getTad_real_money() >= o2.getTad_real_money() ? 1 : -1;
                return  -c;
            }
        });
        T_Shop_Target_Detail total = new T_Shop_Target_Detail();
        total.setTad_em_name("总体进度");
        total.setTad_real_count(0);
        total.setTad_vip_count(0);
        total.setTad_real_money(0d);
        total.setTad_target_money(0d);
		for (T_Shop_Target_Detail detail : details) {
			total.setTad_target_money(total.getTad_target_money()+detail.getTad_target_money());
			total.setTad_real_money(total.getTad_real_money()+detail.getTad_real_money());
			total.setTad_vip_count(total.getTad_vip_count()+detail.getTad_vip_count());
			total.setTad_real_count(total.getTad_real_count()+detail.getTad_real_count());
		}
		details.add(0, total);
		return ajaxSuccess(details);
	}*/
	
}
