package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.shop.want.T_Shop_Want;
import zy.form.PageForm;
import zy.service.shop.want.WantService;
import zy.util.CommonUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("shop/want")
public class WantController extends BaseController{
	
	@Resource
	private WantService wantService;

	@RequestMapping(value = "to_list/{wt_type}", method = RequestMethod.GET)
	public String to_list(@PathVariable Integer wt_type,Model model) {
		model.addAttribute("wt_type", wt_type);
		return "shop/want/list";
	}
	@RequestMapping(value = "to_view", method = RequestMethod.GET)
	public String to_view(@RequestParam Integer wt_id,Model model,
			@RequestParam(required = false) String oper) {
		T_Shop_Want want = wantService.load(wt_id);
		if(null != want){
			model.addAttribute("want",want);
		}
		if(StringUtil.isNotEmpty(oper)){
			model.addAttribute("oper",oper);
		}
		return "shop/want/view";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = true) Integer wt_type,
			@RequestParam(required = true) Integer wt_isdraft,
			@RequestParam(required = false) Integer wt_ar_state,
			HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("wt_type", wt_type);
        param.put("wt_isdraft", wt_isdraft);
        param.put("wt_ar_state", wt_ar_state);
		return ajaxSuccess(wantService.page(param));
	}
	
	@RequestMapping(value = "detail_list/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_list(@PathVariable String wt_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_list(params));
	}
	
	@RequestMapping(value = "detail_sum/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_sum(@PathVariable String wt_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_sum(params));
	}
	
	@RequestMapping(value = "detail_size_title/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size_title(@PathVariable String wt_number,HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_size_title(params));
	}
	
	@RequestMapping(value = "detail_size/{wt_number}", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object detail_size(@PathVariable String wt_number,
			@RequestParam Integer wt_ar_state,@RequestParam Integer isFHD,@RequestParam Integer isSend,
			HttpSession session) {
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("wtl_number", wt_number);
		params.put("wt_ar_state", wt_ar_state);
		params.put("isFHD", isFHD);
		params.put("isSend", isSend);
		params.put("companyid", getCompanyid(session));
		return ajaxSuccess(wantService.detail_size(params));
	}
	
	@RequestMapping(value = "receive", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object receive(String number,String wt_indp_code,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("number", number);
		params.put("wt_indp_code", wt_indp_code);
		params.put("us_name", cashier.getEm_name());
		params.put("us_id", 0);
		return ajaxSuccess(wantService.receive(params));
	}
	
	@RequestMapping(value = "reject", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object reject(String number,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, getCompanyid(session));
		params.put("number", number);
		params.put("us_name", cashier.getEm_name());
		return ajaxSuccess(wantService.reject(params));
	}
	
}
