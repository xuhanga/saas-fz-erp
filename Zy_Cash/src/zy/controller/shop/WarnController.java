package zy.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.service.shop.want.WantService;
import zy.util.CommonUtil;

@Controller
@RequestMapping("shop/warn")
public class WarnController extends BaseController{
	@Resource
	private WantService wantService;
	
	@RequestMapping(value = "loadWarnCount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadWarnCount(HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put(CommonUtil.SHOP_TYPE, cashier.getShop_type());
        param.put(CommonUtil.SHOP_CODE, cashier.getCa_shop_code());
        param.put("wt_type", 0);
        param.put("wt_isdraft", 0);
        param.put("wt_ar_state", CommonUtil.WANT_AR_STATE_SEND);
        Map<String, Object> resultMap = new HashMap<String,Object>();
        resultMap.put("wantReceiveCount", wantService.count(param));
		return ajaxSuccess(resultMap);
	}
	
	
}
