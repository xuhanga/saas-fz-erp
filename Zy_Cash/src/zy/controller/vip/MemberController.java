package zy.controller.vip;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.card.T_Sell_Card;
import zy.entity.sell.cashier.T_Sell_Cashier;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.form.PageForm;
import zy.service.sell.card.CardService;
import zy.service.vip.member.MemberService;
import zy.service.vip.membertype.MemberTypeService;
import zy.util.Arith;
import zy.util.CommonUtil;
import zy.util.MD5;
import zy.util.StringUtil;

@Controller
@RequestMapping("vip/member")
public class MemberController extends BaseController{
	
	@Resource
	private MemberService memberService;
	@Resource
	private MemberTypeService memberTypeService;
	@RequestMapping(value = "/to_list", method = RequestMethod.GET)
	public String to_list() {
		return "vip/member/list";
	}
	@RequestMapping(value = "/to_point", method = RequestMethod.GET)
	public String to_point() {
		return "vip/member/point";
	}
	@RequestMapping(value = "/to_vip_point", method = RequestMethod.GET)
	public String to_vip_point(Integer vip_id,Model model) {
		T_Vip_Member member = memberService.vipById(vip_id);
		model.addAttribute("model",member);
		return "cash/vip_point";
	}
	
	@RequestMapping(value = "/to_referee", method = RequestMethod.GET)
	public String to_referee() {
		return "vip/member/referee";
	}
	@RequestMapping(value = "/to_info", method = RequestMethod.GET)
	public String to_info(Integer vip_id,Model model,HttpSession session) {
		T_Vip_Member member = memberService.queryByID(vip_id);
		T_Vip_Member_Info info = memberService.queryInfoByCode(member.getVm_code(), getCompanyid(session));
		model.addAttribute("model",member);
		model.addAttribute("memberinfo",info);
		return "vip/member/info";
	}
	
	@RequestMapping(value = "/to_add", method = RequestMethod.GET)
	public String to_add() {
		return "vip/member/add";
	}
	@RequestMapping(value = "/to_update", method = RequestMethod.GET)
	public String to_update(@RequestParam Integer vm_id,Model model) {
		T_Vip_Member member = memberService.queryByID(vm_id);
		if(null != member){
			T_Vip_Member_Info memberinfo = memberService.queryInfoByCode(member.getVm_code(), member.getCompanyid());
			model.addAttribute("member",member);
			model.addAttribute("memberinfo",memberinfo);
		}
		return "vip/member/update";
	}
	
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			@RequestParam(required = false) String vm_shop_code,
			@RequestParam(required = false) String vm_cardcode,
			@RequestParam(required = false) String vm_name,
			HttpServletRequest request,HttpSession session) {
        Map<String,Object> param = new HashMap<String, Object>();
        bulidParam(param, getCashier(session));
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put("vm_shop_code", vm_shop_code);
        param.put("vm_cardcode", StringUtil.decodeString(vm_cardcode));
        param.put("vm_name", StringUtil.decodeString(vm_name));
		return ajaxSuccess(memberService.pageByShop(param));
	}
	
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Vip_Member member,T_Vip_Member_Info memberInfo,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		member.setCashier(cashier);
		member.setCompanyid(cashier.getCompanyid());
		member.setVm_password(MD5.encryptMd5(CommonUtil.INIT_PWD));
		memberInfo.setCompanyid(cashier.getCompanyid());
		memberService.save(member, memberInfo);
		return ajaxSuccess(member);
	}
	
	@RequestMapping(value = "update", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object update(T_Vip_Member member,T_Vip_Member_Info memberInfo,HttpSession session) {
		T_Sell_Cashier cashier = getCashier(session);
		member.setCashier(cashier);
		member.setCompanyid(cashier.getCompanyid());
		memberInfo.setCompanyid(cashier.getCompanyid());
		memberService.update(member, memberInfo);
		return ajaxSuccess(member);
	}
	
	@RequestMapping(value = "listType", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object listType(String searchContent, HttpSession session) {
		T_Sell_Cashier cash = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
        param.put("searchContent", StringUtil.decodeString(searchContent));
        param.put(CommonUtil.SHOP_CODE, cash.getShop_upcode());//因为会员类别表存的是上级店铺的编号，所以这里传上级编号
        param.put(CommonUtil.COMPANYID, cash.getCompanyid());
		return ajaxSuccess(memberTypeService.list(param));
	}
	@RequestMapping(value = "vipByCode", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object vipByCode(String cardcode, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sell_Cashier cashier = getCashier(session);
        param.put("vm_cardcode", StringUtil.decodeString(cardcode));
        bulidParam(param, cashier);
		return ajaxSuccess(memberService.vipByCode(param));
	}
	@Autowired
	private CardService cardService;
	
	@RequestMapping(value = "mobile", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object vipByMobile(String mobile, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
		T_Sell_Cashier cashier = getCashier(session);
        param.put("mobile", StringUtil.decodeString(mobile));
        bulidParam(param, cashier);
        T_Vip_Member vip_Member = memberService.vipByMobile(param);
        T_Sell_Card card = cardService.queryByCode(param);
        param.clear();
        param.put("vip", vip_Member);
        param.put("card", card);
		return ajaxSuccess(param);
	}
	
	@RequestMapping(value = "updatePoint", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updatePoint(HttpServletRequest request,HttpSession session) {
		Object vm_id = request.getParameter("vm_id");
		Object vm_code = request.getParameter("vm_code");
		Object option = request.getParameter("option");
		Object point = request.getParameter("point");
		Object vpl_remark = request.getParameter("vpl_remark");
		T_Sell_Cashier cashier = getCashier(session);
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("vm_id", vm_id);
		param.put("vm_code", vm_code);
		param.put(CommonUtil.SHOP_NAME, cashier.getShop_name());
		param.put(CommonUtil.COMPANYID, cashier.getCompanyid());
		param.put(CommonUtil.EMP_NAME, cashier.getEm_name());
		if(null != option && "1".equals(option)){
			param.put("point", -Math.abs(Double.parseDouble(point.toString())));
			param.put("vpl_type", 5);//减积分 
		}else{
			param.put("point", point);//增积分
			param.put("vpl_type", 6);
		}
		param.put("vpl_remark", vpl_remark);
		memberService.updatePoint(param);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "listSell", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object listSell(String vip_code, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("vip_code", vip_code);
		return ajaxSuccess(memberService.listSell(param));
	}
	@RequestMapping(value = "listPoint", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody Object listPoint(String vip_code, HttpSession session) {
		Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.COMPANYID, getCompanyid(session));
        param.put("vip_code", vip_code);
		return ajaxSuccess(memberService.listPoint(param));
	}
	public static void main(String[] args) {
		double a = 258.4d;
		double b = 8.4d;
		System.out.println(a-b);
		System.out.println(Arith.sub(a, b));
	}
}
