<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	<title>购物车</title>
    <jsp:include page="/common/style.jsp"></jsp:include>
</head>
<body>
    <div class="main flex-wrap">

        <div class="content flex-con"> 
            <div class="none">
                <img src="${base_path}resources/images/carnone.jpg">
                <p>购物车还没有商品，赶快去添加吧</p>
                <a class="border">去购买</a>
            </div>
        </div>
        <jsp:include page="/common/menu.jsp">
			<jsp:param value="cart" name="class"/>
		</jsp:include>
    </div>
</body>

<script>
    $(function(){
        $('.carlist').on('swipeLeft', '.carlistveiw', function(){
            $(this).parent().siblings().children('.carlistveiw').removeClass("selected");
            $(this).addClass("selected");            
        });
        $('.carlist').on('swipeRight', '.carlistveiw', function(){
            if($(this).hasClass("selected"))
               $(this).removeClass("selected");
        });
    });
</script>
</html>