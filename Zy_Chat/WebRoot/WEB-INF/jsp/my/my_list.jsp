<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	<title>微商城</title>
    <%@ include file="/common/style.jsp"%>
</head>
<body>
    <div class="main flex-wrap">

        <div class="content flex-con">
            <div class="my-top tbox box-s mb-10">
                <div><img class="img-round" src="${base_path}resources/images/dianpu.jpg"></div>
                <div class="arrow">
                    <h5>未登录</h5>
                    <!-- <h5>登录帐号</h5>
                    <h6>其他信息</h6> -->
                </div>
            </div>
            <div class="box-s mb-10 mylist">
                <h5>我的钱包<p class="fr">余额、欠款</p></h5>
                <ul class="flex-wrap text-center border-t">
                    <li class="flex-con">
                        <b>0</b>
                        <p>余额</p>
                    </li>
                    <li class="flex-con border-l">
                        <b>0</b>
                        <p>优惠券</p>
                    </li>
                </ul>
            </div>
            <div class="box-s mb-10 mylist">
                <h5 class="arrow">我的订单<p class="fr">查看全部订单</p></h5>
                <ul class="flex-wrap text-center border-t">
                    <li class="flex-con">
                        <p><i class="iconfont icon-pay"></i></p>
                        <h5>代付款</h5>
                    </li>
                    <li class="flex-con">
                        <p><i class="iconfont icon-pay"></i></p>
                        <h5>待发货</h5>
                    </li>
                    <li class="flex-con">
                        <p><i class="iconfont icon-deliver"></i></p>
                        <h5>待收货</h5>
                    </li>
                </ul>
            </div>
            <div class="box-s mb-10 mylist gongju">
                <h5>帮助工具</h5>
                <ul class="text-center border-t border-l">
                    <li class="col-xs-3 border-r border-b">
                        <p><i class="iconfont icon-shouchang"></i></p>
                        <h5>我的收藏</h5>
                    </li>
                    <li class="col-xs-3 border-r border-b">
                        <p><i class="iconfont icon-fapiaoshenqing"></i></p>
                        <h5>我的发票</h5>
                    </li>
                    <li class="col-xs-3 border-r border-b">
                        <p><i class="iconfont icon-common-changjianwenti-copy"></i></p>
                        <h5>常见问题</h5>
                    </li>
                    <li class="col-xs-3 border-r border-b">
                        <p><i class="iconfont icon-service"></i></p>
                        <h5>客服电话</h5>
                    </li>
                    <div class="clearfix"></div>
                </ul>
            </div>
        </div>
        <jsp:include page="/common/menu.jsp">
			<jsp:param value="cart" name="class"/>
		</jsp:include>
    </div>
</body>

<script>
    $(function(){
        $('.carlist').on('swipeLeft', '.carlistveiw', function(){
            $(this).parent().siblings().children('.carlistveiw').removeClass("selected");
            $(this).addClass("selected");            
        });
        $('.carlist').on('swipeRight', '.carlistveiw', function(){
            if($(this).hasClass("selected"))
               $(this).removeClass("selected");
        });
    })
</script>
</html>