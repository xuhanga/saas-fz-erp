<link rel="stylesheet" href="${base_path}resources/css/reset.css"/>
<link rel="stylesheet" href="${base_path}resources/css/common.css"/>
<link rel="stylesheet" href="${base_path}resources/css/style.css"/>
<link rel="stylesheet" href="${base_path}resources/css/iconfont/iconfont.css"/>
<script src="${base_path}resources/js/common.js"></script>
<script src="${base_path}resources/js/zepto.min.js"></script>
<script src="${base_path}resources/js/touch.js"></script>
<script src="${base_path}resources/js/touchControl.js"></script>