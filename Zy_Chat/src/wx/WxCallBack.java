package wx;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import zy.entity.wx.order.T_Wx_Order;
import zy.service.order.WxOrderService;
import zy.util.DateUtil;
import zy.util.StringUtil;

public class WxCallBack {
	@Autowired
	private WxOrderService orderService;

//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//	}
	
	public Map<String,Object> OrderCallBack(String ordernumber,String paymoney,String tradeno){
		
		try
		{
			Map<String,Object> paramMap= new HashMap<String,Object>();
			paramMap.put("number", ordernumber);
			T_Wx_Order t_wx_order = orderService.getOrder(paramMap);
			if (t_wx_order != null){
				if (StringUtil.isEmpty(t_wx_order.getTrade_no())){
					
					paramMap.put("id", t_wx_order.getId());
					paramMap.put("order_status", "1");
					paramMap.put("pay_status", "success");
					paramMap.put("pay_time", DateUtil.getCurrentDate());
					double fee = Double.parseDouble(paymoney);
					//使用优惠券或账户余额抵扣，支付金额为0的处理
					if (fee==0d){
						paramMap.put("trade_no", ordernumber+"(备注：等同流水号)");
					}else{
						//正常微信支付处理
						paramMap.put("trade_no", tradeno);
					}
					
					orderService.ModifyOrder(paramMap);					
				}
				
			}
			return null;
		}catch(Exception ex){
			return null;
			
		}
	}

}
