package wx;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.StringUtils;

public class WxPay {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
//			createMiniProgramTradePay();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	/**
	 * @Description: 发起微信支付
	 * @param request
	 */
	public Object wxPay(String openid, HttpServletRequest request){
		try{
			//生成的随机字符串
			String nonce_str = "";//StringUtils.getRandomStringByLength(32);
			//商品名称
			String body = "测试商品名称";
			//获取客户端的ip地址
			String spbill_create_ip = "";//IpUtil.getIpAddr(request);
			
			//组装参数，用户生成统一下单接口的签名
			Map<String, String> packageParams = new HashMap<String, String>();
			packageParams.put("appid", WxPayConfig.appid);
			packageParams.put("mch_id", WxPayConfig.mch_id);
			packageParams.put("nonce_str", nonce_str);
			packageParams.put("body", body);
			packageParams.put("out_trade_no", "123456789");//商户订单号
			packageParams.put("total_fee", "1");//支付金额，这边需要转成字符串类型，否则后面的签名会失败
			packageParams.put("spbill_create_ip", spbill_create_ip);
			packageParams.put("notify_url", WxPayConfig.notify_url);//支付成功后的回调地址
			packageParams.put("trade_type", WxPayConfig.TRADETYPE);//支付方式
			packageParams.put("openid", openid);
			   
	        	String prestr = WxPayUtil.createLinkString(packageParams); // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串 
	        
	        	//MD5运算生成签名，这里是第一次签名，用于调用统一下单接口
	        	String mysign = WxPayUtil.sign(prestr, WxPayConfig.key, "utf-8").toUpperCase();
	        
	        //拼接统一下单接口使用的xml数据，要将上一步生成的签名一起拼接进去
	        String xml = "<xml>" + "<appid>" + WxPayConfig.appid + "</appid>" 
                    + "<body><![CDATA[" + body + "]]></body>" 
                    + "<mch_id>" + WxPayConfig.mch_id + "</mch_id>" 
                    + "<nonce_str>" + nonce_str + "</nonce_str>" 
                    + "<notify_url>" + WxPayConfig.notify_url + "</notify_url>" 
                    + "<openid>" + openid + "</openid>" 
                    + "<out_trade_no>" + "123456789" + "</out_trade_no>" 
                    + "<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>" 
                    + "<total_fee>" + "1" + "</total_fee>"
                    + "<trade_type>" + WxPayConfig.TRADETYPE + "</trade_type>" 
                    + "<sign>" + mysign + "</sign>"
                    + "</xml>";
	        
	        System.out.println("调试模式_统一下单接口 请求XML数据：" + xml);
 
	        //调用统一下单接口，并接受返回的结果
	        String result = WxPayUtil.httpRequest(WxPayConfig.pay_url, "POST", xml);
	        
	        System.out.println("调试模式_统一下单接口 返回XML数据：" + result);
	        
	        // 将解析结果存储在HashMap中   
	        Map map = WxPayUtil.doXMLParse(result);
	        
	        String return_code = (String) map.get("return_code");//返回状态码
	        
		    Map<String, Object> response = new HashMap<String, Object>();//返回给小程序端需要的参数
	        if(return_code=="SUCCESS"||return_code.equals(return_code)){   
	            String prepay_id = (String) map.get("prepay_id");//返回的预付单信息   
	            response.put("nonceStr", nonce_str);
	            response.put("package", "prepay_id=" + prepay_id);
	            Long timeStamp = System.currentTimeMillis() / 1000;   
	            response.put("timeStamp", timeStamp + "");//这边要将返回的时间戳转化成字符串，不然小程序端调用wx.requestPayment方法会报签名错误
	            //拼接签名需要的参数
	            String stringSignTemp = "appId=" + WxPayConfig.appid + "&nonceStr=" + nonce_str + "&package=prepay_id=" + prepay_id+ "&signType=MD5&timeStamp=" + timeStamp;   
	            //再次签名，这个签名用于小程序端调用wx.requesetPayment方法
	            String paySign = WxPayUtil.sign(stringSignTemp, WxPayConfig.key, "utf-8").toUpperCase();
	            
	            response.put("paySign", paySign);
	        }
			
	        response.put("appid", WxPayConfig.appid);
			
	        return response;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * StringUtils工具类方法
	 * 获取一定长度的随机字符串，范围0-9，a-z
	 * @param length：指定字符串长度
	 * @return 一定长度的随机字符串
	 */
	public static String getRandomStringByLength(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
       }
	/**
	 * IpUtils工具类方法
	 * 获取真实的ip地址
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
	    if(!StringUtils.isNullOrEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	         //多次反向代理后会有多个ip值，第一个ip才是真实ip
	    	int index = ip.indexOf(",");
	        if(index != -1){
	            return ip.substring(0,index);
	        }else{
	            return ip;
	        }
	    }
	    ip = request.getHeader("X-Real-IP");
	    if(!StringUtils.isNullOrEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	       return ip;
	    }
	    return request.getRemoteAddr();

	

	}
	
	/**
     * @Description:微信支付
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/wxNotify")
    @ResponseBody
    public void wxNotify(HttpServletRequest request,HttpServletResponse response) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream)request.getInputStream()));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while((line = br.readLine()) != null){
            sb.append(line);
        }
        br.close();
        //sb为微信返回的xml
        String notityXml = sb.toString();
        String resXml = "";
        System.out.println("接收到的报文：" + notityXml);
 
        Map map = WxPayUtil.doXMLParse(notityXml);
 
        String returnCode = (String) map.get("return_code");
        if("SUCCESS".equals(returnCode)){
            //验证签名是否正确
            Map<String, String> validParams = WxPayUtil.paraFilter(map);  //回调验签时需要去除sign和空值参数
            String validStr = WxPayUtil.createLinkString(validParams);//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
            String sign = WxPayUtil.sign(validStr, WxPayConfig.key, "utf-8").toUpperCase();//拼装生成服务器端验证的签名
            //根据微信官网的介绍，此处不仅对回调的参数进行验签，还需要对返回的金额与系统订单的金额进行比对等
            if(sign.equals(map.get("sign"))){
                /**此处添加自己的业务逻辑代码start**/
 
 
                /**此处添加自己的业务逻辑代码end**/
                //通知微信服务器已经支付成功
                resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                        + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
            }
        }else{
            resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                    + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
        System.out.println(resXml);
        System.out.println("微信支付回调数据结束");
 
 
        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    }
//	public static class MiniprogramConfig implements WXPayConfig{
//
//	    private byte[] certData;
//
//	    public MiniprogramConfig() throws Exception {
//	    	
//	    	//java 程序
//	    	// abc.properties放在webroot/WEB-INF/classes/目录下
////	    	InputStream in = getClass().getClassLoader().getResourceAsStream("abc.properties"); 
//	    
//	        InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("cert/"+WeixinUtil.mchid+"/apiclient_cert.p12");
//	        this.certData = IOUtils.toByteArray(certStream);
//	        certStream.close();
//	    }
//
//	    @Override
//	    public String getAppID() {
//	        return WeixinUtil.my_app_id;
//	    }  //小程序appid
//
//	    @Override
//	    public String getMchID() {
//	        return WeixinUtil.mchid;
//	    }  //商户号
//
//	    /** 商户平台-账户中心-API安全中的密钥 */
//	    @Override
//	    public String getKey() {
//	        return WeixinUtil.my_app_secret;
//	    }
//
//	    @Override
//	    public InputStream getCertStream() {
//	        return new ByteArrayInputStream(this.certData);
//	    }
//
//	   
//
////		public int getTimeOutMs() {
////			// TODO Auto-generated method stub
////			return 0;
////		}
//
//		@Override
//		public int getHttpConnectTimeoutMs() {
//			// TODO Auto-generated method stub
//			return 0;
//		}
//
//		@Override
//		public int getHttpReadTimeoutMs() {
//			// TODO Auto-generated method stub
//			return 0;
//		}
//	}
//	private static Map<String, String> createMiniProgramTradePay() throws Exception {
//        MiniprogramConfig config = new MiniprogramConfig();
//
//        WXPay wxpay = new WXPay(config);
//    	String body="订单支付";
//    	String fee="0.01";
//    	String openid="";//
//    	String tradno="";//订单编号
//    	String notifyUrl="http://xcx.zhihuishu.com/Interface/WxPay/callback";//回调地址
//        Map<String, String> data = new HashMap<>();
//        data.put("appid",config.getAppID());
//        data.put("mch_id",config.getMchID());
//        data.put("body",body);
//        data.put("out_trade_no", tradno);
//        data.put("device_info", "");
//        data.put("fee_type", "CNY");
//        data.put("total_fee", fee);
//        data.put("spbill_create_ip", "127.0.0.1");
//        data.put("notify_url", notifyUrl);
//        data.put("trade_type", "JSAPI");
//        data.put("openid",openid);
////        data.put("nonce_str","1add1a30ac87aa2db72f57a2375d8fec");
//        String sign = WXPayUtil.generateSignature(data, config.getKey());
//        data.put("sign",sign);
//
//        Map<String, String> resp = wxpay.unifiedOrder(data);
//        if ("SUCCESS".equals(resp.get("return_code"))) {
//            //再次签名
//            /** 重要的事情说三遍  小程序支付 所有的字段必须大写 驼峰模式 严格按照小程序支付文档
//             *https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_7&index=3#
//             * ******* 我当初就因为timeStamp中S没大写弄了3个小时 **********
//             * */
//            Map<String, String> reData = new HashMap<>();
//            reData.put("appId", config.getAppID());
//            reData.put("nonceStr", resp.get("nonce_str"));
//            String newPackage = "prepay_id=" + resp.get("prepay_id");
//            reData.put("package", newPackage);
//            reData.put("signType","MD5");
//            reData.put("timeStamp", String.valueOf(System.currentTimeMillis() / 1000));
//
//            String newSign = WXPayUtil.generateSignature(reData, config.getKey());
//            resp.put("paySign",newSign);
//            resp.put("timeStamp", reData.get("timeStamp"));
//            return resp;
//        } else {
////            String  return_msg =resp.get("return_msg");
//            return resp;
//            
//        }
//
//    }
}
