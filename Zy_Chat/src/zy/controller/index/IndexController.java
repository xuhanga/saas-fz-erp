package zy.controller.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import zy.controller.BaseController;
@Controller
public class IndexController extends BaseController {
	@RequestMapping(value = "/to_cart", method = RequestMethod.GET)
	public String index() {
		return "cart/cart_list";
	}
	@RequestMapping(value = "/to_home", method = RequestMethod.GET)
	public String home() {
		return "index";
	}
	@RequestMapping(value = "/to_msg", method = RequestMethod.GET)
	public String msg() {
		return "cart/cart_list";
	}
	@RequestMapping(value = "/to_my", method = RequestMethod.GET)
	public String my() {
		return "my/my_list";
	}
	@RequestMapping(value = "/to_product", method = RequestMethod.GET)
	public String product() {
		return "product/product_list";
	}
}
