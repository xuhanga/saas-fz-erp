package zy.controller.sell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import wx.WxCallBack;
import zy.controller.BaseController;
import zy.entity.wx.order.T_Wx_Order;
import zy.entity.wx.order.T_Wx_Order_Detail;
import zy.service.order.WxOrderService;
import zy.util.CommonUtil;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping("api/wx/order")
public class OrderController extends BaseController{
	@Autowired
	private WxOrderService orderService;
	@RequestMapping(value = "getOrderList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getOrderList(@RequestBody Map<String, Object> params) {

		Object object = orderService.listOrder(params);
		return returnResult(object);
	}
	
	@RequestMapping(value = "getOrder", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getOrder(@RequestBody Map<String, Object> params) {
		T_Wx_Order t_wx_Order = orderService.getOrder(params);
		return returnResult(t_wx_Order);
	}
	
	@RequestMapping(value = "getOrderDetail", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getOrderDetail(@RequestBody Map<String, Object> params) {
		List<T_Wx_Order_Detail> detaillist = orderService.getOrderDetail(params);
		return returnResult(detaillist);
	}
	
	@RequestMapping(value = "confirmOrder", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object confirmOrder(@RequestBody Map<String, Object> params) {
		int flag = orderService.confirmOrder(params);
		return result(flag);
	}
	@RequestMapping(value = "cancleOrder", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object cancleOrder( @RequestBody Map<String, Object> params) {
		
		int flag = orderService.cancleOrder(params);
		return result(flag);
	}
	@RequestMapping(value = "saveOrder", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveOrder(@RequestBody Map<String, Object> params) {
		Object object = orderService.saveOrder(params);
		
		@SuppressWarnings("unchecked")
		Map<String,Object> map = (Map<String,Object>)object;
		//订单如果使用优惠券或者账户余额付款，则模拟微信回调接口处理相关业务数据状态回调
		if ("1".equals(map.get("orderstatus"))){
			WxCallBack wxcallback = new WxCallBack();
			wxcallback.OrderCallBack(map.get("ordernumber").toString(), map.get("paymoney").toString(), "");
		}
		return returnResult(object);
	}
}
