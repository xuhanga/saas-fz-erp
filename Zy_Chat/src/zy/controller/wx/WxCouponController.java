package zy.controller.wx;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.ecoupon.T_Sell_ECouponList;
import zy.service.wx.coupon.WxCouponService;

@Controller
@RequestMapping("api/wx/coupon")
public class WxCouponController extends BaseController{
	
	@Resource
	private WxCouponService wxCouponService;
	
	/**
	 * 获取领券中心优惠券列表
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getCouponCenterList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getCouponCenterList(@RequestBody Map<String, Object> params) {
		List<T_Sell_ECouponList> list = wxCouponService.getCouponCenterList(params);
		return ajaxSuccess(list);
	}
	
	/**
	 * 领取优惠券
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "drawCoupon", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object drawCoupon(@RequestBody Map<String, Object> params) {
		wxCouponService.drawCoupon(params);
		return ajaxSuccess();
	}
	
}
