package zy.controller.wx;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.wx.my.T_Wx_Address;
import zy.service.wx.my.WxMyService;

@Controller
@RequestMapping("api/wx/my")
public class WxMyController extends BaseController{
	
	@Resource
	private WxMyService wxMyService;
	
	@RequestMapping(value = "getMyValue/{mobile}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getMyValue(@PathVariable String mobile,@RequestBody Map<String, Object> params) {
		return ajaxSuccess(wxMyService.getMyValue(mobile,params));
	}
	
	//修改密码
	@RequestMapping(value = "updatePassword", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updatePassword(@RequestBody Map<String, Object> params) {
		wxMyService.updatePassword(params);
		return ajaxSuccess();
	}
	
	/**
	 * 根据用户code获取地址列表
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getAddrListByUserCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getAddrListByUserCode(@RequestBody Map<String, Object> params) {
		List<T_Wx_Address> list = wxMyService.getAddrListByUserCode(params);
		return ajaxSuccess(list);
	}
	
	/**
	 * 保存用户地址信息
	 * @param wxAddress
	 * @return
	 */
	@RequestMapping(value = "saveAddress", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object saveAddress(@RequestBody T_Wx_Address t_Wx_Address) {
		return ajaxSuccess(wxMyService.saveAddress(t_Wx_Address));
	}
	
	/**
	 * 设置默认地址
	 * @param wd_id
	 * @return
	 */
	@RequestMapping(value = "updateAddressStateById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateAddressStateById(@RequestBody Map<String, Object> params) {
		wxMyService.updateAddressStateById(params);
		return ajaxSuccess();
	}
	
	/**
	 * 根据地址表中ID获取地址信息
	 * @param wd_id 地址id
	 * @param companyid 商家编号
	 * @return
	 */
	@RequestMapping(value = "getAddressInfoById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getAddressInfoById(@RequestBody Map<String, Object> params) {
		T_Wx_Address t_Wx_Address = wxMyService.getAddressInfoById(params);
		return ajaxSuccess(t_Wx_Address);
	}
	
	/**
	 * 根据地址ID删除地址信息
	 * @param wd_id 地址id
	 * @return
	 */
	@RequestMapping(value = "deleteAddressById/{wd_id}", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object deleteAddressById(@PathVariable Integer wd_id) {
		wxMyService.deleteAddressById(wd_id);
		return ajaxSuccess();
	}
	
	/**
	 * 根据ID修改用户地址信息
	 * @param wxAddress
	 * @return
	 */
	@RequestMapping(value = "updateAddressById", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateAddressById(@RequestBody T_Wx_Address t_Wx_Address) {
		wxMyService.updateAddressById(t_Wx_Address);
		return ajaxSuccess();
	}
	
	/**
	 * 根据用户编号获取默认地址
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getDefaultAddressInfoByCode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getDefaultAddressInfoByCode(@RequestBody Map<String, Object> params) {
		T_Wx_Address t_Wx_Address = wxMyService.getDefaultAddressInfoByCode(params);
		return ajaxSuccess(t_Wx_Address);
	}
	
	/**
	 * 我的优惠券列表
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "getMyCouponList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getMyCouponList(@RequestBody Map<String, Object> params) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String status = "200";
//		try{
//			
			List<T_Sell_Ecoupon_User> list = wxMyService.getMyCouponList(params);
			return ajaxSuccess(list);
//			Object obj= ajaxSuccess(list);
//			
//		    map.put("status", status);
//	        map.put("data", obj);
//	        
//	    } catch (Exception e) {
//	    	 map.put("status", 500);
//	         map.put("msg", e.getMessage());
//	    }
//	    return ajaxSuccess(map);
	}
}
