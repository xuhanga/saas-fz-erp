package zy.controller.wx;

import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.wx.product.T_Wx_Product;
import zy.entity.wx.product.T_Wx_ProductType;
import zy.service.wx.product.WxProductTypeService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import zy.vo.wx.WxProductTypeVO;
import zy.vo.wx.WxProductVO;

@Controller
@RequestMapping("api/wx/producttype")
public class WxProductTypeController extends BaseController{
	@Autowired
	private WxProductTypeService wxProductTypeService;
	
	@RequestMapping(value = "list_level", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list_level(@RequestBody Map<String, Object> params) {
        List<T_Wx_ProductType> types = wxProductTypeService.list(params);
        for (T_Wx_ProductType item : types) {
			if(StringUtil.isNotEmpty(item.getPt_img_path())){
				item.setPt_img_path(CommonUtil.FTP_SERVER_PATH+item.getPt_img_path());
			}else {
				item.setPt_img_path(CommonUtil.DEFAULT_PICTURE);
			}
		}
		return ajaxSuccess(WxProductTypeVO.buildLevel(types));
	}
	
	
	@RequestMapping(value = "getProductType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getProductType(@RequestBody Map<String, Object> params) {
        List<T_Wx_ProductType> types = wxProductTypeService.getProductType(params);
//        for (T_Wx_ProductType item : types) {
//			if(StringUtil.isNotEmpty(item.getPt_img_path())){
//				item.setPt_img_path(CommonUtil.FTP_SERVER_PATH+item.getPt_img_path());
//			}else {
//				item.setPt_img_path(CommonUtil.DEFAULT_PICTURE);
//			}
//		}
		return ajaxSuccess(types);
	}

	@RequestMapping(value = "getProductByType", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object getProductByType(@RequestBody Map<String, Object> params) {
        List<T_Wx_Product> products = wxProductTypeService.getProductByType(params);
        for (T_Wx_Product item : products) {
			if(StringUtil.isNotEmpty(item.getPdm_img_path())){
				item.setPdm_img_path(CommonUtil.FTP_SERVER_PATH+item.getPdm_img_path());
			}else {
				item.setPdm_img_path(CommonUtil.DEFAULT_PICTURE);
			}
		}
		return ajaxSuccess(products);
	}
	
	
}
