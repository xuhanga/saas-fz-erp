package zy.dao.cart;

import java.util.List;
import java.util.Map;

import zy.entity.wx.cart.T_Wx_Cartlist;

public interface WxCartDAO {
	public List<T_Wx_Cartlist> listCart(Map<String, Object> paramMap);
	void modifyCartNum(Map<String,Object> paramMap);
	void deleteCart(List<T_Wx_Cartlist> list);
	void addCart(Map<String,Object> paramMap);
}
