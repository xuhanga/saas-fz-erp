package zy.dao.coupon.impl;

import org.springframework.stereotype.Repository;

import zy.dao.BaseDaoImpl;
import zy.dao.coupon.WxCouponDAO;
import zy.util.Arith;
@Repository
public class WxCouponImpl extends BaseDaoImpl implements WxCouponDAO{
	public static void main(String[] args) {
		double a = 10.12;
		double b = 1*a;
		double c = 2*a;
		double d = 3*a;
		double e = 4*a;
		double f = Arith.mul(5, a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);
		
	}
}
