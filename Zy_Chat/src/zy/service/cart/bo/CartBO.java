package zy.service.cart.bo;

import java.util.List;

import zy.entity.wx.cart.T_Wx_Cartlist;
import zy.util.StringUtil;

public class CartBO {
	public static void buildSkuName(List<T_Wx_Cartlist> list){
		if(null != list && list.size() > 0){
			for(T_Wx_Cartlist item:list){
				String br_name = "";
				if(StringUtil.isNotEmpty(item.getBr_name())){
					br_name = "/"+item.getBr_name();
				}
				item.setSub_name(item.getCr_name()+"/"+item.getSz_name()+ br_name);
			}
		}
	}
}
