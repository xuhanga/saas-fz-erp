package zy.service.order;

import java.util.List;
import java.util.Map;

import zy.entity.wx.order.T_Wx_Order;
import zy.entity.wx.order.T_Wx_Order_Detail;

public interface WxOrderService {
	
	Object listOrder(Map<String,Object> paramMap);
	T_Wx_Order getOrder(Map<String,Object> paramMap);
	int ModifyOrder(Map<String, Object> paramMap);
	List<T_Wx_Order_Detail> getOrderDetail(Map<String,Object> paramMap);
	int confirmOrder(Map<String,Object> paramMap);
	int cancleOrder(Map<String,Object> paramMap);
	Object saveOrder(Map<String,Object> paramMap);
}
