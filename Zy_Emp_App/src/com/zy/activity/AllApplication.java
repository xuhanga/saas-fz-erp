package com.zy.activity;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;

public class AllApplication extends Application{
	private Bitmap image;//用于查看图片详情
	private List<String> imgList;
	private List<Activity> activitylist = new LinkedList<Activity>();
	private static AllApplication instance = null;
 	
	public static synchronized AllApplication getInstance(){
		if(null == instance){
			instance = new AllApplication();
		}
		return instance;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}
	
	public Bitmap getImage() {
		return image;
	}
	public void setImage(Bitmap image) {
		this.image = image;
	}
	public List<String> getImgList() {
		return imgList;
	}
	public void setImgList(List<String> imgList) {
		this.imgList = imgList;
	}
	public void addActivity(Activity activity) {
		activitylist.add(activity);
	}
	
	public void exit(){
		for(Activity activity:activitylist){
			activity.finish();
		}
		System.exit(0);
	}
	public void clear(){
		for(Activity activity:activitylist){
			activity.finish();
		}
	}
	public void init(){
		imgList = null;
		clear();
	}
}
