package com.zy.activity;

import java.util.HashMap;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.util.CommonUtil;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.zy.util.SPUtil;
import com.zy.view.CrashHandler;

public class BaseActivity extends Activity{
	public static final int STATE_LOAD = 1;
	public static final int STATE_DELETE = 2;
	public static final int STATE_SAVE = 3;
	public static final int STATE_UPDATE = 4;
	public static final int STATE_ERROR = 10;
	public static final int REQUEST_CODE_SCANNER = 0x21;
	public static final int REQUEST_CODE_ADD = 0x22;
	public static final int REQUEST_CODE_EDIT = 0x23;
	public static final int REQUEST_CODE_CHOOSE = 0x24;
	public static final int REQUEST_CODE_DELETE = 0x25;
	
	public Typeface iconfont;
	public ProgressDialog progress;
	public SharedPreferences sp;
	
	public BaseActivity(){
	}
	protected void changeSoftKeys() {
		// �����̿�
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm.isActive()) {
			// �л�����
			imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		iconfont = Typeface.createFromAsset(BaseActivity.this.getAssets(), "iconfont/iconfont.ttf");
		sp = getSharedPreferences(SPUtil.FILE_NAME, Activity.MODE_PRIVATE);
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(this);
	}
	
	public EmpLoginDto getUser(){
		return SPUtil.getUser(this);
	}
	public Map<String, Object> buildBaseParam(){
		EmpLoginDto user = getUser();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CommonUtil.COMPANYID, user.getCompanyid());
		params.put(CommonUtil.SHOP_CODE, user.getEm_shop_code());
		params.put(CommonUtil.SHOP_TYPE, user.getShoptype());
		params.put(CommonUtil.SHOP_UPCODE, user.getShop_upcode());
		params.put(CommonUtil.SHOP_UPTYPE, user.getShop_uptype());
		return params;
	}
}
