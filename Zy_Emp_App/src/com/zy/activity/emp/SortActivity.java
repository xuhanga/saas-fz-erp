package com.zy.activity.emp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpSortDto;
import zy.dto.common.TabDto;
import zy.util.DateUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.common.TabGridAdapter;
import com.zy.adapter.emp.SortAdapter;
import com.zy.api.base.EmpAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;

public class SortActivity extends BaseActivity implements OnClickListener{
	private Handler handler;
	private GridView gv_type;
	private ListView listView;
	private SortAdapter sortAdapter;
	private TabGridAdapter tabGridAdapter;
	private List<EmpSortDto> sortDtos = new ArrayList<EmpSortDto>();
	private List<TabDto> tabDtos = new ArrayList<TabDto>();
	private TextView txt_title;
	private ImageView img_back;
	private EmpAPI empAPI;
	private int selectedIndex = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_emp_sort);
		initView();
		initListener();
		initHandle();
		loadData();
	}
	private void initView(){
		gv_type = (GridView)findViewById(R.id.gv_type);
		listView = (ListView) findViewById(R.id.lv_data);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
	}
	
	private void initListener(){
		empAPI = new EmpAPI(this);
		tabDtos.add(new TabDto(0, "昨日"));
		tabDtos.add(new TabDto(1, "今日"));
		tabDtos.add(new TabDto(2, "本周"));
		tabDtos.add(new TabDto(3, "本月"));
		tabGridAdapter = new TabGridAdapter(SortActivity.this, tabDtos);
		tabGridAdapter.setSelectedIndex(1);
		gv_type.setAdapter(tabGridAdapter);
		gv_type.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				tabGridAdapter.setSelectedIndex(position);
				tabGridAdapter.notifyDataSetChanged();
				selectedIndex = position;

				sortDtos.clear();
				sortAdapter.notifyDataSetChanged();
				loadData();
			}
		});
		
		sortAdapter = new SortAdapter(SortActivity.this,sortDtos);
		listView.setAdapter(sortAdapter);
		img_back.setOnClickListener(this);
	}
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD:
						if(msg.obj == null){
							return;
						}
						List<EmpSortDto> result = (List<EmpSortDto>) msg.obj;
						if(result != null && result.size()>0){
							sortDtos.addAll(result);
						}
						sortAdapter.notifyDataSetChanged();
		                break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(SortActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void loadData() {
		Map<String, Object> params = buildBaseParam();
		switch (selectedIndex) {
		case 0:// 昨日
			params.put("begindate",DateUtil.getYesterdayDate(DateUtil.FORMAT_YEAR_MON_DAY));
			params.put("enddate",DateUtil.getYesterdayDate(DateUtil.FORMAT_YEAR_MON_DAY)+ " 23:59:59");
			break;
		case 1:// 今日
			params.put("begindate", DateUtil.getYearMonthDate());
			params.put("enddate", DateUtil.getYearMonthDate() + " 23:59:59");
			break;
		case 2:// 本周
			params.put("begindate",DateUtil.weekFirstDay(DateUtil.getYearMonthDate()));
			params.put("enddate",DateUtil.weekLastDay(DateUtil.getYearMonthDate()) + " 23:59:59");
			break;
		case 3:// 本月
			params.put("begindate",DateUtil.monthStartDay(DateUtil.getYearMonthDate()));
			params.put("enddate",DateUtil.monthEndDay(DateUtil.getYearMonthDate()) + " 23:59:59");
			break;
		default:
			break;
		}
		empAPI.listEmpSort(params, handler, STATE_LOAD);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(SortActivity.this);
				break;
			default:
				break;
		}
	}
}
