package com.zy.activity.receive;


import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

import zy.entity.sell.day.T_Sell_Day;
import zy.util.DateUtil;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.base.EmpAPI;
import com.zy.api.sell.DayAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CacheVipMemberUtil;
import com.zy.util.Constants;

public class ReceiveIndexActivity extends BaseActivity implements OnClickListener{
	private Handler handler;
	private ImageView img_back;
	private Button btn_receive;
	private TextView tv_sell_money,tv_receive_amount;
	
	private String receiveState;
	private DayAPI dayAPI;
	private EmpAPI empAPI;
	private T_Sell_Day sellDay;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_receive_index);
		initView();
		initListener();
		initHandle();
		receiveState = CacheVipMemberUtil.getReceiveState();
		if("1".equals(receiveState)){
			ActivityUtil.start_Activity(ReceiveIndexActivity.this, ReceiveActivity.class,new BasicNameValuePair(Constants.TITLE, "接待顾客"));
			finish();
		}else {
			loadSellDay();
			loadSellMoney();
		}
	}
	
	private void loadSellDay(){
		Map<String, Object> params = buildBaseParam();
		params.put("em_code", getUser().getEm_code());
		dayAPI.queryDay(params, handler, STATE_LOAD);
	}
	private void loadSellMoney(){
		Map<String, Object> params = buildBaseParam();
		params.put("em_code", getUser().getEm_code());
		params.put("begindate", DateUtil.getYearMonthDate());
		params.put("enddate", DateUtil.getYearMonthDate() + " 23:59:59");
		empAPI.loadEmpSellMoney(params, handler, STATE_SAVE);
	}
	
	private void initView(){
		dayAPI = new DayAPI(this);
		empAPI = new EmpAPI(this);
		progress = new ProgressDialog(this);
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		btn_receive = (Button)findViewById(R.id.btn_receive);
		tv_sell_money = (TextView)findViewById(R.id.tv_sell_money);
		tv_receive_amount = (TextView)findViewById(R.id.tv_receive_amount);
	}
	private void initListener(){
		img_back.setOnClickListener(this);
		btn_receive.setOnClickListener(this);
	}
	@SuppressLint("HandlerLeak")
	private void initHandle(){
		handler = new Handler() {
			public void handleMessage(Message msg) {
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
				case STATE_LOAD:
					sellDay = (T_Sell_Day)msg.obj;
					tv_receive_amount.setText(String.valueOf(sellDay.getDa_receive()));
					break;
				case STATE_SAVE:
					Double sellMoney = (Double)msg.obj;
					tv_sell_money.setText(String.format("%.2f", sellMoney));
					break;
				case STATE_ERROR:
					String message = null;
					if (msg.obj != null) {
						message = msg.obj.toString();
					}
					ActivityUtil.showShortToast(ReceiveIndexActivity.this,message);
					break;
				}
				super.handleMessage(msg);
			}
		};
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			ActivityUtil.finish(ReceiveIndexActivity.this);
			break;
		case R.id.btn_receive:
			Map<String, Object> params = buildBaseParam();
			params.put("em_code", getUser().getEm_code());
			params.put("da_id", sellDay.getDa_id());
			dayAPI.comereceive(params, handler, -1);
			CacheVipMemberUtil.putReceiveState("1");
			ActivityUtil.start_Activity(ReceiveIndexActivity.this, ReceiveActivity.class,new BasicNameValuePair(Constants.TITLE, "接待顾客"));
			finish();
			break;
		default:
			break;
		}
	}
	
}
