package com.zy.activity.sale;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

import zy.entity.shop.sale.T_Shop_Sale;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.activity.vip.MemberInfoActivity;
import com.zy.adapter.sale.SaleAdapter;
import com.zy.api.sale.SaleAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.view.view.AutoListView.OnLoadListener;

public class SaleActivity  extends BaseActivity implements OnLoadListener,OnClickListener{
	private TextView txt_title;
	private ImageView img_back;
	private Handler handler;
	private ListView listView;
	private SaleAdapter saleAdapter;
	private SaleAPI saleAPI;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_sale);
		initView();
		initListener();
		initHandle();
		loadData();
	}
	private void initView(){
		txt_title = (TextView)findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		listView = (ListView) findViewById(R.id.lv_data);
	}
	private void initListener(){
		img_back.setOnClickListener(this);
		saleAdapter = new SaleAdapter(SaleActivity.this);
		listView.setAdapter(saleAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == (listView.getCount() - 1))
					return;
				ActivityUtil.start_Activity(SaleActivity.this, MemberInfoActivity.class, new BasicNameValuePair(Constants.TITLE, "��Ա�б�"));
			}
		});
	}
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD:{
						if(msg.obj == null){
							return;
						}
						List<T_Shop_Sale> result = (List<T_Shop_Sale>) msg.obj;
						saleAdapter.setSales(result);
						saleAdapter.notifyDataSetChanged();
		                break;
					}
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(SaleActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void loadData(){
		saleAPI = new SaleAPI(this);
		Map<String, Object> paramMap = buildBaseParam();
		saleAPI.list(paramMap, handler, STATE_LOAD);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(SaleActivity.this);
				break;
			case R.id.btn_search:
				flush();
				loadData();
				break;
			default:
				break;
		}
	}
	private void flush(){
		saleAdapter.setSales(new ArrayList<T_Shop_Sale>());
		saleAdapter.notifyDataSetChanged();
	}
	@Override
	public void onLoad() {
		loadData();
	}

}
