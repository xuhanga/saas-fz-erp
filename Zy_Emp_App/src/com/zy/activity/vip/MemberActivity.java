package com.zy.activity.vip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import zy.entity.vip.member.T_Vip_Member;
import zy.util.CommonUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.vip.MemberAdapter;
import com.zy.api.vip.MemberAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.dialog.TitleMenu.ActionItem;
import com.zy.view.dialog.popup.TopPopup;
import com.zy.view.dialog.popup.TopPopup.OnItemOnClickListener;
import com.zy.view.view.AutoListView;
import com.zy.view.view.AutoListView.OnLoadListener;

public class MemberActivity extends BaseActivity implements OnLoadListener,OnClickListener{
	private TopPopup topPopup;
	private Handler handler;
	private AutoListView autoListView;
	private MemberAdapter memberAdapter;
	private TextView txt_title;
	private ImageView img_back,img_screen,img_order;
	private int pageindex = 1;
	private List<T_Vip_Member> members = new ArrayList<T_Vip_Member>();
	private MemberAPI memberAPI;
	private int order_index = 0;
	private HashMap<String, Object> params;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_member);
		initView();
		initListener();
		initHandle();
		loadData();
		initPopWindow();
	}
	private void initPopWindow() {
		topPopup = new TopPopup(this, LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
		topPopup.addAction(new ActionItem(this, R.string.orderby_date,R.drawable.icon_add));
		topPopup.addAction(new ActionItem(this, R.string.orderby_consume_time,R.drawable.icon_add));
		topPopup.addAction(new ActionItem(this, R.string.orderby_consume_money,R.drawable.icon_add));
		topPopup.addAction(new ActionItem(this, R.string.orderby_consume_times,R.drawable.icon_add));
		topPopup.selectByIndex(order_index);
		topPopup.setItemOnClickListener(new OnItemOnClickListener() {
			@Override
			public void onItemClick(ActionItem item, int position) {
				order_index = position;
				topPopup.selectByIndex(order_index);
				flush();
				loadData();
			}
		});
	}
	private void initView(){
		memberAPI = new MemberAPI(this);
		autoListView = (AutoListView) findViewById(R.id.lv_data);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_screen = (ImageView) findViewById(R.id.img_screen);
		img_order = (ImageView) findViewById(R.id.img_order);
		img_back.setVisibility(View.VISIBLE);
	}
	
	private void initListener(){
		memberAdapter = new MemberAdapter(MemberActivity.this,members);
		autoListView.setOnLoadListener(this);
		autoListView.setAdapter(memberAdapter);
		autoListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == (autoListView.getCount() - 1))
					return;
				T_Vip_Member member = members.get(position);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.TITLE, "会员资料");
				bundle.putSerializable("member", member);
				ActivityUtil.start_Activity(MemberActivity.this, MemberInfoActivity.class, bundle);
			}
		});
		img_back.setOnClickListener(this);
		img_screen.setOnClickListener(this);
		img_order.setOnClickListener(this);
	}
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				switch (msg.what) {
					case STATE_LOAD:{
						if(msg.obj == null){
							return;
						}
						List<T_Vip_Member> result = (List<T_Vip_Member>) msg.obj;
						autoListView.onLoadComplete();
						autoListView.onRefreshComplete();
						members.addAll(result);
						if (members.size() > 0) {
							autoListView.setResultSize(result.size(), members.size());
			                pageindex = members.size()/CommonParam.pagesize+1;
			                memberAdapter.notifyDataSetChanged();
						}else {
							autoListView.setResultSize(result.size(), members.size());
						}
		                break;
					}
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(MemberActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	private void loadData() {
		Map<String, Object> paramMap = buildBaseParam();
		if(params != null){
			paramMap.putAll(params);
		}
		paramMap.put(CommonUtil.PAGEINDEX, pageindex);
		paramMap.put(CommonUtil.PAGESIZE, CommonUtil.PAGE_SIZE);
		switch (order_index) {
			case 0://办卡日期
				paramMap.put(CommonUtil.SIDX, "vm_id");
				paramMap.put(CommonUtil.SORD, "DESC");
				break;
			case 1://消费时间
				paramMap.put(CommonUtil.SIDX, "vm_lastbuy_date");
				paramMap.put(CommonUtil.SORD, "DESC");
				break;
			case 2://消费金额
				paramMap.put(CommonUtil.SIDX, "vm_total_money");
				paramMap.put(CommonUtil.SORD, "DESC");
				break;
			case 3://消费次数
				paramMap.put(CommonUtil.SIDX, "vm_times");
				paramMap.put(CommonUtil.SORD, "DESC");
				break;
			default:
				break;
		}
		memberAPI.list(paramMap, handler, STATE_LOAD);
	}
	
	@Override
	public void onLoad() {
		loadData();
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(MemberActivity.this);
				break;
			case R.id.img_order:
				topPopup.show(findViewById(R.id.layout_bar));
				break;
			case R.id.img_screen:
				Bundle bundle = new Bundle();
				bundle.putString(Constants.TITLE, "高级筛选");
				ActivityUtil.start_ActivityForResult(MemberActivity.this, ScreenActivity.class, REQUEST_CODE_CHOOSE, bundle);
				break;
			case R.id.btn_search:
				flush();
				loadData();
				break;
			default:
				break;
		}
	}
	private void flush(){
		pageindex = 1;
		members.clear();
		memberAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			if(requestCode == REQUEST_CODE_CHOOSE){//高级筛选
				params = (HashMap<String, Object>)data.getSerializableExtra("params");
				flush();
				loadData();
			}
		}
	}
	
}
