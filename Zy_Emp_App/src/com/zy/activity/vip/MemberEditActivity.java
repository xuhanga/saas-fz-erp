package com.zy.activity.vip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.entity.vip.membertype.T_Vip_MemberType;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.LunarCalendar;
import zy.util.StringUtil;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.vip.MemberAPI;
import com.zy.api.vip.MemberTypeAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.dialog.BirthdayPickerDialog;
import com.zy.view.dialog.DatePickerDialog;
import com.zy.view.dialog.popup.BottomPopup;

public class MemberEditActivity extends BaseActivity implements OnClickListener{
	private Handler handler;
	private TextView txt_title,txt_right;
	private TextView tv_tab_base,tv_tab_life,tv_tab_wear;
	private ImageView img_back;
	private Button btn_more,btn_samemobile;
	private EditText et_vm_name,et_vm_mobile,et_vm_cardcode;
	private TextView tv_vm_mt_code,tv_vm_birthday,tv_vm_date,tv_vm_enddate;
	private EditText et_vmi_address,et_vmi_idcard,et_vmi_work_unit,et_vmi_qq,et_vmi_wechat,et_vmi_email,et_vmi_faith,et_vmi_family_festival,et_vmi_family,
				et_vmi_oral_habit,et_vmi_like_topic,et_vmi_taboo_topic,et_vmi_transport,et_vmi_smoke,et_vmi_like_food,et_vmi_hobby,et_vmi_character,
				et_vmi_height,et_vmi_weight,et_vmi_skin,et_vmi_bust,et_vmi_waist,et_vmi_hips,et_vmi_size_top,et_vmi_size_lower,et_vmi_size_shoes,et_vmi_size_bra,
				et_vmi_trousers_length,et_vmi_like_color,et_vmi_like_brand,et_vmi_wear_prefer;
	private TextView tv_vm_sex,tv_vmi_marriage_state,tv_vmi_nation,tv_vmi_bloodtype,tv_vmi_idcard_type,tv_vmi_job,tv_vmi_culture,tv_vmi_income_level,tv_vmi_foottype;
	
	private List<T_Vip_MemberType> memberTypes = new ArrayList<T_Vip_MemberType>();
	private MemberTypeAPI memberTypeAPI;
	private MemberAPI memberAPI;
	private BottomPopup memberTypeBottomPopup;
	private DatePickerDialog vmDateDialog,vmEndDateDialog;
	private BirthdayPickerDialog vmBirthdayDialog;
	
	private T_Vip_Member member;
	private T_Vip_Member_Info memberInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_member_add);
		initData();
		initView();
		initListener();
		initHandle();
		loadData();
	}
	
	private void initView(){
		findViewById(R.id.ll_head).setVisibility(View.GONE);
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_right = (TextView) findViewById(R.id.txt_right);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		txt_right.setText("保存");
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		btn_more = (Button)findViewById(R.id.btn_more);
		btn_samemobile = (Button)findViewById(R.id.btn_samemobile);
		tv_tab_base = (TextView) findViewById(R.id.tv_tab_base);
		tv_tab_life = (TextView) findViewById(R.id.tv_tab_life);
		tv_tab_wear = (TextView) findViewById(R.id.tv_tab_wear);
		tv_vm_mt_code = (TextView) findViewById(R.id.tv_vm_mt_code);
		tv_vm_birthday = (TextView) findViewById(R.id.tv_vm_birthday);
		tv_vm_date = (TextView) findViewById(R.id.tv_vm_date);
		tv_vm_enddate = (TextView) findViewById(R.id.tv_vm_enddate);
		et_vm_name = (EditText) findViewById(R.id.et_vm_name);
		et_vm_mobile = (EditText) findViewById(R.id.et_vm_mobile);
		et_vm_cardcode = (EditText) findViewById(R.id.et_vm_cardcode);
		et_vmi_address = (EditText) findViewById(R.id.et_vmi_address);
		et_vmi_idcard = (EditText) findViewById(R.id.et_vmi_idcard);
		et_vmi_work_unit = (EditText) findViewById(R.id.et_vmi_work_unit);
		et_vmi_qq = (EditText) findViewById(R.id.et_vmi_qq);
		et_vmi_wechat = (EditText) findViewById(R.id.et_vmi_wechat);
		et_vmi_email = (EditText) findViewById(R.id.et_vmi_email);
		et_vmi_faith = (EditText) findViewById(R.id.et_vmi_faith);
		et_vmi_family_festival = (EditText) findViewById(R.id.et_vmi_family_festival);
		et_vmi_family = (EditText) findViewById(R.id.et_vmi_family);
		et_vmi_oral_habit = (EditText) findViewById(R.id.et_vmi_oral_habit);
		et_vmi_like_topic = (EditText) findViewById(R.id.et_vmi_like_topic);
		et_vmi_taboo_topic = (EditText) findViewById(R.id.et_vmi_taboo_topic);
		et_vmi_transport = (EditText) findViewById(R.id.et_vmi_transport);
		et_vmi_smoke = (EditText) findViewById(R.id.et_vmi_smoke);
		et_vmi_like_food = (EditText) findViewById(R.id.et_vmi_like_food);
		et_vmi_hobby = (EditText) findViewById(R.id.et_vmi_hobby);
		et_vmi_character = (EditText) findViewById(R.id.et_vmi_character);
		et_vmi_height = (EditText) findViewById(R.id.et_vmi_height);
		et_vmi_weight = (EditText) findViewById(R.id.et_vmi_weight);
		et_vmi_skin = (EditText) findViewById(R.id.et_vmi_skin);
		et_vmi_bust = (EditText) findViewById(R.id.et_vmi_bust);
		et_vmi_waist = (EditText) findViewById(R.id.et_vmi_waist);
		et_vmi_hips = (EditText) findViewById(R.id.et_vmi_hips);
		et_vmi_size_top = (EditText) findViewById(R.id.et_vmi_size_top);
		et_vmi_size_lower = (EditText) findViewById(R.id.et_vmi_size_lower);
		et_vmi_size_shoes = (EditText) findViewById(R.id.et_vmi_size_shoes);
		et_vmi_size_bra = (EditText) findViewById(R.id.et_vmi_size_bra);
		et_vmi_trousers_length = (EditText) findViewById(R.id.et_vmi_trousers_length);
		et_vmi_like_color = (EditText) findViewById(R.id.et_vmi_like_color);
		et_vmi_like_brand = (EditText) findViewById(R.id.et_vmi_like_brand);
		et_vmi_wear_prefer = (EditText) findViewById(R.id.et_vmi_wear_prefer);
		
		tv_vm_sex = (TextView) findViewById(R.id.tv_vm_sex);
		tv_vmi_marriage_state = (TextView) findViewById(R.id.tv_vmi_marriage_state);
		tv_vmi_nation = (TextView) findViewById(R.id.tv_vmi_nation);
		tv_vmi_bloodtype = (TextView) findViewById(R.id.tv_vmi_bloodtype);
		tv_vmi_idcard_type = (TextView) findViewById(R.id.tv_vmi_idcard_type);
		tv_vmi_job = (TextView) findViewById(R.id.tv_vmi_job);
		tv_vmi_culture = (TextView) findViewById(R.id.tv_vmi_culture);
		tv_vmi_income_level = (TextView) findViewById(R.id.tv_vmi_income_level);
		tv_vmi_foottype = (TextView) findViewById(R.id.tv_vmi_foottype);
		initViewValue();
	}
	
	private void initViewValue(){
		if(member != null){
			et_vm_name.setText(StringUtil.trimString(member.getVm_name()));
			et_vm_mobile.setText(StringUtil.trimString(member.getVm_mobile()));
			et_vm_cardcode.setText(StringUtil.trimString(member.getVm_cardcode()));
			tv_vm_mt_code.setText(StringUtil.trimString(member.getMt_name()));
			if(member.getVm_birthday_type() != null){
				if(member.getVm_birthday_type().intValue() == 0){//公历
					tv_vm_birthday.setText(StringUtil.trimString(member.getVm_birthday())+"  "+formatBirthdayType(member.getVm_birthday_type()));
				}else if(member.getVm_birthday_type().intValue() == 1){//农历
					tv_vm_birthday.setText(StringUtil.trimString(member.getVm_lunar_birth())+"  "+formatBirthdayType(member.getVm_birthday_type()));
				}
			}
			tv_vm_date.setText(StringUtil.trimString(member.getVm_date()));
			tv_vm_enddate.setText(StringUtil.trimString(member.getVm_enddate()));
			tv_vm_sex.setText(StringUtil.trimString(member.getVm_sex()));
		}
		if(memberInfo != null){
			et_vmi_address.setText(StringUtil.trimString(memberInfo.getVmi_address()));
			et_vmi_idcard.setText(StringUtil.trimString(memberInfo.getVmi_idcard()));
			et_vmi_work_unit.setText(StringUtil.trimString(memberInfo.getVmi_work_unit()));
			et_vmi_qq.setText(StringUtil.trimString(memberInfo.getVmi_qq()));
			et_vmi_wechat.setText(StringUtil.trimString(memberInfo.getVmi_wechat()));
			et_vmi_email.setText(StringUtil.trimString(memberInfo.getVmi_email()));
			et_vmi_faith.setText(StringUtil.trimString(memberInfo.getVmi_faith()));
			et_vmi_family_festival.setText(StringUtil.trimString(memberInfo.getVmi_family_festival()));
			et_vmi_family.setText(StringUtil.trimString(memberInfo.getVmi_family()));
			et_vmi_oral_habit.setText(StringUtil.trimString(memberInfo.getVmi_oral_habit()));
			et_vmi_like_topic.setText(StringUtil.trimString(memberInfo.getVmi_like_topic()));
			et_vmi_taboo_topic.setText(StringUtil.trimString(memberInfo.getVmi_taboo_topic()));
			et_vmi_transport.setText(StringUtil.trimString(memberInfo.getVmi_transport()));
			et_vmi_smoke.setText(StringUtil.trimString(memberInfo.getVmi_smoke()));
			et_vmi_like_food.setText(StringUtil.trimString(memberInfo.getVmi_like_food()));
			et_vmi_hobby.setText(StringUtil.trimString(memberInfo.getVmi_hobby()));
			et_vmi_character.setText(StringUtil.trimString(memberInfo.getVmi_character()));
			et_vmi_height.setText(StringUtil.trimString(memberInfo.getVmi_height()));
			et_vmi_weight.setText(StringUtil.trimString(memberInfo.getVmi_weight()));
			et_vmi_skin.setText(StringUtil.trimString(memberInfo.getVmi_skin()));
			et_vmi_bust.setText(StringUtil.trimString(memberInfo.getVmi_bust()));
			et_vmi_waist.setText(StringUtil.trimString(memberInfo.getVmi_waist()));
			et_vmi_hips.setText(StringUtil.trimString(memberInfo.getVmi_hips()));
			et_vmi_size_top.setText(StringUtil.trimString(memberInfo.getVmi_size_top()));
			et_vmi_size_lower.setText(StringUtil.trimString(memberInfo.getVmi_size_lower()));
			et_vmi_size_shoes.setText(StringUtil.trimString(memberInfo.getVmi_size_shoes()));
			et_vmi_size_bra.setText(StringUtil.trimString(memberInfo.getVmi_size_bra()));
			et_vmi_trousers_length.setText(StringUtil.trimString(memberInfo.getVmi_trousers_length()));
			et_vmi_like_color.setText(StringUtil.trimString(memberInfo.getVmi_like_color()));
			et_vmi_like_brand.setText(StringUtil.trimString(memberInfo.getVmi_like_brand()));
			et_vmi_wear_prefer.setText(StringUtil.trimString(memberInfo.getVmi_wear_prefer()));
			
			tv_vmi_marriage_state.setText(formatMarriageState(memberInfo.getVmi_marriage_state()));
			tv_vmi_nation.setText(StringUtil.trimString(memberInfo.getVmi_nation()));
			tv_vmi_bloodtype.setText(StringUtil.trimString(memberInfo.getVmi_bloodtype()));
			tv_vmi_idcard_type.setText(formatIdcardType(memberInfo.getVmi_idcard_type()));
			tv_vmi_job.setText(StringUtil.trimString(memberInfo.getVmi_job()));
			tv_vmi_culture.setText(StringUtil.trimString(memberInfo.getVmi_culture()));
			tv_vmi_income_level.setText(StringUtil.trimString(memberInfo.getVmi_income_level()));
			tv_vmi_foottype.setText(StringUtil.trimString(memberInfo.getVmi_foottype()));
		}
	}
	
	private void initData(){
		progress = new ProgressDialog(this);
		memberTypeAPI = new MemberTypeAPI(this);
		memberAPI = new MemberAPI(this);
		member = (T_Vip_Member)getIntent().getSerializableExtra("member");
	}
	
	private void initListener(){
		img_back.setOnClickListener(this);
		txt_right.setOnClickListener(this);
		btn_more.setOnClickListener(this);
		btn_samemobile.setOnClickListener(this);
		tv_vm_mt_code.setOnClickListener(this);
		tv_vm_birthday.setOnClickListener(this);
		tv_vm_date.setOnClickListener(this);
		tv_vm_enddate.setOnClickListener(this);
	}
	
	private void initHandle(){
		handler = new Handler() {
			@SuppressWarnings("unchecked")
			public void handleMessage(Message msg) {
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
					case CommonParam.STATE_LOAD:
						Map<String, Object> resultMap = (Map<String, Object>)msg.obj;
						member = (T_Vip_Member)resultMap.get("member");
						memberInfo = (T_Vip_Member_Info)resultMap.get("memberInfo");
						initViewValue();
						break;
					case R.id.tv_vm_mt_code:
						List<T_Vip_MemberType> result = (List<T_Vip_MemberType>)msg.obj;
						memberTypes.addAll(result);
						String[] memberTypesOption = new String[memberTypes.size()];
						for (int i = 0; i < memberTypes.size(); i++) {
							memberTypesOption[i] = memberTypes.get(i).getMt_name();
						}
						memberTypeBottomPopup = new BottomPopup(MemberEditActivity.this, "会员类别", memberTypesOption);
						memberTypeBottomPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
							@Override
							public void onItemClick(int position) {
								member.setVm_mt_code(memberTypes.get(position).getMt_code());
								tv_vm_mt_code.setText(memberTypes.get(position).getMt_name());
							}
						});
						memberTypeBottomPopup.showView();
						break;
					case R.id.txt_right:
						ActivityUtil.showShortToast(MemberEditActivity.this, "保存成功");
						Intent intent = new Intent();
						intent.putExtra("member", member);
						intent.putExtra("memberInfo", memberInfo);
						setResult(RESULT_OK, intent);
						ActivityUtil.finish(MemberEditActivity.this);
						break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(MemberEditActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	private void loadData() {
		memberAPI.load(member.getVm_id(), handler, CommonParam.STATE_LOAD);
	}
	
	private String formatBirthdayType(Integer birthdaytype){
		if(birthdaytype == null){
			return "";
		}
		if(birthdaytype.intValue() == 0){
			return "公历";
		}
		if(birthdaytype.intValue() == 1){
			return "农历";
		}
		return "";
	}
	private String formatMarriageState(Integer vmi_marriage_state){//0未婚 1已婚 2曾婚
		if(vmi_marriage_state == null){
			return "";
		}
		if(vmi_marriage_state.intValue() == 0){
			return "未婚";
		}
		if(vmi_marriage_state.intValue() == 1){
			return "已婚";
		}
		if(vmi_marriage_state.intValue() == 2){
			return "曾婚";
		}
		return "";
	}
	private String formatIdcardType(Integer vmi_idcard_type){//0身份证 1军官证 2驾照  3护照 4学生证 5其他
		if(vmi_idcard_type == null){
			return "";
		}
		if(vmi_idcard_type.intValue() == 0){
			return "身份证";
		}
		if(vmi_idcard_type.intValue() == 1){
			return "军官证";
		}
		if(vmi_idcard_type.intValue() == 2){
			return "驾照";
		}
		if(vmi_idcard_type.intValue() == 3){
			return "护照";
		}
		if(vmi_idcard_type.intValue() == 4){
			return "4学生证";
		}
		if(vmi_idcard_type.intValue() == 5){
			return "其他";
		}
		return "";
	}
	
	private void buildModel(){
		member.setVm_name(StringUtil.trimString(et_vm_name.getText()));
		member.setVm_mobile(StringUtil.trimString(et_vm_mobile.getText()));
		member.setVm_cardcode(StringUtil.trimString(et_vm_cardcode.getText()));
		memberInfo.setVmi_address(StringUtil.trimString(et_vmi_address.getText()));
		memberInfo.setVmi_idcard(StringUtil.trimString(et_vmi_idcard.getText()));
		memberInfo.setVmi_work_unit(StringUtil.trimString(et_vmi_work_unit.getText()));
		memberInfo.setVmi_qq(StringUtil.trimString(et_vmi_qq.getText()));
		memberInfo.setVmi_wechat(StringUtil.trimString(et_vmi_wechat.getText()));
		memberInfo.setVmi_email(StringUtil.trimString(et_vmi_email.getText()));
		memberInfo.setVmi_faith(StringUtil.trimString(et_vmi_faith.getText()));
		memberInfo.setVmi_family_festival(StringUtil.trimString(et_vmi_family_festival.getText()));
		memberInfo.setVmi_family(StringUtil.trimString(et_vmi_family.getText()));
		memberInfo.setVmi_oral_habit(StringUtil.trimString(et_vmi_oral_habit.getText()));
		memberInfo.setVmi_like_topic(StringUtil.trimString(et_vmi_like_topic.getText()));
		memberInfo.setVmi_taboo_topic(StringUtil.trimString(et_vmi_taboo_topic.getText()));
		memberInfo.setVmi_transport(StringUtil.trimString(et_vmi_transport.getText()));
		memberInfo.setVmi_smoke(StringUtil.trimString(et_vmi_smoke.getText()));
		memberInfo.setVmi_like_food(StringUtil.trimString(et_vmi_like_food.getText()));
		memberInfo.setVmi_hobby(StringUtil.trimString(et_vmi_hobby.getText()));
		memberInfo.setVmi_character(StringUtil.trimString(et_vmi_character.getText()));
		memberInfo.setVmi_height(StringUtil.trimString(et_vmi_height.getText()));
		memberInfo.setVmi_weight(StringUtil.trimString(et_vmi_weight.getText()));
		memberInfo.setVmi_skin(StringUtil.trimString(et_vmi_skin.getText()));
		memberInfo.setVmi_bust(StringUtil.trimString(et_vmi_bust.getText()));
		memberInfo.setVmi_waist(StringUtil.trimString(et_vmi_waist.getText()));
		memberInfo.setVmi_hips(StringUtil.trimString(et_vmi_hips.getText()));
		memberInfo.setVmi_size_top(StringUtil.trimString(et_vmi_size_top.getText()));
		memberInfo.setVmi_size_lower(StringUtil.trimString(et_vmi_size_lower.getText()));
		memberInfo.setVmi_size_shoes(StringUtil.trimString(et_vmi_size_shoes.getText()));
		memberInfo.setVmi_size_bra(StringUtil.trimString(et_vmi_size_bra.getText()));
		memberInfo.setVmi_trousers_length(StringUtil.trimString(et_vmi_trousers_length.getText()));
		memberInfo.setVmi_like_color(StringUtil.trimString(et_vmi_like_color.getText()));
		memberInfo.setVmi_like_brand(StringUtil.trimString(et_vmi_like_brand.getText()));
		memberInfo.setVmi_wear_prefer(StringUtil.trimString(et_vmi_wear_prefer.getText()));
	}
	
	private void save(){
		buildModel();
		if(StringUtil.isEmpty(member.getVm_name())){
			ActivityUtil.showShortToast(MemberEditActivity.this, "请输入会员姓名");
			return;
		}
		if(StringUtil.isEmpty(member.getVm_mobile())){
			ActivityUtil.showShortToast(MemberEditActivity.this, "请输入会员手机号码");
			return;
		}
		if(StringUtil.isEmpty(member.getVm_cardcode())){
			ActivityUtil.showShortToast(MemberEditActivity.this, "请输入会员卡号");
			return;
		}
		if(StringUtil.isEmpty(member.getVm_sex())){
			ActivityUtil.showShortToast(MemberEditActivity.this, "请选择性别");
			return;
		}
		if(StringUtil.isEmpty(member.getVm_mt_code())){
			ActivityUtil.showShortToast(MemberEditActivity.this, "请选择卡类型");
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("member", JSON.toJSONString(member));
		params.put("memberInfo", JSON.toJSONString(memberInfo));
		params.put("empLoginDto", JSON.toJSONString(getUser()));
		progress.show();
		memberAPI.update(params, handler, R.id.txt_right);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.img_back:
				ActivityUtil.finish(MemberEditActivity.this);
				break;
			case R.id.txt_right:
				save();
				break;
			case R.id.btn_samemobile:
				et_vm_cardcode.setText(et_vm_mobile.getText());
				break;
			case R.id.btn_more:
				findViewById(R.id.rl_more).setVisibility(View.GONE);
				findViewById(R.id.ll_other_tab).setVisibility(View.VISIBLE);
				findViewById(R.id.ll_base_info).setVisibility(View.VISIBLE);
				break;
			case R.id.tv_vm_mt_code:
				if(memberTypes == null || memberTypes.size() == 0){
					EmpLoginDto loginDto = getUser();
					Map<String, Object> params = new HashMap<String, Object>();
					params.put(CommonUtil.COMPANYID, loginDto.getCompanyid());
					params.put(CommonUtil.SHOP_CODE, loginDto.getShop_upcode());
					memberTypeAPI.list(params, handler, R.id.tv_vm_mt_code);
				}else {
					memberTypeBottomPopup.showView();
				}
				break;
			case R.id.tv_vm_birthday:
				if(vmBirthdayDialog == null){
					vmBirthdayDialog = new BirthdayPickerDialog(MemberEditActivity.this, null);
					vmBirthdayDialog.getOkButton().setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String birthday = vmBirthdayDialog.getDate();
							Integer birthdaytype = vmBirthdayDialog.getBirthdayType();
							tv_vm_birthday.setText(birthday+"  "+formatBirthdayType(birthdaytype));
							member.setVm_birthday_type(birthdaytype);
							if(birthdaytype.intValue() == 0){//公历
								member.setVm_birthday(birthday);
								member.setVm_lunar_birth(LunarCalendar.solarToLunar(birthday));
							}else if(birthdaytype.intValue() == 1){//农历
								member.setVm_lunar_birth(birthday);
								member.setVm_birthday(LunarCalendar.lunarToSolar(birthday));
							}
							memberInfo.setVmi_anima(LunarCalendar.animalsYear(member.getVm_lunar_birth()));
							vmBirthdayDialog.dismiss();
						}
					});
				}
				vmBirthdayDialog.show();
				break;
			case R.id.tv_vm_date:
				if (vmDateDialog == null) {
					vmDateDialog = new DatePickerDialog(MemberEditActivity.this, null);
					vmDateDialog.getOkButton().setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String date = vmDateDialog.getDate();
							tv_vm_date.setText(date);
							tv_vm_enddate.setText(DateUtil.getDateAddYears(date, 20));
							member.setVm_date(StringUtil.trimString(tv_vm_date.getText()));
							member.setVm_enddate(StringUtil.trimString(tv_vm_enddate.getText()));
							vmDateDialog.dismiss();
						}
					});
				}
				vmDateDialog.show();
				break;
			case R.id.tv_vm_enddate:
				vmEndDateDialog = new DatePickerDialog(MemberEditActivity.this, member.getVm_enddate());
				vmEndDateDialog.getOkButton().setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						tv_vm_enddate.setText(vmEndDateDialog.getDate());
						member.setVm_enddate(StringUtil.trimString(tv_vm_enddate.getText()));
						vmEndDateDialog.dismiss();
					}
				});
				vmEndDateDialog.show();
				break;
			default:
				break;
		}
	}
	
	public void onTabClick(View v) {
		findViewById(R.id.ll_base_info).setVisibility(View.GONE);
		findViewById(R.id.ll_life_info).setVisibility(View.GONE);
		findViewById(R.id.ll_wear_info).setVisibility(View.GONE);
		tv_tab_base.setTextColor(getResources().getColor(R.color.font_value));
		tv_tab_life.setTextColor(getResources().getColor(R.color.font_value));
		tv_tab_wear.setTextColor(getResources().getColor(R.color.font_value));
		switch (v.getId()) {
			case R.id.tv_tab_base:
				findViewById(R.id.ll_base_info).setVisibility(View.VISIBLE);
				tv_tab_base.setTextColor(getResources().getColor(R.color.blue));
				break;
			case R.id.tv_tab_life:
				findViewById(R.id.ll_life_info).setVisibility(View.VISIBLE);
				tv_tab_life.setTextColor(getResources().getColor(R.color.blue));
				break;
			case R.id.tv_tab_wear:
				findViewById(R.id.ll_wear_info).setVisibility(View.VISIBLE);
				tv_tab_wear.setTextColor(getResources().getColor(R.color.blue));
				break;
		}
	}
	
	public void onTextViewClick(View v){
		switch (v.getId()) {
		case R.id.tv_vm_sex:
			final String[] sexOptions = new String[]{"男","女"};
			BottomPopup vmSexPopup = new BottomPopup(MemberEditActivity.this, "性别", sexOptions);
			vmSexPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vm_sex)).setText(sexOptions[position]);
					member.setVm_sex(sexOptions[position]);
				}
			});
			vmSexPopup.showView();
			break;
		case R.id.tv_vmi_marriage_state:
			final String[] marriageOptions = new String[]{"未婚","已婚","曾婚"};//:0未婚 1已婚 2曾婚
			BottomPopup marriagePopup = new BottomPopup(MemberEditActivity.this, "婚姻状况", marriageOptions);
			marriagePopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_marriage_state)).setText(marriageOptions[position]);
					memberInfo.setVmi_marriage_state(position);
				}
			});
			marriagePopup.showView();		
			break;
		case R.id.tv_vmi_nation:
			final String[] nationOptions = new String[]{"汉族","蒙古族","回族","藏族","维吾尔族","苗族","彝族","壮族","布依族","朝鲜族",
					"满族","侗族","瑶族","白族","土家族","哈尼族","哈萨克族","傣族","黎族","傈僳族","佤族","畲族","高山族","拉祜族","水族",
					"东乡族","纳西族","景颇族","柯尔克孜族","土族","达斡尔族","仫佬族","羌族","布朗族","撒拉族","毛难族","仡佬族","锡伯族","阿昌族","普米族",
					"塔吉克族","怒族","乌孜别克族","俄罗斯族","鄂温克族","崩龙族","保安族","裕固族","京族","塔塔尔族","独龙族","鄂伦春族","赫哲族",
					"门巴族","珞巴族","基诺族"};
			BottomPopup nationPopup = new BottomPopup(MemberEditActivity.this, "民族", nationOptions);
			nationPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_nation)).setText(nationOptions[position]);
					memberInfo.setVmi_nation(nationOptions[position]);
				}
			});
			nationPopup.showView();
			break;
		case R.id.tv_vmi_bloodtype:
			final String[] bloodtypeOptions = new String[]{"A","B","AB","O"};
			BottomPopup bloodtypePopup = new BottomPopup(MemberEditActivity.this, "血型", bloodtypeOptions);
			bloodtypePopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_bloodtype)).setText(bloodtypeOptions[position]);
					memberInfo.setVmi_bloodtype(bloodtypeOptions[position]);
				}
			});
			bloodtypePopup.showView();
			break;
		case R.id.tv_vmi_idcard_type:
			final String[] idcardtypeOptions = new String[]{"身份证","军官证","驾照","护照","学生证","其他"};//0身份证 1军官证 2驾照  3护照 4学生证 5其他'
			BottomPopup idcardtypePopup = new BottomPopup(MemberEditActivity.this, "证件类型", idcardtypeOptions);
			idcardtypePopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_idcard_type)).setText(idcardtypeOptions[position]);
					memberInfo.setVmi_idcard_type(position);
				}
			});
			idcardtypePopup.showView();
			break;
		case R.id.tv_vmi_job:
			final String[] jobOptions = new String[] { "老板", "企业管理者", "公务员", "教师", "职员", "军人", "学生", "医生", "其他" };
			BottomPopup jobPopup = new BottomPopup(MemberEditActivity.this, "职业", jobOptions);
			jobPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_job)).setText(jobOptions[position]);
					memberInfo.setVmi_job(jobOptions[position]);
				}
			});
			jobPopup.showView();
			break;
		case R.id.tv_vmi_culture:
			final String[] cultureOptions = new String[] { "高中及以下", "大专", "本科", "研究生", "博士及以上", "其他" };
			BottomPopup culturePopup = new BottomPopup(MemberEditActivity.this, "文化程度", cultureOptions);
			culturePopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_culture)).setText(cultureOptions[position]);
					memberInfo.setVmi_culture(cultureOptions[position]);
				}
			});
			culturePopup.showView();
			break;
		case R.id.tv_vmi_income_level:
			final String[] incomelevelOptions = new String[] { "保密", "1000以下", "1000-3000", "3000-5000", "5000-8000", "10000以上" };
			BottomPopup incomelevelPopup = new BottomPopup(MemberEditActivity.this, "收入水平", incomelevelOptions);
			incomelevelPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_income_level)).setText(incomelevelOptions[position]);
					memberInfo.setVmi_income_level(incomelevelOptions[position]);
				}
			});
			incomelevelPopup.showView();
			break;
		case R.id.tv_vmi_foottype:
			final String[] foottypeOptions = new String[] { "正常", "偏胖", "偏瘦", "脚面扁平", "脚面偏高"};
			BottomPopup foottypePopup = new BottomPopup(MemberEditActivity.this, "脚型", foottypeOptions);
			foottypePopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vmi_foottype)).setText(foottypeOptions[position]);
					memberInfo.setVmi_foottype(foottypeOptions[position]);
				}
			});
			foottypePopup.showView();
			break;
		}
	}
}
