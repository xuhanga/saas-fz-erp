package com.zy.activity.vip;

import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.view.fragment.vip.Fragment_Info;
import com.zy.view.fragment.vip.Fragment_Report;
import com.zy.view.fragment.vip.Fragment_Sell;
import com.zy.view.fragment.vip.Fragment_Try;

public class MemberInfoActivity extends FragmentActivity implements OnClickListener{
	private Fragment[] fragments;
	private TextView[] textViews;
	private Fragment_Info infoFragment;
	private Fragment_Sell sellFragment;
	private Fragment_Try tryFragment;
	private Fragment_Report reportFragment;
	private int index,currentTabIndex;
	private TextView txt_title;
	private ImageView img_back,img_right;
	private T_Vip_Member member;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_memberinfo);
		initData();
		initTabView();
	}
	
	private void initData(){
		member = (T_Vip_Member)getIntent().getSerializableExtra("member");
	}
	
	private void initTabView() {
		infoFragment = new Fragment_Info(member);
		sellFragment = new Fragment_Sell(member);
		tryFragment = new Fragment_Try(member);
		reportFragment = new Fragment_Report(member);
		fragments = new Fragment[] {infoFragment, sellFragment,
				tryFragment, reportFragment };
		
		img_back = (ImageView)findViewById(R.id.img_back);
		img_right = (ImageView)findViewById(R.id.img_right);
		img_back.setVisibility(View.VISIBLE);
		img_right.setVisibility(View.VISIBLE);
		
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		textViews = new TextView[4];
		textViews[0] = (TextView) findViewById(R.id.tv_vip_info);
		textViews[1] = (TextView) findViewById(R.id.tv_vip_sell);
		textViews[2] = (TextView) findViewById(R.id.tv_vip_try);
		textViews[3] = (TextView) findViewById(R.id.tv_vip_report);
		// 添加显示第一个fragment
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment, infoFragment)
				.add(R.id.fragment, sellFragment)
				.add(R.id.fragment, tryFragment)
				.add(R.id.fragment, reportFragment)
				.hide(reportFragment).hide(tryFragment)
				.hide(sellFragment).show(infoFragment).commit();
		textViews[0].setOnClickListener(this);
		textViews[1].setOnClickListener(this);
		textViews[2].setOnClickListener(this);
		textViews[3].setOnClickListener(this);
		img_back.setOnClickListener(this);
		img_right.setOnClickListener(this);
		img_right.setImageResource(R.drawable.icon_edit);
	} 
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			ActivityUtil.finish(MemberInfoActivity.this);
			break;
		case R.id.img_right:
			Bundle bundle = new Bundle();
			bundle.putString(Constants.TITLE, "修改会员");
			bundle.putSerializable("member", member);
			ActivityUtil.start_ActivityForResult(MemberInfoActivity.this, MemberEditActivity.class, CommonParam.STATE_SAVE, bundle);
			break;
		default:
			onTabClicked(v);
			break;
		}
	}

	public void onTabClicked(View v) {
		switch (v.getId()) {
			case R.id.tv_vip_info:
				index = 0;
				img_right.setVisibility(View.VISIBLE);
				break;
			case R.id.tv_vip_sell:
				index = 1;
				img_right.setVisibility(View.GONE);
				break;
			case R.id.tv_vip_try:
				index = 2;
				img_right.setVisibility(View.GONE);
				break;
			case R.id.tv_vip_report:
				index = 3;
				img_right.setVisibility(View.GONE);
				break;
			default:
				break;
		}
		if (currentTabIndex != index) {
			FragmentTransaction trx = getSupportFragmentManager()
					.beginTransaction();
			trx.hide(fragments[currentTabIndex]);
			if (!fragments[index].isAdded()) {
				trx.add(R.id.fragment, fragments[index]);
			}
			trx.show(fragments[index]).commit();
		}
		textViews[currentTabIndex].setTextColor(Color.parseColor("#FF888271"));
		textViews[index].setTextColor(Color.parseColor("#FF3300FF"));
		currentTabIndex = index;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			if(requestCode == CommonParam.STATE_SAVE){
				T_Vip_Member member = (T_Vip_Member)data.getSerializableExtra("member");
				T_Vip_Member_Info memberInfo = (T_Vip_Member_Info)data.getSerializableExtra("memberInfo");
				infoFragment.setMember(member);
				infoFragment.setMemberInfo(memberInfo);
				infoFragment.initViewValue();
			}
		}
	}

}
