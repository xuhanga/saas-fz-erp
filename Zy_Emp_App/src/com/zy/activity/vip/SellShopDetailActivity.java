package com.zy.activity.vip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.sell.cash.T_Sell_ShopList;
import zy.util.CommonUtil;
import zy.util.StringUtil;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.adapter.vip.SellShopListAdapter;
import com.zy.api.sell.SellReportAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.util.SPUtil;

public class SellShopDetailActivity extends BaseActivity implements OnClickListener{

	private Handler handler;
	private ImageView img_back;
	private TextView txt_title;
	private TextView tv_sh_number,tv_sh_date,tv_shop_name,tv_em_name,tv_sh_amount,tv_sh_money;
	private ListView listView;
	private SellShopListAdapter sellShopListAdapter;
	private List<T_Sell_ShopList> sellShopLists = new ArrayList<T_Sell_ShopList>();
	
	private T_Sell_Shop sellShop;
	private SellReportAPI sellReportAPI;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_vip_sellshopdetail);
		initView();
		initData();
		initListener();
		initHandler();
		loadData();
	}
	
	private void initView(){
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		tv_sh_number = (TextView)findViewById(R.id.tv_sh_number);
		tv_sh_date = (TextView)findViewById(R.id.tv_sh_date);
		tv_shop_name = (TextView)findViewById(R.id.tv_shop_name);
		tv_em_name = (TextView)findViewById(R.id.tv_em_name);
		tv_sh_amount = (TextView)findViewById(R.id.tv_sh_amount);
		tv_sh_money = (TextView)findViewById(R.id.tv_sh_money);
		listView = (ListView)findViewById(R.id.lv_data);
	}
	
	private void initData(){
		progress = new ProgressDialog(this);
		sellReportAPI = new SellReportAPI(this);
		sellShop = (T_Sell_Shop)getIntent().getSerializableExtra("sellShop");
		sellShopListAdapter = new SellShopListAdapter(SellShopDetailActivity.this, sellShopLists);
		listView.setAdapter(sellShopListAdapter);
		tv_sh_number.setText(StringUtil.trimString(sellShop.getSh_number()));
		tv_sh_date.setText(StringUtil.trimString(sellShop.getSh_date()));
		tv_shop_name.setText(StringUtil.trimString(sellShop.getShop_name()));
		tv_em_name.setText(StringUtil.trimString(sellShop.getEm_name()));
		tv_sh_amount.setText(StringUtil.trimString(sellShop.getSh_amount()));
		tv_sh_money.setText(String.format("%.2f", sellShop.getSh_money()));
	}
	
	private void initListener(){
		img_back.setOnClickListener(this);
	}
	
	@SuppressLint("HandlerLeak")
	private void initHandler(){
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
					case STATE_LOAD:
						List<T_Sell_ShopList> result = (List<T_Sell_ShopList>)msg.obj;
						if(result != null && result.size()>0){
							sellShopLists.addAll(result);
						}
						sellShopListAdapter.notifyDataSetChanged();
						break;
					
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(SellShopDetailActivity.this, message);
						break;
					default:
						break;
				}
			}
		};
	}
	
	private void loadData(){
		progress.show();
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, SPUtil.getUser(this).getCompanyid());
		params.put("sh_number", sellShop.getSh_number());
		sellReportAPI.listShopList(params, handler, STATE_LOAD);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img_back:
			ActivityUtil.finish(SellShopDetailActivity.this);
			break;
		default:
			break;
		}
	}
	
	
}
