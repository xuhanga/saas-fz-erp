package com.zy.activity.vip.visit;

import java.util.HashMap;
import java.util.Map;

import zy.dto.base.emp.EmpLoginDto;
import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.util.DateUtil;
import zy.util.StringUtil;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.api.vip.VisitAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.view.dialog.DatePickerDialog;
import com.zy.view.dialog.popup.BottomPopup;

public class VisitAddTelActivity extends BaseActivity implements OnClickListener{
	private Handler handler;
	private TextView txt_title;
	private ImageView img_back;
	private TextView tv_vm_name,tv_vm_cardcode,tv_vm_mobile,tv_icon_tel,tv_vi_date_label,tv_vi_date,tv_vi_remark_label;
	private EditText et_vi_content,et_vi_remark;
	private LinearLayout ll_vi_date,ll_vi_remark;
	private Button btn_save;
	private T_Vip_Member member;
	private VisitAPI visitAPI;
	private T_Vip_Visit visit = new T_Vip_Visit();
	private DatePickerDialog viDateDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_vip_visitadd_tel);
		initData();
		initView();
		initListener();
		initHandle();
	}
	private void initData() {
		progress = new ProgressDialog(this);
		visitAPI = new VisitAPI(this);
		member = (T_Vip_Member)getIntent().getSerializableExtra("member");
	}
	private void initView(){
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(getIntent().getStringExtra(Constants.TITLE));
		img_back = (ImageView) findViewById(R.id.img_back);
		img_back.setVisibility(View.VISIBLE);
		tv_vm_name = (TextView) findViewById(R.id.tv_vm_name);
		tv_vm_cardcode = (TextView) findViewById(R.id.tv_vm_cardcode);
		tv_vm_mobile = (TextView) findViewById(R.id.tv_vm_mobile);
		tv_icon_tel = (TextView) findViewById(R.id.tv_icon_tel);
		tv_icon_tel.setTypeface(iconfont);
		tv_vi_date_label = (TextView) findViewById(R.id.tv_vi_date_label);
		tv_vi_date = (TextView) findViewById(R.id.tv_vi_date);
		tv_vi_remark_label = (TextView) findViewById(R.id.tv_vi_remark_label);
		et_vi_content = (EditText) findViewById(R.id.et_vi_content);
		et_vi_remark = (EditText) findViewById(R.id.et_vi_remark);
		ll_vi_date = (LinearLayout) findViewById(R.id.ll_vi_date);
		ll_vi_remark = (LinearLayout) findViewById(R.id.ll_vi_remark);
		btn_save = (Button) findViewById(R.id.btn_save);
		tv_vm_name.setText(StringUtil.trimString(member.getVm_name()));
		tv_vm_cardcode.setText(StringUtil.trimString(member.getVm_cardcode()));
		tv_vm_mobile.setText(StringUtil.trimString(member.getVm_mobile()));
	}
	
	private void initListener(){
		img_back.setOnClickListener(this);
		tv_icon_tel.setOnClickListener(this);
		btn_save.setOnClickListener(this);
	}
	
	@SuppressLint("HandlerLeak")
	private void initHandle(){
		handler = new Handler() {
			public void handleMessage(Message msg) {
				if(progress != null && progress.isShowing()){
					progress.dismiss();
				}
				switch (msg.what) {
					case R.id.btn_save:
						ActivityUtil.showShortToast(VisitAddTelActivity.this, "回访成功");
						setResult(RESULT_OK);
						ActivityUtil.finish(VisitAddTelActivity.this);
						break;
					case STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(VisitAddTelActivity.this, message);
						break;
				}
				super.handleMessage(msg);
			}
		};
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_icon_tel:
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + member.getVm_mobile()));
			startActivity(intent);
			break;
		case R.id.img_back:
			ActivityUtil.finish(VisitAddTelActivity.this);
			break;
		case R.id.btn_save:
			save();
			break;
		default:
			break;
		}
	}
	
	private void buildModel(){
		EmpLoginDto loginDto = getUser();
		visit.setCompanyid(loginDto.getCompanyid());
		visit.setVi_vm_code(member.getVm_code());
		visit.setVi_vm_name(member.getVm_name());
		visit.setVi_vm_mobile(member.getVm_mobile());
		visit.setVi_consume_date(member.getSh_sysdate());
		visit.setVi_consume_money(member.getSh_sell_money());
		visit.setVi_visit_date(DateUtil.getCurrentTime());
		visit.setVi_manager(loginDto.getEm_name());
		visit.setVi_manager_code(loginDto.getEm_code());
		visit.setVi_content(StringUtil.trimString(et_vi_content.getText()));
		visit.setVi_shop_code(loginDto.getEm_shop_code());
		visit.setVi_us_id(null);
		visit.setVi_sysdate(DateUtil.getCurrentTime());
		visit.setVi_type(getIntent().getIntExtra("vi_type", 0));
		visit.setVi_date(StringUtil.trimString(tv_vi_date.getText()));
		visit.setVi_remark(StringUtil.trimString(et_vi_remark.getText()));
		visit.setVi_way(1);//电话回访
	}
	
	private void save(){
		buildModel();
		if(StringUtil.isEmpty(visit.getVi_satisfaction())){
			ActivityUtil.showShortToast(VisitAddTelActivity.this, "请选择满意度");
			return;
		}
		if(StringUtil.isEmpty(visit.getVi_is_arrive())){
			ActivityUtil.showShortToast(VisitAddTelActivity.this, "请选择反馈信息");
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("visit", JSON.toJSONString(visit));
		progress.show();
		visitAPI.save(params, handler, R.id.btn_save);
	}
	

	public void onTextViewClick(View v){
		switch (v.getId()) {
		case R.id.tv_vi_satisfaction:
			final String[] satisfactionOptions = new String[]{"满意","一般","不满意"};//1满意，2一般 ，3不满意
			BottomPopup satisfactionPopup = new BottomPopup(VisitAddTelActivity.this, "满意度", satisfactionOptions);
			satisfactionPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vi_satisfaction)).setText(satisfactionOptions[position]);
					visit.setVi_satisfaction(position + 1);
				}
			});
			satisfactionPopup.showView();
			break;
		case R.id.tv_vi_is_arrive:
			final String[] arriveOptions = new String[]{"不确定到店","确认到店","确认不到店"};//1:不确定到店 2:确认到店 3:确认不到店
			BottomPopup arrivePopup = new BottomPopup(VisitAddTelActivity.this, "反馈信息", arriveOptions);
			arrivePopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
				@Override
				public void onItemClick(int position) {
					((TextView)findViewById(R.id.tv_vi_is_arrive)).setText(arriveOptions[position]);
					visit.setVi_is_arrive(position + 1);
					if(visit.getVi_is_arrive().intValue() == 1){
						ll_vi_date.setVisibility(View.VISIBLE);
						ll_vi_remark.setVisibility(View.VISIBLE);
						tv_vi_date_label.setText("下次联系时间：");
						tv_vi_remark_label.setText("联系计划内容：");
					}else if(visit.getVi_is_arrive().intValue() == 2){
						ll_vi_date.setVisibility(View.VISIBLE);
						ll_vi_remark.setVisibility(View.VISIBLE);
						tv_vi_date_label.setText("到店日期：");
						tv_vi_remark_label.setText("注意事项：");
					}else if(visit.getVi_is_arrive().intValue() == 3){
						ll_vi_date.setVisibility(View.GONE);
						ll_vi_remark.setVisibility(View.VISIBLE);
						tv_vi_remark_label.setText("反馈问题：");
					}
				}
			});
			arrivePopup.showView();
			break;
		case R.id.tv_vi_date:
			if(viDateDialog == null){
				viDateDialog = new DatePickerDialog(VisitAddTelActivity.this, null);
				viDateDialog.getOkButton().setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String date = viDateDialog.getDate();
						tv_vi_date.setText(date);
						visit.setVi_date(date);
						viDateDialog.dismiss();
					}
				});
			}
			viDateDialog.show();
			break;
		default:
			break;
		}
	}

}
