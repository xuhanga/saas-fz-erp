package com.zy.adapter.emp;

import java.util.ArrayList;
import java.util.List;

import zy.dto.base.emp.EmpPerformanceDto;
import zy.util.StringUtil;

import com.zy.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PerformanceAdapter extends BaseAdapter{
	private Context context;
	private List<EmpPerformanceDto> performanceDtos = new ArrayList<EmpPerformanceDto>();
	private LayoutInflater listContainer; 
	public PerformanceAdapter(Context context,List<EmpPerformanceDto> performanceDtos) {
		this.context = context;
		this.performanceDtos = performanceDtos;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return performanceDtos.size();
	}

	@Override
	public Object getItem(int position) {
		return performanceDtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_emp_performance_item,null);
			listItemView.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			listItemView.tv_value = (TextView) convertView.findViewById(R.id.tv_value);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (performanceDtos.size() > 0) {
			EmpPerformanceDto item = performanceDtos.get(position);
			listItemView.tv_name.setText(StringUtil.trimString(item.getPf_name()));
			listItemView.tv_value.setText(StringUtil.trimString(item.getPf_value()));
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_name, tv_value;
	}
}
