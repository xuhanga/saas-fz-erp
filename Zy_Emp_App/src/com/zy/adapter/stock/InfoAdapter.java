package com.zy.adapter.stock;

import java.util.ArrayList;
import java.util.List;

import zy.entity.stock.data.T_Stock_DataView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zy.R;

@SuppressLint("InflateParams")
public class InfoAdapter extends BaseAdapter{
	private Context context;
	private List<T_Stock_DataView> stocks = new ArrayList<T_Stock_DataView>();
	private LayoutInflater listContainer; 
	public InfoAdapter(Context context) {
		this.context = context;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return stocks.size();
	}

	@Override
	public Object getItem(int position) {
		return stocks.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView item = null;
		if (convertView == null) {
			item = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_stockinfo_list,null);
			item.tv_dp_name = (TextView) convertView.findViewById(R.id.tv_dp_name);
			item.tv_sd_amount = (TextView) convertView.findViewById(R.id.tv_sd_amount);
			convertView.setTag(item);
		} else {
			item = (ListItemView) convertView.getTag();
		}
		if (null != stocks && stocks.size() > 0) {
			T_Stock_DataView data = stocks.get(position);
			item.tv_dp_name.setText("仓库名称："+data.getDp_name());
			item.tv_sd_amount.setText("库存："+data.getSd_amount());
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_dp_name,tv_sd_amount;
	}
	public void setStocks(List<T_Stock_DataView> stocks) {
		this.stocks = stocks;
	}
	
}
