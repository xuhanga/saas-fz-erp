package com.zy.adapter.stock;

import java.util.ArrayList;
import java.util.List;

import zy.entity.stock.data.T_Stock_Data;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.BaseActivity;

public class StockAdapter extends BaseAdapter{
	private Context context;
	private List<T_Stock_Data> datas = new ArrayList<T_Stock_Data>();
	private LayoutInflater listContainer; 
	public StockAdapter(Context context,List<T_Stock_Data> data) {
		this.context = context;
		this.datas = data;
		listContainer = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return datas.size();
	}

	@Override
	public Object getItem(int position) {
		return datas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return datas.get(position).getSd_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItemView item = null;
		if (convertView == null) {
			item = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_stocklist,null);
			item.tv_pd_name = (TextView) convertView.findViewById(R.id.tv_pd_name);
			item.tv_pd_price = (TextView) convertView.findViewById(R.id.tv_pd_price);
			item.tv_sd_amount = (TextView) convertView.findViewById(R.id.tv_sd_amount);
			item.tv_icon_next = (TextView) convertView.findViewById(R.id.tv_icon_next);
			convertView.setTag(item);
		} else {
			item = (ListItemView) convertView.getTag();
		}
		if (null != datas && datas.size() > 0) {
			T_Stock_Data data = datas.get(position);
			item.tv_pd_name.setText("商品："+data.getPd_no()+"/"+data.getPd_name());
			item.tv_sd_amount.setText("库存数量："+data.getSd_amount());
			item.tv_pd_price.setText("价格："+data.getPd_price());
			item.tv_icon_next.setTypeface(((BaseActivity)context).iconfont);
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_pd_name, tv_pd_price,tv_sd_amount,tv_icon_next;
	}
}
