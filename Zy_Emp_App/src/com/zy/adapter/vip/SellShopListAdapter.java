package com.zy.adapter.vip;

import java.util.ArrayList;
import java.util.List;

import zy.entity.sell.cash.T_Sell_ShopList;
import zy.util.StringUtil;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.zy.R;
import com.zy.util.BitmapCache;
import com.zy.util.CommonParam;
import com.zy.util.ImageUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SellShopListAdapter extends BaseAdapter{
	private Context context;
	private List<T_Sell_ShopList> sellShopLists = new ArrayList<T_Sell_ShopList>();
	private LayoutInflater listContainer; 
	private ImageLoader imageLoader;
	
	
	public SellShopListAdapter(Context context,List<T_Sell_ShopList> sellShopLists) {
		this.context = context;
		this.sellShopLists = sellShopLists;
		listContainer = LayoutInflater.from(context);
		imageLoader = new ImageLoader(Volley.newRequestQueue(context), new BitmapCache());
	}
	
	@Override
	public int getCount() {
		return sellShopLists.size();
	}

	@Override
	public Object getItem(int position) {
		return sellShopLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return sellShopLists.get(position).getShl_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_vip_sellshopdetail_item,null);
			listItemView.iv_product_img = (ImageView) convertView.findViewById(R.id.iv_product_img);
			listItemView.tv_pd_no = (TextView) convertView.findViewById(R.id.tv_pd_no);
			listItemView.tv_pd_name = (TextView) convertView.findViewById(R.id.tv_pd_name);
			listItemView.tv_brand_type = (TextView) convertView.findViewById(R.id.tv_brand_type);
			listItemView.tv_product_info = (TextView) convertView.findViewById(R.id.tv_product_info);
			listItemView.tv_consume_info = (TextView) convertView.findViewById(R.id.tv_consume_info);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (null != sellShopLists && sellShopLists.size() > 0) {
			T_Sell_ShopList item = sellShopLists.get(position);
			listItemView.tv_pd_no.setText(StringUtil.trimString(item.getPd_no()));
			listItemView.tv_pd_name.setText(StringUtil.trimString(item.getPd_name()));
			listItemView.tv_brand_type.setText(StringUtil.trimString(item.getTp_name())+"/"+StringUtil.trimString(item.getBd_name()));
			StringBuffer productinfo = new StringBuffer();
			productinfo.append(StringUtil.trimString(item.getCr_name()));
			productinfo.append("/").append(StringUtil.trimString(item.getSz_name()));
			if(StringUtil.isNotEmpty(item.getBr_name())){
				productinfo.append("/").append(StringUtil.trimString(item.getBr_name()));
			}
			listItemView.tv_product_info.setText(productinfo.toString());
			listItemView.tv_consume_info.setText("件数："+item.getShl_amount()+"，金额：￥"+String.format("%.2f", item.getShl_money()));
			
			if(StringUtil.isNotEmpty(item.getPd_img_path())){
				loadBitmap(CommonParam.file_url+item.getPd_img_path(), listItemView.iv_product_img);
			}else {
				listItemView.iv_product_img.setImageResource(R.drawable.nophoto);
			}
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public ImageView iv_product_img;
		public TextView tv_pd_no, tv_pd_name,tv_brand_type,tv_product_info,tv_consume_info;
	}
	private void loadBitmap(String url,final ImageView imageView){
		imageLoader.get(url, new ImageListener() {
			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				Bitmap bitmap = response.getBitmap();
				if(bitmap != null){
					imageView.setImageBitmap(ImageUtil.getCircleBitmap(bitmap));
				}
			}
			@Override
			public void onErrorResponse(VolleyError arg0) {
				
			}
		});
	}
}
