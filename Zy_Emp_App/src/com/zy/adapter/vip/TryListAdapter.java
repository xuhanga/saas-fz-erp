package com.zy.adapter.vip;

import java.util.ArrayList;
import java.util.List;

import zy.entity.vip.trylist.T_Vip_TryList;
import zy.util.DateUtil;
import zy.util.StringUtil;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.zy.R;
import com.zy.util.BitmapCache;
import com.zy.util.CommonParam;
import com.zy.util.ImageUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TryListAdapter extends BaseAdapter{
	private Context context;
	private List<T_Vip_TryList> tryLists = new ArrayList<T_Vip_TryList>();
	private LayoutInflater listContainer; 
	private ImageLoader imageLoader;
	public TryListAdapter(Context context,List<T_Vip_TryList> tryLists) {
		this.context = context;
		this.tryLists = tryLists;
		listContainer = LayoutInflater.from(context);
		imageLoader = new ImageLoader(Volley.newRequestQueue(context), new BitmapCache());
	}
	
	@Override
	public int getCount() {
		return tryLists.size();
	}

	@Override
	public Object getItem(int position) {
		return tryLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return tryLists.get(position).getTr_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.fragment_try_item,null);
			listItemView.iv_product_img = (ImageView) convertView.findViewById(R.id.iv_product_img);
			listItemView.tv_pd_no = (TextView) convertView.findViewById(R.id.tv_pd_no);
			listItemView.tv_pd_name = (TextView) convertView.findViewById(R.id.tv_pd_name);
			listItemView.tv_brand_type = (TextView) convertView.findViewById(R.id.tv_brand_type);
			listItemView.tv_product_info = (TextView) convertView.findViewById(R.id.tv_product_info);
			listItemView.tv_sell_price = (TextView) convertView.findViewById(R.id.tv_sell_price);
			listItemView.tv_tr_date = (TextView) convertView.findViewById(R.id.tv_tr_date);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (null != tryLists && tryLists.size() > 0) {
			T_Vip_TryList item = tryLists.get(position);
			listItemView.tv_pd_no.setText(StringUtil.trimString(item.getPd_no()));
			listItemView.tv_pd_name.setText(StringUtil.trimString(item.getPd_name()));
			listItemView.tv_brand_type.setText(StringUtil.trimString(item.getTp_name())+"/"+StringUtil.trimString(item.getBd_name())+" "+StringUtil.trimString(item.getPd_style()));
			listItemView.tv_sell_price.setText("零售价：￥"+String.format("%.2f", item.getTr_sell_price()));
			listItemView.tv_tr_date.setText(DateUtil.formatDate(DateUtil.parseString2Date(item.getTr_sysdate()), DateUtil.FORMAT_YEAR_MON_DAY));
			StringBuffer productinfo = new StringBuffer();
			productinfo.append(StringUtil.trimString(item.getCr_name()));
			productinfo.append("/").append(StringUtil.trimString(item.getSz_name()));
			if(StringUtil.isNotEmpty(item.getBr_name())){
				productinfo.append("/").append(StringUtil.trimString(item.getBr_name()));
			}
			listItemView.tv_product_info.setText(productinfo.toString());
			if(StringUtil.isNotEmpty(item.getPd_img_path())){
				loadBitmap(CommonParam.file_url+item.getPd_img_path(), listItemView.iv_product_img);
			}else {
				listItemView.iv_product_img.setImageResource(R.drawable.nophoto);
			}
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public ImageView iv_product_img;
		public TextView tv_pd_no, tv_pd_name,tv_brand_type,tv_product_info,tv_sell_price,tv_tr_date;
	}
	private void loadBitmap(String url,final ImageView imageView){
		imageLoader.get(url, new ImageListener() {
			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				Bitmap bitmap = response.getBitmap();
				if(bitmap != null){
					imageView.setImageBitmap(ImageUtil.getCircleBitmap(bitmap));
				}
			}
			@Override
			public void onErrorResponse(VolleyError arg0) {
				
			}
		});
	}
}
