package com.zy.adapter.vip.visit;

import java.util.ArrayList;
import java.util.List;

import zy.entity.vip.member.T_Vip_Member;
import zy.util.DateUtil;
import zy.util.StringUtil;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.activity.vip.visit.VisitWayActivity;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BirthdayVisitAdapter extends BaseAdapter{
	private Context context;
	private List<T_Vip_Member> members = new ArrayList<T_Vip_Member>();
	private LayoutInflater listContainer; 
	public BirthdayVisitAdapter(Context context,List<T_Vip_Member> members) {
		this.context = context;
		this.members = members;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return members.size();
	}

	@Override
	public Object getItem(int position) {
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		return members.get(position).getVm_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_vip_birthdayvisit_item,null);
			listItemView.tv_vm_name = (TextView) convertView.findViewById(R.id.tv_vm_name);
			listItemView.tv_vm_birthday = (TextView) convertView.findViewById(R.id.tv_vm_birthday);
			listItemView.tv_consume_info = (TextView) convertView.findViewById(R.id.tv_consume_info);
			listItemView.tv_icon_next = (TextView) convertView.findViewById(R.id.tv_icon_next);
			listItemView.tv_visit = (TextView) convertView.findViewById(R.id.tv_visit);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (null != members && members.size() > 0) {
			final T_Vip_Member member = members.get(position);
			listItemView.tv_vm_name.setText(StringUtil.trimString(member.getVm_name())+"("+StringUtil.trimString(member.getVm_mobile())+")");
			if(member.getVm_birthday_type() == null){
				listItemView.tv_vm_birthday.setText("");
			}else if(member.getVm_birthday_type().intValue() == 0){
				listItemView.tv_vm_birthday.setText("生日：公历  "+StringUtil.trimString(member.getVm_birthday()));
			}else if(member.getVm_birthday_type().intValue() == 1){
				listItemView.tv_vm_birthday.setText("生日：农历  "+StringUtil.trimString(member.getVm_lunar_birth()));
			}
			StringBuffer info = new StringBuffer();
			info.append("消费"+StringUtil.trimString(member.getVm_times())+"次，");
			info.append("￥:"+StringUtil.trimString(member.getVm_total_money())+"元，");
			if(StringUtil.isNotEmpty(member.getVm_lastbuy_date())){
				info.append(DateUtil.getDaysBetween(member.getVm_lastbuy_date(), DateUtil.getYearMonthDate())).append("天未消费");
			}else {
				info.append(DateUtil.getDaysBetween(member.getVm_date(), DateUtil.getYearMonthDate())).append("天未消费");
			}
			listItemView.tv_consume_info.setText(info.toString());
			listItemView.tv_icon_next.setTypeface(((BaseActivity)context).iconfont);
			listItemView.tv_visit.setTypeface(((BaseActivity)context).iconfont);
			listItemView.tv_visit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Bundle bundle = new Bundle();
					bundle.putString(Constants.TITLE, "回访方式");
					bundle.putInt("vi_type", 2);
					bundle.putSerializable("member", member);
					ActivityUtil.start_ActivityForResult((BaseActivity)context, VisitWayActivity.class,CommonParam.STATE_LOAD, bundle);
				}
			});
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_vm_name,tv_vm_birthday,tv_consume_info,tv_icon_next,tv_visit;
	}
}
