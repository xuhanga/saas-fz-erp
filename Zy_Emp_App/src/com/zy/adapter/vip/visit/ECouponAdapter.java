package com.zy.adapter.vip.visit;

import java.util.ArrayList;
import java.util.List;

import zy.dto.sell.ecoupon.SellECouponDto;
import zy.util.StringUtil;

import com.zy.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ECouponAdapter extends BaseAdapter{
	private Context context;
	private List<SellECouponDto> eCouponDtos = new ArrayList<SellECouponDto>();
	private LayoutInflater listContainer; 
	private int[] bg_color = new int[]{R.drawable.ecoupon_yellow,R.drawable.ecoupon_green,R.drawable.ecoupon_blue};
	public ECouponAdapter(Context context,List<SellECouponDto> eCouponDtos) {
		this.context = context;
		this.eCouponDtos = eCouponDtos;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return eCouponDtos.size();
	}

	@Override
	public Object getItem(int position) {
		return eCouponDtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return eCouponDtos.get(position).getEcl_id();
	}
	
	public int getSelectedIndex() {
		for (int i = 0; i < eCouponDtos.size(); i++) {
			if (eCouponDtos.get(i).isSelected()) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_vip_visitadd_ecoupon_item,null);
			listItemView.rl_top = (RelativeLayout) convertView.findViewById(R.id.rl_top);
			listItemView.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
			listItemView.tv_ec_name = (TextView) convertView.findViewById(R.id.tv_ec_name);
			listItemView.tv_limit_money = (TextView) convertView.findViewById(R.id.tv_limit_money);
			listItemView.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
			listItemView.tv_left_amount = (TextView) convertView.findViewById(R.id.tv_left_amount);
			listItemView.tv_selected = (TextView) convertView.findViewById(R.id.tv_selected);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (null != eCouponDtos && eCouponDtos.size() > 0) {
			listItemView.rl_top.setBackgroundResource(bg_color[position % bg_color.length]);
			SellECouponDto eCouponDto = eCouponDtos.get(position);
			listItemView.tv_money.setText("￥"+String.format("%.0f", eCouponDto.getEcl_money()));
			listItemView.tv_ec_name.setText(StringUtil.trimString(eCouponDto.getEc_name()));
			listItemView.tv_limit_money.setText("满"+String.format("%.0f", eCouponDto.getEcl_limitmoney())+"元使用");
			listItemView.tv_date.setText("有效期："+StringUtil.trimString(eCouponDto.getEc_begindate())+"~"+StringUtil.trimString(eCouponDto.getEc_enddate()));
			listItemView.tv_left_amount.setText("剩余："+(eCouponDto.getEcl_amount()-eCouponDto.getReceive_amount()));
			if(eCouponDto.isSelected()){
				listItemView.tv_selected.setVisibility(View.VISIBLE);
			}else {
				listItemView.tv_selected.setVisibility(View.GONE);
			}
			convertView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					for (int i = 0; i < eCouponDtos.size(); i++) {
						eCouponDtos.get(i).setSelected(false);
					}
					eCouponDtos.get(position).setSelected(true);
					notifyDataSetChanged();
				}
			});
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public RelativeLayout rl_top;
		public TextView tv_money, tv_ec_name,tv_limit_money,tv_date,tv_left_amount,tv_selected;
	}
}
