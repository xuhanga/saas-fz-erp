package com.zy.adapter.vip.visit;

import java.util.ArrayList;
import java.util.List;

import zy.entity.vip.member.T_Vip_Member;
import zy.util.StringUtil;

import com.zy.R;
import com.zy.activity.BaseActivity;
import com.zy.activity.vip.visit.VisitWayActivity;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SellVisitAdapter extends BaseAdapter{
	private Context context;
	private List<T_Vip_Member> members = new ArrayList<T_Vip_Member>();
	private LayoutInflater listContainer; 
	public SellVisitAdapter(Context context,List<T_Vip_Member> members) {
		this.context = context;
		this.members = members;
		listContainer = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return members.size();
	}

	@Override
	public Object getItem(int position) {
		return members.get(position);
	}

	@Override
	public long getItemId(int position) {
		return members.get(position).getVm_id();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// 自定义视图
		ListItemView listItemView = null;
		if (convertView == null) {
			listItemView = new ListItemView();
			convertView = listContainer.inflate(R.layout.layout_vip_sellvisit_item,null);
			listItemView.tv_vm_name = (TextView) convertView.findViewById(R.id.tv_vm_name);
			listItemView.tv_shop_name = (TextView) convertView.findViewById(R.id.tv_shop_name);
			listItemView.tv_sh_date = (TextView) convertView.findViewById(R.id.tv_sh_date);
			listItemView.tv_consume_info = (TextView) convertView.findViewById(R.id.tv_consume_info);
			listItemView.tv_icon_next = (TextView) convertView.findViewById(R.id.tv_icon_next);
			listItemView.tv_visit = (TextView) convertView.findViewById(R.id.tv_visit);
			convertView.setTag(listItemView);
		} else {
			listItemView = (ListItemView) convertView.getTag();
		}
		if (null != members && members.size() > 0) {
			final T_Vip_Member member = members.get(position);
			listItemView.tv_vm_name.setText(StringUtil.trimString(member.getVm_name())+"("+StringUtil.trimString(member.getVm_mobile())+")");
			listItemView.tv_shop_name.setText("销售店铺："+StringUtil.trimString(member.getShop_name()));
			listItemView.tv_sh_date.setText("销售日期："+StringUtil.trimString(member.getSh_sysdate()));
			listItemView.tv_consume_info.setText("消费金额："+String.format("%.2f", member.getSh_sell_money())+"元");
			listItemView.tv_icon_next.setTypeface(((BaseActivity)context).iconfont);
			listItemView.tv_visit.setTypeface(((BaseActivity)context).iconfont);
			
			listItemView.tv_visit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Bundle bundle = new Bundle();
					bundle.putString(Constants.TITLE, "回访方式");
					bundle.putInt("vi_type", 3);
					bundle.putSerializable("member", member);
					ActivityUtil.start_ActivityForResult((BaseActivity)context, VisitWayActivity.class,CommonParam.STATE_LOAD, bundle);
				}
			});
		}
		return convertView;
	}
	public final class ListItemView { // 自定义控件集合
		public TextView tv_vm_name,tv_shop_name,tv_sh_date,tv_consume_info,tv_icon_next,tv_visit;
	}
}
