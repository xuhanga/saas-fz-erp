package com.zy.adapter.vip.visit;

import com.zy.R;
import com.zy.view.ViewHolder;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class VisitWayGridAdapter extends BaseAdapter{
	private Context mContext;
	private Typeface iconfont;
	public String[] img_text = {
			"电话","微信","推券","短信"
			};
	public int[] imgs = { 
			R.string.icon_font_tel,R.string.icon_font_wechat, R.string.icon_font_voucher, R.string.icon_font_sms
	};
	public int[] colors = {
			R.color.icon_orange,R.color.icon_green,R.color.icon_red,R.color.icon_blue
	};
	public VisitWayGridAdapter(Context mContext) {
		super();
		this.mContext = mContext;
		iconfont = Typeface.createFromAsset(mContext.getAssets(), "iconfont/iconfont.ttf");
	}

	@Override
	public int getCount() {
		return img_text.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.home_grid_item, parent, false);
			TextView tv = ViewHolder.get(convertView, R.id.tv_item);
			TextView iv = ViewHolder.get(convertView, R.id.iv_item);
			iv.setTypeface(iconfont);
			iv.setText(imgs[position]);
			iv.setTextColor(mContext.getResources().getColor(colors[position]));
			tv.setText(img_text[position]);
		}
		return convertView;
	}
}
