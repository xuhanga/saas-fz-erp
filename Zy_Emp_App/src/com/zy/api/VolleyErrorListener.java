package com.zy.api;

import android.os.Handler;
import android.os.Message;

import com.android.volley.Response.ErrorListener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.zy.util.CommonParam;

public class VolleyErrorListener implements ErrorListener{
private Handler handler;
	
	public VolleyErrorListener(Handler handler){
		this.handler = handler;
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		Message message = Message.obtain();
		message.what = CommonParam.STATE_ERROR;
		if(error instanceof TimeoutError){
			message.obj = "���ӳ�ʱ";
		}else {
			message.obj = error.toString();
		}
		handler.sendMessage(message);
	}
}
