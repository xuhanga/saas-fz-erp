package com.zy.api.base;

import java.util.Map;

import zy.dto.base.product.ProductCartDto;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class ProductAPI extends BaseAPI{
	
	public ProductAPI(Context context){
		super(context);
	}
	
	public void list(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/base/product/list", ProductCartDto.class, handler, SUCC_STATE);
	}
	
	public void loadByBarcode(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		load(params, CommonParam.http_url+"api/base/product/loadByBarcode", ProductCartDto.class, handler, SUCC_STATE);
	}
}
