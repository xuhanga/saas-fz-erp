package com.zy.api.sell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import zy.entity.sell.cart.T_Sell_CartList;
import zy.entity.vip.member.T_Vip_Member;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zy.api.BaseAPI;
import com.zy.api.VolleyErrorListener;
import com.zy.util.CommonParam;

public class CartAPI extends BaseAPI {
	
	public CartAPI(Context context) {
		super(context);
	}
	
	public void loadCart(Map<String, Object> params,final Handler handler,final int SUCC_STATE){
		String url = CommonParam.http_url+"api/sell/cart/loadCart";
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(params);
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
								T_Vip_Member member = JSON.parseObject(resultJson.getJSONObject("data").getString("member"),T_Vip_Member.class);
								List<T_Sell_CartList> cartLists = JSON.parseArray(resultJson.getJSONObject("data").getString("cartLists"), T_Sell_CartList.class);
								Map<String, Object> resultMap = new HashMap<String,Object>();
								resultMap.put("member", member);
								resultMap.put("cartLists", cartLists);
								message.obj = resultMap;
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						}catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "���ݼ��ش���!";
						}finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	public void listCartList(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		list(params, CommonParam.http_url + "api/sell/cart/listCartList", T_Sell_CartList.class, handler, SUCC_STATE);
	}
	
	public void updateAmount(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/sell/cart/updateAmount", handler, SUCC_STATE);
	}
	
	public void saveCartList(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		save(params, CommonParam.http_url + "api/sell/cart/saveCartList", handler, SUCC_STATE);
	}
	
	public void delCartList(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/sell/cart/delCartList", handler, SUCC_STATE);
	}
	
	public void clearCartList(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/sell/cart/clearCartList", handler, SUCC_STATE);
	}
	
	public void updateCartListVip(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/sell/cart/updateCartListVip", handler, SUCC_STATE);
	}
	
	public void saveCart(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		save(params, CommonParam.http_url + "api/sell/cart/saveCart", handler, SUCC_STATE);
	}
	
}
