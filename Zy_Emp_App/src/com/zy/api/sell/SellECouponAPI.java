package com.zy.api.sell;

import java.util.Map;

import zy.dto.sell.ecoupon.SellECouponDto;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class SellECouponAPI extends BaseAPI{

	public SellECouponAPI(Context context){
		super(context);
	}
	
	public void listEcoupon4Push(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/sell/ecoupon/listEcoupon4Push", SellECouponDto.class, handler, SUCC_STATE);
	}
}
