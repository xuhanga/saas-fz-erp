package com.zy.api.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import zy.entity.base.bra.T_Base_Bra;
import zy.entity.base.color.T_Base_Color;
import zy.entity.base.size.T_Base_Size;
import zy.entity.stock.data.T_Stock_Data;
import zy.entity.stock.data.T_Stock_DataView;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zy.api.BaseAPI;
import com.zy.api.VolleyErrorListener;
import com.zy.util.CommonParam;

public class StockAPI extends BaseAPI{
	public StockAPI(Context context){
		super(context);
	}
	
	public void list(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/stock/list", T_Stock_Data.class, handler, SUCC_STATE);
	}
	
	public void loadByBarcode(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		load(params, CommonParam.http_url+"api/stock/loadByBarcode", T_Stock_Data.class, handler, SUCC_STATE);
	}
	
	public void otherStock(Map<String,Object> param,final Handler handler,final int state){
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(param);
		String url = CommonParam.http_url+"api/stock/other";
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = state;
								message.obj = JSON.parseArray(resultJson.getString("data"),T_Stock_DataView.class);
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "查询数据错误!";
						} finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	public void queryStock(Map<String,Object> param,final Handler handler,final int state){
		final Message message = Message.obtain();
		JSONObject jsonObject = new JSONObject(param);
		String url = CommonParam.http_url+"api/stock/query";
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST,url,
				jsonObject, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = state;
								message.obj = buildMap(resultJson.getString("data"));
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						} catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "查询数据错误!";
						} finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	@SuppressWarnings("static-access")
	private Map<String,Object> buildMap(String data){
		com.alibaba.fastjson.JSONObject obj = JSON.parseObject(data);
		Map<String,Object> map = new HashMap<String, Object>();
		if(null != obj){
			if(obj.containsKey("stock")){
				List<T_Stock_DataView> stocks = obj.parseArray(obj.getString("stock"), T_Stock_DataView.class);
				if(null != stocks && stocks.size() > 0){
					T_Stock_DataView stock = new T_Stock_DataView();
					stock.setPd_no(stocks.get(0).getPd_no());
					stock.setPd_name(stocks.get(0).getPd_name());
					stock.setSd_dp_code(stocks.get(0).getSd_dp_code());
					int amount = 0;
					for(T_Stock_DataView item:stocks){
						amount += item.getSd_amount();
					}
					stock.setPd_sell_price(stocks.get(0).getPd_sell_price());
					stock.setSd_amount(amount);
					map.put("stock", stock);
				}
				map.put("stocks", stocks);
			}
			if(obj.containsKey("size")){
				List<T_Base_Size> sizes = obj.parseArray(obj.getString("size"), T_Base_Size.class);
				map.put("size", sizes);
			}
			if(obj.containsKey("bra")){
				List<T_Base_Bra> bras = obj.parseArray(obj.getString("bra"), T_Base_Bra.class);
				map.put("bra", bras);
			}
			if(obj.containsKey("color")){
				List<T_Base_Color> bras = obj.parseArray(obj.getString("color"), T_Base_Color.class);
				map.put("color", bras);
			}
		}
		return map;
	}
}
