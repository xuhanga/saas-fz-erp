package com.zy.api.vip;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.zy.api.BaseAPI;
import com.zy.api.VolleyErrorListener;
import com.zy.util.CommonParam;
import com.zy.util.MultipartRequest;

public class MemberAPI extends BaseAPI{
	
	public MemberAPI(Context context){
		super(context);
	}
	
	public void list(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/vip/member/list", T_Vip_Member.class, handler, SUCC_STATE);
	}
	
	public void load(Integer vm_id,final Handler handler,final int SUCC_STATE){
		String url = CommonParam.http_url+"api/vip/member/load/"+vm_id;
		final Message message = Message.obtain();
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.GET,url,
				null, new Response.Listener<JSONObject>(){
					@Override
					public void onResponse(JSONObject resultJson) {
						try {
							int stat = resultJson.getInt("stat");
							if(stat == 200){
								message.what = SUCC_STATE;
								T_Vip_Member member = JSON.parseObject(resultJson.getJSONObject("data").getString("member"),T_Vip_Member.class);
								T_Vip_Member_Info memberInfo = JSON.parseObject(resultJson.getJSONObject("data").getString("memberInfo"),T_Vip_Member_Info.class);
								Map<String, Object> resultMap = new HashMap<String,Object>();
								resultMap.put("member", member);
								resultMap.put("memberInfo", memberInfo);
								message.obj = resultMap;
							}else {
								message.what = CommonParam.STATE_ERROR;
								message.obj = resultJson.getString("message");
							}
						}catch (JSONException e) {
							message.what = CommonParam.STATE_ERROR;
							message.obj = "数据加载错误!";
						}finally{
							handler.sendMessage(message);
						}
					}
				}, new VolleyErrorListener(handler));
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(jsonObjectRequest);
	}
	
	public void save(Map<String, Object> params,File imgFile, final Handler handler, final int SUCC_STATE){
		String url = CommonParam.http_url+"api/vip/member/save";
		final Message message = Message.obtain();
		MultipartRequest request = new MultipartRequest(url, new Response.Listener<String>() {
			@Override
			public void onResponse(String result) {
				try {
					JSONObject resultJson =new JSONObject(result);
					int stat = resultJson.getInt("stat");
					if(stat == 200){
						message.what = SUCC_STATE;
						message.obj = JSON.parseObject(resultJson.getString("data"),T_Vip_Member.class);
					}else {
						message.what = CommonParam.STATE_ERROR;
						message.obj = resultJson.getString("message");
					}
				} catch (JSONException e) {
					message.what = CommonParam.STATE_ERROR;
					message.obj = "保存数据错误!";
				} finally{
					handler.sendMessage(message);
				}
			}
		}, new VolleyErrorListener(handler));
		request.addStringPart4Object(params);
		request.addFilePart("img", imgFile);
		request.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(request);
	}
	
	public void update(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		update(params, CommonParam.http_url + "api/vip/member/update", handler, SUCC_STATE);
	}
	
	public void updateImg(Map<String, Object> params,File img,final Handler handler,final int SUCC_STATE){
		String url = CommonParam.http_url+"api/vip/member/updateImg";
		final Message message = Message.obtain();
		MultipartRequest request = new MultipartRequest(url, new Response.Listener<String>() {
			@Override
			public void onResponse(String result) {
				try {
					JSONObject resultJson =new JSONObject(result);
					int stat = resultJson.getInt("stat");
					if(stat == 200){
						message.what = SUCC_STATE;
					}else {
						message.what = CommonParam.STATE_ERROR;
						message.obj = resultJson.getString("message");
					}
				} catch (JSONException e) {
					message.what = CommonParam.STATE_ERROR;
					message.obj = "保存数据错误!";
				} finally{
					handler.sendMessage(message);
				}
			}
		}, new VolleyErrorListener(handler));
		request.addStringPart4Object(params);
		request.addFilePart("img", img);
		request.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		Volley.newRequestQueue(context).add(request);
	}
	
	public void search(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		load(params, CommonParam.http_url+"api/vip/member/search", T_Vip_Member.class, handler, SUCC_STATE);
	}
	
}
