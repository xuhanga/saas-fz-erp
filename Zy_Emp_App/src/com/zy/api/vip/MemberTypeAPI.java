package com.zy.api.vip;

import java.util.Map;

import zy.entity.vip.membertype.T_Vip_MemberType;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class MemberTypeAPI extends BaseAPI{
	
	public MemberTypeAPI(Context context){
		super(context);
	}
	
	public void list(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/vip/membertype/list", T_Vip_MemberType.class, handler, SUCC_STATE);
	}
}
