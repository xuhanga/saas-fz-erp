package com.zy.api.vip;

import java.util.Map;

import zy.entity.vip.trylist.T_Vip_TryList;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class TryListAPI extends BaseAPI{
	
	public TryListAPI(Context context) {
		super(context);
	}
	
	public void list(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/vip/trylist/list", T_Vip_TryList.class, handler, SUCC_STATE);
	}
	
	public void saveTryList(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		save(params, CommonParam.http_url + "api/vip/trylist/save", handler, SUCC_STATE);
	}
	
}
