package com.zy.api.vip;

import java.util.Map;

import zy.entity.vip.member.T_Vip_Member;
import android.content.Context;
import android.os.Handler;

import com.zy.api.BaseAPI;
import com.zy.util.CommonParam;

public class VisitAPI extends BaseAPI{
	
	public VisitAPI(Context context){
		super(context);
	}
	
	public void listSellVisit(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/vip/visit/listSellVisit", T_Vip_Member.class, handler, SUCC_STATE);
	}
	
	public void listBirthdayVisit(Map<String, Object> params, final Handler handler, final int SUCC_STATE) {
		list(params, CommonParam.http_url+"api/vip/visit/listBirthdayVisit", T_Vip_Member.class, handler, SUCC_STATE);
	}
	
	public void save(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		save(params, CommonParam.http_url + "api/vip/visit/save", handler, SUCC_STATE);
	}
	
	public void save_visit_ecoupon(Map<String, Object> params, final Handler handler, final int SUCC_STATE){
		save(params, CommonParam.http_url + "api/vip/visit/save_visit_ecoupon", handler, SUCC_STATE);
	}
	
}
