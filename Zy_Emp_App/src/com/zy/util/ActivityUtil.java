package com.zy.util;

import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.zy.R;

public class ActivityUtil {
	public static void showLongToast(Context context, String pMsg) {
		Toast.makeText(context, pMsg, Toast.LENGTH_LONG).show();
	}

	public static void showShortToast(Context context, String pMsg) {
		Toast.makeText(context, pMsg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 关闭 Activity
	 * 
	 * @param activity
	 */
	public static void finish(Activity activity) {
		activity.finish();
		activity.overridePendingTransition(R.anim.push_right_in,
				R.anim.push_right_out);
	}

	/**
	 * 打开Activity
	 * 
	 * @param activity
	 * @param cls
	 * @param name
	 */
	public static void start_Activity(Activity activity, Class<?> cls,
			BasicNameValuePair... name) {
		Intent intent = new Intent();
		intent.setClass(activity, cls);
		if (name != null)
			for (int i = 0; i < name.length; i++) {
				intent.putExtra(name[i].getName(), name[i].getValue());
			}
		activity.startActivity(intent);
		activity.overridePendingTransition(R.anim.push_left_in,
				R.anim.push_left_out);

	}
	/**
	 * 打开Activity
	 * 
	 * @param activity
	 * @param cls
	 * @param param 参数集合
	 */
	public static void startActivity(Activity activity, Class<?> cls,
			Map<String,String> param) {
		Intent intent = new Intent();
		intent.setClass(activity, cls);
		if (param != null && param.size() > 0){
			for (String key:param.keySet()) {
				intent.putExtra(key, param.get(key));
			}
		}
		activity.startActivity(intent);
		activity.overridePendingTransition(R.anim.push_left_in,
				R.anim.push_left_out);
	}
	/**
	 * 打开Activity
	 * 
	 * @param activity
	 * @param cls
	 * @param bundle
	 */
	public static void start_Activity(Activity activity, Class<?> cls,
			Bundle bundle) {
		Intent intent = new Intent();
		intent.setClass(activity, cls);
		intent.putExtras(bundle);
		activity.startActivity(intent);
		activity.overridePendingTransition(R.anim.push_left_in,
				R.anim.push_left_out);

	}
	
	/**
	 * 打开Activity
	 * @param activity
	 * @param cls
	 * @param requestCode
	 * @param name
	 */
	public static void start_ActivityForResult(Activity activity, Class<?> cls,int requestCode,
			BasicNameValuePair... name) {
		Intent intent = new Intent();
		intent.setClass(activity, cls);
		if (name != null)
			for (int i = 0; i < name.length; i++) {
				intent.putExtra(name[i].getName(), name[i].getValue());
			}
		activity.startActivityForResult(intent, requestCode);
		activity.overridePendingTransition(R.anim.push_left_in,
				R.anim.push_left_out);

	}
	/**
	 * 打开Activity
	 * @param activity
	 * @param cls
	 * @param requestCode
	 * @param bundle
	 */
	public static void start_ActivityForResult(Activity activity, Class<?> cls,int requestCode,
			Bundle bundle) {
		Intent intent = new Intent();
		intent.setClass(activity, cls);
		intent.putExtras(bundle);
		activity.startActivityForResult(intent, requestCode);
		activity.overridePendingTransition(R.anim.push_left_in,
				R.anim.push_left_out);

	}
	
	/**
	 * 判断是否有网络
	 */
	public static boolean isNetworkAvailable(Context context) {
		if (context.checkCallingOrSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
			return false;
		} else {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			if (connectivity == null) {
				Log.w("Utility", "couldn't get connectivity manager");
			} else {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].isAvailable()) {
							Log.d("Utility", "network is available");
							return true;
						}
					}
				}
			}
		}
		Log.d("Utility", "network is not available");
		return false;
	}

	/**
	 * 发送文字通知
	 * 
	 * @param context
	 * @param Msg
	 * @param Title
	 * @param content
	 * @param i
	 */
	@SuppressWarnings("deprecation")
	public static void sendText(Context context, String Msg, String Title,
			String content, Intent i) {
		NotificationManager mn = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.icon_app,
				Msg, System.currentTimeMillis());
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, i,
				PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setLatestEventInfo(context, Title, content, contentIntent);
		mn.notify(0, notification);
	}
	/**
	 * 获取版本号
	 * 
	 * @return 当前应用的版本号
	 */
	public static String getVersion(Context context) {
		try {
			PackageManager manager = context.getPackageManager();
			PackageInfo info = manager.getPackageInfo(context.getPackageName(),
					0);
			String version = info.versionName;
			return version;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
