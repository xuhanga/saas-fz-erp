package com.zy.util;

import java.util.List;

import zy.entity.vip.member.T_Vip_Member;

public class CacheVipMemberUtil {
	
	private static final String fileName = "vipmember.json";
	private static final String fileName_state = "state.json";
	private static final String fileName_try = "try.json";
	private static final String buffPath = "receive";
	public static synchronized T_Vip_Member getMember() {
		BufferStore<T_Vip_Member> buffStore = new BufferStore<T_Vip_Member>(buffPath,fileName);
		List<T_Vip_Member> list = buffStore.read();
		if(list != null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}

	public static synchronized void putMember(T_Vip_Member member) {
		BufferStore<T_Vip_Member> buffStore = new BufferStore<T_Vip_Member>(buffPath,fileName);
		List<T_Vip_Member> list = buffStore.read();
		list.add(member);
		buffStore.write(list);
	}

	public static synchronized void clear() {
		BufferStore<T_Vip_Member> buffStore = new BufferStore<T_Vip_Member>(buffPath,fileName);
		buffStore.clear();
	}
	
	public static synchronized void putReceiveState(String receiveState){
		BufferStore<String> buffStore = new BufferStore<String>(buffPath,fileName_state);
		List<String> list = buffStore.read();
		list.add(receiveState);
		buffStore.write(list);
	}
	
	public static synchronized String getReceiveState(){
		BufferStore<String> buffStore = new BufferStore<String>(buffPath,fileName_state);
		List<String> list = buffStore.read();
		if(list != null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
	public static synchronized void putTryState(String tryState){
		BufferStore<String> buffStore = new BufferStore<String>(buffPath,fileName_try);
		List<String> list = buffStore.read();
		list.add(tryState);
		buffStore.write(list);
	}
	
	public static synchronized String getTryState(){
		BufferStore<String> buffStore = new BufferStore<String>(buffPath,fileName_try);
		List<String> list = buffStore.read();
		if(list != null&&list.size()>0){
			return list.get(0);
		}
		return null;
	}
	
}
