package com.zy.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class FileUtil {
	private static int width = 180;
	private static int height = 300;
	
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// 源图片的高度和宽度
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {
			// 计算出实际宽高和目标宽高的比率
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			// 选择宽和高中最小的比率作为inSampleSize的值，这样可以保证最终图片的宽和高
			// 一定都会大于等于目标的宽和高。
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}
	/**
	 * 加载图片，进行压缩,并替换原文件
	 * */
	public static Bitmap loadAndCompressBitmap(String path) {
		Bitmap bitmap = loadBitmap(path);
		compressBitmap(bitmap, path);
		return bitmap;
	}
	public static Bitmap loadAndCompressBitmap(String srcPath,String descPath) {
		Bitmap bitmap = loadBitmap(srcPath);
		compressBitmap(bitmap, descPath);
		return bitmap;
	}
	/**
	 * 加载图片，不进行压缩
	 * */
	public static Bitmap loadBitmap(String path) {
		// 第一次解析将inJustDecodeBounds设置为true，来获取图片大小
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		// 调用上面定义的方法计算inSampleSize值
		options.inSampleSize = calculateInSampleSize(options, width, height);
		// 使用获取到的inSampleSize值再次解析图片
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeFile(path, options);
		return bitmap;
	}
	public static Bitmap loadBitmap(String path,int _width,int _heiht) {
		// 第一次解析将inJustDecodeBounds设置为true，来获取图片大小
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		// 调用上面定义的方法计算inSampleSize值
		options.inSampleSize = calculateInSampleSize(options, _width, _heiht);
		// 使用获取到的inSampleSize值再次解析图片
		options.inJustDecodeBounds = false;
		Bitmap bitmap = BitmapFactory.decodeFile(path, options);
		return bitmap;
	}
	public static Bitmap loadAndCompressBitmap(String path,int _width,int _heiht) {
		Bitmap bitmap = loadBitmap(path);
		compressBitmap(bitmap, path);
		return bitmap;
	}
	/**
	 * 处理完图片之后重新保存
	 * @param bitmap
	 */
	public static void compressBitmap(Bitmap bitmap,String tempImgPath){
		//将要保存图片的路径
		File file=new File(tempImgPath);
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(file));
			bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
			bos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if(null != bos)	bos.close();
			} catch (IOException e) {
			}
		}
	}
	
	public static boolean existFile(String path){
		File files = new File(path);
		if (!files.exists()) { // 当没有目录时 创建目录
			return false;
		} else {
			return true;
		}
	}
	
	public static void delFile(String filepath){
		if(null != filepath && !"".equals(filepath)){
			File file = new File(filepath);
			if(file.exists()){
				file.delete();
			}
		}
	}
	public static void createFile(String path){
		File files = new File(path);
		if (!files.exists()) { // 当没有目录时 创建目录
			files.mkdirs();
		}
	}
	public static void delFiles(String path) {
		File files = new File(path);
		if (!files.exists()) { // 当没有目录时 创建目录
			files.mkdirs();
		} else {
			for (File f : files.listFiles()) {
				f.delete();
			}
		}
	}
	public static void saveFile(Bitmap bitmap,String path,String fileName){
		BufferedOutputStream out = null;
		try {
			File file = new File(path);
			if (!file.exists()) {
				file.mkdir();
			}
			File img = new File(path+fileName);
			out = new BufferedOutputStream(new FileOutputStream(img));
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
