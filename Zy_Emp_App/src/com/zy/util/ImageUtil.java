package com.zy.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Bitmap.Config;

public class ImageUtil {
	public static Bitmap getCircleBitmap(Bitmap source) {
		final Paint paint = new Paint();  
        paint.setAntiAlias(true);  
        int radius = Math.min(source.getHeight(), source.getWidth());
        Bitmap target = Bitmap.createBitmap(radius, radius, Config.ARGB_8888);  
        /** 
         * 产生一个同样大小的画布 
         */  
        Canvas canvas = new Canvas(target);  
        /** 
         * 首先绘制圆形 
         */  
        canvas.drawCircle(radius / 2, radius / 2, radius / 2, paint);  
        /** 
         * 使用SRC_IN 
         */  
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));  
        /** 
         * 绘制图片 
         */  
        canvas.drawBitmap(source, 0, 0, paint);  
        return target; 
	}
}
