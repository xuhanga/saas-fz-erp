package com.zy.util;

import java.util.Comparator;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import zy.entity.sys.user.T_Sys_User;
import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class StringComparator implements Comparator<Object>{
	@Override
	public int compare(Object _user0, Object _user1) {
		// ������������
		T_Sys_User user0 = (T_Sys_User) _user0;
		T_Sys_User user1 = (T_Sys_User) _user1;
		String catalog0 = "";
		String catalog1 = "";

		if (user0 != null && user0.getUs_name() != null
				&& user0.getUs_name().length() > 1)
			catalog0 = converterToFirstSpell(user0.getUs_name())
					.substring(0, 2);

		if (user1 != null && user1.getUs_name() != null
				&& user1.getUs_name().length() > 1)
			catalog1 = converterToFirstSpell(user1.getUs_name())
					.substring(0, 2);
		int flag = catalog0.compareTo(catalog1);
		return flag;
	}
	
	/**
	 * ���ַ����е�����ת��Ϊƴ��,�����ַ�����
	 * 
	 * @param inputString
	 * @return
	 */
	public static String getPingYin(String inputString) {
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		format.setVCharType(HanyuPinyinVCharType.WITH_V);

		char[] input = inputString.trim().toCharArray();
		String output = "";

		try {
			for (int i = 0; i < input.length; i++) {
				if (java.lang.Character.toString(input[i]).matches(
						"[\\u4E00-\\u9FA5]+")) {
					String[] temp = PinyinHelper.toHanyuPinyinStringArray(
							input[i], format);
					output += temp[0];
				} else
					output += java.lang.Character.toString(input[i]);
			}
		} catch (BadHanyuPinyinOutputFormatCombination e) {
			e.printStackTrace();
		}
		return output;
	}

	/**
	 * ����ת��λ����ƴ������ĸ��Ӣ���ַ�����
	 * 
	 * @param chines
	 *            ����
	 * @return ƴ��
	 */
	public static String converterToFirstSpell(String chines) {
		String pinyinName = "";
		char[] nameChar = chines.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		for (int i = 0; i < nameChar.length; i++) {
			if (nameChar[i] > 128) {
				try {
					pinyinName += PinyinHelper.toHanyuPinyinStringArray(
							nameChar[i], defaultFormat)[0].charAt(0);
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}
			} else {
				pinyinName += nameChar[i];
			}
		}
		return pinyinName.toUpperCase();
	}
}
