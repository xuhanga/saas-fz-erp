package com.zy.view;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class CrashHandler implements UncaughtExceptionHandler{
	public static final String TAG = "CrashHandler";
	private static CrashHandler instance = new CrashHandler();
	private Context mContext;
	private Thread.UncaughtExceptionHandler mDefaultHandler;
	
	private CrashHandler(){
		
	}
	public static CrashHandler getInstance(){
		return instance;
	}
	public void init(Context ctx){
		mContext = ctx;
		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);
	}
	
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		 if (!handleException(ex) && mDefaultHandler != null) {   
	            //如果用户没有处理则让系统默认的异常处理器来处理    
	            mDefaultHandler.uncaughtException(thread, ex);   
	        } else {
//	        	((Activity)mContext).finish();
	            try {
	                Thread.sleep(1000);
	            } catch (InterruptedException e) {
	                Log.e(TAG, "error : ", e);
	            }
	            //退出程序
	            android.os.Process.killProcess(android.os.Process.myPid());
	            System.exit(1);
	        } 
	}
	/** 
     * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成. 
     * @param ex 
     * @return true:如果处理了该异常信息;否则返回false. 
     */   
    private boolean handleException(final Throwable ex) {   
        if (ex == null) {   
            return false;   
        }
        //使用Toast来显示异常信息    
        new Thread() {   
            @Override   
            public void run() {   
                Looper.prepare();   
                Toast.makeText(mContext, ex.toString()+"很抱歉,程序出现异常...", Toast.LENGTH_LONG).show();   
                Looper.loop();   
            }   
        }.start();
        ex.printStackTrace();
        Log.e("error", ex.toString());
        return true;   
    }   
}
