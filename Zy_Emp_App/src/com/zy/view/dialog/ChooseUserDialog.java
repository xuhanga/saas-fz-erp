package com.zy.view.dialog;

import java.util.ArrayList;
import java.util.List;

import zy.entity.sys.user.T_Sys_User;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.zy.R;

public class ChooseUserDialog extends Dialog implements android.view.View.OnClickListener{
	private TextView btn_cancel,btn_ok,dialog_title;
	private ListView listView;
	private List<T_Sys_User> users;
	private String title;
	private Context context;
	private boolean[] state; // �Ƿ�ѡ��
	
	public ChooseUserDialog(Context context,String title,List<T_Sys_User> users) {
		super(context,R.style.dialog);
		setContentView(R.layout.layout_dialog_chooselist);
		this.title = title;
		this.users = users;
		this.context = context;
		this.state = new boolean[users.size()];
		initView();
		initListener();
	}

	private void initView() {
		dialog_title = (TextView)findViewById(R.id.dialog_title);
		dialog_title.setText(title);
		btn_cancel = (TextView)findViewById(R.id.btn_cancel);
		btn_ok = (TextView)findViewById(R.id.btn_ok);
		listView = (ListView)findViewById(R.id.lv_content_list);
	}
	
	private void initListener() {
		btn_cancel.setOnClickListener(this);
//		listView.setAdapter(new ChooseUserListAdapter(context, users, state));
	}
	public TextView getOkButton(){
		return btn_ok;
	}
	public List<T_Sys_User> getSelectedModel(){
		List<T_Sys_User> selecteds = new ArrayList<T_Sys_User>();
		for (int i = 0; i < state.length; i++) {
			if (state[i]) {
				selecteds.add(users.get(i));
			}
		}
		return selecteds;
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_cancel:
				if (isShowing()) {
					super.dismiss();
				}
			break;
		}
	}
}
