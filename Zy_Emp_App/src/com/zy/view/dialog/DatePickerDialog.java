package com.zy.view.dialog;

import java.util.Calendar;

import zy.util.DateUtil;
import zy.util.StringUtil;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;

import com.zy.R;
public class DatePickerDialog extends Dialog implements OnDateChangedListener, android.view.View.OnClickListener{
	private DatePicker datePicker;  
    private String date;  
    private TextView btn_cancel, btn_ok,dialog_title;
    private String initDate;
    public DatePickerDialog(Context context,String initDate){
    	super(context);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_dialog_date);
		this.initDate = initDate;
		initViews();
		initListener();
		initDate();
		initEvents();
		setCancelable(true);
		setCanceledOnTouchOutside(true);
    }
    private void initViews() {
		datePicker = (DatePicker)findViewById(R.id.datepicker);
		dialog_title = (TextView)findViewById(R.id.dialog_title);
		btn_cancel = (TextView) findViewById(R.id.btn_cancel);
		btn_ok = (TextView) findViewById(R.id.btn_ok);
	}
    private void initListener(){
    	btn_cancel.setOnClickListener(this);
    }
    private void initDate(){
        Calendar calendar = Calendar.getInstance(); 
        if(StringUtil.isNotEmpty(initDate)){
        	calendar = DateUtil.parseDateString(initDate, DateUtil.FORMAT_YEAR_MON_DAY); 
        }
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);  
  	}
    private void initEvents() {
		onDateChanged(null, 0, 0, 0);
	}
	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();  
        calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
        date = DateUtil.format(calendar, DateUtil.FORMAT_YEAR_MON_DAY);
        dialog_title.setText(date);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_cancel:
				if(isShowing()){
					super.dismiss();
				}
			break;
		}
	}
	public TextView getOkButton(){
		return btn_ok;
	}
	public String getDate(){
		return date;
	}
}
