package com.zy.view.dialog;

import java.util.Calendar;

import zy.util.DateUtil;

import com.zy.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.DatePicker.OnDateChangedListener;

public class DateSectionPickerDialog extends Dialog implements OnDateChangedListener, android.view.View.OnClickListener{
	private DatePicker begindatePicker, enddatePicker;
	private String begindate, enddate;
	private TextView btn_cancel, btn_ok, dialog_title;
    public DateSectionPickerDialog(Context context){
    	super(context);
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_dialog_datesection);
		initViews();
		initListener();
		initDate();
		initEvents();
		setCancelable(true);
		setCanceledOnTouchOutside(true);
    }
    private void initViews() {
    	begindatePicker = (DatePicker)findViewById(R.id.begindatepicker);
    	enddatePicker = (DatePicker)findViewById(R.id.enddatepicker);
		dialog_title = (TextView)findViewById(R.id.dialog_title);
		btn_cancel = (TextView) findViewById(R.id.btn_cancel);
		btn_ok = (TextView) findViewById(R.id.btn_ok);
	}
    private void initListener(){
    	btn_cancel.setOnClickListener(this);
    }
    private void initDate(){
        Calendar calendar = Calendar.getInstance();  
        begindatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);  
        enddatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), this);
        begindate = DateUtil.getYearMonthDate();
        enddate = DateUtil.getYearMonthDate();
  	}
    private void initEvents() {
		onDateChanged(null, 0, 0, 0);
	}
	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();  
        calendar.set(begindatePicker.getYear(), begindatePicker.getMonth(), begindatePicker.getDayOfMonth());
        begindate = DateUtil.format(calendar, DateUtil.FORMAT_YEAR_MON_DAY);
        calendar.set(enddatePicker.getYear(), enddatePicker.getMonth(), enddatePicker.getDayOfMonth());
        enddate = DateUtil.format(calendar, DateUtil.FORMAT_YEAR_MON_DAY);
        dialog_title.setText(begindate+"~"+enddate);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_cancel:
				if(isShowing()){
					super.dismiss();
				}
			break;
		}
	}
	public TextView getOkButton(){
		return btn_ok;
	}
	public String[] getDate(){
		return new String[] { begindate, enddate };
	}
}
