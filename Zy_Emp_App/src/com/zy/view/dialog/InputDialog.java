package com.zy.view.dialog;

import zy.util.StringUtil;

import com.zy.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

public class InputDialog extends Dialog implements android.view.View.OnClickListener{
	private TextView btn_cancel, btn_ok,dialog_title;
	private EditText et_content;
	private Context context;
	private String title;
	
	public InputDialog(Context context,String title) {
		super(context);
		this.context = context;
		this.title = title;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_dialog_input);
		initViews();
		initListener();
	}
	
	private void initViews() {
		dialog_title = (TextView) findViewById(R.id.dialog_title);
		btn_cancel = (TextView) findViewById(R.id.btn_cancel);
		btn_ok = (TextView) findViewById(R.id.btn_ok);
		et_content = (EditText)findViewById(R.id.et_content);
		dialog_title.setText(title);
	}
	private void initListener() {
		btn_cancel.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_cancel:
			if (isShowing()) {
				super.dismiss();
			}
			break;
		}
	}
	public TextView getOkButton(){
		return btn_ok;
	}
	public String getInputValue(){
		return StringUtil.trimString(et_content.getText());
	}
}
