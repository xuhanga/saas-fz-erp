package com.zy.view.dialog.TitleMenu;

import android.content.Context;

/**
 * 功能描述：弹窗内部子类项（绘制标题和图标）
 */
public class ActionItem {
	// 定义文本对象
	public CharSequence mTitle;
	
	public int mIconId;

	public ActionItem(Context context, CharSequence title) {
		this.mTitle = title;
	}

	public ActionItem(Context context,int titleId,int iconId) {
		this.mTitle = context.getResources().getText(titleId);
		this.mIconId = iconId;
	}
}
