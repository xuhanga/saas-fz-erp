package com.zy.view.dialog.popup;

import com.zy.R;
import com.zy.util.DensityUtil;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class BottomPopup extends PopupWindow{
	//上下文对象  
    private Context mContext; 
    //Title文字  
    private String mTitle;  
    //选项的文字  
    private String[] options;  
    //选项的颜色  
    private int[] Colors;  
    //点击事件  
    private onPopupWindowItemClickListener itemClickListener;  
    
    public BottomPopup(Context context, String title,String[] options) {  
        this.mContext = context;  
        this.mTitle = title; 
        this.options = options;
        setAnimationStyle(R.style.popwindow_anim_style);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		setContentView(LayoutInflater.from(mContext).inflate(R.layout.popup_bottom, null));
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);  
        setOutsideTouchable(true);  
        setOnDismissListener(new PopupWindow.OnDismissListener() {  
            @Override  
            public void onDismiss() {  
                setWindowAlpa(false);  
            }  
        });  
        initView();
    } 
    
    private void initView() {
    	LinearLayout lin_layout = (LinearLayout) getContentView().findViewById(R.id.layout_popup_add);  
        //Title  
        TextView tv_pop_title = (TextView) getContentView().findViewById(R.id.tv_popup_title);  
        //取消按钮  
        Button btn_cancel = (Button) getContentView().findViewById(R.id.btn_cancel);  
        btn_cancel.setOnClickListener(new View.OnClickListener() {  
            @Override  
            public void onClick(View v) {  
                dismiss();  
            }  
        });  
        if (mTitle != null) {  
            tv_pop_title.setText(mTitle);  
        } else {  
            tv_pop_title.setVisibility(View.GONE);  
        }  
        if (options != null && options.length > 0) {  
        	LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,DensityUtil.dip2px(mContext, 50));
        	
            for (int i = 0; i < options.length; i++) {
                View item = LayoutInflater.from(mContext).inflate(R.layout.popup_bottom_item, null);
                item.setLayoutParams(layoutParams);
                Button btn_txt = (Button) item.findViewById(R.id.btn_popup_option);  
                btn_txt.setText(options[i]);  
                if (Colors != null && Colors.length == options.length) {  
                    btn_txt.setTextColor(Colors[i]);  
                }  
                final int finalI = i;  
                btn_txt.setOnClickListener(new View.OnClickListener() {  
                    @Override  
                    public void onClick(View v) {  
                    	dismiss();
                        if (itemClickListener != null) {  
                            itemClickListener.onItemClick(finalI);  
                        }  
                    }  
                });  
                lin_layout.addView(item);  
            }  
        }  
	}
    
    public void showView(){
    	 if (!isShowing()) {  
             showAtLocation(getContentView(), Gravity.BOTTOM, 0, 0);  
         }  
         setWindowAlpa(true); 
    }
    
    public void dismiss() {  
        if (isShowing()) {  
            super.dismiss();  
        }  
    } 
    
    public void setWindowAlpa(boolean isopen) {  
        if (android.os.Build.VERSION.SDK_INT < 11) {  
            return;  
        }  
        final Window window = ((Activity) mContext).getWindow();
        final WindowManager.LayoutParams lp = window.getAttributes();  
        window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);  
        ValueAnimator animator;  
        if (isopen) {  
            animator = ValueAnimator.ofFloat(1.0f, 0.5f);  
        } else {  
            animator = ValueAnimator.ofFloat(0.5f, 1.0f);  
        }  
        animator.setDuration(400);  
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {  
  
            @Override  
            public void onAnimationUpdate(ValueAnimator animation) {  
                float alpha = (Float) animation.getAnimatedValue();  
                lp.alpha = alpha;  
                window.setAttributes(lp);  
            }  
        });  
        animator.start();  
    }  
    
    /** 
     * 设置选项的点击事件 
     * 
     * @param itemClickListener 
     */  
    public void setItemClickListener(onPopupWindowItemClickListener itemClickListener) {  
        this.itemClickListener = itemClickListener;  
    }  
  
    /** 
     * 设置选项文字颜色，必须要和选项的文字对应 
     */  
    public void setColors(int... color) {  
        Colors = color;  
    }  
    
    /** 
     * 点击事件选择回调 
     */  
    public interface onPopupWindowItemClickListener {  
        void onItemClick(int position);  
    } 
}
