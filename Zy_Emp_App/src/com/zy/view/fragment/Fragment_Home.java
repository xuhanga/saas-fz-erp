package com.zy.view.fragment;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.emp.PerformanceActivity;
import com.zy.activity.receive.ReceiveIndexActivity;
import com.zy.activity.sale.SaleActivity;
import com.zy.activity.stock.StockActivity;
import com.zy.activity.vip.MemberActivity;
import com.zy.activity.vip.MemberAddActivity;
import com.zy.activity.vip.visit.BirthdayVisitActivity;
import com.zy.activity.vip.visit.SellVisitActivity;
import com.zy.adapter.index.HomeGridAdapter;
import com.zy.util.ActivityUtil;
import com.zy.util.Constants;
import com.zy.util.NetUtil;
import com.zy.view.view.HomeGridView;

public class Fragment_Home extends Fragment implements OnClickListener,OnItemClickListener {
	private Activity context;
	private View layout;
	public RelativeLayout errorItem;
	private HomeGridView gridview;
	public TextView tv_qrcode,tv_cart,tv_vip,errorText,
	tv_vipadd_icon,tv_vip_day_icon,tv_performance_icon;//
	public Typeface iconfont;
	private Map<String,String> keyMap;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (layout == null) {
			context = this.getActivity();
			layout = context.getLayoutInflater().inflate(R.layout.fragment_home,null);
			initView();
			setOnListener();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}
	private void initView(){
		iconfont = Typeface.createFromAsset(context.getAssets(), "iconfont/iconfont.ttf");
		errorItem = (RelativeLayout) layout.findViewById(R.id.rl_error_item);
		gridview = (HomeGridView) layout.findViewById(R.id.gridview);
		gridview.setAdapter(new HomeGridAdapter(context));
		tv_qrcode = (TextView) layout.findViewById(R.id.tv_qrcode);
		tv_cart = (TextView) layout.findViewById(R.id.tv_cart);
		tv_vip = (TextView) layout.findViewById(R.id.tv_vip);
		tv_performance_icon = (TextView) layout.findViewById(R.id.tv_performance_icon);
		tv_vip_day_icon = (TextView) layout.findViewById(R.id.tv_vip_day_icon);
		tv_vipadd_icon  = (TextView) layout.findViewById(R.id.tv_vipadd_icon);
		tv_qrcode.setTypeface(iconfont);
		tv_cart.setTypeface(iconfont);
		tv_vip.setTypeface(iconfont);
		tv_vip_day_icon.setTypeface(iconfont);
		tv_vipadd_icon.setTypeface(iconfont);
		tv_performance_icon.setTypeface(iconfont);
		keyMap = new HashMap<String, String>(3);
	}

	private void setOnListener() {
		gridview.setOnItemClickListener(this);
		errorItem.setOnClickListener(this);
		layout.findViewById(R.id.ll_menu_vipadd).setOnClickListener(this);
		layout.findViewById(R.id.ll_menu_vip_day).setOnClickListener(this);
		layout.findViewById(R.id.ll_menu_performance).setOnClickListener(this);
	}
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long number) {
		switch (position) { //对应首页的功能模		
			case 0:
				keyMap.clear();
				keyMap.put(Constants.TITLE, "接待顾客");
				ActivityUtil.startActivity(context, ReceiveIndexActivity.class, keyMap);
				break;
			case 1:
				ActivityUtil.start_Activity(context, MemberActivity.class, new BasicNameValuePair(Constants.TITLE, "会员列表"));
				break;
			case 2:
				ActivityUtil.start_Activity(context, MemberAddActivity.class, new BasicNameValuePair(Constants.TITLE, "新增会员"));
				break;
			case 3:
				ActivityUtil.start_Activity(context, StockActivity.class, new BasicNameValuePair(Constants.TITLE, "库存查询"));
				break;
			case 4:
				ActivityUtil.start_Activity(context, SellVisitActivity.class, new BasicNameValuePair(Constants.TITLE, "销售回访"));
				break;
			case 5:
				ActivityUtil.start_Activity(context, BirthdayVisitActivity.class, new BasicNameValuePair(Constants.TITLE, "生日回访"));
				break;
			case 7:
				ActivityUtil.start_Activity(context, SaleActivity.class, new BasicNameValuePair(Constants.TITLE, "促销查询"));
				break;
			default:
				break;
		}
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.rl_error_item:
				NetUtil.openSetNetWork(getActivity());
				break;
			case R.id.ll_menu_vipadd:
				ActivityUtil.start_Activity(context, MemberAddActivity.class, new BasicNameValuePair(Constants.TITLE, "新增会员"));
				break;
			case R.id.ll_menu_vip_day:
				ActivityUtil.start_Activity(context, BirthdayVisitActivity.class, new BasicNameValuePair(Constants.TITLE, "生日回访"));
				break;
			case R.id.ll_menu_performance:
				ActivityUtil.start_Activity(context, PerformanceActivity.class, new BasicNameValuePair(Constants.TITLE, "本月业绩"));
				break;
			default:
				break;
		}
	}
}
