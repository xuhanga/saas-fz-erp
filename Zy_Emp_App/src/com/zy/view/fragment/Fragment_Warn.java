package com.zy.view.fragment;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.index.MainActivity;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;

public class Fragment_Warn extends Fragment implements OnClickListener{
	private Handler handler;
	private Activity context;
	private View layout;
	private TextView tv_notice_count,tv_stall_count,tv_stallproperty_count,tv_checkdata_count;
	private int notice_count,stall_count,stallproperty_count,checkdata_count;
	private static final int STATE_LOAD_NOTICE = 2;
	private static final int STATE_LOAD_STALL = 3;
	private static final int STATE_LOAD_STALLPROPERTY = 4;
	private static final int STATE_LOAD_CHECKDATA = 5;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (layout == null) {
			context = this.getActivity();
			layout = context.getLayoutInflater().inflate(R.layout.fragment_warn,null);
			initViews();
			setOnListener();
			initHandler();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}

	private void initViews() {
		tv_notice_count = (TextView)layout.findViewById(R.id.tv_notice_count);
		tv_stall_count = (TextView)layout.findViewById(R.id.tv_stall_count);
		tv_stallproperty_count = (TextView)layout.findViewById(R.id.tv_stallproperty_count);
		tv_checkdata_count = (TextView)layout.findViewById(R.id.tv_checkdata_count);
	}
	
	private void setOnListener() {
		layout.findViewById(R.id.ll_notice).setOnClickListener(this);
		layout.findViewById(R.id.ll_stall).setOnClickListener(this);
		layout.findViewById(R.id.ll_stallproperty).setOnClickListener(this);
		layout.findViewById(R.id.ll_checkdata).setOnClickListener(this);
	}
	private void initHandler(){
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
					case STATE_LOAD_NOTICE:
						notice_count = (Integer)msg.obj;
						tv_notice_count.setText(notice_count+"");
						if(notice_count > 0 && context instanceof MainActivity){
							((MainActivity)context).showWarn();
						}
						break;
					case STATE_LOAD_STALL:
						stall_count = (Integer)msg.obj;
						tv_stall_count.setText(stall_count+"");
						if(stall_count > 0 && context instanceof MainActivity){
							((MainActivity)context).showWarn();
						}
						break;
					case STATE_LOAD_STALLPROPERTY:
						stallproperty_count = (Integer)msg.obj;
						tv_stallproperty_count.setText(stallproperty_count+"");
						if(stallproperty_count > 0 && context instanceof MainActivity){
							((MainActivity)context).showWarn();
						}
						break;
					case STATE_LOAD_CHECKDATA:
						checkdata_count = (Integer)msg.obj;
						tv_checkdata_count.setText(checkdata_count+"");
						if(checkdata_count > 0 && context instanceof MainActivity){
							((MainActivity)context).showWarn();
						}
						break;
					case CommonParam.STATE_ERROR:
						ActivityUtil.showShortToast(context, "数据加载错误");
						break;
				}
			}
		};
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ll_notice:
				if(notice_count <= 0 ){
					return;
				}
				Bundle bundle1 = new Bundle();
				bundle1.putString(Constants.TITLE, "公告通知");
//				ActivityUtil.start_Activity(context, NoticeActivity.class, bundle1);
				break;
			case R.id.ll_stall:
				if(stall_count <= 0 ){
					return;
				}
				Bundle bundle2 = new Bundle();
				bundle2.putString(Constants.TITLE, "摊位即将到期");
//				ActivityUtil.start_Activity(context, StallWarnActivity.class, bundle2);
				break;
			case R.id.ll_stallproperty:
				if(stallproperty_count <= 0 ){
					return;
				}
				Bundle bundle3 = new Bundle();
				bundle3.putString(Constants.TITLE, "物业即将到期");
				bundle3.putString("isproperty", "1");
//				ActivityUtil.start_Activity(context, StallWarnActivity.class, bundle3);
				break;
			case R.id.ll_checkdata:
				if(checkdata_count <= 0 ){
					return;
				}
				Bundle bundle4 = new Bundle();
				bundle4.putString(Constants.TITLE, "未交款违章");
//				ActivityUtil.start_Activity(context, CheckDataWarnActivity.class, bundle4);
				break;
			default:
				break;
		}
	}
	
	private void loadNoticeData() {
		Map<String, Object> params = new HashMap<String, Object>();
//		noticeAPI.count(params, handler, STATE_LOAD_NOTICE);
	}
	private void loadStallData(){
		Map<String, Object> params = new HashMap<String, Object>();
//		stallAPI.countWarn(params, handler, STATE_LOAD_STALL);
	}
	private void loadStallPropertyData(){
		Map<String, Object> params = new HashMap<String, Object>();
//		stallAPI.countPropertyWarn(params, handler, STATE_LOAD_STALLPROPERTY);
	}
	private void loadCheckData(){
		Map<String, Object> params = new HashMap<String, Object>();
//		dataAPI.countWarn(params, handler, STATE_LOAD_CHECKDATA);
	}
	
	
	public void refresh(){
		loadNoticeData();
		loadStallData();
		loadStallPropertyData();
		loadCheckData();
	}
}
