package com.zy.view.fragment.vip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import zy.entity.vip.member.T_Vip_Member;
import zy.entity.vip.member.T_Vip_Member_Info;
import zy.util.StringUtil;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zy.R;
import com.zy.activity.AllApplication;
import com.zy.activity.util.ShowImageActivity;
import com.zy.api.vip.MemberAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.FileUtil;
import com.zy.util.ImageUtil;
import com.zy.view.dialog.popup.BottomPopup;

public class Fragment_Info extends Fragment implements OnClickListener{
	private Handler handler;
	private View layout = null;
	private Activity context;
	
	private TextView tv_vm_cardcode,tv_vm_mobile,tv_vm_name,tv_vm_date,tv_vm_shop_code,tv_vm_mt_code,tv_vm_birthday,tv_vm_manager,tv_vmi_marriage_state,tv_vmi_nation,tv_vmi_bloodtype,tv_vmi_address;
	private ImageView iv_vm_img;
	private T_Vip_Member member;
	private T_Vip_Member_Info memberInfo;
	private MemberAPI memberAPI;
	
	private BottomPopup photoPopup;
	
	private static final int SUCC_STATE_DOWNLOADIMG = 3;
	private String IMG_PATH;
	private int PHOTO_REQUEST_CAMERA = 0x11,PHOTO_REQUEST_PICK = 0x12;
	private String img_name = "member.jpg";
	private Bitmap bitmap_img;
	
	public Fragment_Info(T_Vip_Member member){
		this.member = member;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (layout == null) {
			context = this.getActivity();
			layout = context.getLayoutInflater().inflate(R.layout.fragment_vipinfo,null);
			initHandler();
			initView();
			initListener();
			initData();
			initPopWindow();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}
	
	private void initPopWindow() {
		final String[] photoOptions = new String[]{"拍照","从相册选择","查看原图"};
		photoPopup = new BottomPopup(context, null, photoOptions);
		photoPopup.setItemClickListener(new BottomPopup.onPopupWindowItemClickListener() {
			@Override
			public void onItemClick(int position) {
				if(position == 0){//拍照
					if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
						FileUtil.createFile(IMG_PATH);
						Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						// 下面这句指定调用相机拍照后的照片存储的路径
						intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(IMG_PATH,img_name)));
						startActivityForResult(intent,PHOTO_REQUEST_CAMERA);// 打开系统拍照
					} else {
						ActivityUtil.showShortToast(context,"请检查SD卡!");
					}
				}else if(position == 1){//从相册选择
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(intent, PHOTO_REQUEST_PICK);
				}else if(position == 2){//查看原图
					if(bitmap_img != null){
						AllApplication.getInstance().setImage(bitmap_img);
						ActivityUtil.start_Activity(context,ShowImageActivity.class);
					}
				}
			}
		});
	}

	private void initView(){
		memberAPI = new MemberAPI(context);
		iv_vm_img = (ImageView)layout.findViewById(R.id.iv_vm_img);
		tv_vm_name = (TextView)layout.findViewById(R.id.tv_vm_name);
		tv_vm_date = (TextView)layout.findViewById(R.id.tv_vm_date);
		tv_vm_shop_code = (TextView)layout.findViewById(R.id.tv_vm_shop_code);
		tv_vm_mt_code = (TextView)layout.findViewById(R.id.tv_vm_mt_code);
		tv_vm_birthday = (TextView)layout.findViewById(R.id.tv_vm_birthday);
		tv_vm_manager = (TextView)layout.findViewById(R.id.tv_vm_manager);
		tv_vmi_marriage_state = (TextView)layout.findViewById(R.id.tv_vmi_marriage_state);
		tv_vmi_nation = (TextView)layout.findViewById(R.id.tv_vmi_nation);
		tv_vmi_bloodtype = (TextView)layout.findViewById(R.id.tv_vmi_bloodtype);
		tv_vm_cardcode = (TextView)layout.findViewById(R.id.tv_vm_cardcode);
		tv_vm_mobile = (TextView)layout.findViewById(R.id.tv_vm_mobile);
		tv_vmi_address = (TextView)layout.findViewById(R.id.tv_vmi_address);
		initViewValue();
	}
	
	private void initListener() {
		iv_vm_img.setOnClickListener(this);
	}
	
	public void initViewValue(){
		if(member != null){
			tv_vm_name.setText(StringUtil.trimString(member.getVm_name())+" "+StringUtil.trimString(member.getVm_sex()));
			tv_vm_date.setText(StringUtil.trimString(member.getVm_date()));
			tv_vm_shop_code.setText(StringUtil.trimString(member.getShop_name()));
			tv_vm_mt_code.setText(StringUtil.trimString(member.getMt_name()));
			if(member.getVm_birthday_type() != null){
				if(member.getVm_birthday_type().intValue() == 0){//公历
					tv_vm_birthday.setText(StringUtil.trimString(member.getVm_birthday())+"  "+formatBirthdayType(member.getVm_birthday_type()));
				}else if(member.getVm_birthday_type().intValue() == 1){//农历
					tv_vm_birthday.setText(StringUtil.trimString(member.getVm_lunar_birth())+"  "+formatBirthdayType(member.getVm_birthday_type()));
				}
			}
			tv_vm_manager.setText(StringUtil.trimString(member.getVm_manager()));
			tv_vm_cardcode.setText(StringUtil.trimString(member.getVm_cardcode()));
			tv_vm_mobile.setText(StringUtil.trimString(member.getVm_mobile()));
			if(StringUtil.isNotEmpty(member.getVm_img_path())){
				memberAPI.loadImg(CommonParam.file_url+member.getVm_img_path(), handler, SUCC_STATE_DOWNLOADIMG);
			}
		}
		if(memberInfo != null){
			tv_vmi_marriage_state.setText(formatMarriageState(memberInfo.getVmi_marriage_state()));
			tv_vmi_nation.setText(StringUtil.trimString(memberInfo.getVmi_nation()));
			tv_vmi_bloodtype.setText(StringUtil.trimString(memberInfo.getVmi_bloodtype()));
			tv_vmi_address.setText(StringUtil.trimString(memberInfo.getVmi_address()));
		}
	}
	
	private String formatBirthdayType(Integer birthdaytype){
		if(birthdaytype == null){
			return "";
		}
		if(birthdaytype.intValue() == 0){
			return "公历";
		}
		if(birthdaytype.intValue() == 1){
			return "农历";
		}
		return "";
	}
	private String formatMarriageState(Integer vmi_marriage_state){//0未婚 1已婚 2曾婚
		if(vmi_marriage_state == null){
			return "";
		}
		if(vmi_marriage_state.intValue() == 0){
			return "未婚";
		}
		if(vmi_marriage_state.intValue() == 1){
			return "已婚";
		}
		if(vmi_marriage_state.intValue() == 2){
			return "曾婚";
		}
		return "";
	}
	
	@SuppressLint("HandlerLeak")
	private void initHandler(){
		handler = new Handler(){
			@SuppressWarnings("unchecked")
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
					case SUCC_STATE_DOWNLOADIMG:
						if(msg.obj != null){
							bitmap_img = (Bitmap)msg.obj;
							iv_vm_img.setImageBitmap(ImageUtil.getCircleBitmap(bitmap_img));
						}
						break;
					case CommonParam.STATE_LOAD:
						Map<String, Object> resultMap = (Map<String, Object>)msg.obj;
						member = (T_Vip_Member)resultMap.get("member");
						memberInfo = (T_Vip_Member_Info)resultMap.get("memberInfo");
						initViewValue();
						break;
					case CommonParam.STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(context, message);
						break;
				}
			}
		};
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_vm_img:
			photoPopup.showView();
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			
			if(requestCode == PHOTO_REQUEST_CAMERA){//拍照
				bitmap_img = FileUtil.loadAndCompressBitmap(IMG_PATH + img_name);
				iv_vm_img.setImageBitmap(ImageUtil.getCircleBitmap(bitmap_img));
				updateImg();
				return;
			}
			if(requestCode == PHOTO_REQUEST_PICK){//从相册选择
				Uri uri = data.getData();
				try {
					Bitmap bit = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
					FileUtil.saveFile(bit, IMG_PATH, img_name);
					bitmap_img = FileUtil.loadAndCompressBitmap(IMG_PATH + img_name);
					iv_vm_img.setImageBitmap(ImageUtil.getCircleBitmap(bitmap_img));
					updateImg();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void updateImg(){
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("vm_id", member.getVm_id());
		memberAPI.updateImg(params, new File(IMG_PATH + img_name), handler, R.id.iv_vm_img);
	}
	
	private void initData(){
		IMG_PATH = Environment.getExternalStorageDirectory()+CommonParam.PATH_MEMBER_IMG;
		FileUtil.delFiles(IMG_PATH);
		memberAPI.load(member.getVm_id(), handler, CommonParam.STATE_LOAD);
	}
	public void setMember(T_Vip_Member member) {
		this.member = member;
	}
	public void setMemberInfo(T_Vip_Member_Info memberInfo) {
		this.memberInfo = memberInfo;
	}
}
