package com.zy.view.fragment.vip;

import java.util.HashMap;
import java.util.Map;

import zy.dto.vip.member.ConsumeSnapshotDto;
import zy.entity.vip.member.T_Vip_Member;
import zy.util.CommonUtil;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zy.R;
import com.zy.api.sell.SellReportAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.SPUtil;

public class Fragment_Report extends Fragment{
	private Handler handler;
	private View layout = null;
	private Activity context;
	private TextView tv_customer_unit_price,tv_unit_price,tv_joint_rate,tv_consume_money,tv_consume_times,tv_consume_amount,tv_recommend_amount,tv_consume_rate,tv_lastbuy_date,tv_lastbuy_money;
	private T_Vip_Member member;
	private ConsumeSnapshotDto consumeSnapshot;
	private SellReportAPI sellReportAPI;
	
	public Fragment_Report(T_Vip_Member member){
		this.member = member;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		if (layout == null) {
			context = this.getActivity();
			layout = context.getLayoutInflater().inflate(R.layout.fragment_report,null);
			initView();
			initHandler();
			initData();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}
	private void initView(){
		sellReportAPI = new SellReportAPI(context);
		tv_customer_unit_price = (TextView)layout.findViewById(R.id.tv_customer_unit_price);
		tv_unit_price = (TextView)layout.findViewById(R.id.tv_unit_price);
		tv_joint_rate = (TextView)layout.findViewById(R.id.tv_joint_rate);
		tv_consume_money = (TextView)layout.findViewById(R.id.tv_consume_money);
		tv_consume_times = (TextView)layout.findViewById(R.id.tv_consume_times);
		tv_consume_amount = (TextView)layout.findViewById(R.id.tv_consume_amount);
		tv_recommend_amount = (TextView)layout.findViewById(R.id.tv_recommend_amount);
		tv_consume_rate = (TextView)layout.findViewById(R.id.tv_consume_rate);
		tv_lastbuy_date = (TextView)layout.findViewById(R.id.tv_lastbuy_date);
		tv_lastbuy_money = (TextView)layout.findViewById(R.id.tv_lastbuy_money);
	}
	private void initViewValue(){
		if(consumeSnapshot != null){
			tv_customer_unit_price.setText("客单价："+String.format("%.2f", consumeSnapshot.getCustomer_unit_price())+"元");
			tv_unit_price.setText("物单价："+String.format("%.2f", consumeSnapshot.getUnit_price())+"元");
			tv_joint_rate.setText("连带率："+String.format("%.2f", consumeSnapshot.getJoint_rate())+"件");
			tv_consume_money.setText("消费金额："+String.format("%.2f", consumeSnapshot.getConsume_money())+"元");
			tv_consume_times.setText("消费次数："+consumeSnapshot.getConsume_times()+"次");
			tv_consume_amount.setText("消费件数："+consumeSnapshot.getConsume_amount()+"件");
			tv_recommend_amount.setText("推荐人数："+consumeSnapshot.getRecommend_amount()+"人");
			tv_consume_rate.setText("消费频率："+consumeSnapshot.getConsume_rate()+"天/次");
			tv_lastbuy_date.setText("最近消费时间："+consumeSnapshot.getLastbuy_date());
			tv_lastbuy_money.setText("最近消费金额："+String.format("%.2f", consumeSnapshot.getLastbuy_money())+"元");
		}
	}
	@SuppressLint("HandlerLeak")
	private void initHandler(){
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
					case CommonParam.STATE_LOAD:
						consumeSnapshot = (ConsumeSnapshotDto)msg.obj;
						initViewValue();
						break;
					case CommonParam.STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(context, message);
						break;
				}
			}
		};
	}
	private void initData(){
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, SPUtil.getUser(context).getCompanyid());
		params.put("vm_code", member.getVm_code());
		sellReportAPI.loadConsumeSnapshot(params, handler, CommonParam.STATE_LOAD);
	}
}
