package com.zy.view.fragment.vip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zy.entity.sell.cash.T_Sell_Shop;
import zy.entity.vip.member.T_Vip_Member;
import zy.util.CommonUtil;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.zy.R;
import com.zy.activity.vip.SellShopDetailActivity;
import com.zy.adapter.vip.SellShopAdapter;
import com.zy.api.sell.SellReportAPI;
import com.zy.util.ActivityUtil;
import com.zy.util.CommonParam;
import com.zy.util.Constants;
import com.zy.util.SPUtil;
import com.zy.view.view.AutoListView;
import com.zy.view.view.AutoListView.OnLoadListener;

public class Fragment_Sell extends Fragment implements OnLoadListener{
	private Handler handler;
	private AutoListView autoListView;
	private SellShopAdapter sellShopAdapter;
	private View layout = null;
	private Activity context;
	private int pageindex = 1;
	private T_Vip_Member member;
	private List<T_Sell_Shop> sellShops = new ArrayList<T_Sell_Shop>();
	private SellReportAPI sellReportAPI;
	
	public Fragment_Sell(T_Vip_Member member) {
		this.member = member;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (layout == null) {
			context = this.getActivity();
			layout = context.getLayoutInflater().inflate(R.layout.fragment_sell,null);
			initView();
			initHandler();
			loadData();
		} else {
			ViewGroup parent = (ViewGroup) layout.getParent();
			if (parent != null) {
				parent.removeView(layout);
			}
		}
		return layout;
	}
	
	private void initView(){
		sellReportAPI = new SellReportAPI(context);
		autoListView = (AutoListView)layout.findViewById(R.id.lv_data);
		sellShopAdapter  = new SellShopAdapter(context,sellShops);
		autoListView.setOnLoadListener(this);
		autoListView.setAdapter(sellShopAdapter);
		autoListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == (autoListView.getCount() - 1))
					return;
				T_Sell_Shop sellShop = sellShops.get(position);
				Bundle bundle = new Bundle();
				bundle.putString(Constants.TITLE, "收银明细");
				bundle.putSerializable("sellShop", sellShop);
				ActivityUtil.start_Activity(context, SellShopDetailActivity.class, bundle);
			}
		});
	}
	@SuppressLint("HandlerLeak")
	private void initHandler(){
		handler = new Handler(){
			@SuppressWarnings("unchecked")
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
					case CommonParam.STATE_LOAD:
						if(msg.obj == null){
							return;
						}
						List<T_Sell_Shop> result = (List<T_Sell_Shop>)msg.obj;
						sellShops.addAll(result);
						autoListView.onLoadComplete();
						autoListView.onRefreshComplete();
						if (sellShops.size() > 0) {
							autoListView.setResultSize(result.size(), sellShops.size());
			                pageindex = sellShops.size()/CommonParam.pagesize+1;
			                sellShopAdapter.notifyDataSetChanged();
						}else {
							autoListView.setResultSize(result.size(), sellShops.size());
						}
						break;
					case CommonParam.STATE_ERROR:
						String message = null;
						if(msg.obj != null){
							message = msg.obj.toString();
						}
						ActivityUtil.showShortToast(context, message);
						break;
				}
			}
		};
	}
	
	@Override
	public void onLoad() {
		loadData();
	}
	
	private void loadData(){
		Map<String, Object> params = new HashMap<String,Object>();
		params.put(CommonUtil.COMPANYID, SPUtil.getUser(context).getCompanyid());
		params.put(CommonUtil.PAGEINDEX, pageindex);
		params.put(CommonUtil.PAGESIZE, CommonUtil.PAGE_SIZE);
		params.put("sh_vip_code", member.getVm_code());
		sellReportAPI.listShopByVip(params, handler, CommonParam.STATE_LOAD);
	}
}
