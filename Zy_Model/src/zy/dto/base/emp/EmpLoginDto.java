package zy.dto.base.emp;

import zy.entity.base.emp.T_Base_Emp;

public class EmpLoginDto extends T_Base_Emp{
	private static final long serialVersionUID = 6627658517718302844L;
	private String co_code;
	private int shoptype;
	private String shop_upcode;
	private int shop_uptype;
	public String getCo_code() {
		return co_code;
	}
	public void setCo_code(String co_code) {
		this.co_code = co_code;
	}
	public int getShoptype() {
		return shoptype;
	}
	public void setShoptype(int shoptype) {
		this.shoptype = shoptype;
	}
	public String getShop_upcode() {
		return shop_upcode;
	}
	public void setShop_upcode(String shop_upcode) {
		this.shop_upcode = shop_upcode;
	}
	public int getShop_uptype() {
		return shop_uptype;
	}
	public void setShop_uptype(int shop_uptype) {
		this.shop_uptype = shop_uptype;
	}
}
