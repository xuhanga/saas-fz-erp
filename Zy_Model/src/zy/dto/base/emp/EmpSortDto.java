package zy.dto.base.emp;

import zy.entity.base.emp.T_Base_Emp;

public class EmpSortDto extends T_Base_Emp{
	private static final long serialVersionUID = -7838137833678204860L;
	private Double sellmoney;
	private Integer receivecount;
	private Integer dealcount;
	public Double getSellmoney() {
		return sellmoney;
	}
	public void setSellmoney(Double sellmoney) {
		this.sellmoney = sellmoney;
	}
	public Integer getReceivecount() {
		return receivecount;
	}
	public void setReceivecount(Integer receivecount) {
		this.receivecount = receivecount;
	}
	public Integer getDealcount() {
		return dealcount;
	}
	public void setDealcount(Integer dealcount) {
		this.dealcount = dealcount;
	}
}
