package zy.dto.batch.money;

import zy.entity.batch.client.T_Batch_Client;

public class ClientBackAnalysisDto extends T_Batch_Client{
	private double init_money;
	private double sell_money;
	private double return_money;
	private double fee_money;
	private double settle_money;
	private double discount_money;
	private double prepay;
	private double receivable;
	private double received;
	private String nosenddays;
	public double getInit_money() {
		return init_money;
	}
	public void setInit_money(double init_money) {
		this.init_money = init_money;
	}
	public double getSell_money() {
		return sell_money;
	}
	public void setSell_money(double sell_money) {
		this.sell_money = sell_money;
	}
	public double getReturn_money() {
		return return_money;
	}
	public void setReturn_money(double return_money) {
		this.return_money = return_money;
	}
	public double getFee_money() {
		return fee_money;
	}
	public void setFee_money(double fee_money) {
		this.fee_money = fee_money;
	}
	public double getSettle_money() {
		return settle_money;
	}
	public void setSettle_money(double settle_money) {
		this.settle_money = settle_money;
	}
	public double getDiscount_money() {
		return discount_money;
	}
	public void setDiscount_money(double discount_money) {
		this.discount_money = discount_money;
	}
	public double getPrepay() {
		return prepay;
	}
	public void setPrepay(double prepay) {
		this.prepay = prepay;
	}
	public double getReceivable() {
		return receivable;
	}
	public void setReceivable(double receivable) {
		this.receivable = receivable;
	}
	public double getReceived() {
		return received;
	}
	public void setReceived(double received) {
		this.received = received;
	}
	public String getNosenddays() {
		return nosenddays;
	}
	public void setNosenddays(String nosenddays) {
		this.nosenddays = nosenddays;
	}
}
