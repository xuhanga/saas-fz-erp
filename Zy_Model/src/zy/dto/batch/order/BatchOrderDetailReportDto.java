package zy.dto.batch.order;

import zy.entity.batch.order.T_Batch_OrderList;

public class BatchOrderDetailReportDto extends T_Batch_OrderList{
	private static final long serialVersionUID = 4821671919921859566L;
	private String od_make_date;
	private String od_state;
	private String od_manager;
	private String client_name;
	private String depot_name;
	public String getOd_make_date() {
		return od_make_date;
	}
	public void setOd_make_date(String od_make_date) {
		this.od_make_date = od_make_date;
	}
	public String getOd_state() {
		return od_state;
	}
	public void setOd_state(String od_state) {
		this.od_state = od_state;
	}
	public String getOd_manager() {
		return od_manager;
	}
	public void setOd_manager(String od_manager) {
		this.od_manager = od_manager;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
}
