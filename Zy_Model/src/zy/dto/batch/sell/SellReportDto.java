package zy.dto.batch.sell;

import java.io.Serializable;

public class SellReportDto implements Serializable{
	private static final long serialVersionUID = 4859913238179552849L;
	private Long id;
	private String code;
	private String name;
	private String pd_no;
	private Integer amount;
	private Double money;
	private Integer amount_th;
	private Double money_th;
	private Double sell_money;
	private Double cost_money;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Integer getAmount_th() {
		return amount_th;
	}
	public void setAmount_th(Integer amount_th) {
		this.amount_th = amount_th;
	}
	public Double getMoney_th() {
		return money_th;
	}
	public void setMoney_th(Double money_th) {
		this.money_th = money_th;
	}
	public Double getSell_money() {
		return sell_money;
	}
	public void setSell_money(Double sell_money) {
		this.sell_money = sell_money;
	}
	public Double getCost_money() {
		return cost_money;
	}
	public void setCost_money(Double cost_money) {
		this.cost_money = cost_money;
	}
}
