package zy.dto.buy.enter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class EnterDetailSizeReportDto implements Serializable{
	private static final long serialVersionUID = 5909560089888717083L;
	private Integer id;
	private String cr_code;
	private String cr_name;
	private String br_code;
	private String br_name;
	private String size_amount;
	private String totalamount;
	private Map<String, Integer> amountMap = new HashMap<String, Integer>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getSize_amount() {
		return size_amount;
	}
	public void setSize_amount(String size_amount) {
		this.size_amount = size_amount;
	}
	public String getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}
	public Map<String, Integer> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, Integer> amountMap) {
		this.amountMap = amountMap;
	}
	
}
