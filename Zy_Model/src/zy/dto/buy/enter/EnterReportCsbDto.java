package zy.dto.buy.enter;

import java.io.Serializable;

public class EnterReportCsbDto implements Serializable{
	private static final long serialVersionUID = 2482427116438275567L;
	private Integer id;
	private String code;
	private String name;
	private Integer amount;
	private Double money;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
}
