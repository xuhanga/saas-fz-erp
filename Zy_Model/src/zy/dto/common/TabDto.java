package zy.dto.common;

import java.io.Serializable;

public class TabDto implements Serializable{
	private static final long serialVersionUID = 1266080380744630885L;
	private int id;
	private String name;
	public TabDto(){}
	public TabDto(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
