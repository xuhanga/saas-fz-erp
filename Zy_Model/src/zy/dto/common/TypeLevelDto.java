package zy.dto.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TypeLevelDto implements Serializable{
	private static final long serialVersionUID = 4379109646660386889L;
	private String tp_code;
	private String tp_name;
	private Map<String, Integer> amountMap = new HashMap<String, Integer>();
	private Map<String, Double> moneyMap = new HashMap<String, Double>();
	private Integer totalAmount;
	private Double totalMoney;
	private List<String> child_type = new ArrayList<String>();
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public Map<String, Integer> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, Integer> amountMap) {
		this.amountMap = amountMap;
	}
	public Map<String, Double> getMoneyMap() {
		return moneyMap;
	}
	public void setMoneyMap(Map<String, Double> moneyMap) {
		this.moneyMap = moneyMap;
	}
	public Integer getTotalAmount() {
		if (amountMap != null) {
			int total = 0;
			for (String key : amountMap.keySet()) {
				total += amountMap.get(key);
			}
			return total;
		}
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getTotalMoney() {
		if (moneyMap != null) {
			double total = 0d;
			for (String key : moneyMap.keySet()) {
				total += moneyMap.get(key);
			}
			return total;
		}
		return totalMoney;
	}
	public void setTotalMoney(Double totalMoney) {
		this.totalMoney = totalMoney;
	}
	public List<String> getChild_type() {
		return child_type;
	}
	public void setChild_type(List<String> child_type) {
		this.child_type = child_type;
	}
}
