package zy.dto.money.access;

import zy.entity.money.bank.T_Money_Bank;

public class AccessListDto extends T_Money_Bank{
	private static final long serialVersionUID = -7155331668369315831L;
	private Double acl_money;
	public Double getAcl_money() {
		return acl_money;
	}
	public void setAcl_money(Double acl_money) {
		this.acl_money = acl_money;
	}
}
