package zy.dto.report.kpi;

import java.io.Serializable;

public class KpiAnalysisDto implements Serializable{
	private static final long serialVersionUID = 6230219138717288123L;
	private String id;
	private String shop_code;
	private String shop_name;
	private String project_name;
	private int comeamount;//进店量
	private int receiveamount;//接待人数
	private int tryamount;//试穿人数
	private int dealcount;//成交单数
	private int vipcount;//开卡人数
	private int sellamount;//销售数量
	private double sellmoney;//销售金额
	private double retailmoney;//零售金额
	private double costmoney;//成本
	private double profits;//利润
	private double avg_discount;//平均折扣率
	private double joint_rate;//连带率
	private double sellmoney_vip;//会员消费
	private double sellmoney_vip_proportion;//会员消费占比
	private double avg_price;//平均件单价
	private double avg_sellprice;//平均客单价
	private int cardcount;//新增储值卡个数
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShop_code() {
		return shop_code;
	}
	public void setShop_code(String shop_code) {
		this.shop_code = shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public int getComeamount() {
		return comeamount;
	}
	public void setComeamount(int comeamount) {
		this.comeamount = comeamount;
	}
	public int getReceiveamount() {
		return receiveamount;
	}
	public void setReceiveamount(int receiveamount) {
		this.receiveamount = receiveamount;
	}
	public int getTryamount() {
		return tryamount;
	}
	public void setTryamount(int tryamount) {
		this.tryamount = tryamount;
	}
	public int getDealcount() {
		return dealcount;
	}
	public void setDealcount(int dealcount) {
		this.dealcount = dealcount;
	}
	public int getVipcount() {
		return vipcount;
	}
	public void setVipcount(int vipcount) {
		this.vipcount = vipcount;
	}
	public int getSellamount() {
		return sellamount;
	}
	public void setSellamount(int sellamount) {
		this.sellamount = sellamount;
	}
	public double getSellmoney() {
		return sellmoney;
	}
	public void setSellmoney(double sellmoney) {
		this.sellmoney = sellmoney;
	}
	public double getRetailmoney() {
		return retailmoney;
	}
	public void setRetailmoney(double retailmoney) {
		this.retailmoney = retailmoney;
	}
	public double getCostmoney() {
		return costmoney;
	}
	public void setCostmoney(double costmoney) {
		this.costmoney = costmoney;
	}
	public double getProfits() {
		return profits;
	}
	public void setProfits(double profits) {
		this.profits = profits;
	}
	public double getAvg_discount() {
		return avg_discount;
	}
	public void setAvg_discount(double avg_discount) {
		this.avg_discount = avg_discount;
	}
	public double getJoint_rate() {
		return joint_rate;
	}
	public void setJoint_rate(double joint_rate) {
		this.joint_rate = joint_rate;
	}
	public double getSellmoney_vip() {
		return sellmoney_vip;
	}
	public void setSellmoney_vip(double sellmoney_vip) {
		this.sellmoney_vip = sellmoney_vip;
	}
	public double getSellmoney_vip_proportion() {
		return sellmoney_vip_proportion;
	}
	public void setSellmoney_vip_proportion(double sellmoney_vip_proportion) {
		this.sellmoney_vip_proportion = sellmoney_vip_proportion;
	}
	public double getAvg_price() {
		return avg_price;
	}
	public void setAvg_price(double avg_price) {
		this.avg_price = avg_price;
	}
	public double getAvg_sellprice() {
		return avg_sellprice;
	}
	public void setAvg_sellprice(double avg_sellprice) {
		this.avg_sellprice = avg_sellprice;
	}
	public int getCardcount() {
		return cardcount;
	}
	public void setCardcount(int cardcount) {
		this.cardcount = cardcount;
	}
}
