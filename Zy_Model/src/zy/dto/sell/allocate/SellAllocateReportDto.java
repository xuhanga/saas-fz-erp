package zy.dto.sell.allocate;

import zy.entity.sell.allocate.T_Sell_Allocate;

public class SellAllocateReportDto extends T_Sell_Allocate{
	private static final long serialVersionUID = -6231184782979877922L;
	private Integer acl_id;
	private String acl_pd_code;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private Integer acl_amount;
	private Double acl_sell_price;
	private Double acl_cost_price;
	private Double acl_in_sell_price;
	private Double acl_in_cost_price;
	public Integer getAcl_id() {
		return acl_id;
	}
	public void setAcl_id(Integer acl_id) {
		this.acl_id = acl_id;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Integer getAcl_amount() {
		return acl_amount;
	}
	public void setAcl_amount(Integer acl_amount) {
		this.acl_amount = acl_amount;
	}
	public Double getAcl_sell_price() {
		return acl_sell_price;
	}
	public void setAcl_sell_price(Double acl_sell_price) {
		this.acl_sell_price = acl_sell_price;
	}
	public Double getAcl_in_sell_price() {
		return acl_in_sell_price;
	}
	public void setAcl_in_sell_price(Double acl_in_sell_price) {
		this.acl_in_sell_price = acl_in_sell_price;
	}
	public String getAcl_pd_code() {
		return acl_pd_code;
	}
	public void setAcl_pd_code(String acl_pd_code) {
		this.acl_pd_code = acl_pd_code;
	}
	public Double getAcl_cost_price() {
		return acl_cost_price;
	}
	public void setAcl_cost_price(Double acl_cost_price) {
		this.acl_cost_price = acl_cost_price;
	}
	public Double getAcl_in_cost_price() {
		return acl_in_cost_price;
	}
	public void setAcl_in_cost_price(Double acl_in_cost_price) {
		this.acl_in_cost_price = acl_in_cost_price;
	}
}
