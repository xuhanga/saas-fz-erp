package zy.dto.sell.ecoupon;

import zy.entity.sell.ecoupon.T_Sell_ECoupon;

public class SellECouponDto extends T_Sell_ECoupon{
	private static final long serialVersionUID = -8161035882226823740L;
	private Integer ecl_id;
	private String ecl_code;
	private Double ecl_limitmoney;
	private Double ecl_money;
	private Integer ecl_amount;
	private Integer receive_amount;
	private boolean selected;
	public Integer getEcl_id() {
		return ecl_id;
	}
	public void setEcl_id(Integer ecl_id) {
		this.ecl_id = ecl_id;
	}
	public String getEcl_code() {
		return ecl_code;
	}
	public void setEcl_code(String ecl_code) {
		this.ecl_code = ecl_code;
	}
	public Double getEcl_limitmoney() {
		return ecl_limitmoney;
	}
	public void setEcl_limitmoney(Double ecl_limitmoney) {
		this.ecl_limitmoney = ecl_limitmoney;
	}
	public Double getEcl_money() {
		return ecl_money;
	}
	public void setEcl_money(Double ecl_money) {
		this.ecl_money = ecl_money;
	}
	public Integer getEcl_amount() {
		return ecl_amount;
	}
	public void setEcl_amount(Integer ecl_amount) {
		this.ecl_amount = ecl_amount;
	}
	public Integer getReceive_amount() {
		return receive_amount;
	}
	public void setReceive_amount(Integer receive_amount) {
		this.receive_amount = receive_amount;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
