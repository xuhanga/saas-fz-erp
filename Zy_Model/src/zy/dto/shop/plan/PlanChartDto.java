package zy.dto.shop.plan;

import java.io.Serializable;

public class PlanChartDto implements Serializable{
	private static final long serialVersionUID = -4069599291560119824L;
	private String shop_code;
	private String shop_name;
	private Integer month;
	private Double sell_money_plan;
	private Double sell_money_real;
	public String getShop_code() {
		return shop_code;
	}
	public void setShop_code(String shop_code) {
		this.shop_code = shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Double getSell_money_plan() {
		return sell_money_plan;
	}
	public void setSell_money_plan(Double sell_money_plan) {
		this.sell_money_plan = sell_money_plan;
	}
	public Double getSell_money_real() {
		return sell_money_real;
	}
	public void setSell_money_real(Double sell_money_real) {
		this.sell_money_real = sell_money_real;
	}
}
