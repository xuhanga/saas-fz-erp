package zy.dto.shop.want;

import java.io.Serializable;

public class WantReportDto implements Serializable{
	private static final long serialVersionUID = -6812390993169273710L;
	private Long id;
	private String code;
	private String name;
	private String pd_no;
	private String indp_code;
	private String indp_name;
	private Integer apply_amount;
	private Integer send_amount;
	private Integer apply_amount_th;
	private Integer send_amount_th;
	private Double apply_money;
	private Double send_money;
	private Double send_cost;
	private Double apply_money_th;
	private Double send_money_th;
	private Double send_cost_th;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getIndp_code() {
		return indp_code;
	}
	public void setIndp_code(String indp_code) {
		this.indp_code = indp_code;
	}
	public String getIndp_name() {
		return indp_name;
	}
	public void setIndp_name(String indp_name) {
		this.indp_name = indp_name;
	}
	public Integer getApply_amount() {
		return apply_amount;
	}
	public void setApply_amount(Integer apply_amount) {
		this.apply_amount = apply_amount;
	}
	public Integer getSend_amount() {
		return send_amount;
	}
	public void setSend_amount(Integer send_amount) {
		this.send_amount = send_amount;
	}
	public Integer getApply_amount_th() {
		return apply_amount_th;
	}
	public void setApply_amount_th(Integer apply_amount_th) {
		this.apply_amount_th = apply_amount_th;
	}
	public Integer getSend_amount_th() {
		return send_amount_th;
	}
	public void setSend_amount_th(Integer send_amount_th) {
		this.send_amount_th = send_amount_th;
	}
	public Double getApply_money() {
		return apply_money;
	}
	public void setApply_money(Double apply_money) {
		this.apply_money = apply_money;
	}
	public Double getSend_money() {
		return send_money;
	}
	public void setSend_money(Double send_money) {
		this.send_money = send_money;
	}
	public Double getSend_cost() {
		return send_cost;
	}
	public void setSend_cost(Double send_cost) {
		this.send_cost = send_cost;
	}
	public Double getApply_money_th() {
		return apply_money_th;
	}
	public void setApply_money_th(Double apply_money_th) {
		this.apply_money_th = apply_money_th;
	}
	public Double getSend_money_th() {
		return send_money_th;
	}
	public void setSend_money_th(Double send_money_th) {
		this.send_money_th = send_money_th;
	}
	public Double getSend_cost_th() {
		return send_cost_th;
	}
	public void setSend_cost_th(Double send_cost_th) {
		this.send_cost_th = send_cost_th;
	}
}
