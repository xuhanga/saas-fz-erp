package zy.dto.sqb;

import java.io.Serializable;

/**
 * 收钱吧支付接口请求参数
 *
 */
public class PayReq implements Serializable{
	private static final long serialVersionUID = -6481542193893882077L;
	private String terminal_sn;//终端号
	private String terminal_key;//终端密钥
	private String client_sn;//商户系统订单号
	private double money;//交易总金额,以元为单位
	private String total_amount;//交易总金额,以分为单位
	private String payway;//支付方式
	private String dynamic_id;//条码内容
	private String subject;//交易简介
	private String operator;//门店操作员
	private String description;//商品详情
	private String longitude;//经度
	private String latitude;//维度
	private String device_id;//设备指纹
	private String extended;//扩展参数集合	JSON map
	private String goods_details;//商品详情	JSON
	private String reflect;//反射参数
	private String notify_url;//回调
	public String getTerminal_sn() {
		return terminal_sn;
	}
	public void setTerminal_sn(String terminal_sn) {
		this.terminal_sn = terminal_sn;
	}
	public String getTerminal_key() {
		return terminal_key;
	}
	public void setTerminal_key(String terminal_key) {
		this.terminal_key = terminal_key;
	}
	public String getClient_sn() {
		return client_sn;
	}
	public void setClient_sn(String client_sn) {
		this.client_sn = client_sn;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public String getTotal_amount() {
		return String.format("%.0f", 100 * money);
	}
	public String getPayway() {
		return payway;
	}
	public void setPayway(String payway) {
		this.payway = payway;
	}
	public String getDynamic_id() {
		return dynamic_id;
	}
	public void setDynamic_id(String dynamic_id) {
		this.dynamic_id = dynamic_id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getExtended() {
		return extended;
	}
	public void setExtended(String extended) {
		this.extended = extended;
	}
	public String getGoods_details() {
		return goods_details;
	}
	public void setGoods_details(String goods_details) {
		this.goods_details = goods_details;
	}
	public String getReflect() {
		return reflect;
	}
	public void setReflect(String reflect) {
		this.reflect = reflect;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
}
