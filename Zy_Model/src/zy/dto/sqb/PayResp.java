package zy.dto.sqb;

import java.io.Serializable;

public class PayResp implements Serializable{
	private static final long serialVersionUID = 8400432184266770368L;
	private String result_code;//结果码
	private String error_code;//错误码
	private String error_message;//错误消息
	private String terminal_sn;//终端号
	private String sn;//收钱吧唯一订单号
	private String client_sn;//商户订单号
	private String client_tsn;
	private String trade_no;//支付服务商订单号
	private String status;//流水状态
	private String order_status;//订单状态
	private String payway;//一级支付方式
	private String sub_payway;//二级支付方式
	private String payer_uid;//付款人ID
	private String payer_login;//付款人账号
	private String total_amount;//交易总额
	private String net_amount;//实收金额
	private String subject;//交易概述
	private String finish_time;//付款动作在收钱吧的完成时间
	private String channel_finish_time;//付款动作在支付服务商的完成时间
	private String operator;//操作员
	private String reflect;//反射参数
	private String payment_list;//活动优惠
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	public String getTerminal_sn() {
		return terminal_sn;
	}
	public void setTerminal_sn(String terminal_sn) {
		this.terminal_sn = terminal_sn;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getClient_sn() {
		return client_sn;
	}
	public void setClient_sn(String client_sn) {
		this.client_sn = client_sn;
	}
	public String getClient_tsn() {
		return client_tsn;
	}
	public void setClient_tsn(String client_tsn) {
		this.client_tsn = client_tsn;
	}
	public String getTrade_no() {
		return trade_no;
	}
	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrder_status() {
		return order_status;
	}
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	public String getPayway() {
		return payway;
	}
	public void setPayway(String payway) {
		this.payway = payway;
	}
	public String getSub_payway() {
		return sub_payway;
	}
	public void setSub_payway(String sub_payway) {
		this.sub_payway = sub_payway;
	}
	public String getPayer_uid() {
		return payer_uid;
	}
	public void setPayer_uid(String payer_uid) {
		this.payer_uid = payer_uid;
	}
	public String getPayer_login() {
		return payer_login;
	}
	public void setPayer_login(String payer_login) {
		this.payer_login = payer_login;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	public String getNet_amount() {
		return net_amount;
	}
	public void setNet_amount(String net_amount) {
		this.net_amount = net_amount;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getFinish_time() {
		return finish_time;
	}
	public void setFinish_time(String finish_time) {
		this.finish_time = finish_time;
	}
	public String getChannel_finish_time() {
		return channel_finish_time;
	}
	public void setChannel_finish_time(String channel_finish_time) {
		this.channel_finish_time = channel_finish_time;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getReflect() {
		return reflect;
	}
	public void setReflect(String reflect) {
		this.reflect = reflect;
	}
	public String getPayment_list() {
		return payment_list;
	}
	public void setPayment_list(String payment_list) {
		this.payment_list = payment_list;
	}
}
