package zy.dto.stock.allocate;

import java.io.Serializable;

public class StockAllocateReportDto implements Serializable{
	private static final long serialVersionUID = -7906531757811227702L;
	private String id;
	private String dp_name;
	private String code;
	private String name;
	private String pd_no;
	private Integer in_amount;
	private Integer out_amount;
	private Double in_money;
	private Double out_money;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDp_name() {
		return dp_name;
	}
	public void setDp_name(String dp_name) {
		this.dp_name = dp_name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public Integer getIn_amount() {
		return in_amount;
	}
	public void setIn_amount(Integer in_amount) {
		this.in_amount = in_amount;
	}
	public Integer getOut_amount() {
		return out_amount;
	}
	public void setOut_amount(Integer out_amount) {
		this.out_amount = out_amount;
	}
	public Double getIn_money() {
		return in_money;
	}
	public void setIn_money(Double in_money) {
		this.in_money = in_money;
	}
	public Double getOut_money() {
		return out_money;
	}
	public void setOut_money(Double out_money) {
		this.out_money = out_money;
	}
}
