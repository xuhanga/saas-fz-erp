package zy.dto.stock.data;

import java.io.Serializable;

public class ProductSingleTrackDto implements Serializable{
	private static final long serialVersionUID = -3149561533870619198L;
	private String dp_code;
	private String dp_name;
	private String pd_code;
	private String pd_no;
	private String pd_name;
	private String cr_code;
	private String sz_code;
	private String br_code;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_name;
	private String tp_name;
	private String sub_code;
	private String dl_date;
	private String dl_type;
	private String dl_number;
	private Integer dl_in_amount;
	private Integer dl_out_amount;
	public String getDp_code() {
		return dp_code;
	}
	public void setDp_code(String dp_code) {
		this.dp_code = dp_code;
	}
	public String getDp_name() {
		return dp_name;
	}
	public void setDp_name(String dp_name) {
		this.dp_name = dp_name;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getSz_code() {
		return sz_code;
	}
	public void setSz_code(String sz_code) {
		this.sz_code = sz_code;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getSub_code() {
		return sub_code;
	}
	public void setSub_code(String sub_code) {
		this.sub_code = sub_code;
	}
	public String getDl_date() {
		return dl_date;
	}
	public void setDl_date(String dl_date) {
		this.dl_date = dl_date;
	}
	public String getDl_type() {
		return dl_type;
	}
	public void setDl_type(String dl_type) {
		this.dl_type = dl_type;
	}
	public String getDl_number() {
		return dl_number;
	}
	public void setDl_number(String dl_number) {
		this.dl_number = dl_number;
	}
	public Integer getDl_in_amount() {
		return dl_in_amount;
	}
	public void setDl_in_amount(Integer dl_in_amount) {
		this.dl_in_amount = dl_in_amount;
	}
	public Integer getDl_out_amount() {
		return dl_out_amount;
	}
	public void setDl_out_amount(Integer dl_out_amount) {
		this.dl_out_amount = dl_out_amount;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
}
