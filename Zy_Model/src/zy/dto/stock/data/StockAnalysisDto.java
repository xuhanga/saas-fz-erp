package zy.dto.stock.data;

import java.io.Serializable;

public class StockAnalysisDto implements Serializable{
	private static final long serialVersionUID = 8322339610929122213L;
	private String code;
	private String name;
	private Integer amount;
	private Double cost_money;
	private Double sell_money;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Double getCost_money() {
		return cost_money;
	}
	public void setCost_money(Double cost_money) {
		this.cost_money = cost_money;
	}
	public Double getSell_money() {
		return sell_money;
	}
	public void setSell_money(Double sell_money) {
		this.sell_money = sell_money;
	}
}
