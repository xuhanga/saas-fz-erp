package zy.dto.stock.data;

import java.util.HashMap;
import java.util.Map;

import zy.entity.stock.data.T_Stock_Data;

public class StockDataByDepotDto extends T_Stock_Data{
	private static final long serialVersionUID = 7970051661908813002L;
	private String depot_amount;
	private Map<String, Integer> amountMap = new HashMap<String, Integer>();
	public String getDepot_amount() {
		return depot_amount;
	}
	public void setDepot_amount(String depot_amount) {
		this.depot_amount = depot_amount;
	}
	public Map<String, Integer> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, Integer> amountMap) {
		this.amountMap = amountMap;
	}
}
