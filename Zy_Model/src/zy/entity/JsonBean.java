package zy.entity;

public class JsonBean {
	public JsonBean() {
	}
	private int stat;
    private Object data;
    private String message;

    public int getStat() {
        return stat;
    }

    public void setStat(int stat) {
        this.stat = stat;
    }

	public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
