package zy.entity;

/**
*/

import java.util.List;
import java.util.Map;

public class PageData<T>{

	private List<T> list;
	private PageInfo pageInfo;
	private int pageNumber = 10;
	private Map<String,Object> data;
	private String headData;
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pagenumber) {
		this.pageNumber = pagenumber;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	public String getHeadData() {
		return headData;
	}
	public void setHeadData(String headData) {
		this.headData = headData;
	}
}

