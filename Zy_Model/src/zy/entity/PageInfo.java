package zy.entity;
/**
 */
public class PageInfo {
	
	private int totalRow;	
	private int totalPage;	
	private int currentPage = 1; 
	private int pageSize = 10;   
	private boolean hasPrevious;
	private boolean hasNext;	
	private boolean bof;		
	private boolean eof;		
    
	/**
	 * @param totalRow
	 * @param pageSize
	 * @param pageNum 
     */
	public PageInfo(int totalRow, int pageSize, int pageNum) {
		if(pageSize < 1){
			pageSize = 1;
		}
		if(pageNum < 1){
			pageNum = 1;
		}
		this.totalRow = totalRow;
		this.pageSize = pageSize;
		this.totalPage = countTotalPage(this.pageSize, this.totalRow);
		setCurrentPage(pageNum);
		init();
	}
	
	public PageInfo(){}
	public int getTotalRow(){
		return totalRow;
	}
	
	public int getTotalPage() {
		return totalPage;
	}
	public int getCurrentPage() {
		return this.currentPage;
	}
	public void setCurrentPage(int currentPage) {
        if(currentPage>this.totalPage){
        	this.currentPage=this.totalPage;        	
        }else if (currentPage<1){
        	this.currentPage=1;
        }
        else{
        	this.currentPage=currentPage;
        }
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getBeginIndex() {
		int beginIndex = (currentPage - 1) * pageSize; 
		return beginIndex;
	}
	public int countTotalPage(int pageSize, int totalRow) {
		int totalPage = totalRow % pageSize == 0 ? totalRow / pageSize : totalRow / pageSize + 1;
		return totalPage;
	}
	public int getNextPage() {
		if (currentPage + 1 >= this.totalPage) { 
			return this.totalPage;
		}
		return currentPage + 1; 
	}
	public int getPreviousPage() {
		if (currentPage - 1 <= 1) {
			return 1;
		} else {
			return currentPage - 1;
		}
	}
	public boolean isHasPrevious() {
		return hasPrevious;
	}
	public boolean isHasNext() {
		return hasNext;
	}
	public boolean isBof() {
		return bof;
	}
	public boolean isEof() {
		return eof;
	}
	public boolean hasNext() {
		return currentPage < this.totalPage;
	}
	public boolean hasPrevious() {
		return currentPage > 1;
	}
	public boolean isFirst() {
		return currentPage == 1;
	}
	public boolean isLast() {
		return currentPage >= this.totalPage;
	}
	// ��ʼ����Ϣ
	private void init() {
		this.hasNext = hasNext();
		this.hasPrevious = hasPrevious();
		this.bof = isFirst();
		this.eof = isLast();
	}
}
