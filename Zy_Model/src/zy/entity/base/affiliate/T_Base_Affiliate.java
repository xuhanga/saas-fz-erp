package zy.entity.base.affiliate;

import java.io.Serializable;

public class T_Base_Affiliate implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer af_id;
	private String af_code;
	private String af_name;
	private String af_upcode;
	private String af_addr;
	private String af_man;
	private String af_tel;
	private String af_mobile;
	private String af_remark;
	private Integer companyid;
	public Integer getAf_id() {
		return af_id;
	}
	public void setAf_id(Integer af_id) {
		this.af_id = af_id;
	}
	public String getAf_code() {
		return af_code;
	}
	public void setAf_code(String af_code) {
		this.af_code = af_code;
	}
	public String getAf_name() {
		return af_name;
	}
	public void setAf_name(String af_name) {
		this.af_name = af_name;
	}
	public String getAf_upcode() {
		return af_upcode;
	}
	public void setAf_upcode(String af_upcode) {
		this.af_upcode = af_upcode;
	}
	public String getAf_addr() {
		return af_addr;
	}
	public void setAf_addr(String af_addr) {
		this.af_addr = af_addr;
	}
	public String getAf_man() {
		return af_man;
	}
	public void setAf_man(String af_man) {
		this.af_man = af_man;
	}
	public String getAf_tel() {
		return af_tel;
	}
	public void setAf_tel(String af_tel) {
		this.af_tel = af_tel;
	}
	public String getAf_mobile() {
		return af_mobile;
	}
	public void setAf_mobile(String af_mobile) {
		this.af_mobile = af_mobile;
	}
	public String getAf_remark() {
		return af_remark;
	}
	public void setAf_remark(String af_remark) {
		this.af_remark = af_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
