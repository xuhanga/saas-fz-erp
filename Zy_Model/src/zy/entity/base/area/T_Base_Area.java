package zy.entity.base.area;

import java.io.Serializable;

public class T_Base_Area implements Serializable{
	private static final long serialVersionUID = 2199037318586704805L;
	private Integer ar_id;
	private String ar_code;
	private String ar_name;
	private String ar_upcode;
	private String ar_spell;
	private Integer companyid;
	public Integer getAr_id() {
		return ar_id;
	}
	public void setAr_id(Integer ar_id) {
		this.ar_id = ar_id;
	}
	public String getAr_code() {
		return ar_code;
	}
	public void setAr_code(String ar_code) {
		this.ar_code = ar_code;
	}
	public String getAr_name() {
		return ar_name;
	}
	public void setAr_name(String ar_name) {
		this.ar_name = ar_name;
	}
	public String getAr_upcode() {
		return ar_upcode;
	}
	public void setAr_upcode(String ar_upcode) {
		this.ar_upcode = ar_upcode;
	}
	public String getAr_spell() {
		return ar_spell;
	}
	public void setAr_spell(String ar_spell) {
		this.ar_spell = ar_spell;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
