package zy.entity.base.bra;

import java.io.Serializable;

public class T_Base_Bra implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer br_id;
	private String br_code;
	private String br_name;
	private Integer companyid;

	public Integer getBr_id() {
		return br_id;
	}

	public void setBr_id(Integer br_id) {
		this.br_id = br_id;
	}

	public String getBr_code() {
		return br_code;
	}

	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}

	public String getBr_name() {
		return br_name;
	}

	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
