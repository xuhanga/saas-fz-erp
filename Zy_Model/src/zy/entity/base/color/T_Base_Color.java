package zy.entity.base.color;

import java.io.Serializable;

public class T_Base_Color implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer cr_id;
	private String cr_code;
	private String cr_name;
	private String cr_spell;
	private Integer companyid;
	public Integer getCr_id() {
		return cr_id;
	}

	public void setCr_id(Integer cr_id) {
		this.cr_id = cr_id;
	}

	public String getCr_code() {
		return cr_code;
	}

	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}

	public String getCr_name() {
		return cr_name;
	}

	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}

	public String getCr_spell() {
		return cr_spell;
	}

	public void setCr_spell(String cr_spell) {
		this.cr_spell = cr_spell;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
