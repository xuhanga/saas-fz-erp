package zy.entity.base.depot;

import java.io.Serializable;

public class T_Base_Depot implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer dp_id;
	private String dp_code;
	private String dp_name;
	private String dp_spell;
	private String dp_man;
	private String dp_tel;
	private String dp_mobile;
	private String dp_addr;
	private Integer dp_default;
	private Integer dp_state;
	private String dp_remark;
	private String dp_shop_code;
	private String shop_name;
	private String ty_name;
	private Integer companyid;
	public Integer getDp_id() {
		return dp_id;
	}
	public void setDp_id(Integer dp_id) {
		this.dp_id = dp_id;
	}
	public String getDp_code() {
		return dp_code;
	}
	public void setDp_code(String dp_code) {
		this.dp_code = dp_code;
	}
	public String getDp_name() {
		return dp_name;
	}
	public void setDp_name(String dp_name) {
		this.dp_name = dp_name;
	}
	public String getDp_spell() {
		return dp_spell;
	}
	public void setDp_spell(String dp_spell) {
		this.dp_spell = dp_spell;
	}
	public String getDp_man() {
		return dp_man;
	}
	public void setDp_man(String dp_man) {
		this.dp_man = dp_man;
	}
	public String getDp_tel() {
		return dp_tel;
	}
	public void setDp_tel(String dp_tel) {
		this.dp_tel = dp_tel;
	}
	public String getDp_mobile() {
		return dp_mobile;
	}
	public void setDp_mobile(String dp_mobile) {
		this.dp_mobile = dp_mobile;
	}
	public String getDp_addr() {
		return dp_addr;
	}
	public void setDp_addr(String dp_addr) {
		this.dp_addr = dp_addr;
	}
	public Integer getDp_default() {
		return dp_default;
	}
	public void setDp_default(Integer dp_default) {
		this.dp_default = dp_default;
	}
	public Integer getDp_state() {
		return dp_state;
	}
	public void setDp_state(Integer dp_state) {
		this.dp_state = dp_state;
	}
	public String getDp_remark() {
		return dp_remark;
	}
	public void setDp_remark(String dp_remark) {
		this.dp_remark = dp_remark;
	}
	public String getDp_shop_code() {
		return dp_shop_code;
	}
	public void setDp_shop_code(String dp_shop_code) {
		this.dp_shop_code = dp_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getTy_name() {
		return ty_name;
	}
	public void setTy_name(String ty_name) {
		this.ty_name = ty_name;
	}
}
