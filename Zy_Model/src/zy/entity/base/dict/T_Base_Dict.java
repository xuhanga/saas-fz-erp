package zy.entity.base.dict;

import java.io.Serializable;

public class T_Base_Dict implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer dt_id;
	private String dt_code;
	private String dt_name;
	public Integer getDt_id() {
		return dt_id;
	}
	public void setDt_id(Integer dt_id) {
		this.dt_id = dt_id;
	}
	public String getDt_code() {
		return dt_code;
	}
	public void setDt_code(String dt_code) {
		this.dt_code = dt_code;
	}
	public String getDt_name() {
		return dt_name;
	}
	public void setDt_name(String dt_name) {
		this.dt_name = dt_name;
	}

}
