package zy.entity.base.dict;

import java.io.Serializable;

public class T_Base_DictList implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer dtl_id;
	private String dtl_code;
	private String dtl_upcode;
	private String dtl_name;
	private Integer companyid;

	public Integer getDtl_id() {
		return dtl_id;
	}

	public void setDtl_id(Integer dtl_id) {
		this.dtl_id = dtl_id;
	}

	public String getDtl_code() {
		return dtl_code;
	}

	public void setDtl_code(String dtl_code) {
		this.dtl_code = dtl_code;
	}

	public String getDtl_upcode() {
		return dtl_upcode;
	}

	public void setDtl_upcode(String dtl_upcode) {
		this.dtl_upcode = dtl_upcode;
	}

	public String getDtl_name() {
		return dtl_name;
	}

	public void setDtl_name(String dtl_name) {
		this.dtl_name = dtl_name;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
