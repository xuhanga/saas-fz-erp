package zy.entity.base.emp;

import java.io.Serializable;

public class T_Base_EmpGroup implements Serializable{
	private static final long serialVersionUID = -7657499359259912013L;
	private Integer eg_id;
	private String eg_code;
	private String eg_name;
	private String eg_remark;
	private String eg_shop_code;
	private Integer companyid;
	public Integer getEg_id() {
		return eg_id;
	}
	public void setEg_id(Integer eg_id) {
		this.eg_id = eg_id;
	}
	public String getEg_code() {
		return eg_code;
	}
	public void setEg_code(String eg_code) {
		this.eg_code = eg_code;
	}
	public String getEg_name() {
		return eg_name;
	}
	public void setEg_name(String eg_name) {
		this.eg_name = eg_name;
	}
	public String getEg_remark() {
		return eg_remark;
	}
	public void setEg_remark(String eg_remark) {
		this.eg_remark = eg_remark;
	}
	public String getEg_shop_code() {
		return eg_shop_code;
	}
	public void setEg_shop_code(String eg_shop_code) {
		this.eg_shop_code = eg_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
