package zy.entity.base.emp;

import java.io.Serializable;

public class T_Base_EmpGroupList implements Serializable{
	private static final long serialVersionUID = -8829455124220284378L;
	private Integer egl_id;
	private String egl_eg_code;
	private String egl_em_code;
	private String em_name;
	private Integer companyid;
	public Integer getEgl_id() {
		return egl_id;
	}
	public void setEgl_id(Integer egl_id) {
		this.egl_id = egl_id;
	}
	public String getEgl_eg_code() {
		return egl_eg_code;
	}
	public void setEgl_eg_code(String egl_eg_code) {
		this.egl_eg_code = egl_eg_code;
	}
	public String getEgl_em_code() {
		return egl_em_code;
	}
	public void setEgl_em_code(String egl_em_code) {
		this.egl_em_code = egl_em_code;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
