package zy.entity.base.emp;

import java.io.Serializable;

public class T_Base_Emp_Reward implements Serializable{
	private static final long serialVersionUID = -6362635379098822510L;
	private Integer er_id;
	private String er_em_code;
	private String er_rw_code;
	private String er_sysdate;
	private Integer er_state;
	private String er_remark;
	private Integer companyid;
	private String rw_name;
	private String rw_icon;
	private String rw_style;
	private Integer reward_count;
	private String em_name;
	public Integer getEr_id() {
		return er_id;
	}
	public void setEr_id(Integer er_id) {
		this.er_id = er_id;
	}
	public String getEr_em_code() {
		return er_em_code;
	}
	public void setEr_em_code(String er_em_code) {
		this.er_em_code = er_em_code;
	}
	public String getEr_rw_code() {
		return er_rw_code;
	}
	public void setEr_rw_code(String er_rw_code) {
		this.er_rw_code = er_rw_code;
	}
	public String getEr_sysdate() {
		return er_sysdate;
	}
	public void setEr_sysdate(String er_sysdate) {
		this.er_sysdate = er_sysdate;
	}
	public Integer getEr_state() {
		return er_state;
	}
	public void setEr_state(Integer er_state) {
		this.er_state = er_state;
	}
	public String getEr_remark() {
		return er_remark;
	}
	public void setEr_remark(String er_remark) {
		this.er_remark = er_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getRw_name() {
		return rw_name;
	}
	public void setRw_name(String rw_name) {
		this.rw_name = rw_name;
	}
	public String getRw_icon() {
		return rw_icon;
	}
	public void setRw_icon(String rw_icon) {
		this.rw_icon = rw_icon;
	}
	public String getRw_style() {
		return rw_style;
	}
	public void setRw_style(String rw_style) {
		this.rw_style = rw_style;
	}
	public Integer getReward_count() {
		return reward_count;
	}
	public void setReward_count(Integer reward_count) {
		this.reward_count = reward_count;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
}
