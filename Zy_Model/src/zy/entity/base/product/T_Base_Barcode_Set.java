package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Barcode_Set implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer bs_id;
	private Double bs_paper_width;
	private Double bs_paper_height;
	private Double bs_top_backgauge;
	private Double bs_left_backgauge;
	private String bs_bartype;
	private Integer bs_line;
	private Double bs_hight_spacing;
	private Double bs_width_spacing;
	private Double bs_width;
	private Double bs_height;
	private Double bs_p_space;
	private Double bs_ps_space;
	private String bs_font_name;
	private Integer bs_font_size;
	private String bs_bold;
	private Integer bs_pdname_ifshow;
	private Integer bs_pdname_row;
	private Integer bs_class_ifshow;
	private Integer bs_class_row;
	private Integer bs_safestandard_ifshow;
	private Integer bs_safestandard_row;
	private Integer bs_filling_ifshow;
	private Integer bs_filling_row;
	private Integer bs_cr_ifshow;
	private Integer bs_cr_row;
	private Integer bs_pdno_ifshow;
	private Integer bs_pdno_row;
	private Integer bs_sz_ifshow;
	private Integer bs_sz_row;
	private Integer bs_bs_ifshow;
	private Integer bs_bs_row;
	private Integer bs_bd_ifshow;
	private Integer bs_bd_row;
	private Integer bs_price_ifshow;
	private Integer bs_price_row;
	private String bs_show_fiename;
	private String bs_sysdate;
	private String bs_plan_name;
	private Integer bs_plan_state;
	private Integer bs_subcode_showup;
	private Integer bs_price_showdown;
	private Integer bs_executionstandard_ifshow;
	private Integer bs_executionstandard_row;
	private Integer bs_language;
	private Integer bs_vipprice_ifshow;
	private Integer bs_vipprice_row;
	private Integer bs_signprice_ifshow;
	private Integer bs_signprice_row;
	private Integer bs_fabric_ifshow;
	private Integer bs_fabric_row;
	private Integer bs_infabric_ifshow;
	private Integer bs_infabric_row;
	private Integer bs_type_ifshow;
	private Integer bs_type_row;
	private Integer bs_place_ifshow;
	private Integer bs_place_row;
	private Integer bs_washexplain_ifshow;
	private Integer bs_washexplain_row;
	private Integer companyid;
	public Integer getBs_id() {
		return bs_id;
	}
	public void setBs_id(Integer bs_id) {
		this.bs_id = bs_id;
	}
	public Double getBs_paper_width() {
		return bs_paper_width;
	}
	public void setBs_paper_width(Double bs_paper_width) {
		this.bs_paper_width = bs_paper_width;
	}
	public Double getBs_paper_height() {
		return bs_paper_height;
	}
	public void setBs_paper_height(Double bs_paper_height) {
		this.bs_paper_height = bs_paper_height;
	}
	public Double getBs_top_backgauge() {
		return bs_top_backgauge;
	}
	public void setBs_top_backgauge(Double bs_top_backgauge) {
		this.bs_top_backgauge = bs_top_backgauge;
	}
	public Double getBs_left_backgauge() {
		return bs_left_backgauge;
	}
	public void setBs_left_backgauge(Double bs_left_backgauge) {
		this.bs_left_backgauge = bs_left_backgauge;
	}
	public String getBs_bartype() {
		return bs_bartype;
	}
	public void setBs_bartype(String bs_bartype) {
		this.bs_bartype = bs_bartype;
	}
	public Integer getBs_line() {
		return bs_line;
	}
	public void setBs_line(Integer bs_line) {
		this.bs_line = bs_line;
	}
	public Double getBs_hight_spacing() {
		return bs_hight_spacing;
	}
	public void setBs_hight_spacing(Double bs_hight_spacing) {
		this.bs_hight_spacing = bs_hight_spacing;
	}
	public Double getBs_width_spacing() {
		return bs_width_spacing;
	}
	public void setBs_width_spacing(Double bs_width_spacing) {
		this.bs_width_spacing = bs_width_spacing;
	}
	public Double getBs_width() {
		return bs_width;
	}
	public void setBs_width(Double bs_width) {
		this.bs_width = bs_width;
	}
	public Double getBs_height() {
		return bs_height;
	}
	public void setBs_height(Double bs_height) {
		this.bs_height = bs_height;
	}
	public Double getBs_p_space() {
		return bs_p_space;
	}
	public void setBs_p_space(Double bs_p_space) {
		this.bs_p_space = bs_p_space;
	}
	public Double getBs_ps_space() {
		return bs_ps_space;
	}
	public void setBs_ps_space(Double bs_ps_space) {
		this.bs_ps_space = bs_ps_space;
	}
	public String getBs_font_name() {
		return bs_font_name;
	}
	public void setBs_font_name(String bs_font_name) {
		this.bs_font_name = bs_font_name;
	}
	public Integer getBs_font_size() {
		return bs_font_size;
	}
	public void setBs_font_size(Integer bs_font_size) {
		this.bs_font_size = bs_font_size;
	}
	public String getBs_bold() {
		return bs_bold;
	}
	public void setBs_bold(String bs_bold) {
		this.bs_bold = bs_bold;
	}
	public Integer getBs_pdname_ifshow() {
		return bs_pdname_ifshow;
	}
	public void setBs_pdname_ifshow(Integer bs_pdname_ifshow) {
		this.bs_pdname_ifshow = bs_pdname_ifshow;
	}
	public Integer getBs_pdname_row() {
		return bs_pdname_row;
	}
	public void setBs_pdname_row(Integer bs_pdname_row) {
		this.bs_pdname_row = bs_pdname_row;
	}
	public Integer getBs_class_ifshow() {
		return bs_class_ifshow;
	}
	public void setBs_class_ifshow(Integer bs_class_ifshow) {
		this.bs_class_ifshow = bs_class_ifshow;
	}
	public Integer getBs_class_row() {
		return bs_class_row;
	}
	public void setBs_class_row(Integer bs_class_row) {
		this.bs_class_row = bs_class_row;
	}
	public Integer getBs_safestandard_ifshow() {
		return bs_safestandard_ifshow;
	}
	public void setBs_safestandard_ifshow(Integer bs_safestandard_ifshow) {
		this.bs_safestandard_ifshow = bs_safestandard_ifshow;
	}
	public Integer getBs_safestandard_row() {
		return bs_safestandard_row;
	}
	public void setBs_safestandard_row(Integer bs_safestandard_row) {
		this.bs_safestandard_row = bs_safestandard_row;
	}
	public Integer getBs_filling_ifshow() {
		return bs_filling_ifshow;
	}
	public void setBs_filling_ifshow(Integer bs_filling_ifshow) {
		this.bs_filling_ifshow = bs_filling_ifshow;
	}
	public Integer getBs_filling_row() {
		return bs_filling_row;
	}
	public void setBs_filling_row(Integer bs_filling_row) {
		this.bs_filling_row = bs_filling_row;
	}
	public Integer getBs_cr_ifshow() {
		return bs_cr_ifshow;
	}
	public void setBs_cr_ifshow(Integer bs_cr_ifshow) {
		this.bs_cr_ifshow = bs_cr_ifshow;
	}
	public Integer getBs_cr_row() {
		return bs_cr_row;
	}
	public void setBs_cr_row(Integer bs_cr_row) {
		this.bs_cr_row = bs_cr_row;
	}
	public Integer getBs_pdno_ifshow() {
		return bs_pdno_ifshow;
	}
	public void setBs_pdno_ifshow(Integer bs_pdno_ifshow) {
		this.bs_pdno_ifshow = bs_pdno_ifshow;
	}
	public Integer getBs_pdno_row() {
		return bs_pdno_row;
	}
	public void setBs_pdno_row(Integer bs_pdno_row) {
		this.bs_pdno_row = bs_pdno_row;
	}
	public Integer getBs_sz_ifshow() {
		return bs_sz_ifshow;
	}
	public void setBs_sz_ifshow(Integer bs_sz_ifshow) {
		this.bs_sz_ifshow = bs_sz_ifshow;
	}
	public Integer getBs_sz_row() {
		return bs_sz_row;
	}
	public void setBs_sz_row(Integer bs_sz_row) {
		this.bs_sz_row = bs_sz_row;
	}
	public Integer getBs_bs_ifshow() {
		return bs_bs_ifshow;
	}
	public void setBs_bs_ifshow(Integer bs_bs_ifshow) {
		this.bs_bs_ifshow = bs_bs_ifshow;
	}
	public Integer getBs_bs_row() {
		return bs_bs_row;
	}
	public void setBs_bs_row(Integer bs_bs_row) {
		this.bs_bs_row = bs_bs_row;
	}
	public Integer getBs_bd_ifshow() {
		return bs_bd_ifshow;
	}
	public void setBs_bd_ifshow(Integer bs_bd_ifshow) {
		this.bs_bd_ifshow = bs_bd_ifshow;
	}
	public Integer getBs_bd_row() {
		return bs_bd_row;
	}
	public void setBs_bd_row(Integer bs_bd_row) {
		this.bs_bd_row = bs_bd_row;
	}
	public Integer getBs_price_ifshow() {
		return bs_price_ifshow;
	}
	public void setBs_price_ifshow(Integer bs_price_ifshow) {
		this.bs_price_ifshow = bs_price_ifshow;
	}
	public Integer getBs_price_row() {
		return bs_price_row;
	}
	public void setBs_price_row(Integer bs_price_row) {
		this.bs_price_row = bs_price_row;
	}
	public String getBs_show_fiename() {
		return bs_show_fiename;
	}
	public void setBs_show_fiename(String bs_show_fiename) {
		this.bs_show_fiename = bs_show_fiename;
	}
	public String getBs_sysdate() {
		return bs_sysdate;
	}
	public void setBs_sysdate(String bs_sysdate) {
		this.bs_sysdate = bs_sysdate;
	}
	public String getBs_plan_name() {
		return bs_plan_name;
	}
	public void setBs_plan_name(String bs_plan_name) {
		this.bs_plan_name = bs_plan_name;
	}
	public Integer getBs_plan_state() {
		return bs_plan_state;
	}
	public void setBs_plan_state(Integer bs_plan_state) {
		this.bs_plan_state = bs_plan_state;
	}
	public Integer getBs_subcode_showup() {
		return bs_subcode_showup;
	}
	public void setBs_subcode_showup(Integer bs_subcode_showup) {
		this.bs_subcode_showup = bs_subcode_showup;
	}
	public Integer getBs_price_showdown() {
		return bs_price_showdown;
	}
	public void setBs_price_showdown(Integer bs_price_showdown) {
		this.bs_price_showdown = bs_price_showdown;
	}
	public Integer getBs_executionstandard_ifshow() {
		return bs_executionstandard_ifshow;
	}
	public void setBs_executionstandard_ifshow(Integer bs_executionstandard_ifshow) {
		this.bs_executionstandard_ifshow = bs_executionstandard_ifshow;
	}
	public Integer getBs_executionstandard_row() {
		return bs_executionstandard_row;
	}
	public void setBs_executionstandard_row(Integer bs_executionstandard_row) {
		this.bs_executionstandard_row = bs_executionstandard_row;
	}
	public Integer getBs_language() {
		return bs_language;
	}
	public void setBs_language(Integer bs_language) {
		this.bs_language = bs_language;
	}
	public Integer getBs_vipprice_ifshow() {
		return bs_vipprice_ifshow;
	}
	public void setBs_vipprice_ifshow(Integer bs_vipprice_ifshow) {
		this.bs_vipprice_ifshow = bs_vipprice_ifshow;
	}
	public Integer getBs_vipprice_row() {
		return bs_vipprice_row;
	}
	public void setBs_vipprice_row(Integer bs_vipprice_row) {
		this.bs_vipprice_row = bs_vipprice_row;
	}
	public Integer getBs_signprice_ifshow() {
		return bs_signprice_ifshow;
	}
	public void setBs_signprice_ifshow(Integer bs_signprice_ifshow) {
		this.bs_signprice_ifshow = bs_signprice_ifshow;
	}
	public Integer getBs_signprice_row() {
		return bs_signprice_row;
	}
	public void setBs_signprice_row(Integer bs_signprice_row) {
		this.bs_signprice_row = bs_signprice_row;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getBs_infabric_ifshow() {
		return bs_infabric_ifshow;
	}
	public void setBs_infabric_ifshow(Integer bs_infabric_ifshow) {
		this.bs_infabric_ifshow = bs_infabric_ifshow;
	}
	public Integer getBs_infabric_row() {
		return bs_infabric_row;
	}
	public void setBs_infabric_row(Integer bs_infabric_row) {
		this.bs_infabric_row = bs_infabric_row;
	}
	public Integer getBs_type_ifshow() {
		return bs_type_ifshow;
	}
	public void setBs_type_ifshow(Integer bs_type_ifshow) {
		this.bs_type_ifshow = bs_type_ifshow;
	}
	public Integer getBs_type_row() {
		return bs_type_row;
	}
	public void setBs_type_row(Integer bs_type_row) {
		this.bs_type_row = bs_type_row;
	}
	public Integer getBs_place_ifshow() {
		return bs_place_ifshow;
	}
	public void setBs_place_ifshow(Integer bs_place_ifshow) {
		this.bs_place_ifshow = bs_place_ifshow;
	}
	public Integer getBs_place_row() {
		return bs_place_row;
	}
	public void setBs_place_row(Integer bs_place_row) {
		this.bs_place_row = bs_place_row;
	}
	public Integer getBs_fabric_ifshow() {
		return bs_fabric_ifshow;
	}
	public void setBs_fabric_ifshow(Integer bs_fabric_ifshow) {
		this.bs_fabric_ifshow = bs_fabric_ifshow;
	}
	public Integer getBs_fabric_row() {
		return bs_fabric_row;
	}
	public void setBs_fabric_row(Integer bs_fabric_row) {
		this.bs_fabric_row = bs_fabric_row;
	}
	public Integer getBs_washexplain_ifshow() {
		return bs_washexplain_ifshow;
	}
	public void setBs_washexplain_ifshow(Integer bs_washexplain_ifshow) {
		this.bs_washexplain_ifshow = bs_washexplain_ifshow;
	}
	public Integer getBs_washexplain_row() {
		return bs_washexplain_row;
	}
	public void setBs_washexplain_row(Integer bs_washexplain_row) {
		this.bs_washexplain_row = bs_washexplain_row;
	}
}
