package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Product_Br  implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer pdb_id;
	private String pdb_pd_code;
	private String pd_no;
	private String pdb_br_code;
	private String br_name;
	private Integer companyid;
	public Integer getPdb_id() {
		return pdb_id;
	}
	public void setPdb_id(Integer pdb_id) {
		this.pdb_id = pdb_id;
	}
	public String getPdb_pd_code() {
		return pdb_pd_code;
	}
	public void setPdb_pd_code(String pdb_pd_code) {
		this.pdb_pd_code = pdb_pd_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPdb_br_code() {
		return pdb_br_code;
	}
	public void setPdb_br_code(String pdb_br_code) {
		this.pdb_br_code = pdb_br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
