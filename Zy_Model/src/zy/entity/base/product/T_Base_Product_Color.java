package zy.entity.base.product;

import java.io.Serializable;

public class T_Base_Product_Color implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer pdc_id;
	private String pdc_pd_code;
	private String pd_no;
	private String pdc_cr_code;
	private String cr_name;
	private Integer companyid;
	public Integer getPdc_id() {
		return pdc_id;
	}
	public void setPdc_id(Integer pdc_id) {
		this.pdc_id = pdc_id;
	}
	public String getPdc_pd_code() {
		return pdc_pd_code;
	}
	public void setPdc_pd_code(String pdc_pd_code) {
		this.pdc_pd_code = pdc_pd_code;
	}
	public String getPdc_cr_code() {
		return pdc_cr_code;
	}
	public void setPdc_cr_code(String pdc_cr_code) {
		this.pdc_cr_code = pdc_cr_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
