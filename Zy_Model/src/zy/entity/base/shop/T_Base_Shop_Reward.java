package zy.entity.base.shop;

import java.io.Serializable;

public class T_Base_Shop_Reward implements Serializable{
	private static final long serialVersionUID = 8495944361915706614L;
	private Integer sr_id;
	private String sr_sp_code;
	private String sr_rw_code;
	private String sr_sysdate;
	private Integer sr_state;
	private String sr_remark;
	private Integer companyid;
	private String rw_name;
	private String rw_icon;
	private String rw_style;
	private Integer reward_count;
	private String shop_name;
	public Integer getSr_id() {
		return sr_id;
	}
	public void setSr_id(Integer sr_id) {
		this.sr_id = sr_id;
	}
	public String getSr_sp_code() {
		return sr_sp_code;
	}
	public void setSr_sp_code(String sr_sp_code) {
		this.sr_sp_code = sr_sp_code;
	}
	public String getSr_rw_code() {
		return sr_rw_code;
	}
	public void setSr_rw_code(String sr_rw_code) {
		this.sr_rw_code = sr_rw_code;
	}
	public String getSr_sysdate() {
		return sr_sysdate;
	}
	public void setSr_sysdate(String sr_sysdate) {
		this.sr_sysdate = sr_sysdate;
	}
	public Integer getSr_state() {
		return sr_state;
	}
	public void setSr_state(Integer sr_state) {
		this.sr_state = sr_state;
	}
	public String getSr_remark() {
		return sr_remark;
	}
	public void setSr_remark(String sr_remark) {
		this.sr_remark = sr_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getRw_name() {
		return rw_name;
	}
	public void setRw_name(String rw_name) {
		this.rw_name = rw_name;
	}
	public String getRw_icon() {
		return rw_icon;
	}
	public void setRw_icon(String rw_icon) {
		this.rw_icon = rw_icon;
	}
	public String getRw_style() {
		return rw_style;
	}
	public void setRw_style(String rw_style) {
		this.rw_style = rw_style;
	}
	public Integer getReward_count() {
		return reward_count;
	}
	public void setReward_count(Integer reward_count) {
		this.reward_count = reward_count;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
}
