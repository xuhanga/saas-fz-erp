package zy.entity.base.size;

import java.io.Serializable;

public class T_Base_SizeList implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer szl_id;
	private String szl_sz_code;
	private String szl_szg_code;
	private String sz_name;
	private Integer szl_order;
	private Integer companyid;

	public Integer getSzl_id() {
		return szl_id;
	}

	public void setSzl_id(Integer szl_id) {
		this.szl_id = szl_id;
	}

	public String getSzl_szg_code() {
		return szl_szg_code;
	}

	public void setSzl_szg_code(String szl_szg_code) {
		this.szl_szg_code = szl_szg_code;
	}

	public String getSzl_sz_code() {
		return szl_sz_code;
	}

	public void setSzl_sz_code(String szl_sz_code) {
		this.szl_sz_code = szl_sz_code;
	}

	public String getSz_name() {
		return sz_name;
	}

	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}

	public Integer getSzl_order() {
		return szl_order;
	}

	public void setSzl_order(Integer szl_order) {
		this.szl_order = szl_order;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
