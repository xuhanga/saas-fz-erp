package zy.entity.base.stream;

import java.io.Serializable;

public class T_Base_Stream implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer se_id;
	private String se_code;
	private String se_name;
	private String se_spell;
	private String se_man;
	private String se_tel;
	private String se_addr;
	private String se_mobile;
	private Double se_init_debt;
	private Double se_payable;
	private Double se_payabled;
	private String se_sp_code;
	private Integer companyid;
	public Integer getSe_id() {
		return se_id;
	}
	public void setSe_id(Integer se_id) {
		this.se_id = se_id;
	}
	public String getSe_code() {
		return se_code;
	}
	public void setSe_code(String se_code) {
		this.se_code = se_code;
	}
	public String getSe_name() {
		return se_name;
	}
	public void setSe_name(String se_name) {
		this.se_name = se_name;
	}
	public String getSe_spell() {
		return se_spell;
	}
	public void setSe_spell(String se_spell) {
		this.se_spell = se_spell;
	}
	public String getSe_man() {
		return se_man;
	}
	public void setSe_man(String se_man) {
		this.se_man = se_man;
	}
	public String getSe_tel() {
		return se_tel;
	}
	public void setSe_tel(String se_tel) {
		this.se_tel = se_tel;
	}
	public String getSe_addr() {
		return se_addr;
	}
	public void setSe_addr(String se_addr) {
		this.se_addr = se_addr;
	}
	public String getSe_mobile() {
		return se_mobile;
	}
	public void setSe_mobile(String se_mobile) {
		this.se_mobile = se_mobile;
	}
	public Double getSe_init_debt() {
		return se_init_debt;
	}
	public void setSe_init_debt(Double se_init_debt) {
		this.se_init_debt = se_init_debt;
	}
	public Double getSe_payable() {
		return se_payable;
	}
	public void setSe_payable(Double se_payable) {
		this.se_payable = se_payable;
	}
	public Double getSe_payabled() {
		return se_payabled;
	}
	public void setSe_payabled(Double se_payabled) {
		this.se_payabled = se_payabled;
	}
	public String getSe_sp_code() {
		return se_sp_code;
	}
	public void setSe_sp_code(String se_sp_code) {
		this.se_sp_code = se_sp_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
