package zy.entity.base.stream;

import java.io.Serializable;

public class T_Base_Stream_Bill implements Serializable{
	private static final long serialVersionUID = 5310260044006125981L;
	private Integer seb_id;
	private String seb_se_code;
	private String stream_name;
	private String seb_number;
	private String seb_date;
	private Double seb_money;
	private Double seb_discount_money;
	private Double seb_payable;
	private Double seb_payabled;
	private Integer seb_pay_state;
	private String seb_join_number;
	private Integer seb_type;
	private String seb_sysdate;
	private Integer companyid;
	public Integer getSeb_id() {
		return seb_id;
	}
	public void setSeb_id(Integer seb_id) {
		this.seb_id = seb_id;
	}
	public String getSeb_se_code() {
		return seb_se_code;
	}
	public void setSeb_se_code(String seb_se_code) {
		this.seb_se_code = seb_se_code;
	}
	public String getStream_name() {
		return stream_name;
	}
	public void setStream_name(String stream_name) {
		this.stream_name = stream_name;
	}
	public String getSeb_number() {
		return seb_number;
	}
	public void setSeb_number(String seb_number) {
		this.seb_number = seb_number;
	}
	public String getSeb_date() {
		return seb_date;
	}
	public void setSeb_date(String seb_date) {
		this.seb_date = seb_date;
	}
	public Double getSeb_money() {
		return seb_money;
	}
	public void setSeb_money(Double seb_money) {
		this.seb_money = seb_money;
	}
	public Double getSeb_discount_money() {
		return seb_discount_money;
	}
	public void setSeb_discount_money(Double seb_discount_money) {
		this.seb_discount_money = seb_discount_money;
	}
	public Double getSeb_payable() {
		return seb_payable;
	}
	public void setSeb_payable(Double seb_payable) {
		this.seb_payable = seb_payable;
	}
	public Double getSeb_payabled() {
		return seb_payabled;
	}
	public void setSeb_payabled(Double seb_payabled) {
		this.seb_payabled = seb_payabled;
	}
	public Integer getSeb_pay_state() {
		return seb_pay_state;
	}
	public void setSeb_pay_state(Integer seb_pay_state) {
		this.seb_pay_state = seb_pay_state;
	}
	public String getSeb_join_number() {
		return seb_join_number;
	}
	public void setSeb_join_number(String seb_join_number) {
		this.seb_join_number = seb_join_number;
	}
	public Integer getSeb_type() {
		return seb_type;
	}
	public void setSeb_type(Integer seb_type) {
		this.seb_type = seb_type;
	}
	public String getSeb_sysdate() {
		return seb_sysdate;
	}
	public void setSeb_sysdate(String seb_sysdate) {
		this.seb_sysdate = seb_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
