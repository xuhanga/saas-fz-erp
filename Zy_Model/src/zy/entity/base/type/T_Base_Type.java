package zy.entity.base.type;

import java.io.Serializable;

public class T_Base_Type implements Serializable{
	private static final long serialVersionUID = -3063155428478877709L;
	private Integer tp_id;
	private String tp_code;
	private String tp_name;
	private String tp_spell;
	private String tp_upcode;
	private Double tp_rate;
	private Double tp_salerate;
	private Integer companyid;
	
	public Integer getTp_id() {
		return tp_id;
	}
	public void setTp_id(Integer tp_id) {
		this.tp_id = tp_id;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getTp_spell() {
		return tp_spell;
	}
	public void setTp_spell(String tp_spell) {
		this.tp_spell = tp_spell;
	}
	public String getTp_upcode() {
		return tp_upcode;
	}
	public void setTp_upcode(String tp_upcode) {
		this.tp_upcode = tp_upcode;
	}
	public Double getTp_rate() {
		return tp_rate;
	}
	public void setTp_rate(Double tp_rate) {
		this.tp_rate = tp_rate;
	}
	public Double getTp_salerate() {
		return tp_salerate;
	}
	public void setTp_salerate(Double tp_salerate) {
		this.tp_salerate = tp_salerate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
