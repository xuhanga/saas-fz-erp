package zy.entity.batch.client;

import java.io.Serializable;

public class T_Batch_ClientPrice implements Serializable{
	private static final long serialVersionUID = 1075435282045253602L;
	private Integer cp_id;
	private String cp_pd_code;
	private String cp_ci_code;
	private Double cp_price;
	private String cp_sysdate;
	private Integer companyid;
	public Integer getCp_id() {
		return cp_id;
	}
	public void setCp_id(Integer cp_id) {
		this.cp_id = cp_id;
	}
	public String getCp_pd_code() {
		return cp_pd_code;
	}
	public void setCp_pd_code(String cp_pd_code) {
		this.cp_pd_code = cp_pd_code;
	}
	public String getCp_ci_code() {
		return cp_ci_code;
	}
	public void setCp_ci_code(String cp_ci_code) {
		this.cp_ci_code = cp_ci_code;
	}
	public Double getCp_price() {
		return cp_price;
	}
	public void setCp_price(Double cp_price) {
		this.cp_price = cp_price;
	}
	public String getCp_sysdate() {
		return cp_sysdate;
	}
	public void setCp_sysdate(String cp_sysdate) {
		this.cp_sysdate = cp_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
