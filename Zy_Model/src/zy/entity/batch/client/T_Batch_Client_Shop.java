package zy.entity.batch.client;

import java.io.Serializable;

public class T_Batch_Client_Shop implements Serializable{
	private static final long serialVersionUID = 8747271642540284845L;
	private Integer cis_id;
	private String cis_code;
	private String cis_ci_code;
	private String cis_name;
	private String cis_spell;
	private String cis_linkman;
	private String cis_link_tel;
	private String cis_link_mobile;
	private String cis_link_adr;
	private Integer cis_stutas;
	private Integer companyid;
	public Integer getCis_id() {
		return cis_id;
	}
	public void setCis_id(Integer cis_id) {
		this.cis_id = cis_id;
	}
	public String getCis_code() {
		return cis_code;
	}
	public void setCis_code(String cis_code) {
		this.cis_code = cis_code;
	}
	public String getCis_ci_code() {
		return cis_ci_code;
	}
	public void setCis_ci_code(String cis_ci_code) {
		this.cis_ci_code = cis_ci_code;
	}
	public String getCis_name() {
		return cis_name;
	}
	public void setCis_name(String cis_name) {
		this.cis_name = cis_name;
	}
	public String getCis_spell() {
		return cis_spell;
	}
	public void setCis_spell(String cis_spell) {
		this.cis_spell = cis_spell;
	}
	public String getCis_linkman() {
		return cis_linkman;
	}
	public void setCis_linkman(String cis_linkman) {
		this.cis_linkman = cis_linkman;
	}
	public String getCis_link_tel() {
		return cis_link_tel;
	}
	public void setCis_link_tel(String cis_link_tel) {
		this.cis_link_tel = cis_link_tel;
	}
	public String getCis_link_mobile() {
		return cis_link_mobile;
	}
	public void setCis_link_mobile(String cis_link_mobile) {
		this.cis_link_mobile = cis_link_mobile;
	}
	public String getCis_link_adr() {
		return cis_link_adr;
	}
	public void setCis_link_adr(String cis_link_adr) {
		this.cis_link_adr = cis_link_adr;
	}
	public Integer getCis_stutas() {
		return cis_stutas;
	}
	public void setCis_stutas(Integer cis_stutas) {
		this.cis_stutas = cis_stutas;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
