package zy.entity.batch.fee;

import java.io.Serializable;

public class T_Batch_Fee implements Serializable{
	private static final long serialVersionUID = -1189491106154073143L;
	private Integer fe_id;
	private String fe_number;
	private String fe_date;
	private String fe_client_code;
	private String client_name;
	private Double fe_money;
	private Double fe_discount_money;
	private String fe_maker;
	private String fe_manager;
	private Integer fe_ar_state;
	private String fe_ar_date;
	private Integer fe_pay_state;
	private Double fe_receivable;
	private Double fe_received;
	private Double fe_prepay;
	private String fe_remark;
	private String fe_sysdate;
	private Integer fe_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getFe_id() {
		return fe_id;
	}
	public void setFe_id(Integer fe_id) {
		this.fe_id = fe_id;
	}
	public String getFe_number() {
		return fe_number;
	}
	public void setFe_number(String fe_number) {
		this.fe_number = fe_number;
	}
	public String getFe_date() {
		return fe_date;
	}
	public void setFe_date(String fe_date) {
		this.fe_date = fe_date;
	}
	public String getFe_client_code() {
		return fe_client_code;
	}
	public void setFe_client_code(String fe_client_code) {
		this.fe_client_code = fe_client_code;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public Double getFe_money() {
		return fe_money;
	}
	public void setFe_money(Double fe_money) {
		this.fe_money = fe_money;
	}
	public Double getFe_discount_money() {
		return fe_discount_money;
	}
	public void setFe_discount_money(Double fe_discount_money) {
		this.fe_discount_money = fe_discount_money;
	}
	public String getFe_maker() {
		return fe_maker;
	}
	public void setFe_maker(String fe_maker) {
		this.fe_maker = fe_maker;
	}
	public String getFe_manager() {
		return fe_manager;
	}
	public void setFe_manager(String fe_manager) {
		this.fe_manager = fe_manager;
	}
	public Integer getFe_ar_state() {
		return fe_ar_state;
	}
	public void setFe_ar_state(Integer fe_ar_state) {
		this.fe_ar_state = fe_ar_state;
	}
	public String getFe_ar_date() {
		return fe_ar_date;
	}
	public void setFe_ar_date(String fe_ar_date) {
		this.fe_ar_date = fe_ar_date;
	}
	public Integer getFe_pay_state() {
		return fe_pay_state;
	}
	public void setFe_pay_state(Integer fe_pay_state) {
		this.fe_pay_state = fe_pay_state;
	}
	public Double getFe_receivable() {
		return fe_receivable;
	}
	public void setFe_receivable(Double fe_receivable) {
		this.fe_receivable = fe_receivable;
	}
	public Double getFe_received() {
		return fe_received;
	}
	public void setFe_received(Double fe_received) {
		this.fe_received = fe_received;
	}
	public Double getFe_prepay() {
		return fe_prepay;
	}
	public void setFe_prepay(Double fe_prepay) {
		this.fe_prepay = fe_prepay;
	}
	public String getFe_remark() {
		return fe_remark;
	}
	public void setFe_remark(String fe_remark) {
		this.fe_remark = fe_remark;
	}
	public String getFe_sysdate() {
		return fe_sysdate;
	}
	public void setFe_sysdate(String fe_sysdate) {
		this.fe_sysdate = fe_sysdate;
	}
	public Integer getFe_us_id() {
		return fe_us_id;
	}
	public void setFe_us_id(Integer fe_us_id) {
		this.fe_us_id = fe_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
