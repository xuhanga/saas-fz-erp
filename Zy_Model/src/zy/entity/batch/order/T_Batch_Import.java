package zy.entity.batch.order;

import zy.entity.base.product.T_Base_Barcode;

public class T_Batch_Import extends T_Base_Barcode {
	private static final long serialVersionUID = 6279863959747452772L;
	private String pd_szg_code;
	private Double unit_price;
	private Double retail_price;
	private Double cost_price;
	public String getPd_szg_code() {
		return pd_szg_code;
	}
	public void setPd_szg_code(String pd_szg_code) {
		this.pd_szg_code = pd_szg_code;
	}
	public Double getUnit_price() {
		return unit_price;
	}
	public void setUnit_price(Double unit_price) {
		this.unit_price = unit_price;
	}
	public Double getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(Double retail_price) {
		this.retail_price = retail_price;
	}
	public Double getCost_price() {
		return cost_price;
	}
	public void setCost_price(Double cost_price) {
		this.cost_price = cost_price;
	}
}
