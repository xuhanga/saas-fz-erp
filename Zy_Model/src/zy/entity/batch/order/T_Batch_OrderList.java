package zy.entity.batch.order;

import java.io.Serializable;

public class T_Batch_OrderList implements Serializable{
	private static final long serialVersionUID = -6115285984340428790L;
	private Long odl_id;
	private String odl_number;
	private String odl_pd_code;
	private String odl_sub_code;
	private String odl_sz_code;
	private String odl_szg_code;
	private String odl_cr_code;
	private String odl_br_code;
	private Integer odl_amount;
	private Integer odl_realamount;
	private Double odl_unitprice;
	private Double odl_unitmoney;
	private Double odl_retailprice;
	private Double odl_retailmoney;
	private Double odl_costprice;
	private Double odl_costmoney;
	private Double odl_rate;
	private String odl_remark;
	private Integer odl_pi_type;
	private Integer odl_us_id;
	private Integer odl_type;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Long getOdl_id() {
		return odl_id;
	}
	public void setOdl_id(Long odl_id) {
		this.odl_id = odl_id;
	}
	public String getOdl_number() {
		return odl_number;
	}
	public void setOdl_number(String odl_number) {
		this.odl_number = odl_number;
	}
	public String getOdl_pd_code() {
		return odl_pd_code;
	}
	public void setOdl_pd_code(String odl_pd_code) {
		this.odl_pd_code = odl_pd_code;
	}
	public String getOdl_sub_code() {
		return odl_sub_code;
	}
	public void setOdl_sub_code(String odl_sub_code) {
		this.odl_sub_code = odl_sub_code;
	}
	public String getOdl_sz_code() {
		return odl_sz_code;
	}
	public void setOdl_sz_code(String odl_sz_code) {
		this.odl_sz_code = odl_sz_code;
	}
	public String getOdl_szg_code() {
		return odl_szg_code;
	}
	public void setOdl_szg_code(String odl_szg_code) {
		this.odl_szg_code = odl_szg_code;
	}
	public String getOdl_cr_code() {
		return odl_cr_code;
	}
	public void setOdl_cr_code(String odl_cr_code) {
		this.odl_cr_code = odl_cr_code;
	}
	public String getOdl_br_code() {
		return odl_br_code;
	}
	public void setOdl_br_code(String odl_br_code) {
		this.odl_br_code = odl_br_code;
	}
	public Integer getOdl_amount() {
		return odl_amount;
	}
	public void setOdl_amount(Integer odl_amount) {
		this.odl_amount = odl_amount;
	}
	public Integer getOdl_realamount() {
		return odl_realamount;
	}
	public void setOdl_realamount(Integer odl_realamount) {
		this.odl_realamount = odl_realamount;
	}
	public Double getOdl_unitprice() {
		return odl_unitprice;
	}
	public void setOdl_unitprice(Double odl_unitprice) {
		this.odl_unitprice = odl_unitprice;
	}
	public Double getOdl_unitmoney() {
		if (odl_amount != null && odl_unitprice != null) {
			return odl_amount * odl_unitprice;
		}
		return odl_unitmoney;
	}
	public void setOdl_unitmoney(Double odl_unitmoney) {
		this.odl_unitmoney = odl_unitmoney;
	}
	public Double getOdl_retailprice() {
		return odl_retailprice;
	}
	public void setOdl_retailprice(Double odl_retailprice) {
		this.odl_retailprice = odl_retailprice;
	}
	public Double getOdl_retailmoney() {
		if (odl_amount != null && odl_retailprice != null) {
			return odl_amount * odl_retailprice;
		}
		return odl_retailmoney;
	}
	public void setOdl_retailmoney(Double odl_retailmoney) {
		this.odl_retailmoney = odl_retailmoney;
	}
	public Double getOdl_costprice() {
		return odl_costprice;
	}
	public void setOdl_costprice(Double odl_costprice) {
		this.odl_costprice = odl_costprice;
	}
	public Double getOdl_costmoney() {
		if (odl_amount != null && odl_costprice != null) {
			return odl_amount * odl_costprice;
		}
		return odl_costmoney;
	}
	public void setOdl_costmoney(Double odl_costmoney) {
		this.odl_costmoney = odl_costmoney;
	}
	public Double getOdl_rate() {
		if (odl_unitprice != null && odl_retailprice != null && odl_retailprice != 0) {
			return odl_unitprice / odl_retailprice;
		}
		return odl_rate;
	}
	public void setOdl_rate(Double odl_rate) {
		this.odl_rate = odl_rate;
	}
	public String getOdl_remark() {
		return odl_remark;
	}
	public void setOdl_remark(String odl_remark) {
		this.odl_remark = odl_remark;
	}
	public Integer getOdl_pi_type() {
		return odl_pi_type;
	}
	public void setOdl_pi_type(Integer odl_pi_type) {
		this.odl_pi_type = odl_pi_type;
	}
	public Integer getOdl_us_id() {
		return odl_us_id;
	}
	public void setOdl_us_id(Integer odl_us_id) {
		this.odl_us_id = odl_us_id;
	}
	public Integer getOdl_type() {
		return odl_type;
	}
	public void setOdl_type(Integer odl_type) {
		this.odl_type = odl_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
}
