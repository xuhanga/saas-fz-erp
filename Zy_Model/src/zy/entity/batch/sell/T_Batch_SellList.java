package zy.entity.batch.sell;

import java.io.Serializable;

public class T_Batch_SellList implements Serializable{
	private static final long serialVersionUID = 3022296997157028384L;
	private Long sel_id;
	private String sel_number;
	private String sel_pd_code;
	private String sel_sub_code;
	private String sel_sz_code;
	private String sel_szg_code;
	private String sel_cr_code;
	private String sel_br_code;
	private Integer sel_amount;
	private Double sel_unitprice;
	private Double sel_unitmoney;
	private Double sel_retailprice;
	private Double sel_retailmoney;
	private Double sel_costprice;
	private Double sel_costmoney;
	private Double sel_rebateprice;
	private Double sel_rebatemoney;
	private Double sel_rate;
	private String sel_remark;
	private String sel_order_number;
	private Integer sel_pi_type;
	private Integer sel_us_id;
	private Integer sel_type;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_code;
	private String bd_name;
	private String tp_code;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Long getSel_id() {
		return sel_id;
	}
	public void setSel_id(Long sel_id) {
		this.sel_id = sel_id;
	}
	public String getSel_number() {
		return sel_number;
	}
	public void setSel_number(String sel_number) {
		this.sel_number = sel_number;
	}
	public String getSel_pd_code() {
		return sel_pd_code;
	}
	public void setSel_pd_code(String sel_pd_code) {
		this.sel_pd_code = sel_pd_code;
	}
	public String getSel_sub_code() {
		return sel_sub_code;
	}
	public void setSel_sub_code(String sel_sub_code) {
		this.sel_sub_code = sel_sub_code;
	}
	public String getSel_sz_code() {
		return sel_sz_code;
	}
	public void setSel_sz_code(String sel_sz_code) {
		this.sel_sz_code = sel_sz_code;
	}
	public String getSel_szg_code() {
		return sel_szg_code;
	}
	public void setSel_szg_code(String sel_szg_code) {
		this.sel_szg_code = sel_szg_code;
	}
	public String getSel_cr_code() {
		return sel_cr_code;
	}
	public void setSel_cr_code(String sel_cr_code) {
		this.sel_cr_code = sel_cr_code;
	}
	public String getSel_br_code() {
		return sel_br_code;
	}
	public void setSel_br_code(String sel_br_code) {
		this.sel_br_code = sel_br_code;
	}
	public Integer getSel_amount() {
		return sel_amount;
	}
	public void setSel_amount(Integer sel_amount) {
		this.sel_amount = sel_amount;
	}
	public Double getSel_unitprice() {
		return sel_unitprice;
	}
	public void setSel_unitprice(Double sel_unitprice) {
		this.sel_unitprice = sel_unitprice;
	}
	public Double getSel_unitmoney() {
		if (sel_amount != null && sel_unitprice != null) {
			return sel_amount * sel_unitprice;
		}
		return sel_unitmoney;
	}
	public void setSel_unitmoney(Double sel_unitmoney) {
		this.sel_unitmoney = sel_unitmoney;
	}
	public Double getSel_retailprice() {
		return sel_retailprice;
	}
	public void setSel_retailprice(Double sel_retailprice) {
		this.sel_retailprice = sel_retailprice;
	}
	public Double getSel_retailmoney() {
		if (sel_amount != null && sel_retailprice != null) {
			return sel_amount * sel_retailprice;
		}
		return sel_retailmoney;
	}
	public void setSel_retailmoney(Double sel_retailmoney) {
		this.sel_retailmoney = sel_retailmoney;
	}
	public Double getSel_costprice() {
		return sel_costprice;
	}
	public void setSel_costprice(Double sel_costprice) {
		this.sel_costprice = sel_costprice;
	}
	public Double getSel_costmoney() {
		if (sel_amount != null && sel_costprice != null) {
			return sel_amount * sel_costprice;
		}
		return sel_costmoney;
	}
	public void setSel_costmoney(Double sel_costmoney) {
		this.sel_costmoney = sel_costmoney;
	}
	public Double getSel_rebateprice() {
		return sel_rebateprice;
	}
	public void setSel_rebateprice(Double sel_rebateprice) {
		this.sel_rebateprice = sel_rebateprice;
	}
	public Double getSel_rebatemoney() {
		if (sel_amount != null && sel_rebateprice != null) {
			return sel_amount * sel_rebateprice;
		}
		return sel_rebatemoney;
	}
	public void setSel_rebatemoney(Double sel_rebatemoney) {
		this.sel_rebatemoney = sel_rebatemoney;
	}
	public Double getSel_rate() {
		if (sel_unitprice != null && sel_retailprice != null && sel_retailprice != 0) {
			return sel_unitprice / sel_retailprice;
		}
		return sel_rate;
	}
	public void setSel_rate(Double sel_rate) {
		this.sel_rate = sel_rate;
	}
	public String getSel_remark() {
		return sel_remark;
	}
	public void setSel_remark(String sel_remark) {
		this.sel_remark = sel_remark;
	}
	public String getSel_order_number() {
		return sel_order_number;
	}
	public void setSel_order_number(String sel_order_number) {
		this.sel_order_number = sel_order_number;
	}
	public Integer getSel_pi_type() {
		return sel_pi_type;
	}
	public void setSel_pi_type(Integer sel_pi_type) {
		this.sel_pi_type = sel_pi_type;
	}
	public Integer getSel_us_id() {
		return sel_us_id;
	}
	public void setSel_us_id(Integer sel_us_id) {
		this.sel_us_id = sel_us_id;
	}
	public Integer getSel_type() {
		return sel_type;
	}
	public void setSel_type(Integer sel_type) {
		this.sel_type = sel_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	
}
