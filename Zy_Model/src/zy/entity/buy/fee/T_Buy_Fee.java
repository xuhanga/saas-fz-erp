package zy.entity.buy.fee;

import java.io.Serializable;

public class T_Buy_Fee implements Serializable{
	private static final long serialVersionUID = 5339365911355448044L;
	private Integer fe_id;
	private String fe_number;
	private String fe_date;
	private String fe_supply_code;
	private String supply_name;
	private Double fe_money;
	private Double fe_discount_money;
	private String fe_maker;
	private String fe_manager;
	private Integer fe_ar_state;
	private String fe_ar_date;
	private Integer fe_pay_state;
	private Double fe_payable;
	private Double fe_payabled;
	private Double fe_prepay;
	private String fe_remark;
	private String fe_sysdate;
	private Integer fe_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getFe_id() {
		return fe_id;
	}
	public void setFe_id(Integer fe_id) {
		this.fe_id = fe_id;
	}
	public String getFe_number() {
		return fe_number;
	}
	public void setFe_number(String fe_number) {
		this.fe_number = fe_number;
	}
	public String getFe_date() {
		return fe_date;
	}
	public void setFe_date(String fe_date) {
		this.fe_date = fe_date;
	}
	public String getFe_supply_code() {
		return fe_supply_code;
	}
	public void setFe_supply_code(String fe_supply_code) {
		this.fe_supply_code = fe_supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public Double getFe_money() {
		return fe_money;
	}
	public void setFe_money(Double fe_money) {
		this.fe_money = fe_money;
	}
	public Double getFe_discount_money() {
		return fe_discount_money;
	}
	public void setFe_discount_money(Double fe_discount_money) {
		this.fe_discount_money = fe_discount_money;
	}
	public String getFe_maker() {
		return fe_maker;
	}
	public void setFe_maker(String fe_maker) {
		this.fe_maker = fe_maker;
	}
	public String getFe_manager() {
		return fe_manager;
	}
	public void setFe_manager(String fe_manager) {
		this.fe_manager = fe_manager;
	}
	public Integer getFe_ar_state() {
		return fe_ar_state;
	}
	public void setFe_ar_state(Integer fe_ar_state) {
		this.fe_ar_state = fe_ar_state;
	}
	public String getFe_ar_date() {
		return fe_ar_date;
	}
	public void setFe_ar_date(String fe_ar_date) {
		this.fe_ar_date = fe_ar_date;
	}
	public Integer getFe_pay_state() {
		return fe_pay_state;
	}
	public void setFe_pay_state(Integer fe_pay_state) {
		this.fe_pay_state = fe_pay_state;
	}
	public Double getFe_payable() {
		return fe_payable;
	}
	public void setFe_payable(Double fe_payable) {
		this.fe_payable = fe_payable;
	}
	public Double getFe_payabled() {
		return fe_payabled;
	}
	public void setFe_payabled(Double fe_payabled) {
		this.fe_payabled = fe_payabled;
	}
	public Double getFe_prepay() {
		return fe_prepay;
	}
	public void setFe_prepay(Double fe_prepay) {
		this.fe_prepay = fe_prepay;
	}
	public String getFe_remark() {
		return fe_remark;
	}
	public void setFe_remark(String fe_remark) {
		this.fe_remark = fe_remark;
	}
	public String getFe_sysdate() {
		return fe_sysdate;
	}
	public void setFe_sysdate(String fe_sysdate) {
		this.fe_sysdate = fe_sysdate;
	}
	public Integer getFe_us_id() {
		return fe_us_id;
	}
	public void setFe_us_id(Integer fe_us_id) {
		this.fe_us_id = fe_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
