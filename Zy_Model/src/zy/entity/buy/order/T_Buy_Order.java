package zy.entity.buy.order;

import java.io.Serializable;

public class T_Buy_Order implements Serializable{
	private static final long serialVersionUID = 6465158940262782458L;
	private Integer od_id;
	private String od_number;
	private String od_make_date;
	private String od_delivery_date;
	private String od_supply_code;
	private String supply_name;
	private String od_depot_code;
	private String depot_name;
	private String od_maker;
	private String od_manager;
	private Integer od_state;
	private String od_handnumber;
	private Integer od_amount;
	private Integer od_realamount;
	private Double od_money;
	private String od_remark;
	private Integer od_ar_state;
	private String od_ar_date;
	private String od_ar_name;
	private Integer od_isdraft;
	private String od_sysdate;
	private Integer od_us_id;
	private Integer od_type;
	private Integer companyid;
	
	private String ar_describe;
	public Integer getOd_id() {
		return od_id;
	}
	public void setOd_id(Integer od_id) {
		this.od_id = od_id;
	}
	public String getOd_number() {
		return od_number;
	}
	public void setOd_number(String od_number) {
		this.od_number = od_number;
	}
	public String getOd_make_date() {
		return od_make_date;
	}
	public void setOd_make_date(String od_make_date) {
		this.od_make_date = od_make_date;
	}
	public String getOd_delivery_date() {
		return od_delivery_date;
	}
	public void setOd_delivery_date(String od_delivery_date) {
		this.od_delivery_date = od_delivery_date;
	}
	public String getOd_supply_code() {
		return od_supply_code;
	}
	public void setOd_supply_code(String od_supply_code) {
		this.od_supply_code = od_supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public String getOd_depot_code() {
		return od_depot_code;
	}
	public void setOd_depot_code(String od_depot_code) {
		this.od_depot_code = od_depot_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getOd_maker() {
		return od_maker;
	}
	public void setOd_maker(String od_maker) {
		this.od_maker = od_maker;
	}
	public String getOd_manager() {
		return od_manager;
	}
	public void setOd_manager(String od_manager) {
		this.od_manager = od_manager;
	}
	public Integer getOd_state() {
		return od_state;
	}
	public void setOd_state(Integer od_state) {
		this.od_state = od_state;
	}
	public String getOd_handnumber() {
		return od_handnumber;
	}
	public void setOd_handnumber(String od_handnumber) {
		this.od_handnumber = od_handnumber;
	}
	public Integer getOd_amount() {
		return od_amount;
	}
	public void setOd_amount(Integer od_amount) {
		this.od_amount = od_amount;
	}
	public Integer getOd_realamount() {
		return od_realamount;
	}
	public void setOd_realamount(Integer od_realamount) {
		this.od_realamount = od_realamount;
	}
	public Double getOd_money() {
		return od_money;
	}
	public void setOd_money(Double od_money) {
		this.od_money = od_money;
	}
	public String getOd_remark() {
		return od_remark;
	}
	public void setOd_remark(String od_remark) {
		this.od_remark = od_remark;
	}
	public Integer getOd_ar_state() {
		return od_ar_state;
	}
	public void setOd_ar_state(Integer od_ar_state) {
		this.od_ar_state = od_ar_state;
	}
	public String getOd_ar_date() {
		return od_ar_date;
	}
	public void setOd_ar_date(String od_ar_date) {
		this.od_ar_date = od_ar_date;
	}
	public String getOd_ar_name() {
		return od_ar_name;
	}
	public void setOd_ar_name(String od_ar_name) {
		this.od_ar_name = od_ar_name;
	}
	public Integer getOd_isdraft() {
		return od_isdraft;
	}
	public void setOd_isdraft(Integer od_isdraft) {
		this.od_isdraft = od_isdraft;
	}
	public String getOd_sysdate() {
		return od_sysdate;
	}
	public void setOd_sysdate(String od_sysdate) {
		this.od_sysdate = od_sysdate;
	}
	public Integer getOd_us_id() {
		return od_us_id;
	}
	public void setOd_us_id(Integer od_us_id) {
		this.od_us_id = od_us_id;
	}
	public Integer getOd_type() {
		return od_type;
	}
	public void setOd_type(Integer od_type) {
		this.od_type = od_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
