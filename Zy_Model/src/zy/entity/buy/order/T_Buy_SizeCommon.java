package zy.entity.buy.order;

public class T_Buy_SizeCommon {
	private int id;
	private String pd_code;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String bd_name;
	private String tp_name;
	private String cr_code;
	private String cr_name;
	private String sz_code;
	private String sz_name;
	private String szg_code;
	private String br_code;
	private String br_name;
	private int totalamount;
	private double unitprice;
	private double unitmoney;
	private double costprice;
	private double costmoney;
	private double retailprice;
	private double retailmoney;
	private String pd_type;//商品类型，0商品1赠品2样品
	private int[] single_amount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_code() {
		return sz_code;
	}
	public void setSz_code(String sz_code) {
		this.sz_code = sz_code;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getSzg_code() {
		return szg_code;
	}
	public void setSzg_code(String szg_code) {
		this.szg_code = szg_code;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public int getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(int totalamount) {
		this.totalamount = totalamount;
	}
	public double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}
	public double getUnitmoney() {
		return unitmoney;
	}
	public void setUnitmoney(double unitmoney) {
		this.unitmoney = unitmoney;
	}
	public double getCostprice() {
		return costprice;
	}
	public void setCostprice(double costprice) {
		this.costprice = costprice;
	}
	public double getCostmoney() {
		return costmoney;
	}
	public void setCostmoney(double costmoney) {
		this.costmoney = costmoney;
	}
	public double getRetailprice() {
		return retailprice;
	}
	public void setRetailprice(double retailprice) {
		this.retailprice = retailprice;
	}
	public double getRetailmoney() {
		return retailmoney;
	}
	public void setRetailmoney(double retailmoney) {
		this.retailmoney = retailmoney;
	}
	public String getPd_type() {
		return pd_type;
	}
	public void setPd_type(String pd_type) {
		this.pd_type = pd_type;
	}
	public int[] getSingle_amount() {
		return single_amount;
	}
	public void setSingle_amount(int[] single_amount) {
		this.single_amount = single_amount;
	}
}
