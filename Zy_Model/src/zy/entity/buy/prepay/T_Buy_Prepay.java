package zy.entity.buy.prepay;

import java.io.Serializable;

public class T_Buy_Prepay implements Serializable{
	private static final long serialVersionUID = 8281315892455447835L;
	private Integer pp_id;
	private String pp_number;
	private String pp_date;
	private String pp_supply_code;
	private String supply_name;
	private Integer pp_type;
	private Double pp_money;
	private String pp_maker;
	private String pp_manager;
	private String pp_ba_code;
	private String ba_name;
	private String pp_remark;
	private Integer pp_ar_state;
	private String pp_ar_date;
	private String pp_sysdate;
	private Integer pp_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getPp_id() {
		return pp_id;
	}
	public void setPp_id(Integer pp_id) {
		this.pp_id = pp_id;
	}
	public String getPp_number() {
		return pp_number;
	}
	public void setPp_number(String pp_number) {
		this.pp_number = pp_number;
	}
	public String getPp_date() {
		return pp_date;
	}
	public void setPp_date(String pp_date) {
		this.pp_date = pp_date;
	}
	public String getPp_supply_code() {
		return pp_supply_code;
	}
	public void setPp_supply_code(String pp_supply_code) {
		this.pp_supply_code = pp_supply_code;
	}
	public String getSupply_name() {
		return supply_name;
	}
	public void setSupply_name(String supply_name) {
		this.supply_name = supply_name;
	}
	public Integer getPp_type() {
		return pp_type;
	}
	public void setPp_type(Integer pp_type) {
		this.pp_type = pp_type;
	}
	public Double getPp_money() {
		return pp_money;
	}
	public void setPp_money(Double pp_money) {
		this.pp_money = pp_money;
	}
	public String getPp_maker() {
		return pp_maker;
	}
	public void setPp_maker(String pp_maker) {
		this.pp_maker = pp_maker;
	}
	public String getPp_manager() {
		return pp_manager;
	}
	public void setPp_manager(String pp_manager) {
		this.pp_manager = pp_manager;
	}
	public String getPp_ba_code() {
		return pp_ba_code;
	}
	public void setPp_ba_code(String pp_ba_code) {
		this.pp_ba_code = pp_ba_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public String getPp_remark() {
		return pp_remark;
	}
	public void setPp_remark(String pp_remark) {
		this.pp_remark = pp_remark;
	}
	public Integer getPp_ar_state() {
		return pp_ar_state;
	}
	public void setPp_ar_state(Integer pp_ar_state) {
		this.pp_ar_state = pp_ar_state;
	}
	public String getPp_ar_date() {
		return pp_ar_date;
	}
	public void setPp_ar_date(String pp_ar_date) {
		this.pp_ar_date = pp_ar_date;
	}
	public String getPp_sysdate() {
		return pp_sysdate;
	}
	public void setPp_sysdate(String pp_sysdate) {
		this.pp_sysdate = pp_sysdate;
	}
	public Integer getPp_us_id() {
		return pp_us_id;
	}
	public void setPp_us_id(Integer pp_us_id) {
		this.pp_us_id = pp_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
