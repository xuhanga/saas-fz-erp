package zy.entity.buy.settle;

import java.io.Serializable;

public class T_Buy_SettleList implements Serializable{
	private static final long serialVersionUID = 2073990430981421364L;
	private Long stl_id;
	private String stl_number;
	private Double stl_payable;
	private Double stl_payabled;
	private Double stl_discount_money_yet;
	private Double stl_prepay_yet;
	private Double stl_unpayable;
	private Double stl_discount_money;
	private Double stl_prepay;
	private Double stl_real_pay;
	private String stl_bill_number;
	private String stl_remark;
	private Integer stl_type;
	private Integer stl_join;
	private Integer stl_us_id;
	private Integer companyid;
	public Long getStl_id() {
		return stl_id;
	}
	public void setStl_id(Long stl_id) {
		this.stl_id = stl_id;
	}
	public String getStl_number() {
		return stl_number;
	}
	public void setStl_number(String stl_number) {
		this.stl_number = stl_number;
	}
	public Double getStl_payable() {
		return stl_payable;
	}
	public void setStl_payable(Double stl_payable) {
		this.stl_payable = stl_payable;
	}
	public Double getStl_payabled() {
		return stl_payabled;
	}
	public void setStl_payabled(Double stl_payabled) {
		this.stl_payabled = stl_payabled;
	}
	public Double getStl_discount_money_yet() {
		return stl_discount_money_yet;
	}
	public void setStl_discount_money_yet(Double stl_discount_money_yet) {
		this.stl_discount_money_yet = stl_discount_money_yet;
	}
	public Double getStl_prepay_yet() {
		return stl_prepay_yet;
	}
	public void setStl_prepay_yet(Double stl_prepay_yet) {
		this.stl_prepay_yet = stl_prepay_yet;
	}
	public Double getStl_unpayable() {
		return stl_unpayable;
	}
	public void setStl_unpayable(Double stl_unpayable) {
		this.stl_unpayable = stl_unpayable;
	}
	public Double getStl_discount_money() {
		return stl_discount_money;
	}
	public void setStl_discount_money(Double stl_discount_money) {
		this.stl_discount_money = stl_discount_money;
	}
	public Double getStl_prepay() {
		return stl_prepay;
	}
	public void setStl_prepay(Double stl_prepay) {
		this.stl_prepay = stl_prepay;
	}
	public Double getStl_real_pay() {
		return stl_real_pay;
	}
	public void setStl_real_pay(Double stl_real_pay) {
		this.stl_real_pay = stl_real_pay;
	}
	public String getStl_bill_number() {
		return stl_bill_number;
	}
	public void setStl_bill_number(String stl_bill_number) {
		this.stl_bill_number = stl_bill_number;
	}
	public String getStl_remark() {
		return stl_remark;
	}
	public void setStl_remark(String stl_remark) {
		this.stl_remark = stl_remark;
	}
	public Integer getStl_type() {
		return stl_type;
	}
	public void setStl_type(Integer stl_type) {
		this.stl_type = stl_type;
	}
	public Integer getStl_join() {
		return stl_join;
	}
	public void setStl_join(Integer stl_join) {
		this.stl_join = stl_join;
	}
	public Integer getStl_us_id() {
		return stl_us_id;
	}
	public void setStl_us_id(Integer stl_us_id) {
		this.stl_us_id = stl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
