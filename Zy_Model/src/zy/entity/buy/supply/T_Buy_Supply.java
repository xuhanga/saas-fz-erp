package zy.entity.buy.supply;

import java.io.Serializable;

public class T_Buy_Supply implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sp_id;
	private String sp_code;
	private String sp_name;
	private String sp_spell;
	private Double sp_rate;
	private Double sp_init_debt;
	private Double sp_payable;
	private Double sp_payabled;
	private Double sp_prepay;
	private String sp_ar_code;
	private Integer sp_buy_cycle;
	private Integer sp_settle_cycle;
	private Integer companyid;
	//子表信息
	private String spi_man;
	private String spi_tel;
	private String spi_mobile;
	private String spi_addr;
	private String spi_bank_open;
	private String spi_bank_code;
	private String spi_remark;
	private Double spi_earnest;
	private Double spi_deposit;
	public Integer getSp_id() {
		return sp_id;
	}
	public String getSpi_man() {
		return spi_man;
	}
	public void setSpi_man(String spi_man) {
		this.spi_man = spi_man;
	}
	public String getSpi_tel() {
		return spi_tel;
	}
	public void setSpi_tel(String spi_tel) {
		this.spi_tel = spi_tel;
	}
	public String getSpi_mobile() {
		return spi_mobile;
	}
	public void setSpi_mobile(String spi_mobile) {
		this.spi_mobile = spi_mobile;
	}
	public String getSpi_addr() {
		return spi_addr;
	}
	public void setSpi_addr(String spi_addr) {
		this.spi_addr = spi_addr;
	}
	public String getSpi_bank_open() {
		return spi_bank_open;
	}
	public void setSpi_bank_open(String spi_bank_open) {
		this.spi_bank_open = spi_bank_open;
	}
	public String getSpi_bank_code() {
		return spi_bank_code;
	}
	public void setSpi_bank_code(String spi_bank_code) {
		this.spi_bank_code = spi_bank_code;
	}
	public String getSpi_remark() {
		return spi_remark;
	}
	public void setSpi_remark(String spi_remark) {
		this.spi_remark = spi_remark;
	}
	public Double getSpi_earnest() {
		return spi_earnest;
	}
	public void setSpi_earnest(Double spi_earnest) {
		this.spi_earnest = spi_earnest;
	}
	public Double getSpi_deposit() {
		return spi_deposit;
	}
	public void setSpi_deposit(Double spi_deposit) {
		this.spi_deposit = spi_deposit;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_spell() {
		return sp_spell;
	}
	public void setSp_spell(String sp_spell) {
		this.sp_spell = sp_spell;
	}
	public Double getSp_rate() {
		return sp_rate;
	}
	public void setSp_rate(Double sp_rate) {
		this.sp_rate = sp_rate;
	}
	public Double getSp_init_debt() {
		return sp_init_debt;
	}
	public void setSp_init_debt(Double sp_init_debt) {
		this.sp_init_debt = sp_init_debt;
	}
	public Double getSp_payable() {
		return sp_payable;
	}
	public void setSp_payable(Double sp_payable) {
		this.sp_payable = sp_payable;
	}
	public Double getSp_payabled() {
		return sp_payabled;
	}
	public void setSp_payabled(Double sp_payabled) {
		this.sp_payabled = sp_payabled;
	}
	public Double getSp_prepay() {
		return sp_prepay;
	}
	public void setSp_prepay(Double sp_prepay) {
		this.sp_prepay = sp_prepay;
	}
	public String getSp_ar_code() {
		return sp_ar_code;
	}
	public void setSp_ar_code(String sp_ar_code) {
		this.sp_ar_code = sp_ar_code;
	}
	public Integer getSp_buy_cycle() {
		return sp_buy_cycle;
	}
	public void setSp_buy_cycle(Integer sp_buy_cycle) {
		this.sp_buy_cycle = sp_buy_cycle;
	}
	public Integer getSp_settle_cycle() {
		return sp_settle_cycle;
	}
	public void setSp_settle_cycle(Integer sp_settle_cycle) {
		this.sp_settle_cycle = sp_settle_cycle;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
