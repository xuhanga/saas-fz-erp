package zy.entity.buy.supply;

import java.io.Serializable;

public class T_Buy_Supply_Info implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer spi_id;
	private String spi_sp_code;
	private String spi_man;
	private String spi_tel;
	private String spi_mobile;
	private String spi_addr;
	private String spi_bank_open;
	private String spi_bank_code;
	private String spi_remark;
	private float spi_earnest;
	private float spi_deposit;
	private Integer companyid;
	public Integer getSpi_id() {
		return spi_id;
	}
	public void setSpi_id(Integer spi_id) {
		this.spi_id = spi_id;
	}
	public String getSpi_sp_code() {
		return spi_sp_code;
	}
	public void setSpi_sp_code(String spi_sp_code) {
		this.spi_sp_code = spi_sp_code;
	}
	public String getSpi_man() {
		return spi_man;
	}
	public void setSpi_man(String spi_man) {
		this.spi_man = spi_man;
	}
	public String getSpi_tel() {
		return spi_tel;
	}
	public void setSpi_tel(String spi_tel) {
		this.spi_tel = spi_tel;
	}
	public String getSpi_mobile() {
		return spi_mobile;
	}
	public void setSpi_mobile(String spi_mobile) {
		this.spi_mobile = spi_mobile;
	}
	public String getSpi_addr() {
		return spi_addr;
	}
	public void setSpi_addr(String spi_addr) {
		this.spi_addr = spi_addr;
	}
	public String getSpi_bank_open() {
		return spi_bank_open;
	}
	public void setSpi_bank_open(String spi_bank_open) {
		this.spi_bank_open = spi_bank_open;
	}
	public String getSpi_bank_code() {
		return spi_bank_code;
	}
	public void setSpi_bank_code(String spi_bank_code) {
		this.spi_bank_code = spi_bank_code;
	}
	public String getSpi_remark() {
		return spi_remark;
	}
	public void setSpi_remark(String spi_remark) {
		this.spi_remark = spi_remark;
	}
	public float getSpi_earnest() {
		return spi_earnest;
	}
	public void setSpi_earnest(float spi_earnest) {
		this.spi_earnest = spi_earnest;
	}
	public float getSpi_deposit() {
		return spi_deposit;
	}
	public void setSpi_deposit(float spi_deposit) {
		this.spi_deposit = spi_deposit;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
