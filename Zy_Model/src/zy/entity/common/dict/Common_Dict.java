package zy.entity.common.dict;

import java.io.Serializable;

public class Common_Dict implements Serializable{
	private static final long serialVersionUID = 4292878308338225686L;
	private Integer dt_id;
	private String dt_code;
	private String dt_name;
	private String dt_type;
	public Integer getDt_id() {
		return dt_id;
	}
	public void setDt_id(Integer dt_id) {
		this.dt_id = dt_id;
	}
	public String getDt_code() {
		return dt_code;
	}
	public void setDt_code(String dt_code) {
		this.dt_code = dt_code;
	}
	public String getDt_name() {
		return dt_name;
	}
	public void setDt_name(String dt_name) {
		this.dt_name = dt_name;
	}
	public String getDt_type() {
		return dt_type;
	}
	public void setDt_type(String dt_type) {
		this.dt_type = dt_type;
	}
}
