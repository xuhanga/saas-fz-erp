package zy.entity.common.kpi;

import java.io.Serializable;

public class Common_Kpi implements Serializable{
	private static final long serialVersionUID = -1179943139932612579L;
	private Integer ki_id;
	private String ki_code;
	private String ki_name;
	private String ki_remark;
	private String ki_identity;
	private Integer ki_state;
	public Integer getKi_id() {
		return ki_id;
	}
	public void setKi_id(Integer ki_id) {
		this.ki_id = ki_id;
	}
	public String getKi_code() {
		return ki_code;
	}
	public void setKi_code(String ki_code) {
		this.ki_code = ki_code;
	}
	public String getKi_name() {
		return ki_name;
	}
	public void setKi_name(String ki_name) {
		this.ki_name = ki_name;
	}
	public String getKi_remark() {
		return ki_remark;
	}
	public void setKi_remark(String ki_remark) {
		this.ki_remark = ki_remark;
	}
	public String getKi_identity() {
		return ki_identity;
	}
	public void setKi_identity(String ki_identity) {
		this.ki_identity = ki_identity;
	}
	public Integer getKi_state() {
		return ki_state;
	}
	public void setKi_state(Integer ki_state) {
		this.ki_state = ki_state;
	}
}
