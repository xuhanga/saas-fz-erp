package zy.entity.common.print;

import java.io.Serializable;

public class Common_PrintField implements Serializable{
	private static final long serialVersionUID = 6531580964378873521L;
	private Integer ptf_id;
	private Integer ptf_pt_id;
	private String ptf_code;
	private String ptf_name;
	private Integer ptf_colspan;
	private Integer ptf_show;
	private Integer ptf_line;
	private Integer ptf_row;
	private Integer ptf_position;
	public Integer getPtf_id() {
		return ptf_id;
	}
	public void setPtf_id(Integer ptf_id) {
		this.ptf_id = ptf_id;
	}
	public Integer getPtf_pt_id() {
		return ptf_pt_id;
	}
	public void setPtf_pt_id(Integer ptf_pt_id) {
		this.ptf_pt_id = ptf_pt_id;
	}
	public String getPtf_code() {
		return ptf_code;
	}
	public void setPtf_code(String ptf_code) {
		this.ptf_code = ptf_code;
	}
	public String getPtf_name() {
		return ptf_name;
	}
	public void setPtf_name(String ptf_name) {
		this.ptf_name = ptf_name;
	}
	public Integer getPtf_colspan() {
		return ptf_colspan;
	}
	public void setPtf_colspan(Integer ptf_colspan) {
		this.ptf_colspan = ptf_colspan;
	}
	public Integer getPtf_show() {
		return ptf_show;
	}
	public void setPtf_show(Integer ptf_show) {
		this.ptf_show = ptf_show;
	}
	public Integer getPtf_line() {
		return ptf_line;
	}
	public void setPtf_line(Integer ptf_line) {
		this.ptf_line = ptf_line;
	}
	public Integer getPtf_row() {
		return ptf_row;
	}
	public void setPtf_row(Integer ptf_row) {
		this.ptf_row = ptf_row;
	}
	public Integer getPtf_position() {
		return ptf_position;
	}
	public void setPtf_position(Integer ptf_position) {
		this.ptf_position = ptf_position;
	}
}
