package zy.entity.common.rewardicon;

import java.io.Serializable;

public class Common_RewardIcon implements Serializable{
	private static final long serialVersionUID = -5483206241361317044L;
	private Integer ri_id;
	private String ri_icon;
	private String ri_style;
	public Integer getRi_id() {
		return ri_id;
	}
	public void setRi_id(Integer ri_id) {
		this.ri_id = ri_id;
	}
	public String getRi_icon() {
		return ri_icon;
	}
	public void setRi_icon(String ri_icon) {
		this.ri_icon = ri_icon;
	}
	public String getRi_style() {
		return ri_style;
	}
	public void setRi_style(String ri_style) {
		this.ri_style = ri_style;
	}
}
