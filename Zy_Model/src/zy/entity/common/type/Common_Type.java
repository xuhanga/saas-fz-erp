package zy.entity.common.type;

import java.io.Serializable;

public class Common_Type implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer ty_id;
	private String ty_name;
	private String ty_type;
	private String ty_state;
	private String ty_ver;
	public Integer getTy_id() {
		return ty_id;
	}
	public void setTy_id(Integer ty_id) {
		this.ty_id = ty_id;
	}
	public String getTy_name() {
		return ty_name;
	}
	public void setTy_name(String ty_name) {
		this.ty_name = ty_name;
	}
	public String getTy_type() {
		return ty_type;
	}
	public void setTy_type(String ty_type) {
		this.ty_type = ty_type;
	}
	public String getTy_state() {
		return ty_state;
	}
	public void setTy_state(String ty_state) {
		this.ty_state = ty_state;
	}
	public String getTy_ver() {
		return ty_ver;
	}
	public void setTy_ver(String ty_ver) {
		this.ty_ver = ty_ver;
	}
	
}
