package zy.entity.money.access;

import java.io.Serializable;

public class T_Money_Access implements Serializable{
	private static final long serialVersionUID = 2566973721998332764L;
	private Integer ac_id;
	private String ac_number;
	private String ac_maker;
	private String ac_manager;
	private String ac_date;
	private Integer ac_ar_state;
	private String ac_ar_date;
	private Double ac_money;
	private String ac_remark;
	private String ac_sysdate;
	private String ac_shop_code;
	private Integer ac_us_id;
	private Integer ac_ca_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getAc_id() {
		return ac_id;
	}
	public void setAc_id(Integer ac_id) {
		this.ac_id = ac_id;
	}
	public String getAc_number() {
		return ac_number;
	}
	public void setAc_number(String ac_number) {
		this.ac_number = ac_number;
	}
	public String getAc_maker() {
		return ac_maker;
	}
	public void setAc_maker(String ac_maker) {
		this.ac_maker = ac_maker;
	}
	public String getAc_manager() {
		return ac_manager;
	}
	public void setAc_manager(String ac_manager) {
		this.ac_manager = ac_manager;
	}
	public String getAc_date() {
		return ac_date;
	}
	public void setAc_date(String ac_date) {
		this.ac_date = ac_date;
	}
	public Integer getAc_ar_state() {
		return ac_ar_state;
	}
	public void setAc_ar_state(Integer ac_ar_state) {
		this.ac_ar_state = ac_ar_state;
	}
	public String getAc_ar_date() {
		return ac_ar_date;
	}
	public void setAc_ar_date(String ac_ar_date) {
		this.ac_ar_date = ac_ar_date;
	}
	public Double getAc_money() {
		return ac_money;
	}
	public void setAc_money(Double ac_money) {
		this.ac_money = ac_money;
	}
	public String getAc_remark() {
		return ac_remark;
	}
	public void setAc_remark(String ac_remark) {
		this.ac_remark = ac_remark;
	}
	public String getAc_sysdate() {
		return ac_sysdate;
	}
	public void setAc_sysdate(String ac_sysdate) {
		this.ac_sysdate = ac_sysdate;
	}
	public String getAc_shop_code() {
		return ac_shop_code;
	}
	public void setAc_shop_code(String ac_shop_code) {
		this.ac_shop_code = ac_shop_code;
	}
	public Integer getAc_ca_id() {
		return ac_ca_id;
	}
	public void setAc_ca_id(Integer ac_ca_id) {
		this.ac_ca_id = ac_ca_id;
	}
	public Integer getAc_us_id() {
		return ac_us_id;
	}
	public void setAc_us_id(Integer ac_us_id) {
		this.ac_us_id = ac_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
