package zy.entity.money.access;

import java.io.Serializable;

public class T_Money_AccessList implements Serializable{
	private static final long serialVersionUID = 3061888332581984602L;
	private Integer acl_id;
	private String acl_number;
	private String acl_ba_code;
	private String ba_name;
	private Integer acl_type;
	private Double acl_money;
	private String acl_remark;
	private Integer acl_us_id;
	private Integer acl_ca_id;
	private Integer companyid;
	public Integer getAcl_id() {
		return acl_id;
	}
	public void setAcl_id(Integer acl_id) {
		this.acl_id = acl_id;
	}
	public String getAcl_number() {
		return acl_number;
	}
	public void setAcl_number(String acl_number) {
		this.acl_number = acl_number;
	}
	public String getAcl_ba_code() {
		return acl_ba_code;
	}
	public void setAcl_ba_code(String acl_ba_code) {
		this.acl_ba_code = acl_ba_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public Integer getAcl_type() {
		return acl_type;
	}
	public void setAcl_type(Integer acl_type) {
		this.acl_type = acl_type;
	}
	public Double getAcl_money() {
		return acl_money;
	}
	public void setAcl_money(Double acl_money) {
		this.acl_money = acl_money;
	}
	public String getAcl_remark() {
		return acl_remark;
	}
	public void setAcl_remark(String acl_remark) {
		this.acl_remark = acl_remark;
	}
	public Integer getAcl_us_id() {
		return acl_us_id;
	}
	public void setAcl_us_id(Integer acl_us_id) {
		this.acl_us_id = acl_us_id;
	}
	public Integer getAcl_ca_id() {
		return acl_ca_id;
	}
	public void setAcl_ca_id(Integer acl_ca_id) {
		this.acl_ca_id = acl_ca_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
