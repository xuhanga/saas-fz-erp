package zy.entity.money.bank;

import java.io.Serializable;

public class T_Money_Bank implements Serializable{
	private static final long serialVersionUID = 7998162212287251654L;
	private Integer ba_id;
	private String ba_code;
	private String ba_name;
	private String ba_spell;
	private String ba_remark;
	private String ba_shop_code;
	private Double ba_init_balance;
	private Double ba_balance;
	private Integer companyid;
	public Integer getBa_id() {
		return ba_id;
	}
	public void setBa_id(Integer ba_id) {
		this.ba_id = ba_id;
	}
	public String getBa_code() {
		return ba_code;
	}
	public void setBa_code(String ba_code) {
		this.ba_code = ba_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public String getBa_spell() {
		return ba_spell;
	}
	public void setBa_spell(String ba_spell) {
		this.ba_spell = ba_spell;
	}
	public String getBa_remark() {
		return ba_remark;
	}
	public void setBa_remark(String ba_remark) {
		this.ba_remark = ba_remark;
	}
	public String getBa_shop_code() {
		return ba_shop_code;
	}
	public void setBa_shop_code(String ba_shop_code) {
		this.ba_shop_code = ba_shop_code;
	}
	public Double getBa_init_balance() {
		return ba_init_balance;
	}
	public void setBa_init_balance(Double ba_init_balance) {
		this.ba_init_balance = ba_init_balance;
	}
	public Double getBa_balance() {
		return ba_balance;
	}
	public void setBa_balance(Double ba_balance) {
		this.ba_balance = ba_balance;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
