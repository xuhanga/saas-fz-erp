package zy.entity.money.bank;

import java.io.Serializable;

public class T_Money_BankRun implements Serializable{
	private static final long serialVersionUID = 3742399704936294158L;
	private Integer br_id;
	private String br_date;
	private String br_number;
	private String br_bt_code;
	private String bt_name;
	private String br_manager;
	private String br_ba_code;
	private String bank_name;
	private Double br_enter;
	private Double br_out;
	private Double br_balance;
	private String br_remark;
	private String br_sysdate;
	private String br_shop_code;
	private Integer companyid;
	public Integer getBr_id() {
		return br_id;
	}
	public void setBr_id(Integer br_id) {
		this.br_id = br_id;
	}
	public String getBr_date() {
		return br_date;
	}
	public void setBr_date(String br_date) {
		this.br_date = br_date;
	}
	public String getBr_number() {
		return br_number;
	}
	public void setBr_number(String br_number) {
		this.br_number = br_number;
	}
	public String getBr_bt_code() {
		return br_bt_code;
	}
	public void setBr_bt_code(String br_bt_code) {
		this.br_bt_code = br_bt_code;
	}
	public String getBt_name() {
		return bt_name;
	}
	public void setBt_name(String bt_name) {
		this.bt_name = bt_name;
	}
	public String getBr_manager() {
		return br_manager;
	}
	public void setBr_manager(String br_manager) {
		this.br_manager = br_manager;
	}
	public String getBr_ba_code() {
		return br_ba_code;
	}
	public void setBr_ba_code(String br_ba_code) {
		this.br_ba_code = br_ba_code;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public Double getBr_enter() {
		return br_enter;
	}
	public void setBr_enter(Double br_enter) {
		this.br_enter = br_enter;
	}
	public Double getBr_out() {
		return br_out;
	}
	public void setBr_out(Double br_out) {
		this.br_out = br_out;
	}
	public Double getBr_balance() {
		return br_balance;
	}
	public void setBr_balance(Double br_balance) {
		this.br_balance = br_balance;
	}
	public String getBr_remark() {
		return br_remark;
	}
	public void setBr_remark(String br_remark) {
		this.br_remark = br_remark;
	}
	public String getBr_sysdate() {
		return br_sysdate;
	}
	public void setBr_sysdate(String br_sysdate) {
		this.br_sysdate = br_sysdate;
	}
	public String getBr_shop_code() {
		return br_shop_code;
	}
	public void setBr_shop_code(String br_shop_code) {
		this.br_shop_code = br_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
