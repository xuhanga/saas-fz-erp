package zy.entity.money.income;

import java.io.Serializable;

public class T_Money_IncomeList implements Serializable{
	private static final long serialVersionUID = 7998162212287251654L;
	private Integer icl_id;
	private String icl_number;
	private String icl_mp_code;
	private String mp_name;
	private Double icl_money;
	private String icl_remark;
	private Integer icl_us_id;
	private Integer companyid;
	private String ic_date;
	public Integer getIcl_id() {
		return icl_id;
	}
	public void setIcl_id(Integer icl_id) {
		this.icl_id = icl_id;
	}
	public String getIcl_number() {
		return icl_number;
	}
	public void setIcl_number(String icl_number) {
		this.icl_number = icl_number;
	}
	public String getIcl_mp_code() {
		return icl_mp_code;
	}
	public void setIcl_mp_code(String icl_mp_code) {
		this.icl_mp_code = icl_mp_code;
	}
	public String getMp_name() {
		return mp_name;
	}
	public void setMp_name(String mp_name) {
		this.mp_name = mp_name;
	}
	public Double getIcl_money() {
		return icl_money;
	}
	public void setIcl_money(Double icl_money) {
		this.icl_money = icl_money;
	}
	public String getIcl_remark() {
		return icl_remark;
	}
	public void setIcl_remark(String icl_remark) {
		this.icl_remark = icl_remark;
	}
	public Integer getIcl_us_id() {
		return icl_us_id;
	}
	public void setIcl_us_id(Integer icl_us_id) {
		this.icl_us_id = icl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getIc_date() {
		return ic_date;
	}
	public void setIc_date(String ic_date) {
		this.ic_date = ic_date;
	}
}
