package zy.entity.money.property;

import java.io.Serializable;

public class T_Money_Property implements Serializable{
	private static final long serialVersionUID = 844288511232275590L;
	private Integer pp_id;
	private String pp_code;
	private String pp_name;
	private String pp_spell;
	private String pp_shop_code;
	private Integer pp_type;
	private Integer companyid;
	public Integer getPp_id() {
		return pp_id;
	}
	public void setPp_id(Integer pp_id) {
		this.pp_id = pp_id;
	}
	public String getPp_code() {
		return pp_code;
	}
	public void setPp_code(String pp_code) {
		this.pp_code = pp_code;
	}
	public String getPp_name() {
		return pp_name;
	}
	public void setPp_name(String pp_name) {
		this.pp_name = pp_name;
	}
	public String getPp_spell() {
		return pp_spell;
	}
	public void setPp_spell(String pp_spell) {
		this.pp_spell = pp_spell;
	}
	public String getPp_shop_code() {
		return pp_shop_code;
	}
	public void setPp_shop_code(String pp_shop_code) {
		this.pp_shop_code = pp_shop_code;
	}
	public Integer getPp_type() {
		return pp_type;
	}
	public void setPp_type(Integer pp_type) {
		this.pp_type = pp_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
