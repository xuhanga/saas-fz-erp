package zy.entity.money.stream;

import java.io.Serializable;

public class T_Money_StreamSettle implements Serializable{
	private static final long serialVersionUID = 8946827548073316143L;
	private Integer st_id;
	private String st_number;
	private String st_date;
	private String st_stream_code;
	private String stream_name;
	private Double st_discount_money;
	private Double st_paid;
	private String st_maker;
	private String st_manager;
	private String st_ba_code;
	private String ba_name;
	private Integer st_ar_state;
	private String st_ar_date;
	private String st_remark;
	private String st_sysdate;
	private Integer st_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getSt_id() {
		return st_id;
	}
	public void setSt_id(Integer st_id) {
		this.st_id = st_id;
	}
	public String getSt_number() {
		return st_number;
	}
	public void setSt_number(String st_number) {
		this.st_number = st_number;
	}
	public String getSt_date() {
		return st_date;
	}
	public void setSt_date(String st_date) {
		this.st_date = st_date;
	}
	public String getSt_stream_code() {
		return st_stream_code;
	}
	public void setSt_stream_code(String st_stream_code) {
		this.st_stream_code = st_stream_code;
	}
	public String getStream_name() {
		return stream_name;
	}
	public void setStream_name(String stream_name) {
		this.stream_name = stream_name;
	}
	public Double getSt_discount_money() {
		return st_discount_money;
	}
	public void setSt_discount_money(Double st_discount_money) {
		this.st_discount_money = st_discount_money;
	}
	public Double getSt_paid() {
		return st_paid;
	}
	public void setSt_paid(Double st_paid) {
		this.st_paid = st_paid;
	}
	public String getSt_maker() {
		return st_maker;
	}
	public void setSt_maker(String st_maker) {
		this.st_maker = st_maker;
	}
	public String getSt_manager() {
		return st_manager;
	}
	public void setSt_manager(String st_manager) {
		this.st_manager = st_manager;
	}
	public String getSt_ba_code() {
		return st_ba_code;
	}
	public void setSt_ba_code(String st_ba_code) {
		this.st_ba_code = st_ba_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public Integer getSt_ar_state() {
		return st_ar_state;
	}
	public void setSt_ar_state(Integer st_ar_state) {
		this.st_ar_state = st_ar_state;
	}
	public String getSt_ar_date() {
		return st_ar_date;
	}
	public void setSt_ar_date(String st_ar_date) {
		this.st_ar_date = st_ar_date;
	}
	public String getSt_remark() {
		return st_remark;
	}
	public void setSt_remark(String st_remark) {
		this.st_remark = st_remark;
	}
	public String getSt_sysdate() {
		return st_sysdate;
	}
	public void setSt_sysdate(String st_sysdate) {
		this.st_sysdate = st_sysdate;
	}
	public Integer getSt_us_id() {
		return st_us_id;
	}
	public void setSt_us_id(Integer st_us_id) {
		this.st_us_id = st_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
