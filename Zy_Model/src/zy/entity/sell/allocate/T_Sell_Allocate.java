package zy.entity.sell.allocate;

import java.io.Serializable;

public class T_Sell_Allocate implements Serializable{
	private static final long serialVersionUID = 4037290047441255925L;
	private Integer ac_id;
	private String ac_number;
	private String ac_date;
	private String ac_out_shop;
	private String ac_out_dp;
	private String ac_in_shop;
	private String ac_in_dp;
	private String outshop_name;
	private String outdepot_name;
	private String inshop_name;
	private String indepot_name;
	private String ac_man;
	private String ac_receiver;
	private String ac_recedate;
	private Integer ac_amount;
	private Double ac_sell_money;
	private Double ac_cost_money;
	private Double ac_in_sell_money;
	private Double ac_in_cost_money;
	private String ac_remark;
	private Integer ac_state;
	private String ac_sysdate;
	private Integer companyid;
	public Integer getAc_id() {
		return ac_id;
	}
	public void setAc_id(Integer ac_id) {
		this.ac_id = ac_id;
	}
	public String getAc_number() {
		return ac_number;
	}
	public void setAc_number(String ac_number) {
		this.ac_number = ac_number;
	}
	public String getAc_date() {
		return ac_date;
	}
	public void setAc_date(String ac_date) {
		this.ac_date = ac_date;
	}
	public String getAc_out_shop() {
		return ac_out_shop;
	}
	public void setAc_out_shop(String ac_out_shop) {
		this.ac_out_shop = ac_out_shop;
	}
	public String getAc_out_dp() {
		return ac_out_dp;
	}
	public void setAc_out_dp(String ac_out_dp) {
		this.ac_out_dp = ac_out_dp;
	}
	public String getAc_in_shop() {
		return ac_in_shop;
	}
	public void setAc_in_shop(String ac_in_shop) {
		this.ac_in_shop = ac_in_shop;
	}
	public String getAc_in_dp() {
		return ac_in_dp;
	}
	public void setAc_in_dp(String ac_in_dp) {
		this.ac_in_dp = ac_in_dp;
	}
	public String getOutshop_name() {
		return outshop_name;
	}
	public void setOutshop_name(String outshop_name) {
		this.outshop_name = outshop_name;
	}
	public String getOutdepot_name() {
		return outdepot_name;
	}
	public void setOutdepot_name(String outdepot_name) {
		this.outdepot_name = outdepot_name;
	}
	public String getInshop_name() {
		return inshop_name;
	}
	public void setInshop_name(String inshop_name) {
		this.inshop_name = inshop_name;
	}
	public String getIndepot_name() {
		return indepot_name;
	}
	public void setIndepot_name(String indepot_name) {
		this.indepot_name = indepot_name;
	}
	public String getAc_man() {
		return ac_man;
	}
	public void setAc_man(String ac_man) {
		this.ac_man = ac_man;
	}
	public String getAc_receiver() {
		return ac_receiver;
	}
	public void setAc_receiver(String ac_receiver) {
		this.ac_receiver = ac_receiver;
	}
	public String getAc_recedate() {
		return ac_recedate;
	}
	public void setAc_recedate(String ac_recedate) {
		this.ac_recedate = ac_recedate;
	}
	public Integer getAc_amount() {
		return ac_amount;
	}
	public void setAc_amount(Integer ac_amount) {
		this.ac_amount = ac_amount;
	}
	public Double getAc_sell_money() {
		return ac_sell_money;
	}
	public void setAc_sell_money(Double ac_sell_money) {
		this.ac_sell_money = ac_sell_money;
	}
	public Double getAc_cost_money() {
		return ac_cost_money;
	}
	public void setAc_cost_money(Double ac_cost_money) {
		this.ac_cost_money = ac_cost_money;
	}
	public Double getAc_in_sell_money() {
		return ac_in_sell_money;
	}
	public void setAc_in_sell_money(Double ac_in_sell_money) {
		this.ac_in_sell_money = ac_in_sell_money;
	}
	public Double getAc_in_cost_money() {
		return ac_in_cost_money;
	}
	public void setAc_in_cost_money(Double ac_in_cost_money) {
		this.ac_in_cost_money = ac_in_cost_money;
	}
	public String getAc_remark() {
		return ac_remark;
	}
	public void setAc_remark(String ac_remark) {
		this.ac_remark = ac_remark;
	}
	public Integer getAc_state() {
		return ac_state;
	}
	public void setAc_state(Integer ac_state) {
		this.ac_state = ac_state;
	}
	public String getAc_sysdate() {
		return ac_sysdate;
	}
	public void setAc_sysdate(String ac_sysdate) {
		this.ac_sysdate = ac_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
