package zy.entity.sell.card;

import java.io.Serializable;

public class T_Sell_Card implements Serializable,Cloneable{
	private static final long serialVersionUID = 1573089257667449060L;
	private Integer cd_id;
	private String cd_code;
	private String cd_cardcode;
	private String cd_shop_code;
	private String shop_name;
	private String cd_grantdate;
	private String cd_enddate;
	private Double cd_money;
	private Double cd_realcash;
	private Double cd_bankmoney;
	private Double cd_used_money;
	private Double cd_cashrate;
	private Integer cd_state;
	private String cd_manager;
	private String cd_name;
	private String cd_mobile;
	private String cd_idcard;
	private String cd_pass;
	private String cd_verify_code;
	private Integer companyid;
	
	private String cdl_ba_code;
	private String cdl_bank_code;
	public Integer getCd_id() {
		return cd_id;
	}
	public void setCd_id(Integer cd_id) {
		this.cd_id = cd_id;
	}
	public String getCd_code() {
		return cd_code;
	}
	public void setCd_code(String cd_code) {
		this.cd_code = cd_code;
	}
	public String getCd_cardcode() {
		return cd_cardcode;
	}
	public void setCd_cardcode(String cd_cardcode) {
		this.cd_cardcode = cd_cardcode;
	}
	public String getCd_shop_code() {
		return cd_shop_code;
	}
	public void setCd_shop_code(String cd_shop_code) {
		this.cd_shop_code = cd_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getCd_grantdate() {
		return cd_grantdate;
	}
	public void setCd_grantdate(String cd_grantdate) {
		this.cd_grantdate = cd_grantdate;
	}
	public String getCd_enddate() {
		return cd_enddate;
	}
	public void setCd_enddate(String cd_enddate) {
		this.cd_enddate = cd_enddate;
	}
	public Double getCd_money() {
		return cd_money;
	}
	public void setCd_money(Double cd_money) {
		this.cd_money = cd_money;
	}
	public Double getCd_realcash() {
		return cd_realcash;
	}
	public void setCd_realcash(Double cd_realcash) {
		this.cd_realcash = cd_realcash;
	}
	public Double getCd_bankmoney() {
		return cd_bankmoney;
	}
	public void setCd_bankmoney(Double cd_bankmoney) {
		this.cd_bankmoney = cd_bankmoney;
	}
	public Double getCd_used_money() {
		return cd_used_money;
	}
	public void setCd_used_money(Double cd_used_money) {
		this.cd_used_money = cd_used_money;
	}
	public Double getCd_cashrate() {
		return cd_cashrate;
	}
	public void setCd_cashrate(Double cd_cashrate) {
		this.cd_cashrate = cd_cashrate;
	}
	public Integer getCd_state() {
		return cd_state;
	}
	public void setCd_state(Integer cd_state) {
		this.cd_state = cd_state;
	}
	public String getCd_manager() {
		return cd_manager;
	}
	public void setCd_manager(String cd_manager) {
		this.cd_manager = cd_manager;
	}
	public String getCd_name() {
		return cd_name;
	}
	public void setCd_name(String cd_name) {
		this.cd_name = cd_name;
	}
	public String getCd_mobile() {
		return cd_mobile;
	}
	public void setCd_mobile(String cd_mobile) {
		this.cd_mobile = cd_mobile;
	}
	public String getCd_idcard() {
		return cd_idcard;
	}
	public void setCd_idcard(String cd_idcard) {
		this.cd_idcard = cd_idcard;
	}
	public String getCd_pass() {
		return cd_pass;
	}
	public void setCd_pass(String cd_pass) {
		this.cd_pass = cd_pass;
	}
	public String getCd_verify_code() {
		return cd_verify_code;
	}
	public void setCd_verify_code(String cd_verify_code) {
		this.cd_verify_code = cd_verify_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	public String getCdl_ba_code() {
		return cdl_ba_code;
	}
	public void setCdl_ba_code(String cdl_ba_code) {
		this.cdl_ba_code = cdl_ba_code;
	}
	public String getCdl_bank_code() {
		return cdl_bank_code;
	}
	public void setCdl_bank_code(String cdl_bank_code) {
		this.cdl_bank_code = cdl_bank_code;
	}
	
}
