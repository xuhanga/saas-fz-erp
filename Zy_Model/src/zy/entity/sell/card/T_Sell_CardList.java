package zy.entity.sell.card;

import java.io.Serializable;

public class T_Sell_CardList implements Serializable{
	private static final long serialVersionUID = 780093716816923518L;
	private Integer cdl_id;
	private String cdl_cd_code;
	private String cdl_cardcode;
	private Double cdl_money;
	private Double cdl_realcash;
	private Double cdl_bankmoney;
	private String cdl_date;
	private Integer cdl_type;
	private String cdl_shop_code;
	private String shop_name;
	private String cdl_ba_code;
	private String cdl_bank_code;
	private String ba_name;
	private String bank_name;
	private String cdl_number;
	private String cdl_manager;
	private Integer companyid;
	private Integer number;
	public Integer getCdl_id() {
		return cdl_id;
	}
	public void setCdl_id(Integer cdl_id) {
		this.cdl_id = cdl_id;
	}
	public String getCdl_cd_code() {
		return cdl_cd_code;
	}
	public void setCdl_cd_code(String cdl_cd_code) {
		this.cdl_cd_code = cdl_cd_code;
	}
	public String getCdl_cardcode() {
		return cdl_cardcode;
	}
	public void setCdl_cardcode(String cdl_cardcode) {
		this.cdl_cardcode = cdl_cardcode;
	}
	public Double getCdl_money() {
		return cdl_money;
	}
	public void setCdl_money(Double cdl_money) {
		this.cdl_money = cdl_money;
	}
	public Double getCdl_realcash() {
		return cdl_realcash;
	}
	public void setCdl_realcash(Double cdl_realcash) {
		this.cdl_realcash = cdl_realcash;
	}
	public Double getCdl_bankmoney() {
		return cdl_bankmoney;
	}
	public void setCdl_bankmoney(Double cdl_bankmoney) {
		this.cdl_bankmoney = cdl_bankmoney;
	}
	public String getCdl_date() {
		return cdl_date;
	}
	public void setCdl_date(String cdl_date) {
		this.cdl_date = cdl_date;
	}
	public Integer getCdl_type() {
		return cdl_type;
	}
	public void setCdl_type(Integer cdl_type) {
		this.cdl_type = cdl_type;
	}
	public String getCdl_shop_code() {
		return cdl_shop_code;
	}
	public void setCdl_shop_code(String cdl_shop_code) {
		this.cdl_shop_code = cdl_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getCdl_ba_code() {
		return cdl_ba_code;
	}
	public void setCdl_ba_code(String cdl_ba_code) {
		this.cdl_ba_code = cdl_ba_code;
	}
	public String getCdl_bank_code() {
		return cdl_bank_code;
	}
	public void setCdl_bank_code(String cdl_bank_code) {
		this.cdl_bank_code = cdl_bank_code;
	}
	public String getBa_name() {
		return ba_name;
	}
	public void setBa_name(String ba_name) {
		this.ba_name = ba_name;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getCdl_number() {
		return cdl_number;
	}
	public void setCdl_number(String cdl_number) {
		this.cdl_number = cdl_number;
	}
	public String getCdl_manager() {
		return cdl_manager;
	}
	public void setCdl_manager(String cdl_manager) {
		this.cdl_manager = cdl_manager;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	
}
