package zy.entity.sell.cash;

import java.io.Serializable;

import zy.entity.stock.data.T_Stock_Data;

public class T_Sell_ShopList extends T_Stock_Data implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer shl_id;
	private String shl_number;
	private String shl_date;
	private String shl_sysdate;
	private String shl_pd_code;
	private String sp_code;
	private String sp_name;
	private String pd_name;
	private String pd_no;
	private String pd_unit;
	private String pd_date;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String em_name;
	private String bd_code;
	private String tp_code;
	private String tp_name;
	private String bd_name;
	private String shl_sub_code;
	private String shl_cr_code;
	private String shl_sz_code;
	private String shl_br_code;
	private String shl_em_code;
	private String shl_slave;
	private String slave_name;
	private String shl_main;
	private String main_name;
	private String shl_da_code;
	private String da_name;
	private String shl_shop_code;
	private String shop_name;
	private String shl_dp_code;
	private Integer shl_amount;
	private Double shl_sign_price;
	private Double shl_sell_price;
	private Double retailMoney;
	private Double shl_sell_money;
	private Double shl_avg_price;
	private Double shl_price;
	private Double shl_price_rate;
	private Double shl_money;
	private Double shl_rate;
	private Double shl_cost_price;
	private Double shl_cost_money;
	private Double shl_profit;
	private Double shl_profit_rate;
	private Double shl_profit_ratio;
	private Double shl_upcost_price;
	private Double pd_sign_price;
	private Integer shl_state;
	private Integer companyid;
	private String shl_vip_code;
	private String vm_name;
	private String shl_sale_code;
	private String pd_img_path;
	private String pd_fabric;
	private String pd_season;
	private String pd_year;
	private String pd_style;
	private Integer pd_days;//上市天数
	
	private Integer da_try;
	private Double da_try_rate;//试穿率
	private Integer da_receive;
	private Double da_receive_rate;//接待率
	private Integer shl_vips;
	private Integer shl_count;
	
	private Double shl_profits;//利润
	private Double shl_discountmonery;//让利金额
	private String vip_name;//会员名称
	private String ca_name;//收银员名称
	public Integer getShl_id() {
		return shl_id;
	}
	public void setShl_id(Integer shl_id) {
		this.shl_id = shl_id;
	}
	public String getShl_number() {
		return shl_number;
	}
	public void setShl_number(String shl_number) {
		this.shl_number = shl_number;
	}
	public String getShl_date() {
		return shl_date;
	}
	public void setShl_date(String shl_date) {
		this.shl_date = shl_date;
	}
	public String getShl_sysdate() {
		return shl_sysdate;
	}
	public void setShl_sysdate(String shl_sysdate) {
		this.shl_sysdate = shl_sysdate;
	}
	public String getShl_pd_code() {
		return shl_pd_code;
	}
	public void setShl_pd_code(String shl_pd_code) {
		this.shl_pd_code = shl_pd_code;
	}
	public String getShl_sub_code() {
		return shl_sub_code;
	}
	public void setShl_sub_code(String shl_sub_code) {
		this.shl_sub_code = shl_sub_code;
	}
	public String getShl_cr_code() {
		return shl_cr_code;
	}
	public void setShl_cr_code(String shl_cr_code) {
		this.shl_cr_code = shl_cr_code;
	}
	public String getShl_sz_code() {
		return shl_sz_code;
	}
	public void setShl_sz_code(String shl_sz_code) {
		this.shl_sz_code = shl_sz_code;
	}
	public String getShl_br_code() {
		return shl_br_code;
	}
	public void setShl_br_code(String shl_br_code) {
		this.shl_br_code = shl_br_code;
	}
	public String getShl_em_code() {
		return shl_em_code;
	}
	public void setShl_em_code(String shl_em_code) {
		this.shl_em_code = shl_em_code;
	}
	public String getShl_slave() {
		return shl_slave;
	}
	public void setShl_slave(String shl_slave) {
		this.shl_slave = shl_slave;
	}
	public String getShl_main() {
		return shl_main;
	}
	public void setShl_main(String shl_main) {
		this.shl_main = shl_main;
	}
	public String getShl_da_code() {
		return shl_da_code;
	}
	public void setShl_da_code(String shl_da_code) {
		this.shl_da_code = shl_da_code;
	}
	public String getShl_shop_code() {
		return shl_shop_code;
	}
	public void setShl_shop_code(String shl_shop_code) {
		this.shl_shop_code = shl_shop_code;
	}
	public String getShl_dp_code() {
		return shl_dp_code;
	}
	public void setShl_dp_code(String shl_dp_code) {
		this.shl_dp_code = shl_dp_code;
	}
	public Integer getShl_amount() {
		return shl_amount;
	}
	public void setShl_amount(Integer shl_amount) {
		this.shl_amount = shl_amount;
	}
	public Double getShl_sell_price() {
		return shl_sell_price;
	}
	public void setShl_sell_price(Double shl_sell_price) {
		this.shl_sell_price = shl_sell_price;
	}
	public Double getShl_price() {
		return shl_price;
	}
	public void setShl_price(Double shl_price) {
		this.shl_price = shl_price;
	}
	public Double getShl_cost_price() {
		return shl_cost_price;
	}
	public void setShl_cost_price(Double shl_cost_price) {
		this.shl_cost_price = shl_cost_price;
	}
	public Double getShl_upcost_price() {
		return shl_upcost_price;
	}
	public void setShl_upcost_price(Double shl_upcost_price) {
		this.shl_upcost_price = shl_upcost_price;
	}
	public Double getShl_money() {
		return shl_money;
	}
	public void setShl_money(Double shl_money) {
		this.shl_money = shl_money;
	}
	public Integer getShl_state() {
		return shl_state;
	}
	public void setShl_state(Integer shl_state) {
		this.shl_state = shl_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getShl_vip_code() {
		return shl_vip_code;
	}
	public void setShl_vip_code(String shl_vip_code) {
		this.shl_vip_code = shl_vip_code;
	}
	public String getShl_sale_code() {
		return shl_sale_code;
	}
	public void setShl_sale_code(String shl_sale_code) {
		this.shl_sale_code = shl_sale_code;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getEm_name() {
		return em_name;
	}
	public void setEm_name(String em_name) {
		this.em_name = em_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getSlave_name() {
		return slave_name;
	}
	public void setSlave_name(String slave_name) {
		this.slave_name = slave_name;
	}
	public String getMain_name() {
		return main_name;
	}
	public void setMain_name(String main_name) {
		this.main_name = main_name;
	}
	public String getDa_name() {
		return da_name;
	}
	public void setDa_name(String da_name) {
		this.da_name = da_name;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Double getShl_sign_price() {
		return shl_sign_price;
	}
	public void setShl_sign_price(Double shl_sign_price) {
		this.shl_sign_price = shl_sign_price;
	}
	public String getPd_img_path() {
		return pd_img_path;
	}
	public void setPd_img_path(String pd_img_path) {
		this.pd_img_path = pd_img_path;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public String getPd_fabric() {
		return pd_fabric;
	}
	public void setPd_fabric(String pd_fabric) {
		this.pd_fabric = pd_fabric;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public String getPd_year() {
		return pd_year;
	}
	public void setPd_year(String pd_year) {
		this.pd_year = pd_year;
	}
	public Double getRetailMoney() {
		return retailMoney;
	}
	public void setRetailMoney(Double retailMoney) {
		this.retailMoney = retailMoney;
	}
	public Double getPd_sign_price() {
		return pd_sign_price;
	}
	public void setPd_sign_price(Double pd_sign_price) {
		this.pd_sign_price = pd_sign_price;
	}
	public Double getShl_sell_money() {
		return shl_sell_money;
	}
	public void setShl_sell_money(Double shl_sell_money) {
		this.shl_sell_money = shl_sell_money;
	}
	public Double getShl_cost_money() {
		return shl_cost_money;
	}
	public void setShl_cost_money(Double shl_cost_money) {
		this.shl_cost_money = shl_cost_money;
	}
	public Double getShl_profit() {
		return shl_profit;
	}
	public void setShl_profit(Double shl_profit) {
		this.shl_profit = shl_profit;
	}
	public Double getShl_avg_price() {
		return shl_avg_price;
	}
	public void setShl_avg_price(Double shl_avg_price) {
		this.shl_avg_price = shl_avg_price;
	}
	public Double getShl_rate() {
		return shl_rate;
	}
	public void setShl_rate(Double shl_rate) {
		this.shl_rate = shl_rate;
	}
	public Double getShl_profit_rate() {
		return shl_profit_rate;
	}
	public void setShl_profit_rate(Double shl_profit_rate) {
		this.shl_profit_rate = shl_profit_rate;
	}
	public Double getShl_profit_ratio() {
		return shl_profit_ratio;
	}
	public void setShl_profit_ratio(Double shl_profit_ratio) {
		this.shl_profit_ratio = shl_profit_ratio;
	}
	public String getPd_date() {
		return pd_date;
	}
	public void setPd_date(String pd_date) {
		this.pd_date = pd_date;
	}
	public Integer getDa_try() {
		return da_try;
	}
	public void setDa_try(Integer da_try) {
		this.da_try = da_try;
	}
	public Integer getDa_receive() {
		return da_receive;
	}
	public void setDa_receive(Integer da_receive) {
		this.da_receive = da_receive;
	}
	public Integer getShl_vips() {
		return shl_vips;
	}
	public void setShl_vips(Integer shl_vips) {
		this.shl_vips = shl_vips;
	}
	public Integer getShl_count() {
		return shl_count;
	}
	public void setShl_count(Integer shl_count) {
		this.shl_count = shl_count;
	}
	public Double getShl_profits() {
		return shl_profits;
	}
	public void setShl_profits(Double shl_profits) {
		this.shl_profits = shl_profits;
	}
	public Double getShl_discountmonery() {
		return shl_discountmonery;
	}
	public void setShl_discountmonery(Double shl_discountmonery) {
		this.shl_discountmonery = shl_discountmonery;
	}
	public String getVip_name() {
		return vip_name;
	}
	public void setVip_name(String vip_name) {
		this.vip_name = vip_name;
	}
	public String getCa_name() {
		return ca_name;
	}
	public void setCa_name(String ca_name) {
		this.ca_name = ca_name;
	}
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public Double getDa_try_rate() {
		return da_try_rate;
	}
	public void setDa_try_rate(Double da_try_rate) {
		this.da_try_rate = da_try_rate;
	}
	public Double getDa_receive_rate() {
		return da_receive_rate;
	}
	public void setDa_receive_rate(Double da_receive_rate) {
		this.da_receive_rate = da_receive_rate;
	}
	public String getPd_style() {
		return pd_style;
	}
	public void setPd_style(String pd_style) {
		this.pd_style = pd_style;
	}
	public Integer getPd_days() {
		return pd_days;
	}
	public void setPd_days(Integer pd_days) {
		this.pd_days = pd_days;
	}
	public Double getShl_price_rate() {
		return shl_price_rate;
	}
	public void setShl_price_rate(Double shl_price_rate) {
		this.shl_price_rate = shl_price_rate;
	}
}
