package zy.entity.sell.cashier;

import java.io.Serializable;

public class T_Sell_Cashier_Set implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer cs_id;
	private String cs_key;
	private String cs_value;
	private String cs_em_code;
	private Integer companyid;
	public Integer getCs_id() {
		return cs_id;
	}
	public void setCs_id(Integer cs_id) {
		this.cs_id = cs_id;
	}
	public String getCs_key() {
		return cs_key;
	}
	public void setCs_key(String cs_key) {
		this.cs_key = cs_key;
	}
	public String getCs_value() {
		return cs_value;
	}
	public void setCs_value(String cs_value) {
		this.cs_value = cs_value;
	}
	public String getCs_em_code() {
		return cs_em_code;
	}
	public void setCs_em_code(String cs_em_code) {
		this.cs_em_code = cs_em_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	
}
