package zy.entity.sell.coupon;

import java.io.Serializable;

public class T_Sell_Coupon implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer sc_id;
	private String sc_number;
	private String sc_return_number;
	private String sc_name;
	private String sc_shop_code;
	private String sc_shop_name;
	private Double sc_full_money;
	private Double sc_minus_money;
	private Integer sc_type;
	private Integer sc_type_bak;
	private Integer sc_amount;
	private Integer sc_receive_amount;
	private String sc_begindate;
	private String sc_enddate;
	private String sc_remark;
	private Integer sc_state;
	private String sc_maker;
	private String sc_makedate;
	private String sc_sysdate;
	private Integer sc_us_id;
	private Integer sc_show_sysinfo;
	private Integer sc_show_chat;
	private Integer companyid;
	private String codes;
	public Integer getSc_id() {
		return sc_id;
	}
	public void setSc_id(Integer sc_id) {
		this.sc_id = sc_id;
	}
	public String getSc_number() {
		return sc_number;
	}
	public void setSc_number(String sc_number) {
		this.sc_number = sc_number;
	}
	public String getSc_name() {
		return sc_name;
	}
	public void setSc_name(String sc_name) {
		this.sc_name = sc_name;
	}
	public String getSc_shop_code() {
		return sc_shop_code;
	}
	public void setSc_shop_code(String sc_shop_code) {
		this.sc_shop_code = sc_shop_code;
	}
	public Double getSc_full_money() {
		return sc_full_money;
	}
	public void setSc_full_money(Double sc_full_money) {
		this.sc_full_money = sc_full_money;
	}
	public Double getSc_minus_money() {
		return sc_minus_money;
	}
	public void setSc_minus_money(Double sc_minus_money) {
		this.sc_minus_money = sc_minus_money;
	}
	public Integer getSc_type() {
		return sc_type;
	}
	public void setSc_type(Integer sc_type) {
		this.sc_type = sc_type;
	}
	public Integer getSc_amount() {
		return sc_amount;
	}
	public void setSc_amount(Integer sc_amount) {
		this.sc_amount = sc_amount;
	}
	public Integer getSc_receive_amount() {
		return sc_receive_amount;
	}
	public void setSc_receive_amount(Integer sc_receive_amount) {
		this.sc_receive_amount = sc_receive_amount;
	}
	public String getSc_begindate() {
		return sc_begindate;
	}
	public void setSc_begindate(String sc_begindate) {
		this.sc_begindate = sc_begindate;
	}
	public String getSc_enddate() {
		return sc_enddate;
	}
	public void setSc_enddate(String sc_enddate) {
		this.sc_enddate = sc_enddate;
	}
	public String getSc_remark() {
		return sc_remark;
	}
	public void setSc_remark(String sc_remark) {
		this.sc_remark = sc_remark;
	}
	public Integer getSc_state() {
		return sc_state;
	}
	public void setSc_state(Integer sc_state) {
		this.sc_state = sc_state;
	}
	public String getSc_maker() {
		return sc_maker;
	}
	public void setSc_maker(String sc_maker) {
		this.sc_maker = sc_maker;
	}
	public String getSc_makedate() {
		return sc_makedate;
	}
	public void setSc_makedate(String sc_makedate) {
		this.sc_makedate = sc_makedate;
	}
	public String getSc_sysdate() {
		return sc_sysdate;
	}
	public void setSc_sysdate(String sc_sysdate) {
		this.sc_sysdate = sc_sysdate;
	}
	public Integer getSc_us_id() {
		return sc_us_id;
	}
	public void setSc_us_id(Integer sc_us_id) {
		this.sc_us_id = sc_us_id;
	}
	public Integer getSc_show_sysinfo() {
		return sc_show_sysinfo;
	}
	public void setSc_show_sysinfo(Integer sc_show_sysinfo) {
		this.sc_show_sysinfo = sc_show_sysinfo;
	}
	public Integer getSc_show_chat() {
		return sc_show_chat;
	}
	public void setSc_show_chat(Integer sc_show_chat) {
		this.sc_show_chat = sc_show_chat;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSc_shop_name() {
		return sc_shop_name;
	}
	public void setSc_shop_name(String sc_shop_name) {
		this.sc_shop_name = sc_shop_name;
	}
	public String getCodes() {
		return codes;
	}
	public void setCodes(String codes) {
		this.codes = codes;
	}
	public String getSc_return_number() {
		return sc_return_number;
	}
	public void setSc_return_number(String sc_return_number) {
		this.sc_return_number = sc_return_number;
	}
	public Integer getSc_type_bak() {
		return sc_type_bak;
	}
	public void setSc_type_bak(Integer sc_type_bak) {
		this.sc_type_bak = sc_type_bak;
	}
}
