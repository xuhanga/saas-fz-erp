package zy.entity.sell.coupon;

import java.io.Serializable;

public class T_Sell_Coupon_Brand implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer scb_id;
	private String scb_sc_number;
	private String scb_bd_code;
	private Integer companyid;
	public Integer getScb_id() {
		return scb_id;
	}
	public void setScb_id(Integer scb_id) {
		this.scb_id = scb_id;
	}
	public String getScb_sc_number() {
		return scb_sc_number;
	}
	public void setScb_sc_number(String scb_sc_number) {
		this.scb_sc_number = scb_sc_number;
	}
	public String getScb_bd_code() {
		return scb_bd_code;
	}
	public void setScb_bd_code(String scb_bd_code) {
		this.scb_bd_code = scb_bd_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
