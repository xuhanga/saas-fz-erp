package zy.entity.sell.coupon;

import java.io.Serializable;

public class T_Sell_Coupon_Type implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer sct_id;
	private String sct_sc_number;
	private String sct_tp_code;
	private Integer companyid;
	public Integer getSct_id() {
		return sct_id;
	}
	public void setSct_id(Integer sct_id) {
		this.sct_id = sct_id;
	}
	public String getSct_sc_number() {
		return sct_sc_number;
	}
	public void setSct_sc_number(String sct_sc_number) {
		this.sct_sc_number = sct_sc_number;
	}
	public String getSct_tp_code() {
		return sct_tp_code;
	}
	public void setSct_tp_code(String sct_tp_code) {
		this.sct_tp_code = sct_tp_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
