package zy.entity.sell.day;

import java.io.Serializable;

public class T_Sell_Day implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer da_id;
	private String da_date;
	private String da_shop_code;
	private String em_code;
	private Integer companyid;
	private Integer da_come;
	private Integer da_receive;
	private Integer da_try;
	private String da_type;
	private Integer sh_amount;
	private Double sh_money;
	private Integer sh_count;
	public Integer getDa_id() {
		return da_id;
	}
	public void setDa_id(Integer da_id) {
		this.da_id = da_id;
	}
	public String getDa_date() {
		return da_date;
	}
	public void setDa_date(String da_date) {
		this.da_date = da_date;
	}
	public String getDa_shop_code() {
		return da_shop_code;
	}
	public void setDa_shop_code(String da_shop_code) {
		this.da_shop_code = da_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getDa_come() {
		return da_come;
	}
	public void setDa_come(Integer da_come) {
		this.da_come = da_come;
	}
	public Integer getDa_receive() {
		return da_receive;
	}
	public void setDa_receive(Integer da_receive) {
		this.da_receive = da_receive;
	}
	public Integer getDa_try() {
		return da_try;
	}
	public void setDa_try(Integer da_try) {
		this.da_try = da_try;
	}
	public Integer getSh_amount() {
		return sh_amount;
	}
	public void setSh_amount(Integer sh_amount) {
		this.sh_amount = sh_amount;
	}
	public Double getSh_money() {
		return sh_money;
	}
	public void setSh_money(Double sh_money) {
		this.sh_money = sh_money;
	}
	public Integer getSh_count() {
		return sh_count;
	}
	public void setSh_count(Integer sh_count) {
		this.sh_count = sh_count;
	}
	public String getDa_type() {
		return da_type;
	}
	public void setDa_type(String da_type) {
		this.da_type = da_type;
	}
	public String getEm_code() {
		return em_code;
	}
	public void setEm_code(String em_code) {
		this.em_code = em_code;
	}
	
}
