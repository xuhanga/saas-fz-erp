package zy.entity.sell.deposit;

import java.io.Serializable;

public class T_Sell_Deposit implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sd_id;
	private String sd_number;
	private String sd_date;
	private String sd_enddate;
	private Integer sd_state;
	private String sd_customer;
	private String sd_tel;
	private Double sd_deposit;
	private String sd_em_code;
	private String emp_name;
	private String sd_shop_code;
	private String sd_remark;
	private Integer companyid;
	public Integer getSd_id() {
		return sd_id;
	}
	public void setSd_id(Integer sd_id) {
		this.sd_id = sd_id;
	}
	public String getSd_number() {
		return sd_number;
	}
	public void setSd_number(String sd_number) {
		this.sd_number = sd_number;
	}
	public String getSd_date() {
		return sd_date;
	}
	public void setSd_date(String sd_date) {
		this.sd_date = sd_date;
	}
	public Integer getSd_state() {
		return sd_state;
	}
	public void setSd_state(Integer sd_state) {
		this.sd_state = sd_state;
	}
	public String getSd_customer() {
		return sd_customer;
	}
	public void setSd_customer(String sd_customer) {
		this.sd_customer = sd_customer;
	}
	public String getSd_tel() {
		return sd_tel;
	}
	public void setSd_tel(String sd_tel) {
		this.sd_tel = sd_tel;
	}
	public Double getSd_deposit() {
		return sd_deposit;
	}
	public void setSd_deposit(Double sd_deposit) {
		this.sd_deposit = sd_deposit;
	}
	public String getSd_shop_code() {
		return sd_shop_code;
	}
	public void setSd_shop_code(String sd_shop_code) {
		this.sd_shop_code = sd_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSd_em_code() {
		return sd_em_code;
	}
	public void setSd_em_code(String sd_em_code) {
		this.sd_em_code = sd_em_code;
	}
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public String getSd_remark() {
		return sd_remark;
	}
	public void setSd_remark(String sd_remark) {
		this.sd_remark = sd_remark;
	}
	public String getSd_enddate() {
		return sd_enddate;
	}
	public void setSd_enddate(String sd_enddate) {
		this.sd_enddate = sd_enddate;
	}
	
}
