package zy.entity.sell.deposit;

import java.io.Serializable;

public class T_Sell_DepositList implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sdl_id;
	private String sdl_number;
	private String sdl_pd_code;
	private String sdl_sub_code;
	private String sdl_cr_code;
	private String sdl_sz_code;
	private String sdl_br_code;
	private String sdl_tp_code;
	private String sdl_shop_code;
	private String sdl_em_code;
	private String sdl_bd_code;
	private String sdl_main;
	private String sdl_slave;
	private String sdl_area;
	private String sdl_vip_code;
	private Integer sdl_amount;
	private Double sdl_sell_price;
	private Double sdl_sign_price;
	private Double sdl_price;
	private Double sdl_cost_price;
	private Double sdl_upcost_price;
	private Double sdl_money;
	private Double sdl_vip_money;
	private Double sdl_hand_money;
	private Double sdl_sale_money;
	private String sdl_sale_model;
	private String sdl_sale_code;
	private Integer sdl_ispoint;
	private Integer sdl_isgift;
	private String sdl_remark;
	private Integer sdl_state;
	private Integer sdl_ismessage;
	private Integer companyid;
	private String sdl_cr_name;
	private String sdl_sz_name;
	private String sdl_br_name;
	private String sdl_pd_name;
	private String sdl_pd_no;
	private String sd_customer;
	private String sd_tel;
	private Integer sd_state;
	private String sdl_sp_name;
	public Integer getSdl_id() {
		return sdl_id;
	}
	public void setSdl_id(Integer sdl_id) {
		this.sdl_id = sdl_id;
	}
	public String getSdl_number() {
		return sdl_number;
	}
	public void setSdl_number(String sdl_number) {
		this.sdl_number = sdl_number;
	}
	public String getSdl_pd_code() {
		return sdl_pd_code;
	}
	public void setSdl_pd_code(String sdl_pd_code) {
		this.sdl_pd_code = sdl_pd_code;
	}
	public String getSdl_sub_code() {
		return sdl_sub_code;
	}
	public void setSdl_sub_code(String sdl_sub_code) {
		this.sdl_sub_code = sdl_sub_code;
	}
	public String getSdl_cr_code() {
		return sdl_cr_code;
	}
	public void setSdl_cr_code(String sdl_cr_code) {
		this.sdl_cr_code = sdl_cr_code;
	}
	public String getSdl_sz_code() {
		return sdl_sz_code;
	}
	public void setSdl_sz_code(String sdl_sz_code) {
		this.sdl_sz_code = sdl_sz_code;
	}
	public String getSdl_br_code() {
		return sdl_br_code;
	}
	public void setSdl_br_code(String sdl_br_code) {
		this.sdl_br_code = sdl_br_code;
	}
	public String getSdl_tp_code() {
		return sdl_tp_code;
	}
	public void setSdl_tp_code(String sdl_tp_code) {
		this.sdl_tp_code = sdl_tp_code;
	}
	public String getSdl_shop_code() {
		return sdl_shop_code;
	}
	public void setSdl_shop_code(String sdl_shop_code) {
		this.sdl_shop_code = sdl_shop_code;
	}
	public String getSdl_em_code() {
		return sdl_em_code;
	}
	public void setSdl_em_code(String sdl_em_code) {
		this.sdl_em_code = sdl_em_code;
	}
	public String getSdl_bd_code() {
		return sdl_bd_code;
	}
	public void setSdl_bd_code(String sdl_bd_code) {
		this.sdl_bd_code = sdl_bd_code;
	}
	public String getSdl_main() {
		return sdl_main;
	}
	public void setSdl_main(String sdl_main) {
		this.sdl_main = sdl_main;
	}
	public String getSdl_slave() {
		return sdl_slave;
	}
	public void setSdl_slave(String sdl_slave) {
		this.sdl_slave = sdl_slave;
	}
	public String getSdl_area() {
		return sdl_area;
	}
	public void setSdl_area(String sdl_area) {
		this.sdl_area = sdl_area;
	}
	public String getSdl_vip_code() {
		return sdl_vip_code;
	}
	public void setSdl_vip_code(String sdl_vip_code) {
		this.sdl_vip_code = sdl_vip_code;
	}
	public Integer getSdl_amount() {
		return sdl_amount;
	}
	public void setSdl_amount(Integer sdl_amount) {
		this.sdl_amount = sdl_amount;
	}
	public Double getSdl_sell_price() {
		return sdl_sell_price;
	}
	public void setSdl_sell_price(Double sdl_sell_price) {
		this.sdl_sell_price = sdl_sell_price;
	}
	public Double getSdl_sign_price() {
		return sdl_sign_price;
	}
	public void setSdl_sign_price(Double sdl_sign_price) {
		this.sdl_sign_price = sdl_sign_price;
	}
	public Double getSdl_price() {
		return sdl_price;
	}
	public void setSdl_price(Double sdl_price) {
		this.sdl_price = sdl_price;
	}
	public Double getSdl_cost_price() {
		return sdl_cost_price;
	}
	public void setSdl_cost_price(Double sdl_cost_price) {
		this.sdl_cost_price = sdl_cost_price;
	}
	public Double getSdl_upcost_price() {
		return sdl_upcost_price;
	}
	public void setSdl_upcost_price(Double sdl_upcost_price) {
		this.sdl_upcost_price = sdl_upcost_price;
	}
	public Double getSdl_money() {
		return sdl_money;
	}
	public void setSdl_money(Double sdl_money) {
		this.sdl_money = sdl_money;
	}
	public Double getSdl_vip_money() {
		return sdl_vip_money;
	}
	public void setSdl_vip_money(Double sdl_vip_money) {
		this.sdl_vip_money = sdl_vip_money;
	}
	public Double getSdl_hand_money() {
		return sdl_hand_money;
	}
	public void setSdl_hand_money(Double sdl_hand_money) {
		this.sdl_hand_money = sdl_hand_money;
	}
	public Double getSdl_sale_money() {
		return sdl_sale_money;
	}
	public void setSdl_sale_money(Double sdl_sale_money) {
		this.sdl_sale_money = sdl_sale_money;
	}
	public String getSdl_sale_model() {
		return sdl_sale_model;
	}
	public void setSdl_sale_model(String sdl_sale_model) {
		this.sdl_sale_model = sdl_sale_model;
	}
	public String getSdl_sale_code() {
		return sdl_sale_code;
	}
	public void setSdl_sale_code(String sdl_sale_code) {
		this.sdl_sale_code = sdl_sale_code;
	}
	public Integer getSdl_ispoint() {
		return sdl_ispoint;
	}
	public void setSdl_ispoint(Integer sdl_ispoint) {
		this.sdl_ispoint = sdl_ispoint;
	}
	public Integer getSdl_isgift() {
		return sdl_isgift;
	}
	public void setSdl_isgift(Integer sdl_isgift) {
		this.sdl_isgift = sdl_isgift;
	}
	public String getSdl_remark() {
		return sdl_remark;
	}
	public void setSdl_remark(String sdl_remark) {
		this.sdl_remark = sdl_remark;
	}
	public Integer getSdl_state() {
		return sdl_state;
	}
	public void setSdl_state(Integer sdl_state) {
		this.sdl_state = sdl_state;
	}
	public Integer getSdl_ismessage() {
		return sdl_ismessage;
	}
	public void setSdl_ismessage(Integer sdl_ismessage) {
		this.sdl_ismessage = sdl_ismessage;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSdl_cr_name() {
		return sdl_cr_name;
	}
	public void setSdl_cr_name(String sdl_cr_name) {
		this.sdl_cr_name = sdl_cr_name;
	}
	public String getSdl_sz_name() {
		return sdl_sz_name;
	}
	public void setSdl_sz_name(String sdl_sz_name) {
		this.sdl_sz_name = sdl_sz_name;
	}
	public String getSdl_br_name() {
		return sdl_br_name;
	}
	public void setSdl_br_name(String sdl_br_name) {
		this.sdl_br_name = sdl_br_name;
	}
	public String getSdl_pd_name() {
		return sdl_pd_name;
	}
	public void setSdl_pd_name(String sdl_pd_name) {
		this.sdl_pd_name = sdl_pd_name;
	}
	public String getSdl_pd_no() {
		return sdl_pd_no;
	}
	public void setSdl_pd_no(String sdl_pd_no) {
		this.sdl_pd_no = sdl_pd_no;
	}
	public String getSd_customer() {
		return sd_customer;
	}
	public void setSd_customer(String sd_customer) {
		this.sd_customer = sd_customer;
	}
	public String getSd_tel() {
		return sd_tel;
	}
	public void setSd_tel(String sd_tel) {
		this.sd_tel = sd_tel;
	}
	public Integer getSd_state() {
		return sd_state;
	}
	public void setSd_state(Integer sd_state) {
		this.sd_state = sd_state;
	}
	public String getSdl_sp_name() {
		return sdl_sp_name;
	}
	public void setSdl_sp_name(String sdl_sp_name) {
		this.sdl_sp_name = sdl_sp_name;
	}
}
