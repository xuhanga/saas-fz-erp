package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_ECoupon_Brand implements Serializable{
	private static final long serialVersionUID = 6489415738486673194L;
	private Integer ecb_id;
	private String ecb_ec_number;
	private String ecb_bd_code;
	private Integer ecb_type;
	private Integer companyid;
	public Integer getEcb_id() {
		return ecb_id;
	}
	public void setEcb_id(Integer ecb_id) {
		this.ecb_id = ecb_id;
	}
	public String getEcb_ec_number() {
		return ecb_ec_number;
	}
	public void setEcb_ec_number(String ecb_ec_number) {
		this.ecb_ec_number = ecb_ec_number;
	}
	public String getEcb_bd_code() {
		return ecb_bd_code;
	}
	public void setEcb_bd_code(String ecb_bd_code) {
		this.ecb_bd_code = ecb_bd_code;
	}
	public Integer getEcb_type() {
		return ecb_type;
	}
	public void setEcb_type(Integer ecb_type) {
		this.ecb_type = ecb_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
