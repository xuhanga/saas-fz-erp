package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_ECoupon_Product implements Serializable{
	private static final long serialVersionUID = 3818599255383179815L;
	private Integer ecp_id;
	private String ecp_ec_number;
	private String ecp_pd_code;
	private Integer ecp_type;
	private Integer companyid;
	public Integer getEcp_id() {
		return ecp_id;
	}
	public void setEcp_id(Integer ecp_id) {
		this.ecp_id = ecp_id;
	}
	public String getEcp_ec_number() {
		return ecp_ec_number;
	}
	public void setEcp_ec_number(String ecp_ec_number) {
		this.ecp_ec_number = ecp_ec_number;
	}
	public String getEcp_pd_code() {
		return ecp_pd_code;
	}
	public void setEcp_pd_code(String ecp_pd_code) {
		this.ecp_pd_code = ecp_pd_code;
	}
	public Integer getEcp_type() {
		return ecp_type;
	}
	public void setEcp_type(Integer ecp_type) {
		this.ecp_type = ecp_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
