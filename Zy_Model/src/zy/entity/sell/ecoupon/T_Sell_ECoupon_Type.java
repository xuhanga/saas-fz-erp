package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_ECoupon_Type implements Serializable{
	private static final long serialVersionUID = 1543673837426997184L;
	private Integer ect_id;
	private String ect_ec_number;
	private String ect_tp_code;
	private Integer ect_type;
	private Integer companyid;
	public Integer getEct_id() {
		return ect_id;
	}
	public void setEct_id(Integer ect_id) {
		this.ect_id = ect_id;
	}
	public String getEct_ec_number() {
		return ect_ec_number;
	}
	public void setEct_ec_number(String ect_ec_number) {
		this.ect_ec_number = ect_ec_number;
	}
	public String getEct_tp_code() {
		return ect_tp_code;
	}
	public void setEct_tp_code(String ect_tp_code) {
		this.ect_tp_code = ect_tp_code;
	}
	public Integer getEct_type() {
		return ect_type;
	}
	public void setEct_type(Integer ect_type) {
		this.ect_type = ect_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
