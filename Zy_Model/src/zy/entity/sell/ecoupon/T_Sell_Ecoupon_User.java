package zy.entity.sell.ecoupon;

import java.io.Serializable;

public class T_Sell_Ecoupon_User implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer ecu_id;
	private String pd_code;
	private String bd_code;
	private String tp_code;
	private String use_model;//使用范围类型
	private Integer use_state;
	private String ecu_ec_number;
	private String ecu_code;
	private String ecu_sh_number;
	private String ecu_date;
	private String ecu_name;
	private String ecu_tel;
	private String ecu_vip_code;
	private String vip_cardcode;
	private Double ecu_money;
	private Double ecu_limitmoney;
	private String ecu_shop_code;
	private String ecu_begindate;
	private String ecu_enddate;
	private Integer ecu_state;
	private Double ecu_use_number;
	private String ecu_use_date;
	private Integer ecu_use_type;
	private String ecu_check_no;
	private Integer companyid;
	public Integer getEcu_id() {
		return ecu_id;
	}
	public void setEcu_id(Integer ecu_id) {
		this.ecu_id = ecu_id;
	}
	public String getEcu_ec_number() {
		return ecu_ec_number;
	}
	public void setEcu_ec_number(String ecu_ec_number) {
		this.ecu_ec_number = ecu_ec_number;
	}
	public String getEcu_code() {
		return ecu_code;
	}
	public void setEcu_code(String ecu_code) {
		this.ecu_code = ecu_code;
	}
	public String getEcu_sh_number() {
		return ecu_sh_number;
	}
	public void setEcu_sh_number(String ecu_sh_number) {
		this.ecu_sh_number = ecu_sh_number;
	}
	public String getEcu_date() {
		return ecu_date;
	}
	public void setEcu_date(String ecu_date) {
		this.ecu_date = ecu_date;
	}
	public String getEcu_name() {
		return ecu_name;
	}
	public void setEcu_name(String ecu_name) {
		this.ecu_name = ecu_name;
	}
	public String getEcu_tel() {
		return ecu_tel;
	}
	public void setEcu_tel(String ecu_tel) {
		this.ecu_tel = ecu_tel;
	}
	public String getEcu_vip_code() {
		return ecu_vip_code;
	}
	public void setEcu_vip_code(String ecu_vip_code) {
		this.ecu_vip_code = ecu_vip_code;
	}
	public Double getEcu_money() {
		return ecu_money;
	}
	public void setEcu_money(Double ecu_money) {
		this.ecu_money = ecu_money;
	}
	public Double getEcu_limitmoney() {
		return ecu_limitmoney;
	}
	public void setEcu_limitmoney(Double ecu_limitmoney) {
		this.ecu_limitmoney = ecu_limitmoney;
	}
	public String getEcu_shop_code() {
		return ecu_shop_code;
	}
	public void setEcu_shop_code(String ecu_shop_code) {
		this.ecu_shop_code = ecu_shop_code;
	}
	public String getEcu_begindate() {
		return ecu_begindate;
	}
	public void setEcu_begindate(String ecu_begindate) {
		this.ecu_begindate = ecu_begindate;
	}
	public String getEcu_enddate() {
		return ecu_enddate;
	}
	public void setEcu_enddate(String ecu_enddate) {
		this.ecu_enddate = ecu_enddate;
	}
	public Integer getEcu_state() {
		return ecu_state;
	}
	public void setEcu_state(Integer ecu_state) {
		this.ecu_state = ecu_state;
	}
	public Double getEcu_use_number() {
		return ecu_use_number;
	}
	public void setEcu_use_number(Double ecu_use_number) {
		this.ecu_use_number = ecu_use_number;
	}
	public String getEcu_use_date() {
		return ecu_use_date;
	}
	public void setEcu_use_date(String ecu_use_date) {
		this.ecu_use_date = ecu_use_date;
	}
	public Integer getEcu_use_type() {
		return ecu_use_type;
	}
	public void setEcu_use_type(Integer ecu_use_type) {
		this.ecu_use_type = ecu_use_type;
	}
	public String getEcu_check_no() {
		return ecu_check_no;
	}
	public void setEcu_check_no(String ecu_check_no) {
		this.ecu_check_no = ecu_check_no;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public String getUse_model() {
		return use_model;
	}
	public void setUse_model(String use_model) {
		this.use_model = use_model;
	}
	public Integer getUse_state() {
		return use_state;
	}
	public void setUse_state(Integer use_state) {
		this.use_state = use_state;
	}
	public String getVip_cardcode() {
		return vip_cardcode;
	}
	public void setVip_cardcode(String vip_cardcode) {
		this.vip_cardcode = vip_cardcode;
	}

}
