package zy.entity.sell.set;

import java.io.Serializable;

public class T_Sell_PrintData implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer spd_id;
	private String spd_code;
	private String spd_name;
	private String spd_namecustom;
	private Integer spd_order;
	private Integer spd_show;
	private Integer spd_width;
	private Integer spd_align;
	private String spd_sp_code;
	private Integer companyid;
	public Integer getSpd_id() {
		return spd_id;
	}
	public void setSpd_id(Integer spd_id) {
		this.spd_id = spd_id;
	}
	public String getSpd_code() {
		return spd_code;
	}
	public void setSpd_code(String spd_code) {
		this.spd_code = spd_code;
	}
	public String getSpd_name() {
		return spd_name;
	}
	public void setSpd_name(String spd_name) {
		this.spd_name = spd_name;
	}
	public String getSpd_namecustom() {
		return spd_namecustom;
	}
	public void setSpd_namecustom(String spd_namecustom) {
		this.spd_namecustom = spd_namecustom;
	}
	public Integer getSpd_order() {
		return spd_order;
	}
	public void setSpd_order(Integer spd_order) {
		this.spd_order = spd_order;
	}
	public Integer getSpd_show() {
		return spd_show;
	}
	public void setSpd_show(Integer spd_show) {
		this.spd_show = spd_show;
	}
	public Integer getSpd_width() {
		return spd_width;
	}
	public void setSpd_width(Integer spd_width) {
		this.spd_width = spd_width;
	}
	public Integer getSpd_align() {
		return spd_align;
	}
	public void setSpd_align(Integer spd_align) {
		this.spd_align = spd_align;
	}
	public String getSpd_sp_code() {
		return spd_sp_code;
	}
	public void setSpd_sp_code(String spd_sp_code) {
		this.spd_sp_code = spd_sp_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
