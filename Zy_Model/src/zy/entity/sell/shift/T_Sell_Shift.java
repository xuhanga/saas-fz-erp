package zy.entity.sell.shift;

import java.io.Serializable;

public class T_Sell_Shift implements Serializable{
	private static final long serialVersionUID = -2743950803102010712L;
	private Integer st_id;
	private String st_code;
	private String st_name;
	private String st_shop_code;
	private String shop_name;
	private String st_begintime;
	private String st_endtime;
	private Integer companyid;
	public Integer getSt_id() {
		return st_id;
	}
	public void setSt_id(Integer st_id) {
		this.st_id = st_id;
	}
	public String getSt_code() {
		return st_code;
	}
	public void setSt_code(String st_code) {
		this.st_code = st_code;
	}
	public String getSt_name() {
		return st_name;
	}
	public void setSt_name(String st_name) {
		this.st_name = st_name;
	}
	public String getSt_shop_code() {
		return st_shop_code;
	}
	public void setSt_shop_code(String st_shop_code) {
		this.st_shop_code = st_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getSt_begintime() {
		return st_begintime;
	}
	public void setSt_begintime(String st_begintime) {
		this.st_begintime = st_begintime;
	}
	public String getSt_endtime() {
		return st_endtime;
	}
	public void setSt_endtime(String st_endtime) {
		this.st_endtime = st_endtime;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
