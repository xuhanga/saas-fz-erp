package zy.entity.sell.voucher;

import java.io.Serializable;

public class T_Sell_VoucherList implements Serializable{
	private static final long serialVersionUID = 429130269246304099L;
	private Integer vcl_id;
	private String vcl_vc_code;
	private String vcl_cardcode;
	private Double vcl_money;
	private Double vcl_cash;
	private String vcl_date;
	private Integer vcl_type;
	private String vcl_shop_code;
	private String shop_name;
	private String vcl_ba_code;
	private String bank_name;
	private String vcl_number;
	private String vcl_manager;
	private Integer companyid;
	public Integer getVcl_id() {
		return vcl_id;
	}
	public void setVcl_id(Integer vcl_id) {
		this.vcl_id = vcl_id;
	}
	public String getVcl_vc_code() {
		return vcl_vc_code;
	}
	public void setVcl_vc_code(String vcl_vc_code) {
		this.vcl_vc_code = vcl_vc_code;
	}
	public String getVcl_cardcode() {
		return vcl_cardcode;
	}
	public void setVcl_cardcode(String vcl_cardcode) {
		this.vcl_cardcode = vcl_cardcode;
	}
	public Double getVcl_money() {
		return vcl_money;
	}
	public void setVcl_money(Double vcl_money) {
		this.vcl_money = vcl_money;
	}
	public Double getVcl_cash() {
		return vcl_cash;
	}
	public void setVcl_cash(Double vcl_cash) {
		this.vcl_cash = vcl_cash;
	}
	public String getVcl_date() {
		return vcl_date;
	}
	public void setVcl_date(String vcl_date) {
		this.vcl_date = vcl_date;
	}
	public Integer getVcl_type() {
		return vcl_type;
	}
	public void setVcl_type(Integer vcl_type) {
		this.vcl_type = vcl_type;
	}
	public String getVcl_shop_code() {
		return vcl_shop_code;
	}
	public void setVcl_shop_code(String vcl_shop_code) {
		this.vcl_shop_code = vcl_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getVcl_ba_code() {
		return vcl_ba_code;
	}
	public void setVcl_ba_code(String vcl_ba_code) {
		this.vcl_ba_code = vcl_ba_code;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getVcl_number() {
		return vcl_number;
	}
	public void setVcl_number(String vcl_number) {
		this.vcl_number = vcl_number;
	}
	public String getVcl_manager() {
		return vcl_manager;
	}
	public void setVcl_manager(String vcl_manager) {
		this.vcl_manager = vcl_manager;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
