package zy.entity.shop.complaint;

import java.io.Serializable;

public class T_Shop_Complaint implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sc_id;
	private String sc_username;
	private String sc_mobile;
	private String sc_sysdate;
	private String sc_content;
	private String sc_processname;
	private String sc_acceptdate;
	private String sc_processinfo;
	private Integer sc_state;
	private String sc_shop_code;
	private String sc_shop_name;
	private Integer sc_us_id;
	private Integer sc_satis;
	private Integer sc_type;
	private String sc_enddate;
	private String sc_selman;
	private Integer companyid;
	public Integer getSc_id() {
		return sc_id;
	}
	public void setSc_id(Integer sc_id) {
		this.sc_id = sc_id;
	}
	public String getSc_username() {
		return sc_username;
	}
	public void setSc_username(String sc_username) {
		this.sc_username = sc_username;
	}
	public String getSc_mobile() {
		return sc_mobile;
	}
	public void setSc_mobile(String sc_mobile) {
		this.sc_mobile = sc_mobile;
	}
	public String getSc_sysdate() {
		return sc_sysdate;
	}
	public void setSc_sysdate(String sc_sysdate) {
		this.sc_sysdate = sc_sysdate;
	}
	public String getSc_content() {
		return sc_content;
	}
	public void setSc_content(String sc_content) {
		this.sc_content = sc_content;
	}
	public String getSc_processname() {
		return sc_processname;
	}
	public void setSc_processname(String sc_processname) {
		this.sc_processname = sc_processname;
	}
	public String getSc_acceptdate() {
		return sc_acceptdate;
	}
	public void setSc_acceptdate(String sc_acceptdate) {
		this.sc_acceptdate = sc_acceptdate;
	}
	public String getSc_processinfo() {
		return sc_processinfo;
	}
	public void setSc_processinfo(String sc_processinfo) {
		this.sc_processinfo = sc_processinfo;
	}
	public Integer getSc_state() {
		return sc_state;
	}
	public void setSc_state(Integer sc_state) {
		this.sc_state = sc_state;
	}
	public String getSc_shop_code() {
		return sc_shop_code;
	}
	public void setSc_shop_code(String sc_shop_code) {
		this.sc_shop_code = sc_shop_code;
	}
	public Integer getSc_us_id() {
		return sc_us_id;
	}
	public void setSc_us_id(Integer sc_us_id) {
		this.sc_us_id = sc_us_id;
	}
	public Integer getSc_satis() {
		return sc_satis;
	}
	public void setSc_satis(Integer sc_satis) {
		this.sc_satis = sc_satis;
	}
	public Integer getSc_type() {
		return sc_type;
	}
	public void setSc_type(Integer sc_type) {
		this.sc_type = sc_type;
	}
	public String getSc_enddate() {
		return sc_enddate;
	}
	public void setSc_enddate(String sc_enddate) {
		this.sc_enddate = sc_enddate;
	}
	public String getSc_selman() {
		return sc_selman;
	}
	public void setSc_selman(String sc_selman) {
		this.sc_selman = sc_selman;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSc_shop_name() {
		return sc_shop_name;
	}
	public void setSc_shop_name(String sc_shop_name) {
		this.sc_shop_name = sc_shop_name;
	}
}
