package zy.entity.shop.gift;

import java.io.Serializable;

public class T_Shop_GiftList extends T_Shop_Gift implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer gil_id;
	private String gil_sub_code;
	private String gil_pd_code;
	private String gil_cr_code;
	private String gil_sz_code;
	private String gil_szg_code;
	private String gil_bs_code;
	private Integer gil_totalamount;
	private Integer gil_getamount;
	private Integer gil_amount;
	private String gil_sysdate;
	private String gil_shop_code;
	private String gil_user_code;
	private String gil_vm_code;
	private Integer companyid;
	private String pd_no;
	private String pd_code;
	private String pd_name;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String pd_unit;
	private Integer pi_score;
	private String operateType;
	private Integer gil_addamount;
	private Integer gil_point;
	private Integer sd_amount;
	private Integer gil_lastamount;//剩余数量
	public Integer getGil_id() {
		return gil_id;
	}
	public void setGil_id(Integer gil_id) {
		this.gil_id = gil_id;
	}
	public String getGil_pd_code() {
		return gil_pd_code;
	}
	public void setGil_pd_code(String gil_pd_code) {
		this.gil_pd_code = gil_pd_code;
	}
	public String getGil_cr_code() {
		return gil_cr_code;
	}
	public void setGil_cr_code(String gil_cr_code) {
		this.gil_cr_code = gil_cr_code;
	}
	public String getGil_sz_code() {
		return gil_sz_code;
	}
	public void setGil_sz_code(String gil_sz_code) {
		this.gil_sz_code = gil_sz_code;
	}
	public String getGil_bs_code() {
		return gil_bs_code;
	}
	public void setGil_bs_code(String gil_bs_code) {
		this.gil_bs_code = gil_bs_code;
	}
	public Integer getGil_totalamount() {
		return gil_totalamount;
	}
	public void setGil_totalamount(Integer gil_totalamount) {
		this.gil_totalamount = gil_totalamount;
	}
	public Integer getGil_getamount() {
		return gil_getamount;
	}
	public void setGil_getamount(Integer gil_getamount) {
		this.gil_getamount = gil_getamount;
	}
	public String getGil_sysdate() {
		return gil_sysdate;
	}
	public void setGil_sysdate(String gil_sysdate) {
		this.gil_sysdate = gil_sysdate;
	}
	public String getGil_shop_code() {
		return gil_shop_code;
	}
	public void setGil_shop_code(String gil_shop_code) {
		this.gil_shop_code = gil_shop_code;
	}
	public String getGil_user_code() {
		return gil_user_code;
	}
	public void setGil_user_code(String gil_user_code) {
		this.gil_user_code = gil_user_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getGil_szg_code() {
		return gil_szg_code;
	}
	public void setGil_szg_code(String gil_szg_code) {
		this.gil_szg_code = gil_szg_code;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public Integer getPi_score() {
		return pi_score;
	}
	public void setPi_score(Integer pi_score) {
		this.pi_score = pi_score;
	}
	public String getOperateType() {
		return operateType;
	}
	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	public Integer getGil_addamount() {
		return gil_addamount;
	}
	public void setGil_addamount(Integer gil_addamount) {
		this.gil_addamount = gil_addamount;
	}
	public Integer getGil_point() {
		return gil_point;
	}
	public void setGil_point(Integer gil_point) {
		this.gil_point = gil_point;
	}
	public String getGil_sub_code() {
		return gil_sub_code;
	}
	public void setGil_sub_code(String gil_sub_code) {
		this.gil_sub_code = gil_sub_code;
	}
	public Integer getSd_amount() {
		return sd_amount;
	}
	public void setSd_amount(Integer sd_amount) {
		this.sd_amount = sd_amount;
	}
	public String getPd_code() {
		return pd_code;
	}
	public void setPd_code(String pd_code) {
		this.pd_code = pd_code;
	}
	public Integer getGil_lastamount() {
		return gil_lastamount;
	}
	public void setGil_lastamount(Integer gil_lastamount) {
		this.gil_lastamount = gil_lastamount;
	}
	public Integer getGil_amount() {
		return gil_amount;
	}
	public void setGil_amount(Integer gil_amount) {
		this.gil_amount = gil_amount;
	}
	public String getGil_vm_code() {
		return gil_vm_code;
	}
	public void setGil_vm_code(String gil_vm_code) {
		this.gil_vm_code = gil_vm_code;
	}
	
}
