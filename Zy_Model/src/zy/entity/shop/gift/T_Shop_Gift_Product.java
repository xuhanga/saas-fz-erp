package zy.entity.shop.gift;

import java.io.Serializable;

public class T_Shop_Gift_Product implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer id;
	private String sz_code;
	private String sz_name;
	private String cr_code;
	private String cr_name;
	private String br_code;
	private String br_name;
	private Integer totalamount;
	private Integer getamount;//领取数量
	private Integer lastamount;//剩余数量
	private Integer addamount;//补充发布数量
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSz_code() {
		return sz_code;
	}
	public void setSz_code(String sz_code) {
		this.sz_code = sz_code;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getCr_code() {
		return cr_code;
	}
	public void setCr_code(String cr_code) {
		this.cr_code = cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getBr_code() {
		return br_code;
	}
	public void setBr_code(String br_code) {
		this.br_code = br_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Integer getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(Integer totalamount) {
		this.totalamount = totalamount;
	}
	public Integer getGetamount() {
		return getamount;
	}
	public void setGetamount(Integer getamount) {
		this.getamount = getamount;
	}
	public Integer getLastamount() {
		return lastamount;
	}
	public void setLastamount(Integer lastamount) {
		this.lastamount = lastamount;
	}
	public Integer getAddamount() {
		return addamount;
	}
	public void setAddamount(Integer addamount) {
		this.addamount = addamount;
	}
}
