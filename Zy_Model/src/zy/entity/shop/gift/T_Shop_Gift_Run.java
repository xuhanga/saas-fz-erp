package zy.entity.shop.gift;

import java.io.Serializable;

public class T_Shop_Gift_Run implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer gir_id;
	private String gir_number;
	private String gir_sub_code;
	private String gir_pd_code;
	private String gir_cr_code;
	private String cr_name;
	private String gir_sz_code;
	private String sz_name;
	private String gir_szg_code;
	private String gir_bs_code;//杯型br_code
	private String br_name;
	private Integer gir_totalamount;
	private String gir_begindate;
	private String gir_enddate;
	private String gir_sysdate;
	private String gir_shop_code;
	private String gir_user_code;
	private String user_name;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String sp_name;
	public Integer getGir_id() {
		return gir_id;
	}
	public void setGir_id(Integer gir_id) {
		this.gir_id = gir_id;
	}
	public String getGir_number() {
		return gir_number;
	}
	public void setGir_number(String gir_number) {
		this.gir_number = gir_number;
	}
	public String getGir_sub_code() {
		return gir_sub_code;
	}
	public void setGir_sub_code(String gir_sub_code) {
		this.gir_sub_code = gir_sub_code;
	}
	public String getGir_pd_code() {
		return gir_pd_code;
	}
	public void setGir_pd_code(String gir_pd_code) {
		this.gir_pd_code = gir_pd_code;
	}
	public String getGir_cr_code() {
		return gir_cr_code;
	}
	public void setGir_cr_code(String gir_cr_code) {
		this.gir_cr_code = gir_cr_code;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getGir_sz_code() {
		return gir_sz_code;
	}
	public void setGir_sz_code(String gir_sz_code) {
		this.gir_sz_code = gir_sz_code;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getGir_szg_code() {
		return gir_szg_code;
	}
	public void setGir_szg_code(String gir_szg_code) {
		this.gir_szg_code = gir_szg_code;
	}
	public String getGir_bs_code() {
		return gir_bs_code;
	}
	public void setGir_bs_code(String gir_bs_code) {
		this.gir_bs_code = gir_bs_code;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Integer getGir_totalamount() {
		return gir_totalamount;
	}
	public void setGir_totalamount(Integer gir_totalamount) {
		this.gir_totalamount = gir_totalamount;
	}
	public String getGir_begindate() {
		return gir_begindate;
	}
	public void setGir_begindate(String gir_begindate) {
		this.gir_begindate = gir_begindate;
	}
	public String getGir_enddate() {
		return gir_enddate;
	}
	public void setGir_enddate(String gir_enddate) {
		this.gir_enddate = gir_enddate;
	}
	public String getGir_sysdate() {
		return gir_sysdate;
	}
	public void setGir_sysdate(String gir_sysdate) {
		this.gir_sysdate = gir_sysdate;
	}
	public String getGir_shop_code() {
		return gir_shop_code;
	}
	public void setGir_shop_code(String gir_shop_code) {
		this.gir_shop_code = gir_shop_code;
	}
	public String getGir_user_code() {
		return gir_user_code;
	}
	public void setGir_user_code(String gir_user_code) {
		this.gir_user_code = gir_user_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
}
