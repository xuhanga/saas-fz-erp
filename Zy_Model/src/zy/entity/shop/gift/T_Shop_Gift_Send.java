package zy.entity.shop.gift;

import java.io.Serializable;

public class T_Shop_Gift_Send implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer gs_id;
	private String gs_pd_code;
	private String gs_cr_code;
	private String gs_sz_code;
	private String gs_br_code;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String pd_no;
	private String pd_name;
	private Integer gs_amount;
	private Integer gs_last_amount;
	private Integer gs_state;
	private String gs_shop_code;
	private String shop_name;
	private String gs_em_code;
	private String gs_vm_code;
	private String vm_name;
	private String gs_send_date;
	private String gs_date;
	private String gs_sysdate;
	private Integer gs_type;
	private String gs_check_no;
	private Integer companyid;
	public Integer getGs_id() {
		return gs_id;
	}
	public void setGs_id(Integer gs_id) {
		this.gs_id = gs_id;
	}
	public String getGs_pd_code() {
		return gs_pd_code;
	}
	public void setGs_pd_code(String gs_pd_code) {
		this.gs_pd_code = gs_pd_code;
	}
	public String getGs_cr_code() {
		return gs_cr_code;
	}
	public void setGs_cr_code(String gs_cr_code) {
		this.gs_cr_code = gs_cr_code;
	}
	public String getGs_sz_code() {
		return gs_sz_code;
	}
	public void setGs_sz_code(String gs_sz_code) {
		this.gs_sz_code = gs_sz_code;
	}
	public String getGs_br_code() {
		return gs_br_code;
	}
	public void setGs_br_code(String gs_br_code) {
		this.gs_br_code = gs_br_code;
	}
	public Integer getGs_amount() {
		return gs_amount;
	}
	public void setGs_amount(Integer gs_amount) {
		this.gs_amount = gs_amount;
	}
	public Integer getGs_last_amount() {
		return gs_last_amount;
	}
	public void setGs_last_amount(Integer gs_last_amount) {
		this.gs_last_amount = gs_last_amount;
	}
	public Integer getGs_state() {
		return gs_state;
	}
	public void setGs_state(Integer gs_state) {
		this.gs_state = gs_state;
	}
	public String getGs_shop_code() {
		return gs_shop_code;
	}
	public void setGs_shop_code(String gs_shop_code) {
		this.gs_shop_code = gs_shop_code;
	}
	public String getGs_em_code() {
		return gs_em_code;
	}
	public void setGs_em_code(String gs_em_code) {
		this.gs_em_code = gs_em_code;
	}
	public String getGs_vm_code() {
		return gs_vm_code;
	}
	public void setGs_vm_code(String gs_vm_code) {
		this.gs_vm_code = gs_vm_code;
	}
	public String getGs_send_date() {
		return gs_send_date;
	}
	public void setGs_send_date(String gs_send_date) {
		this.gs_send_date = gs_send_date;
	}
	public String getGs_date() {
		return gs_date;
	}
	public void setGs_date(String gs_date) {
		this.gs_date = gs_date;
	}
	public String getGs_sysdate() {
		return gs_sysdate;
	}
	public void setGs_sysdate(String gs_sysdate) {
		this.gs_sysdate = gs_sysdate;
	}
	public Integer getGs_type() {
		return gs_type;
	}
	public void setGs_type(Integer gs_type) {
		this.gs_type = gs_type;
	}
	public String getGs_check_no() {
		return gs_check_no;
	}
	public void setGs_check_no(String gs_check_no) {
		this.gs_check_no = gs_check_no;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}

}
