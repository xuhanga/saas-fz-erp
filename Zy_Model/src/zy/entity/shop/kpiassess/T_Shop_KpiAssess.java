package zy.entity.shop.kpiassess;

import java.io.Serializable;

public class T_Shop_KpiAssess implements Serializable{
	private static final long serialVersionUID = -8422417174049489809L;
	private Integer ka_id;
	private String ka_number;
	private Integer ka_type;
	private String ka_begin;
	private String ka_end;
	private String ka_remark;
	private Integer ka_state;
	private String ka_summary;
	private String ka_sysdate;
	private String ka_shop_code;
	private Integer ka_us_id;
	private Integer companyid;
	private String kal_name;
	public Integer getKa_id() {
		return ka_id;
	}
	public void setKa_id(Integer ka_id) {
		this.ka_id = ka_id;
	}
	public String getKa_number() {
		return ka_number;
	}
	public void setKa_number(String ka_number) {
		this.ka_number = ka_number;
	}
	public Integer getKa_type() {
		return ka_type;
	}
	public void setKa_type(Integer ka_type) {
		this.ka_type = ka_type;
	}
	public String getKa_begin() {
		return ka_begin;
	}
	public void setKa_begin(String ka_begin) {
		this.ka_begin = ka_begin;
	}
	public String getKa_end() {
		return ka_end;
	}
	public void setKa_end(String ka_end) {
		this.ka_end = ka_end;
	}
	public String getKa_remark() {
		return ka_remark;
	}
	public void setKa_remark(String ka_remark) {
		this.ka_remark = ka_remark;
	}
	public Integer getKa_state() {
		return ka_state;
	}
	public void setKa_state(Integer ka_state) {
		this.ka_state = ka_state;
	}
	public String getKa_summary() {
		return ka_summary;
	}
	public void setKa_summary(String ka_summary) {
		this.ka_summary = ka_summary;
	}
	public String getKa_sysdate() {
		return ka_sysdate;
	}
	public void setKa_sysdate(String ka_sysdate) {
		this.ka_sysdate = ka_sysdate;
	}
	public String getKa_shop_code() {
		return ka_shop_code;
	}
	public void setKa_shop_code(String ka_shop_code) {
		this.ka_shop_code = ka_shop_code;
	}
	public Integer getKa_us_id() {
		return ka_us_id;
	}
	public void setKa_us_id(Integer ka_us_id) {
		this.ka_us_id = ka_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getKal_name() {
		return kal_name;
	}
	public void setKal_name(String kal_name) {
		this.kal_name = kal_name;
	}
}
