package zy.entity.shop.kpipk;

import java.io.Serializable;

public class T_Shop_KpiPkList implements Serializable{
	private static final long serialVersionUID = 1033381969630715141L;
	private Integer kpl_id;
	private String kpl_number;
	private String kpl_code;
	private String kpl_name;
	private String kpl_ki_code;
	private String kpl_complete;
	private Integer kpl_score;
	private Integer companyid;
	private String ki_name;
	private String ki_identity;
	public Integer getKpl_id() {
		return kpl_id;
	}
	public void setKpl_id(Integer kpl_id) {
		this.kpl_id = kpl_id;
	}
	public String getKpl_number() {
		return kpl_number;
	}
	public void setKpl_number(String kpl_number) {
		this.kpl_number = kpl_number;
	}
	public String getKpl_code() {
		return kpl_code;
	}
	public void setKpl_code(String kpl_code) {
		this.kpl_code = kpl_code;
	}
	public String getKpl_name() {
		return kpl_name;
	}
	public void setKpl_name(String kpl_name) {
		this.kpl_name = kpl_name;
	}
	public String getKpl_ki_code() {
		return kpl_ki_code;
	}
	public void setKpl_ki_code(String kpl_ki_code) {
		this.kpl_ki_code = kpl_ki_code;
	}
	public String getKpl_complete() {
		return kpl_complete;
	}
	public void setKpl_complete(String kpl_complete) {
		this.kpl_complete = kpl_complete;
	}
	public Integer getKpl_score() {
		return kpl_score;
	}
	public void setKpl_score(Integer kpl_score) {
		this.kpl_score = kpl_score;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getKi_identity() {
		return ki_identity;
	}
	public void setKi_identity(String ki_identity) {
		this.ki_identity = ki_identity;
	}
	public String getKi_name() {
		return ki_name;
	}
	public void setKi_name(String ki_name) {
		this.ki_name = ki_name;
	}
}
