package zy.entity.shop.kpipk;

import java.io.Serializable;

public class T_Shop_KpiPkReward implements Serializable{
	private static final long serialVersionUID = -8753327380748438504L;
	private Integer kpr_id;
	private String kpr_number;
	private String kpr_rw_code;
	private Integer companyid;
	public Integer getKpr_id() {
		return kpr_id;
	}
	public void setKpr_id(Integer kpr_id) {
		this.kpr_id = kpr_id;
	}
	public String getKpr_number() {
		return kpr_number;
	}
	public void setKpr_number(String kpr_number) {
		this.kpr_number = kpr_number;
	}
	public String getKpr_rw_code() {
		return kpr_rw_code;
	}
	public void setKpr_rw_code(String kpr_rw_code) {
		this.kpr_rw_code = kpr_rw_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
