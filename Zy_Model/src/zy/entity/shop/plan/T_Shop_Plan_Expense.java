package zy.entity.shop.plan;

import java.io.Serializable;

public class T_Shop_Plan_Expense implements Serializable{
	private static final long serialVersionUID = 7556015009944457886L;
	private Integer pe_id;
	private String pe_number;
	private String pe_expensecode;
	private String pe_expensename;
	private Double pe_money;
	private Integer companyid;
	public Integer getPe_id() {
		return pe_id;
	}
	public void setPe_id(Integer pe_id) {
		this.pe_id = pe_id;
	}
	public String getPe_number() {
		return pe_number;
	}
	public void setPe_number(String pe_number) {
		this.pe_number = pe_number;
	}
	public String getPe_expensecode() {
		return pe_expensecode;
	}
	public void setPe_expensecode(String pe_expensecode) {
		this.pe_expensecode = pe_expensecode;
	}
	public String getPe_expensename() {
		return pe_expensename;
	}
	public void setPe_expensename(String pe_expensename) {
		this.pe_expensename = pe_expensename;
	}
	public Double getPe_money() {
		return pe_money;
	}
	public void setPe_money(Double pe_money) {
		this.pe_money = pe_money;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
