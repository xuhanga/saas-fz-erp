package zy.entity.shop.plan;

import java.io.Serializable;

public class T_Shop_Plan_Month implements Serializable{
	private static final long serialVersionUID = -3259142287594211460L;
	private Integer pm_id;
	private String pm_number;
	private Integer pm_year;
	private Integer pm_month;
	private Double pm_sell_money_plan;
	private Double pm_sell_money_pre;
	private Double pm_sell_money_real;
	private String pm_remark;
	private Integer companyid;
	public Integer getPm_id() {
		return pm_id;
	}
	public void setPm_id(Integer pm_id) {
		this.pm_id = pm_id;
	}
	public String getPm_number() {
		return pm_number;
	}
	public void setPm_number(String pm_number) {
		this.pm_number = pm_number;
	}
	public Integer getPm_year() {
		return pm_year;
	}
	public void setPm_year(Integer pm_year) {
		this.pm_year = pm_year;
	}
	public Integer getPm_month() {
		return pm_month;
	}
	public void setPm_month(Integer pm_month) {
		this.pm_month = pm_month;
	}
	public Double getPm_sell_money_plan() {
		return pm_sell_money_plan;
	}
	public void setPm_sell_money_plan(Double pm_sell_money_plan) {
		this.pm_sell_money_plan = pm_sell_money_plan;
	}
	public Double getPm_sell_money_pre() {
		return pm_sell_money_pre;
	}
	public void setPm_sell_money_pre(Double pm_sell_money_pre) {
		this.pm_sell_money_pre = pm_sell_money_pre;
	}
	public Double getPm_sell_money_real() {
		return pm_sell_money_real;
	}
	public void setPm_sell_money_real(Double pm_sell_money_real) {
		this.pm_sell_money_real = pm_sell_money_real;
	}
	public String getPm_remark() {
		return pm_remark;
	}
	public void setPm_remark(String pm_remark) {
		this.pm_remark = pm_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
