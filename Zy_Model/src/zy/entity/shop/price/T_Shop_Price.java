package zy.entity.shop.price;

import java.io.Serializable;

public class T_Shop_Price implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sp_id;
	private String sp_number;
	private String sp_maker;
	private String sp_makerdate;
	private String sp_sysdate;
	private String sp_manager;
	private Integer sp_is_sellprice;
	private Integer sp_is_vipprice;
	private Integer sp_is_sortprice;
	private Integer sp_is_costprice;
	private Integer sp_state;
	private Integer sp_shop_type;
	private Integer sp_us_id;
	private String sp_remark;
	private Integer companyid;
	private String sp_shop_code;
	private String sp_sp_name;
	private String ar_describe;
	public Integer getSp_id() {
		return sp_id;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSp_number() {
		return sp_number;
	}
	public void setSp_number(String sp_number) {
		this.sp_number = sp_number;
	}
	public String getSp_maker() {
		return sp_maker;
	}
	public void setSp_maker(String sp_maker) {
		this.sp_maker = sp_maker;
	}
	public String getSp_makerdate() {
		return sp_makerdate;
	}
	public void setSp_makerdate(String sp_makerdate) {
		this.sp_makerdate = sp_makerdate;
	}
	public String getSp_sysdate() {
		return sp_sysdate;
	}
	public void setSp_sysdate(String sp_sysdate) {
		this.sp_sysdate = sp_sysdate;
	}
	public String getSp_manager() {
		return sp_manager;
	}
	public void setSp_manager(String sp_manager) {
		this.sp_manager = sp_manager;
	}
	public Integer getSp_is_sellprice() {
		return sp_is_sellprice;
	}
	public void setSp_is_sellprice(Integer sp_is_sellprice) {
		this.sp_is_sellprice = sp_is_sellprice;
	}
	public Integer getSp_is_vipprice() {
		return sp_is_vipprice;
	}
	public void setSp_is_vipprice(Integer sp_is_vipprice) {
		this.sp_is_vipprice = sp_is_vipprice;
	}
	public Integer getSp_is_sortprice() {
		return sp_is_sortprice;
	}
	public void setSp_is_sortprice(Integer sp_is_sortprice) {
		this.sp_is_sortprice = sp_is_sortprice;
	}
	public Integer getSp_is_costprice() {
		return sp_is_costprice;
	}
	public void setSp_is_costprice(Integer sp_is_costprice) {
		this.sp_is_costprice = sp_is_costprice;
	}
	public Integer getSp_state() {
		return sp_state;
	}
	public void setSp_state(Integer sp_state) {
		this.sp_state = sp_state;
	}
	public Integer getSp_shop_type() {
		return sp_shop_type;
	}
	public void setSp_shop_type(Integer sp_shop_type) {
		this.sp_shop_type = sp_shop_type;
	}
	public Integer getSp_us_id() {
		return sp_us_id;
	}
	public void setSp_us_id(Integer sp_us_id) {
		this.sp_us_id = sp_us_id;
	}
	public String getSp_remark() {
		return sp_remark;
	}
	public void setSp_remark(String sp_remark) {
		this.sp_remark = sp_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSp_sp_name() {
		return sp_sp_name;
	}
	public void setSp_sp_name(String sp_sp_name) {
		this.sp_sp_name = sp_sp_name;
	}
	public String getSp_shop_code() {
		return sp_shop_code;
	}
	public void setSp_shop_code(String sp_shop_code) {
		this.sp_shop_code = sp_shop_code;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
