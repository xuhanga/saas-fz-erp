package zy.entity.shop.price;

import java.io.Serializable;

public class T_Shop_PriceList implements Serializable{
	private static final long serialVersionUID = -4224706226298517064L;
	private Integer spl_id;
	private String spl_number;
	private String spl_pd_code;
	private Integer spl_pd_point;
	private Double spl_old_sellprice;
	private Double spl_new_sellprice;
	private Double spl_old_vipprice;
	private Double spl_new_vipprice;
	private Double spl_old_sortprice;
	private Double spl_new_sortprice;
	private Double spl_old_costprice;
	private Double spl_new_costprice;
	private String spl_shop_code;
	private Integer spl_us_id;
	private Integer companyid;
	private String sp_name;
	private String pd_name;
	private String pd_no;
	public Integer getSpl_id() {
		return spl_id;
	}
	public void setSpl_id(Integer spl_id) {
		this.spl_id = spl_id;
	}
	public String getSpl_number() {
		return spl_number;
	}
	public void setSpl_number(String spl_number) {
		this.spl_number = spl_number;
	}
	public String getSpl_pd_code() {
		return spl_pd_code;
	}
	public void setSpl_pd_code(String spl_pd_code) {
		this.spl_pd_code = spl_pd_code;
	}
	public Double getSpl_old_sellprice() {
		return spl_old_sellprice;
	}
	public void setSpl_old_sellprice(Double spl_old_sellprice) {
		this.spl_old_sellprice = spl_old_sellprice;
	}
	public Double getSpl_new_sellprice() {
		return spl_new_sellprice;
	}
	public void setSpl_new_sellprice(Double spl_new_sellprice) {
		this.spl_new_sellprice = spl_new_sellprice;
	}
	public Double getSpl_old_vipprice() {
		return spl_old_vipprice;
	}
	public void setSpl_old_vipprice(Double spl_old_vipprice) {
		this.spl_old_vipprice = spl_old_vipprice;
	}
	public Double getSpl_new_vipprice() {
		return spl_new_vipprice;
	}
	public void setSpl_new_vipprice(Double spl_new_vipprice) {
		this.spl_new_vipprice = spl_new_vipprice;
	}
	public Double getSpl_old_sortprice() {
		return spl_old_sortprice;
	}
	public void setSpl_old_sortprice(Double spl_old_sortprice) {
		this.spl_old_sortprice = spl_old_sortprice;
	}
	public Double getSpl_new_sortprice() {
		return spl_new_sortprice;
	}
	public void setSpl_new_sortprice(Double spl_new_sortprice) {
		this.spl_new_sortprice = spl_new_sortprice;
	}
	public Double getSpl_old_costprice() {
		return spl_old_costprice;
	}
	public void setSpl_old_costprice(Double spl_old_costprice) {
		this.spl_old_costprice = spl_old_costprice;
	}
	public Double getSpl_new_costprice() {
		return spl_new_costprice;
	}
	public void setSpl_new_costprice(Double spl_new_costprice) {
		this.spl_new_costprice = spl_new_costprice;
	}
	public String getSpl_shop_code() {
		return spl_shop_code;
	}
	public void setSpl_shop_code(String spl_shop_code) {
		this.spl_shop_code = spl_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public Integer getSpl_us_id() {
		return spl_us_id;
	}
	public void setSpl_us_id(Integer spl_us_id) {
		this.spl_us_id = spl_us_id;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public Integer getSpl_pd_point() {
		return spl_pd_point;
	}
	public void setSpl_pd_point(Integer spl_pd_point) {
		this.spl_pd_point = spl_pd_point;
	}
}
