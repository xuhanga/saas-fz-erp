package zy.entity.shop.price;

import java.io.Serializable;

public class T_Shop_Price_Shop  implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sps_id;
	private String sps_number;
	private String sps_shop_code;
	private Integer companyid;
	public Integer getSps_id() {
		return sps_id;
	}
	public void setSps_id(Integer sps_id) {
		this.sps_id = sps_id;
	}
	public String getSps_number() {
		return sps_number;
	}
	public void setSps_number(String sps_number) {
		this.sps_number = sps_number;
	}
	public String getSps_shop_code() {
		return sps_shop_code;
	}
	public void setSps_shop_code(String sps_shop_code) {
		this.sps_shop_code = sps_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
