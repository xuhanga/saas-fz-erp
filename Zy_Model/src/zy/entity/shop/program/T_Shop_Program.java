package zy.entity.shop.program;

import java.io.Serializable;

public class T_Shop_Program implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer sp_id;
	private Integer sps_id;
	private String sp_shop_code;
	private String sp_shop_name;
	private String sp_title;
	private String sp_info;
	private String sp_imgpath;
	private Integer sps_state;
	private Integer sp_us_id;
	private String sp_us_name;
	private String sp_sysdate;
	private Integer companyid;
	public Integer getSp_id() {
		return sp_id;
	}
	public void setSp_id(Integer sp_id) {
		this.sp_id = sp_id;
	}
	public String getSp_shop_code() {
		return sp_shop_code;
	}
	public Integer getSps_id() {
		return sps_id;
	}
	public void setSps_id(Integer sps_id) {
		this.sps_id = sps_id;
	}
	public void setSp_shop_code(String sp_shop_code) {
		this.sp_shop_code = sp_shop_code;
	}
	public String getSp_shop_name() {
		return sp_shop_name;
	}
	public void setSp_shop_name(String sp_shop_name) {
		this.sp_shop_name = sp_shop_name;
	}
	public String getSp_title() {
		return sp_title;
	}
	public void setSp_title(String sp_title) {
		this.sp_title = sp_title;
	}
	public String getSp_info() {
		return sp_info;
	}
	public void setSp_info(String sp_info) {
		this.sp_info = sp_info;
	}
	public String getSp_imgpath() {
		return sp_imgpath;
	}
	public void setSp_imgpath(String sp_imgpath) {
		this.sp_imgpath = sp_imgpath;
	}
	public Integer getSps_state() {
		return sps_state;
	}
	public void setSps_state(Integer sps_state) {
		this.sps_state = sps_state;
	}
	public Integer getSp_us_id() {
		return sp_us_id;
	}
	public void setSp_us_id(Integer sp_us_id) {
		this.sp_us_id = sp_us_id;
	}
	public String getSp_us_name() {
		return sp_us_name;
	}
	public void setSp_us_name(String sp_us_name) {
		this.sp_us_name = sp_us_name;
	}
	public String getSp_sysdate() {
		return sp_sysdate;
	}
	public void setSp_sysdate(String sp_sysdate) {
		this.sp_sysdate = sp_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
