package zy.entity.shop.sale;

import java.io.Serializable;

public class T_Shop_Sale_Present implements Serializable{
	private static final long serialVersionUID = -5933914747225546123L;
	private Integer ssp_id;
	private String ssp_ss_code;
	private String ssp_subcode;
	private String ssp_cr_code;
	private String ssp_sz_code;
	private String ssp_br_code;
	private String ssp_dp_code;
	private Integer ssp_count;
	private String ssp_pd_code;
	private Integer ssp_index;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private Double ssp_addMoney;
	public Integer getSsp_id() {
		return ssp_id;
	}
	public void setSsp_id(Integer ssp_id) {
		this.ssp_id = ssp_id;
	}
	public String getSsp_ss_code() {
		return ssp_ss_code;
	}
	public void setSsp_ss_code(String ssp_ss_code) {
		this.ssp_ss_code = ssp_ss_code;
	}
	public String getSsp_subcode() {
		return ssp_subcode;
	}
	public void setSsp_subcode(String ssp_subcode) {
		this.ssp_subcode = ssp_subcode;
	}
	public String getSsp_cr_code() {
		return ssp_cr_code;
	}
	public void setSsp_cr_code(String ssp_cr_code) {
		this.ssp_cr_code = ssp_cr_code;
	}
	public String getSsp_sz_code() {
		return ssp_sz_code;
	}
	public void setSsp_sz_code(String ssp_sz_code) {
		this.ssp_sz_code = ssp_sz_code;
	}
	public String getSsp_br_code() {
		return ssp_br_code;
	}
	public void setSsp_br_code(String ssp_br_code) {
		this.ssp_br_code = ssp_br_code;
	}
	public String getSsp_dp_code() {
		return ssp_dp_code;
	}
	public void setSsp_dp_code(String ssp_dp_code) {
		this.ssp_dp_code = ssp_dp_code;
	}
	public Integer getSsp_count() {
		return ssp_count;
	}
	public void setSsp_count(Integer ssp_count) {
		this.ssp_count = ssp_count;
	}
	public String getSsp_pd_code() {
		return ssp_pd_code;
	}
	public void setSsp_pd_code(String ssp_pd_code) {
		this.ssp_pd_code = ssp_pd_code;
	}
	public Integer getSsp_index() {
		return ssp_index;
	}
	public void setSsp_index(Integer ssp_index) {
		this.ssp_index = ssp_index;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Double getSsp_addMoney() {
		return ssp_addMoney;
	}
	public void setSsp_addMoney(Double ssp_addMoney) {
		this.ssp_addMoney = ssp_addMoney;
	}
	
}
