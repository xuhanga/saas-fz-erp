package zy.entity.shop.target;

import java.io.Serializable;

public class T_Shop_Target_DetailList implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer tadl_id;
	private String tadl_ta_number;
	private String tadl_em_code;
	private String tadl_project_code;
	private Double tadl_money;
	private Double tadl_real_money;
	private String tadl_remark;
	private Integer companyid;
	public Integer getTadl_id() {
		return tadl_id;
	}
	public void setTadl_id(Integer tadl_id) {
		this.tadl_id = tadl_id;
	}
	public String getTadl_ta_number() {
		return tadl_ta_number;
	}
	public void setTadl_ta_number(String tadl_ta_number) {
		this.tadl_ta_number = tadl_ta_number;
	}
	public String getTadl_em_code() {
		return tadl_em_code;
	}
	public void setTadl_em_code(String tadl_em_code) {
		this.tadl_em_code = tadl_em_code;
	}
	public String getTadl_project_code() {
		return tadl_project_code;
	}
	public void setTadl_project_code(String tadl_project_code) {
		this.tadl_project_code = tadl_project_code;
	}
	public Double getTadl_money() {
		return tadl_money;
	}
	public void setTadl_money(Double tadl_money) {
		this.tadl_money = tadl_money;
	}
	public Double getTadl_real_money() {
		return tadl_real_money;
	}
	public void setTadl_real_money(Double tadl_real_money) {
		this.tadl_real_money = tadl_real_money;
	}
	public String getTadl_remark() {
		return tadl_remark;
	}
	public void setTadl_remark(String tadl_remark) {
		this.tadl_remark = tadl_remark;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
