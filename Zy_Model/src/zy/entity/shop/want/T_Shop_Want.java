package zy.entity.shop.want;

import java.io.Serializable;

public class T_Shop_Want implements Serializable{
	private static final long serialVersionUID = 5848504557389282722L;
	private Integer wt_id;
	private String wt_number;
	private String wt_date;
	private String wt_shop_code;
	private String shop_name;
	private String wt_outdp_code;
	private String wt_indp_code;
	private String outdepot_name;
	private String indepot_name;
	private String wt_maker;
	private String wt_manager;
	private String wt_handnumber;
	private Integer wt_applyamount;
	private Integer wt_sendamount;
	private Double wt_applymoney;
	private Double wt_sendmoney;
	private Double wt_sendcostmoney;
	private String wt_property;
	private String wt_remark;
	private Integer wt_ar_state;
	private String wt_ar_date;
	private Integer wt_ar_usid;
	private Integer wt_type;
	private Integer wt_isdraft;
	private String wt_sysdate;
	private Integer wt_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getWt_id() {
		return wt_id;
	}
	public void setWt_id(Integer wt_id) {
		this.wt_id = wt_id;
	}
	public String getWt_number() {
		return wt_number;
	}
	public void setWt_number(String wt_number) {
		this.wt_number = wt_number;
	}
	public String getWt_date() {
		return wt_date;
	}
	public void setWt_date(String wt_date) {
		this.wt_date = wt_date;
	}
	public String getWt_shop_code() {
		return wt_shop_code;
	}
	public void setWt_shop_code(String wt_shop_code) {
		this.wt_shop_code = wt_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getWt_outdp_code() {
		return wt_outdp_code;
	}
	public void setWt_outdp_code(String wt_outdp_code) {
		this.wt_outdp_code = wt_outdp_code;
	}
	public String getWt_indp_code() {
		return wt_indp_code;
	}
	public void setWt_indp_code(String wt_indp_code) {
		this.wt_indp_code = wt_indp_code;
	}
	public String getOutdepot_name() {
		return outdepot_name;
	}
	public void setOutdepot_name(String outdepot_name) {
		this.outdepot_name = outdepot_name;
	}
	public String getIndepot_name() {
		return indepot_name;
	}
	public void setIndepot_name(String indepot_name) {
		this.indepot_name = indepot_name;
	}
	public String getWt_maker() {
		return wt_maker;
	}
	public void setWt_maker(String wt_maker) {
		this.wt_maker = wt_maker;
	}
	public String getWt_manager() {
		return wt_manager;
	}
	public void setWt_manager(String wt_manager) {
		this.wt_manager = wt_manager;
	}
	public String getWt_handnumber() {
		return wt_handnumber;
	}
	public void setWt_handnumber(String wt_handnumber) {
		this.wt_handnumber = wt_handnumber;
	}
	public Integer getWt_applyamount() {
		return wt_applyamount;
	}
	public void setWt_applyamount(Integer wt_applyamount) {
		this.wt_applyamount = wt_applyamount;
	}
	public Integer getWt_sendamount() {
		return wt_sendamount;
	}
	public void setWt_sendamount(Integer wt_sendamount) {
		this.wt_sendamount = wt_sendamount;
	}
	public Double getWt_applymoney() {
		return wt_applymoney;
	}
	public void setWt_applymoney(Double wt_applymoney) {
		this.wt_applymoney = wt_applymoney;
	}
	public Double getWt_sendmoney() {
		return wt_sendmoney;
	}
	public void setWt_sendmoney(Double wt_sendmoney) {
		this.wt_sendmoney = wt_sendmoney;
	}
	public Double getWt_sendcostmoney() {
		return wt_sendcostmoney;
	}
	public void setWt_sendcostmoney(Double wt_sendcostmoney) {
		this.wt_sendcostmoney = wt_sendcostmoney;
	}
	public String getWt_property() {
		return wt_property;
	}
	public void setWt_property(String wt_property) {
		this.wt_property = wt_property;
	}
	public String getWt_remark() {
		return wt_remark;
	}
	public void setWt_remark(String wt_remark) {
		this.wt_remark = wt_remark;
	}
	public Integer getWt_ar_state() {
		return wt_ar_state;
	}
	public void setWt_ar_state(Integer wt_ar_state) {
		this.wt_ar_state = wt_ar_state;
	}
	public String getWt_ar_date() {
		return wt_ar_date;
	}
	public void setWt_ar_date(String wt_ar_date) {
		this.wt_ar_date = wt_ar_date;
	}
	public Integer getWt_ar_usid() {
		return wt_ar_usid;
	}
	public void setWt_ar_usid(Integer wt_ar_usid) {
		this.wt_ar_usid = wt_ar_usid;
	}
	public Integer getWt_type() {
		return wt_type;
	}
	public void setWt_type(Integer wt_type) {
		this.wt_type = wt_type;
	}
	public Integer getWt_isdraft() {
		return wt_isdraft;
	}
	public void setWt_isdraft(Integer wt_isdraft) {
		this.wt_isdraft = wt_isdraft;
	}
	public String getWt_sysdate() {
		return wt_sysdate;
	}
	public void setWt_sysdate(String wt_sysdate) {
		this.wt_sysdate = wt_sysdate;
	}
	public Integer getWt_us_id() {
		return wt_us_id;
	}
	public void setWt_us_id(Integer wt_us_id) {
		this.wt_us_id = wt_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
