package zy.entity.shop.want;

import java.io.Serializable;

public class T_Shop_WantList implements Serializable{
	private static final long serialVersionUID = -2630503594657525263L;
	private Long wtl_id;
	private String wtl_number;
	private String wtl_pd_code;
	private String wtl_sub_code;
	private String wtl_sz_code;
	private String wtl_szg_code;
	private String wtl_cr_code;
	private String wtl_br_code;
	private Integer wtl_applyamount;
	private Integer wtl_sendamount;
	private Double wtl_unitprice;
	private Double wtl_unitmoney;
	private Double wtl_costprice;
	private Double wtl_costmoney;
	private String wtl_remark;
	private Integer wtl_us_id;
	private Integer wtl_type;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_code;
	private String tp_code;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Long getWtl_id() {
		return wtl_id;
	}
	public void setWtl_id(Long wtl_id) {
		this.wtl_id = wtl_id;
	}
	public String getWtl_number() {
		return wtl_number;
	}
	public void setWtl_number(String wtl_number) {
		this.wtl_number = wtl_number;
	}
	public String getWtl_pd_code() {
		return wtl_pd_code;
	}
	public void setWtl_pd_code(String wtl_pd_code) {
		this.wtl_pd_code = wtl_pd_code;
	}
	public String getWtl_sub_code() {
		return wtl_sub_code;
	}
	public void setWtl_sub_code(String wtl_sub_code) {
		this.wtl_sub_code = wtl_sub_code;
	}
	public String getWtl_sz_code() {
		return wtl_sz_code;
	}
	public void setWtl_sz_code(String wtl_sz_code) {
		this.wtl_sz_code = wtl_sz_code;
	}
	public String getWtl_szg_code() {
		return wtl_szg_code;
	}
	public void setWtl_szg_code(String wtl_szg_code) {
		this.wtl_szg_code = wtl_szg_code;
	}
	public String getWtl_cr_code() {
		return wtl_cr_code;
	}
	public void setWtl_cr_code(String wtl_cr_code) {
		this.wtl_cr_code = wtl_cr_code;
	}
	public String getWtl_br_code() {
		return wtl_br_code;
	}
	public void setWtl_br_code(String wtl_br_code) {
		this.wtl_br_code = wtl_br_code;
	}
	public Integer getWtl_applyamount() {
		return wtl_applyamount;
	}
	public void setWtl_applyamount(Integer wtl_applyamount) {
		this.wtl_applyamount = wtl_applyamount;
	}
	public Integer getWtl_sendamount() {
		return wtl_sendamount;
	}
	public void setWtl_sendamount(Integer wtl_sendamount) {
		this.wtl_sendamount = wtl_sendamount;
	}
	public Double getWtl_unitprice() {
		return wtl_unitprice;
	}
	public void setWtl_unitprice(Double wtl_unitprice) {
		this.wtl_unitprice = wtl_unitprice;
	}
	public Double getWtl_unitmoney() {
		if (wtl_applyamount != null && wtl_unitprice != null) {
			return wtl_applyamount * wtl_unitprice;
		}
		return wtl_unitmoney;
	}
	public void setWtl_unitmoney(Double wtl_unitmoney) {
		this.wtl_unitmoney = wtl_unitmoney;
	}
	public Double getWtl_costprice() {
		return wtl_costprice;
	}
	public void setWtl_costprice(Double wtl_costprice) {
		this.wtl_costprice = wtl_costprice;
	}
	public Double getWtl_costmoney() {
		if (wtl_applyamount != null && wtl_costprice != null) {
			return wtl_applyamount * wtl_costprice;
		}
		return wtl_costmoney;
	}
	public void setWtl_costmoney(Double wtl_costmoney) {
		this.wtl_costmoney = wtl_costmoney;
	}
	public String getWtl_remark() {
		return wtl_remark;
	}
	public void setWtl_remark(String wtl_remark) {
		this.wtl_remark = wtl_remark;
	}
	public Integer getWtl_us_id() {
		return wtl_us_id;
	}
	public void setWtl_us_id(Integer wtl_us_id) {
		this.wtl_us_id = wtl_us_id;
	}
	public Integer getWtl_type() {
		return wtl_type;
	}
	public void setWtl_type(Integer wtl_type) {
		this.wtl_type = wtl_type;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	
}
