package zy.entity.sort.allot;

import java.io.Serializable;

public class T_Sort_AllotList implements Serializable{
	private static final long serialVersionUID = -1377111524130633064L;
	private Long atl_id;
	private String atl_number;
	private String atl_pd_code;
	private String atl_sub_code;
	private String atl_sz_code;
	private String atl_szg_code;
	private String atl_cr_code;
	private String atl_br_code;
	private Integer atl_applyamount;
	private Integer atl_sendamount;
	private Double atl_unitprice;
	private Double atl_unitmoney;
	private Double atl_sellprice;
	private Double atl_sellmoney;
	private Double atl_costprice;
	private Double atl_costmoney;
	private String atl_remark;
	private Integer atl_us_id;
	private Integer atl_type;
	private Integer atl_up;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_code;
	private String tp_code;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Long getAtl_id() {
		return atl_id;
	}
	public void setAtl_id(Long atl_id) {
		this.atl_id = atl_id;
	}
	public String getAtl_number() {
		return atl_number;
	}
	public void setAtl_number(String atl_number) {
		this.atl_number = atl_number;
	}
	public String getAtl_pd_code() {
		return atl_pd_code;
	}
	public void setAtl_pd_code(String atl_pd_code) {
		this.atl_pd_code = atl_pd_code;
	}
	public String getAtl_sub_code() {
		return atl_sub_code;
	}
	public void setAtl_sub_code(String atl_sub_code) {
		this.atl_sub_code = atl_sub_code;
	}
	public String getAtl_sz_code() {
		return atl_sz_code;
	}
	public void setAtl_sz_code(String atl_sz_code) {
		this.atl_sz_code = atl_sz_code;
	}
	public String getAtl_szg_code() {
		return atl_szg_code;
	}
	public void setAtl_szg_code(String atl_szg_code) {
		this.atl_szg_code = atl_szg_code;
	}
	public String getAtl_cr_code() {
		return atl_cr_code;
	}
	public void setAtl_cr_code(String atl_cr_code) {
		this.atl_cr_code = atl_cr_code;
	}
	public String getAtl_br_code() {
		return atl_br_code;
	}
	public void setAtl_br_code(String atl_br_code) {
		this.atl_br_code = atl_br_code;
	}
	public Integer getAtl_applyamount() {
		return atl_applyamount;
	}
	public void setAtl_applyamount(Integer atl_applyamount) {
		this.atl_applyamount = atl_applyamount;
	}
	public Integer getAtl_sendamount() {
		return atl_sendamount;
	}
	public void setAtl_sendamount(Integer atl_sendamount) {
		this.atl_sendamount = atl_sendamount;
	}
	public Double getAtl_unitprice() {
		return atl_unitprice;
	}
	public void setAtl_unitprice(Double atl_unitprice) {
		this.atl_unitprice = atl_unitprice;
	}
	public Double getAtl_unitmoney() {
		if (atl_applyamount != null && atl_unitprice != null) {
			return atl_applyamount * atl_unitprice;
		}
		return atl_unitmoney;
	}
	public void setAtl_unitmoney(Double atl_unitmoney) {
		this.atl_unitmoney = atl_unitmoney;
	}
	public Double getAtl_sellprice() {
		return atl_sellprice;
	}
	public void setAtl_sellprice(Double atl_sellprice) {
		this.atl_sellprice = atl_sellprice;
	}
	public Double getAtl_sellmoney() {
		if (atl_applyamount != null && atl_sellprice != null) {
			return atl_applyamount * atl_sellprice;
		}
		return atl_sellmoney;
	}
	public void setAtl_sellmoney(Double atl_sellmoney) {
		this.atl_sellmoney = atl_sellmoney;
	}
	public Double getAtl_costprice() {
		return atl_costprice;
	}
	public void setAtl_costprice(Double atl_costprice) {
		this.atl_costprice = atl_costprice;
	}
	public Double getAtl_costmoney() {
		if (atl_applyamount != null && atl_costprice != null) {
			return atl_applyamount * atl_costprice;
		}
		return atl_costmoney;
	}
	public void setAtl_costmoney(Double atl_costmoney) {
		this.atl_costmoney = atl_costmoney;
	}
	public String getAtl_remark() {
		return atl_remark;
	}
	public void setAtl_remark(String atl_remark) {
		this.atl_remark = atl_remark;
	}
	public Integer getAtl_us_id() {
		return atl_us_id;
	}
	public void setAtl_us_id(Integer atl_us_id) {
		this.atl_us_id = atl_us_id;
	}
	public Integer getAtl_type() {
		return atl_type;
	}
	public void setAtl_type(Integer atl_type) {
		this.atl_type = atl_type;
	}
	public Integer getAtl_up() {
		return atl_up;
	}
	public void setAtl_up(Integer atl_up) {
		this.atl_up = atl_up;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	
}
