package zy.entity.sort.dealings;

import java.io.Serializable;

public class T_Sort_Dealings implements Serializable{
	private static final long serialVersionUID = 9086695722163175224L;
	private Long dl_id;
	private String dl_shop_code;
	private String shop_name;
	private String dl_number;
	private Integer dl_type;
	private Double dl_discount_money;
	private Double dl_receivable;
	private Double dl_received;
	private Double dl_debt;
	private String dl_date;
	private String dl_manager;
	private String dl_remark;
	private String dl_sysdate;
	private Integer dl_amount;
	private Integer companyid;
	public Long getDl_id() {
		return dl_id;
	}
	public void setDl_id(Long dl_id) {
		this.dl_id = dl_id;
	}
	public String getDl_shop_code() {
		return dl_shop_code;
	}
	public void setDl_shop_code(String dl_shop_code) {
		this.dl_shop_code = dl_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getDl_number() {
		return dl_number;
	}
	public void setDl_number(String dl_number) {
		this.dl_number = dl_number;
	}
	public Integer getDl_type() {
		return dl_type;
	}
	public void setDl_type(Integer dl_type) {
		this.dl_type = dl_type;
	}
	public Double getDl_discount_money() {
		return dl_discount_money;
	}
	public void setDl_discount_money(Double dl_discount_money) {
		this.dl_discount_money = dl_discount_money;
	}
	public Double getDl_receivable() {
		return dl_receivable;
	}
	public void setDl_receivable(Double dl_receivable) {
		this.dl_receivable = dl_receivable;
	}
	public Double getDl_received() {
		return dl_received;
	}
	public void setDl_received(Double dl_received) {
		this.dl_received = dl_received;
	}
	public Double getDl_debt() {
		return dl_debt;
	}
	public void setDl_debt(Double dl_debt) {
		this.dl_debt = dl_debt;
	}
	public String getDl_date() {
		return dl_date;
	}
	public void setDl_date(String dl_date) {
		this.dl_date = dl_date;
	}
	public String getDl_manager() {
		return dl_manager;
	}
	public void setDl_manager(String dl_manager) {
		this.dl_manager = dl_manager;
	}
	public String getDl_remark() {
		return dl_remark;
	}
	public void setDl_remark(String dl_remark) {
		this.dl_remark = dl_remark;
	}
	public String getDl_sysdate() {
		return dl_sysdate;
	}
	public void setDl_sysdate(String dl_sysdate) {
		this.dl_sysdate = dl_sysdate;
	}
	public Integer getDl_amount() {
		return dl_amount;
	}
	public void setDl_amount(Integer dl_amount) {
		this.dl_amount = dl_amount;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
