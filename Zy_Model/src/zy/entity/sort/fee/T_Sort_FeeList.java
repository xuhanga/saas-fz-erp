package zy.entity.sort.fee;

import java.io.Serializable;

public class T_Sort_FeeList implements Serializable{
	private static final long serialVersionUID = 6585096156514275743L;
	private Long fel_id;
	private String fel_number;
	private String fel_mp_code;
	private String mp_name;
	private Double fel_money;
	private String fel_remark;
	private Integer fel_us_id;
	private Integer companyid;
	public Long getFel_id() {
		return fel_id;
	}
	public void setFel_id(Long fel_id) {
		this.fel_id = fel_id;
	}
	public String getFel_number() {
		return fel_number;
	}
	public void setFel_number(String fel_number) {
		this.fel_number = fel_number;
	}
	public String getFel_mp_code() {
		return fel_mp_code;
	}
	public void setFel_mp_code(String fel_mp_code) {
		this.fel_mp_code = fel_mp_code;
	}
	public String getMp_name() {
		return mp_name;
	}
	public void setMp_name(String mp_name) {
		this.mp_name = mp_name;
	}
	public Double getFel_money() {
		return fel_money;
	}
	public void setFel_money(Double fel_money) {
		this.fel_money = fel_money;
	}
	public String getFel_remark() {
		return fel_remark;
	}
	public void setFel_remark(String fel_remark) {
		this.fel_remark = fel_remark;
	}
	public Integer getFel_us_id() {
		return fel_us_id;
	}
	public void setFel_us_id(Integer fel_us_id) {
		this.fel_us_id = fel_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
