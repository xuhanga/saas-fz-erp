package zy.entity.sort.settle;

import java.io.Serializable;

public class T_Sort_SettleList implements Serializable{
	private static final long serialVersionUID = 7917296262489233165L;
	private Long stl_id;
	private String stl_number;
	private Double stl_receivable;
	private Double stl_received;
	private Double stl_discount_money_yet;
	private Double stl_prepay_yet;
	private Double stl_unreceivable;
	private Double stl_discount_money;
	private Double stl_prepay;
	private Double stl_real_received;
	private String stl_bill_number;
	private String stl_remark;
	private Integer stl_type;
	private Integer stl_join;
	private Integer stl_ar_state;
	private Integer stl_us_id;
	private Integer companyid;
	public Long getStl_id() {
		return stl_id;
	}
	public void setStl_id(Long stl_id) {
		this.stl_id = stl_id;
	}
	public String getStl_number() {
		return stl_number;
	}
	public void setStl_number(String stl_number) {
		this.stl_number = stl_number;
	}
	public Double getStl_receivable() {
		return stl_receivable;
	}
	public void setStl_receivable(Double stl_receivable) {
		this.stl_receivable = stl_receivable;
	}
	public Double getStl_received() {
		return stl_received;
	}
	public void setStl_received(Double stl_received) {
		this.stl_received = stl_received;
	}
	public Double getStl_discount_money_yet() {
		return stl_discount_money_yet;
	}
	public void setStl_discount_money_yet(Double stl_discount_money_yet) {
		this.stl_discount_money_yet = stl_discount_money_yet;
	}
	public Double getStl_prepay_yet() {
		return stl_prepay_yet;
	}
	public void setStl_prepay_yet(Double stl_prepay_yet) {
		this.stl_prepay_yet = stl_prepay_yet;
	}
	public Double getStl_unreceivable() {
		return stl_unreceivable;
	}
	public void setStl_unreceivable(Double stl_unreceivable) {
		this.stl_unreceivable = stl_unreceivable;
	}
	public Double getStl_discount_money() {
		return stl_discount_money;
	}
	public void setStl_discount_money(Double stl_discount_money) {
		this.stl_discount_money = stl_discount_money;
	}
	public Double getStl_prepay() {
		return stl_prepay;
	}
	public void setStl_prepay(Double stl_prepay) {
		this.stl_prepay = stl_prepay;
	}
	public Double getStl_real_received() {
		return stl_real_received;
	}
	public void setStl_real_received(Double stl_real_received) {
		this.stl_real_received = stl_real_received;
	}
	public String getStl_bill_number() {
		return stl_bill_number;
	}
	public void setStl_bill_number(String stl_bill_number) {
		this.stl_bill_number = stl_bill_number;
	}
	public String getStl_remark() {
		return stl_remark;
	}
	public void setStl_remark(String stl_remark) {
		this.stl_remark = stl_remark;
	}
	public Integer getStl_type() {
		return stl_type;
	}
	public void setStl_type(Integer stl_type) {
		this.stl_type = stl_type;
	}
	public Integer getStl_join() {
		return stl_join;
	}
	public void setStl_join(Integer stl_join) {
		this.stl_join = stl_join;
	}
	public Integer getStl_ar_state() {
		return stl_ar_state;
	}
	public void setStl_ar_state(Integer stl_ar_state) {
		this.stl_ar_state = stl_ar_state;
	}
	public Integer getStl_us_id() {
		return stl_us_id;
	}
	public void setStl_us_id(Integer stl_us_id) {
		this.stl_us_id = stl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
