package zy.entity.stock.adjust;

import java.io.Serializable;

public class T_Stock_Adjust implements Serializable{
	private static final long serialVersionUID = 4619817287478082270L;
	private Integer aj_id;
	private String aj_number;
	private String aj_date;
	private String aj_dp_code;
	private String depot_name;
	private String aj_manager;
	private Integer aj_amount;
	private Double aj_money;
	private String aj_remark;
	private Integer aj_ar_state;
	private String aj_ar_date;
	private Integer aj_isdraft;
	private String aj_sysdate;
	private Integer aj_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getAj_id() {
		return aj_id;
	}
	public void setAj_id(Integer aj_id) {
		this.aj_id = aj_id;
	}
	public String getAj_number() {
		return aj_number;
	}
	public void setAj_number(String aj_number) {
		this.aj_number = aj_number;
	}
	public String getAj_date() {
		return aj_date;
	}
	public void setAj_date(String aj_date) {
		this.aj_date = aj_date;
	}
	public String getAj_dp_code() {
		return aj_dp_code;
	}
	public void setAj_dp_code(String aj_dp_code) {
		this.aj_dp_code = aj_dp_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getAj_manager() {
		return aj_manager;
	}
	public void setAj_manager(String aj_manager) {
		this.aj_manager = aj_manager;
	}
	public Integer getAj_amount() {
		return aj_amount;
	}
	public void setAj_amount(Integer aj_amount) {
		this.aj_amount = aj_amount;
	}
	public Double getAj_money() {
		return aj_money;
	}
	public void setAj_money(Double aj_money) {
		this.aj_money = aj_money;
	}
	public String getAj_remark() {
		return aj_remark;
	}
	public void setAj_remark(String aj_remark) {
		this.aj_remark = aj_remark;
	}
	public Integer getAj_ar_state() {
		return aj_ar_state;
	}
	public void setAj_ar_state(Integer aj_ar_state) {
		this.aj_ar_state = aj_ar_state;
	}
	public String getAj_ar_date() {
		return aj_ar_date;
	}
	public void setAj_ar_date(String aj_ar_date) {
		this.aj_ar_date = aj_ar_date;
	}
	public Integer getAj_isdraft() {
		return aj_isdraft;
	}
	public void setAj_isdraft(Integer aj_isdraft) {
		this.aj_isdraft = aj_isdraft;
	}
	public String getAj_sysdate() {
		return aj_sysdate;
	}
	public void setAj_sysdate(String aj_sysdate) {
		this.aj_sysdate = aj_sysdate;
	}
	public Integer getAj_us_id() {
		return aj_us_id;
	}
	public void setAj_us_id(Integer aj_us_id) {
		this.aj_us_id = aj_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
