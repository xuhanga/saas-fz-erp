package zy.entity.stock.adjust;

import java.io.Serializable;

public class T_Stock_AdjustList implements Serializable{
	private static final long serialVersionUID = 835556395162959962L;
	private Integer ajl_id;
	private String ajl_number;
	private String ajl_pd_code;
	private String ajl_sub_code;
	private String ajl_sz_code;
	private String ajl_szg_code;
	private String ajl_cr_code;
	private String ajl_br_code;
	private Integer ajl_amount;
	private Double ajl_unitprice;
	private Double ajl_unitmoney;
	private String ajl_remark;
	private Integer ajl_us_id;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_code;
	private String tp_code;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Integer getAjl_id() {
		return ajl_id;
	}
	public void setAjl_id(Integer ajl_id) {
		this.ajl_id = ajl_id;
	}
	public String getAjl_number() {
		return ajl_number;
	}
	public void setAjl_number(String ajl_number) {
		this.ajl_number = ajl_number;
	}
	public String getAjl_pd_code() {
		return ajl_pd_code;
	}
	public void setAjl_pd_code(String ajl_pd_code) {
		this.ajl_pd_code = ajl_pd_code;
	}
	public String getAjl_sub_code() {
		return ajl_sub_code;
	}
	public void setAjl_sub_code(String ajl_sub_code) {
		this.ajl_sub_code = ajl_sub_code;
	}
	public String getAjl_sz_code() {
		return ajl_sz_code;
	}
	public void setAjl_sz_code(String ajl_sz_code) {
		this.ajl_sz_code = ajl_sz_code;
	}
	public String getAjl_szg_code() {
		return ajl_szg_code;
	}
	public void setAjl_szg_code(String ajl_szg_code) {
		this.ajl_szg_code = ajl_szg_code;
	}
	public String getAjl_cr_code() {
		return ajl_cr_code;
	}
	public void setAjl_cr_code(String ajl_cr_code) {
		this.ajl_cr_code = ajl_cr_code;
	}
	public String getAjl_br_code() {
		return ajl_br_code;
	}
	public void setAjl_br_code(String ajl_br_code) {
		this.ajl_br_code = ajl_br_code;
	}
	public Integer getAjl_amount() {
		return ajl_amount;
	}
	public void setAjl_amount(Integer ajl_amount) {
		this.ajl_amount = ajl_amount;
	}
	public Double getAjl_unitprice() {
		return ajl_unitprice;
	}
	public void setAjl_unitprice(Double ajl_unitprice) {
		this.ajl_unitprice = ajl_unitprice;
	}
	public Double getAjl_unitmoney() {
		if (ajl_amount != null && ajl_unitprice != null) {
			return ajl_amount * ajl_unitprice;
		}
		return ajl_unitmoney;
	}
	public String getAjl_remark() {
		return ajl_remark;
	}
	public void setAjl_remark(String ajl_remark) {
		this.ajl_remark = ajl_remark;
	}
	public Integer getAjl_us_id() {
		return ajl_us_id;
	}
	public void setAjl_us_id(Integer ajl_us_id) {
		this.ajl_us_id = ajl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
	public String getBd_code() {
		return bd_code;
	}
	public void setBd_code(String bd_code) {
		this.bd_code = bd_code;
	}
	public String getTp_code() {
		return tp_code;
	}
	public void setTp_code(String tp_code) {
		this.tp_code = tp_code;
	}
	public void setAjl_unitmoney(Double ajl_unitmoney) {
		this.ajl_unitmoney = ajl_unitmoney;
	}
	
}
