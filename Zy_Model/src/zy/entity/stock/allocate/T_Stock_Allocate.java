package zy.entity.stock.allocate;

import java.io.Serializable;

public class T_Stock_Allocate implements Serializable{
	private static final long serialVersionUID = 1096055926832248991L;
	private Integer ac_id;
	private String ac_number;
	private String ac_date;
	private String ac_outdp_code;
	private String ac_indp_code;
	private String outdepot_name;
	private String indepot_name;
	private String ac_manager;
	private Integer ac_amount;
	private Double ac_money;
	private String ac_remark;
	private Integer ac_ar_state;
	private String ac_ar_date;
	private Integer ac_isdraft;
	private String ac_sysdate;
	private Integer ac_us_id;
	private Integer companyid;
	
	private String ar_describe;
	public Integer getAc_id() {
		return ac_id;
	}
	public void setAc_id(Integer ac_id) {
		this.ac_id = ac_id;
	}
	public String getAc_number() {
		return ac_number;
	}
	public void setAc_number(String ac_number) {
		this.ac_number = ac_number;
	}
	public String getAc_date() {
		return ac_date;
	}
	public void setAc_date(String ac_date) {
		this.ac_date = ac_date;
	}
	public String getAc_outdp_code() {
		return ac_outdp_code;
	}
	public void setAc_outdp_code(String ac_outdp_code) {
		this.ac_outdp_code = ac_outdp_code;
	}
	public String getAc_indp_code() {
		return ac_indp_code;
	}
	public void setAc_indp_code(String ac_indp_code) {
		this.ac_indp_code = ac_indp_code;
	}
	public String getOutdepot_name() {
		return outdepot_name;
	}
	public void setOutdepot_name(String outdepot_name) {
		this.outdepot_name = outdepot_name;
	}
	public String getIndepot_name() {
		return indepot_name;
	}
	public void setIndepot_name(String indepot_name) {
		this.indepot_name = indepot_name;
	}
	public String getAc_manager() {
		return ac_manager;
	}
	public void setAc_manager(String ac_manager) {
		this.ac_manager = ac_manager;
	}
	public Integer getAc_amount() {
		return ac_amount;
	}
	public void setAc_amount(Integer ac_amount) {
		this.ac_amount = ac_amount;
	}
	public Double getAc_money() {
		return ac_money;
	}
	public void setAc_money(Double ac_money) {
		this.ac_money = ac_money;
	}
	public String getAc_remark() {
		return ac_remark;
	}
	public void setAc_remark(String ac_remark) {
		this.ac_remark = ac_remark;
	}
	public Integer getAc_ar_state() {
		return ac_ar_state;
	}
	public void setAc_ar_state(Integer ac_ar_state) {
		this.ac_ar_state = ac_ar_state;
	}
	public String getAc_ar_date() {
		return ac_ar_date;
	}
	public void setAc_ar_date(String ac_ar_date) {
		this.ac_ar_date = ac_ar_date;
	}
	public Integer getAc_isdraft() {
		return ac_isdraft;
	}
	public void setAc_isdraft(Integer ac_isdraft) {
		this.ac_isdraft = ac_isdraft;
	}
	public String getAc_sysdate() {
		return ac_sysdate;
	}
	public void setAc_sysdate(String ac_sysdate) {
		this.ac_sysdate = ac_sysdate;
	}
	public Integer getAc_us_id() {
		return ac_us_id;
	}
	public void setAc_us_id(Integer ac_us_id) {
		this.ac_us_id = ac_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
