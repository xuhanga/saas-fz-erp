package zy.entity.stock.check;

import java.io.Serializable;

public class T_Stock_Batch implements Serializable{
	private static final long serialVersionUID = 2323931937550388956L;
	private Integer ba_id;
	private String ba_number;
	private String ba_date;
	private String ba_dp_code;
	private String depot_name;
	private String ba_manager;
	private Integer ba_scope;
	private String ba_scope_code;
	private String ba_scope_name;
	private String ba_remark;
	private Integer ba_ar_state;
	private Integer ba_isexec;
	private String ba_execdate;
	private Integer ba_amount;
	private Integer ba_stockamount;
	private Double ba_costmoney;
	private Double ba_retailmoney;
	private Integer ba_us_id;
	private Integer companyid;
	private String ck_number;
	private String ar_describe;
	public Integer getBa_id() {
		return ba_id;
	}
	public void setBa_id(Integer ba_id) {
		this.ba_id = ba_id;
	}
	public String getBa_number() {
		return ba_number;
	}
	public void setBa_number(String ba_number) {
		this.ba_number = ba_number;
	}
	public String getBa_date() {
		return ba_date;
	}
	public void setBa_date(String ba_date) {
		this.ba_date = ba_date;
	}
	public String getBa_dp_code() {
		return ba_dp_code;
	}
	public void setBa_dp_code(String ba_dp_code) {
		this.ba_dp_code = ba_dp_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getBa_manager() {
		return ba_manager;
	}
	public void setBa_manager(String ba_manager) {
		this.ba_manager = ba_manager;
	}
	public Integer getBa_scope() {
		return ba_scope;
	}
	public void setBa_scope(Integer ba_scope) {
		this.ba_scope = ba_scope;
	}
	public String getBa_scope_code() {
		return ba_scope_code;
	}
	public void setBa_scope_code(String ba_scope_code) {
		this.ba_scope_code = ba_scope_code;
	}
	public String getBa_scope_name() {
		return ba_scope_name;
	}
	public void setBa_scope_name(String ba_scope_name) {
		this.ba_scope_name = ba_scope_name;
	}
	public String getBa_remark() {
		return ba_remark;
	}
	public void setBa_remark(String ba_remark) {
		this.ba_remark = ba_remark;
	}
	public Integer getBa_ar_state() {
		return ba_ar_state;
	}
	public void setBa_ar_state(Integer ba_ar_state) {
		this.ba_ar_state = ba_ar_state;
	}
	public Integer getBa_isexec() {
		return ba_isexec;
	}
	public void setBa_isexec(Integer ba_isexec) {
		this.ba_isexec = ba_isexec;
	}
	public String getBa_execdate() {
		return ba_execdate;
	}
	public void setBa_execdate(String ba_execdate) {
		this.ba_execdate = ba_execdate;
	}
	public Integer getBa_amount() {
		return ba_amount;
	}
	public void setBa_amount(Integer ba_amount) {
		this.ba_amount = ba_amount;
	}
	public Integer getBa_stockamount() {
		return ba_stockamount;
	}
	public void setBa_stockamount(Integer ba_stockamount) {
		this.ba_stockamount = ba_stockamount;
	}
	public Double getBa_costmoney() {
		return ba_costmoney;
	}
	public void setBa_costmoney(Double ba_costmoney) {
		this.ba_costmoney = ba_costmoney;
	}
	public Double getBa_retailmoney() {
		return ba_retailmoney;
	}
	public void setBa_retailmoney(Double ba_retailmoney) {
		this.ba_retailmoney = ba_retailmoney;
	}
	public Integer getBa_us_id() {
		return ba_us_id;
	}
	public void setBa_us_id(Integer ba_us_id) {
		this.ba_us_id = ba_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getCk_number() {
		return ck_number;
	}
	public void setCk_number(String ck_number) {
		this.ck_number = ck_number;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
