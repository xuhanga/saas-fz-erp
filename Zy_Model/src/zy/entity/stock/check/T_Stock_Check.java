package zy.entity.stock.check;

import java.io.Serializable;

public class T_Stock_Check implements Serializable{
	private static final long serialVersionUID = 8416619678424501214L;
	private Integer ck_id;
	private String ck_ba_number;
	private String ck_number;
	private String ck_date;
	private String ck_dp_code;
	private String depot_name;
	private String ck_manager;
	private String ck_remark;
	private Integer ck_isdraft;
	private Integer ck_isexec;
	private String ck_execdate;
	private Integer ck_amount;
	private Integer ck_stockamount;
	private Double ck_money;
	private Double ck_retailmoney;
	private Integer ck_us_id;
	private String us_name;
	private Integer companyid;
	public Integer getCk_id() {
		return ck_id;
	}
	public void setCk_id(Integer ck_id) {
		this.ck_id = ck_id;
	}
	public String getCk_ba_number() {
		return ck_ba_number;
	}
	public void setCk_ba_number(String ck_ba_number) {
		this.ck_ba_number = ck_ba_number;
	}
	public String getCk_number() {
		return ck_number;
	}
	public void setCk_number(String ck_number) {
		this.ck_number = ck_number;
	}
	public String getCk_date() {
		return ck_date;
	}
	public void setCk_date(String ck_date) {
		this.ck_date = ck_date;
	}
	public String getCk_dp_code() {
		return ck_dp_code;
	}
	public void setCk_dp_code(String ck_dp_code) {
		this.ck_dp_code = ck_dp_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getCk_manager() {
		return ck_manager;
	}
	public void setCk_manager(String ck_manager) {
		this.ck_manager = ck_manager;
	}
	public String getCk_remark() {
		return ck_remark;
	}
	public void setCk_remark(String ck_remark) {
		this.ck_remark = ck_remark;
	}
	public Integer getCk_isdraft() {
		return ck_isdraft;
	}
	public void setCk_isdraft(Integer ck_isdraft) {
		this.ck_isdraft = ck_isdraft;
	}
	public Integer getCk_isexec() {
		return ck_isexec;
	}
	public void setCk_isexec(Integer ck_isexec) {
		this.ck_isexec = ck_isexec;
	}
	public String getCk_execdate() {
		return ck_execdate;
	}
	public void setCk_execdate(String ck_execdate) {
		this.ck_execdate = ck_execdate;
	}
	public Integer getCk_amount() {
		return ck_amount;
	}
	public void setCk_amount(Integer ck_amount) {
		this.ck_amount = ck_amount;
	}
	public Integer getCk_stockamount() {
		return ck_stockamount;
	}
	public void setCk_stockamount(Integer ck_stockamount) {
		this.ck_stockamount = ck_stockamount;
	}
	public Double getCk_money() {
		return ck_money;
	}
	public void setCk_money(Double ck_money) {
		this.ck_money = ck_money;
	}
	public Double getCk_retailmoney() {
		return ck_retailmoney;
	}
	public void setCk_retailmoney(Double ck_retailmoney) {
		this.ck_retailmoney = ck_retailmoney;
	}
	public Integer getCk_us_id() {
		return ck_us_id;
	}
	public void setCk_us_id(Integer ck_us_id) {
		this.ck_us_id = ck_us_id;
	}
	public String getUs_name() {
		return us_name;
	}
	public void setUs_name(String us_name) {
		this.us_name = us_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
