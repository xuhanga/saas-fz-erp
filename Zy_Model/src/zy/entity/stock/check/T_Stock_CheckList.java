package zy.entity.stock.check;

import java.io.Serializable;

public class T_Stock_CheckList implements Serializable{
	private static final long serialVersionUID = 7896498559948805319L;
	private Integer ckl_id;
	private String ckl_ba_number;
	private String ckl_number;
	private String ckl_pd_code;
	private String ckl_sub_code;
	private String ckl_szg_code;
	private String ckl_sz_code;
	private String ckl_cr_code;
	private String ckl_br_code;
	private Integer ckl_stock_amount;
	private Integer ckl_amount;
	private Double ckl_unitprice;
	private Double ckl_retailprice;
	private Double ckl_unitmoney;
	private Double ckl_retailmoney;
	private String ckl_remark;
	private Integer ckl_isrepair;
	private Integer ckl_us_id;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_name;
	private String tp_name;
	
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;

	public Integer getCkl_id() {
		return ckl_id;
	}

	public void setCkl_id(Integer ckl_id) {
		this.ckl_id = ckl_id;
	}

	public String getCkl_ba_number() {
		return ckl_ba_number;
	}
	
	public void setCkl_ba_number(String ckl_ba_number) {
		this.ckl_ba_number = ckl_ba_number;
	}

	public String getCkl_number() {
		return ckl_number;
	}

	public void setCkl_number(String ckl_number) {
		this.ckl_number = ckl_number;
	}

	public String getCkl_pd_code() {
		return ckl_pd_code;
	}

	public void setCkl_pd_code(String ckl_pd_code) {
		this.ckl_pd_code = ckl_pd_code;
	}

	public String getCkl_sub_code() {
		return ckl_sub_code;
	}

	public void setCkl_sub_code(String ckl_sub_code) {
		this.ckl_sub_code = ckl_sub_code;
	}

	public String getCkl_szg_code() {
		return ckl_szg_code;
	}

	public void setCkl_szg_code(String ckl_szg_code) {
		this.ckl_szg_code = ckl_szg_code;
	}

	public String getCkl_sz_code() {
		return ckl_sz_code;
	}

	public void setCkl_sz_code(String ckl_sz_code) {
		this.ckl_sz_code = ckl_sz_code;
	}

	public String getCkl_cr_code() {
		return ckl_cr_code;
	}

	public void setCkl_cr_code(String ckl_cr_code) {
		this.ckl_cr_code = ckl_cr_code;
	}

	public String getCkl_br_code() {
		return ckl_br_code;
	}

	public void setCkl_br_code(String ckl_br_code) {
		this.ckl_br_code = ckl_br_code;
	}

	public Integer getCkl_stock_amount() {
		return ckl_stock_amount;
	}

	public void setCkl_stock_amount(Integer ckl_stock_amount) {
		this.ckl_stock_amount = ckl_stock_amount;
	}

	public Integer getCkl_amount() {
		return ckl_amount;
	}

	public void setCkl_amount(Integer ckl_amount) {
		this.ckl_amount = ckl_amount;
	}

	public Double getCkl_unitprice() {
		return ckl_unitprice;
	}

	public void setCkl_unitprice(Double ckl_unitprice) {
		this.ckl_unitprice = ckl_unitprice;
	}

	public Double getCkl_retailprice() {
		return ckl_retailprice;
	}

	public void setCkl_retailprice(Double ckl_retailprice) {
		this.ckl_retailprice = ckl_retailprice;
	}

	public Double getCkl_unitmoney() {
		if (ckl_amount != null && ckl_unitprice != null) {
			return ckl_amount * ckl_unitprice;
		}
		return ckl_unitmoney;
	}

	public void setCkl_unitmoney(Double ckl_unitmoney) {
		this.ckl_unitmoney = ckl_unitmoney;
	}

	public Double getCkl_retailmoney() {
		if (ckl_amount != null && ckl_retailprice != null) {
			return ckl_amount * ckl_retailprice;
		}
		return ckl_retailmoney;
	}

	public void setCkl_retailmoney(Double ckl_retailmoney) {
		this.ckl_retailmoney = ckl_retailmoney;
	}

	public String getCkl_remark() {
		return ckl_remark;
	}

	public void setCkl_remark(String ckl_remark) {
		this.ckl_remark = ckl_remark;
	}

	public Integer getCkl_isrepair() {
		return ckl_isrepair;
	}

	public void setCkl_isrepair(Integer ckl_isrepair) {
		this.ckl_isrepair = ckl_isrepair;
	}

	public Integer getCkl_us_id() {
		return ckl_us_id;
	}

	public void setCkl_us_id(Integer ckl_us_id) {
		this.ckl_us_id = ckl_us_id;
	}
	
	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

	public String getPd_no() {
		return pd_no;
	}

	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}

	public String getPd_name() {
		return pd_name;
	}

	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}

	public String getPd_unit() {
		return pd_unit;
	}

	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}

	public String getPd_season() {
		return pd_season;
	}

	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}

	public Integer getPd_year() {
		return pd_year;
	}

	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}

	public String getCr_name() {
		return cr_name;
	}

	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}

	public String getSz_name() {
		return sz_name;
	}

	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}

	public String getBr_name() {
		return br_name;
	}

	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}

	public String getBd_name() {
		return bd_name;
	}

	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}

	public String getTp_name() {
		return tp_name;
	}

	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}

	public String getOperate_type() {
		return operate_type;
	}

	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
}
