package zy.entity.stock.data;

public class T_Stock_DataBill extends T_Stock_Data{
	private static final long serialVersionUID = 3307511130063546365L;
	private Integer bill_amount;
	private Double pd_cost_price;
	public Integer getBill_amount() {
		return bill_amount;
	}
	public void setBill_amount(Integer bill_amount) {
		this.bill_amount = bill_amount;
	}
	public Double getPd_cost_price() {
		return pd_cost_price;
	}
	public void setPd_cost_price(Double pd_cost_price) {
		this.pd_cost_price = pd_cost_price;
	}
}
