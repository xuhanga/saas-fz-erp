package zy.entity.stock.data;

import java.util.HashMap;
import java.util.Map;

public class T_Stock_DataSizeView extends T_Stock_Data{
	private static final long serialVersionUID = -5790308185484305763L;
	private String pd_szg_code;
	private Double pd_cost_price;
	private Double pd_sell_price;
	private Double cost_money;
	private Double sell_money;
	private Map<String, Integer> amountMap = new HashMap<String, Integer>();
	public String getPd_szg_code() {
		return pd_szg_code;
	}
	public void setPd_szg_code(String pd_szg_code) {
		this.pd_szg_code = pd_szg_code;
	}
	public Double getPd_cost_price() {
		return pd_cost_price;
	}
	public void setPd_cost_price(Double pd_cost_price) {
		this.pd_cost_price = pd_cost_price;
	}
	public Double getPd_sell_price() {
		return pd_sell_price;
	}
	public void setPd_sell_price(Double pd_sell_price) {
		this.pd_sell_price = pd_sell_price;
	}
	public Double getCost_money() {
		if (sd_amount != null && pd_cost_price != null) {
			return sd_amount * pd_cost_price;
		}
		return cost_money;
	}
	public void setCost_money(Double cost_money) {
		this.cost_money = cost_money;
	}
	public Double getSell_money() {
		if (sd_amount != null && pd_sell_price != null) {
			return sd_amount * pd_sell_price;
		}
		return sell_money;
	}
	public void setSell_money(Double sell_money) {
		this.sell_money = sell_money;
	}
	public Map<String, Integer> getAmountMap() {
		return amountMap;
	}
	public void setAmountMap(Map<String, Integer> amountMap) {
		this.amountMap = amountMap;
	}
}
