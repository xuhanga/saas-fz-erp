package zy.entity.stock.loss;

import java.io.Serializable;

public class T_Stock_Loss implements Serializable{
	private static final long serialVersionUID = -248059757262493087L;
	private Integer lo_id;
	private String lo_number;
	private String lo_date;
	private String lo_dp_code;
	private String depot_name;
	private String lo_manager;
	private Integer lo_amount;
	private Double lo_money;
	private String lo_remark;
	private Integer lo_ar_state;
	private String lo_ar_date;
	private Integer lo_isdraft;
	private String lo_sysdate;
	private Integer lo_us_id;
	private String lo_maker;
	private Integer companyid;
	private String ar_describe;
	public Integer getLo_id() {
		return lo_id;
	}
	public void setLo_id(Integer lo_id) {
		this.lo_id = lo_id;
	}
	public String getLo_number() {
		return lo_number;
	}
	public void setLo_number(String lo_number) {
		this.lo_number = lo_number;
	}
	public String getLo_date() {
		return lo_date;
	}
	public void setLo_date(String lo_date) {
		this.lo_date = lo_date;
	}
	public String getLo_dp_code() {
		return lo_dp_code;
	}
	public void setLo_dp_code(String lo_dp_code) {
		this.lo_dp_code = lo_dp_code;
	}
	public String getDepot_name() {
		return depot_name;
	}
	public void setDepot_name(String depot_name) {
		this.depot_name = depot_name;
	}
	public String getLo_manager() {
		return lo_manager;
	}
	public void setLo_manager(String lo_manager) {
		this.lo_manager = lo_manager;
	}
	public Integer getLo_amount() {
		return lo_amount;
	}
	public void setLo_amount(Integer lo_amount) {
		this.lo_amount = lo_amount;
	}
	public Double getLo_money() {
		return lo_money;
	}
	public void setLo_money(Double lo_money) {
		this.lo_money = lo_money;
	}
	public String getLo_remark() {
		return lo_remark;
	}
	public void setLo_remark(String lo_remark) {
		this.lo_remark = lo_remark;
	}
	public Integer getLo_ar_state() {
		return lo_ar_state;
	}
	public void setLo_ar_state(Integer lo_ar_state) {
		this.lo_ar_state = lo_ar_state;
	}
	public String getLo_ar_date() {
		return lo_ar_date;
	}
	public void setLo_ar_date(String lo_ar_date) {
		this.lo_ar_date = lo_ar_date;
	}
	public Integer getLo_isdraft() {
		return lo_isdraft;
	}
	public void setLo_isdraft(Integer lo_isdraft) {
		this.lo_isdraft = lo_isdraft;
	}
	public String getLo_sysdate() {
		return lo_sysdate;
	}
	public void setLo_sysdate(String lo_sysdate) {
		this.lo_sysdate = lo_sysdate;
	}
	public Integer getLo_us_id() {
		return lo_us_id;
	}
	public void setLo_us_id(Integer lo_us_id) {
		this.lo_us_id = lo_us_id;
	}
	public String getLo_maker() {
		return lo_maker;
	}
	public void setLo_maker(String lo_maker) {
		this.lo_maker = lo_maker;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
	
}
