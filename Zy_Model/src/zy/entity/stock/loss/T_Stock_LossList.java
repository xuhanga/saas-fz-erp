package zy.entity.stock.loss;

import java.io.Serializable;

public class T_Stock_LossList implements Serializable{
	private static final long serialVersionUID = -8700377820086670881L;
	private Integer lol_id;
	private String lol_number;
	private String lol_pd_code;
	private String lol_sub_code;
	private String lol_sz_code;
	private String lol_szg_code;
	private String lol_cr_code;
	private String lol_br_code;
	private Integer lol_amount;
	private Double lol_unitprice;
	private Double lol_unitmoney;
	private String lol_remark;
	private Integer lol_us_id;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Integer getLol_id() {
		return lol_id;
	}
	public void setLol_id(Integer lol_id) {
		this.lol_id = lol_id;
	}
	public String getLol_number() {
		return lol_number;
	}
	public void setLol_number(String lol_number) {
		this.lol_number = lol_number;
	}
	public String getLol_pd_code() {
		return lol_pd_code;
	}
	public void setLol_pd_code(String lol_pd_code) {
		this.lol_pd_code = lol_pd_code;
	}
	public String getLol_sub_code() {
		return lol_sub_code;
	}
	public void setLol_sub_code(String lol_sub_code) {
		this.lol_sub_code = lol_sub_code;
	}
	public String getLol_sz_code() {
		return lol_sz_code;
	}
	public void setLol_sz_code(String lol_sz_code) {
		this.lol_sz_code = lol_sz_code;
	}
	public String getLol_szg_code() {
		return lol_szg_code;
	}
	public void setLol_szg_code(String lol_szg_code) {
		this.lol_szg_code = lol_szg_code;
	}
	public String getLol_cr_code() {
		return lol_cr_code;
	}
	public void setLol_cr_code(String lol_cr_code) {
		this.lol_cr_code = lol_cr_code;
	}
	public String getLol_br_code() {
		return lol_br_code;
	}
	public void setLol_br_code(String lol_br_code) {
		this.lol_br_code = lol_br_code;
	}
	public Integer getLol_amount() {
		return lol_amount;
	}
	public void setLol_amount(Integer lol_amount) {
		this.lol_amount = lol_amount;
	}
	public Double getLol_unitprice() {
		return lol_unitprice;
	}
	public void setLol_unitprice(Double lol_unitprice) {
		this.lol_unitprice = lol_unitprice;
	}
	public Double getLol_unitmoney() {
		if (lol_amount != null && lol_unitprice != null) {
			return lol_amount * lol_unitprice;
		}
		return lol_unitmoney;
	}
	public String getLol_remark() {
		return lol_remark;
	}
	public void setLol_remark(String lol_remark) {
		this.lol_remark = lol_remark;
	}
	public Integer getLol_us_id() {
		return lol_us_id;
	}
	public void setLol_us_id(Integer lol_us_id) {
		this.lol_us_id = lol_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
}
