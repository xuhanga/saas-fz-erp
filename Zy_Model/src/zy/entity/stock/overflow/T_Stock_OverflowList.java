package zy.entity.stock.overflow;

import java.io.Serializable;

public class T_Stock_OverflowList implements Serializable{
	private static final long serialVersionUID = 1076564582037414279L;
	private Integer ofl_id;
	private String ofl_number;
	private String ofl_pd_code;
	private String ofl_sub_code;
	private String ofl_sz_code;
	private String ofl_szg_code;
	private String ofl_cr_code;
	private String ofl_br_code;
	private Integer ofl_amount;
	private Double ofl_unitprice;
	private Double ofl_unitmoney;
	private String ofl_remark;
	private Integer ofl_us_id;
	private Integer companyid;
	
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_name;
	private String tp_name;
	/**
	 * 操作类型 update,add
	 */
	private String operate_type;
	public Integer getOfl_id() {
		return ofl_id;
	}
	public void setOfl_id(Integer ofl_id) {
		this.ofl_id = ofl_id;
	}
	public String getOfl_number() {
		return ofl_number;
	}
	public void setOfl_number(String ofl_number) {
		this.ofl_number = ofl_number;
	}
	public String getOfl_pd_code() {
		return ofl_pd_code;
	}
	public void setOfl_pd_code(String ofl_pd_code) {
		this.ofl_pd_code = ofl_pd_code;
	}
	public String getOfl_sub_code() {
		return ofl_sub_code;
	}
	public void setOfl_sub_code(String ofl_sub_code) {
		this.ofl_sub_code = ofl_sub_code;
	}
	public String getOfl_sz_code() {
		return ofl_sz_code;
	}
	public void setOfl_sz_code(String ofl_sz_code) {
		this.ofl_sz_code = ofl_sz_code;
	}
	public String getOfl_szg_code() {
		return ofl_szg_code;
	}
	public void setOfl_szg_code(String ofl_szg_code) {
		this.ofl_szg_code = ofl_szg_code;
	}
	public String getOfl_cr_code() {
		return ofl_cr_code;
	}
	public void setOfl_cr_code(String ofl_cr_code) {
		this.ofl_cr_code = ofl_cr_code;
	}
	public String getOfl_br_code() {
		return ofl_br_code;
	}
	public void setOfl_br_code(String ofl_br_code) {
		this.ofl_br_code = ofl_br_code;
	}
	public Integer getOfl_amount() {
		return ofl_amount;
	}
	public void setOfl_amount(Integer ofl_amount) {
		this.ofl_amount = ofl_amount;
	}
	public Double getOfl_unitprice() {
		return ofl_unitprice;
	}
	public void setOfl_unitprice(Double ofl_unitprice) {
		this.ofl_unitprice = ofl_unitprice;
	}
	public Double getOfl_unitmoney() {
		if (ofl_amount != null && ofl_unitprice != null) {
			return ofl_amount * ofl_unitprice;
		}
		return ofl_unitmoney;
	}
	public String getOfl_remark() {
		return ofl_remark;
	}
	public void setOfl_remark(String ofl_remark) {
		this.ofl_remark = ofl_remark;
	}
	public Integer getOfl_us_id() {
		return ofl_us_id;
	}
	public void setOfl_us_id(Integer ofl_us_id) {
		this.ofl_us_id = ofl_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getOperate_type() {
		return operate_type;
	}
	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}
}
