package zy.entity.stock.price;

import java.io.Serializable;

public class T_Stock_Price implements Serializable{
	private static final long serialVersionUID = 619887640581716395L;
	private Integer pc_id;
	private String pc_number;
	private String pc_date;
	private String pc_manager;
	private String pc_remark;
	private Integer pc_ar_state;
	private String pc_ar_date;
	private Integer pc_isdraft;
	private String pc_sysdate;
	private Integer pc_us_id;
	private Integer companyid;
	private String ar_describe;
	public Integer getPc_id() {
		return pc_id;
	}
	public void setPc_id(Integer pc_id) {
		this.pc_id = pc_id;
	}
	public String getPc_number() {
		return pc_number;
	}
	public void setPc_number(String pc_number) {
		this.pc_number = pc_number;
	}
	public String getPc_date() {
		return pc_date;
	}
	public void setPc_date(String pc_date) {
		this.pc_date = pc_date;
	}
	public String getPc_manager() {
		return pc_manager;
	}
	public void setPc_manager(String pc_manager) {
		this.pc_manager = pc_manager;
	}
	public String getPc_remark() {
		return pc_remark;
	}
	public void setPc_remark(String pc_remark) {
		this.pc_remark = pc_remark;
	}
	public Integer getPc_ar_state() {
		return pc_ar_state;
	}
	public void setPc_ar_state(Integer pc_ar_state) {
		this.pc_ar_state = pc_ar_state;
	}
	public String getPc_ar_date() {
		return pc_ar_date;
	}
	public void setPc_ar_date(String pc_ar_date) {
		this.pc_ar_date = pc_ar_date;
	}
	public Integer getPc_isdraft() {
		return pc_isdraft;
	}
	public void setPc_isdraft(Integer pc_isdraft) {
		this.pc_isdraft = pc_isdraft;
	}
	public String getPc_sysdate() {
		return pc_sysdate;
	}
	public void setPc_sysdate(String pc_sysdate) {
		this.pc_sysdate = pc_sysdate;
	}
	public Integer getPc_us_id() {
		return pc_us_id;
	}
	public void setPc_us_id(Integer pc_us_id) {
		this.pc_us_id = pc_us_id;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getAr_describe() {
		return ar_describe;
	}
	public void setAr_describe(String ar_describe) {
		this.ar_describe = ar_describe;
	}
}
