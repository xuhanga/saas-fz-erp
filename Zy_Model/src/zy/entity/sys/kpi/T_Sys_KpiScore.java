package zy.entity.sys.kpi;

import java.io.Serializable;

public class T_Sys_KpiScore implements Serializable{
	private static final long serialVersionUID = -8311488630714728597L;
	private Integer ks_id;
	private String ks_ki_code;
	private String ki_name;
	private Double ks_min;
	private Double ks_max;
	private Integer ks_score;
	private String ks_shop_code;
	private String ks_rw_code;
	private String reward_name;
	private Integer companyid;
	public Integer getKs_id() {
		return ks_id;
	}
	public void setKs_id(Integer ks_id) {
		this.ks_id = ks_id;
	}
	public String getKs_ki_code() {
		return ks_ki_code;
	}
	public void setKs_ki_code(String ks_ki_code) {
		this.ks_ki_code = ks_ki_code;
	}
	public String getKi_name() {
		return ki_name;
	}
	public void setKi_name(String ki_name) {
		this.ki_name = ki_name;
	}
	public Double getKs_min() {
		return ks_min;
	}
	public void setKs_min(Double ks_min) {
		this.ks_min = ks_min;
	}
	public Double getKs_max() {
		return ks_max;
	}
	public void setKs_max(Double ks_max) {
		this.ks_max = ks_max;
	}
	public Integer getKs_score() {
		return ks_score;
	}
	public void setKs_score(Integer ks_score) {
		this.ks_score = ks_score;
	}
	public String getKs_shop_code() {
		return ks_shop_code;
	}
	public void setKs_shop_code(String ks_shop_code) {
		this.ks_shop_code = ks_shop_code;
	}
	public String getKs_rw_code() {
		return ks_rw_code;
	}
	public void setKs_rw_code(String ks_rw_code) {
		this.ks_rw_code = ks_rw_code;
	}
	public String getReward_name() {
		return reward_name;
	}
	public void setReward_name(String reward_name) {
		this.reward_name = reward_name;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
