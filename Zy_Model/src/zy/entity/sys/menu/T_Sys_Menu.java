package zy.entity.sys.menu;

import java.io.Serializable;

public class T_Sys_Menu implements Serializable{
	private static final long serialVersionUID = 1L;
	private int mn_id;
	private String mn_code;
	private String mn_name;
	private String mn_upcode;
	private String mn_shop_type;
	private String mn_url;
	private String mn_style;
	private String mn_icon;
	private String mn_child;
	private int mn_state;
	private String mn_limit;
	private int mn_app;
	
	private String ro_code;
	private String rm_limit;
	public int getMn_id() {
		return mn_id;
	}
	public void setMn_id(int mn_id) {
		this.mn_id = mn_id;
	}
	public String getMn_code() {
		return mn_code;
	}
	public void setMn_code(String mn_code) {
		this.mn_code = mn_code;
	}
	public String getMn_name() {
		return mn_name;
	}
	public void setMn_name(String mn_name) {
		this.mn_name = mn_name;
	}
	public String getMn_upcode() {
		return mn_upcode;
	}
	public void setMn_upcode(String mn_upcode) {
		this.mn_upcode = mn_upcode;
	}
	public String getMn_shop_type() {
		return mn_shop_type;
	}
	public void setMn_shop_type(String mn_shop_type) {
		this.mn_shop_type = mn_shop_type;
	}
	public String getMn_url() {
		return mn_url;
	}
	public void setMn_url(String mn_url) {
		this.mn_url = mn_url;
	}
	public String getMn_style() {
		return mn_style;
	}
	public void setMn_style(String mn_style) {
		this.mn_style = mn_style;
	}
	public String getMn_icon() {
		return mn_icon;
	}
	public void setMn_icon(String mn_icon) {
		this.mn_icon = mn_icon;
	}
	public int getMn_state() {
		return mn_state;
	}
	public void setMn_state(int mn_state) {
		this.mn_state = mn_state;
	}
	public String getMn_limit() {
		return mn_limit;
	}
	public void setMn_limit(String mn_limit) {
		this.mn_limit = mn_limit;
	}
	public String getMn_child() {
		return mn_child;
	}
	public void setMn_child(String mn_child) {
		this.mn_child = mn_child;
	}
	public int getMn_app() {
		return mn_app;
	}
	public void setMn_app(int mn_app) {
		this.mn_app = mn_app;
	}
	public String getRo_code() {
		return ro_code;
	}
	public void setRo_code(String ro_code) {
		this.ro_code = ro_code;
	}
	public String getRm_limit() {
		return rm_limit;
	}
	public void setRm_limit(String rm_limit) {
		this.rm_limit = rm_limit;
	}
}
