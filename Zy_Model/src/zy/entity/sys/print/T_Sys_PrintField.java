package zy.entity.sys.print;

import java.io.Serializable;

public class T_Sys_PrintField implements Serializable{
	private static final long serialVersionUID = 9181351998872547142L;
	private Integer spf_id;
	private Integer spf_sp_id;
	private String spf_code;
	private String spf_name;
	private String spf_namecustom;
	private Integer spf_colspan;
	private Integer spf_show;
	private Integer spf_line;
	private Integer spf_row;
	private Integer spf_position;
	private Integer companyid;
	public Integer getSpf_id() {
		return spf_id;
	}
	public void setSpf_id(Integer spf_id) {
		this.spf_id = spf_id;
	}
	public Integer getSpf_sp_id() {
		return spf_sp_id;
	}
	public void setSpf_sp_id(Integer spf_sp_id) {
		this.spf_sp_id = spf_sp_id;
	}
	public String getSpf_code() {
		return spf_code;
	}
	public void setSpf_code(String spf_code) {
		this.spf_code = spf_code;
	}
	public String getSpf_name() {
		return spf_name;
	}
	public void setSpf_name(String spf_name) {
		this.spf_name = spf_name;
	}
	public String getSpf_namecustom() {
		return spf_namecustom;
	}
	public void setSpf_namecustom(String spf_namecustom) {
		this.spf_namecustom = spf_namecustom;
	}
	public Integer getSpf_colspan() {
		return spf_colspan;
	}
	public void setSpf_colspan(Integer spf_colspan) {
		this.spf_colspan = spf_colspan;
	}
	public Integer getSpf_show() {
		return spf_show;
	}
	public void setSpf_show(Integer spf_show) {
		this.spf_show = spf_show;
	}
	public Integer getSpf_line() {
		return spf_line;
	}
	public void setSpf_line(Integer spf_line) {
		this.spf_line = spf_line;
	}
	public Integer getSpf_row() {
		return spf_row;
	}
	public void setSpf_row(Integer spf_row) {
		this.spf_row = spf_row;
	}
	public Integer getSpf_position() {
		return spf_position;
	}
	public void setSpf_position(Integer spf_position) {
		this.spf_position = spf_position;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
