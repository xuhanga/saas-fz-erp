package zy.entity.sys.reward;

import java.io.Serializable;

public class T_Sys_Reward implements Serializable{
	private static final long serialVersionUID = 6295002829680788000L;
	private Integer rw_id;
	private String rw_code;
	private String rw_name;
	private Integer rw_score;
	private String rw_icon;
	private String rw_style;
	private String rw_remark;
	private String rw_shop_code;
	private Integer companyid;
	public Integer getRw_id() {
		return rw_id;
	}
	public void setRw_id(Integer rw_id) {
		this.rw_id = rw_id;
	}
	public String getRw_code() {
		return rw_code;
	}
	public void setRw_code(String rw_code) {
		this.rw_code = rw_code;
	}
	public String getRw_name() {
		return rw_name;
	}
	public void setRw_name(String rw_name) {
		this.rw_name = rw_name;
	}
	public Integer getRw_score() {
		return rw_score;
	}
	public void setRw_score(Integer rw_score) {
		this.rw_score = rw_score;
	}
	public String getRw_icon() {
		return rw_icon;
	}
	public void setRw_icon(String rw_icon) {
		this.rw_icon = rw_icon;
	}
	public String getRw_style() {
		return rw_style;
	}
	public void setRw_style(String rw_style) {
		this.rw_style = rw_style;
	}
	public String getRw_remark() {
		return rw_remark;
	}
	public void setRw_remark(String rw_remark) {
		this.rw_remark = rw_remark;
	}
	public String getRw_shop_code() {
		return rw_shop_code;
	}
	public void setRw_shop_code(String rw_shop_code) {
		this.rw_shop_code = rw_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
