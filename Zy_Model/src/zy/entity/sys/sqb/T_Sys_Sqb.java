package zy.entity.sys.sqb;

import java.io.Serializable;

public class T_Sys_Sqb implements Serializable{
	private static final long serialVersionUID = 2586209971621058867L;
	private Integer sqb_id;
	private String sqb_device_id;
	private String sqb_device_name;
	private String sqb_terminal_sn;
	private String sqb_terminal_key;
	private String sqb_activate_time;
	private String sqb_checkin_time;
	private String sqb_shop_code;
	private Integer sqb_state;
	private Integer companyid;
	public Integer getSqb_id() {
		return sqb_id;
	}
	public void setSqb_id(Integer sqb_id) {
		this.sqb_id = sqb_id;
	}
	public String getSqb_device_id() {
		return sqb_device_id;
	}
	public void setSqb_device_id(String sqb_device_id) {
		this.sqb_device_id = sqb_device_id;
	}
	public String getSqb_device_name() {
		return sqb_device_name;
	}
	public void setSqb_device_name(String sqb_device_name) {
		this.sqb_device_name = sqb_device_name;
	}
	public String getSqb_terminal_sn() {
		return sqb_terminal_sn;
	}
	public void setSqb_terminal_sn(String sqb_terminal_sn) {
		this.sqb_terminal_sn = sqb_terminal_sn;
	}
	public String getSqb_terminal_key() {
		return sqb_terminal_key;
	}
	public void setSqb_terminal_key(String sqb_terminal_key) {
		this.sqb_terminal_key = sqb_terminal_key;
	}
	public String getSqb_activate_time() {
		return sqb_activate_time;
	}
	public void setSqb_activate_time(String sqb_activate_time) {
		this.sqb_activate_time = sqb_activate_time;
	}
	public String getSqb_checkin_time() {
		return sqb_checkin_time;
	}
	public void setSqb_checkin_time(String sqb_checkin_time) {
		this.sqb_checkin_time = sqb_checkin_time;
	}
	public String getSqb_shop_code() {
		return sqb_shop_code;
	}
	public void setSqb_shop_code(String sqb_shop_code) {
		this.sqb_shop_code = sqb_shop_code;
	}
	public Integer getSqb_state() {
		return sqb_state;
	}
	public void setSqb_state(Integer sqb_state) {
		this.sqb_state = sqb_state;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
