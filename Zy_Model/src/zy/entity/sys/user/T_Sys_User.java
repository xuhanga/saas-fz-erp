package zy.entity.sys.user;

import java.io.Serializable;

public class T_Sys_User implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer us_id;
	private String us_name;
	private String us_code;
	private String us_account;
	private String us_pass;
	private int us_state;
	private String us_date;
	private String us_last;
	private int us_lock;
	private String us_end;
	private int us_pay;
	private int us_default;
	private String us_ro_code;
	private String ro_name;
	private String us_shop_code;
	private String shop_upcode;
	private String shop_name;
	private String us_limit;
	private int companyid;
	private String companyname;
	private int sp_init;
	private String co_code;
	private int co_type;
	private int co_ver;
	private int co_users;
	private int co_shops;
	private String shoptype;
	private String shop_uptype;
	private String ty_name; 
	public Integer getUs_id() {
		return us_id;
	}
	public void setUs_id(Integer us_id) {
		this.us_id = us_id;
	}
	
	public String getUs_account() {
		return us_account;
	}
	public void setUs_account(String us_account) {
		this.us_account = us_account;
	}
	public String getUs_name() {
		return us_name;
	}
	public void setUs_name(String us_name) {
		this.us_name = us_name;
	}
	public String getUs_code() {
		return us_code;
	}
	public void setUs_code(String us_code) {
		this.us_code = us_code;
	}
	public String getUs_pass() {
		return us_pass;
	}
	public void setUs_pass(String us_pass) {
		this.us_pass = us_pass;
	}
	public int getUs_state() {
		return us_state;
	}
	public void setUs_state(int us_state) {
		this.us_state = us_state;
	}
	public String getUs_date() {
		return us_date;
	}
	public void setUs_date(String us_date) {
		this.us_date = us_date;
	}
	public String getUs_last() {
		return us_last;
	}
	public void setUs_last(String us_last) {
		this.us_last = us_last;
	}
	public int getUs_lock() {
		return us_lock;
	}
	public void setUs_lock(int us_lock) {
		this.us_lock = us_lock;
	}
	public String getUs_end() {
		return us_end;
	}
	public void setUs_end(String us_end) {
		this.us_end = us_end;
	}
	public int getUs_pay() {
		return us_pay;
	}
	public void setUs_pay(int us_pay) {
		this.us_pay = us_pay;
	}
	public int getUs_default() {
		return us_default;
	}
	public void setUs_default(int us_default) {
		this.us_default = us_default;
	}
	public String getUs_ro_code() {
		return us_ro_code;
	}
	public void setUs_ro_code(String us_ro_code) {
		this.us_ro_code = us_ro_code;
	}
	public String getUs_shop_code() {
		return us_shop_code;
	}
	public void setUs_shop_code(String us_shop_code) {
		this.us_shop_code = us_shop_code;
	}
	public String getShop_upcode() {
		return shop_upcode;
	}
	public void setShop_upcode(String shop_upcode) {
		this.shop_upcode = shop_upcode;
	}
	public String getUs_limit() {
		return us_limit;
	}
	public void setUs_limit(String us_limit) {
		this.us_limit = us_limit;
	}
	public int getCompanyid() {
		return companyid;
	}
	public void setCompanyid(int companyid) {
		this.companyid = companyid;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public int getCo_type() {
		return co_type;
	}
	public void setCo_type(int co_type) {
		this.co_type = co_type;
	}
	public int getSp_init() {
		return sp_init;
	}
	public void setSp_init(int sp_init) {
		this.sp_init = sp_init;
	}
	public int getCo_ver() {
		return co_ver;
	}
	public void setCo_ver(int co_ver) {
		this.co_ver = co_ver;
	}
	public int getCo_users() {
		return co_users;
	}
	public void setCo_users(int co_users) {
		this.co_users = co_users;
	}
	public int getCo_shops() {
		return co_shops;
	}
	public void setCo_shops(int co_shops) {
		this.co_shops = co_shops;
	}
	public String getShoptype() {
		return shoptype;
	}
	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}
	public String getShop_uptype() {
		return shop_uptype;
	}
	public void setShop_uptype(String shop_uptype) {
		this.shop_uptype = shop_uptype;
	}
	public String getRo_name() {
		return ro_name;
	}
	public void setRo_name(String ro_name) {
		this.ro_name = ro_name;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getTy_name() {
		return ty_name;
	}
	public void setTy_name(String ty_name) {
		this.ty_name = ty_name;
	}
	public String getCo_code() {
		return co_code;
	}
	public void setCo_code(String co_code) {
		this.co_code = co_code;
	}
	
}
