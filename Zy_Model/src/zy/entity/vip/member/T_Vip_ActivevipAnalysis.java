package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 活跃会员分析报表实体类
 *
 */
public class T_Vip_ActivevipAnalysis implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Integer id;
	private String vm_cardcode;
	private String vm_name;
	private String mt_name;
	private String gd_name;
	private String vm_sex;
	private String vm_mobile;
	private Double vm_total_point;
	private Double vm_points;
	private Integer vm_times;
	private Double vm_total_money;
	private Integer buy_count;
	private Double buy_money;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVm_cardcode() {
		return vm_cardcode;
	}
	public void setVm_cardcode(String vm_cardcode) {
		this.vm_cardcode = vm_cardcode;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public String getGd_name() {
		return gd_name;
	}
	public void setGd_name(String gd_name) {
		this.gd_name = gd_name;
	}
	public String getVm_sex() {
		return vm_sex;
	}
	public void setVm_sex(String vm_sex) {
		this.vm_sex = vm_sex;
	}
	public String getVm_mobile() {
		return vm_mobile;
	}
	public void setVm_mobile(String vm_mobile) {
		this.vm_mobile = vm_mobile;
	}
	public Double getVm_total_point() {
		return vm_total_point;
	}
	public void setVm_total_point(Double vm_total_point) {
		this.vm_total_point = vm_total_point;
	}
	public Double getVm_points() {
		return vm_points;
	}
	public void setVm_points(Double vm_points) {
		this.vm_points = vm_points;
	}
	public Integer getBuy_count() {
		return buy_count;
	}
	public void setBuy_count(Integer buy_count) {
		this.buy_count = buy_count;
	}
	public Double getBuy_money() {
		return buy_money;
	}
	public void setBuy_money(Double buy_money) {
		this.buy_money = buy_money;
	}
	public Integer getVm_times() {
		return vm_times;
	}
	public void setVm_times(Integer vm_times) {
		this.vm_times = vm_times;
	}
	public Double getVm_total_money() {
		return vm_total_money;
	}
	public void setVm_total_money(Double vm_total_money) {
		this.vm_total_money = vm_total_money;
	}
}
