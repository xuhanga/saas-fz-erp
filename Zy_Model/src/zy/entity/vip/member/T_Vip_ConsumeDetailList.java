package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 会员类别报表实体类
 *
 */
public class T_Vip_ConsumeDetailList implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Integer id;
	private String vm_cardcode;
	private String vm_name;
	private String vm_mobile;
	private String vm_manager;
	private String vm_manager_code;
	private String shl_number;
	private String shl_date;
	private String pd_no;
	private String pd_name;
	private String pd_img_path;
	private String pd_unit;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private Integer shl_amount;
	private Double shl_sell_price;
	private Double shl_discount;
	private Double shl_price;
	private Double shl_money;
	private String sp_name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVm_cardcode() {
		return vm_cardcode;
	}
	public void setVm_cardcode(String vm_cardcode) {
		this.vm_cardcode = vm_cardcode;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getVm_mobile() {
		return vm_mobile;
	}
	public void setVm_mobile(String vm_mobile) {
		this.vm_mobile = vm_mobile;
	}
	public String getVm_manager() {
		return vm_manager;
	}
	public void setVm_manager(String vm_manager) {
		this.vm_manager = vm_manager;
	}
	public String getVm_manager_code() {
		return vm_manager_code;
	}
	public void setVm_manager_code(String vm_manager_code) {
		this.vm_manager_code = vm_manager_code;
	}
	public String getShl_number() {
		return shl_number;
	}
	public void setShl_number(String shl_number) {
		this.shl_number = shl_number;
	}
	public String getShl_date() {
		return shl_date;
	}
	public void setShl_date(String shl_date) {
		this.shl_date = shl_date;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public Integer getShl_amount() {
		return shl_amount;
	}
	public void setShl_amount(Integer shl_amount) {
		this.shl_amount = shl_amount;
	}
	public Double getShl_sell_price() {
		return shl_sell_price;
	}
	public void setShl_sell_price(Double shl_sell_price) {
		this.shl_sell_price = shl_sell_price;
	}
	public Double getShl_discount() {
		return shl_discount;
	}
	public void setShl_discount(Double shl_discount) {
		this.shl_discount = shl_discount;
	}
	public Double getShl_price() {
		return shl_price;
	}
	public void setShl_price(Double shl_price) {
		this.shl_price = shl_price;
	}
	public Double getShl_money() {
		return shl_money;
	}
	public void setShl_money(Double shl_money) {
		this.shl_money = shl_money;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getPd_img_path() {
		return pd_img_path;
	}
	public void setPd_img_path(String pd_img_path) {
		this.pd_img_path = pd_img_path;
	}
	
}
