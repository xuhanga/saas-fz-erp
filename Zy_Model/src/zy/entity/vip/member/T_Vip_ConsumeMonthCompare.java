package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 
 *
 */
public class T_Vip_ConsumeMonthCompare implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Double sh_money;
	private String vm_month;
	public Double getSh_money() {
		return sh_money;
	}
	public void setSh_money(Double sh_money) {
		this.sh_money = sh_money;
	}
	public String getVm_month() {
		return vm_month;
	}
	public void setVm_month(String vm_month) {
		this.vm_month = vm_month;
	}
}
