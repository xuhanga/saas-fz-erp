package zy.entity.vip.member;

import java.io.Serializable;

import zy.entity.sell.cashier.T_Sell_Cashier;

public class T_Vip_Member implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer vm_id;
	private String vm_code;
	private String vm_mobile;
	private String vm_cardcode;
	private String vm_mt_code;
	private Double mt_discount;
	private String vm_shop_code;
	private T_Sell_Cashier cashier;
	private String shop_name;
	private String mt_name;
	private String vm_name;
	private String vm_sex;
	private String vm_password;
	private String vm_type;//会员消费的类型
	private Integer vm_birthday_type;
	private String vm_birthday;
	private String isbirthday;
	private String vm_lunar_birth;
	private Integer vm_state;
	private String vm_date;
	private String vm_enddate;
	private String vm_manager_code;
	private String vm_manager;
	private String vm_old_manager;
	private Double vm_total_point;
	private Double vm_points;
	private Double vm_used_points;
	private Double vm_last_points;
	private Double vm_init_points;
	private Integer vm_times;
	private Double vm_total_money;
	private String vm_lastbuy_date;
	private String vm_notbuy_day;//未购物天数
	private Double vm_lastbuy_money;
	private Integer companyid;
	private String vm_sysdate;
	private String vm_img_path;
	private Integer vip_other;//是否是借卡，仅收银时使用
	private Double sh_sell_money;
	private String sh_sysdate;
	private String vi_visit_date;
	private String vi_manager;
	private String gd_code;
	private String gd_name;//会员等级
	private String vm_remark;
	private Integer vm_age;
	private String vm_tag_name;
	public Integer getVm_id() {
		return vm_id;
	}
	public void setVm_id(Integer vm_id) {
		this.vm_id = vm_id;
	}
	public String getVm_code() {
		return vm_code;
	}
	public void setVm_code(String vm_code) {
		this.vm_code = vm_code;
	}
	public String getVm_mobile() {
		return vm_mobile;
	}
	public void setVm_mobile(String vm_mobile) {
		this.vm_mobile = vm_mobile;
	}
	public String getVm_cardcode() {
		return vm_cardcode;
	}
	public void setVm_cardcode(String vm_cardcode) {
		this.vm_cardcode = vm_cardcode;
	}
	public String getVm_mt_code() {
		return vm_mt_code;
	}
	public void setVm_mt_code(String vm_mt_code) {
		this.vm_mt_code = vm_mt_code;
	}
	public String getVm_shop_code() {
		return vm_shop_code;
	}
	public void setVm_shop_code(String vm_shop_code) {
		this.vm_shop_code = vm_shop_code;
	}
	public T_Sell_Cashier getCashier() {
		return cashier;
	}
	public void setCashier(T_Sell_Cashier cashier) {
		this.cashier = cashier;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getVm_sex() {
		return vm_sex;
	}
	public void setVm_sex(String vm_sex) {
		this.vm_sex = vm_sex;
	}
	public String getVm_password() {
		return vm_password;
	}
	public void setVm_password(String vm_password) {
		this.vm_password = vm_password;
	}
	public Integer getVm_birthday_type() {
		return vm_birthday_type;
	}
	public void setVm_birthday_type(Integer vm_birthday_type) {
		this.vm_birthday_type = vm_birthday_type;
	}
	public String getVm_birthday() {
		return vm_birthday;
	}
	public void setVm_birthday(String vm_birthday) {
		this.vm_birthday = vm_birthday;
	}
	public String getVm_lunar_birth() {
		return vm_lunar_birth;
	}
	public void setVm_lunar_birth(String vm_lunar_birth) {
		this.vm_lunar_birth = vm_lunar_birth;
	}
	public Integer getVm_state() {
		return vm_state;
	}
	public void setVm_state(Integer vm_state) {
		this.vm_state = vm_state;
	}
	public String getVm_date() {
		return vm_date;
	}
	public void setVm_date(String vm_date) {
		this.vm_date = vm_date;
	}
	public String getVm_enddate() {
		return vm_enddate;
	}
	public void setVm_enddate(String vm_enddate) {
		this.vm_enddate = vm_enddate;
	}
	public String getVm_manager_code() {
		return vm_manager_code;
	}
	public void setVm_manager_code(String vm_manager_code) {
		this.vm_manager_code = vm_manager_code;
	}
	public String getVm_manager() {
		return vm_manager;
	}
	public void setVm_manager(String vm_manager) {
		this.vm_manager = vm_manager;
	}
	public Double getVm_points() {
		return vm_points;
	}
	public void setVm_points(Double vm_points) {
		this.vm_points = vm_points;
	}
	public Double getVm_used_points() {
		return vm_used_points;
	}
	public void setVm_used_points(Double vm_used_points) {
		this.vm_used_points = vm_used_points;
	}
	public Double getVm_init_points() {
		return vm_init_points;
	}
	public void setVm_init_points(Double vm_init_points) {
		this.vm_init_points = vm_init_points;
	}
	public Integer getVm_times() {
		return vm_times;
	}
	public void setVm_times(Integer vm_times) {
		this.vm_times = vm_times;
	}
	public Double getVm_total_money() {
		return vm_total_money;
	}
	public void setVm_total_money(Double vm_total_money) {
		this.vm_total_money = vm_total_money;
	}
	public String getVm_lastbuy_date() {
		return vm_lastbuy_date;
	}
	public void setVm_lastbuy_date(String vm_lastbuy_date) {
		this.vm_lastbuy_date = vm_lastbuy_date;
	}
	public Double getVm_lastbuy_money() {
		return vm_lastbuy_money;
	}
	public void setVm_lastbuy_money(Double vm_lastbuy_money) {
		this.vm_lastbuy_money = vm_lastbuy_money;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getIsbirthday() {
		return isbirthday;
	}
	public void setIsbirthday(String isbirthday) {
		this.isbirthday = isbirthday;
	}
	public Double getMt_discount() {
		return mt_discount;
	}
	public void setMt_discount(Double mt_discount) {
		this.mt_discount = mt_discount;
	}
	public Integer getVip_other() {
		return vip_other;
	}
	public void setVip_other(Integer vip_other) {
		this.vip_other = vip_other;
	}
	public String getVm_sysdate() {
		return vm_sysdate;
	}
	public void setVm_sysdate(String vm_sysdate) {
		this.vm_sysdate = vm_sysdate;
	}
	public Double getVm_last_points() {
		return vm_last_points;
	}
	public void setVm_last_points(Double vm_last_points) {
		this.vm_last_points = vm_last_points;
	}
	public String getVm_old_manager() {
		return vm_old_manager;
	}
	public void setVm_old_manager(String vm_old_manager) {
		this.vm_old_manager = vm_old_manager;
	}
	public Double getVm_total_point() {
		return vm_total_point;
	}
	public void setVm_total_point(Double vm_total_point) {
		this.vm_total_point = vm_total_point;
	}
	public Double getSh_sell_money() {
		return sh_sell_money;
	}
	public void setSh_sell_money(Double sh_sell_money) {
		this.sh_sell_money = sh_sell_money;
	}
	public String getSh_sysdate() {
		return sh_sysdate;
	}
	public void setSh_sysdate(String sh_sysdate) {
		this.sh_sysdate = sh_sysdate;
	}
	public String getVi_visit_date() {
		return vi_visit_date;
	}
	public void setVi_visit_date(String vi_visit_date) {
		this.vi_visit_date = vi_visit_date;
	}
	public String getVi_manager() {
		return vi_manager;
	}
	public void setVi_manager(String vi_manager) {
		this.vi_manager = vi_manager;
	}
	public String getGd_code() {
		return gd_code;
	}
	public void setGd_code(String gd_code) {
		this.gd_code = gd_code;
	}
	public String getGd_name() {
		return gd_name;
	}
	public void setGd_name(String gd_name) {
		this.gd_name = gd_name;
	}
	public String getVm_img_path() {
		return vm_img_path;
	}
	public void setVm_img_path(String vm_img_path) {
		this.vm_img_path = vm_img_path;
	}
	public String getVm_type() {
		return vm_type;
	}
	public void setVm_type(String vm_type) {
		this.vm_type = vm_type;
	}
	public String getVm_remark() {
		return vm_remark;
	}
	public void setVm_remark(String vm_remark) {
		this.vm_remark = vm_remark;
	}
	public Integer getVm_age() {
		return vm_age;
	}
	public void setVm_age(Integer vm_age) {
		this.vm_age = vm_age;
	}
	public String getVm_notbuy_day() {
		return vm_notbuy_day;
	}
	public void setVm_notbuy_day(String vm_notbuy_day) {
		this.vm_notbuy_day = vm_notbuy_day;
	}
	public String getVm_tag_name() {
		return vm_tag_name;
	}
	public void setVm_tag_name(String vm_tag_name) {
		this.vm_tag_name = vm_tag_name;
	}
}
