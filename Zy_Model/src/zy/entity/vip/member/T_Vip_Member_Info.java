package zy.entity.vip.member;

import java.io.Serializable;

public class T_Vip_Member_Info implements Serializable{
	private static final long serialVersionUID = 7576782667296327422L;
	private Integer vmi_id;
	private String vmi_code;
	private String vmi_anima;
	private Integer vmi_marriage_state;
	private String vmi_nation;
	private String vmi_bloodtype;
	private Integer vmi_idcard_type;
	private String vmi_idcard;
	private String vmi_job;
	private String vmi_qq;
	private String vmi_wechat;
	private String vmi_email;
	private String vmi_address;
	private String vmi_work_unit;
	private String vmi_faith;
	private String vmi_family_festival;
	private String vmi_culture;
	private String vmi_family;
	private String vmi_oral_habit;
	private String vmi_transport;
	private String vmi_like_topic;
	private String vmi_taboo_topic;
	private String vmi_smoke;
	private String vmi_like_food;
	private String vmi_income_level;
	private String vmi_character;
	private String vmi_hobby;
	private String vmi_height;
	private String vmi_weight;
	private String vmi_skin;
	private String vmi_bust;
	private String vmi_waist;
	private String vmi_hips;
	private String vmi_foottype;
	private String vmi_size_top;
	private String vmi_size_lower;
	private String vmi_size_shoes;
	private String vmi_size_bra;
	private String vmi_trousers_length;
	private String vmi_like_color;
	private String vmi_like_brand;
	private String vmi_wear_prefer;
	private Integer companyid;
	public Integer getVmi_id() {
		return vmi_id;
	}
	public void setVmi_id(Integer vmi_id) {
		this.vmi_id = vmi_id;
	}
	public String getVmi_code() {
		return vmi_code;
	}
	public void setVmi_code(String vmi_code) {
		this.vmi_code = vmi_code;
	}
	public String getVmi_anima() {
		return vmi_anima;
	}
	public void setVmi_anima(String vmi_anima) {
		this.vmi_anima = vmi_anima;
	}
	public Integer getVmi_marriage_state() {
		return vmi_marriage_state;
	}
	public void setVmi_marriage_state(Integer vmi_marriage_state) {
		this.vmi_marriage_state = vmi_marriage_state;
	}
	public String getVmi_nation() {
		return vmi_nation;
	}
	public void setVmi_nation(String vmi_nation) {
		this.vmi_nation = vmi_nation;
	}
	public String getVmi_bloodtype() {
		return vmi_bloodtype;
	}
	public void setVmi_bloodtype(String vmi_bloodtype) {
		this.vmi_bloodtype = vmi_bloodtype;
	}
	public Integer getVmi_idcard_type() {
		return vmi_idcard_type;
	}
	public void setVmi_idcard_type(Integer vmi_idcard_type) {
		this.vmi_idcard_type = vmi_idcard_type;
	}
	public String getVmi_idcard() {
		return vmi_idcard;
	}
	public void setVmi_idcard(String vmi_idcard) {
		this.vmi_idcard = vmi_idcard;
	}
	public String getVmi_job() {
		return vmi_job;
	}
	public void setVmi_job(String vmi_job) {
		this.vmi_job = vmi_job;
	}
	public String getVmi_qq() {
		return vmi_qq;
	}
	public void setVmi_qq(String vmi_qq) {
		this.vmi_qq = vmi_qq;
	}
	public String getVmi_wechat() {
		return vmi_wechat;
	}
	public void setVmi_wechat(String vmi_wechat) {
		this.vmi_wechat = vmi_wechat;
	}
	public String getVmi_email() {
		return vmi_email;
	}
	public void setVmi_email(String vmi_email) {
		this.vmi_email = vmi_email;
	}
	public String getVmi_address() {
		return vmi_address;
	}
	public void setVmi_address(String vmi_address) {
		this.vmi_address = vmi_address;
	}
	public String getVmi_work_unit() {
		return vmi_work_unit;
	}
	public void setVmi_work_unit(String vmi_work_unit) {
		this.vmi_work_unit = vmi_work_unit;
	}
	public String getVmi_faith() {
		return vmi_faith;
	}
	public void setVmi_faith(String vmi_faith) {
		this.vmi_faith = vmi_faith;
	}
	public String getVmi_family_festival() {
		return vmi_family_festival;
	}
	public void setVmi_family_festival(String vmi_family_festival) {
		this.vmi_family_festival = vmi_family_festival;
	}
	public String getVmi_culture() {
		return vmi_culture;
	}
	public void setVmi_culture(String vmi_culture) {
		this.vmi_culture = vmi_culture;
	}
	public String getVmi_family() {
		return vmi_family;
	}
	public void setVmi_family(String vmi_family) {
		this.vmi_family = vmi_family;
	}
	public String getVmi_oral_habit() {
		return vmi_oral_habit;
	}
	public void setVmi_oral_habit(String vmi_oral_habit) {
		this.vmi_oral_habit = vmi_oral_habit;
	}
	public String getVmi_transport() {
		return vmi_transport;
	}
	public void setVmi_transport(String vmi_transport) {
		this.vmi_transport = vmi_transport;
	}
	public String getVmi_like_topic() {
		return vmi_like_topic;
	}
	public void setVmi_like_topic(String vmi_like_topic) {
		this.vmi_like_topic = vmi_like_topic;
	}
	public String getVmi_taboo_topic() {
		return vmi_taboo_topic;
	}
	public void setVmi_taboo_topic(String vmi_taboo_topic) {
		this.vmi_taboo_topic = vmi_taboo_topic;
	}
	public String getVmi_smoke() {
		return vmi_smoke;
	}
	public void setVmi_smoke(String vmi_smoke) {
		this.vmi_smoke = vmi_smoke;
	}
	public String getVmi_like_food() {
		return vmi_like_food;
	}
	public void setVmi_like_food(String vmi_like_food) {
		this.vmi_like_food = vmi_like_food;
	}
	public String getVmi_income_level() {
		return vmi_income_level;
	}
	public void setVmi_income_level(String vmi_income_level) {
		this.vmi_income_level = vmi_income_level;
	}
	public String getVmi_character() {
		return vmi_character;
	}
	public void setVmi_character(String vmi_character) {
		this.vmi_character = vmi_character;
	}
	public String getVmi_hobby() {
		return vmi_hobby;
	}
	public void setVmi_hobby(String vmi_hobby) {
		this.vmi_hobby = vmi_hobby;
	}
	public String getVmi_height() {
		return vmi_height;
	}
	public void setVmi_height(String vmi_height) {
		this.vmi_height = vmi_height;
	}
	public String getVmi_weight() {
		return vmi_weight;
	}
	public void setVmi_weight(String vmi_weight) {
		this.vmi_weight = vmi_weight;
	}
	public String getVmi_skin() {
		return vmi_skin;
	}
	public void setVmi_skin(String vmi_skin) {
		this.vmi_skin = vmi_skin;
	}
	public String getVmi_bust() {
		return vmi_bust;
	}
	public void setVmi_bust(String vmi_bust) {
		this.vmi_bust = vmi_bust;
	}
	public String getVmi_waist() {
		return vmi_waist;
	}
	public void setVmi_waist(String vmi_waist) {
		this.vmi_waist = vmi_waist;
	}
	public String getVmi_hips() {
		return vmi_hips;
	}
	public void setVmi_hips(String vmi_hips) {
		this.vmi_hips = vmi_hips;
	}
	public String getVmi_foottype() {
		return vmi_foottype;
	}
	public void setVmi_foottype(String vmi_foottype) {
		this.vmi_foottype = vmi_foottype;
	}
	public String getVmi_size_top() {
		return vmi_size_top;
	}
	public void setVmi_size_top(String vmi_size_top) {
		this.vmi_size_top = vmi_size_top;
	}
	public String getVmi_size_lower() {
		return vmi_size_lower;
	}
	public void setVmi_size_lower(String vmi_size_lower) {
		this.vmi_size_lower = vmi_size_lower;
	}
	public String getVmi_size_shoes() {
		return vmi_size_shoes;
	}
	public void setVmi_size_shoes(String vmi_size_shoes) {
		this.vmi_size_shoes = vmi_size_shoes;
	}
	public String getVmi_size_bra() {
		return vmi_size_bra;
	}
	public void setVmi_size_bra(String vmi_size_bra) {
		this.vmi_size_bra = vmi_size_bra;
	}
	public String getVmi_trousers_length() {
		return vmi_trousers_length;
	}
	public void setVmi_trousers_length(String vmi_trousers_length) {
		this.vmi_trousers_length = vmi_trousers_length;
	}
	public String getVmi_like_color() {
		return vmi_like_color;
	}
	public void setVmi_like_color(String vmi_like_color) {
		this.vmi_like_color = vmi_like_color;
	}
	public String getVmi_like_brand() {
		return vmi_like_brand;
	}
	public void setVmi_like_brand(String vmi_like_brand) {
		this.vmi_like_brand = vmi_like_brand;
	}
	public String getVmi_wear_prefer() {
		return vmi_wear_prefer;
	}
	public void setVmi_wear_prefer(String vmi_wear_prefer) {
		this.vmi_wear_prefer = vmi_wear_prefer;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
