package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 无购物会员分析实体类
 *
 */
public class T_Vip_NoShopAnalysis implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Integer id;
	private String vm_cardcode;
	private String vm_name;
	private String mt_name;
	private String vm_sex;
	private String vm_mobile;
	private Integer vm_times;
	private Double vm_total_money;
	private Double vm_total_point;
	private Double vm_points;
	private String sh_date;
	private String vm_lastbuy_date;
	private Double vm_lastbuy_money;
	private String vm_date;
	private String vm_manager_code;
	private String vm_manager;
	private String vm_shop_code;
	private String sp_name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVm_cardcode() {
		return vm_cardcode;
	}
	public void setVm_cardcode(String vm_cardcode) {
		this.vm_cardcode = vm_cardcode;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public String getVm_sex() {
		return vm_sex;
	}
	public void setVm_sex(String vm_sex) {
		this.vm_sex = vm_sex;
	}
	public String getVm_mobile() {
		return vm_mobile;
	}
	public void setVm_mobile(String vm_mobile) {
		this.vm_mobile = vm_mobile;
	}
	public Integer getVm_times() {
		return vm_times;
	}
	public void setVm_times(Integer vm_times) {
		this.vm_times = vm_times;
	}
	public Double getVm_total_money() {
		return vm_total_money;
	}
	public void setVm_total_money(Double vm_total_money) {
		this.vm_total_money = vm_total_money;
	}
	public Double getVm_total_point() {
		return vm_total_point;
	}
	public void setVm_total_point(Double vm_total_point) {
		this.vm_total_point = vm_total_point;
	}
	public Double getVm_points() {
		return vm_points;
	}
	public void setVm_points(Double vm_points) {
		this.vm_points = vm_points;
	}
	public String getSh_date() {
		return sh_date;
	}
	public void setSh_date(String sh_date) {
		this.sh_date = sh_date;
	}
	public String getVm_date() {
		return vm_date;
	}
	public void setVm_date(String vm_date) {
		this.vm_date = vm_date;
	}
	public String getVm_manager_code() {
		return vm_manager_code;
	}
	public void setVm_manager_code(String vm_manager_code) {
		this.vm_manager_code = vm_manager_code;
	}
	public String getVm_lastbuy_date() {
		return vm_lastbuy_date;
	}
	public void setVm_lastbuy_date(String vm_lastbuy_date) {
		this.vm_lastbuy_date = vm_lastbuy_date;
	}
	public Double getVm_lastbuy_money() {
		return vm_lastbuy_money;
	}
	public void setVm_lastbuy_money(Double vm_lastbuy_money) {
		this.vm_lastbuy_money = vm_lastbuy_money;
	}
	public String getVm_manager() {
		return vm_manager;
	}
	public void setVm_manager(String vm_manager) {
		this.vm_manager = vm_manager;
	}
	public String getVm_shop_code() {
		return vm_shop_code;
	}
	public void setVm_shop_code(String vm_shop_code) {
		this.vm_shop_code = vm_shop_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
}
