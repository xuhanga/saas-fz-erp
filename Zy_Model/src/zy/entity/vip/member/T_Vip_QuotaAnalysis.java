package zy.entity.vip.member;

import java.io.Serializable;

/**
 * 会员指标分析
 *
 */
public class T_Vip_QuotaAnalysis implements Serializable{
	private static final long serialVersionUID = 7984068804508490205L;
	private Integer id;
	private String vm_cardcode;
	private String vm_name;
	private String vm_mobile;
	private String mt_name;
	private String gd_name;
	private String vm_sex;
	private Integer sh_times;
	private Double sh_total_money;
	private Integer vm_times;
	private Double vm_total_money;
	private String sp_name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVm_cardcode() {
		return vm_cardcode;
	}
	public void setVm_cardcode(String vm_cardcode) {
		this.vm_cardcode = vm_cardcode;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getVm_mobile() {
		return vm_mobile;
	}
	public void setVm_mobile(String vm_mobile) {
		this.vm_mobile = vm_mobile;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public String getGd_name() {
		return gd_name;
	}
	public void setGd_name(String gd_name) {
		this.gd_name = gd_name;
	}
	public String getVm_sex() {
		return vm_sex;
	}
	public void setVm_sex(String vm_sex) {
		this.vm_sex = vm_sex;
	}
	public Integer getSh_times() {
		return sh_times;
	}
	public void setSh_times(Integer sh_times) {
		this.sh_times = sh_times;
	}
	public Double getSh_total_money() {
		return sh_total_money;
	}
	public void setSh_total_money(Double sh_total_money) {
		this.sh_total_money = sh_total_money;
	}
	public Integer getVm_times() {
		return vm_times;
	}
	public void setVm_times(Integer vm_times) {
		this.vm_times = vm_times;
	}
	public Double getVm_total_money() {
		return vm_total_money;
	}
	public void setVm_total_money(Double vm_total_money) {
		this.vm_total_money = vm_total_money;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
}
