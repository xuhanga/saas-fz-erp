package zy.entity.vip.member;

import java.io.Serializable;

public class T_Vip_Tag implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer vt_id;
	private String vt_code;
	private String vt_name;
	private String vt_manager_code;
	private String vt_manager_name;
	private String vt_shop_code;
	private String vt_sysdate;
	private Integer companyid;
	private Integer vip_member_used;
	private Integer vip_used_count;
	private String vm_code;
	public Integer getVt_id() {
		return vt_id;
	}
	public void setVt_id(Integer vt_id) {
		this.vt_id = vt_id;
	}
	public String getVt_code() {
		return vt_code;
	}
	public void setVt_code(String vt_code) {
		this.vt_code = vt_code;
	}
	public String getVt_name() {
		return vt_name;
	}
	public void setVt_name(String vt_name) {
		this.vt_name = vt_name;
	}
	public String getVt_manager_code() {
		return vt_manager_code;
	}
	public void setVt_manager_code(String vt_manager_code) {
		this.vt_manager_code = vt_manager_code;
	}
	public String getVt_manager_name() {
		return vt_manager_name;
	}
	public void setVt_manager_name(String vt_manager_name) {
		this.vt_manager_name = vt_manager_name;
	}
	public String getVt_shop_code() {
		return vt_shop_code;
	}
	public void setVt_shop_code(String vt_shop_code) {
		this.vt_shop_code = vt_shop_code;
	}
	public String getVt_sysdate() {
		return vt_sysdate;
	}
	public void setVt_sysdate(String vt_sysdate) {
		this.vt_sysdate = vt_sysdate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getVip_member_used() {
		return vip_member_used;
	}
	public void setVip_member_used(Integer vip_member_used) {
		this.vip_member_used = vip_member_used;
	}
	public Integer getVip_used_count() {
		return vip_used_count;
	}
	public void setVip_used_count(Integer vip_used_count) {
		this.vip_used_count = vip_used_count;
	}
	public String getVm_code() {
		return vm_code;
	}
	public void setVm_code(String vm_code) {
		this.vm_code = vm_code;
	}
}
