package zy.entity.vip.rate;

import java.io.Serializable;

public class T_Vip_Brand_Rate implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer br_id;
	private String br_bd_code;
	private String bd_name;
	private String br_mt_code;
	private String mt_name;
	private Double br_rate;
	private Integer companyid;
	public Integer getBr_id() {
		return br_id;
	}
	public void setBr_id(Integer br_id) {
		this.br_id = br_id;
	}
	public String getBr_bd_code() {
		return br_bd_code;
	}
	public void setBr_bd_code(String br_bd_code) {
		this.br_bd_code = br_bd_code;
	}
	public String getBr_mt_code() {
		return br_mt_code;
	}
	public void setBr_mt_code(String br_mt_code) {
		this.br_mt_code = br_mt_code;
	}
	public Double getBr_rate() {
		return br_rate;
	}
	public void setBr_rate(Double br_rate) {
		this.br_rate = br_rate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}

}
