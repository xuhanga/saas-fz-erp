package zy.entity.vip.rate;

import java.io.Serializable;

public class T_Vip_Type_Rate implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer tr_id;
	private String tr_tp_code;
	private String tp_name;
	private String tr_mt_code;
	private String mt_name;
	private Double tr_rate;
	private Integer companyid;
	public Integer getTr_id() {
		return tr_id;
	}
	public void setTr_id(Integer tr_id) {
		this.tr_id = tr_id;
	}
	public String getTr_tp_code() {
		return tr_tp_code;
	}
	public void setTr_tp_code(String tr_tp_code) {
		this.tr_tp_code = tr_tp_code;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
	public String getTr_mt_code() {
		return tr_mt_code;
	}
	public void setTr_mt_code(String tr_mt_code) {
		this.tr_mt_code = tr_mt_code;
	}
	public String getMt_name() {
		return mt_name;
	}
	public void setMt_name(String mt_name) {
		this.mt_name = mt_name;
	}
	public Double getTr_rate() {
		return tr_rate;
	}
	public void setTr_rate(Double tr_rate) {
		this.tr_rate = tr_rate;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

}
