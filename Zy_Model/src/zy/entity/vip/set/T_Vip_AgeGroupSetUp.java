package zy.entity.vip.set;

import java.io.Serializable;

public class T_Vip_AgeGroupSetUp implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer ags_id;
	private Integer ags_beg_age;
	private Integer ags_end_age;
	private String ags_shop_code;
	private Integer companyid;
	private Integer vip_count;//年龄段会员数量
	private Double sh_sell_money;
	public Integer getAgs_id() {
		return ags_id;
	}
	public void setAgs_id(Integer ags_id) {
		this.ags_id = ags_id;
	}
	public Integer getAgs_beg_age() {
		return ags_beg_age;
	}
	public void setAgs_beg_age(Integer ags_beg_age) {
		this.ags_beg_age = ags_beg_age;
	}
	public Integer getAgs_end_age() {
		return ags_end_age;
	}
	public void setAgs_end_age(Integer ags_end_age) {
		this.ags_end_age = ags_end_age;
	}
	public String getAgs_shop_code() {
		return ags_shop_code;
	}
	public void setAgs_shop_code(String ags_shop_code) {
		this.ags_shop_code = ags_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public Integer getVip_count() {
		return vip_count;
	}
	public void setVip_count(Integer vip_count) {
		this.vip_count = vip_count;
	}
	public Double getSh_sell_money() {
		return sh_sell_money;
	}
	public void setSh_sell_money(Double sh_sell_money) {
		this.sh_sell_money = sh_sell_money;
	}
}
