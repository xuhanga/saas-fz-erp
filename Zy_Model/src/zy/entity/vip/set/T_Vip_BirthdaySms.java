package zy.entity.vip.set;

import java.io.Serializable;

public class T_Vip_BirthdaySms implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer vb_id;
	private String vb_shop_code;
	private String shop_name;
	private Integer vb_state;
	private Integer vb_agowarn_day;
	private String vb_sms_content;
	private Integer companyid;
	public Integer getVb_id() {
		return vb_id;
	}
	public void setVb_id(Integer vb_id) {
		this.vb_id = vb_id;
	}
	public String getVb_shop_code() {
		return vb_shop_code;
	}
	public void setVb_shop_code(String vb_shop_code) {
		this.vb_shop_code = vb_shop_code;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public Integer getVb_state() {
		return vb_state;
	}
	public void setVb_state(Integer vb_state) {
		this.vb_state = vb_state;
	}
	public Integer getVb_agowarn_day() {
		return vb_agowarn_day;
	}
	public void setVb_agowarn_day(Integer vb_agowarn_day) {
		this.vb_agowarn_day = vb_agowarn_day;
	}
	public String getVb_sms_content() {
		return vb_sms_content;
	}
	public void setVb_sms_content(String vb_sms_content) {
		this.vb_sms_content = vb_sms_content;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
