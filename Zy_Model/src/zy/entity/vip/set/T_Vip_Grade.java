package zy.entity.vip.set;

import java.io.Serializable;

public class T_Vip_Grade implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer gd_id;
	private Double gd_lower;
	private Double gd_upper;
	private String gd_color;
	private String gd_name;
	private String gd_remark;
	private String gd_code;
	private Integer companyid;
	public Integer getGd_id() {
		return gd_id;
	}
	public void setGd_id(Integer gd_id) {
		this.gd_id = gd_id;
	}
	public Double getGd_lower() {
		return gd_lower;
	}
	public void setGd_lower(Double gd_lower) {
		this.gd_lower = gd_lower;
	}
	public Double getGd_upper() {
		return gd_upper;
	}
	public void setGd_upper(Double gd_upper) {
		this.gd_upper = gd_upper;
	}
	public String getGd_color() {
		return gd_color;
	}
	public void setGd_color(String gd_color) {
		this.gd_color = gd_color;
	}
	public String getGd_name() {
		return gd_name;
	}
	public void setGd_name(String gd_name) {
		this.gd_name = gd_name;
	}
	public String getGd_remark() {
		return gd_remark;
	}
	public void setGd_remark(String gd_remark) {
		this.gd_remark = gd_remark;
	}
	public String getGd_code() {
		return gd_code;
	}
	public void setGd_code(String gd_code) {
		this.gd_code = gd_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
