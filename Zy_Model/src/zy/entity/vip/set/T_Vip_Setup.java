package zy.entity.vip.set;

import java.io.Serializable;

public class T_Vip_Setup implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer vs_id;
	private Integer vs_loss_day;
	private Integer vs_consume_day;
	private Integer vs_loyalvip_times;
	private Double vs_richvip_money;
	private Integer vs_activevip_day;
	private String vs_shop_code;
	private Integer companyid;
	public Integer getVs_id() {
		return vs_id;
	}
	public void setVs_id(Integer vs_id) {
		this.vs_id = vs_id;
	}
	public Integer getVs_loss_day() {
		return vs_loss_day;
	}
	public void setVs_loss_day(Integer vs_loss_day) {
		this.vs_loss_day = vs_loss_day;
	}
	public Integer getVs_consume_day() {
		return vs_consume_day;
	}
	public void setVs_consume_day(Integer vs_consume_day) {
		this.vs_consume_day = vs_consume_day;
	}
	public Integer getVs_loyalvip_times() {
		return vs_loyalvip_times;
	}
	public void setVs_loyalvip_times(Integer vs_loyalvip_times) {
		this.vs_loyalvip_times = vs_loyalvip_times;
	}
	public Double getVs_richvip_money() {
		return vs_richvip_money;
	}
	public void setVs_richvip_money(Double vs_richvip_money) {
		this.vs_richvip_money = vs_richvip_money;
	}
	public Integer getVs_activevip_day() {
		return vs_activevip_day;
	}
	public void setVs_activevip_day(Integer vs_activevip_day) {
		this.vs_activevip_day = vs_activevip_day;
	}
	public String getVs_shop_code() {
		return vs_shop_code;
	}
	public void setVs_shop_code(String vs_shop_code) {
		this.vs_shop_code = vs_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
