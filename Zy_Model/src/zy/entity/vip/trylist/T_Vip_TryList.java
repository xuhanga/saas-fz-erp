package zy.entity.vip.trylist;

import java.io.Serializable;

public class T_Vip_TryList implements Serializable{
	private static final long serialVersionUID = 8464629058009163997L;
	private Long tr_id;
	private String tr_sysdate;
	private String tr_pd_code;
	private String tr_sub_code;
	private String tr_cr_code;
	private String tr_sz_code;
	private String tr_br_code;
	private Double tr_sell_price;
	private String tr_vm_code;
	private String tr_em_code;
	private String tr_shop_code;
	private Integer companyid;
	private String pd_no;
	private String pd_name;
	private String pd_unit;
	private String pd_season;
	private Integer pd_year;
	private String pd_style;
	private String pd_img_path;
	private String cr_name;
	private String sz_name;
	private String br_name;
	private String bd_name;
	private String tp_name;
	public Long getTr_id() {
		return tr_id;
	}
	public void setTr_id(Long tr_id) {
		this.tr_id = tr_id;
	}
	public String getTr_sysdate() {
		return tr_sysdate;
	}
	public void setTr_sysdate(String tr_sysdate) {
		this.tr_sysdate = tr_sysdate;
	}
	public String getTr_pd_code() {
		return tr_pd_code;
	}
	public void setTr_pd_code(String tr_pd_code) {
		this.tr_pd_code = tr_pd_code;
	}
	public String getTr_sub_code() {
		return tr_sub_code;
	}
	public void setTr_sub_code(String tr_sub_code) {
		this.tr_sub_code = tr_sub_code;
	}
	public String getTr_cr_code() {
		return tr_cr_code;
	}
	public void setTr_cr_code(String tr_cr_code) {
		this.tr_cr_code = tr_cr_code;
	}
	public String getTr_sz_code() {
		return tr_sz_code;
	}
	public void setTr_sz_code(String tr_sz_code) {
		this.tr_sz_code = tr_sz_code;
	}
	public String getTr_br_code() {
		return tr_br_code;
	}
	public void setTr_br_code(String tr_br_code) {
		this.tr_br_code = tr_br_code;
	}
	public Double getTr_sell_price() {
		return tr_sell_price;
	}
	public void setTr_sell_price(Double tr_sell_price) {
		this.tr_sell_price = tr_sell_price;
	}
	public String getTr_vm_code() {
		return tr_vm_code;
	}
	public void setTr_vm_code(String tr_vm_code) {
		this.tr_vm_code = tr_vm_code;
	}
	public String getTr_em_code() {
		return tr_em_code;
	}
	public void setTr_em_code(String tr_em_code) {
		this.tr_em_code = tr_em_code;
	}
	public String getTr_shop_code() {
		return tr_shop_code;
	}
	public void setTr_shop_code(String tr_shop_code) {
		this.tr_shop_code = tr_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getPd_no() {
		return pd_no;
	}
	public void setPd_no(String pd_no) {
		this.pd_no = pd_no;
	}
	public String getPd_name() {
		return pd_name;
	}
	public void setPd_name(String pd_name) {
		this.pd_name = pd_name;
	}
	public String getPd_unit() {
		return pd_unit;
	}
	public void setPd_unit(String pd_unit) {
		this.pd_unit = pd_unit;
	}
	public String getPd_season() {
		return pd_season;
	}
	public void setPd_season(String pd_season) {
		this.pd_season = pd_season;
	}
	public Integer getPd_year() {
		return pd_year;
	}
	public void setPd_year(Integer pd_year) {
		this.pd_year = pd_year;
	}
	public String getPd_style() {
		return pd_style;
	}
	public void setPd_style(String pd_style) {
		this.pd_style = pd_style;
	}
	public String getPd_img_path() {
		return pd_img_path;
	}
	public void setPd_img_path(String pd_img_path) {
		this.pd_img_path = pd_img_path;
	}
	public String getCr_name() {
		return cr_name;
	}
	public void setCr_name(String cr_name) {
		this.cr_name = cr_name;
	}
	public String getSz_name() {
		return sz_name;
	}
	public void setSz_name(String sz_name) {
		this.sz_name = sz_name;
	}
	public String getBr_name() {
		return br_name;
	}
	public void setBr_name(String br_name) {
		this.br_name = br_name;
	}
	public String getBd_name() {
		return bd_name;
	}
	public void setBd_name(String bd_name) {
		this.bd_name = bd_name;
	}
	public String getTp_name() {
		return tp_name;
	}
	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}
}
