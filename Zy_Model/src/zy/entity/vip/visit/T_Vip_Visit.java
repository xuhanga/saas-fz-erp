package zy.entity.vip.visit;

import java.io.Serializable;

public class T_Vip_Visit implements Serializable{
	private static final long serialVersionUID = -5925841660661293379L;
	private Integer vi_id;
	private String vi_vm_code;
	private String vi_vm_cardcode;
	private String vi_vm_name;
	private String vi_vm_mobile;
	private Double vi_consume_money;
	private String vi_consume_date;
	private String vi_visit_date;
	private String vi_manager;
	private String vi_content;
	private Integer vi_satisfaction;
	private String vi_manager_code;
	private String vi_shop_code;
	private Integer vi_us_id;
	private String vi_sysdate;
	private Integer vi_type;
	private Integer vi_is_arrive;
	private String vi_date;
	private String vi_next_date;
	private String vi_remark;
	private Integer vi_way;
	private Integer companyid;
	public Integer getVi_id() {
		return vi_id;
	}
	public void setVi_id(Integer vi_id) {
		this.vi_id = vi_id;
	}
	public String getVi_vm_code() {
		return vi_vm_code;
	}
	public void setVi_vm_code(String vi_vm_code) {
		this.vi_vm_code = vi_vm_code;
	}
	public String getVi_vm_name() {
		return vi_vm_name;
	}
	public void setVi_vm_name(String vi_vm_name) {
		this.vi_vm_name = vi_vm_name;
	}
	public String getVi_vm_mobile() {
		return vi_vm_mobile;
	}
	public void setVi_vm_mobile(String vi_vm_mobile) {
		this.vi_vm_mobile = vi_vm_mobile;
	}
	public Double getVi_consume_money() {
		return vi_consume_money;
	}
	public void setVi_consume_money(Double vi_consume_money) {
		this.vi_consume_money = vi_consume_money;
	}
	public String getVi_consume_date() {
		return vi_consume_date;
	}
	public void setVi_consume_date(String vi_consume_date) {
		this.vi_consume_date = vi_consume_date;
	}
	public String getVi_visit_date() {
		return vi_visit_date;
	}
	public void setVi_visit_date(String vi_visit_date) {
		this.vi_visit_date = vi_visit_date;
	}
	public String getVi_manager() {
		return vi_manager;
	}
	public void setVi_manager(String vi_manager) {
		this.vi_manager = vi_manager;
	}
	public String getVi_content() {
		return vi_content;
	}
	public void setVi_content(String vi_content) {
		this.vi_content = vi_content;
	}
	public Integer getVi_satisfaction() {
		return vi_satisfaction;
	}
	public void setVi_satisfaction(Integer vi_satisfaction) {
		this.vi_satisfaction = vi_satisfaction;
	}
	public String getVi_manager_code() {
		return vi_manager_code;
	}
	public void setVi_manager_code(String vi_manager_code) {
		this.vi_manager_code = vi_manager_code;
	}
	public String getVi_shop_code() {
		return vi_shop_code;
	}
	public void setVi_shop_code(String vi_shop_code) {
		this.vi_shop_code = vi_shop_code;
	}
	public Integer getVi_us_id() {
		return vi_us_id;
	}
	public void setVi_us_id(Integer vi_us_id) {
		this.vi_us_id = vi_us_id;
	}
	public String getVi_sysdate() {
		return vi_sysdate;
	}
	public void setVi_sysdate(String vi_sysdate) {
		this.vi_sysdate = vi_sysdate;
	}
	public Integer getVi_type() {
		return vi_type;
	}
	public void setVi_type(Integer vi_type) {
		this.vi_type = vi_type;
	}
	public Integer getVi_is_arrive() {
		return vi_is_arrive;
	}
	public void setVi_is_arrive(Integer vi_is_arrive) {
		this.vi_is_arrive = vi_is_arrive;
	}
	public String getVi_date() {
		return vi_date;
	}
	public void setVi_date(String vi_date) {
		this.vi_date = vi_date;
	}
	public String getVi_remark() {
		return vi_remark;
	}
	public void setVi_remark(String vi_remark) {
		this.vi_remark = vi_remark;
	}
	public Integer getVi_way() {
		return vi_way;
	}
	public void setVi_way(Integer vi_way) {
		this.vi_way = vi_way;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
	public String getVi_vm_cardcode() {
		return vi_vm_cardcode;
	}
	public void setVi_vm_cardcode(String vi_vm_cardcode) {
		this.vi_vm_cardcode = vi_vm_cardcode;
	}
	public String getVi_next_date() {
		return vi_next_date;
	}
	public void setVi_next_date(String vi_next_date) {
		this.vi_next_date = vi_next_date;
	}
}
