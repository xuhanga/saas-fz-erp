package zy.entity.wx.authorizer;

import java.io.Serializable;

public class T_Wx_Authorizer implements Serializable{
	private static final long serialVersionUID = 833571391858385977L;
	private Integer wa_id;
	private String wa_appid;
	private String wa_access_token;
	private String wa_refresh_token;
	private String wa_expire_time;
	public Integer getWa_id() {
		return wa_id;
	}
	public void setWa_id(Integer wa_id) {
		this.wa_id = wa_id;
	}
	public String getWa_appid() {
		return wa_appid;
	}
	public void setWa_appid(String wa_appid) {
		this.wa_appid = wa_appid;
	}
	public String getWa_access_token() {
		return wa_access_token;
	}
	public void setWa_access_token(String wa_access_token) {
		this.wa_access_token = wa_access_token;
	}
	public String getWa_refresh_token() {
		return wa_refresh_token;
	}
	public void setWa_refresh_token(String wa_refresh_token) {
		this.wa_refresh_token = wa_refresh_token;
	}
	public String getWa_expire_time() {
		return wa_expire_time;
	}
	public void setWa_expire_time(String wa_expire_time) {
		this.wa_expire_time = wa_expire_time;
	}
}
