package zy.entity.wx.product;

import java.io.Serializable;

public class T_Wx_ProductType implements Serializable{
	private static final long serialVersionUID = -2655161408652202823L;
	private Integer pt_id;
	private String pt_code;
	private String pt_upcode;
	private String pt_name;
	private Integer pt_weight;
	private Integer pt_state;
	private String pt_img_path;
	private String pt_shop_code;
	private Integer companyid;
	public Integer getPt_id() {
		return pt_id;
	}
	public void setPt_id(Integer pt_id) {
		this.pt_id = pt_id;
	}
	public String getPt_code() {
		return pt_code;
	}
	public void setPt_code(String pt_code) {
		this.pt_code = pt_code;
	}
	public String getPt_upcode() {
		return pt_upcode;
	}
	public void setPt_upcode(String pt_upcode) {
		this.pt_upcode = pt_upcode;
	}
	public String getPt_name() {
		return pt_name;
	}
	public void setPt_name(String pt_name) {
		this.pt_name = pt_name;
	}
	public Integer getPt_weight() {
		return pt_weight;
	}
	public void setPt_weight(Integer pt_weight) {
		this.pt_weight = pt_weight;
	}
	public Integer getPt_state() {
		return pt_state;
	}
	public void setPt_state(Integer pt_state) {
		this.pt_state = pt_state;
	}
	public String getPt_img_path() {
		return pt_img_path;
	}
	public void setPt_img_path(String pt_img_path) {
		this.pt_img_path = pt_img_path;
	}
	public String getPt_shop_code() {
		return pt_shop_code;
	}
	public void setPt_shop_code(String pt_shop_code) {
		this.pt_shop_code = pt_shop_code;
	}
	public Integer getCompanyid() {
		return companyid;
	}
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}
}
