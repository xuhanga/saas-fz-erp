package zy.entity.wx.user;

import java.io.Serializable;
import java.util.Date;

public class T_Wx_User implements Serializable{
	private static final long serialVersionUID = 3435556958195611281L;
	private Integer wu_id;
	private String wu_code;
	private String wu_mobile;
	private String wu_nickname;
	private String wu_password;
	private String wu_headimage;
	private Integer wu_state;
	private String wu_sysdate;
	private String wu_openid;
	private Double wu_totalmoney;
	private Double wu_usedmoney;
	private Double wu_balance;
	
	private String born;
	private String sex;
	private String wxnumber;
	private String name;
	private String address;
	
	public void setWu_id(Integer wu_id){
		this.wu_id=wu_id;
	}
	public Integer getWu_id(){
		return wu_id;
	}
	public void setWu_code(String wu_code){
		this.wu_code=wu_code;
	}
	public String getWu_code(){
		return wu_code;
	}
	public void setWu_mobile(String wu_mobile){
		this.wu_mobile=wu_mobile;
	}
	public String getWu_mobile(){
		return wu_mobile;
	}
	public void setWu_nickname(String wu_nickname){
		this.wu_nickname=wu_nickname;
	}
	public String getWu_nickname(){
		return wu_nickname;
	}
	public void setWu_password(String wu_password){
		this.wu_password=wu_password;
	}
	public String getWu_password(){
		return wu_password;
	}
	public void setWu_headimage(String wu_headimage){
		this.wu_headimage=wu_headimage;
	}
	public String getWu_headimage(){
		return wu_headimage;
	}
	public void setWu_state(Integer wu_state){
		this.wu_state=wu_state;
	}
	public Integer getWu_state(){
		return wu_state;
	}
	public void setWu_sysdate(String wu_sysdate){
		this.wu_sysdate=wu_sysdate;
	}
	public String getWu_sysdate(){
		return wu_sysdate;
	}
	public void setWu_openid(String wu_openid){
		this.wu_openid=wu_openid;
	}
	public String getWu_openid(){
		return wu_openid;
	}
	public void setWu_totalmoney(Double wu_totalmoney){
		this.wu_totalmoney=wu_totalmoney;
	}
	public Double getWu_totalmoney(){
		return wu_totalmoney;
	}
	public void setWu_usedmoney(Double wu_usedmoney){
		this.wu_usedmoney=wu_usedmoney;
	}
	public Double getWu_usedmoney(){
		return wu_usedmoney;
	}
	public void setWu_balance(Double wu_balance){
		this.wu_balance=wu_balance;
	}
	public Double getWu_balance(){
		return wu_balance;
	}
	public String getBorn() {
		return born;
	}
	public void setBorn(String born) {
		this.born = born;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getWxnumber() {
		return wxnumber;
	}
	public void setWxnumber(String wxnumber) {
		this.wxnumber = wxnumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
