<%@ page contentType="text/html; charset=utf-8" %>
<%@ include file="/common/path.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>智慧数后台管理</title>
<link href="<%=basePath%>resources/css/index.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
var basepath = '${static_path}';
document.write("<scr"+"ipt src=\""+basepath+"/resources/jquery/jquery-1.11.1.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\""+basepath+"/resources/dialog/lhgdialog.js?self=false\"></sc"+"ript>");
</script>
<script type="text/javascript">
$(document).ready(function(e) {
    $("ul li input").focus(function(e) {
		$("ul li input").css("background-position","0 0px");
		$(this).css("background-position","0 -44px");
    });
    $("#btn_submit").on('click',function(){
    	var us_pwd = $.trim(document.getElementById("us_pwd").value);
    	var us_code = $.trim(document.getElementById("us_code").value);
    	if(us_code == ''){
    		$.dialog.tips("请输入用户编号!",1,"32X32/hits.png");
    		$("#us_code").focus();
    		return;
    	}
    	if(us_pwd == ''){
    		$.dialog.tips("请输入密码!",1,"32X32/hits.png");
    		$("#us_pwd").focus();
    		return;
    	}
    	var url = basepath+"login";
    	$.ajax({
    		type:"POST",
    		data:{"us_code":us_code,"us_pwd":us_pwd},
    		url:url,
    		cache:false,
    		dataType:"json",
    		success:function(data){
    			if(undefined != data && data.stat == 200){
    				window.location.replace(basepath+"main");
    			}else{
    				$.dialog.tips("帐户或密码有误!",1,"32X32/hits.png");
    			}
    		}
    	 });
    });
});
function doRemember(obj){
	if(obj.checked){
		document.getElementById("remember").value=1;
	}else{
		document.getElementById("remember").value=0;
	}
}
</script>
</head>
<body>
	<div class="login">
    	<div class="login-w">
        	<div class="tit">
        			<b>商家管理系统</b>
            </div>
            <div class="login-n">
            	<div class="login-tip">智慧匠人后台管理</div>
                <div class="inputin">
                	<label>帐户：</label><input id="us_code" class="us_code" type="text" name="us_code" value=""/>
                </div>
                <div class="inputin" id="div_pwd" style="display:;">
                	<label>密码：</label><input id="us_pwd" class="password" type=password name="us_pwd" value=""/>
                </div>
                <div class="btn">
                	<input type="button" class="btn_submit" id="btn_submit" name="btn_submit" value="登 录" />
                </div>
            </div>
        </div>
    </div>
</body>
</html>
