<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/iconfont/iconfont.css"/>
    <link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/info.css"/>
	<script type="text/javascript">
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
		document.write("<scr"+"ipt src=\"<%=basePath%>resources/dialog/lhgdialog.js?self=false&skin=mac\"></sc"+"ript>");
	</script>
    <script>
		function addTags(obj,name){
			window.parent.addTags(obj,name);
		}
	</script>	
</head>
<body id="main_content">
<div class="main-menu">
	<div class="basebock">
    	<div class="icon-menu">
        	<div class="icons">
				<ul>
					<c:if test="${sessionScope.menu == null}">
						<c:redirect url="${basePath}/toIndex"></c:redirect>
					</c:if>
					<c:forEach var="menu" items="${sessionScope.menu}" varStatus="as">
        				<c:if test="${menu.mn_upcode == param.mn_code && !empty menu.mn_icon }">
							<li>
								<a class="last_menu" dataType="iframe" dataLink="${menu.mn_url}" id="${menu.mn_code}"
									jerichotabindex="${menu.mn_code}" onclick="javascript:addTags(this,'${menu.mn_name}');">
									<i class="iconfont ${menu.mn_style}">${menu.mn_icon}</i>
									<span class="last_menu">${menu.mn_name}</span>
								</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
            </div>
        </div>
        <div class="list-menu">
        	<%-- <h2>功能列表</h2>
        	<ul>
        		<c:set var="index" value="0"></c:set>
       			<c:forEach var="menu" items="${sessionScope.menu}" varStatus="as">
       				<c:if test="${menu.mn_upcode == param.mn_code && empty menu.mn_icon}">
        				<li>
        					<span onclick="javascript:addTags(this,'${menu.mn_name}');" 
        						jerichotabindex="${menu.mn_code}" id="${menu.mn_code}" datalink="${menu.mn_url}" 
        						datatype="iframe" class="last_menu">${menu.mn_name}</span>
        				</li>
        				<c:set var="index" value="${1+index}"></c:set>
        			</c:if>
        		</c:forEach>
        		<c:if test="${index > 0 && index % 2 != 0}">
        			<li><span class="last_menu">&nbsp;</span></li>
        		</c:if>
        	</ul> --%>
        </div>       
    </div>
</div>
</body>
</html>

