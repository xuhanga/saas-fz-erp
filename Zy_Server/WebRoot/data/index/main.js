var path = parent.CONFIG.BASEPATH;
var jericho = {
    showLoader: function() {
        $('#divMainLoader').css('display', 'none');
    },
    removeLoader: function() {
        $('#divMainLoader').css('display', 'none');
    },
    buildTree: function() {
        $('span.list').click(function() {
        	$(".tab_selected").each(function(){
        		$(this).attr("class","jericho_tabs tab_unselect");
        	});
        	$(".curholder").each(function(){
        		$(this).css({'display':'none'});
        	});
        	$("#jerichotab_0").attr("class","jericho_tabs tab_selected");
        	$("#jerichotabholder_0").attr("class","curholder");
        	$("#jerichotabholder_0").css({'display':'block'});
        	var tit=$(this).children("b").text();
        	var info='<div class="tab_left"><div class="tab_text" title="'+tit+'" style="">'+tit+'</div></div>';
        	$("#jerichotab_0").html(info);
        	var dataLink=$(this).attr("dataLink");
        	dataLink = path+dataLink;
        	$("#jerichotabiframe_0").attr("src",dataLink);
        });
        $(".last_menu").click(function(){
        	if(tabHash.length >= 9){alert("菜单打开太多!!!");return;}
        	$.fn.jerichoTab.addTab({
				tabId:$(this).attr("id"),
                tabFirer: $(this),
                title: $(this).text(),
                closeable: true,
                iconImg: $(this).attr('iconImg'),
                data: {
                    dataType: $(this).attr('dataType'),
                    dataLink: $("#project_name").val()+$(this).attr('dataLink')
                }
            }).showLoader().loadData();
		});
    },
    buildTabpanel: function() {
    	var url = "home";
    	var title = '首页';
    	var dataLink = path+url;
        $.fn.initJerichoTab({
            renderTo: '.divRight',
            uniqueId: 'baseInfo',
            contentCss: { 'height': $('.divRight').height()-18,'margin':0},
            tabs: [{
                title: title,
                closeable: false,
                iconImg: '',
                data: { dataType: 'iframe', dataLink: dataLink }
            }],
            activeTabIndex: 0,
            loadOnce: true
            });
        }
    };
$().ready(function() {
jericho.showLoader();
jericho.buildTree();
jericho.buildTabpanel();
jericho.removeLoader();
});
function addTags(obj,name){
	if(tabHash.length >= 9){alert("菜单打开太多!!!");return;}
	var dataType = $(obj).attr("dataType");
	var dataLink = $(obj).attr("dataLink");
	var id = $(obj).attr("id");
	dataLink = path+dataLink;
	$.fn.jerichoTab.addTab({
				tabId:id,
                tabFirer: obj,
                title: name,
                closeable: true,
                iconImg: '',
                data: {
                    dataType: dataType,
                    dataLink: dataLink
                }
    }).showLoader().loadData();
}
