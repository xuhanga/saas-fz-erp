var config = parent.parent.parent.CONFIG,system=parent.parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var vadiUtil={
		vadiNumber:function(obj) {
			var data = $(obj).val();
			if (isNaN(data)) {
				W.Public.tips({type: 1, content : "请输入整数"});
				$(obj).val("").focus();
				return;
			}
		}
	};
var handle = {
	delayShop:function(){
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"shop/delayShop",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					setTimeout("api.close()",300);
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	}	
};
var THISPAGE = {
	init:function(){
		this.initEvent();
	},
	initEvent:function(){
		$("#btn-save").on('click',function(){
			handle.delayShop();
		});
	}
	
} 
THISPAGE.init();