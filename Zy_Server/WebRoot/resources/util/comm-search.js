$(function(){
	$('.ui-btn-menu .menu-btn').on('mouseenter.menuEvent',function(e){
		e.preventDefault();
		doStop($(this));
	});
	$('.ui-btn-menu .menu-btn').on('mouseout',function(e){
		e.preventDefault();
		clearTimeout(timer);
	});
	$('.ui-btn-menu .menu-btn').on('mouseover',function(e){
		e.preventDefault();
		doStop($(this));
	});
	var timer = null;
	var doStop = function(_this,_parent){
		clearTimeout(timer);
	    timer = setTimeout(function(){
	    	if(_this.hasClass("ui-btn-dis")){
				return false;
			}
	    	var _parent = _this.parent();
	    	_parent.addClass('ui-btn-menu-cur');
	    	$(_this).blur();
	    }, 300);
	}
	$(document).on('click.menu',function(e){
		var target = e.target || e.srcElement;
		$('.ui-btn-menu').each(function(){
			var menu = $(this);
			if($(target).closest(menu).length == 0 && $('.con',menu).is(':visible')){
				menu.removeClass('ui-btn-menu-cur');
			}
		})
	});
});