package zy.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import zy.entity.JsonBean;
import zy.entity.common.city.Common_City;
import zy.entity.sys.user.T_Sys_User;
import zy.service.common.city.CityService;
import zy.util.CommonUtil;

public class BaseController {
	protected final int SUCCESS = 200;
	protected final int FAIL = 500;
	protected int pagesize = 10;
	
	@Resource
	private CityService cityService; 
	
	@RequestMapping(value = "queryProvince", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryProvince(HttpServletRequest request,HttpSession session){
		List<Common_City> list = cityService.queryProvince();
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	@RequestMapping(value = "queryCity", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryCity(Integer cy_id,HttpServletRequest request,HttpSession session){
		List<Common_City> list = cityService.queryCity(cy_id);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	@RequestMapping(value = "queryTown", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryTown(Integer cy_id,HttpServletRequest request,HttpSession session){
		List<Common_City> list = cityService.queryTown(cy_id);
		return ajaxSuccess(list, "查询成功, 共" + list.size() + "条数据");
	}
	public Object ajaxSuccess() {
		return ajaxSuccessMessage("");
	}

	public Object ajaxSuccess(Object data) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(data);
		jsonBean.setMessage("操作成功");
		return jsonBean;
	}

	public Object ajaxSuccess(Object data, String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		jsonBean.setData(data);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作成功");
		}
		return jsonBean;
	}

	public Object ajaxSuccessMessage(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(SUCCESS);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作成功");
		}
		return jsonBean;
	}

	public Object ajaxFailMessage(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败");
		}
		return jsonBean;
	}

	public Object ajaxFail() {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setMessage("");
		return jsonBean;
	}

	public Object ajaxFail(String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败");
		}
		return jsonBean;
	}

	public Object ajaxFail(Object data, String message) {
		JsonBean jsonBean = new JsonBean();
		jsonBean.setStat(FAIL);
		jsonBean.setData(data);
		if (message != null && !message.equals("")) {
			jsonBean.setMessage(message);
		} else {
			jsonBean.setMessage("操作失败,请联系管理员!");
		}
		return jsonBean;
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public Object illegalArgumentException(WebRequest request, IllegalArgumentException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail("非法参数:" + exception.getMessage());
		} else {
			return ajaxFail();
		}
	}

	@ExceptionHandler(EmptyResultDataAccessException.class)
	@ResponseBody
	public Object emptyResultDataAccessException(WebRequest request, EmptyResultDataAccessException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail("没有查询到数据:" + exception.getMessage());
		} else {
			return ajaxFail();
		}
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public Object runtimeException(WebRequest request, RuntimeException exception) {
		if (exception.getMessage() != null && !exception.getMessage().equals("")) {
			return ajaxFail(exception.getMessage());
		} else {
			return ajaxFail("");
		}
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Object exception(WebRequest request, Exception exception) {
		return ajaxFail("操作失败!");
	}
	
	public T_Sys_User getUser(HttpSession session) {
		T_Sys_User user = (T_Sys_User) session.getAttribute(CommonUtil.KEY_USER);
		return user;
	}
}
