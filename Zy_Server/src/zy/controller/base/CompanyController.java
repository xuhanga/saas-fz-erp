package zy.controller.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.company.T_Sys_Company;
import zy.entity.sys.company.T_Zhjr_Unit;
import zy.entity.sys.user.T_Sys_User;
import zy.form.PageForm;
import zy.service.base.company.CompanyService;
import zy.util.CommonUtil;
import zy.util.IDUtil;

@Controller
@RequestMapping("company")
public class CompanyController extends BaseController{
	@Resource
	private CompanyService companyService;
	@RequestMapping("to_list")
	public String to_company_list() {
		return "company/list";
	}
	@RequestMapping("to_add")
	public String to_add(Model model) {
		String code = buildCode(6);
		model.addAttribute("co_code", code);
		return "company/add";
	}
	private String buildCode(int length){
		String code = IDUtil.buildCode(length);
		Integer id = companyService.queryByCode(code);
		if(null != id && id > 0){
			code = buildCode(length);
		}
		return code;
	}
	@RequestMapping(value = "page", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object page(PageForm pageForm,
			HttpServletRequest request,HttpSession session) {
		T_Sys_User user = getUser(session);
        Map<String,Object> param = new HashMap<String, Object>();
        param.put(CommonUtil.PAGESIZE, pageForm.getRows());
        param.put(CommonUtil.PAGEINDEX, pageForm.getPage());
        param.put(CommonUtil.SIDX, pageForm.getSidx());
        param.put(CommonUtil.SORD, pageForm.getSord());
        param.put(CommonUtil.KEY_USER, user);
		return ajaxSuccess(companyService.company_list(param));
	}
	@RequestMapping(value = "save", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object save(T_Sys_Company company) {
		companyService.save(company);
		return ajaxSuccess();
	}
	@RequestMapping(value = "queryUnit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryUnit(HttpServletRequest request,HttpSession session){
		List<T_Zhjr_Unit> list = companyService.queryUnit();
		return ajaxSuccess(list);
	}
}
