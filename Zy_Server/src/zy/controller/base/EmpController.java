package zy.controller.base;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.dto.base.emp.EmpLoginDto;
import zy.service.base.emp.EmpService;
import zy.service.report.EmpReportService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/base/emp")
public class EmpController extends BaseController{
	@Resource
	private EmpService empService;
	@Resource
	private EmpReportService empReportService;
	
	@RequestMapping(value = "login", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		String em_code = StringUtil.trimString(params.get("em_code"));
		String em_pass = StringUtil.trimString(params.get("em_pass"));
		String co_code = StringUtil.trimString(params.get("co_code"));
		EmpLoginDto empLoginDto = empService.login(em_code,em_pass,co_code);
		if(empLoginDto != null){
			return ajaxSuccess(empLoginDto);
		}
		return ajaxFail("账号或密码错误");
	}
	
	@RequestMapping(value = "updatePwd", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updatePwd(@RequestBody Map<String, Object> params) {
		Integer em_id = Integer.parseInt(StringUtil.trimString(params.get("em_id")));
		String old_pass = StringUtil.trimString(params.get("old_pass"));
		String new_pass = StringUtil.trimString(params.get("new_pass"));
		empService.updatePwd(em_id, new_pass, old_pass);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "loadEmpSellMoney", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadEmpSellMoney(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(empReportService.loadEmpSellMoney(params));
	}
	
	@RequestMapping(value = "loadPerformanceByEmp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadPerformanceByEmp(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(empReportService.loadPerformanceByEmp(params));
	}
	
	@RequestMapping(value = "listEmpSort", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listEmpSort(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(empReportService.listEmpSort(params));
	}
	
}
