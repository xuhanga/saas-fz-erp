package zy.controller.base;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.base.product.ProductService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/base/product")
public class ProductController extends BaseController{
	
	@Resource
	private ProductService productService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(productService.list_server(params));
	}
	
	@RequestMapping(value = "loadByBarcode", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadByBarcode(@RequestBody Map<String, Object> params) {
		String barcode = StringUtil.trimString(params.get("barcode"));
		Integer companyid = Integer.parseInt(StringUtil.trimString(params.get("companyid")));
		return ajaxSuccess(productService.loadByBarcode(barcode, companyid));
	}
	
}
