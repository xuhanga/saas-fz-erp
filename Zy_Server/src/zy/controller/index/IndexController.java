package zy.controller.index;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sys.user.T_Sys_User;
import zy.interceptor.CookieHandler;
import zy.interceptor.SessionHandler;
import zy.service.IndexService;
import zy.util.CommonUtil;
import zy.util.DateUtil;
import zy.util.MD5;
@Controller
public class IndexController extends BaseController{
	@Resource
	private IndexService indexService;
	
	@RequestMapping(value = "to_index", method = RequestMethod.GET)
	public String to_index() {
		return "to_index";
	}
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	/**
	 * 管理员主页面
	 * */
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String main(HttpServletRequest request) {
		request.setAttribute("today", DateUtil.getYearMonthDate());
		return "index/main";
	}
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public String info() {
		return "index/info";
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		return "index/home";
	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody Object login(HttpServletResponse response,HttpServletRequest request,HttpSession session) {
		String us_code = request.getParameter("us_code");
		String us_pwd = request.getParameter("us_pwd");
		String remember = request.getParameter("remember");
		Map<String,Object> paramMap = new HashMap<String,Object>(3);
		paramMap.put("us_code", us_code);
		paramMap.put("us_pass", MD5.encryptMd5(us_pwd));
		indexService.login(paramMap);
		T_Sys_User user = (T_Sys_User)paramMap.get(CommonUtil.KEY_USER);
		session.setAttribute(CommonUtil.KEY_USER, user);
		if(null != user){
			session.setAttribute(CommonUtil.KEY_MENU, paramMap.get(CommonUtil.KEY_MENU));
			session.setAttribute(CommonUtil.KEY_MENULIMIT, paramMap.get(CommonUtil.KEY_MENULIMIT));
			session.setAttribute(CommonUtil.KEY_SYSSET, paramMap.get(CommonUtil.KEY_SYSSET));
			SessionHandler.kickUser(user.getCompanyid()+""+user.getUs_id(), session);
			paramMap.remove(CommonUtil.KEY_MENU);
			paramMap.remove(CommonUtil.KEY_MENULIMIT);
			paramMap.remove(CommonUtil.KEY_SYSSET);
			paramMap.remove(CommonUtil.KEY_USER);
		}
		Map<String,String> cookie = CookieHandler.getCookies(request);
		cookie.put("us_code",us_code);
		cookie.put("us_remember",remember);
		CookieHandler.createCookie(response,paramMap,10);
		return ajaxSuccess(user);
	}
	@RequestMapping(value = "/logout", method = {RequestMethod.GET,RequestMethod.POST})
	public String logout(HttpSession session) {
		T_Sys_User user = (T_Sys_User)session.getAttribute(CommonUtil.KEY_USER);
		if(user != null){
			SessionHandler.delUserFromSession(user.getCompanyid()+""+user.getUs_id());
		}
		Enumeration<?> keys = session.getAttributeNames();
		String key;
		for (;keys.hasMoreElements();){
			key = (String)keys.nextElement();
			session.removeAttribute(key);
		}
		if(session != null)session.invalidate();
		return "redirect:/index";
	}
}
