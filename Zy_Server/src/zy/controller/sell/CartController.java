package zy.controller.sell;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.sell.cart.T_Sell_Cart;
import zy.entity.sell.cart.T_Sell_CartList;
import zy.service.sell.cart.CartService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/sell/cart")
public class CartController extends BaseController{
	@Resource
	private CartService cartService;
	
	@RequestMapping(value = "loadCart", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadCart(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(cartService.loadCart(params));
	}
	
	@RequestMapping(value = "listCartList", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listCartList(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(cartService.listCartList(params));
	}
	
	@RequestMapping(value = "updateAmount", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object updateAmount(@RequestBody Map<String, Object> params) {
		Integer scl_amount = Integer.parseInt(StringUtil.trimString(params.get("scl_amount")));
		Long scl_id = Long.parseLong(StringUtil.trimString(params.get("scl_id")));
		cartService.updateAmount(scl_amount, scl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "saveCartList", method = RequestMethod.POST)
	@ResponseBody
	public Object saveCartList(@RequestBody Map<String, Object> params) {
		T_Sell_CartList cartList = JSON.parseObject(StringUtil.trimString(params.get("cartList")), T_Sell_CartList.class);
		cartService.saveCartList(cartList);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "delCartList", method = RequestMethod.POST)
	@ResponseBody
	public Object delCartList(@RequestBody Map<String, Object> params) {
		Long scl_id = Long.parseLong(StringUtil.trimString(params.get("scl_id")));
		cartService.delCartList(scl_id);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "clearCartList", method = RequestMethod.POST)
	@ResponseBody
	public Object clearCartList(@RequestBody Map<String, Object> params) {
		String em_code = StringUtil.trimString(params.get("em_code"));
		String shop_code = StringUtil.trimString(params.get("shop_code"));
		Integer companyid = Integer.parseInt(StringUtil.trimString(params.get("companyid")));
		cartService.clearCartList(em_code, shop_code, companyid);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "updateCartListVip", method = RequestMethod.POST)
	@ResponseBody
	public Object updateCartListVip(@RequestBody Map<String, Object> params) {
		String vm_code = StringUtil.trimString(params.get("vm_code"));
		String em_code = StringUtil.trimString(params.get("em_code"));
		String shop_code = StringUtil.trimString(params.get("shop_code"));
		Integer companyid = Integer.parseInt(StringUtil.trimString(params.get("companyid")));
		cartService.updateCartListVip(vm_code,em_code, shop_code, companyid);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "saveCart", method = RequestMethod.POST)
	@ResponseBody
	public Object saveCart(@RequestBody Map<String, Object> params) {
		T_Sell_Cart cart = JSON.parseObject(StringUtil.trimString(params.get("cart")), T_Sell_Cart.class);
		cartService.saveCart(cart);
		return ajaxSuccess();
	}
	
}
