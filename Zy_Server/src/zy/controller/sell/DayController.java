package zy.controller.sell;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.sell.day.T_Sell_Day;
import zy.service.sell.day.DayService;
import zy.util.DateUtil;

@Controller
@RequestMapping("api/sell/day")
public class DayController extends BaseController{
	
	@Resource
	private DayService dayService;
	
	@RequestMapping(value = "queryDay", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object queryDay(@RequestBody Map<String, Object> params) {
		params.put("today", DateUtil.getYearMonthDate());
		T_Sell_Day sellDay = dayService.queryDay(params);
		sellDay.setDa_receive(dayService.countreceive(params));
		return ajaxSuccess(sellDay);
	}
	
	@RequestMapping(value = "comereceive", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object comereceive(@RequestBody Map<String, Object> params) {
		params.put("today", DateUtil.getYearMonthDate());
		dayService.comereceive(params);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "doTry", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object doTry(@RequestBody Map<String, Object> params) {
		params.put("today", DateUtil.getYearMonthDate());
		dayService.doTry(params);
		return ajaxSuccess();
	}

}
