package zy.controller.sell;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.sell.ecoupon.ECouponService;

@Controller
@RequestMapping("api/sell/ecoupon")
public class ECouponController extends BaseController{
	@Resource
	private ECouponService eCouponService;
	
	@RequestMapping(value = "listEcoupon4Push", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listEcoupon4Push(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(eCouponService.listEcoupon4Push(params));
	}
	
	
}
