package zy.controller.shop;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.shop.sale.T_Shop_Sale;
import zy.service.shop.sale.SaleService;
@Controller
@RequestMapping("api/shop/sale")
public class SaleController extends BaseController {
	@Resource
	private SaleService saleService;
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		List<T_Shop_Sale> list = saleService.list(params);
		return ajaxSuccess(list);
	}
}
