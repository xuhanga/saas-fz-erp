package zy.controller.vip;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.service.vip.membertype.MemberTypeService;

@Controller
@RequestMapping("api/vip/membertype")
public class MemberTypeController extends BaseController{
	@Resource
	private MemberTypeService memberTypeService;
	
	
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object list(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(memberTypeService.list(params));
	}
}
