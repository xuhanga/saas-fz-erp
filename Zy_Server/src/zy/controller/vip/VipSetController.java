package zy.controller.vip;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zy.controller.BaseController;
import zy.entity.vip.set.T_Vip_ReturnSetUp;
import zy.service.vip.set.VipSetService;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/vip/set")
public class VipSetController extends BaseController{
	@Resource
	private VipSetService vipSetService;
	
	@RequestMapping(value = "loadReturnSetUp", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object loadReturnSetUp(@RequestBody Map<String, Object> params) {
		//根据店铺查询回访设置，如果本店铺无数据，则查询上级店铺回访设置
		String shop_code = StringUtil.trimString(params.get("shop_code"));
		String shop_upcode = StringUtil.trimString(params.get("shop_upcode"));
		Integer companyid = Integer.parseInt(StringUtil.trimString(params.get("companyid")));
		List<T_Vip_ReturnSetUp> setUps = vipSetService.loadReturnSetUp(shop_code, companyid);
		if (setUps != null && setUps.size() > 0) {
			return ajaxSuccess(setUps);
		}
		return ajaxSuccess(vipSetService.loadReturnSetUp(shop_upcode, companyid));
	}
	
	
}
