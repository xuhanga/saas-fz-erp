package zy.controller.vip;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import zy.controller.BaseController;
import zy.entity.sell.ecoupon.T_Sell_Ecoupon_User;
import zy.entity.vip.visit.T_Vip_Visit;
import zy.service.vip.member.MemberService;
import zy.service.vip.visit.VisitService;
import zy.util.DateUtil;
import zy.util.StringUtil;

@Controller
@RequestMapping("api/vip/visit")
public class VisitController extends BaseController{
	
	@Resource
	private VisitService visitService;
	
	@Resource
	private MemberService memberService;
	
	@RequestMapping(value = "listSellVisit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listSellVisit(@RequestBody Map<String, Object> params) {
		Integer setDay = Integer.parseInt(StringUtil.trimString(params.get("setDay")));
		String sell_date = DateUtil.getDateAddDays(-setDay);
		params.put("sell_date", sell_date);
		params.put("visit", "0");
		return ajaxSuccess(visitService.list(params));
	}
	
	@RequestMapping(value = "listBirthdayVisit", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public Object listBirthdayVisit(@RequestBody Map<String, Object> params) {
		return ajaxSuccess(visitService.list_birthday_visit(params));
	}
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public Object save(@RequestBody Map<String, Object> params) {
		T_Vip_Visit visit = JSON.parseObject(StringUtil.trimString(params.get("visit")), T_Vip_Visit.class);
		visit.setVi_sysdate(DateUtil.getCurrentTime());
		memberService.save_visit(visit);
		return ajaxSuccess();
	}
	
	@RequestMapping(value = "save_visit_ecoupon", method = RequestMethod.POST)
	@ResponseBody
	public Object save_visit_ecoupon(@RequestBody Map<String, Object> params) {
		T_Vip_Visit visit = JSON.parseObject(StringUtil.trimString(params.get("visit")), T_Vip_Visit.class);
		T_Sell_Ecoupon_User ecouponUser = JSON.parseObject(StringUtil.trimString(params.get("ecouponUser")), T_Sell_Ecoupon_User.class);
		visit.setVi_sysdate(DateUtil.getCurrentTime());
		visitService.save_visit_ecoupon(visit, ecouponUser);
		return ajaxSuccess();
	}
	
	
}
