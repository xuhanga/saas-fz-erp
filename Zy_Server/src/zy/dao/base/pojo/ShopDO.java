package zy.dao.base.pojo;

import java.io.Serializable;

public class ShopDO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int sp_id;
	private String sp_code;
	private String sp_name;
	private String sp_end;
	private String sp_state;
	private String co_code;
	private String co_name;
	private String co_man;
	public int getSp_id() {
		return sp_id;
	}
	public void setSp_id(int sp_id) {
		this.sp_id = sp_id;
	}
	
	public String getSp_code() {
		return sp_code;
	}
	public void setSp_code(String sp_code) {
		this.sp_code = sp_code;
	}
	public String getSp_name() {
		return sp_name;
	}
	public void setSp_name(String sp_name) {
		this.sp_name = sp_name;
	}
	public String getSp_end() {
		return sp_end;
	}
	public void setSp_end(String sp_end) {
		this.sp_end = sp_end;
	}
	public String getSp_state() {
		return sp_state;
	}
	public void setSp_state(String sp_state) {
		this.sp_state = sp_state;
	}
	public String getCo_code() {
		return co_code;
	}
	public void setCo_code(String co_code) {
		this.co_code = co_code;
	}
	public String getCo_name() {
		return co_name;
	}
	public void setCo_name(String co_name) {
		this.co_name = co_name;
	}
	public String getCo_man() {
		return co_man;
	}
	public void setCo_man(String co_man) {
		this.co_man = co_man;
	}
	
}
