package zy.interceptor;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

public class SessionHandler {
	private static final ConcurrentHashMap<String, HttpSession> sessionMap = new ConcurrentHashMap<String, HttpSession>();

	public static synchronized void kickUser(String key, HttpSession session) {
		HttpSession oldSession = sessionMap.get(key);
		if (oldSession != null) {
			if (!oldSession.getId().equals(session.getId())) {
				try {
					oldSession.invalidate();
				} catch (Exception ex) {
				}
				sessionMap.put(key, session);
				return;
			}
		}
		sessionMap.put(key, session);
	}

	public static void delUserFromSession(String key) {
		if(sessionMap != null){
			if (sessionMap.containsKey(key))
				sessionMap.remove(key);
		}
	}
}
