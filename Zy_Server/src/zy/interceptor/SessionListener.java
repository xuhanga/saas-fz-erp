package zy.interceptor;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import zy.entity.sys.user.T_Sys_User;
import zy.util.CommonUtil;

public class SessionListener implements HttpSessionListener {
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession session = sessionEvent.getSession();
		if(session != null){
			T_Sys_User login = (T_Sys_User)session.getAttribute(CommonUtil.KEY_USER);
			if(login != null){
				SessionHandler.delUserFromSession(login.getCompanyid()+""+login.getUs_id());
			}
		}
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
	}
}
