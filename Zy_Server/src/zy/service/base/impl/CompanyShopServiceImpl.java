package zy.service.base.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import zy.dao.base.CompanyShopDAO;
import zy.dao.base.pojo.ShopDO;
import zy.entity.PageData;
import zy.entity.PageInfo;
import zy.service.base.CompanyShopService;
import zy.util.CommonUtil;
import zy.util.StringUtil;
@Service
public class CompanyShopServiceImpl implements CompanyShopService{
	@Autowired
	private CompanyShopDAO companyShopDAO;
	@Override
	public PageData<ShopDO> listShop(Map<String, Object> paramMap) {
		Object pageSize = paramMap.get(CommonUtil.PAGESIZE);
		Object pageIndex = paramMap.get(CommonUtil.PAGEINDEX);
		Integer _pageSize = (Integer)pageSize;
		Integer _pageIndex = (Integer)pageIndex;
		
		Integer totalCount = companyShopDAO.countShop(paramMap);
		PageInfo pageInfo = new PageInfo(totalCount, _pageSize, _pageIndex);
		paramMap.put(CommonUtil.START, (_pageIndex-1)*_pageSize);
		paramMap.put(CommonUtil.END, _pageSize);
		
		List<ShopDO> list = companyShopDAO.listShop(paramMap);
		PageData<ShopDO> pageData = new PageData<ShopDO>();
		pageData.setPageInfo(pageInfo);
		pageData.setList(list);
		return pageData;
	}
	@Transactional
	@Override
	public void delayShop(Map<String, Object> paramMap) {
		Object sp_id = paramMap.get("sp_id");
		if(StringUtil.isEmpty(sp_id)){
			return;
		}
		Object months = paramMap.get("months");
		if(StringUtil.isEmpty(months)){
			return;
		}
		companyShopDAO.delayShop(paramMap);
	}

}
