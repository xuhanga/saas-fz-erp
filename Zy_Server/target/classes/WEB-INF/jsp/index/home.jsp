<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>首页</title>
<meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
<link type="text/css" rel="stylesheet" href="<%=basePath%>resources/css/home.css"/>
<script type="text/javascript">
document.write("<scr"+"ipt src=\"<%=basePath%>resources/jquery/jquery.min.js\"></sc"+"ript>");
document.write("<scr"+"ipt src=\"<%=basePath%>resources/chart/highcharts.4.0.3.js\"></sc"+"ript>");
</script>
</head>
<body>
	<table class="m_tables" width="100%" border="0" cellspacing="15" cellpadding="0">
  <tr>
    <td rowspan="2">
        <div class="part1">
            <div class="part-title">
                <b>数据统计(上月/本月)</b>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="">
              <tr class="color_g">
                <td align="right" style="width:80px;">进店人数：</td>
                <td>350/300</td>
              </tr>
            </table>
        </div>
    </td>
    <td>
        <div class="part2">
            <div class="part-title">
                <b>月度零售</b>
            </div>
            <div class="part-list" id="container" style="min-width:580px;height:204px">
            </div>
        </div>    
    </td>
    <td rowspan="2">
        <div class="part3">
            <div class="part-title">
                <b>日志信息</b>
            </div>
            <div class="part-list" >
                <ul>
                    <li><a href="">1.2017.07.01 智慧数平台正式上线</a></li>
                </ul>
            </div>
        </div>    
    </td>
  </tr>
</table>
<script src="<%=basePath%>data/index/home.js"></script>
</body>
</html>
