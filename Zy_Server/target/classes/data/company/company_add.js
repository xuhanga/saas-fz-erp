var config = parent.parent.parent.CONFIG,system=parent.parent.parent.SYSTEM;
var api = frameElement.api, W = api.opener;
var callback = api.data.callback,oper = api.data.oper;
var allAreaData = null;
var handle = {
	save:function(){
		var co_name = $("#co_name").val().trim();
		var co_type = $("#co_type").val();		 	
		var co_shops = $("#co_shops").val();
		var co_tel =  $("#co_tel").val().trim();
		var co_man = $("#co_man").val().trim();
	    var co_province = $("#co_province").val();
	    var co_city = $("#co_city").val();
	    var co_addr = $("#co_addr").val().trim();  
	    if(co_name == ''){
	    	W.Public.tips({type: 1, content : "名称不能为空"});
	        $("#co_name").select().focus();
	     	setTimeout("api.zindex()",400);
	        return;
	    }else{
	    	$("#co_name").val(co_name);//防止空格
	    }
		//验证联系人
	    if(co_man == null || co_man == ''){
	    	$("#co_man").focus();
		    W.Public.tips({type: 1, content : "联系人不能为空"});
	        setTimeout("api.zindex()",400);
	        return false;
	    }
		//验证店铺电话
	    if(co_tel == null || co_tel == ''){
	        $("#co_tel").focus();
	        W.Public.tips({type: 1, content : "电话不能为空"});
	        setTimeout("api.zindex()",400);
	        return false;
	    }
	    //验证折扣率, 不能为空
	    if(co_shops == null || co_shops == ''){
	        $("#co_shops").focus();
	        W.Public.tips({type: 1, content : "门店数不能为空"});
	        setTimeout("api.zindex()",400);
	        return;
	    }
	    if(co_province == "" || co_city=="" || co_town==""  || co_addr==""){
	    	   W.Public.tips({type: 1, content : "店铺地址不完整"});
	    	   setTimeout("api.zindex()",400);
	    	   return;
	    }
	    if(co_type == ''){
		   W.Public.tips({type: 1, content : "类型不能为空"});
		   setTimeout("api.zindex()",400);
    	   return; 
	    }  
		$("#btn-save").attr("disabled",true);
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"company/save",
			data:$('#form1').serialize(),
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.stat == 200){
					W.isRefresh = true;
					setTimeout("api.close()",300);
					handle.loadData();
				}else{
					W.Public.tips({type: 1, content : data.message});
				}
				$("#btn-save").attr("disabled",false);
			}
		});
	},
	loadData:function(){
		if(callback && typeof callback == 'function'){
			callback(null,oper,window);
		}
	}
};
var vadiUtil={
	vadiDouble:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入数字"});
			$(obj).val("").focus();
			return;
		}
		if (data<0 || data>1) {
			W.Public.tips({type: 1, content : "数字必须0-1之间"});
			$(obj).val("").focus();
			return;
		}
	},
	vadiNumber:function(obj) {
		var data = $(obj).val();
		if (isNaN(data)) {
			W.Public.tips({type: 1, content : "请输入整数"});
			$(obj).val("").focus();
			return;
		}
	}
};

var THISPAGE = {
	init:function (){
		this.initParam();
		this.initEvent();
		this.queryProvince();
		this.queryUnit();
	},
	initParam:function(){
		$("#co_name").focus();
		tdata = [{id:"0",name:"体验商户"},{id:"1",name:"正式商户"}];
		typeCombo = $('#spanType').combo({
			data:tdata,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#co_type").val(data.id);
				}
			}
		}).getCombo();
		tdata = [{id:"1",name:"总代版"},{id:"2",name:"连锁版"},{id:"3",name:"基础版"},{id:"4",name:"大众版"}];
		verCombo = $('#spanVer').combo({
			data:tdata,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#co_ver").val(data.id);
				}
			}
		}).getCombo();
		pdata = [{id:"0",name:"否"},{id:"1",name:"是"}];
		mainCombo = $('#spanMain').combo({
			data:pdata,
	        value: 'id',
	        text: 'name',
	        width : 158,
			height : 30,
			listId : '',
			editable : false,
			defaultSelected:0,
			callback : {
				onChange : function(data) {
					$("#co_main").val(data.id);
				}
			}
		}).getCombo();
		provinceCombo = $('#spanProvince').combo({
			data:allAreaData,
			value: 'cy_id',
	        text: 'cy_name',
			width : 131,
			height : 30,
			listId : '',
			editable : true,
			callback : {
				onChange : function(data) {
					$("#co_province").val(data.cy_name);
					THISPAGE.queryCity(data.cy_id);
				}
			}
		}).getCombo();
		cityCombo = $('#spanCity').combo({
			value: 'cy_id',
	        text: 'cy_name',
			width : 131,
			height : 30,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#co_city").val(data.cy_name);
					THISPAGE.queryTown(data.cy_id);
				}
			}
		}).getCombo();
		townCombo = $('#spanTown').combo({
			value: 'cy_id',
	        text: 'cy_name',
			width : 131,
			height : 30,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#co_town").val(data.cy_name);
				}
			}
		}).getCombo();
		unitCombo = $('#spanUnit').combo({
			value: 'un_id',
	        text: 'un_name',
			width : 158,
			height : 30,
			listId : '',
			defaultSelected : 0,
			editable : false,
			callback : {
				onChange : function(data) {
					$("#co_unit").val(data.un_id);
				}
			}
		}).getCombo();
	},
	queryProvince:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"queryProvince",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					allAreaData = data.data;
					allAreaData.unshift({cy_id:"",cy_name:"--请选择--"});
					provinceCombo.loadData(allAreaData);
				}
			}
		 });
	},
	queryCity:function(id){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"queryCity",
			data:{"cy_id":id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					cityCombo.loadData(data.data);
				}
			}
		 });
	},
	queryTown:function(id){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"queryTown",
			data:{"cy_id":id},
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					townCombo.loadData(data.data);
				}
			}
		 });
	},
	queryUnit:function(){
		$.ajax({
			type:"POST",
			url:config.BASEPATH+"company/queryUnit",
			cache:false,
			dataType:"json",
			success:function(data){
				if(undefined != data && data.data != "" && data.data.length > 0){
					unitCombo.loadData(data.data);
				}
			}
		 });
	},
	initEvent:function(){
		$('#btn-save').on('click', function(e){
			e.preventDefault();
			handle.save();
		});
		$("#btn_close").click(function(){
			api.close();
		});
	}
};
THISPAGE.init();