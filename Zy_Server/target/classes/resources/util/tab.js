//; (function($) {
$.extend($.fn, {
    initJerichoTab: function(setting) {
        var opts = $.fn.extend({
            renderTo: null,
            uniqueId: null,
            tabs: [],
            activeTabIndex: 0,
            contentCss: {
                'height': '462px'
            },
            loadOnce: true,
            tabWidth: 113,
            loader: 'righttag'
        }, setting);
        function createJerichoTab() {
            if (opts.renderTo == null) { alert('you must set the \'renderTo\' property\r\t--JeirchoTab'); return; }
            if (opts.uniqueId == null) { alert('you must set the \'uniqueId\' property\r\t--JeirchoTab'); return; }
            if ($('#' + opts.uniqueId).length > 0) { alert('you must set the \'uniqueId\' property as unique\r\t--JeirchoTab'); return; }
            var jerichotab = $('<div class="jericho_tab" style="height:'+($(document).height()-85)+'px"><div class="tab_pages" ><div class="tabs"><ul /></div></div><div class="tab_content"><div id="jerichotab_contentholder" class="content" /></div></div>')
                                            .appendTo($(opts.renderTo));
            $('.tab_content>.content', jerichotab)
                                    .css(opts.contentCss);
            $.fn.jerichoTab = {
                master: jerichotab,
                tabWidth: opts.tabWidth,
                tabPageWidth: $('.tab_pages', jerichotab).width(),
                loader: opts.loader,
                loadOnce: opts.loadOnce,
                tabpage: $('.tab_pages>.tabs>ul', jerichotab),
                addTab: function(tabsetting) {
                    var ps = $.fn.extend({
                        tabId:"",
                        tabFirer: null,
                        title: '',
                        data: { dataType: '', dataLink: '' },
                        iconImg: '',
                        closeable: true,
                        onLoadCompleted: null
                    }, tabsetting);
                    tagGuid = (typeof tagGuid == 'undefined' ? 0 : ps.tabId);
                    var curIndex = tagGuid;
                    if (ps.tabFirer != null) {
                        var jerichotabindex = $(ps.tabFirer).attr('jerichotabindex');
                        if(jerichotabindex == 'undefined'){jerichotabindex=ps.tagId;}
                        
                        if (typeof jerichotabindex != 'undefined' && $('#jerichotab_' + jerichotabindex).length > 0)
                            return $.fn.setTabActive(jerichotabindex).adaptSlider().loadData();
                        $(ps.tabFirer).attr('jerichotabindex', curIndex);
                    }
                    var newTab = $('<li class="jericho_tabs tab_selected" style=""  id="jerichotab_' + curIndex + '" dataType="' + ps.data.dataType + '" dataLink="' + ps.data.dataLink + '">' +
                                                    '<div class="tab_left">' +
                                                        '<div class="tab_text" title="' + ps.title + '" style="">' + ps.title.cut(opts.tabWidth / 8 - 1) + '</div>  ' +
                                                        '<div class="tab_close">' + (ps.closeable ? '<a href="javascript:void(0)" class="iconfont" title="关闭">&#xe658;</a>' : '') + '</div>' +
                                                    '</div>' +
                                                '</li>')
                                                    .appendTo($.fn.jerichoTab.tabpage)
                                                        .css('opacity', '0')
                                                            .applyHover()
                                                                .applyCloseEvent()
                                                                        .animate({ 'opacity': '1' }, function() {
                                                                            $.fn.setTabActive(curIndex);
                                                                        });
                    tabHash = (typeof tabHash == 'undefined' ? [] : tabHash);
                    tabHash.push({
                        index: curIndex,
                        tabFirer: ps.tabFirer,
                        tabId: 'jerichotab_' + curIndex,
                        holderId: 'jerichotabholder_' + curIndex,
                        iframeId: 'jerichotabiframe_' + curIndex,
                        onCompleted: ps.onLoadCompleted
                    });
                    return newTab.applySlider();
                }
            };
            $.each(opts.tabs, function(i, n) {
                $.fn.jerichoTab.addTab(n);
            });
            if (tabHash.length == 0)
                jerichotab.css({ 'display': 'none' });
        }
        createJerichoTab();
        $.fn.setTabActive(opts.activeTabIndex).loadData();

        $(window).resize(function() {
            $.fn.jerichoTab.tabPageWidth = $('.tab_pages', $.fn.jerichoTab.master).width() - (($('.jericho_slider').length > 0) ? 38 : 0);
            $('.tabs', $.fn.jerichoTab.master).width($.fn.jerichoTab.tabPageWidth).applySlider().adaptSlider();
        });
    },
    setTabActiveByOrder: function(orderKey) {
        var lastTab = $.fn.jerichoTab.tabpage.children('li').filter('.tab_selected');
        if (lastTab.length > 0) lastTab.swapTabEnable();
        return $('#jericho_tabs').filter(':nth-child(' + orderKey + ')').swapTabEnable();
    },
    setTabActive: function(orderKey) {
        var lastTab = $.fn.jerichoTab.tabpage.children('li').filter('.tab_selected');
        if (lastTab.length > 0) lastTab.swapTabEnable();
        return $('#jerichotab_' + orderKey).swapTabEnable();
    },
    buildIFrame: function(src) {
        return this.each(function() {
            var onComleted = null, jerichotabiframe = '';
            for (var tab in tabHash) {
                if (tabHash[tab].holderId == $(this).attr('id')) {
                    onComleted = tabHash[tab].onCompleted;
                    jerichotabiframe = tabHash[tab].iframeId;
                    break;
                }
            }
            $('<iframe id="' + jerichotabiframe + '" name="' + jerichotabiframe + '" src="' + src + '" frameborder="0" scrolling="no" />')
                                        .css({ width: $(this).parent().width(), height: $(this).parent().height()-13, border: 0 })
                                            .appendTo($(this));
            document.getElementById(jerichotabiframe).onload = function() {
                !!onComleted ? onComleted(arguments[1]) : true;
                $.fn.removeLoader();
            };
        });
    },
    loadData: function() {
        return this.each(function() {
            $('#jerichotab_contentholder').showLoader();
            var onComleted = null, holderId = '';//, tabId = ''
            for (var tab in tabHash) {
                if (tabHash[tab].tabId == $(this).attr('id')) {
                    onComleted = tabHash[tab].onCompleted;
                    holderId = '#' + tabHash[tab].holderId;
//                    tabId = '#' + tabHash[tab].tabId;
                    break;
                }
            }
            var dataType = $(this).attr('dataType');
            var dataLink = $(this).attr('dataLink');
            if (typeof dataType == 'undefined' || dataType == '' || dataType == 'undefined') { removeLoading(); return; }
            $('#jerichotab_contentholder').children('div[class=curholder]').attr('class', 'holder').css({ 'display': 'none'});
            var holder = $(holderId);
            if (holder.length == 0) {
                holder = $('<div class="curholder" id="' + holderId.replace('#', '') + '" />').appendTo($('#jerichotab_contentholder'));
                load(holder);
            }
            else {
                holder.attr('class', 'curholder').css({ 'display': 'block' });
                if ($.fn.jerichoTab.loadOnce)
                    removeLoading();
                else {
                    holder.html('');
                    load(holder);
                }
            }
            function load(c) {
                switch (dataType) {
                    case 'formtag':
                        $(dataLink).css('display','none');
                        var clone = $(dataLink).clone(true).appendTo(c).css('display','block');
                        removeLoading(holder);
                        break;
                    case 'html':
                        holder.load(dataLink + '?t=' + Math.floor(Math.random()), function() {
                            removeLoading(holder);
                        });
                        break;
                    case 'iframe':
                        holder.buildIFrame(dataLink, holder);
                        break;
                    case 'ajax':
                        $.ajax({
                            url: dataLink,
                            data: { t: Math.floor(Math.random()) },
                            error: function(r) {
                                holder.html('error! can\'t load data by ajax');
                                removeLoading(holder);
                            },
                            success: function(r) {
                                holder.html(r);
                                removeLoading(holder);
                            }
                        });
                        break;
                }
            }
            function removeLoading(h) {
                !!onComleted ? onComleted(h) : true;
                $.fn.removeLoader();
            }
        });
    },
    attachSliderEvent: function() {
        return this.each(function() {
            var me = this;
            $(me).hover(function() {
                $(me).swapClass('jericho_slider' + $(me).attr('pos') + '_enable', 'jericho_slider' + $(me).attr('pos') + '_hover');
            }, function() {
                $(me).swapClass('jericho_slider' + $(me).attr('pos') + '_hover', 'jericho_slider' + $(me).attr('pos') + '_enable');
            }).mouseup(function() {
                if ($(me).is('[slide=no]')) return;
                var offLeft = parseInt($.fn.jerichoTab.tabpage.css('left'));
                var maxLeft = tabHash.length * $.fn.jerichoTab.tabWidth - $.fn.jerichoTab.tabPageWidth + 38;
                switch ($(me).attr('pos')) {
                    case 'left':
                        if (offLeft + $.fn.jerichoTab.tabWidth < 0)
                            $.fn.jerichoTab.tabpage.animate({ left: offLeft + $.fn.jerichoTab.tabWidth });
                        else
                            $.fn.jerichoTab.tabpage.animate({ left: 0 }, function() {
                                $(me).attr({ 'slide': 'no', 'class': 'jericho_sliders jericho_sliderleft_disable' });
                            });
                        $('.jericho_sliders[pos=right]').attr({ 'slide': 'yes', 'class': 'jericho_sliders jericho_sliderright_enable' });
                        break;
                    case 'right':
                        if (offLeft - $.fn.jerichoTab.tabWidth > -maxLeft)
                            $.fn.jerichoTab.tabpage.animate({ left: offLeft - $.fn.jerichoTab.tabWidth });
                        else
                            $.fn.jerichoTab.tabpage.animate({ left: -maxLeft }, function() {
                                $(me).attr({ 'slide': 'no', 'class': 'jericho_sliders jericho_sliderright_disable' });
                            });
                        $('.jericho_sliders[pos=left]').attr({ 'slide': 'yes', 'class': 'jericho_sliders jericho_sliderleft_enable' });
                        break;
                }
            });
        });
    },
    applySlider: function() {
        return this.each(function() {
            if (typeof tabHash == 'undefined' || tabHash.length == 0) return;
            $.fn.jerichoTab.tabpage.parent().css({ width: $.fn.jerichoTab.tabPageWidth });
        });
    },
    adaptSlider: function() {
        return this.each(function() {
        	if (typeof tabHash == 'undefined' || tabHash.length == 0) return;
        });
    },
    applyCloseEvent: function() {
        return this.each(function() {
            var me = this;
            $('.tab_close>a', this).mouseup(function(e) {
                e.stopPropagation();
                if ($(this).length == 0) return;
                $(me).animate({ 'opacity': '0', width: '0px' }, function() {
                    var lastTab = $.fn.jerichoTab.tabpage.children('li').filter('.tab_selected');
                    if (lastTab.attr('id') == $(this).attr('id')) {
                        $(this).prev().swapTabEnable().loadData();
                    }
                    for (var t in tabHash) {
                        if (tabHash[t].tabId == $(me).attr('id')) {
                            if (tabHash[t].tabFirer != null)
                            tabHash.splice(t, 1);
                        }
                    }
                    $(me).applySlider().remove();
                    $('#jerichotabholder_' + $(me).attr('id').replace('jerichotab_', '')).remove();
                });
            });
        });
    },
    applyHover: function() {
        return this.each(function() {
            $(this).hover(function() {
                if ($(this).hasClass('tab_unselect')) $(this).addClass('tab_unselect_h');
            }, function() {
                if ($(this).hasClass('tab_unselect_h')) $(this).removeClass('tab_unselect_h');
            }).mouseup(function() {
                if ($(this).hasClass('tab_selected')) return;
                var lastTab = $.fn.jerichoTab.tabpage.children('li').filter('.tab_selected');
                lastTab.attr('class', 'jericho_tabs tab_unselect');
                $('#jerichotabholder_' + lastTab.attr('id').replace('jerichotab_', '')).css({ 'display': 'none' });
                $(this).attr('class', 'jericho_tabs tab_selected').loadData().adaptSlider();
            });
        });
    },
    swapTabEnable: function() {
        return this.each(function() {
            if ($(this).hasClass('tab_selected'))
                $(this).swapClass('tab_selected', 'tab_unselect');
            else if ($(this).hasClass('tab_unselect'))
                $(this).swapClass('tab_unselect', 'tab_selected');
        });
    },
    swapClass: function(css1, css2) {
        return this.each(function() {
            $(this).removeClass(css1).addClass(css2);
        });
    },
    showLoader: function() {
        return this.each(function() {
            switch ($.fn.jerichoTab.loader) {
                case 'usebg':
                    break;
                case 'righttag':
                    if ($('#jerichotab_contentholder>.righttag').length == 0){}
                    else
                    break;
            }
        });
    },
    removeLoader: function() {
        switch ($.fn.jerichoTab.loader) {
            case 'usebg':
                break;
            case 'righttag':
                break;
        }
    }
});
//})(jQuery);

String.prototype.cut = function(len) {
    var position = 0;
    var result = [];
    var tale = '';
    for (var i = 0; i < this.length; i++) {
        if (position >= len) {
            tale = '';
            break;
        }
        if (this.charCodeAt(i) > 255) {
            position += 2;
            result.push(this.substr(i, 1));
        }
        else {
            position++;
            result.push(this.substr(i, 1));
        }
    }
    return result.join("") + tale;
};