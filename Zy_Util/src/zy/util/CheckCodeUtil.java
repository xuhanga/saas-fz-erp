package zy.util;

import java.util.Random;

public class CheckCodeUtil {
	public static String buildCheckCode(int numbers){
		int[] array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		Random rand = new Random();
		for (int i = 10; i > 1; i--) {
			int index = rand.nextInt(i);
			int tmp = array[index];
			array[index] = array[i - 1];
			array[i - 1] = tmp;
		}
		int result = 0;
		for (int i = 0; i < numbers; i++){
			result = result * 10 + array[i];
		}
		String str = result + "";
		if(str.length() < numbers){//如果生成的随机数不足以位，加6补齐
			for(int i =0,len=str.length();i<numbers-len;i++){
				str += "6";
			} 
		}
		return str;
	}
}
