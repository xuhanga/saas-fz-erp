package zy.util;

public class CommonUtil {
	
	public final static int SUCCESS = 200;
	public final static int FAIL = 500;
	public final static int EXISTED = 304;
	public final static int NOTFOUND = 404;
	public final static String DEFAULT_UPCODE = "0";
	public final static String KEY_MENU = "menu";
	public final static String KEY_MENULIMIT = "menulimit";
	public final static String KEY_USER = "user";
	public final static String KEY_SYSSET = "sysset";
	public final static String KEY_CASHIER = "cashier";
	public final static String KEY_CASHIER_SET = "cashier_set";
	public final static String COMPANYID = "companyid";
	public final static String SHOP_TYPE = "shop_type";
	public final static String KEY_PRINT = "print";
	//上级店铺的类型
	public final static String SHOP_UPTYPE = "shop_uptype";
	public final static String SHOP_CODE = "shop_code";
	public final static String SHOP_NAME = "shop_name";
	public final static String VERSION = "version";
	public final static String SHOP_UPCODE = "shop_upcode";
	public final static String KEY_VIP = "vip_info";
	public final static String EMP_NAME = "emp_name";
	public final static String EMP_CODE = "emp_code";
	public final static String VIP_CODE = "vip_code";
	public final static String VIP_OHTER = "vip_ohter";
	public final static String DP_CODE = "dp_code";
	public final static String SELL_TYPE="sell_type";
	public final static String PUTUP="putup";
	public final static int PAGE_SIZE = 10;
	
	public final static String STAT_VALID_CN = "正常";
	public final static String STAT_VALID_EN = "0";
	public final static String STAT_INVALID_CN = "停用";
	public final static String STAT_INVALID_EN = "1";
	
	public final static String INIT_PWD = "123456";
	/**
	 * 收钱吧服务商序列号、密钥、域名
	 * 测试序列号密钥(20160317111402、7a87156a7c8e9ca9cecf2787fefe47d3)
	 */
	public final static String SQB_VENDOR_SN = "20160317111402";
	public final static String SQB_VENDOR_KEY = "7a87156a7c8e9ca9cecf2787fefe47d3";
	public final static String SQB_API_DOMAIN = "https://api.shouqianba.com";
	/**
	 * 系统登录用户价格权限：【C:成本价 S:零售价 D:配送价 V:会员价 W:批发价 E:进货价 P:利润】
	 * */
	public final static String PRICE_LIMIT = "CSDVWEP";
	public static final String PRICE_LIMIT_COST = "C";
	public static final String PRICE_LIMIT_RETAIL = "S";
	public static final String PRICE_LIMIT_SEND = "D";
	public static final String PRICE_LIMIT_VIP = "V";
	public static final String PRICE_LIMIT_BATCH = "W";
	public static final String PRICE_LIMIT_ENTER = "E";
	public static final String PRICE_LIMIT_PROFIT = "P";
	
	
	
	
	public static String PAGEINDEX = "pageIndex";
	public static String PAGESIZE = "pageSize";
	public static String SIDX = "sidx";
	public static String SORD = "sord";
	public static String START = "start";
	public static String END = "end";
	
	
	public static Integer ZERO = 0;
	/**
	 * 店铺类型 1-总公司; 2-分公司; 3-自营店; 4-加盟店; 5-合伙店;
	 */
	public static String ONE = "1";
	public static String TWO = "2";
	public static String THREE = "3";
	public static String FOUR = "4";
	public static String FIVE = "5";
	
	public static String SEASON = "KEY_SEASON";//季节
	public static String UNITNAME = "KEY_UNITNAME";//单位
	public static String STUFF = "KEY_STUFF";//面料
	public static String STYLE = "KEY_STYLE";//款式
	public static String PRICE = "KEY_PRICE";//价格特性
	public static String BILL = "KEY_BILL";//单据性质
	public static String KEY_CASH = "CASH";//公共表KEY收银员设置
	public static String KEY_SHOP = "SHOP";//公共表KEY店铺设置
	
	public static Integer AR_STATE_NOTAPPROVE = 0;
	public static Integer AR_STATE_APPROVED = 1;
	public static Integer AR_STATE_FAIL = 2;
	public static Integer AR_STATE_REVERSE_APPROVE = 3;
	public static Integer AR_STATE_STOP = 4;
	public static Integer AR_STATE_SEND = 5;
	public static Integer AR_STATE_REJECT = 6;
	public static Integer AR_STATE_REJECTCONFIRM = 7;
	public static Integer AR_STATE_FINISH = 8;
	
	public static Integer STOP_STATE_NOT = 0;
	public static Integer STOP_STATE_YES = 1;
	
	public static Integer EXEC_STATE_UNDO = 0;
	public static Integer EXEC_STATE_ALL = 1;
	public static Integer EXEC_STATE_PART = 2;
	
	//付款状态0:未付 1:付部分 2：付清
	public static Integer PAY_STATE_UNPAY = 0;
	public static Integer PAY_STATE_PART = 1;
	public static Integer PAY_STATE_FINISH = 2;
	//店铺发货单:审核状态 0 待审批 1 已审核/待发货 2 已退回 3 已发货/在途 4 完成 5 拒收
	public static Integer WANT_AR_STATE_NOTAPPROVE = 0;
	public static Integer WANT_AR_STATE_APPROVED = 1;
	public static Integer WANT_AR_STATE_FAIL = 2;
	public static Integer WANT_AR_STATE_SEND = 3;
	public static Integer WANT_AR_STATE_FINISH = 4;
	public static Integer WANT_AR_STATE_REJECT = 5;
	/**
	 * 前台调拨单状态：0-暂存，1-已调出，2-完成，3-已拒收
	 */
	public static Integer ALLOCATE_STATE_NOTSEND = 0;
	public static Integer ALLOCATE_STATE_SEND = 1;
	public static Integer ALLOCATE_STATE_FINISH = 2;
	public static Integer ALLOCATE_STATE_REJECT = 3;
	
	/**
	 * KPI考核类型：0-店铺，1-员工组，2-员工
	 */
	public static Integer KPI_ASSESS_TYPE_SHOP = 0;
	public static Integer KPI_ASSESS_TYPE_EMPGROUP = 1;
	public static Integer KPI_ASSESS_TYPE_EMP = 2;
	
	/**
	 * KPI指标唯一标识
	 */
	public static final String KPI_IDENTITY_COMEAMOUNT = "comeamount";// 进店量
	public static final String KPI_IDENTITY_SELLMONEY = "sellmoney";// 销售额
	public static final String KPI_IDENTITY_SELLAMOUNT = "sellamount";// 销售数量
	public static final String KPI_IDENTITY_RECEIVE_RATE = "receive_rate";// 接待率
	public static final String KPI_IDENTITY_TRY_RATE = "try_rate";// 试穿率
	public static final String KPI_IDENTITY_DEAL_RATE = "deal_rate";// 成交率
	public static final String KPI_IDENTITY_JOINT_RATE = "joint_rate";// 连带率
	public static final String KPI_IDENTITY_RETURN_RATE = "return_rate";// 退单率
	public static final String KPI_IDENTITY_NEWVIP_COUNT = "newvip_count";// 新增会员
	public static final String KPI_IDENTITY_VIPCONTRIBUTION_RATE = "vipcontribution_rate";// VIP贡献率
	public static final String KPI_IDENTITY_AVG_SELLMONEY = "avg_sellmoney";// 人均销售
	public static final String KPI_IDENTITY_AVG_RATE = "avg_rate";// 平均折扣率
	public static final String KPI_IDENTITY_AVG_PRICE = "avg_price";// 平均单价
	public static final String KPI_IDENTITY_AVG_SELLPRICE = "avg_sellprice";// 客单价
	public static final String KPI_IDENTITY_BACK_RATE = "back_rate";// 回购率
	
	
	/**
	 * 待办事项唯一标识
	 */
	public static final String TODO_IDENTITY_PRODUCTRETURNDATE = "productreturndate";// 退换货到期
	public static final String TODO_IDENTITY_VIPBIRTHDAY = "vipbirthday";//会员生日
	public static final String TODO_IDENTITY_CONSUMEVISIT = "consumevisit";//消费回访
	public static final String TODO_IDENTITY_BATCHSELLWARN = "batchsellwarn";//批发出库单审核
	public static final String TODO_IDENTITY_BATCHRETURNWARN = "batchreturnwarn";//批发退货单审核
	public static final String TODO_IDENTITY_WANTWARN = "wantwarn";//店铺发货单
	public static final String TODO_IDENTITY_WANTRETURNWARN = "wantreturnwarn";//店铺退货单
	public static final String TODO_IDENTITY_WANTWARN_SELF = "wantwarn_self";//补货申请单
	public static final String TODO_IDENTITY_ALLOTSENDWARN = "allotsendwarn";//配货发货单
	public static final String TODO_IDENTITY_ALLOTRETURNCONFIRMWARN = "allotreturnconfirmwarn";//退货确认单
	public static final String TODO_IDENTITY_ALLOTAPPLYWARN = "allotapplywarn";//配货申请单
	public static final String TODO_IDENTITY_ALLOTRETURNAPPLYWARN = "allotreturnapplywarn";//退货申请单
	public static final String TODO_IDENTITY_BUYENTERWARN = "buyenterwarn";//进货入库单
	public static final String TODO_IDENTITY_BUYRETURNWARN = "buyreturnwarn";//退货出库单
	
	/**
	 * 银行流水账类别常量
		1	供应商预付款
		2	客户预收款
		3	店铺预收款
		4	供应商结算单
		5	客户结算单
		6	店铺结算单
		7	工资支出
		8	其他费用支出
		9	其他收入
		10	期初金额
		11	代金券发放
		12	储值卡充值
		13	银行存取款
		14	储值卡发放
		15	储值卡减值
		16	日结现金流向
		17	物流公司结算单
	 * */
	public static final String BANKRUN_BUYPREPAY = "1";
	public static final String BANKRUN_BATCHPREPAY = "2";
	public static final String BANKRUN_SORTPREPAY = "3";
	public static final String BANKRUN_BUYSETTLE = "4";
	public static final String BANKRUN_BATCHSETTLE = "5";
	public static final String BANKRUN_SORTSETTLE = "6";
	public static final String BANKRUN_MONEY_EXPENSE = "8";
	public static final String BANKRUN_MONEY_INCOME = "9";
	public static final String BANKRUN_MONEY_QC = "10";
	public static final String BANKRUN_VOUCHER = "11";
	public static final String BANKRUN_CARD_GRANT = "14";
	public static final String BANKRUN_CARD_PLUS = "12";
	public static final String BANKRUN_MONEY_ACCESS = "13";
	public static final String BANKRUN_CARD_MINUS = "15";
	public static final String BANKRUN_DENEND = "16";
	public static final String BANKRUN_STREAMSETTLE = "17";
	
	
	
	public static final String NUMBER_PREFIX_ECOUPON = "YHQ";
	public static final String NUMBER_PREFIX_BUY_ORDER_IN = "JHDD";
	public static final String NUMBER_PREFIX_BUY_ORDER_OUT = "THD";
	public static final String NUMBER_PREFIX_BUY_ENTER_IN = "CGRK";
	public static final String NUMBER_PREFIX_BUY_ENTER_OUT = "CGCK";
	public static final String NUMBER_PREFIX_BUY_PREPAY = "GYF";//供应商预付款
	public static final String NUMBER_PREFIX_BUY_FEE = "GFY";//供应商费用单
	public static final String NUMBER_PREFIX_BUY_SETTLE = "GJS";//供应商结算单
	public static final String NUMBER_PREFIX_BATCH_ORDER_OUT = "PFDD";//批发订单
	public static final String NUMBER_PREFIX_BATCH_ORDER_IN = "THSQ";//批发退货申请单
	public static final String NUMBER_PREFIX_BATCH_SELL_OUT = "PFCK";//批发出库
	public static final String NUMBER_PREFIX_BATCH_SELL_IN = "THRK";//批发退货入库单
	public static final String NUMBER_PREFIX_BATCH_PREPAY = "KYS";//客户预收款
	public static final String NUMBER_PREFIX_BATCH_FEE = "KFY";//客户费用单
	public static final String NUMBER_PREFIX_BATCH_SETTLE = "KJS";//客户结算单
	public static final String NUMBER_PREFIX_SHOP_WANT_DPFHD = "DPFHD";//店铺发货单
	public static final String NUMBER_PREFIX_SHOP_WANT_DPBHD = "DPBHD";//店铺补货单
	public static final String NUMBER_PREFIX_SHOP_WANT_DPTHD = "DPTHD";//店铺退货单
	public static final String NUMBER_PREFIX_SORT_ALLOT_PHFHD = "PHFHD";//配货发货单
	public static final String NUMBER_PREFIX_SORT_ALLOT_PHSQD = "PHSQD";//配货申请单
	public static final String NUMBER_PREFIX_SORT_ALLOT_PHTHD = "PHTHD";//配货退货单
	public static final String NUMBER_PREFIX_SORT_PREPAY = "DPYSK";//店铺预收款
	public static final String NUMBER_PREFIX_SORT_FEE = "DFY";//店铺费用单
	public static final String NUMBER_PREFIX_SORT_SETTLE = "DJS";//店铺结算单
	public static final String NUMBER_PREFIX_WLD = "WLD";//物流单
	public static final String NUMBER_PREFIX_WLJSD = "WJS";//物流结算单
	public static final String NUMBER_PREFIX_STOCK_ALLOCATE = "CKDB";
	public static final String NUMBER_PREFIX_STOCK_PRICE = "CBTJ";
	public static final String NUMBER_PREFIX_STOCK_ADJUST = "KCTZ";
	public static final String NUMBER_PREFIX_STOCK_LOSS = "KCBS";
	public static final String NUMBER_PREFIX_STOCK_OVERFLOW = "KCBY";
	public static final String NUMBER_PREFIX_STOCK_BATCH = "PDPC";
	public static final String NUMBER_PREFIX_STOCK_CHECK = "PD";
	public static final String NUMBER_PREFIX_SALE = "CXD";
	public static final String NUMBER_PREFIX_PRICE = "SPTJD";
	public static final String NUMBER_PREFIX_DEPOSIT = "D";
	public static final String NUMBER_PREFIX_MONEY_EXPENSE = "FYKZ";//费用开支单
	public static final String NUMBER_PREFIX_INCOME = "QTSR";//其他收入单
	public static final String NUMBER_PREFIX_MONEY_ACCESS = "CQK";//银行存取款
	public static final String NUMBER_PREFIX_SELL_ALLOCATE = "DB";//前台调拨
	public static final String NUMBER_PREFIX_SELL_CART = "DGXD";//导购下单
	public static final String NUMBER_PREFIX_SHOP_PLAN = "ZNJH";//智能计划
	public static final String NUMBER_PREFIX_SHOP_MONTHPLAN = "YJH";//月计划
	public static final String NUMBER_PREFIX_TARGET = "MBGL";//目标管理
	public static final String NUMBER_PREFIX_SHOP_KPIASSESS = "KH";//KPI考核
	public static final String NUMBER_PREFIX_SHOP_KPIPK = "PK";//PK工具
	public static final String NUMBER_PREFIX_QC_CLIENT = "QCYS";//客户期初
	public static final String NUMBER_PREFIX_QC_SUPPLY = "QCYF";//供应商期初
	public static final String NUMBER_PREFIX_QC_SHOP = "QCDP";//供应商期初
	/**
	 * 往来明细账 类型：0-进货；1-退货；2-期初；3-费用单；4-预付款；5-预付退款；6-结算单；
	 */
	public static final Integer BUYDEALINGS_TYPE_JH = 0;
	public static final Integer BUYDEALINGS_TYPE_TH = 1;
	public static final Integer BUYDEALINGS_TYPE_QC = 2;
	public static final Integer BUYDEALINGS_TYPE_FY = 3;
	public static final Integer BUYDEALINGS_TYPE_YFK = 4;
	public static final Integer BUYDEALINGS_TYPE_YFTK = 5;
	public static final Integer BUYDEALINGS_TYPE_JS = 6;
	
	public static final Integer BATCHDEALINGS_TYPE_PF = 0;
	public static final Integer BATCHDEALINGS_TYPE_TH = 1;
	public static final Integer BATCHDEALINGS_TYPE_QC = 2;
	public static final Integer BATCHDEALINGS_TYPE_FY = 3;
	public static final Integer BATCHDEALINGS_TYPE_YSK = 4;
	public static final Integer BATCHDEALINGS_TYPE_YSTK = 5;
	public static final Integer BATCHDEALINGS_TYPE_JS = 6;
	
	public static final Integer SORTDEALINGS_TYPE_PF = 0;
	public static final Integer SORTDEALINGS_TYPE_TH = 1;
	public static final Integer SORTDEALINGS_TYPE_QC = 2;
	public static final Integer SORTDEALINGS_TYPE_FY = 3;
	public static final Integer SORTDEALINGS_TYPE_YSK = 4;
	public static final Integer SORTDEALINGS_TYPE_YSTK = 5;
	public static final Integer SORTDEALINGS_TYPE_JS = 6;
	
	
	
	public static String IMPORT_TXT_PATH = "import_txt";
	
	/*public static String CHECK_BASE = "D:/boss";*/
	public static String FTP_SERVER_PATH = "http://ftp.zhjr100.com/";
	public static String DEFAULT_PICTURE = FTP_SERVER_PATH + "common_img/no_photo.png";
	public static String CHECK_BASE = "/usr/local/boss";
	public static String PRODUCT_IMG_PATH = "/product_img";
	public static String WXPRODUCT_IMG_PATH = "/wxproduct_img";
	public static String WXPRODUCT_TXT_PATH = "/wxproduct_txt";
	public static String WXCOMMENT_IMG_PATH = "/wxcomment_img";//微信公众号评价图片存放路径
	public static String PROGRAM_IMG_PATH = "/program_img";
	public static String VIP_IMG_PATH = "/vip_img";
	public static String WX_CER_PATH = "/wx_cer";//微信商户证书
	public static String WX_BG_PATH = "/wx_bg";//微信背景图
	public static String WX_PRODUCTTYPE_PATH = "/wx_producttype";//商品类别
	public static String SHOP_LOGO_PATH = "/shop_logo";//店铺Logo
	public static String EXCEL_PATH = "/excel_upload";//导入数据Excel存放路径
	
	public static final String COLORLEADING = "import_color_temp.xls";//颜色导入模板
	public static final String PRODUCTLEADING = "import_product_temp.xls";//商品资料导入模板
	public static final String BARCODELEADING = "import_barcode_temp.xls";//条形码导入模板
	public static final String STOCKLEADING = "import_stock_temp.xls";//库存导入模板
	public static final String BRANDLEADING = "import_brand_temp.xls";//品牌导入模板
	public static final String MEMBERLEADING = "import_member_temp.xls";//颜色导入模板
	
	//t_import_info表type字段
	public static final String IMPROT_COLORTYPE = "color";//颜色导入信息
	public static final String IMPROT_PRODUCTTYPE = "product";//商品资料导入信息
	public static final String IMPROT_BARCODETYPE = "barcode";//条形码导入信息
	public static final String IMPROT_STOCKTYPE = "stock";//库存导入信息
	public static final String IMPROT_BRANDTYPE = "brand";//品牌导入信息
	public static final String IMPROT_MEMBERTYPE = "member";//会员导入信息
	
	public static String PRINT_TYPE_CASH = "17";
}
