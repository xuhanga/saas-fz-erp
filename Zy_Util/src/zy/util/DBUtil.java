package zy.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBUtil {
	private Connection conn;
	public Connection getConn(String url,String name,String pwd){
		try {
			if(null != conn){
				return conn;
			}
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, name, pwd);
		} catch (Exception e) {
		}
		return conn;
	}
	public void closeConn(){
		try {
			if(null != conn){
				conn.close();
			}
		} catch (SQLException e) {
		}
	}
}
