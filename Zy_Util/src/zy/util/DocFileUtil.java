package zy.util;

public class DocFileUtil {
	// 补齐空格
	public static String getKongGe(int allLen, String s) {
		int nowLen = s.toString().getBytes().length;
		int len = allLen - nowLen;
		String temp = " ";
		StringBuffer buf = new StringBuffer(s);
		for (int i = 0; i < len; i++) {
			buf.append(temp);
		}
		return buf.toString();
	}

	public static String getCopyString(int allLen, String s) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < allLen; i++) {
			stringBuilder.append(s);
		}
		return stringBuilder.toString();
	}

	public static void main(String[] args) {
		String test = DocFileUtil.getCopyString(70, "_");
		System.out.println(test.length());
	}
}
