package zy.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ExportTxtUtil {
	
	public static void string2Txt(String content,String file) {
		OutputStreamWriter writer = null;
		try {
			File savefile = new File(file);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			writer = new OutputStreamWriter(new FileOutputStream(savefile), "UTF-8");   
			writer.write(content);   
			writer.flush();   
		} catch (Exception e) {
		}finally{
			if(writer != null){
				try {
					writer.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	public static String txt2String(String file) {
		StringBuilder result = new StringBuilder();
		BufferedReader reader = null;
		try {
			InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");
			reader = new BufferedReader(isr);// 构造一个BufferedReader类来读取文件
			String s = null;
			while ((s = reader.readLine()) != null) {// 使用readLine方法，一次读一行
				result.append(System.lineSeparator() + s);
			}
			reader.close();
		} catch (Exception e) {
		}finally{
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result.toString();
	}
	
	public void writeTXT(String filepath, List<String[]> list,HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException{
        OutputStream os =null;

        String fileName;
		try {
			  // 设置HTTP头：
	        response.reset();
	        response.setContentType("application/octet-stream");
			fileName = URLDecoder.decode(filepath,"utf-8");
			java.net.URLEncoder.encode(fileName, "utf-8");
	        response.addHeader("Content-Disposition","attachment;"+ "filename=\"" +URLEncoder.encode(fileName, "utf-8") + "\"");

         	   // 写缓冲区：     
             os = response.getOutputStream();
             String historyAlarm="";//historyAlarm为后台得到的数据 
             byte[]byt=historyAlarm.getBytes();
             
             for(String[] strs:list){
          	   String rowStr = StringUtil.trimString(strs[0])+"   "+StringUtil.trimString(strs[1]);
          	   //转换为byte数组
                 byte[] b1 = rowStr.getBytes();
                 //换行符
                 byte[] b2 = "\r\n".getBytes();
                 //依次写入文件
                 os.write(b1);
                 os.write(b2);
                 os.write(byt);
             }
             os.flush();
		}catch (final Exception e) {
			e.printStackTrace();
		}finally{
			 try {
				if(os!=null) os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    } 
}
