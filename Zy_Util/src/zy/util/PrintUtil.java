package zy.util;

public class PrintUtil {
	public static class PrintType{
		/**
		 * 1	进货订单打印模板
		 * 2	进货出入库打印模板
		 * 3	批发打单打印模板
		 * 4	批发出入库打印模板
		 * 5	配货单据打印模板
		 * 6	店铺单据打印模板
		 * 7	仓库调拨单打印模板
		 * 8	库存调整、报损、报溢打印模板
		 * 9	供应商结算单打印模板
		 * 10	客户结算单打印模板
		 * 11	店铺结算单打印模板
		 * 12	供应商费用单打印模板
		 * 13	客户费用单打印模板
		 * 14	店铺费用单打印模板
		 * 15	费用开支、其他收入单打印模板
		 * 16	银行存取款打印模板
		 * */
		public static final int PRINT_TYPE_BUY_ORDER = 1;
		public static final int PRINT_TYPE_BUY_ENTER = 2;
		public static final int PRINT_TYPE_BATCH_ORDER = 3;
		public static final int PRINT_TYPE_BATCH_SELL = 4;
		public static final int PRINT_TYPE_SORT_ALLOT = 5;
		public static final int PRINT_TYPE_WANT = 6;
		public static final int PRINT_TYPE_STOCK_ALLOCATE = 7;
		public static final int PRINT_TYPE_STOCK_ADJUST = 8;
		public static final int PRINT_TYPE_BUY_SETTLE = 9;
		public static final int PRINT_TYPE_BATCH_SETTLE = 10;
		public static final int PRINT_TYPE_SORT_SETTLE = 11;
		public static final int PRINT_TYPE_BUY_FEE = 12;
		public static final int PRINT_TYPE_BATCH_FEE = 13;
		public static final int PRINT_TYPE_SORT_FEE = 14;
		public static final int PRINT_TYPE_MONEY_EXPENSE = 15;
		public static final int PRINT_TYPE_MONEY_ACCESS = 16;
	}
	
	public static class PrintSet{
		/**标题名称*/
		public static final String PAGE_TITLE = "page_title";
		public static final String PAGE_WIDTH = "page_width";
		public static final String PAGE_HEIGHT = "page_height";
		public static final String PAGE_LEFT = "page_left";
		public static final String PAGE_RIGHT = "page_right";
		public static final String PAGE_TOP = "page_top";
		public static final String PAGE_BOTTOM = "page_bottom";
		public static final String PAGE_DIRECTION = "page_direction";
		/**
		 * 页面打印放心：1-纵向，2-横向
		 */
		public static final String PAGE_DIRECTION_VERTICAL = "1";
		public static final String PAGE_DIRECTION_HORIZONTAL = "2";
		/**标题高度、大小、字体*/
		public static final String TITLE_HEIGHT = "title_height";
		public static final String TITLE_SIZE = "title_size";
		public static final String TITLE_FONT = "title_font";
		/**表头字体、大小、每行高度*/
		public static final String HEAD_FONT = "head_font";
		public static final String HEAD_SIZE = "head_size";
		public static final String HEAD_HEIGHT = "head_height";
		
		/**表格数据与表头距离*/
		public static final String TABLE_TOP = "table_top";
		/**表格数据字体、大小、加粗、小计、总计*/
		public static final String TABLE_FONT = "table_font";
		public static final String TABLE_SIZE = "table_size";
		public static final String TABLE_BOLD = "table_bold";
		public static final String TABLE_SUBTOTAL = "table_subtotal";
		public static final String TABLE_TOTAL = "table_total";
		public static final String TABLE_DATAHEIGHT = "table_dataheight";
		/**表格数据表头高度*/
		public static final String TABLE_HEADHEIGHT = "table_headheight";
		
		/**是否每页显示或最后显示*/
		public static final String END_ENDSHOW = "end_endshow";
		
		/**左边距*/
		public static final String HEAD_PADDINGLEFT = "head_paddingleft";
		/**空边距*/
		public static final String EMPTY_HEIGHT = "empty_height";
	}
	
	public static class PrintData{
		public static final String INDEX = "index";
		public static final String PD_NO = "pd_no";
		public static final String PD_NAME = "pd_name";
		public static final String BD_NAME = "bd_name";
		public static final String TP_NAME = "tp_name";
		public static final String PD_SEASON = "pd_season";
		public static final String PD_YEAR = "pd_year";
		public static final String CR_NAME = "cr_name";
		public static final String BR_NAME = "br_name";
		public static final String SZ_NAME = "sz_name";
		public static final String PD_UNIT = "pd_unit";
		public static final String AMOUNT = "amount";
		public static final String REAL_AMOUNT = "real_amount";
		public static final String GAP_AMOUNT = "gap_amount";
		public static final String APPLY_AMOUNT = "apply_amount";
		public static final String SEND_AMOUNT = "send_amount";
		public static final String UNIT_PRICE = "unit_price";
		public static final String UNIT_MONEY = "unit_money";
		public static final String APPLY_MONEY = "apply_money";
		public static final String SEND_MONEY = "send_money";
		public static final String RETAIL_PRICE = "retail_price";
		public static final String RETAIL_MONEY = "retail_money";
		public static final String SELL_PRICE = "sell_price";
		public static final String SELL_MONEY = "sell_money";
		public static final String COST_PRICE = "cost_price";
		public static final String COST_MONEY = "cost_money";
		public static final String REBATE_PRICE = "rebate_price";
		public static final String REBATE_MONEY = "rebate_money";
		public static final String REBATE = "rebate";
		public static final String GIFT = "gift";
		public static final String REMARK = "remark";
		public static final String TYPE = "type";
		public static final String BANK_NAME ="bank_name";
		public static final String PROPERTY_NAME ="property_name";
		public static final String PROPERTY_CODE ="property_code";
		public static final String RECEIVABLE ="receivable";
		public static final String RECEIVED ="received";
		public static final String DISCOUNT_MONEY_YET ="discount_money_yet";
		public static final String PREPAY_YET ="prepay_yet";
		public static final String UNRECEIVABLE ="unreceivable";
		public static final String DISCOUNT_MONEY ="discount_money";
		public static final String PREPAY ="prepay";
		public static final String REAL_RECEIVED ="real_received";
		public static final String PAYABLE ="payable";
		public static final String PAYABLED ="payabled";
		public static final String UNPAYABLE ="unpayable";
		public static final String REAL_PAY ="real_pay";
		public static final String NUMBER = "number";
		public static final String MONEY = "money";
		public static final String DATE = "date";
	}
	
	public static class PrintField{
		public static final String MAKEDATE = "makedate";
		public static final String MANAGER = "manager";
		public static final String NUMBER = "number";
		public static final String SUPPLY_NAME = "supply_name";
		public static final String CLIENT_NAME = "client_name";
		public static final String SHOP_NAME = "shop_name";
		public static final String TEL = "tel";
		public static final String MOBILE = "mobile";
		public static final String DEPOT_NAME = "depot_name";
		public static final String OUTDEPOT_NAME = "outdepot_name";
		public static final String INDEPOT_NAME = "indepot_name";
		public static final String OUTSHOP_NAME = "outshop_name";
		public static final String INSHOP_NAME = "inshop_name";
		public static final String LINKMAN = "linkman";
		public static final String HAND_NUMBER = "hand_number";
		public static final String AMOUNT = "amount";
		public static final String REAL_AMOUNT = "real_amount";
		public static final String GAP_AMOUNT = "gap_amount";
		public static final String APPLY_AMOUNT = "apply_amount";
		public static final String SEND_AMOUNT = "send_amount";
		public static final String MONEY = "money";
		public static final String APPLY_MONEY = "apply_money";
		public static final String SEND_MONEY = "send_money";
		public static final String STREAM_MONEY = "stream_money";
		public static final String REBATE_MONEY = "rebate_money";
		public static final String ADDRESS = "address";
		public static final String PROPERTY = "property";
		public static final String REMARK = "remark";
		public static final String PAYABLE = "payable";
		public static final String RECEIVABLE = "receivable";
		public static final String DEPT = "dept";
		public static final String END_MAKER = "end_maker";
		public static final String END_APPROVER = "end_approver";
		public static final String END_RECEIVER = "end_receiver";
		public static final String END_RECEDATE = "end_recedate";
		public static final String BANK_NAME ="bank_name";
		public static final String DISCOUNT_MONEY = "discount_money";
		public static final String PREPAY = "prepay";
		public static final String PAYABLED = "payabled";
		public static final String PAYABLEDMORE = "payabledmore";
		public static final String RECEIVED = "received";
		public static final String RECEIVEDMORE = "receivedmore";
	}
	
	public static class SellPrint_Data{
		public static final String INDEX = "index";
		public static final String PD_NO = "pd_no";
		public static final String PD_NAME = "pd_name";
		public static final String PD_STYLE = "pd_style";
		public static final String AMOUNT = "amount";
		public static final String SIGN_PRICE = "sign_price";
		public static final String SELL_PRICE = "sell_price";
		public static final String PRICE = "price";
		public static final String MONEY = "money";
	}
	
	public static class SellPrint_Field{
		public static final String NUMBER = "number";
		public static final String CASHTYPE = "cashtype";
		public static final String CASHDATE = "cashdate";
		public static final String CASHMAN = "cashman";
		public static final String GUIDE = "guide";
		public static final String VIP_CODE = "vip_code";
		public static final String VIP_NAME = "vip_name";
		public static final String VIP_TEL = "vip_tel";
		public static final String VIP_POINT = "vip_point";
		public static final String SHOP_NAME = "shop_name";
		public static final String SHOP_TEL = "shop_tel";
		public static final String ADDRESS = "address";
		public static final String REMARK = "remark";
		public static final String SHOP_MOBILE = "shop_mobile";
		public static final String SELL_MONEY = "sell_money";
		public static final String REAL_MONEY = "real_money";
		public static final String LOST_MONEY = "lost_money";
		public static final String CHANGE_MONEY = "change_money";
		public static final String CD_MONEY = "cd_money";
		public static final String EC_MONEY = "ed_money";
		public static final String VC_MONEY = "vc_money";
		public static final String PRINT_TIME = "print_time";
	}
	
	/**通用判断 0*/
	public static final int FALSE = 0;
	/**通用判断 1*/
	public static final int TRUE = 1;
	
	/**宽度*/
	public static final String WIDTH = "width";
	/**对齐方式*/
	public static final String ALIGN = "align";
	
	/**合并单元格值*/
	public static final String COLSPAN = "colspan";
	/**表头行数*/
	public static final String LINE = "line";
	/**表头列数*/
	public static final String ROW = "row";	
	
	public static String doAlign(String val){
		String align = "left";
		if(val != null){
			if(val.equals("1")){
				align = "center";
			}
			if(val.equals("2")){
				align = "right";
			}
		}
		return align;
	}
}
