package zy.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;

public class RequestUtil {
	public static Map<String, String> request2Map(HttpServletRequest request) {
		Map<String, String> map = new HashMap<String, String>();
		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements()) {
			String key = names.nextElement();
			String value = request.getParameter(key);
			if (value == null || value.trim().equals("")) {
				continue;
			}
			map.put(key, value);
		}
		return map;
	}
	public static Map<String, String> request2MapByNumKey(HttpServletRequest request) {
		Map<String, String> map = new HashMap<String, String>();
		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements()) {
			String key = names.nextElement();
			String value = request.getParameter(key);
			if (value == null || value.trim().equals("")) {
				continue;
			}
			map.put(key, value);
		}
		return map;
	}
	
	public static String getIP(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (!checkIP(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (!checkIP(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (!checkIP(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (!checkIP(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (!checkIP(ip)) {
			ip = request.getRemoteAddr();
		}
		// 多级反向代理
		if (null != ip && !"".equals(ip.trim())) {
			StringTokenizer st = new StringTokenizer(ip, ",");
			if (st.countTokens() > 1) {
				return st.nextToken();
			}
		}

		return ip;
	}
	private static boolean checkIP(String ip) {
		if (StringUtils.isNotBlank(ip) && ip.split("\\.").length == 4) {
			return true;
		}
		return false;
	}
	/**
	 * 通过ip获取ip所在城市
	 * @param ip
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String getCityByIP(HttpServletRequest request) {
		URLConnection conn = null;
		StringBuffer sb = null;
		BufferedReader br = null;// 读取data数据流
		String ip = getIP(request);
		String cityName = "";
		try {
			URL url = new URL(URLUtil.TAOBAO_CITYURL + ip);
			conn = url.openConnection();
			conn.setConnectTimeout(1000);

			br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			sb = new StringBuffer();
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			JSONObject jsonData = JSONObject.parseObject(sb.toString());
			JSONObject citydata = jsonData.getJSONObject("data");
			cityName = citydata.getString("city");
		} catch (Exception e) {
			cityName = "";
		}finally{
			try {
				if(br != null){br.close();}
			} catch (Exception e) {
			}
		}
		return cityName;
	}
}
