package zy.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;


/**
 * @author yanhan
 * @version 1.0 Create at 2012-07-16
 * 
 */
public class StringUtil implements Serializable {
	private static final long serialVersionUID = -8544303665746827338L;
	public static String trimString(String str){
		if(null != str && !"".equals(str)){
			return str.trim();
		}else{
			return "";
		}
	}
	public static String trimNullString(String str){
		if(null != str && !"".equals(str) && !"null".equals(str)){
			return str.trim();
		}else{
			return "";
		}
	}
	/**
	* 将jquery ajax 中文字符串 解码
	* @param str 需要解码的字符串
	* @return
	*/
	public static String decodeString(String str){
		if(str==null || str.equals("")){
			return "";
		}
		str = str.trim();
		try {
			str = java.net.URLDecoder.decode(java.net.URLDecoder.decode(str, "UTF-8"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
	/**
	* 将jquery ajax 中文字符串 解码
	* @param str 需要解码的字符串
	* @return
	*/
	public static Object decodeString(Object str){
		if(str==null || str.equals("")){
			return "";
		}
		str = str.toString().trim();
		try {
			str = java.net.URLDecoder.decode(java.net.URLDecoder.decode(str.toString(), "UTF-8"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
	/**
	 * 按长度在字符右边补齐空格
	 * @param str
	 * @param len
	 * @return
	 */
	public static String toChar(String str,int len){
		StringBuffer sb=new StringBuffer(str);
		len=len-str.length();
		for (int i = 0; i < len; i++) {
			sb.append(" ");
		}
		return sb.toString();
	}
	
	public static String trimString(Object obj){
		if (obj==null){
			return "";
		}
		String result =String.valueOf(obj);
		return result ;
	}
	
	public static String toSort(String source){
		if(null != source && !"".equals(source)){
			StringBuffer sort = new StringBuffer();
			char[] c = source.toCharArray();
			java.util.Arrays.sort(c);//对数组进行排序 
			for (int i = 0; i < c.length; i++) {  
				sort.append(String.valueOf(c[i]));  
			}  
			return sort.toString();//返回数组。注：char数组相当于String类型} 完成后直接调用该方法就好了：
		}else{
			return "";
		}
	}
	
	/**
	 * double类型取到小数点后两位
	 * @param fo
	 * @return
	 */
	public static String doubleRtTwo(double doubleValue){
		try {
			if(Double.isNaN(doubleValue)){
				return "0.0";
			}
			//科学计数法转换成普通算法
			return String.format("%.2f", doubleValue);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return "0.0";
		}
	}
	/**
	 * double类型取到小数点后两位
	 * @param fo
	 * @return
	 */
	public static double double2Two(double doubleValue){
		try {
			if(Double.isNaN(doubleValue)){
				return 0.0;
			}
			//科学计数法转换成普通算法
			return Double.parseDouble(String.format("%.2f", doubleValue));
		} catch (RuntimeException e) {
			return 0.0;
		}
	}
	/**
	 * 汉字转换位汉语拼音首字母，英文字符不变
	 * 
	 * @param chines
	 *            汉字
	 * @return 拼音
	 */
	public static String getSpell(String chines) {
		String pinyinName = "";
		char[] nameChar = chines.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		String[] temp;
		for (int i = 0; i < nameChar.length; i++) {
			if (nameChar[i] > 128) {
				try {
					temp = PinyinHelper.toHanyuPinyinStringArray(nameChar[i], defaultFormat);
					if(temp != null && temp.length > 0){
						pinyinName += temp[0].charAt(0);
					}
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}
			} else {
				pinyinName += nameChar[i];
			}
		}
		return pinyinName.toUpperCase();
	}
	
	public static boolean isNotEmpty(Object obj){
		if(obj == null){
			return false;
		}
		if("".equals(obj.toString().trim())){
			return false;
		}
		return true;
	}
	public static boolean isEmpty(Object obj){
		if(obj == null || "".equals(obj.toString().trim()) || "null".equals(obj.toString().trim())){
			return true;
		}
		return false;
	}
	public static List<String> parseList(String code){
		String[] arr = code.split(",");
		List<String> list = new ArrayList<String>(arr.length);
		for(String _code:arr){
			list.add(_code);
		}
		return list;
	}
	
	/**
	 * 将汉字字符串转换为简拼(大写)
	 * @param chinaString	汉字字符串
	 * @return	简拼
	 * @throws BadHanyuPinyinOutputFormatCombination
	 */
	public static String Hanzi2SimpleSpelling(String chinaString){
		String simpleSpelling = "";
		String[] arrStr = null;
		try {
			arrStr = Hanzi2PingYin(chinaString);
		} catch (BadHanyuPinyinOutputFormatCombination e) {
			return "";
		}
		for(String str : arrStr){
			simpleSpelling += str.substring(0,1);
		}
		return simpleSpelling.toUpperCase();
	}
	
	/**
	 * 将汉字转换为汉语拼音
	 * @param chinaString	汉字字符串(不能有标点符号)
	 * @return		转换后的拼音数组
	 * @throws BadHanyuPinyinOutputFormatCombination
	 */
	public static String[] Hanzi2PingYin(String chinaString) throws BadHanyuPinyinOutputFormatCombination {
		
		HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
		outputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		// WITH_U_AND_COLON and WITH_TONE_NUMBER
		outputFormat.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);
		outputFormat.setToneType(HanyuPinyinToneType.WITH_TONE_NUMBER);
		// WITH_V and WITHOUT_TONE
		outputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
		outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		
		char[] arrCha = chinaString.toCharArray();
		String[] arrStr = new String[arrCha.length]; 
		for (int i = 0; i < arrCha.length; i++) {
			String temp = String.valueOf(arrCha[i]);
			try {
				arrStr[i] = PinyinHelper.toHanyuPinyinStringArray(arrCha[i], outputFormat)[0];
			} catch (Exception e) {
				arrStr[i] = temp;
			}
		}
		return arrStr;
	}
	
	/**
	 * Double类别数据四舍五入转成2位小数
	 * 同时去除科学计数法的影响
	 * @param val 数值, type 类型  0(89123456.79)  1(89,123,456.79)
	 * @return 格式化后数值
	 * */
	public static String doubleFixed(double val , int type){
		String returnValue = "";
		if(type == 1){
			NumberFormat nf = NumberFormat.getNumberInstance();
			nf.setMaximumFractionDigits(2);
			returnValue =  nf.format(val);
		}else{
			returnValue = String.format("%.2f", val);
		}
		return returnValue;
	}
	
	/**
	 * 批量增加数据 获取code
	 * @param intcode
	 * @return
	 */
	public static String getTwoCode(int intcode){
		if(intcode < 10){
			return "00"+intcode;
		}else if(intcode >= 10 && intcode < 100){
			return "0"+intcode;
		}else {
			return ""+intcode;
		}
	}
	/**
	 * type类型
	 * name：名称  mobile: 手机 card:卡号
	 * 
	 * */
	public static String addPass(String name,String type){
		if(null != name && !"".equals(name)){
			if("name".equals(type)){
				if(name.length() > 1){
					name = name.substring(0,1)+"**";
				}
			}
			if("mobile".equals(type)){
				if(name.length() > 6){
					name = name.substring(0,7)+"****";
				}
			}
			if("card".equals(type)){
				if(name.length() > 3){
					name = name.substring(0,4)+"****";
				}
			}
		}
		return name;
	}
	
	public static void main(String[] args) {
		System.out.println(addPass("15366959565", "name"));
	}
}
