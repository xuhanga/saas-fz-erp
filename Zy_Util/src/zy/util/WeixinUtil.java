package zy.util;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class WeixinUtil {

	// 平台凭证
	public static String my_app_id = "wx0951b3f84d89fa80";
	// 平台凭证密钥
	public static String my_app_secret = "58fff31864775309f1b0fbf1e9bf4c29";
	//商户号
	public static String mchid = "";
	
	
	
//	// 平台凭证
//	public static String my_app_id = "wx381a4c4a94c1b31a";
//	// 平台凭证密钥
//	public static String my_app_secret = "6293e95959c21e43877cee04e6538bb6";
	//平台Token
	public static String my_token="zhjr100";
	// 平台EncodingAESKey
	public static String my_encoding_aes_key="58acd31864775309f1b0fbf1e9bf4c29wx0951b3f84";
	
	
	//获取第三方平台component_access_token(POST)
	public static String get_component_access_token_url = "https://api.weixin.qq.com/cgi-bin/component/api_component_token";
	//获取预授权码pre_auth_code(POST)
	public static String get_pre_auth_code_url = "https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token={0}";
	//使用授权码换取公众号或小程序的接口调用凭据和授权信息
	public static String get_query_auth_url = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token={0}";
	//授权页
	public static String component_login_page_url = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid={0}&pre_auth_code={1}&redirect_uri={2}";
	//授权页回调地址
	public static String auth_redirect_uri = "http://small.zhjr100.com/queryAuth";
	
	//为授权的小程序帐号上传小程序代码
	public static String code_commit_url = "https://api.weixin.qq.com/wxa/commit?access_token={0}";
	
	public static Map<String, Object> getComponentAccessToken(String verify_ticket) {
		JSONObject json = new JSONObject();
		json.put("component_appid", my_app_id);
		json.put("component_appsecret", my_app_secret);
		json.put("component_verify_ticket", verify_ticket);
		JSONObject resultJson = HttpsUtil.httpsRequest(get_component_access_token_url, "POST", json.toString());
		return resultJson;
	}
	
	public static Map<String, Object> getPreAuthCode(String component_access_token) {
		JSONObject json = new JSONObject();
		json.put("component_appid", my_app_id);
		JSONObject resultJson = HttpsUtil.httpsRequest(MessageFormat.format(get_pre_auth_code_url, component_access_token), "POST", json.toString());
		return resultJson;
	}
	
	public static JSONObject queryAuth(String component_access_token,String auth_code) {
		JSONObject json = new JSONObject();
		json.put("component_appid", my_app_id);
		json.put("authorization_code", auth_code);
		JSONObject resultJson = HttpsUtil.httpsRequest(MessageFormat.format(get_query_auth_url, component_access_token), "POST", json.toString());
		return resultJson;
	}
	
	public static JSONObject code_commit(String authorizer_access_token){
		JSONObject json = new JSONObject();
		json.put("template_id", 1);
		json.put("user_version", "V1.0");
		json.put("user_desc", "Test");
//		json.put("ext_json", value);
		Map<String, Object> extJson = new HashMap<String, Object>();
		extJson.put("extEnable", true);
		extJson.put("extAppid", "wx0951b3f84d89fa80");
		//ext
		Map<String, Object> ext = new HashMap<String,Object>();
		ext.put("AppID", "wx0951b3f84d89fa80");
		ext.put("AppSecret", "58fff31864775309f1b0fbf1e9bf4c29");
		ext.put("AppSecret", "58fff31864775309f1b0fbf1e9bf4c29");
		extJson.put("ext", ext);
		JSONObject resultJson = HttpsUtil.httpsRequest(MessageFormat.format(code_commit_url, authorizer_access_token), "POST", json.toString());
		return resultJson;
	}
	
	
}
