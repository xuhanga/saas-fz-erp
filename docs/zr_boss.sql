/*
 Navicat Premium Data Transfer

 Source Server         : 公司-服装库
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : 61.144.186.197:9701
 Source Schema         : zr_boss

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 09/04/2024 17:20:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for common_city
-- ----------------------------
DROP TABLE IF EXISTS `common_city`;
CREATE TABLE `common_city`  (
  `cy_id` int NOT NULL AUTO_INCREMENT,
  `cy_name` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `cy_upid` int NULL DEFAULT NULL,
  `cy_level` tinyint(1) NULL DEFAULT NULL,
  `cy_area_code` varchar(4) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `cy_post_code` varchar(6) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  `cy_state` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`cy_id`) USING BTREE,
  INDEX `IDX_CY_ID`(`cy_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3307 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_dict
-- ----------------------------
DROP TABLE IF EXISTS `common_dict`;
CREATE TABLE `common_dict`  (
  `dt_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '账户类型编号',
  `dt_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '账户类型名称',
  `dt_type` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '类型：银行流水BANK_RUN、会员积分VIP_POINT',
  PRIMARY KEY (`dt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_kpi
-- ----------------------------
DROP TABLE IF EXISTS `common_kpi`;
CREATE TABLE `common_kpi`  (
  `ki_id` int NOT NULL AUTO_INCREMENT,
  `ki_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '指标编号',
  `ki_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '指标名称',
  `ki_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `ki_identity` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '标识',
  `ki_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态：0-正常，1-停用',
  PRIMARY KEY (`ki_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '系统KPI参数，由控制台维护' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_log
-- ----------------------------
DROP TABLE IF EXISTS `common_log`;
CREATE TABLE `common_log`  (
  `lg_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `lg_title` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '系统消息标题',
  `lg_content` varchar(4000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '系统消息内容',
  `lg_user` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '系统消息发送人',
  `lg_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  PRIMARY KEY (`lg_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_print
-- ----------------------------
DROP TABLE IF EXISTS `common_print`;
CREATE TABLE `common_print`  (
  `pt_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pt_name` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '模板名称',
  `pt_type` tinyint(1) NULL DEFAULT NULL COMMENT '模板类型',
  PRIMARY KEY (`pt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_printdata
-- ----------------------------
DROP TABLE IF EXISTS `common_printdata`;
CREATE TABLE `common_printdata`  (
  `ptd_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ptd_pt_id` int NOT NULL DEFAULT 0 COMMENT '关联ID',
  `ptd_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '数据库字段名',
  `ptd_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '中文名称',
  `ptd_order` smallint NULL DEFAULT 1 COMMENT '默认排序',
  `ptd_show` tinyint(1) NULL DEFAULT 1 COMMENT '默认是否显示 0 否1是',
  `ptd_width` smallint NULL DEFAULT 80 COMMENT '默认列宽',
  `ptd_align` tinyint(1) NULL DEFAULT 0 COMMENT '默认对齐方式：0:左1:中2:右',
  PRIMARY KEY (`ptd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 567 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_printfield
-- ----------------------------
DROP TABLE IF EXISTS `common_printfield`;
CREATE TABLE `common_printfield`  (
  `ptf_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ptf_pt_id` int NOT NULL DEFAULT 0 COMMENT '关联ID',
  `ptf_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '数据库字段名',
  `ptf_name` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '中文名称',
  `ptf_colspan` tinyint(1) NULL DEFAULT 1 COMMENT '默认占列数',
  `ptf_show` tinyint(1) NULL DEFAULT 1 COMMENT '默认是否显示',
  `ptf_line` tinyint(1) NULL DEFAULT 1 COMMENT '第几列',
  `ptf_row` tinyint(1) NULL DEFAULT 1 COMMENT '行数',
  `ptf_position` tinyint(1) NULL DEFAULT 0 COMMENT '字段位置 0:表头 1:表尾',
  PRIMARY KEY (`ptf_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 369 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_printset
-- ----------------------------
DROP TABLE IF EXISTS `common_printset`;
CREATE TABLE `common_printset`  (
  `pts_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pts_pt_id` int NULL DEFAULT 0 COMMENT '模板ID',
  `pts_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字段编号',
  `pts_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '中文名称',
  PRIMARY KEY (`pts_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 560 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_rewardicon
-- ----------------------------
DROP TABLE IF EXISTS `common_rewardicon`;
CREATE TABLE `common_rewardicon`  (
  `ri_id` int NOT NULL AUTO_INCREMENT,
  `ri_icon` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `ri_style` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '图标颜色',
  PRIMARY KEY (`ri_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '奖励图标' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_sale
-- ----------------------------
DROP TABLE IF EXISTS `common_sale`;
CREATE TABLE `common_sale`  (
  `sa_id` int NOT NULL AUTO_INCREMENT,
  `sa_model` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sa_scope` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sa_rule` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sa_remark` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sa_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 346 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_sellprint
-- ----------------------------
DROP TABLE IF EXISTS `common_sellprint`;
CREATE TABLE `common_sellprint`  (
  `sp_id` int NOT NULL AUTO_INCREMENT,
  `sp_key` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '零售设置表kEY',
  `sp_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '零售打印表的名称',
  PRIMARY KEY (`sp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_sellset
-- ----------------------------
DROP TABLE IF EXISTS `common_sellset`;
CREATE TABLE `common_sellset`  (
  `ss_id` int NOT NULL AUTO_INCREMENT,
  `ss_key` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '零售设置表kEY',
  `ss_value` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '零售设置表VALUE',
  `ss_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ss_type` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '类型：SHOP前台设置CASH:收银员设置',
  PRIMARY KEY (`ss_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_todo
-- ----------------------------
DROP TABLE IF EXISTS `common_todo`;
CREATE TABLE `common_todo`  (
  `td_id` int NOT NULL AUTO_INCREMENT,
  `td_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '待办事项名称',
  `td_url` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT 'url',
  `td_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态：0-正常，1-停用',
  `td_version` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '版本号1:总代理2:连锁店3:自营店',
  `td_shop_type` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '角色类型1:总公司2:分公司3:自营店4:加盟店5:合伙店',
  `td_style` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '样式名称',
  `td_icon` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '图标位置样式',
  `td_identity` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '标识',
  PRIMARY KEY (`td_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_type
-- ----------------------------
DROP TABLE IF EXISTS `common_type`;
CREATE TABLE `common_type`  (
  `ty_id` tinyint(1) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ty_name` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ty_type` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '类型能看到哪些下属的类型',
  `ty_sub` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '子级的类型',
  `ty_state` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '新增用户时用来区分能选择哪些角色的',
  `ty_ver` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '2' COMMENT '版本 4_大众版 3_基础版 2_连锁版 1_总代版',
  `ty_one` tinyint(1) NULL DEFAULT 1 COMMENT '总部下的类型',
  `ty_two` tinyint(1) NULL DEFAULT 2 COMMENT '分公司下的类型',
  `ty_three` tinyint(1) NULL DEFAULT 1 COMMENT '只查分部下自营或合伙店',
  `ty_five` tinyint(1) NULL DEFAULT 2 COMMENT '只查分公司下的自营与合伙',
  PRIMARY KEY (`ty_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_useable
-- ----------------------------
DROP TABLE IF EXISTS `common_useable`;
CREATE TABLE `common_useable`  (
  `ua_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ua_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ua_name` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ua_ver` varchar(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '3' COMMENT '版本信息 1:高级版2:综合版3:标准版4:普及版',
  `ua_state` tinyint(1) NULL DEFAULT 0 COMMENT '是否启用 0:否 1:是',
  `ua_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ua_type` tinyint(1) NULL DEFAULT 0 COMMENT '计算方式',
  PRIMARY KEY (`ua_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for common_weather
-- ----------------------------
DROP TABLE IF EXISTS `common_weather`;
CREATE TABLE `common_weather`  (
  `we_id` tinyint NOT NULL,
  `we_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `we_icon` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`we_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for my_log
-- ----------------------------
DROP TABLE IF EXISTS `my_log`;
CREATE TABLE `my_log`  (
  `lg_id` int NOT NULL AUTO_INCREMENT,
  `lg_user` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `lg_content` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `lg_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`lg_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for operate_log
-- ----------------------------
DROP TABLE IF EXISTS `operate_log`;
CREATE TABLE `operate_log`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `operator` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_approve_record
-- ----------------------------
DROP TABLE IF EXISTS `t_approve_record`;
CREATE TABLE `t_approve_record`  (
  `ar_id` int NOT NULL AUTO_INCREMENT COMMENT '审批表，主键字段自增',
  `ar_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `ar_state` tinyint(1) NULL DEFAULT NULL COMMENT '审核状态:1-审核通过,2-审核未通过,3-反审核,4-终止,5-发货，6-拒收，7-完成',
  `ar_describe` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '操作说明，单据打回或不同意时必须填写',
  `ar_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `ar_type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '审核单据类型,存放需要审核表格的名称',
  `ar_us_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '操作人名称',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`ar_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19332 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_affiliate
-- ----------------------------
DROP TABLE IF EXISTS `t_base_affiliate`;
CREATE TABLE `t_base_affiliate`  (
  `af_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `af_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分公司编号',
  `af_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分公司名称',
  `af_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `af_man` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '',
  `af_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `af_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `af_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  `af_upcode` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级编号',
  PRIMARY KEY (`af_id`) USING BTREE,
  INDEX `Index_Rg_Code`(`af_code` ASC) USING BTREE,
  INDEX `Index_Rg_EI_Id`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 409 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_area
-- ----------------------------
DROP TABLE IF EXISTS `t_base_area`;
CREATE TABLE `t_base_area`  (
  `ar_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ar_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '地区编号',
  `ar_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '地区名称',
  `ar_upcode` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '上级编号',
  `ar_spell` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '拼音码',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`ar_id`) USING BTREE,
  INDEX `PK_ar_id`(`ar_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 400 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_barcode
-- ----------------------------
DROP TABLE IF EXISTS `t_base_barcode`;
CREATE TABLE `t_base_barcode`  (
  `bc_id` bigint NOT NULL AUTO_INCREMENT,
  `bc_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `bc_pd_no` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品货号',
  `bc_size` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '尺码',
  `bc_bra` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型',
  `bc_color` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '颜色',
  `bc_subcode` varchar(28) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '子码',
  `bc_sys_barcode` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '系统条码',
  `bc_barcode` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '条形码',
  `companyid` int NOT NULL,
  PRIMARY KEY (`bc_id`) USING BTREE,
  INDEX `IDX_BC_COM_ID`(`bc_id` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_BC_COM_PDCODE`(`bc_pd_code` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_BC_COM_PDNO`(`bc_pd_no` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_BC_COM_SIZE`(`bc_size` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_BC_COM_BRA`(`bc_bra` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_BC_COM_COLOR`(`bc_color` ASC, `companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103066 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_barcode_set
-- ----------------------------
DROP TABLE IF EXISTS `t_base_barcode_set`;
CREATE TABLE `t_base_barcode_set`  (
  `bs_id` int NOT NULL AUTO_INCREMENT,
  `bs_paper_width` decimal(9, 2) NULL DEFAULT NULL COMMENT '条形码纸宽度',
  `bs_paper_height` decimal(9, 2) NULL DEFAULT NULL COMMENT '条形码纸高度',
  `bs_top_backgauge` decimal(9, 2) NULL DEFAULT NULL COMMENT '整页上边距',
  `bs_left_backgauge` decimal(9, 2) NULL DEFAULT NULL COMMENT '整页左边距',
  `bs_bartype` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '条形码类型',
  `bs_line` int NULL DEFAULT 1 COMMENT '打印列数',
  `bs_hight_spacing` decimal(9, 2) NULL DEFAULT NULL COMMENT '条形码高间距',
  `bs_width_spacing` decimal(9, 2) NULL DEFAULT NULL COMMENT '条形码宽间距',
  `bs_width` decimal(9, 2) NULL DEFAULT NULL COMMENT '打印条码宽度',
  `bs_height` decimal(9, 2) NULL DEFAULT NULL COMMENT '打印条码高度',
  `bs_p_space` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '打印商品信息间距',
  `bs_ps_space` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '打印商品和条码间距',
  `bs_font_name` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '宋体' COMMENT '打印字体',
  `bs_font_size` int NULL DEFAULT 9 COMMENT '打印字体大小',
  `bs_bold` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '1代表粗体，0代表非粗体',
  `bs_pdname_ifshow` int NULL DEFAULT 0 COMMENT '打印品名是否显示0:不显示 1:显示',
  `bs_pdname_row` int NULL DEFAULT 0 COMMENT '打印品名显示行号',
  `bs_class_ifshow` int NULL DEFAULT 0 COMMENT '打印等级是否显示0:不显示 1:显示',
  `bs_class_row` int NULL DEFAULT 0 COMMENT '打印等级显示行号',
  `bs_safestandard_ifshow` int NULL DEFAULT 0 COMMENT '打印标准是否显示0:不显示 1:显示',
  `bs_safestandard_row` int NULL DEFAULT 0 COMMENT '打印标准显示行号',
  `bs_filling_ifshow` int NULL DEFAULT 0 COMMENT '打印成分是否显示0:不显示 1:显示',
  `bs_filling_row` int NULL DEFAULT 0 COMMENT '打印成分显示行号',
  `bs_cr_ifshow` int NULL DEFAULT 0 COMMENT '打印颜色是否显示0:不显示 1:显示',
  `bs_cr_row` int NULL DEFAULT 0 COMMENT '打印颜色显示行号',
  `bs_pdno_ifshow` int NULL DEFAULT 0 COMMENT '打印货号是否显示0:不显示 1:显示',
  `bs_pdno_row` int NULL DEFAULT 0 COMMENT '打印货号显示行号',
  `bs_sz_ifshow` int NULL DEFAULT 0 COMMENT '打印尺码是否显示0:不显示 1:显示',
  `bs_sz_row` int NULL DEFAULT 0 COMMENT '打印尺码显示行号',
  `bs_bs_ifshow` int NULL DEFAULT 0 COMMENT '打印杯型是否显示0:不显示 1:显示',
  `bs_bs_row` int NULL DEFAULT 0 COMMENT '打印杯型显示行号',
  `bs_bd_ifshow` int NULL DEFAULT 0 COMMENT '打印品牌是否显示0:不显示 1:显示',
  `bs_bd_row` int NULL DEFAULT 0 COMMENT '打印品牌显示行号',
  `bs_price_ifshow` int NULL DEFAULT 0 COMMENT '打印价格是否显示0:不显示 1:显示',
  `bs_price_row` int NULL DEFAULT 0 COMMENT '打印价格显示行号',
  `bs_show_fiename` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '是否显示字段名称0:不显示 1:显示',
  `bs_sysdate` datetime NULL DEFAULT NULL COMMENT '设置时间',
  `bs_plan_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '方案1' COMMENT '条码设置方案名称',
  `bs_plan_state` tinyint(1) NULL DEFAULT 1 COMMENT '状态 0:不启用 1:启用',
  `bs_subcode_showup` int NULL DEFAULT 0 COMMENT '上方显示条形码',
  `bs_price_showdown` int NULL DEFAULT 0 COMMENT '下方显示价格',
  `bs_executionstandard_ifshow` int NULL DEFAULT 0 COMMENT '打印执行标准是否显示0:不显示 1:显示',
  `bs_executionstandard_row` int NULL DEFAULT 0 COMMENT '打印执行标准显示行号',
  `bs_language` int NULL DEFAULT 0 COMMENT '0中文，1英文',
  `bs_vipprice_ifshow` int NULL DEFAULT NULL COMMENT '会员价0:不显示 1:显示',
  `bs_vipprice_row` int NULL DEFAULT NULL COMMENT '会员价显示第几行',
  `bs_signprice_ifshow` int NULL DEFAULT NULL COMMENT '标牌价0:不显示 1:显示',
  `bs_signprice_row` int NULL DEFAULT NULL COMMENT '标牌价显示第几行',
  `companyid` int NOT NULL COMMENT '商家编号',
  `bs_infabric_ifshow` int NULL DEFAULT NULL COMMENT '里料0:不显示 1:显示',
  `bs_infabric_row` int NULL DEFAULT NULL COMMENT '里料显示第几行',
  `bs_type_ifshow` int NULL DEFAULT NULL COMMENT '商品类别0:不显示 1:显示',
  `bs_type_row` int NULL DEFAULT NULL COMMENT '类别显示第几行',
  `bs_place_ifshow` int NULL DEFAULT NULL COMMENT '产地0:不显示 1:显示',
  `bs_place_row` int NULL DEFAULT NULL COMMENT '产地显示第几行',
  `bs_fabric_ifshow` int NULL DEFAULT NULL COMMENT '面料0:不显示 1:显示',
  `bs_fabric_row` int NULL DEFAULT NULL COMMENT '面料显示第几行',
  `bs_washexplain_ifshow` int NULL DEFAULT NULL COMMENT '洗涤说明0:不显示 1:显示',
  `bs_washexplain_row` int NULL DEFAULT NULL COMMENT '洗涤说明显示第几行',
  PRIMARY KEY (`bs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_bra
-- ----------------------------
DROP TABLE IF EXISTS `t_base_bra`;
CREATE TABLE `t_base_bra`  (
  `br_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `br_code` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '尺码编号',
  `br_name` varchar(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '尺码名称',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`br_id`) USING BTREE,
  INDEX `IDX_BR_CO_BR`(`companyid` ASC, `br_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 392 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_brand
-- ----------------------------
DROP TABLE IF EXISTS `t_base_brand`;
CREATE TABLE `t_base_brand`  (
  `bd_id` int NOT NULL AUTO_INCREMENT,
  `bd_code` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '品牌编号',
  `bd_name` char(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '品牌名称',
  `bd_spell` char(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '拼音简码',
  `companyid` int NOT NULL COMMENT '商家ID',
  `bd_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0正常1停用',
  PRIMARY KEY (`bd_id`) USING BTREE,
  INDEX `IDX_BD_CO_BD`(`companyid` ASC, `bd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 671 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_color
-- ----------------------------
DROP TABLE IF EXISTS `t_base_color`;
CREATE TABLE `t_base_color`  (
  `cr_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cr_code` char(6) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '颜色编号',
  `cr_name` varchar(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '颜色名称',
  `cr_spell` varchar(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '拼音简码，颜色名称首字母缩写',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`cr_id`) USING BTREE,
  INDEX `IDX_CR_CO_CODE`(`companyid` ASC, `cr_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7424 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_department
-- ----------------------------
DROP TABLE IF EXISTS `t_base_department`;
CREATE TABLE `t_base_department`  (
  `dm_id` int NOT NULL AUTO_INCREMENT,
  `dm_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dm_name` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dm_spell` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dm_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺code',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`dm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_depot
-- ----------------------------
DROP TABLE IF EXISTS `t_base_depot`;
CREATE TABLE `t_base_depot`  (
  `dp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dp_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '仓库编号，从001开始编号',
  `dp_name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '仓库名称',
  `dp_spell` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '简拼',
  `dp_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '仓库电话',
  `dp_man` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '联系人',
  `dp_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态0:正常1:停用',
  `dp_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '仓库地址',
  `dp_default` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1:主仓库 0:从仓库  一个门店只有一个主仓库',
  `dp_mobile` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `dp_remark` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `dp_shop_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '店铺编号',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`dp_id`) USING BTREE,
  INDEX `IDX_DP_CO_SP_CODE`(`companyid` ASC, `dp_shop_code` ASC, `dp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 228 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_base_dict`;
CREATE TABLE `t_base_dict`  (
  `dt_id` int NOT NULL AUTO_INCREMENT,
  `dt_code` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `dt_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`dt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_dictlist
-- ----------------------------
DROP TABLE IF EXISTS `t_base_dictlist`;
CREATE TABLE `t_base_dictlist`  (
  `dtl_id` int NOT NULL AUTO_INCREMENT,
  `dtl_code` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `dtl_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `dtl_upcode` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `companyid` int NOT NULL,
  PRIMARY KEY (`dtl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 797 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_emp
-- ----------------------------
DROP TABLE IF EXISTS `t_base_emp`;
CREATE TABLE `t_base_emp`  (
  `em_id` int NOT NULL AUTO_INCREMENT,
  `em_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `em_name` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `em_sex` varchar(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '男/女',
  `em_marry` varchar(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '已婚/未婚',
  `em_cardid` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_dm_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_addr` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_birthday` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0在职1离职',
  `em_workdate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_indate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_outdate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `em_login_shop` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '登录的店铺编号',
  `em_type` tinyint(1) NULL DEFAULT 0 COMMENT '0导购员1业务员2职员3店长',
  `em_img` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_pass` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_royaltyrate` double(5, 2) NULL DEFAULT NULL COMMENT '提成率',
  `companyid` int NOT NULL,
  PRIMARY KEY (`em_id`) USING BTREE,
  INDEX `IDX_EM_CO_SHOP_EM`(`companyid` ASC, `em_shop_code` ASC, `em_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 463 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_emp_reward
-- ----------------------------
DROP TABLE IF EXISTS `t_base_emp_reward`;
CREATE TABLE `t_base_emp_reward`  (
  `er_id` int NOT NULL AUTO_INCREMENT,
  `er_em_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '员工编号',
  `er_rw_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '奖励编号',
  `er_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `er_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否兑换 0 未兑换 1 已兑换',
  `er_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`er_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_emp_wage
-- ----------------------------
DROP TABLE IF EXISTS `t_base_emp_wage`;
CREATE TABLE `t_base_emp_wage`  (
  `emw_id` int NOT NULL,
  `emw_em_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `emw_wage` decimal(9, 2) NULL DEFAULT NULL COMMENT '基本工资',
  `emw_rate` double(5, 2) NULL DEFAULT NULL COMMENT '提成率',
  `emw_state` tinyint(1) NULL DEFAULT 0 COMMENT '工资发放：0正常1停发',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`emw_id`) USING BTREE,
  INDEX `IDX_EMW_CO_EM`(`companyid` ASC, `emw_em_code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_empgroup
-- ----------------------------
DROP TABLE IF EXISTS `t_base_empgroup`;
CREATE TABLE `t_base_empgroup`  (
  `eg_id` int NOT NULL AUTO_INCREMENT,
  `eg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '小组编号',
  `eg_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '小组名称',
  `eg_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `eg_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '创建店铺',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`eg_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '员工组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_empgrouplist
-- ----------------------------
DROP TABLE IF EXISTS `t_base_empgrouplist`;
CREATE TABLE `t_base_empgrouplist`  (
  `egl_id` int NOT NULL AUTO_INCREMENT,
  `egl_eg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '小组编号',
  `egl_em_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '员工编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`egl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '员工组员工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product`;
CREATE TABLE `t_base_product`  (
  `pd_id` bigint NOT NULL AUTO_INCREMENT,
  `pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '编号',
  `pd_no` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '货号',
  `pd_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '款号(不用于关联，只显示)',
  `pd_name` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '名称',
  `pd_spell` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '简码',
  `pd_szg_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '尺码组编号',
  `pd_tp_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '类别编号',
  `pd_tp_upcode` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级类别',
  `pd_unit` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单位名称',
  `pd_bd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '品牌编号',
  `pd_season` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '季节名称',
  `pd_style` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '款式名称',
  `pd_year` int NOT NULL COMMENT '年份名称',
  `pd_cost_price` decimal(8, 2) NOT NULL DEFAULT 0.00 COMMENT '成本价',
  `pd_sell_cycle` int NULL DEFAULT 0 COMMENT '零售周期',
  `pd_sell_price` decimal(8, 2) NOT NULL COMMENT '零售价',
  `pd_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态0正常1停用2淘汰',
  `pd_fabric` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '面料',
  `pd_bra` tinyint(1) NULL DEFAULT 0 COMMENT '内衣0否1是',
  `pd_change` tinyint(1) NULL DEFAULT 0 COMMENT '分店变价0否1是',
  `pd_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上市时间',
  `pd_returndate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '退换货日期',
  `pd_commission` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '导购员提成金额',
  `pd_commission_rate` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '导购员提成率',
  `pd_present` tinyint(1) NULL DEFAULT 0 COMMENT '礼品0否1是',
  `pd_score` int NULL DEFAULT 0 COMMENT '兑换积分',
  `pd_sign_price` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '标牌价',
  `pd_vip_price` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '会员价格',
  `pd_vip_sale` tinyint(1) NULL DEFAULT 1 COMMENT '会员是否打折0否1是',
  `pd_gift` tinyint(1) NOT NULL DEFAULT 0 COMMENT '赠品0否1是',
  `pd_point` tinyint(1) NOT NULL DEFAULT 1 COMMENT '积分0否1是',
  `pd_sale` tinyint(1) NOT NULL DEFAULT 0 COMMENT '手动打折0否1是',
  `pd_sp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '供应商编号',
  `pd_buy_cycle` smallint NULL DEFAULT 15 COMMENT '进货周期',
  `pd_buy_price` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '进货价格',
  `pd_buy_date` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '最后进货时间',
  `pd_buy` tinyint(1) NOT NULL DEFAULT 1 COMMENT '允许采购0否1是',
  `pd_sort_price` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '配送价格',
  `pd_batch_price` decimal(8, 2) NULL DEFAULT NULL,
  `pd_batch_price1` decimal(8, 2) NULL DEFAULT NULL,
  `pd_batch_price2` decimal(8, 2) NULL DEFAULT NULL,
  `pd_batch_price3` decimal(8, 2) NULL DEFAULT NULL,
  `companyid` int(4) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`pd_id`) USING BTREE,
  INDEX `IDX_PD_COM_NO`(`pd_no` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_PD_COM_SZGCODE`(`pd_szg_code` ASC, `companyid` ASC) USING BTREE,
  INDEX `IDX_PD_CO_BD_TP`(`companyid` ASC, `pd_bd_code` ASC, `pd_tp_code` ASC) USING BTREE,
  INDEX `IDX_PD_CO_PD_BD`(`companyid` ASC, `pd_code` ASC, `pd_bd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65015 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_assist
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_assist`;
CREATE TABLE `t_base_product_assist`  (
  `pda_id` int NOT NULL AUTO_INCREMENT COMMENT '商品资料辅助资料模板',
  `pda_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '辅助资料模板名称',
  `pda_fill` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '成分',
  `pda_color` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '相色',
  `pda_size` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '规格',
  `pda_place` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '产地',
  `pda_model` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '型号',
  `pda_safe` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '安全标准',
  `pda_execute` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '执行标准',
  `pda_grade` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '等级',
  `pda_price_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '价格特性',
  `pda_salesman_comm` decimal(8, 2) NULL DEFAULT NULL COMMENT '业务提成金额',
  `pda_salesman_commrate` decimal(8, 2) NULL DEFAULT NULL COMMENT '业务员提成率',
  `pda_gbcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '国标码',
  `pda_wash_explain` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '洗涤说明',
  `pda_sale` tinyint(1) NULL DEFAULT 0 COMMENT '手动打折0否1是',
  `pda_change` tinyint(1) NULL DEFAULT 0 COMMENT '分店变价0否1是',
  `pda_gift` tinyint(1) NULL DEFAULT 0 COMMENT '赠品0否1是',
  `pda_vip_sale` tinyint(1) NULL DEFAULT 1 COMMENT '会员是否打折0否1是',
  `pda_present` tinyint(1) NULL DEFAULT 0 COMMENT '礼品0否1是',
  `pda_buy` tinyint(1) NULL DEFAULT 1 COMMENT '允许采购0否1是',
  `pda_point` tinyint(1) NULL DEFAULT 1 COMMENT '积分0否1是',
  `pda_batch_price1` decimal(8, 2) NULL DEFAULT NULL,
  `pda_batch_price2` decimal(8, 2) NULL DEFAULT NULL,
  `pda_batch_price3` decimal(8, 2) NULL DEFAULT NULL,
  `companyid` int(4) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`pda_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_br
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_br`;
CREATE TABLE `t_base_product_br`  (
  `pdb_id` bigint NOT NULL AUTO_INCREMENT,
  `pdb_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `pdb_br_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '杯型编号',
  `companyid` int NOT NULL,
  PRIMARY KEY (`pdb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1206 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_color
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_color`;
CREATE TABLE `t_base_product_color`  (
  `pdc_id` bigint NOT NULL AUTO_INCREMENT,
  `pdc_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `pdc_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品颜色',
  `companyid` int NOT NULL,
  PRIMARY KEY (`pdc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 130086 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_img
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_img`;
CREATE TABLE `t_base_product_img`  (
  `pdm_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pdm_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `pdm_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色编号',
  `pdm_img_name` varchar(80) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图片名称',
  `pdm_all_path` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图片全路径',
  `pdm_img_path` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `pdm_state` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '0:正常使用 1:停用',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`pdm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 998 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_info
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_info`;
CREATE TABLE `t_base_product_info`  (
  `pd_id` bigint NOT NULL AUTO_INCREMENT,
  `pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `pd_sysdate` datetime NULL DEFAULT NULL COMMENT '生成时间',
  `pd_fill` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '填充物',
  `pd_color` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '相色',
  `pd_size` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '规格',
  `pd_place` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '产地',
  `pd_in_fabric` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '里料',
  `pd_model` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '型号',
  `pd_safe` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '安全标准',
  `pd_execute` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '执行标准',
  `pd_grade` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '等级',
  `pd_add_manager` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人（存中文）',
  `pd_add_date` date NULL DEFAULT NULL COMMENT '创建日期',
  `pd_mod_manager` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '修改人（中文）',
  `pd_mod_date` date NULL DEFAULT NULL COMMENT '修改日期',
  `pd_price_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '价格特性',
  `pd_salesman_comm` decimal(8, 2) NULL DEFAULT NULL COMMENT '业务提成金额',
  `pd_salesman_commrate` decimal(8, 2) NULL DEFAULT NULL COMMENT '业务员提成率',
  `pd_gbcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '国标码',
  `pd_wash_explain` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '洗涤说明',
  `companyid` int NOT NULL,
  PRIMARY KEY (`pd_id`) USING BTREE,
  INDEX `IDX_PDI_COM_PD`(`companyid` ASC, `pd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64899 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_main_bak
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_main_bak`;
CREATE TABLE `t_base_product_main_bak`  (
  `pd_id` bigint NOT NULL AUTO_INCREMENT,
  `pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `pd_sp_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '供应商编号',
  `pd_buy_cycle` int NULL DEFAULT NULL COMMENT '进货周期',
  `pd_buy_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '进货价格',
  `pd_buy_date` date NULL DEFAULT NULL COMMENT '最后进货时间',
  `pd_buy` tinyint(1) NULL DEFAULT 1 COMMENT '允许采购0否1是',
  `pd_sort_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '配送价格',
  `pd_batch_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '批发价',
  `pd_batch_price1` decimal(8, 2) NULL DEFAULT NULL COMMENT '批发价1',
  `pd_batch_price2` decimal(8, 2) NULL DEFAULT NULL COMMENT '批发价2',
  `pd_batch_price3` decimal(8, 2) NULL DEFAULT NULL COMMENT '批发价3',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`pd_id`) USING BTREE,
  INDEX `IDX_PDM_CO_PD`(`companyid` ASC, `pd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_sell_bak
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_sell_bak`;
CREATE TABLE `t_base_product_sell_bak`  (
  `pd_id` bigint NOT NULL AUTO_INCREMENT,
  `pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `pd_sign_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '标牌价',
  `pd_vip_price` decimal(8, 2) NULL DEFAULT 0.00 COMMENT '会员价格',
  `pd_vip_sale` tinyint(1) NULL DEFAULT 1 COMMENT '会员折扣',
  `pd_gift` tinyint(1) NULL DEFAULT 0 COMMENT '赠品0否1是',
  `pd_point` tinyint(1) NULL DEFAULT 1 COMMENT '积分0否1是',
  `pd_sale` tinyint(1) NULL DEFAULT 1 COMMENT '手动打折0否1是',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`pd_id`) USING BTREE,
  INDEX `IDX_PDS_CO_PD`(`companyid` ASC, `pd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_product_shop_price
-- ----------------------------
DROP TABLE IF EXISTS `t_base_product_shop_price`;
CREATE TABLE `t_base_product_shop_price`  (
  `pdp_id` bigint NOT NULL AUTO_INCREMENT,
  `pdp_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `pdp_pd_point` tinyint(1) NULL DEFAULT NULL COMMENT '积分0否1是',
  `pdp_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分店编号',
  `pdp_cost_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '分店成本价',
  `pdp_sell_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '分店零售价',
  `pdp_vip_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '分店会员价',
  `pdp_sort_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '分店配送价',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`pdp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8566 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_base_shop`;
CREATE TABLE `t_base_shop`  (
  `sp_id` int NOT NULL AUTO_INCREMENT,
  `sp_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sp_name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sp_spell` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '简码',
  `sp_shop_type` tinyint(1) NOT NULL COMMENT '店铺类型ID',
  `sp_state` tinyint(1) NOT NULL COMMENT '0正常1停用',
  `sp_end` date NOT NULL COMMENT '有效期',
  `sp_lock` tinyint(1) NULL DEFAULT 0 COMMENT '加密狗0否1是',
  `sp_upcode` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '上级编号：只能是总部和分公司',
  `sp_rate` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '折扣率',
  `sp_init` tinyint(1) NOT NULL DEFAULT 0 COMMENT '初始化0否1是',
  `sp_main` tinyint(1) NULL DEFAULT 0 COMMENT '查看总仓0否1是',
  `sp_init_debt` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '期初欠款',
  `sp_receivable` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '应收款',
  `sp_received` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '已收款',
  `sp_prepay` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '预收金额',
  `sp_sort_cycle` tinyint(1) NULL DEFAULT 7 COMMENT '配送周期',
  `sp_settle_cycle` tinyint(1) NULL DEFAULT 15 COMMENT '结算周期',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`sp_id`) USING BTREE,
  INDEX `IDX_SP_CO_SP_CODE_TY`(`companyid` ASC, `sp_upcode` ASC, `sp_code` ASC, `sp_shop_type` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 187 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_shop_info
-- ----------------------------
DROP TABLE IF EXISTS `t_base_shop_info`;
CREATE TABLE `t_base_shop_info`  (
  `spi_id` int NOT NULL AUTO_INCREMENT,
  `spi_man` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_mobile` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_province` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '省',
  `spi_city` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '市',
  `spi_town` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '县区',
  `spi_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `spi_shop_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `companyid` int NOT NULL,
  PRIMARY KEY (`spi_id`) USING BTREE,
  INDEX `IDX_SPI_CO_SP`(`companyid` ASC, `spi_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 181 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_shop_line
-- ----------------------------
DROP TABLE IF EXISTS `t_base_shop_line`;
CREATE TABLE `t_base_shop_line`  (
  `spl_id` int NOT NULL AUTO_INCREMENT,
  `spl_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `spl_ordershop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '收单店铺编号',
  `spl_project` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '主营项目',
  `spl_logo` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'logo',
  `spl_office_hour` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '营业时间',
  `spl_longitude` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺地理位置-经度',
  `spl_latitude` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺地理位置-纬度',
  `spl_state` tinyint NULL DEFAULT NULL COMMENT '开通状态：0-提交申请，暂未开通；1开通,2审核未通过',
  `spl_sysdate` datetime NULL DEFAULT NULL COMMENT '开通时间',
  `companyid` int NOT NULL,
  PRIMARY KEY (`spl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_shop_money_bak
-- ----------------------------
DROP TABLE IF EXISTS `t_base_shop_money_bak`;
CREATE TABLE `t_base_shop_money_bak`  (
  `spm_id` int NOT NULL,
  `spm_shop_code` int NOT NULL,
  `spm_init_debt` double(9, 2) NOT NULL DEFAULT 0.00 COMMENT '期初欠款',
  `spm_receivable` double(9, 2) NOT NULL COMMENT '应收款',
  `spm_received` double(9, 2) NOT NULL COMMENT '已收款',
  `spm_debt` double(9, 2) NOT NULL COMMENT '实际欠款',
  `spm_sms_money` double(9, 2) NOT NULL DEFAULT 0.00,
  `spm_prepay` double(9, 2) NOT NULL DEFAULT 0.00 COMMENT '预收金额',
  `spm_sort_cycle` tinyint NULL DEFAULT 7 COMMENT '配送周期',
  `spm_settle_cycle` tinyint NULL DEFAULT 15 COMMENT '结算周期',
  `companyid` int NOT NULL,
  PRIMARY KEY (`spm_id`) USING BTREE,
  INDEX `IDX_SPF_CO_SP`(`companyid` ASC, `spm_shop_code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_shop_reward
-- ----------------------------
DROP TABLE IF EXISTS `t_base_shop_reward`;
CREATE TABLE `t_base_shop_reward`  (
  `sr_id` int NOT NULL AUTO_INCREMENT,
  `sr_sp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `sr_rw_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '奖励编号',
  `sr_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `sr_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否兑换 0 未兑换 1 已兑换',
  `sr_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`sr_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_size
-- ----------------------------
DROP TABLE IF EXISTS `t_base_size`;
CREATE TABLE `t_base_size`  (
  `sz_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sz_code` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '尺码编号',
  `sz_name` varchar(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '尺码名称',
  `sz_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0正常1停用',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sz_id`) USING BTREE,
  INDEX `IDX_SZ_CO_SZ`(`companyid` ASC, `sz_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1174 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_size_group
-- ----------------------------
DROP TABLE IF EXISTS `t_base_size_group`;
CREATE TABLE `t_base_size_group`  (
  `szg_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `szg_code` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '尺码编号',
  `szg_name` varchar(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '尺码名称',
  `szg_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0正常1停用',
  `szg_number` tinyint NOT NULL DEFAULT 1 COMMENT '尺码组个数',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`szg_id`) USING BTREE,
  INDEX `IDX_SZ_CO_SZ`(`companyid` ASC, `szg_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 791 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_sizelist
-- ----------------------------
DROP TABLE IF EXISTS `t_base_sizelist`;
CREATE TABLE `t_base_sizelist`  (
  `szl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `szl_szg_code` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `szl_sz_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '尺码编号',
  `szl_order` smallint NULL DEFAULT NULL COMMENT '排序编号',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`szl_id`) USING BTREE,
  INDEX `IDX_SZL_CO_SZ`(`companyid` ASC, `szl_sz_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4963 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_stream
-- ----------------------------
DROP TABLE IF EXISTS `t_base_stream`;
CREATE TABLE `t_base_stream`  (
  `se_id` int NOT NULL AUTO_INCREMENT,
  `se_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '物流公司编号',
  `se_name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '物流公司名称',
  `se_spell` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '简拼',
  `se_man` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `se_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `se_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系地址',
  `se_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `se_init_debt` decimal(11, 2) NULL DEFAULT NULL COMMENT '期初欠款',
  `se_payable` decimal(11, 2) NULL DEFAULT NULL COMMENT '应付金额',
  `se_payabled` decimal(11, 2) NULL DEFAULT NULL COMMENT '已付金额',
  `se_sp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '上级店铺编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`se_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_stream_bill
-- ----------------------------
DROP TABLE IF EXISTS `t_base_stream_bill`;
CREATE TABLE `t_base_stream_bill`  (
  `seb_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `seb_se_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '物流公司编号',
  `seb_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `seb_date` date NULL DEFAULT NULL COMMENT '日期',
  `seb_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总计金额',
  `seb_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `seb_payable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应付金额',
  `seb_payabled` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `seb_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `seb_join_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '关联编号',
  `seb_type` tinyint(1) NULL DEFAULT NULL COMMENT '单据类型：0-期初单，1-批发物流单',
  `seb_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`seb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_base_type
-- ----------------------------
DROP TABLE IF EXISTS `t_base_type`;
CREATE TABLE `t_base_type`  (
  `tp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tp_code` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '分类编号',
  `tp_name` varchar(26) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '商品类别名称',
  `tp_spell` varchar(13) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '拼音码',
  `tp_upcode` char(5) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '0' COMMENT '上级编号ID',
  `tp_rate` double(5, 2) NOT NULL DEFAULT 0.00 COMMENT '导购提成率',
  `tp_salerate` double(5, 2) NOT NULL DEFAULT 0.00 COMMENT '业务员提成率',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`tp_id`) USING BTREE,
  INDEX `Ct_Id`(`tp_id` ASC) USING BTREE,
  INDEX `Index_Ct_Code`(`tp_code` ASC) USING BTREE,
  INDEX `Index_Ct_EI_Id_Code`(`companyid` ASC, `tp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2691 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_client
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_client`;
CREATE TABLE `t_batch_client`  (
  `ci_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ci_code` varchar(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '客户编号',
  `ci_name` varchar(40) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '客户名称',
  `ci_spell` varchar(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '简拼',
  `ci_area` varchar(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '地区',
  `ci_rate` decimal(5, 2) NULL DEFAULT 1.00 COMMENT '折扣',
  `ci_init_debt` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '期初欠款',
  `ci_receivable` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '应收金额',
  `ci_received` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '已收金额',
  `ci_prepay` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '预付款余额',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  `ci_default` tinyint(1) NOT NULL DEFAULT 0 COMMENT '批发价：0\r\n批发价1：1\r\n批发价2：2\r\n批发价3：3\r\n批发价4：4\r\n批发价5：5\r\n批发价6：6',
  `ci_sp_code` varchar(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '上级店铺编号',
  `ci_lastdate` datetime NULL DEFAULT NULL COMMENT '最近批发日期',
  `ci_batch_cycle` tinyint NULL DEFAULT NULL COMMENT '批发周期',
  `ci_settle_cycle` tinyint NULL DEFAULT NULL COMMENT '结帐周期',
  PRIMARY KEY (`ci_id`) USING BTREE,
  INDEX `IDX_CI_CO_CI`(`companyid` ASC, `ci_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_client_info
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_client_info`;
CREATE TABLE `t_batch_client_info`  (
  `cii_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ci_code` varchar(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '客户编号',
  `ci_addr` varchar(50) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '地址',
  `ci_tel` varchar(13) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '电话',
  `ci_man` varchar(16) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '联系人',
  `ci_mobile` varchar(13) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '手机',
  `ci_bank_open` varchar(40) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '开户银行',
  `ci_bank_code` varchar(25) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '银行帐号',
  `ci_remark` varchar(100) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '备注',
  `ci_earnest` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '保证金',
  `ci_deposit` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '装修押金',
  `ci_use_credit` tinyint(1) NULL DEFAULT 0 COMMENT '开启信誉额度0：否 1：是',
  `ci_credit_limit` decimal(11, 2) NULL DEFAULT NULL COMMENT '信誉额度',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`cii_id`) USING BTREE,
  INDEX `Index_Ci_Code`(`ci_code` ASC) USING BTREE,
  INDEX `Index_Ci_EI_Id`(`companyid` ASC) USING BTREE,
  INDEX `Ci_Id`(`cii_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_client_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_client_shop`;
CREATE TABLE `t_batch_client_shop`  (
  `cis_id` int NOT NULL AUTO_INCREMENT COMMENT '客户店铺id',
  `cis_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '客户店铺编号',
  `cis_ci_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '客户编号',
  `cis_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺名称',
  `cis_spell` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '简拼',
  `cis_linkman` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `cis_link_tel` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '座机电话',
  `cis_link_mobile` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `cis_link_adr` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺地址',
  `cis_stutas` tinyint(1) NULL DEFAULT 0 COMMENT '状态 0启用 1停用',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`cis_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_clientprice
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_clientprice`;
CREATE TABLE `t_batch_clientprice`  (
  `cp_id` int NOT NULL AUTO_INCREMENT,
  `cp_pd_code` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `cp_ci_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '客户编号',
  `cp_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '批发价格',
  `cp_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`cp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4086 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_dealings
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_dealings`;
CREATE TABLE `t_batch_dealings`  (
  `dl_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dl_client_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供应商编号',
  `dl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `dl_type` tinyint(1) NULL DEFAULT NULL COMMENT '类型：0-批发出库；1-退货入库；2-期初；3-费用单；4-预收款；5-预收款退款；6-结算单；',
  `dl_discount_money` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `dl_receivable` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `dl_received` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `dl_debt` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '欠款',
  `dl_date` datetime NULL DEFAULT NULL COMMENT '日期',
  `dl_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作员',
  `dl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `dl_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `dl_batchamount` int NULL DEFAULT NULL COMMENT '数量',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`dl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 676 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_fee
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_fee`;
CREATE TABLE `t_batch_fee`  (
  `fe_id` int NOT NULL AUTO_INCREMENT,
  `fe_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `fe_client_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '批发客户编号',
  `fe_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `fe_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `fe_date` date NULL DEFAULT NULL COMMENT '日期',
  `fe_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `fe_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `fe_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `fe_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `fe_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `fe_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收金额',
  `fe_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `fe_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `fe_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `fe_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `fe_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_feelist
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_feelist`;
CREATE TABLE `t_batch_feelist`  (
  `fel_id` bigint NOT NULL AUTO_INCREMENT,
  `fel_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `fel_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用类型编号',
  `fel_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `fel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_feelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_feelist_temp`;
CREATE TABLE `t_batch_feelist_temp`  (
  `fel_id` bigint NOT NULL AUTO_INCREMENT,
  `fel_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用类型编号',
  `fel_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `fel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `fel_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fel_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_order
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_order`;
CREATE TABLE `t_batch_order`  (
  `od_id` int NOT NULL AUTO_INCREMENT,
  `od_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `od_make_date` date NULL DEFAULT NULL COMMENT '制作日期',
  `od_delivery_date` date NULL DEFAULT NULL COMMENT '交货日期',
  `od_client_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '批发客户编号',
  `od_client_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '客户店铺编号',
  `od_depot_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '批发仓库',
  `od_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `od_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `od_state` tinyint(1) NULL DEFAULT 0 COMMENT '0:未完成 1:正常完成 2:已终止 3:超期完成 4：超期未完成',
  `od_handnumber` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手工单号',
  `od_amount` int NULL DEFAULT 0 COMMENT '总数量',
  `od_realamount` int NULL DEFAULT 0 COMMENT '实到数量',
  `od_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `od_retailmoney` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `od_costmoney` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `od_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要',
  `od_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `od_ar_date` date NULL DEFAULT NULL COMMENT '审核人日期',
  `od_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `od_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `od_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `od_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:批发订单  1:退货申请单 ',
  `od_isprint` tinyint(1) NULL DEFAULT 0 COMMENT '货品状态 0 未打单 1：已打单',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`od_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_orderlist
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_orderlist`;
CREATE TABLE `t_batch_orderlist`  (
  `odl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `odl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `odl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `odl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `odl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `odl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `odl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `odl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `odl_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `odl_realamount` int NULL DEFAULT 0 COMMENT '到达数量',
  `odl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `odl_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `odl_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `odl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `odl_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `odl_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:批发订单  1:退货申请单 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`odl_id`) USING BTREE,
  INDEX `IDX_ODL_CO_NUM`(`companyid` ASC, `odl_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 350 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_orderlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_orderlist_temp`;
CREATE TABLE `t_batch_orderlist_temp`  (
  `odl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `odl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `odl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `odl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `odl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `odl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `odl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `odl_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `odl_realamount` int NULL DEFAULT 0 COMMENT '到达数量',
  `odl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `odl_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `odl_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `odl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `odl_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `odl_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:批发订单  1:退货申请单 ',
  `odl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`odl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 286 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_prepay
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_prepay`;
CREATE TABLE `t_batch_prepay`  (
  `pp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `pp_date` date NULL DEFAULT NULL COMMENT '付款日期',
  `pp_client_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '客户编号',
  `pp_type` tinyint(1) NULL DEFAULT 0 COMMENT '收支方式：0：预收款；1：退款',
  `pp_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `pp_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `pp_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `pp_ba_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行编号',
  `pp_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `pp_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `pp_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `pp_sysdate` datetime NULL DEFAULT NULL COMMENT '系统日期',
  `pp_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`pp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_sell
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_sell`;
CREATE TABLE `t_batch_sell`  (
  `se_id` int NOT NULL AUTO_INCREMENT,
  `se_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `se_make_date` date NULL DEFAULT NULL COMMENT '日期',
  `se_client_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '批发客户编号',
  `se_client_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '批发客户店铺编号',
  `se_depot_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '收货仓库',
  `se_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `se_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `se_handnumber` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手工单号',
  `se_amount` int NULL DEFAULT 0 COMMENT '货品总数',
  `se_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `se_retailmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总零售金额',
  `se_costmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总成本金额',
  `se_rebatemoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '返点金额',
  `se_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `se_ba_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '付款帐户id',
  `se_property` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '批发性质',
  `se_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `se_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `se_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `se_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '是否草稿 0 否 1 是',
  `se_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `se_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收金额',
  `se_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `se_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `se_order_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '订单编号',
  `se_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:批发出库  1:退货入库 ',
  `se_stream_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '物流公司Code',
  `se_stream_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '物流金额',
  `se_lastdebt` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '上次欠款',
  `se_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `se_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `se_isprint` tinyint(1) NULL DEFAULT 0 COMMENT '货品状态 0 未打单 1：已打单',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`se_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 966 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_selllist
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_selllist`;
CREATE TABLE `t_batch_selllist`  (
  `sel_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sel_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `sel_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `sel_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `sel_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `sel_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `sel_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `sel_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `sel_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `sel_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `sel_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `sel_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `sel_rebateprice` decimal(16, 2) NULL DEFAULT 0.00 COMMENT '返点价格',
  `sel_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `sel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `sel_order_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '订单单据编号',
  `sel_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:批发出库  1:退货入库 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`sel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20750 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_selllist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_selllist_temp`;
CREATE TABLE `t_batch_selllist_temp`  (
  `sel_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sel_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `sel_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `sel_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `sel_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `sel_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `sel_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `sel_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `sel_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `sel_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `sel_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `sel_rebateprice` decimal(16, 2) NULL DEFAULT 0.00 COMMENT '返点价格',
  `sel_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `sel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `sel_order_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '订单单据编号',
  `sel_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:批发出库  1:退货入库 ',
  `sel_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`sel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19699 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_settle
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_settle`;
CREATE TABLE `t_batch_settle`  (
  `st_id` int NOT NULL AUTO_INCREMENT,
  `st_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `st_date` date NULL DEFAULT NULL COMMENT '制单日期',
  `st_client_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '批发客户编号',
  `st_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `st_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `st_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行编号',
  `st_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `st_prepay` decimal(10, 2) NULL DEFAULT NULL COMMENT '使用预收款金额',
  `st_received` decimal(11, 2) NULL DEFAULT NULL COMMENT '收款金额',
  `st_receivedmore` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实际收款剩余金额',
  `st_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `st_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `st_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `st_entire` tinyint(1) NULL DEFAULT 0 COMMENT '是否整单结算：1-是，0-否',
  `st_pp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '预付款单据编号',
  `st_leftdebt` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '剩余欠款',
  `st_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `st_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_settlelist
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_settlelist`;
CREATE TABLE `t_batch_settlelist`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `stl_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收总金额',
  `stl_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_prepay_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已使用预收款金额',
  `stl_unreceivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未收金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `stl_real_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实收金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '批发单单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_type` tinyint(1) NULL DEFAULT NULL COMMENT '批发单类型  0:出库  1:退货 2:期初',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_batch_settlelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_batch_settlelist_temp`;
CREATE TABLE `t_batch_settlelist_temp`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收总金额',
  `stl_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_prepay_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已使用预收款金额',
  `stl_unreceivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未收金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `stl_real_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实收金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '批发单单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_type` tinyint(1) NULL DEFAULT NULL COMMENT '批发单状态  0:出库  1:退货 2:期初',
  `stl_join` tinyint(1) NULL DEFAULT NULL COMMENT '是否参与结算：1-是，0-否',
  `stl_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_dealings
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_dealings`;
CREATE TABLE `t_buy_dealings`  (
  `dl_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dl_supply_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供应商编号',
  `dl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `dl_type` tinyint(1) NULL DEFAULT NULL COMMENT '类型：0-进货；1-退货；2-期初；3-费用单；4-预付款；5-预付款退款；6-结算单；',
  `dl_discount_money` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `dl_payable` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `dl_payabled` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `dl_debt` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '欠款',
  `dl_date` datetime NULL DEFAULT NULL COMMENT '日期',
  `dl_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作员',
  `dl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `dl_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `dl_buyamount` int NULL DEFAULT NULL COMMENT '数量',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`dl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8167 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_enter
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_enter`;
CREATE TABLE `t_buy_enter`  (
  `et_id` int NOT NULL AUTO_INCREMENT,
  `et_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `et_make_date` date NULL DEFAULT NULL COMMENT '日期',
  `et_supply_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供货厂商',
  `et_depot_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '收货仓库',
  `et_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `et_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `et_handnumber` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手工单号',
  `et_amount` int NULL DEFAULT 0 COMMENT '货品总数',
  `et_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `et_retailmoney` decimal(16, 2) NULL DEFAULT NULL,
  `et_discount_money` decimal(16, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `et_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '付款帐户id',
  `et_property` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '进货性质',
  `et_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `et_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `et_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `et_ar_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '审核人',
  `et_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '是否草稿 0 否 1 是',
  `et_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `et_payable` decimal(16, 2) NULL DEFAULT 0.00 COMMENT '应付金额',
  `et_payabled` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `et_prepay` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '使用预付款金额',
  `et_order_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '订单编号',
  `et_type` tinyint(1) NULL DEFAULT 0 COMMENT '采购状态  0:采购  1:退货 ',
  `et_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `et_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`et_id`) USING BTREE,
  INDEX `IDX_ET_CO_NUM`(`companyid` ASC, `et_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10669 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_enterlist
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_enterlist`;
CREATE TABLE `t_buy_enterlist`  (
  `etl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `etl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `etl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `etl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `etl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `etl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `etl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `etl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `etl_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `etl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `etl_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `etl_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `etl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `etl_order_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '订单单据编号',
  `etl_type` tinyint(1) NULL DEFAULT 0 COMMENT '采购状态  0:采购  1:退货',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  `cr_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sz_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `br_name` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`etl_id`) USING BTREE,
  INDEX `IDX_ETL_CO_NUM`(`companyid` ASC, `etl_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63785 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_enterlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_enterlist_temp`;
CREATE TABLE `t_buy_enterlist_temp`  (
  `etl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `etl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `etl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `etl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `etl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `etl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `etl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `etl_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `etl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `etl_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `etl_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `etl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `etl_order_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '订单单据编号',
  `etl_type` tinyint(1) NULL DEFAULT 0 COMMENT '采购状态  0:采购  1:退货',
  `etl_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`etl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40806 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_fee
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_fee`;
CREATE TABLE `t_buy_fee`  (
  `fe_id` int NOT NULL AUTO_INCREMENT,
  `fe_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `fe_supply_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供应商编号',
  `fe_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `fe_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `fe_date` date NULL DEFAULT NULL COMMENT '日期',
  `fe_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `fe_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `fe_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `fe_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `fe_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `fe_payable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应付金额',
  `fe_payabled` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `fe_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预付款金额',
  `fe_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `fe_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `fe_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_feelist
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_feelist`;
CREATE TABLE `t_buy_feelist`  (
  `fel_id` bigint NOT NULL AUTO_INCREMENT,
  `fel_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `fel_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用类型编号',
  `fel_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `fel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_feelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_feelist_temp`;
CREATE TABLE `t_buy_feelist_temp`  (
  `fel_id` bigint NOT NULL AUTO_INCREMENT,
  `fel_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用类型编号',
  `fel_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `fel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `fel_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fel_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_order
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_order`;
CREATE TABLE `t_buy_order`  (
  `od_id` int NOT NULL AUTO_INCREMENT,
  `od_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `od_make_date` date NULL DEFAULT NULL COMMENT '制作日期',
  `od_delivery_date` date NULL DEFAULT NULL COMMENT '交货日期',
  `od_supply_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供货厂商ID',
  `od_depot_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '收货仓库ID',
  `od_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `od_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `od_state` tinyint(1) NULL DEFAULT 0 COMMENT '0:未完成 1:正常完成 2:已终止 3:超期完成 4：超期未完成',
  `od_handnumber` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手工单号',
  `od_amount` int NULL DEFAULT 0 COMMENT '总数量',
  `od_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `od_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要',
  `od_realamount` int NULL DEFAULT 0 COMMENT '实到数量',
  `od_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `od_ar_date` date NULL DEFAULT NULL COMMENT '审核人日期',
  `od_ar_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '审核人',
  `od_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `od_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `od_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `od_type` tinyint(1) NULL DEFAULT 0 COMMENT '采购状态  0:进货订单  1:退货申请单 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`od_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_orderlist
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_orderlist`;
CREATE TABLE `t_buy_orderlist`  (
  `odl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `odl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `odl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `odl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `odl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `odl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `odl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `odl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `odl_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `odl_realamount` int NULL DEFAULT 0 COMMENT '到达数量',
  `odl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `odl_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `odl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `odl_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品',
  `odl_type` tinyint(1) NULL DEFAULT 0 COMMENT '采购状态  0:进货订单  1:退货申请单 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`odl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 746 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_orderlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_orderlist_temp`;
CREATE TABLE `t_buy_orderlist_temp`  (
  `odl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `odl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `odl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `odl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `odl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `odl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `odl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `odl_amount` int NULL DEFAULT 0 COMMENT '订单数量',
  `odl_realamount` int NULL DEFAULT 0 COMMENT '到达数量',
  `odl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `odl_retailprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `odl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `odl_pi_type` tinyint(1) NULL DEFAULT 0 COMMENT '商品类型，0商品1赠品2样品',
  `odl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `odl_type` tinyint(1) NULL DEFAULT 0 COMMENT '采购状态  0:进货订单  1:退货申请单 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`odl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_prepay
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_prepay`;
CREATE TABLE `t_buy_prepay`  (
  `pp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `pp_date` date NULL DEFAULT NULL COMMENT '付款日期',
  `pp_supply_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '供应商编号',
  `pp_type` tinyint(1) NULL DEFAULT 0 COMMENT '收支方式：0：应付款；1：应收款',
  `pp_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `pp_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `pp_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `pp_ba_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行编号',
  `pp_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `pp_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `pp_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `pp_sysdate` datetime NULL DEFAULT NULL COMMENT '系统日期',
  `pp_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`pp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_settle
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_settle`;
CREATE TABLE `t_buy_settle`  (
  `st_id` int NOT NULL AUTO_INCREMENT,
  `st_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `st_date` datetime NULL DEFAULT NULL COMMENT '制单日期',
  `st_supply_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供货商编号',
  `st_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `st_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `st_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行编号',
  `st_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `st_prepay` decimal(10, 2) NULL DEFAULT NULL COMMENT '使用预付款金额',
  `st_paid` decimal(11, 2) NULL DEFAULT NULL COMMENT '付款金额',
  `st_paidmore` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实际付款剩余金额',
  `st_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `st_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `st_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `st_entire` tinyint(1) NULL DEFAULT 0 COMMENT '是否整单结算：1-是，0-否',
  `st_pp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '预付款单据编号',
  `st_leftdebt` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '剩余欠款',
  `st_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `st_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_settlelist
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_settlelist`;
CREATE TABLE `t_buy_settlelist`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `stl_payable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应付总金额',
  `stl_payabled` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_prepay_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已使用预付款金额',
  `stl_unpayable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未付金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预付款金额',
  `stl_real_pay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实付金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '进货单单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_type` tinyint(1) NULL DEFAULT NULL COMMENT '进货入库单状态  0:采购  1:退货 2:期初',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_settlelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_settlelist_temp`;
CREATE TABLE `t_buy_settlelist_temp`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_payable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应付总金额',
  `stl_payabled` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_prepay_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已使用预付款金额',
  `stl_unpayable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未付金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预付款金额',
  `stl_real_pay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实付金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '进货单单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_type` tinyint(1) NULL DEFAULT NULL COMMENT '进货入库单状态  0:采购  1:退货 2:期初',
  `stl_join` tinyint(1) NULL DEFAULT NULL COMMENT '是否参与结算：1-是，0-否',
  `stl_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_supply
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_supply`;
CREATE TABLE `t_buy_supply`  (
  `sp_id` int NOT NULL AUTO_INCREMENT,
  `sp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sp_name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sp_spell` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sp_rate` decimal(5, 2) NOT NULL DEFAULT 1.00 COMMENT '折扣率',
  `sp_init_debt` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '期初欠款',
  `sp_payable` decimal(12, 2) NOT NULL DEFAULT 0.00 COMMENT '应付金额',
  `sp_payabled` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '已付金额',
  `sp_prepay` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '预付金额',
  `sp_ar_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '区域编号',
  `sp_buy_cycle` tinyint NOT NULL DEFAULT 15 COMMENT '进货周期',
  `sp_settle_cycle` tinyint NOT NULL DEFAULT 15 COMMENT '结帐周期',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sp_id`) USING BTREE,
  INDEX `IDX_SP_CO_SP`(`companyid` ASC, `sp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 897 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_buy_supply_info
-- ----------------------------
DROP TABLE IF EXISTS `t_buy_supply_info`;
CREATE TABLE `t_buy_supply_info`  (
  `spi_id` int NOT NULL AUTO_INCREMENT,
  `spi_sp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_man` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_bank_open` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_bank_code` varchar(25) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `spi_earnest` decimal(8, 1) NULL DEFAULT NULL COMMENT '保证金',
  `spi_deposit` decimal(8, 1) NULL DEFAULT NULL COMMENT '装修押金',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`spi_id`) USING BTREE,
  INDEX `IDX_SPI_CO_SPI_SP`(`companyid` ASC, `spi_sp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 897 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_ec_product
-- ----------------------------
DROP TABLE IF EXISTS `t_ec_product`;
CREATE TABLE `t_ec_product`  (
  `pd_id` bigint NOT NULL,
  `pd_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `pd_pd_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `pd_sell_price` decimal(11, 2) NULL DEFAULT NULL,
  `pd_rate_type` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品类型0新品1折扣',
  `pd_service` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '售后服务',
  `pd_property` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品属性',
  `pd_type` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品分类',
  `pd_sell_real` int NULL DEFAULT 0 COMMENT '销售真实数量',
  `pd_sell_fake` int NULL DEFAULT 0 COMMENT '虚拟销售数量',
  `pd_state` tinyint(1) NULL DEFAULT 0 COMMENT '0在售1停售',
  `pd_collects` int NULL DEFAULT 0 COMMENT '收藏量',
  `pd_looks` int NULL DEFAULT 0 COMMENT '查看次数',
  `pd_sysdate` datetime NULL DEFAULT NULL COMMENT '发布时间',
  `pd_title` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `pd_prior` int NULL DEFAULT 1 COMMENT '展示排序',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`pd_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_import_info
-- ----------------------------
DROP TABLE IF EXISTS `t_import_info`;
CREATE TABLE `t_import_info`  (
  `ii_id` int NOT NULL AUTO_INCREMENT,
  `ii_filename` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '导入文件名',
  `ii_total` int NULL DEFAULT NULL COMMENT '导入总条数',
  `ii_success` int NULL DEFAULT NULL COMMENT '成功条数',
  `ii_fail` int NULL DEFAULT NULL COMMENT '失败条数',
  `ii_error_filename` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '错误文件名',
  `ii_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `ii_sysdate` datetime NULL DEFAULT NULL COMMENT '导入时间',
  `ii_type` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '导入数据类别',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号',
  PRIMARY KEY (`ii_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_access
-- ----------------------------
DROP TABLE IF EXISTS `t_money_access`;
CREATE TABLE `t_money_access`  (
  `ac_id` int NOT NULL AUTO_INCREMENT,
  `ac_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `ac_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `ac_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `ac_date` date NULL DEFAULT NULL COMMENT '日期',
  `ac_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态0 待审批 1 已通过 2 已退回',
  `ac_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `ac_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `ac_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ac_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `ac_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级店铺编号',
  `ac_us_id` int NULL DEFAULT NULL COMMENT '用户id',
  `ac_ca_id` int NULL DEFAULT NULL COMMENT '收银ID',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ac_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_accesslist
-- ----------------------------
DROP TABLE IF EXISTS `t_money_accesslist`;
CREATE TABLE `t_money_accesslist`  (
  `acl_id` int NOT NULL AUTO_INCREMENT,
  `acl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行账户',
  `acl_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '金额',
  `acl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`acl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_accesslist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_money_accesslist_temp`;
CREATE TABLE `t_money_accesslist_temp`  (
  `acl_id` int NOT NULL AUTO_INCREMENT,
  `acl_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行账户',
  `acl_type` tinyint(1) NULL DEFAULT 0 COMMENT '存取类型：0：存款；1：取款',
  `acl_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '金额',
  `acl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `acl_us_id` int NULL DEFAULT NULL COMMENT '用户id',
  `acl_ca_id` int NULL DEFAULT NULL COMMENT '收银id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`acl_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_bank
-- ----------------------------
DROP TABLE IF EXISTS `t_money_bank`;
CREATE TABLE `t_money_bank`  (
  `ba_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行科目编号',
  `ba_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '科目名称',
  `ba_spell` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '拼音码',
  `ba_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ba_init_balance` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '期初余额',
  `ba_balance` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '帐户余额',
  `ba_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级店铺编号',
  `companyid` int NOT NULL COMMENT '企业ID',
  PRIMARY KEY (`ba_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_bankrun
-- ----------------------------
DROP TABLE IF EXISTS `t_money_bankrun`;
CREATE TABLE `t_money_bankrun`  (
  `br_id` int NOT NULL AUTO_INCREMENT,
  `br_date` date NULL DEFAULT NULL COMMENT '日期',
  `br_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `br_bt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '类型',
  `br_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `br_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行帐号编号',
  `br_enter` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '存款金额',
  `br_out` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '取款金额',
  `br_balance` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '当前结余',
  `br_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要',
  `br_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `br_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '登录店铺编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`br_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3141 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_expense
-- ----------------------------
DROP TABLE IF EXISTS `t_money_expense`;
CREATE TABLE `t_money_expense`  (
  `ep_id` int NOT NULL AUTO_INCREMENT,
  `ep_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `ep_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `ep_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `ep_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `ep_date` date NULL DEFAULT NULL COMMENT '日期',
  `ep_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态0 待审批 1 已通过 2 已退回',
  `ep_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `ep_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `ep_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行编号',
  `ep_dm_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '部门编号',
  `ep_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ep_share` tinyint(1) NULL DEFAULT 0 COMMENT '是否按月分摊：1-是，0-否',
  `ep_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `ep_us_id` int NULL DEFAULT NULL COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ep_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_expenselist
-- ----------------------------
DROP TABLE IF EXISTS `t_money_expenselist`;
CREATE TABLE `t_money_expenselist`  (
  `epl_id` int NOT NULL AUTO_INCREMENT,
  `epl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `epl_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '费用类型编号',
  `epl_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '金额',
  `epl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `epl_sharedate` date NULL DEFAULT NULL COMMENT '分摊日期',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`epl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_expenselist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_money_expenselist_temp`;
CREATE TABLE `t_money_expenselist_temp`  (
  `epl_id` int NOT NULL AUTO_INCREMENT,
  `epl_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '费用类型编号',
  `epl_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '金额',
  `epl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `epl_sharemonth` int NULL DEFAULT 1 COMMENT '分摊月数',
  `epl_us_id` int NULL DEFAULT NULL COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`epl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_income
-- ----------------------------
DROP TABLE IF EXISTS `t_money_income`;
CREATE TABLE `t_money_income`  (
  `ic_id` int NOT NULL AUTO_INCREMENT,
  `ic_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `ic_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `ic_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `ic_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `ic_date` date NULL DEFAULT NULL COMMENT '日期',
  `ic_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态0 待审批 1 已通过 2 已退回',
  `ic_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `ic_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `ic_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行编号',
  `ic_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ic_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `ic_us_id` int NULL DEFAULT NULL COMMENT '用户code',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ic_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_incomelist
-- ----------------------------
DROP TABLE IF EXISTS `t_money_incomelist`;
CREATE TABLE `t_money_incomelist`  (
  `icl_id` int NOT NULL AUTO_INCREMENT,
  `icl_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `icl_mp_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '其他收入类型编号',
  `icl_money` decimal(11, 2) NULL DEFAULT NULL COMMENT '金额',
  `icl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`icl_id`) USING BTREE,
  INDEX `PK_ICL_Id`(`icl_id` ASC) USING BTREE,
  INDEX `Index_ICL_Number`(`icl_number` ASC) USING BTREE,
  INDEX `Index_ICL_PP_Code`(`icl_mp_code` ASC) USING BTREE,
  INDEX `Index_ICL_Companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_incomelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_money_incomelist_temp`;
CREATE TABLE `t_money_incomelist_temp`  (
  `icl_id` int NOT NULL AUTO_INCREMENT,
  `icl_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '其他收入类型编号',
  `icl_money` decimal(11, 2) NULL DEFAULT NULL,
  `icl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `icl_us_id` int NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`icl_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_property
-- ----------------------------
DROP TABLE IF EXISTS `t_money_property`;
CREATE TABLE `t_money_property`  (
  `pp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '费用科目编号',
  `pp_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '费用名称',
  `pp_spell` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '拼音码',
  `pp_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级店铺编号',
  `pp_type` tinyint(1) NULL DEFAULT 0 COMMENT '类型:0-费用类型，1-其他收入',
  `companyid` int NOT NULL COMMENT '企业ID',
  PRIMARY KEY (`pp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 121 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_streamsettle
-- ----------------------------
DROP TABLE IF EXISTS `t_money_streamsettle`;
CREATE TABLE `t_money_streamsettle`  (
  `st_id` int NOT NULL AUTO_INCREMENT,
  `st_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `st_date` datetime NULL DEFAULT NULL COMMENT '制单日期',
  `st_stream_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '物流公司编号',
  `st_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `st_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `st_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行编号',
  `st_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `st_paid` decimal(11, 2) NULL DEFAULT NULL COMMENT '付款金额',
  `st_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `st_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `st_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `st_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `st_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_streamsettlelist
-- ----------------------------
DROP TABLE IF EXISTS `t_money_streamsettlelist`;
CREATE TABLE `t_money_streamsettlelist`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `stl_payable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应付总金额',
  `stl_payabled` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_unpayable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未付金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_real_pay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实付金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '物流单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_money_streamsettlelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_money_streamsettlelist_temp`;
CREATE TABLE `t_money_streamsettlelist_temp`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_payable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应付总金额',
  `stl_payabled` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已付金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_unpayable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未付金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_real_pay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实付金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '物流单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_allocate
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_allocate`;
CREATE TABLE `t_sell_allocate`  (
  `ac_id` int NOT NULL AUTO_INCREMENT,
  `ac_number` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ac_date` date NULL DEFAULT NULL,
  `ac_out_shop` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ac_out_dp` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ac_in_shop` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ac_in_dp` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ac_state` tinyint(1) NULL DEFAULT 0 COMMENT '0 暂存 1 已调出 2 完成 3-已拒收',
  `ac_sysdate` datetime NULL DEFAULT NULL,
  `ac_man` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `ac_receiver` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ac_recedate` datetime NULL DEFAULT NULL,
  `ac_sell_money` decimal(11, 2) NULL DEFAULT NULL,
  `ac_cost_money` decimal(11, 2) NULL DEFAULT NULL,
  `ac_amount` int NULL DEFAULT NULL,
  `ac_in_sell_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '调入零售金额',
  `ac_in_cost_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '调入成本金额',
  `ac_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`ac_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1987 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_allocatelist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_allocatelist`;
CREATE TABLE `t_sell_allocatelist`  (
  `acl_id` int NOT NULL AUTO_INCREMENT,
  `acl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码组ID',
  `acl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `acl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `acl_sell_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `acl_cost_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `acl_in_sell_price` decimal(9, 2) NULL DEFAULT NULL,
  `acl_in_cost_price` decimal(9, 2) NULL DEFAULT NULL,
  `acl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`acl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6881 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_allocatelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_allocatelist_temp`;
CREATE TABLE `t_sell_allocatelist_temp`  (
  `acl_id` int NOT NULL AUTO_INCREMENT,
  `acl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `acl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '子码',
  `acl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码组ID',
  `acl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码ID',
  `acl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色ID',
  `acl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '内衣编号',
  `acl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `acl_sell_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `acl_cost_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `acl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `acl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`acl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1016 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_card
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_card`;
CREATE TABLE `t_sell_card`  (
  `cd_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cd_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '储值卡编号',
  `cd_cardcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '储值卡号',
  `cd_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '发卡门店ID',
  `cd_grantdate` date NOT NULL COMMENT '办卡日期',
  `cd_enddate` date NOT NULL COMMENT '有效日期',
  `cd_money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '卡内剩余金额',
  `cd_realcash` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '实收金额',
  `cd_bankmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '刷卡金额',
  `cd_used_money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '累计消费额',
  `cd_cashrate` decimal(9, 2) NOT NULL DEFAULT 1.00 COMMENT '现金率',
  `cd_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0:正常  1:停用',
  `cd_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作用户',
  `cd_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '办卡人姓名',
  `cd_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '办卡人手机号',
  `cd_idcard` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '身份证号',
  `cd_pass` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '初始密码',
  `cd_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `cd_verify_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '校验码',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`cd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 358 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_cardlist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_cardlist`;
CREATE TABLE `t_sell_cardlist`  (
  `cdl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cdl_cd_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '储值卡编号',
  `cdl_cardcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '储值卡号',
  `cdl_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '充值金额',
  `cdl_realcash` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实收现金',
  `cdl_bankmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '刷卡金额',
  `cdl_date` datetime NULL DEFAULT NULL COMMENT '充值日期',
  `cdl_type` tinyint(1) NULL DEFAULT 0 COMMENT '0：期初 1：充值  2：减值  3：消费 4:换货',
  `cdl_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '充值门店编码',
  `cdl_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '现金银行账户',
  `cdl_bank_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '刷卡的帐户',
  `cdl_number` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `cdl_manager` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作人',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`cdl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 768 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_cart
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_cart`;
CREATE TABLE `t_sell_cart`  (
  `sc_id` int NOT NULL AUTO_INCREMENT,
  `sc_vm_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `sc_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '唯一标识码',
  `sc_em_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '导购编号',
  `sc_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `sc_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `sc_amount` int NOT NULL DEFAULT 0 COMMENT '数量',
  `sc_money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '金额',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_cartlist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_cartlist`;
CREATE TABLE `t_sell_cartlist`  (
  `scl_id` bigint NOT NULL AUTO_INCREMENT,
  `scl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '唯一标识码',
  `scl_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `scl_pd_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `scl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品子码',
  `scl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `scl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码',
  `scl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型',
  `scl_amount` int NULL DEFAULT 1 COMMENT '数量',
  `scl_sell_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `scl_state` tinyint(1) NULL DEFAULT 0 COMMENT '下单状态0:未下单 1:已下单',
  `scl_vm_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `scl_em_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '导购编号',
  `scl_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL COMMENT '促销编号',
  PRIMARY KEY (`scl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_cashier
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_cashier`;
CREATE TABLE `t_sell_cashier`  (
  `ca_id` int NOT NULL AUTO_INCREMENT,
  `ca_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '员工编号',
  `ca_pass` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ca_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `ca_state` tinyint(1) NULL DEFAULT NULL,
  `ca_erase` decimal(9, 1) NULL DEFAULT 10.0 COMMENT '抹零金额',
  `ca_maxmoney` decimal(9, 1) NULL DEFAULT 100.0 COMMENT '最高让利',
  `ca_minrate` decimal(3, 2) NULL DEFAULT 0.40 COMMENT '最小折扣',
  `ca_days` smallint NULL DEFAULT 7 COMMENT '零售可查询天数',
  `companyid` int NOT NULL COMMENT '商家ID',
  `ca_last_login` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`ca_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 250 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_cashier_set
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_cashier_set`;
CREATE TABLE `t_sell_cashier_set`  (
  `cs_id` int NOT NULL AUTO_INCREMENT,
  `cs_key` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `cs_value` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `cs_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `companyid` int NOT NULL,
  PRIMARY KEY (`cs_id`) USING BTREE,
  INDEX `IDX_CS_CO_EM`(`companyid` ASC, `cs_em_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6349 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_coupon
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_coupon`;
CREATE TABLE `t_sell_coupon`  (
  `sc_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sc_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '优惠券编号',
  `sc_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '优惠券名称',
  `sc_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '发放店铺编号',
  `sc_full_money` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '满多少元',
  `sc_minus_money` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '减多少元',
  `sc_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '使用范围：0：全场 1：类别 2：品牌 3：商品',
  `sc_amount` int NOT NULL DEFAULT 0 COMMENT '发布数量',
  `sc_receive_amount` int NOT NULL DEFAULT 0 COMMENT '领取数量',
  `sc_begindate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '活动开始日期',
  `sc_enddate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '活动结束日期',
  `sc_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '使用说明',
  `sc_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '审核状态 0:待审核 1:审核通过 2:审核未通过  4:已终止',
  `sc_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `sc_makedate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单日期',
  `sc_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '系统日期',
  `sc_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `sc_show_chat` tinyint(1) NULL DEFAULT 1 COMMENT '是否在微商城显示 0-不显示，1-显示',
  `sc_show_sysinfo` tinyint(1) NULL DEFAULT 1 COMMENT '是否显示系统生成优惠券信息：0-不显示，1-显示',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`sc_id`) USING BTREE,
  INDEX `IDX_SC_CO_SP_NO`(`companyid` ASC, `sc_shop_code` ASC, `sc_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_coupon_brand
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_coupon_brand`;
CREATE TABLE `t_sell_coupon_brand`  (
  `scb_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `scb_sc_number` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '优惠券编号',
  `scb_bd_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '品牌编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`scb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_coupon_product
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_coupon_product`;
CREATE TABLE `t_sell_coupon_product`  (
  `scp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `scp_sc_number` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '优惠券编号',
  `scp_pd_code` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`scp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_coupon_type
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_coupon_type`;
CREATE TABLE `t_sell_coupon_type`  (
  `sct_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sct_sc_number` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '优惠券编号',
  `sct_tp_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '类别编号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家id',
  PRIMARY KEY (`sct_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_day
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_day`;
CREATE TABLE `t_sell_day`  (
  `da_id` int NOT NULL AUTO_INCREMENT,
  `da_come` smallint NULL DEFAULT 0 COMMENT '进店人数',
  `da_receive` smallint NULL DEFAULT 0 COMMENT '接待人数',
  `da_try` smallint NULL DEFAULT NULL,
  `da_date` date NULL DEFAULT NULL COMMENT '时间',
  `da_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`da_id`) USING BTREE,
  INDEX `IDX_DA_CO_SHOP`(`companyid` ASC, `da_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10323 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_dayend
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_dayend`;
CREATE TABLE `t_sell_dayend`  (
  `de_id` int NOT NULL AUTO_INCREMENT,
  `de_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '当班人',
  `de_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `de_st_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '班次',
  `de_state` tinyint(1) NULL DEFAULT 0 COMMENT '0未日结1已日结',
  `de_bills` int NULL DEFAULT 0 COMMENT '总开单数',
  `de_sell_bills` int NULL DEFAULT 0 COMMENT '零售单数',
  `de_back_bills` int NULL DEFAULT 0 COMMENT '退货单数',
  `de_change_bills` int NULL DEFAULT 0 COMMENT '换货单数',
  `de_sell_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '零售金额',
  `de_amount` int NULL DEFAULT 0 COMMENT '销售数量',
  `de_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '销售金额',
  `de_last_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '上班余款',
  `de_petty_cash` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '备用金金额',
  `de_gifts` int NULL DEFAULT 0 COMMENT '礼品发放数量',
  `de_vips` int NULL DEFAULT 0 COMMENT '会员发放卡数量',
  `de_cash` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '收取现金',
  `de_cd_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '储值卡使用金额',
  `de_vc_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '代金券使用金额',
  `de_sd_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '订金使用金额',
  `de_point_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '积分使用金额',
  `de_ec_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '优惠券使用金额',
  `de_bank_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '银行卡金额',
  `de_mall_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '商场卡金额',
  `de_ali_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '支付宝金额',
  `de_wx_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '微信金额',
  `de_lost_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '抹零金额',
  `de_cd_sends` int NULL DEFAULT 0 COMMENT '储值卡发放数量',
  `de_cd_send_cash` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '储值卡总收入',
  `de_cd_send_bank` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '储值卡发放银行刷卡',
  `de_cd_fill_cash` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '储值卡充值现金收入',
  `de_cd_fill_bank` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '储值卡充值银行金额',
  `de_vc_sends` int NULL DEFAULT 0 COMMENT '代金券发放数量',
  `de_vc_send_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '代金券发放金额',
  `de_sd_send_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '订金收取金额',
  `de_sd_back_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '订金退回金额',
  `de_vip_point` int NULL DEFAULT NULL,
  `de_begindate` datetime NULL DEFAULT NULL COMMENT '开始时间2016-12-12 12:12:12',
  `de_enddate` datetime NULL DEFAULT NULL,
  `de_man` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '交班人',
  `de_in_amount` int NULL DEFAULT 0 COMMENT '调入数量',
  `de_out_amount` int NULL DEFAULT 0 COMMENT '调出数量',
  `de_stock_amount` int NULL DEFAULT 0 COMMENT '当班库存',
  `de_in_cash` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '现金流向帐户',
  `de_in_bank` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '刷卡收款银行',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`de_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2230 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_deposit
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_deposit`;
CREATE TABLE `t_sell_deposit`  (
  `sd_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sd_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `sd_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '预订时间',
  `sd_enddate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sd_state` tinyint(1) NULL DEFAULT 0 COMMENT '0:预订 1:提取 2：退订',
  `sd_customer` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '客户姓名',
  `sd_tel` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电话',
  `sd_deposit` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '订金',
  `sd_remark` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `sd_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '收银员',
  `sd_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sd_id`) USING BTREE,
  UNIQUE INDEX `pk_Id`(`sd_id` ASC) USING BTREE,
  INDEX `index_SD_Number`(`sd_number` ASC) USING BTREE,
  INDEX `index_SD_Companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_depositlist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_depositlist`;
CREATE TABLE `t_sell_depositlist`  (
  `sdl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sdl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `sdl_pd_code` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `sdl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品子码',
  `sdl_cr_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `sdl_sz_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `sdl_br_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣尺码',
  `sdl_tp_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '类别code',
  `sdl_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '门店ID',
  `sdl_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '收银员ID',
  `sdl_bd_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '品牌ID',
  `sdl_main` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '主导购',
  `sdl_slave` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '副导购',
  `sdl_area` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sdl_vip_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `sdl_amount` int NULL DEFAULT 1 COMMENT '数量',
  `sdl_sell_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售单价',
  `sdl_sign_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '标牌价',
  `sdl_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '折后价',
  `sdl_cost_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `sdl_upcost_price` decimal(9, 2) NULL DEFAULT NULL,
  `sdl_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '折后金额',
  `sdl_vip_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '会员让利',
  `sdl_hand_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '手动让利',
  `sdl_sale_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '促销让利',
  `sdl_sale_model` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销方式',
  `sdl_sale_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销编号',
  `sdl_ispoint` tinyint(1) NULL DEFAULT NULL COMMENT '会员是否积分',
  `sdl_isgift` tinyint(1) NULL DEFAULT NULL COMMENT '是否赠品',
  `sdl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销备注',
  `sdl_state` tinyint(1) NULL DEFAULT 0 COMMENT '单据处理状态：0：已预付 1：已申报 2：已到货 3：已销售 4:不销售',
  `sdl_ismessage` tinyint(1) NULL DEFAULT 0 COMMENT '是否发送短信提醒：0：未发送 1：已发送',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sdl_id`) USING BTREE,
  UNIQUE INDEX `PK_SDL_ID`(`sdl_id` ASC) USING BTREE,
  INDEX `index_SDL_Number`(`sdl_number` ASC) USING BTREE,
  INDEX `index_SDL_Companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_displayarea
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_displayarea`;
CREATE TABLE `t_sell_displayarea`  (
  `da_id` int NOT NULL AUTO_INCREMENT,
  `da_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '陈列区域编号',
  `da_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '名称',
  `da_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `companyid` int NULL DEFAULT 0 COMMENT '企业ID',
  PRIMARY KEY (`da_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecoupon
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecoupon`;
CREATE TABLE `t_sell_ecoupon`  (
  `ec_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ec_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '电子券编号',
  `ec_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '电子券名称',
  `ec_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `ec_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `ec_deadline` int NULL DEFAULT 0 COMMENT '有效期天数',
  `ec_begindate` date NULL DEFAULT NULL COMMENT '活动开始时间',
  `ec_enddate` date NULL DEFAULT NULL COMMENT '活动结束时间',
  `ec_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '发卡门店编号',
  `ec_sysdate` date NULL DEFAULT NULL COMMENT '系统日期',
  `ec_level` tinyint(1) NULL DEFAULT 1 COMMENT '电子券优先级1.最高2.较高3.一般4.较低5.最低',
  `ec_type` tinyint(1) NULL DEFAULT 0 COMMENT '0主动领取1收银发放',
  `ec_mode` tinyint(1) NULL DEFAULT 1 COMMENT '发放条件：1.全场2.品牌3.类别4.商品',
  `ec_use_mode` tinyint(1) NULL DEFAULT 1 COMMENT '使用范围：1.全场2.品牌3.类别4.商品',
  `ec_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态:0:未审核1:已通过2:已退回4:已终止',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`ec_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecoupon_brand
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecoupon_brand`;
CREATE TABLE `t_sell_ecoupon_brand`  (
  `ecb_id` int NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `ecb_ec_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '电子券编号',
  `ecb_bd_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '品牌编号',
  `ecb_type` tinyint(1) NOT NULL COMMENT '类型1.发放条件2.使用范围',
  `companyid` int NOT NULL COMMENT '商家id',
  PRIMARY KEY (`ecb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecoupon_product
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecoupon_product`;
CREATE TABLE `t_sell_ecoupon_product`  (
  `ecp_id` int NOT NULL AUTO_INCREMENT,
  `ecp_ec_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '电子券编号',
  `ecp_pd_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `ecp_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '类型1.发放条件2.使用范围',
  `companyid` int NOT NULL COMMENT '商家id',
  PRIMARY KEY (`ecp_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecoupon_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecoupon_shop`;
CREATE TABLE `t_sell_ecoupon_shop`  (
  `ecs_id` int NOT NULL AUTO_INCREMENT,
  `ecs_ec_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '电子券编号',
  `ecs_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '活动门店编号',
  `companyid` int NOT NULL COMMENT '商家id',
  PRIMARY KEY (`ecs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecoupon_type
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecoupon_type`;
CREATE TABLE `t_sell_ecoupon_type`  (
  `ect_id` int NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `ect_ec_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '电子券编号',
  `ect_tp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '品牌编号',
  `ect_type` tinyint(1) NOT NULL COMMENT '类型1.发放条件2.使用范围',
  `companyid` int NOT NULL COMMENT '商家id',
  PRIMARY KEY (`ect_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecoupon_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecoupon_user`;
CREATE TABLE `t_sell_ecoupon_user`  (
  `ecu_id` int NOT NULL AUTO_INCREMENT,
  `ecu_ec_number` char(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '优惠券方案单据编号',
  `ecu_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '优惠券编号',
  `ecu_sh_number` char(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '产生的收银编号',
  `ecu_date` date NULL DEFAULT NULL COMMENT '产生时间',
  `ecu_name` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `ecu_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电话',
  `ecu_vip_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `ecu_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `ecu_limitmoney` decimal(9, 1) NULL DEFAULT NULL COMMENT '使用限制',
  `ecu_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `ecu_begindate` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '有效期开始时间',
  `ecu_enddate` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '有效期结束时间',
  `ecu_state` tinyint(1) NULL DEFAULT 0 COMMENT '电子券状态0未使用1已使用',
  `ecu_use_number` varchar(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '使用的收银单据',
  `ecu_use_date` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '使用日期',
  `ecu_use_type` tinyint(1) NULL DEFAULT NULL COMMENT '使用地方1前台2微商城',
  `ecu_check_no` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '验证码，使用时验证',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`ecu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecouponlist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecouponlist`;
CREATE TABLE `t_sell_ecouponlist`  (
  `ecl_id` bigint NOT NULL AUTO_INCREMENT,
  `ecl_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '电子券编号',
  `ecl_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '优惠券编号',
  `ecl_usedmoney` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '满多少元赠送',
  `ecl_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '面值',
  `ecl_limitmoney` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '使用条件满多少元可以使用',
  `ecl_amount` int NULL DEFAULT 0 COMMENT '发放数量',
  `ecl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `companyid` int NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`ecl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_ecouponlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_ecouponlist_temp`;
CREATE TABLE `t_sell_ecouponlist_temp`  (
  `ecl_id` bigint NOT NULL AUTO_INCREMENT,
  `ecl_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '电子券编号',
  `ecl_usedmoney` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '满多少元赠送',
  `ecl_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '面值',
  `ecl_limitmoney` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '使用条件满多少元可以使用',
  `ecl_amount` int NULL DEFAULT 0 COMMENT '发放数量',
  `ecl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `ecl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`ecl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_number
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_number`;
CREATE TABLE `t_sell_number`  (
  `sn_id` int NOT NULL AUTO_INCREMENT,
  `sn_number` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sn_em_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sn_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL,
  `sn_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`sn_id`) USING BTREE,
  INDEX `IDX_SN_CO_DA_SP_EM`(`companyid` ASC, `sn_date` ASC, `sn_shop_code` ASC, `sn_em_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9415 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_pay_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_pay_temp`;
CREATE TABLE `t_sell_pay_temp`  (
  `pt_id` int NOT NULL AUTO_INCREMENT,
  `companyid` int NOT NULL,
  `pt_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `pt_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `pt_state` tinyint(1) NULL DEFAULT NULL,
  `pt_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `pt_money` decimal(11, 2) NOT NULL,
  `pt_paycode` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_print
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_print`;
CREATE TABLE `t_sell_print`  (
  `sp_id` int NOT NULL AUTO_INCREMENT,
  `sp_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sp_title` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sp_qrcode` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sp_remark` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sp_print` tinyint(1) NULL DEFAULT 1 COMMENT '打印小票0否1是',
  `sp_auto` tinyint(1) NULL DEFAULT 0 COMMENT '自动打印0否1是',
  `sp_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_printdata
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_printdata`;
CREATE TABLE `t_sell_printdata`  (
  `spd_id` int NOT NULL AUTO_INCREMENT,
  `spd_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '数据库字段名',
  `spd_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字段名称',
  `spd_namecustom` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '自定义显示名称',
  `spd_order` smallint NULL DEFAULT 0 COMMENT '排序',
  `spd_show` tinyint(1) NULL DEFAULT 1 COMMENT '是否显示0 否 1是',
  `spd_width` int NULL DEFAULT 80 COMMENT '字段宽度',
  `spd_align` tinyint(1) NULL DEFAULT 0 COMMENT '对齐方式0:居左 1:居中 2:居右',
  `spd_sp_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '设置主表编号',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`spd_id`) USING BTREE,
  INDEX `IDX_SPD_CO_SHOP`(`companyid` ASC, `spd_sp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 664 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_printfield
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_printfield`;
CREATE TABLE `t_sell_printfield`  (
  `spf_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `spf_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '数据库字段名',
  `spf_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `spf_namecustom` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '自定义显示名称',
  `spf_colspan` tinyint(1) NULL DEFAULT NULL COMMENT '默认占列数',
  `spf_show` tinyint(1) NULL DEFAULT NULL COMMENT '默认是否显示',
  `spf_line` tinyint(1) NULL DEFAULT NULL,
  `spf_row` tinyint(1) NULL DEFAULT NULL,
  `spf_position` tinyint(1) NULL DEFAULT 0 COMMENT '位置 0:表头 1:表尾',
  `spf_sp_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0' COMMENT '设置主表编号',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`spf_id`) USING BTREE,
  INDEX `IDX_SPF_CO_SHOP`(`companyid` ASC, `spf_sp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1253 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_printset
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_printset`;
CREATE TABLE `t_sell_printset`  (
  `sps_id` int NOT NULL AUTO_INCREMENT,
  `sps_key` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '零售设置表kEY',
  `sps_value` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '零售设置表VALUE',
  `sps_sp_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '设置主表编号',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sps_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 241 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_receive
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_receive`;
CREATE TABLE `t_sell_receive`  (
  `re_id` int NOT NULL AUTO_INCREMENT,
  `re_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `re_date` datetime NULL DEFAULT NULL,
  `re_em_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `re_type` tinyint(1) NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT 0 COMMENT '0进店1接待',
  PRIMARY KEY (`re_id`) USING BTREE,
  INDEX `IDX_RE_CO_SHOP_EM`(`companyid` ASC, `re_shop_code` ASC, `re_em_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14312 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_set
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_set`;
CREATE TABLE `t_sell_set`  (
  `st_id` int NOT NULL AUTO_INCREMENT,
  `st_key` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `st_value` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `st_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `companyid` int NOT NULL,
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1081 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_shift
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_shift`;
CREATE TABLE `t_sell_shift`  (
  `st_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `st_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '班次编号',
  `st_name` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '班次名称',
  `st_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '门店编号',
  `st_begintime` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上班时间',
  `st_endtime` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '下班时间',
  `companyid` int NULL DEFAULT 0 COMMENT '企业ID',
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 216 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_shop`;
CREATE TABLE `t_sell_shop`  (
  `sh_id` bigint NOT NULL AUTO_INCREMENT,
  `sh_number` char(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '单据编号：前缀+收银编号+日期+流水号',
  `sh_sysdate` datetime NOT NULL COMMENT '零售时间显示到秒',
  `sh_date` date NOT NULL COMMENT '零售日期显示到天',
  `sh_em_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '收银员编号',
  `sh_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `sh_vip_other` tinyint(1) NULL DEFAULT 0 COMMENT '借卡0否1是',
  `sh_vip_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `sh_amount` int NOT NULL DEFAULT 1 COMMENT '数量',
  `sh_money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '折后金额',
  `sh_sell_money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '零售金额',
  `sh_cash` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '实收现金',
  `sh_cost_money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '成本价',
  `sh_upcost_money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '总部成本金额',
  `sh_cd_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '储值卡金额',
  `sh_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0零售1退货2换货',
  `companyid` int NOT NULL,
  `sh_cd_rate` decimal(4, 2) NULL DEFAULT 1.00 COMMENT '储值卡现金率',
  `sh_vc_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '代金券',
  `sh_vc_rate` decimal(4, 2) NULL DEFAULT 1.00 COMMENT '代金券现金率',
  `sh_point_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '积分抵用金额',
  `sh_ec_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '优惠券',
  `sh_sd_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '订金',
  `sh_bank_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '银行卡刷卡金额',
  `sh_mall_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '商场卡金额',
  `sh_lost_money` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '抹零金额',
  `sh_wx_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '微信支付',
  `sh_ali_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '支付宝金额',
  `sh_st_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '班次编号',
  `sh_third_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '第三方支付',
  `sh_red_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '红包金额',
  `sh_source` tinyint(1) NULL DEFAULT 1 COMMENT '单据来源1店铺2微商城',
  `sh_remark` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`sh_id`) USING BTREE,
  INDEX `IDX_SH_CO_NO_SP_DA`(`companyid` ASC, `sh_number` ASC, `sh_shop_code` ASC, `sh_date` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50848 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_shop_copy
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_shop_copy`;
CREATE TABLE `t_sell_shop_copy`  (
  `sh_id` bigint NOT NULL AUTO_INCREMENT,
  `sh_number` char(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '单据编号：前缀+收银编号+日期+流水号',
  `sh_sysdate` datetime NULL DEFAULT NULL COMMENT '零售时间显示到秒',
  `sh_date` date NULL DEFAULT NULL COMMENT '零售日期显示到天',
  `sh_em_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '收银员编号',
  `sh_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `sh_vip_other` tinyint(1) NULL DEFAULT 0 COMMENT '借卡0否1是',
  `sh_vip_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `sh_amount` int NULL DEFAULT 1 COMMENT '数量',
  `sh_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '折后金额',
  `sh_sell_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '零售金额',
  `sh_cash` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '实收现金',
  `sh_cost_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `sh_upcost_money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '总部成本金额',
  `sh_cd_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '储值卡金额',
  `sh_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0零售1退货2换货',
  `companyid` int NOT NULL,
  `sh_cd_rate` decimal(4, 2) NULL DEFAULT 1.00 COMMENT '储值卡现金率',
  `sh_vc_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '代金券',
  `sh_vc_rate` decimal(4, 2) NULL DEFAULT 1.00 COMMENT '代金券现金率',
  `sh_point_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '积分抵用金额',
  `sh_ec_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '优惠券',
  `sh_sd_money` decimal(7, 2) NULL DEFAULT 0.00 COMMENT '订金',
  `sh_bank_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '银行卡刷卡金额',
  `sh_mall_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '商场卡金额',
  `sh_lost_money` decimal(5, 2) NULL DEFAULT 0.00 COMMENT '抹零金额',
  `sh_wx_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '微信支付',
  `sh_ali_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '支付宝金额',
  `sh_st_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '班次编号',
  `sh_third_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '第三方支付',
  `sh_red_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '红包金额',
  `sh_source` tinyint(1) NULL DEFAULT 1 COMMENT '单据来源1店铺2微商城',
  `sh_remark` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`sh_id`) USING BTREE,
  INDEX `IDX_SH_CO_NO_SP_DA`(`companyid` ASC, `sh_number` ASC, `sh_shop_code` ASC, `sh_date` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4929 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_shop_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_shop_temp`;
CREATE TABLE `t_sell_shop_temp`  (
  `sht_id` int NOT NULL AUTO_INCREMENT,
  `sht_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sht_sub_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sht_cr_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sht_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sht_br_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sht_tp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '类别',
  `sht_bd_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '品牌',
  `sht_em_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `companyid` int NOT NULL,
  `sht_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sht_da_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sht_main` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sht_slave` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sht_vip_other` tinyint(1) NULL DEFAULT 0 COMMENT '借卡0否1是',
  `sht_vip_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sht_amount` int NOT NULL DEFAULT 1 COMMENT '数量',
  `sht_sell_price` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '零售价',
  `sht_sign_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '标牌价',
  `sht_upcost_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '总部成本价',
  `sht_vip_price` decimal(9, 2) NULL DEFAULT NULL,
  `sht_cost_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `sht_price` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '折后价',
  `sht_money` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '折后金额',
  `sht_sale_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销编号',
  `sht_sale_model` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销方式',
  `sht_sale_priority` tinyint NULL DEFAULT 0,
  `sht_sale_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '促销让利金额',
  `sht_sale_send_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '促销赠送金额',
  `sht_hand_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '手动让利金额',
  `sht_vip_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '会员让利金额',
  `sht_handtype` tinyint(1) NULL DEFAULT 0 COMMENT '手动打折的类型：0无手动折1折扣2让利3实际金额',
  `sht_handvalue` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '手动打折让利的参数',
  `sht_ishand` tinyint(1) NULL DEFAULT 1 COMMENT '手动打折0否1是',
  `sht_vipvalue` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '折扣或价格',
  `sht_viptype` tinyint(1) NULL DEFAULT 0 COMMENT '会员打折的类型0无1折扣2商品价',
  `sht_isvip` tinyint(1) NULL DEFAULT 1 COMMENT '会员打折0否1是',
  `sht_ispoint` tinyint(1) NULL DEFAULT 1 COMMENT '积分0否1是',
  `sht_isgift` tinyint(1) NULL DEFAULT 0 COMMENT '赠品0否1是',
  `sht_isputup` tinyint(1) NOT NULL DEFAULT 0 COMMENT '挂单0否1是',
  `sht_putup_no` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '挂单编号',
  `sht_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0零售1退货2换货',
  `sht_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `pd_no` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '货号',
  `cr_name` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '颜色名称',
  `pd_name` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '名称',
  `sz_name` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '颜色名称',
  `br_name` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '颜色名称',
  `main_name` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '主导购',
  `slave_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `em_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sht_id`) USING BTREE,
  INDEX `IDX_SHT_CO_SP_EM_PT_ST`(`companyid`, `sht_shop_code`, `sht_em_code`, `sht_isputup`, `sht_state`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 108852 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_shopdetail
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_shopdetail`;
CREATE TABLE `t_sell_shopdetail`  (
  `shl_id` bigint NOT NULL AUTO_INCREMENT,
  `shl_number` char(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `shl_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`shl_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_shoplist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_shoplist`;
CREATE TABLE `t_sell_shoplist`  (
  `shl_id` bigint NOT NULL AUTO_INCREMENT,
  `shl_number` char(14) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '单据编号',
  `shl_date` date NOT NULL COMMENT '零售时间精确到天',
  `shl_sysdate` datetime NOT NULL COMMENT '零售时间精确到少',
  `shl_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `shl_sub_code` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品子码',
  `shl_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `shl_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `shl_br_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `shl_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '收银编号',
  `shl_slave` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '副导购',
  `shl_main` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '主导购',
  `shl_da_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '位置编号',
  `shl_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `shl_dp_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '仓库编号',
  `shl_amount` int NOT NULL DEFAULT 1 COMMENT '数量',
  `shl_sell_price` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '零售价',
  `shl_price` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '折后价',
  `shl_cost_price` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '成本价',
  `shl_upcost_price` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '总部成本价',
  `shl_money` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '折后金额',
  `shl_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态0零售1退货2换货',
  `companyid` int NOT NULL COMMENT '促销编号',
  `shl_vip_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `shl_sale_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销编号',
  PRIMARY KEY (`shl_id`) USING BTREE,
  INDEX `IDX_SHL_CO_SHOP_PD_CR_SZ`(`companyid` ASC, `shl_shop_code` ASC, `shl_pd_code` ASC, `shl_cr_code` ASC, `shl_sz_code` ASC, `shl_br_code` ASC) USING BTREE,
  INDEX `IDX_SHL_CO_SP_DA`(`companyid` ASC, `shl_shop_code` ASC, `shl_date` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 92514 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_try
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_try`;
CREATE TABLE `t_sell_try`  (
  `tr_id` int NOT NULL AUTO_INCREMENT,
  `tr_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `tr_date` datetime NULL DEFAULT NULL,
  `tr_em_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`tr_id`) USING BTREE,
  INDEX `IDX_TR_CO_SHOP_EM`(`companyid` ASC, `tr_shop_code` ASC, `tr_em_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3412 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_voucher
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_voucher`;
CREATE TABLE `t_sell_voucher`  (
  `vc_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vc_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '代金券编号',
  `vc_cardcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '代金券号',
  `vc_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '门店编号',
  `vc_grantdate` date NULL DEFAULT NULL COMMENT '发卡日期',
  `vc_enddate` date NULL DEFAULT NULL COMMENT '有效日期',
  `vc_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '面值',
  `vc_used_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '使用金额',
  `vc_realcash` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '实收现金',
  `vc_cashrate` decimal(9, 2) NULL DEFAULT 1.00 COMMENT '现金率',
  `vc_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态 0:正常 1;停用',
  `vc_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `vc_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '姓名',
  `vc_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `vc_manager` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人名称',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`vc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 309 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_voucherlist
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_voucherlist`;
CREATE TABLE `t_sell_voucherlist`  (
  `vcl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vcl_vc_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '代金券编号',
  `vcl_cardcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '代金券号',
  `vcl_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `vcl_cash` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '实收现金',
  `vcl_date` datetime NULL DEFAULT NULL COMMENT '操作日期',
  `vcl_type` tinyint(1) NULL DEFAULT 0 COMMENT '0：期初  1：消费',
  `vcl_shop_code` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '发放门店编号',
  `vcl_ba_code` varchar(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行编号',
  `vcl_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `vcl_manager` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作人',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`vcl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 308 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sell_weather
-- ----------------------------
DROP TABLE IF EXISTS `t_sell_weather`;
CREATE TABLE `t_sell_weather`  (
  `we_id` int NOT NULL AUTO_INCREMENT,
  `we_max_tmp` tinyint NULL DEFAULT NULL COMMENT '最大温度',
  `we_min_tmp` tinyint NULL DEFAULT NULL COMMENT '最小温度',
  `we_we_id` tinyint NULL DEFAULT NULL,
  `we_wind` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '风级',
  `we_date` date NULL DEFAULT NULL COMMENT '时间',
  `we_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`we_id`) USING BTREE,
  INDEX `IDX_DA_CO_SHOP`(`companyid` ASC, `we_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8535 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_complaint
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_complaint`;
CREATE TABLE `t_shop_complaint`  (
  `sc_id` int NOT NULL AUTO_INCREMENT,
  `sc_username` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '投诉人',
  `sc_mobile` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '顾客电话',
  `sc_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '投诉日期',
  `sc_content` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '投诉内容',
  `sc_processname` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '受理人员',
  `sc_acceptdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '受理时间',
  `sc_processinfo` varchar(150) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '处理内容',
  `sc_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '投诉状态 0:待处理 1:处理中 2结束',
  `sc_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0' COMMENT '店铺编号',
  `sc_us_id` int NULL DEFAULT 0 COMMENT '操作人员ID',
  `sc_satis` tinyint(1) NULL DEFAULT 0 COMMENT '满意度1满意2一般3不满意',
  `sc_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '投诉类型 1:产品 2:服务 3:其他',
  `sc_enddate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '结束时间',
  `sc_selman` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '被投诉人',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sc_id`) USING BTREE,
  INDEX `PK_SC_ID`(`sc_id` ASC) USING BTREE,
  INDEX `Index_SC_Companyid`(`companyid` ASC) USING BTREE,
  INDEX `Index_CL_Shop_Code`(`sc_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_gift
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_gift`;
CREATE TABLE `t_shop_gift`  (
  `gi_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gi_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `gi_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '发放店铺',
  `gi_begindate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '开始日期',
  `gi_enddate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '结束日期',
  `gi_sysdate` datetime NULL DEFAULT NULL,
  `gi_state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '上架状态： 0:上架 1：下架',
  `gi_point` int NOT NULL DEFAULT 0 COMMENT '兑换积分数',
  `companyid` int NOT NULL,
  PRIMARY KEY (`gi_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_gift_run
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_gift_run`;
CREATE TABLE `t_shop_gift_run`  (
  `gir_id` int NOT NULL AUTO_INCREMENT,
  `gir_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gir_sub_code` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '子码',
  `gir_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `gir_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `gir_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码',
  `gir_szg_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码组编号',
  `gir_bs_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型',
  `gir_totalamount` int NULL DEFAULT 0 COMMENT '礼品数量',
  `gir_begindate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gir_enddate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gir_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gir_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '发放店铺',
  `gir_user_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户code',
  `companyid` int NOT NULL,
  PRIMARY KEY (`gir_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 255 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_gift_send
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_gift_send`;
CREATE TABLE `t_shop_gift_send`  (
  `gs_id` int NOT NULL AUTO_INCREMENT,
  `gs_pd_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_cr_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_br_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_amount` smallint NULL DEFAULT 0 COMMENT '领取数量',
  `gs_last_amount` smallint NULL DEFAULT 0 COMMENT '剩余发放数量',
  `gs_state` tinyint(1) NULL DEFAULT 0 COMMENT '兑换状态：0：未发放 1：已发放',
  `gs_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_em_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_vm_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gs_send_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '领取日期',
  `gs_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '兑换日期',
  `gs_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '系统时间',
  `gs_type` tinyint(1) NULL DEFAULT 0 COMMENT '0前台1线上',
  `gs_check_no` varchar(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '验证码',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`gs_id`) USING BTREE,
  INDEX `IDX_GS_CO_SHOP`(`companyid` ASC, `gs_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 176 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_giftlist
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_giftlist`;
CREATE TABLE `t_shop_giftlist`  (
  `gil_id` int NOT NULL AUTO_INCREMENT,
  `gil_sub_code` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '子码',
  `gil_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `gil_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '颜色',
  `gil_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '尺码',
  `gil_szg_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '尺码组编号',
  `gil_bs_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型',
  `gil_totalamount` int NOT NULL DEFAULT 0 COMMENT '礼品数量',
  `gil_getamount` int NOT NULL DEFAULT 0 COMMENT '领取数量',
  `gil_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `gil_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '发放店铺',
  `gil_user_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户code',
  `companyid` int NOT NULL,
  PRIMARY KEY (`gil_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_giftlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_giftlist_temp`;
CREATE TABLE `t_shop_giftlist_temp`  (
  `gil_id` int NOT NULL AUTO_INCREMENT,
  `gil_sub_code` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '子码',
  `gil_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `gil_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `gil_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码',
  `gil_szg_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码组编号',
  `gil_bs_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型',
  `gil_totalamount` int NULL DEFAULT 0 COMMENT '礼品数量',
  `gil_getamount` int NULL DEFAULT 0 COMMENT '领取数量',
  `gil_sysdate` datetime NULL DEFAULT NULL,
  `gil_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '发放店铺',
  `gil_user_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户code',
  `gil_addamount` int NULL DEFAULT 0 COMMENT '修改时补充发布数量',
  `gil_point` int NULL DEFAULT 0 COMMENT '兑换积分数',
  `companyid` int NOT NULL,
  PRIMARY KEY (`gil_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_kpiassess
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_kpiassess`;
CREATE TABLE `t_shop_kpiassess`  (
  `ka_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ka_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '单据编号',
  `ka_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型：0-店铺，1-员工组，2-员工',
  `ka_begin` date NULL DEFAULT NULL COMMENT '开始时间',
  `ka_end` date NULL DEFAULT NULL COMMENT '结束时间',
  `ka_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ka_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '考核是否总结 0 未总结 1 总结',
  `ka_summary` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '总结内容',
  `ka_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `ka_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '登录店铺',
  `ka_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`ka_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = 'KPI指标考核主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_kpiassesslist
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_kpiassesslist`;
CREATE TABLE `t_shop_kpiassesslist`  (
  `kal_id` int NOT NULL AUTO_INCREMENT,
  `kal_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '指标考核单据编号',
  `kal_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '类型编号(ka_type:0-店铺编号,1-员工组编号,2-员工编号)',
  `kal_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '类型编号(ka_type:0-店铺名称,1-员工组名称,2-员工名称)',
  `kal_ki_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '考核指标编号',
  `kal_complete` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '考核完成情况',
  `kal_score` int NULL DEFAULT 0 COMMENT '获得分数',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`kal_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1178 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '指标考核明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_kpiassessreward
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_kpiassessreward`;
CREATE TABLE `t_shop_kpiassessreward`  (
  `kar_id` int NOT NULL AUTO_INCREMENT,
  `kar_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '指标考核单据编号',
  `kar_kal_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '店铺编号或者员工组编号或者员工编号',
  `kar_ki_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '指标编号',
  `kar_rw_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '勋章编号',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`kar_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = 'KPI考核奖励发放' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_kpipk
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_kpipk`;
CREATE TABLE `t_shop_kpipk`  (
  `kp_id` int NOT NULL AUTO_INCREMENT,
  `kp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT 'PK单据编号',
  `kp_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型：0-店铺，1-员工组，2-员工',
  `kp_begin` date NULL DEFAULT NULL COMMENT '开始时间',
  `kp_end` date NULL DEFAULT NULL COMMENT '结束时间',
  `kp_score` int NULL DEFAULT 0 COMMENT '标准分',
  `kp_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `kp_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否总结 0 未总结 1 总结',
  `kp_summary` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '总结内容',
  `kp_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `kp_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '登录店铺',
  `kp_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`kp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = 'PK工具主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_kpipklist
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_kpipklist`;
CREATE TABLE `t_shop_kpipklist`  (
  `kpl_id` int NOT NULL AUTO_INCREMENT,
  `kpl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT 'PK单据编号',
  `kpl_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '类型编号(kp_type:0-店铺编号,1-员工组编号,2-员工编号)',
  `kpl_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '类型名称(kp_type:0-店铺名称,1-员工组名称,2-员工名称)',
  `kpl_ki_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT 'KPI编号',
  `kpl_complete` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT 'KPI完成情况',
  `kpl_score` int NULL DEFAULT 0 COMMENT '分数',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`kpl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 490 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = 'PK工具明细表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_kpipkreward
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_kpipkreward`;
CREATE TABLE `t_shop_kpipkreward`  (
  `kpr_id` int NOT NULL AUTO_INCREMENT,
  `kpr_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT 'PK单据编号',
  `kpr_rw_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '奖励编号',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`kpr_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = 'KPI PK奖励发放' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_monthplan
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_monthplan`;
CREATE TABLE `t_shop_monthplan`  (
  `mp_id` int NOT NULL AUTO_INCREMENT,
  `mp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `mp_shop_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `mp_year` int NULL DEFAULT 0 COMMENT '年份',
  `mp_month` int NULL DEFAULT 0 COMMENT '月份',
  `mp_sell_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划销售',
  `mp_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '月度摘要',
  `mp_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `mp_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0:未审核 1:审核通过 2:审核未通过',
  `mp_ar_date` datetime NULL DEFAULT NULL,
  `mp_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `mp_sysdate` datetime NULL DEFAULT NULL COMMENT '制定日期',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`mp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_monthplan_day
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_monthplan_day`;
CREATE TABLE `t_shop_monthplan_day`  (
  `mpd_id` int NOT NULL AUTO_INCREMENT,
  `mpd_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `mpd_year` int NULL DEFAULT 0 COMMENT '年份',
  `mpd_month` int NULL DEFAULT 0 COMMENT '月份',
  `mpd_day` int NULL DEFAULT 0 COMMENT '天',
  `mpd_sell_money_plan` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划销售',
  `mpd_sell_money_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期销售',
  `mpd_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`mpd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 429 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_plan
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_plan`;
CREATE TABLE `t_shop_plan`  (
  `pl_id` int NOT NULL AUTO_INCREMENT,
  `pl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `pl_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `pl_date` date NULL DEFAULT NULL COMMENT '制定日期',
  `pl_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '门店ID',
  `pl_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '年度计划名称',
  `pl_year` int NULL DEFAULT 0 COMMENT '年份',
  `pl_type` tinyint(1) NULL DEFAULT 0 COMMENT '计划方式：0-参考同期数据制定，1-根据预估费用制定，2-目标销售金额制定',
  `pl_gross_profit_rate` decimal(4, 2) NULL DEFAULT 0.00 COMMENT '预计毛利率',
  `pl_sell_grow_percent` decimal(4, 2) NULL DEFAULT 0.00 COMMENT '同期销售增长百分比(type=0)',
  `pl_expense_grow_percent` decimal(4, 2) NULL DEFAULT 0.00 COMMENT '费用开支增长百分比(type=0)',
  `pl_breakeven_grow_percent` decimal(4, 2) NULL DEFAULT 0.00 COMMENT '盈亏平衡增长百分比(type=1)',
  `pl_sell_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划销售金额',
  `pl_cost_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划成本金额',
  `pl_gross_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划毛利润',
  `pl_net_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划净利润',
  `pl_breakeven` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划盈亏平衡点',
  `pl_expense` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划费用开支',
  `pl_sell_money_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期销售金额',
  `pl_cost_money_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期成本金额',
  `pl_gross_money_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期毛利润',
  `pl_net_money_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期净利润',
  `pl_breakeven_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期盈亏平衡点',
  `pl_expense_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期费用开支',
  `pl_gross_profit_rate_pre` decimal(4, 2) NULL DEFAULT 0.00 COMMENT '同期毛利率',
  `pl_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `pl_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0:未审核 1:审核通过 2:审核未通过',
  `pl_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `pl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '计划摘要',
  `pl_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '计划制定人',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`pl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_plan_expense
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_plan_expense`;
CREATE TABLE `t_shop_plan_expense`  (
  `pe_id` int NOT NULL AUTO_INCREMENT,
  `pe_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `pe_expensecode` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用编号',
  `pe_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '费用金额',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`pe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_plan_month
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_plan_month`;
CREATE TABLE `t_shop_plan_month`  (
  `pm_id` int NOT NULL AUTO_INCREMENT,
  `pm_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `pm_year` int NULL DEFAULT 0 COMMENT '年份',
  `pm_month` int NULL DEFAULT 0 COMMENT '月份',
  `pm_sell_money_plan` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '计划销售',
  `pm_sell_money_pre` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '同期销售',
  `pm_sell_money_real` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '当月实际销售金额',
  `pm_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '月度摘要',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`pm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 241 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_price
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_price`;
CREATE TABLE `t_shop_price`  (
  `sp_id` int NOT NULL AUTO_INCREMENT,
  `sp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '单据编号',
  `sp_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `sp_makerdate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sp_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `sp_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `sp_is_sellprice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识是否修改了零售价',
  `sp_is_vipprice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识是否修改了会员价',
  `sp_is_sortprice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识是否修改了配送价',
  `sp_is_costprice` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识是否修改了成本价',
  `sp_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态: 0未审核  1审核通过   2审核不通过',
  `sp_shop_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '调价店铺类型：3-自营店; 4-加盟店; 5-合伙店',
  `sp_us_id` int NULL DEFAULT NULL,
  `sp_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sp_id`) USING BTREE,
  UNIQUE INDEX `PK_PMP_Id`(`sp_id` ASC) USING BTREE,
  INDEX `Index_PMP_Companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_price_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_price_shop`;
CREATE TABLE `t_shop_price_shop`  (
  `sps_id` int NOT NULL AUTO_INCREMENT,
  `sps_number` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品调价单编号',
  `sps_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`sps_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_pricelist
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_pricelist`;
CREATE TABLE `t_shop_pricelist`  (
  `spl_id` int NOT NULL AUTO_INCREMENT,
  `spl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `spl_pd_code` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `spl_pd_point` tinyint(1) NULL DEFAULT NULL COMMENT '积分0否1是',
  `spl_old_sellprice` double(8, 2) NULL DEFAULT NULL COMMENT '原来的零售价',
  `spl_new_sellprice` double(8, 2) NULL DEFAULT NULL COMMENT '现在的零售价',
  `spl_old_vipprice` double(8, 2) NULL DEFAULT NULL COMMENT '原来的会员价',
  `spl_new_vipprice` double(8, 2) NULL DEFAULT NULL COMMENT '现在的零售价',
  `spl_old_sortprice` double(8, 2) NULL DEFAULT NULL COMMENT '原来的配送价',
  `spl_new_sortprice` double(8, 2) NULL DEFAULT NULL COMMENT '现在的零售价',
  `spl_old_costprice` double(8, 2) NULL DEFAULT NULL,
  `spl_new_costprice` double(8, 2) NULL DEFAULT NULL,
  `spl_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺 编号',
  `spl_us_id` int NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`spl_id`) USING BTREE,
  UNIQUE INDEX `PK_PMPL_Id`(`spl_id` ASC) USING BTREE,
  INDEX `Index_PMPL_Pd_Code`(`spl_pd_code` ASC) USING BTREE,
  INDEX `Index_PMPL_Companyid`(`companyid` ASC) USING BTREE,
  INDEX `Index_PMPL_Shop_Code`(`spl_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_pricelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_pricelist_temp`;
CREATE TABLE `t_shop_pricelist_temp`  (
  `spl_id` int NOT NULL AUTO_INCREMENT,
  `spl_number` varchar(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '单据编号',
  `spl_pd_code` varchar(8) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '商品编号',
  `spl_pd_point` tinyint(1) NULL DEFAULT NULL COMMENT '积分0否1是',
  `spl_old_sellprice` double(8, 2) NULL DEFAULT NULL COMMENT '原来的零售价',
  `spl_new_sellprice` double(8, 2) NULL DEFAULT NULL COMMENT '现在的零售价',
  `spl_old_vipprice` double(8, 2) NULL DEFAULT NULL COMMENT '原来的会员价',
  `spl_new_vipprice` double(8, 2) NULL DEFAULT NULL COMMENT '现在的零售价',
  `spl_old_sortprice` double(8, 2) NULL DEFAULT NULL COMMENT '原来的配送价',
  `spl_new_sortprice` double(8, 2) NULL DEFAULT NULL COMMENT '现在的零售价',
  `spl_old_costprice` double(8, 2) NULL DEFAULT NULL,
  `spl_new_costprice` double(8, 2) NULL DEFAULT NULL,
  `spl_shop_code` char(6) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '店铺 编号',
  `spl_us_id` int NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`spl_id`) USING BTREE,
  UNIQUE INDEX `PK_PMPL_Id`(`spl_id` ASC) USING BTREE,
  INDEX `Index_PMPL_Pd_Code`(`spl_pd_code` ASC) USING BTREE,
  INDEX `Index_PMPL_Companyid`(`companyid` ASC) USING BTREE,
  INDEX `Index_PMPL_Shop_Code`(`spl_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_program
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_program`;
CREATE TABLE `t_shop_program`  (
  `sp_id` int NOT NULL AUTO_INCREMENT COMMENT '搭配方案主键',
  `sp_title` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '搭配方案标题',
  `sp_info` varchar(400) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '搭配方案说明',
  `sp_imgpath` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '搭配方案图片路径',
  `sp_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `sp_us_name` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '发布人',
  `sp_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建日期',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`sp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_program_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_program_shop`;
CREATE TABLE `t_shop_program_shop`  (
  `sps_id` int NOT NULL AUTO_INCREMENT,
  `sps_sp_id` int NULL DEFAULT NULL COMMENT '商品调价单编号',
  `sps_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `sps_state` tinyint(1) NULL DEFAULT 0 COMMENT '0:未阅读  1:已阅读',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sps_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale`;
CREATE TABLE `t_shop_sale`  (
  `ss_id` int NOT NULL AUTO_INCREMENT COMMENT '促销ID',
  `ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '促销编号',
  `ss_name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '促销方案名称',
  `ss_begin_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '促销开始时间',
  `ss_end_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '促销结束时间',
  `ss_week` varchar(7) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '周几',
  `ss_mt_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '会员类别编号',
  `ss_point` tinyint(1) NOT NULL DEFAULT 1 COMMENT '积分：0否1是',
  `ss_manager` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `ss_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `ss_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态0未审批1已通过2已退回4已终止',
  `ss_priority` tinyint(1) NOT NULL DEFAULT 1 COMMENT '优先级1开始',
  `ss_model` varchar(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '模式类型',
  `ss_current_spcode` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建促销人所在店铺编号',
  `companyid` int NOT NULL,
  PRIMARY KEY (`ss_id`) USING BTREE,
  INDEX `IDX_SS_CO_SS`(`companyid` ASC, `ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 196 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_all
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_all`;
CREATE TABLE `t_shop_sale_all`  (
  `ssa_id` int NOT NULL AUTO_INCREMENT COMMENT '范围全场ID',
  `ssa_ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '促销信息编号',
  `ssa_discount` double(5, 2) NULL DEFAULT 0.00 COMMENT '折扣',
  `ssa_buy_full` double(8, 2) NULL DEFAULT 0.00 COMMENT '买满金额',
  `ssa_buy_number` int NULL DEFAULT 0 COMMENT '买满数量',
  `ssa_donation_number` int NULL DEFAULT NULL COMMENT '赠送数量',
  `ssa_donation_amount` double(8, 2) NULL DEFAULT NULL COMMENT '赠送金额',
  `ssa_reduce_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '减少金额',
  `ssa_increased_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '增加金额',
  `ssa_index` tinyint(1) NULL DEFAULT 0 COMMENT '序列号',
  `companyid` int NOT NULL,
  PRIMARY KEY (`ssa_id`) USING BTREE,
  INDEX `IDX_SSA_CO_SS`(`companyid` ASC, `ssa_ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_brand
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_brand`;
CREATE TABLE `t_shop_sale_brand`  (
  `ssb_id` int NOT NULL AUTO_INCREMENT COMMENT '范围品牌ID',
  `ssb_ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '促销信息编号',
  `ssb_bd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '品牌编号',
  `ssb_discount` double(4, 2) NULL DEFAULT 0.00 COMMENT '折扣',
  `ssb_buy_full` double(8, 2) NULL DEFAULT 0.00 COMMENT '买满金额',
  `ssb_buy_number` int NULL DEFAULT 0 COMMENT '买满数量',
  `ssb_donation_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '赠送金额',
  `ssb_reduce_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '减少金额',
  `ssb_increased_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '增加金额',
  `ssb_index` tinyint(1) NULL DEFAULT 0 COMMENT '对应列数',
  `companyid` int NOT NULL,
  PRIMARY KEY (`ssb_id`) USING BTREE,
  INDEX `IDX_SSB_CO_SS`(`companyid` ASC, `ssb_ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_model
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_model`;
CREATE TABLE `t_shop_sale_model`  (
  `ssm_code` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '',
  `ssm_model` int NULL DEFAULT NULL,
  `ssm_model_explain` char(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ssm_scope` int NULL DEFAULT NULL,
  `ssm_scope_explain` char(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ssm_rule` int NULL DEFAULT NULL,
  `ssm_rule_explain` char(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ssm_code`) USING BTREE,
  INDEX `IDX_SSM_CODE`(`ssm_code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_present
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_present`;
CREATE TABLE `t_shop_sale_present`  (
  `ssp_id` int NOT NULL AUTO_INCREMENT COMMENT '赠送商品ID',
  `ssp_ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '促销信息编号',
  `ssp_subcode` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ssp_cr_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色编号',
  `ssp_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码编号',
  `ssp_br_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型编号',
  `ssp_count` int NULL DEFAULT 0 COMMENT '赠送数量',
  `ssp_pd_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '货号',
  `ssp_index` tinyint(1) NULL DEFAULT 0 COMMENT '对应列数',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`ssp_id`) USING BTREE,
  INDEX `IDX_SSP_CO_SS`(`companyid` ASC, `ssp_ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_product
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_product`;
CREATE TABLE `t_shop_sale_product`  (
  `ssp_id` int NOT NULL AUTO_INCREMENT COMMENT '范围商品ID',
  `ssp_ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '促销信息编号',
  `ssp_pd_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品货号',
  `ssp_discount` double(8, 2) NULL DEFAULT 0.00 COMMENT '折扣',
  `ssp_buy_full` double(8, 2) NULL DEFAULT 0.00 COMMENT '买满金额',
  `ssp_buy_number` int NULL DEFAULT 0 COMMENT '买满数量',
  `ssp_donation_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '赠送金额',
  `ssp_reduce_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '减少金额',
  `ssp_increased_amount` double(8, 2) NULL DEFAULT 0.00 COMMENT '增加金额',
  `ssp_special_offer` double(8, 2) NULL DEFAULT 0.00 COMMENT '特价',
  `ssp_index` tinyint(1) NULL DEFAULT 0 COMMENT '对应列数',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`ssp_id`) USING BTREE,
  INDEX `IDX_SSP_CO_SS`(`companyid` ASC, `ssp_ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 750 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_shop`;
CREATE TABLE `t_shop_sale_shop`  (
  `sss_id` int NOT NULL AUTO_INCREMENT,
  `sss_ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销编号',
  `sss_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`sss_id`) USING BTREE,
  INDEX `IDX_SSS_CO_SP_SS`(`companyid` ASC, `sss_shop_code` ASC, `sss_ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 405 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_sale_type
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_sale_type`;
CREATE TABLE `t_shop_sale_type`  (
  `sst_id` int NOT NULL AUTO_INCREMENT COMMENT '范围类型ID',
  `sst_ss_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '促销信息编号',
  `sst_tp_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '类别编号',
  `sst_discount` double(4, 2) NULL DEFAULT NULL COMMENT '折扣',
  `sst_buy_full` double(8, 2) NULL DEFAULT NULL COMMENT '买满金额',
  `sst_buy_number` int NULL DEFAULT NULL COMMENT '买满数量',
  `sst_donation_amount` double(8, 2) NULL DEFAULT NULL COMMENT '赠送金额',
  `sst_reduce_amount` double(8, 2) NULL DEFAULT NULL COMMENT '减少金额',
  `sst_increased_amount` double(8, 2) NULL DEFAULT NULL COMMENT '增加金额',
  `sst_index` tinyint(1) NULL DEFAULT 0 COMMENT '对应列数',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sst_id`) USING BTREE,
  INDEX `IDX_SST_CO_SS`(`companyid` ASC, `sst_ss_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 192 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_service
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_service`;
CREATE TABLE `t_shop_service`  (
  `ss_id` int NOT NULL AUTO_INCREMENT,
  `ss_pd_no` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品货号',
  `ss_name` varchar(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '客户姓名',
  `ss_tel` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `ss_startdate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '服务接受时间',
  `ss_servicedate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '维修完成时间',
  `ss_enddate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '取货时间',
  `ss_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '接受受理人名称',
  `ss_pickupname` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '取货受理人名称',
  `ss_question` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '存在问题',
  `ss_hadsolve` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '解决问题',
  `ss_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0：已接受  1：已维修  2：已取走 3.预约',
  `ss_sms_count` int NULL DEFAULT 0 COMMENT '短信发送条数',
  `ss_satis` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '满意度',
  `ss_result` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '维护结果',
  `ss_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `ss_ca_emcode` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '收银员Code',
  `ss_img_path` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `ss_ssi_id` int NULL DEFAULT NULL COMMENT '维修项目id',
  `ss_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '预约时间',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`ss_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_service_item
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_service_item`;
CREATE TABLE `t_shop_service_item`  (
  `ssi_id` int NOT NULL AUTO_INCREMENT,
  `ssi_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NOT NULL,
  PRIMARY KEY (`ssi_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_target
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_target`;
CREATE TABLE `t_shop_target`  (
  `ta_id` int NOT NULL AUTO_INCREMENT COMMENT '目标ID',
  `ta_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '目标编号',
  `ta_type` int NULL DEFAULT 1 COMMENT '目标类别 0:其他 1 日报 2 周报 3 月报',
  `ta_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `ta_begintime` datetime NULL DEFAULT NULL COMMENT '目标开始时间',
  `ta_endtime` datetime NULL DEFAULT NULL COMMENT '目标结束时间',
  `ta_money` double(8, 2) NULL DEFAULT NULL COMMENT '目标金额',
  `ta_realmoney` double(8, 2) NULL DEFAULT 0.00 COMMENT '实际完成金额',
  `ta_vipcount` int NULL DEFAULT 0 COMMENT '目标会员卡数',
  `ta_realcount` int NULL DEFAULT 0 COMMENT '实际开卡数',
  `ta_state` tinyint(1) NULL DEFAULT NULL COMMENT '目标是否总结 0 未总结 1 总结',
  `ta_complete` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '目标完成情况总结',
  `ta_hinder` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '目标问题和障碍总结',
  `ta_method` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '目标对策和方法总结',
  `ta_gain` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '目标创新与收获总结',
  `ta_lead` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '领导点评',
  `ta_createtime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `ta_summary` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '摘要信息',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ta_id`) USING BTREE,
  INDEX `Index_ta_number`(`ta_number` ASC) USING BTREE,
  INDEX `Index_Companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_target_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_target_detail`;
CREATE TABLE `t_shop_target_detail`  (
  `tad_id` int NOT NULL AUTO_INCREMENT COMMENT '目标明细编号',
  `tad_ta_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `tad_em_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '员工编号',
  `tad_em_name` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '员工名称',
  `tad_target_money` decimal(9, 2) NULL DEFAULT NULL COMMENT '目标金额',
  `tad_real_money` decimal(9, 2) NULL DEFAULT NULL COMMENT '实际完成金额',
  `tad_vip_count` int NULL DEFAULT 0 COMMENT '办理会员卡数',
  `tad_real_count` int NULL DEFAULT 0 COMMENT '实际开卡数',
  `tad_project_type` tinyint(1) NULL DEFAULT NULL COMMENT '项目类型:1-品牌，2-类别',
  `tad_remark` char(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `tad_select` char(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '选择的ID',
  `tad_target_content` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '项目内容',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`tad_id`) USING BTREE,
  INDEX `Index_tad_ta_number`(`tad_ta_number` ASC) USING BTREE,
  INDEX `Index_companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_target_detaillist
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_target_detaillist`;
CREATE TABLE `t_shop_target_detaillist`  (
  `tadl_id` int NOT NULL AUTO_INCREMENT,
  `tadl_ta_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `tadl_em_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '员工编号',
  `tadl_project_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '项目编号',
  `tadl_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '目标金额',
  `tadl_real_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '完成金额',
  `tadl_remark` char(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`tadl_id`) USING BTREE,
  INDEX `Index_tadl_ta_number`(`tadl_ta_number` ASC) USING BTREE,
  INDEX `Index_companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_target_detailmedal
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_target_detailmedal`;
CREATE TABLE `t_shop_target_detailmedal`  (
  `tadm_id` int NOT NULL AUTO_INCREMENT,
  `tadm_ta_number` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `tadm_em_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '员工编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`tadm_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_want
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_want`;
CREATE TABLE `t_shop_want`  (
  `wt_id` int NOT NULL AUTO_INCREMENT,
  `wt_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `wt_date` date NULL DEFAULT NULL COMMENT '补货日期',
  `wt_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '门店ID',
  `wt_outdp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '发货仓库',
  `wt_indp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '收获仓库',
  `wt_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `wt_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `wt_handnumber` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手工单号',
  `wt_applyamount` int NULL DEFAULT 0 COMMENT '申请数量',
  `wt_sendamount` int NULL DEFAULT 0 COMMENT '实发数量',
  `wt_applymoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '申请金额',
  `wt_sendmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实发总金额',
  `wt_sendcostmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实发总成本',
  `wt_property` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '单据性质',
  `wt_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `wt_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已审核/待发货 2 已退回 3 已发货/在途 4 完成 5 拒收',
  `wt_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `wt_ar_usid` int NULL DEFAULT NULL COMMENT '指定审批人id，若不指定，有审批权限的人都可以审批',
  `wt_type` tinyint(1) NULL DEFAULT 0 COMMENT '状态  0:补货  1:退货 ',
  `wt_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `wt_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `wt_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '是否草稿 0 否 1 是',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`wt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1097 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_wantlist
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_wantlist`;
CREATE TABLE `t_shop_wantlist`  (
  `wtl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wtl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `wtl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `wtl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `wtl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `wtl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `wtl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `wtl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `wtl_applyamount` int NULL DEFAULT 0 COMMENT '申请数量',
  `wtl_sendamount` int NULL DEFAULT 0 COMMENT '实发数量',
  `wtl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `wtl_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `wtl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `wtl_type` tinyint(1) NULL DEFAULT 0 COMMENT '状态  0:补货  1:退货 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`wtl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22203 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shop_wantlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_shop_wantlist_temp`;
CREATE TABLE `t_shop_wantlist_temp`  (
  `wtl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wtl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `wtl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `wtl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `wtl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `wtl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `wtl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `wtl_applyamount` int NULL DEFAULT 0 COMMENT '申请数量',
  `wtl_sendamount` int NULL DEFAULT 0 COMMENT '实发数量',
  `wtl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `wtl_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `wtl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `wtl_type` tinyint(1) NULL DEFAULT 0 COMMENT '状态  0:补货  1:退货 ',
  `wtl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`wtl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6309 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_allot
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_allot`;
CREATE TABLE `t_sort_allot`  (
  `at_id` int NOT NULL AUTO_INCREMENT,
  `at_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `at_date` date NULL DEFAULT NULL COMMENT '配货日期',
  `at_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '门店ID',
  `at_outdp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '发货仓库',
  `at_indp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '收获仓库',
  `at_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `at_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `at_handnumber` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手工单号',
  `at_applyamount` int NULL DEFAULT 0 COMMENT '申请数量',
  `at_sendamount` int NULL DEFAULT 0 COMMENT '实发数量',
  `at_applymoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '申请金额',
  `at_sendmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实发总金额',
  `at_sendcostmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实发总成本',
  `at_sendsellmoney` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实发零售金额',
  `at_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `at_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '付款帐户id',
  `at_property` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '单据性质',
  `at_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `at_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已审核/待发货 2 已退回 3 已发货/在途 4 完成 5 拒收',
  `at_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `at_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `at_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收金额',
  `at_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `at_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `at_lastdebt` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '上次欠款',
  `at_type` tinyint(1) NULL DEFAULT 0 COMMENT '批发状态  0:配货  1:退货 ',
  `at_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `at_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `at_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '是否草稿 0 否 1 是',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`at_id`) USING BTREE,
  INDEX `IDX_AT_CO_DATE_NO`(`companyid` ASC, `at_date` ASC, `at_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3048 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_allotlist
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_allotlist`;
CREATE TABLE `t_sort_allotlist`  (
  `atl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `atl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `atl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `atl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `atl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `atl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `atl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `atl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `atl_applyamount` int NULL DEFAULT 0 COMMENT '申请数量',
  `atl_sendamount` int NULL DEFAULT 0 COMMENT '实发数量',
  `atl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `atl_sellprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `atl_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `atl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `atl_type` tinyint(1) NULL DEFAULT 0 COMMENT '配货状态  0:配货  1:退货 ',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`atl_id`) USING BTREE,
  INDEX `IDX_ATL_CO_NO`(`companyid` ASC, `atl_number` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58098 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_allotlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_allotlist_temp`;
CREATE TABLE `t_sort_allotlist_temp`  (
  `atl_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `atl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `atl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `atl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `atl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `atl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `atl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `atl_applyamount` int NULL DEFAULT 0 COMMENT '申请数量',
  `atl_sendamount` int NULL DEFAULT 0 COMMENT '实发数量',
  `atl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `atl_sellprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `atl_costprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '成本价',
  `atl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `atl_type` tinyint(1) NULL DEFAULT 0 COMMENT '配货状态  0:配货  1:退货 ',
  `atl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `atl_up` int NULL DEFAULT NULL COMMENT '此字段针对分公司有用，0-配货发货单、退货确认单，1-配货申请单、退货申请单',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`atl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_dealings
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_dealings`;
CREATE TABLE `t_sort_dealings`  (
  `dl_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dl_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '供应商编号',
  `dl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `dl_type` tinyint(1) NULL DEFAULT NULL COMMENT '类型：0-配货发货单；1-退货申请单库；2-期初；3-费用单；4-预收款；5-预收款退款；6-结算单；',
  `dl_discount_money` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `dl_receivable` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `dl_received` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `dl_debt` decimal(12, 2) NULL DEFAULT 0.00 COMMENT '欠款',
  `dl_date` datetime NULL DEFAULT NULL COMMENT '日期',
  `dl_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '操作员',
  `dl_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `dl_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `dl_amount` int NULL DEFAULT NULL COMMENT '数量',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`dl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1787 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_fee
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_fee`;
CREATE TABLE `t_sort_fee`  (
  `fe_id` int NOT NULL AUTO_INCREMENT,
  `fe_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `fe_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '门店编号',
  `fe_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `fe_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `fe_date` date NULL DEFAULT NULL COMMENT '日期',
  `fe_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `fe_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `fe_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总金额',
  `fe_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `fe_pay_state` tinyint(1) NULL DEFAULT 0 COMMENT '付款状态0:未付 1:付部分 2：付清',
  `fe_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收金额',
  `fe_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `fe_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `fe_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `fe_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `fe_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_feelist
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_feelist`;
CREATE TABLE `t_sort_feelist`  (
  `fel_id` bigint NOT NULL AUTO_INCREMENT,
  `fel_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `fel_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用类型编号',
  `fel_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `fel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_feelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_feelist_temp`;
CREATE TABLE `t_sort_feelist_temp`  (
  `fel_id` bigint NOT NULL AUTO_INCREMENT,
  `fel_mp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '费用类型编号',
  `fel_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `fel_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要信息',
  `fel_us_id` int NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`fel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_prepay
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_prepay`;
CREATE TABLE `t_sort_prepay`  (
  `pp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `pp_date` date NULL DEFAULT NULL COMMENT '付款日期',
  `pp_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '门店编号',
  `pp_type` tinyint(1) NULL DEFAULT 0 COMMENT '收支方式：0：应付款；1：应收款',
  `pp_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '金额',
  `pp_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `pp_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `pp_ba_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '银行编号',
  `pp_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `pp_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `pp_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `pp_sysdate` datetime NULL DEFAULT NULL COMMENT '系统日期',
  `pp_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`pp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_settle
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_settle`;
CREATE TABLE `t_sort_settle`  (
  `st_id` int NOT NULL AUTO_INCREMENT,
  `st_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `st_date` date NULL DEFAULT NULL COMMENT '制单日期',
  `st_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '门店编号',
  `st_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '制单人',
  `st_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `st_ba_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '银行编号',
  `st_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `st_prepay` decimal(10, 2) NULL DEFAULT NULL COMMENT '使用预收款金额',
  `st_received` decimal(11, 2) NULL DEFAULT NULL COMMENT '收款金额',
  `st_receivedmore` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实际收款剩余金额',
  `st_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `st_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态 0 待审批 1 已通过 2 已退回',
  `st_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `st_entire` tinyint(1) NULL DEFAULT 0 COMMENT '是否整单结算：1-是，0-否',
  `st_pp_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '预收款单据编号',
  `st_leftdebt` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '剩余欠款',
  `st_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `st_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_settlelist
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_settlelist`;
CREATE TABLE `t_sort_settlelist`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `stl_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收总金额',
  `stl_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_prepay_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已使用预收款金额',
  `stl_unreceivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未收金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `stl_real_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实收金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '配货单单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_type` tinyint(1) NULL DEFAULT NULL COMMENT '配货单类型  0:配货  1:退货 2:期初',
  `stl_ar_state` tinyint(1) NULL DEFAULT NULL COMMENT '单据状态，用于判断是否能参与结算',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sort_settlelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_sort_settlelist_temp`;
CREATE TABLE `t_sort_settlelist_temp`  (
  `stl_id` int NOT NULL AUTO_INCREMENT,
  `stl_receivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '应收总金额',
  `stl_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已收金额',
  `stl_discount_money_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已优惠金额',
  `stl_prepay_yet` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '已使用预收款金额',
  `stl_unreceivable` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '未收金额',
  `stl_discount_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `stl_prepay` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '使用预收款金额',
  `stl_real_received` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '实收金额',
  `stl_bill_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '配货单单据编号',
  `stl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `stl_type` tinyint(1) NULL DEFAULT NULL COMMENT '配货单类型  0:配货  1:退货 2:期初',
  `stl_join` tinyint(1) NULL DEFAULT NULL COMMENT '是否参与结算：1-是，0-否',
  `stl_ar_state` tinyint(1) NULL DEFAULT NULL COMMENT '单据状态，用于判断是否能参与结算',
  `stl_us_id` int NULL DEFAULT NULL COMMENT '用户Id',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  PRIMARY KEY (`stl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 106 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_adjust
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_adjust`;
CREATE TABLE `t_stock_adjust`  (
  `aj_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `aj_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `aj_date` date NULL DEFAULT NULL COMMENT '日期',
  `aj_dp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '仓库code',
  `aj_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人ID',
  `aj_amount` int NULL DEFAULT 0 COMMENT '总计数量',
  `aj_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '总计金额',
  `aj_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `aj_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态:0：未审核；1：已审核2：已退回',
  `aj_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `aj_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `aj_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `aj_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`aj_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 460 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_adjustlist
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_adjustlist`;
CREATE TABLE `t_stock_adjustlist`  (
  `ajl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ajl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `ajl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ajl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ajl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ajl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ajl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ajl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ajl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `ajl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ajl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ajl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2320 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_adjustlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_adjustlist_temp`;
CREATE TABLE `t_stock_adjustlist_temp`  (
  `ajl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ajl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ajl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ajl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ajl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ajl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ajl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ajl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `ajl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ajl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ajl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ajl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 229 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_allocate
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_allocate`;
CREATE TABLE `t_stock_allocate`  (
  `ac_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ac_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `ac_date` date NULL DEFAULT NULL COMMENT '调拨日期',
  `ac_outdp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '调出仓库code',
  `ac_indp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '调入仓库code',
  `ac_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人ID',
  `ac_amount` int NULL DEFAULT 0 COMMENT '总计数量',
  `ac_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '总计金额',
  `ac_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ac_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态:0：未审核；1：已审核2：已退回',
  `ac_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `ac_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `ac_sysdate` datetime NULL DEFAULT NULL COMMENT '日期',
  `ac_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`ac_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1207 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_allocatelist
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_allocatelist`;
CREATE TABLE `t_stock_allocatelist`  (
  `acl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `acl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `acl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `acl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `acl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `acl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `acl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `acl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `acl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `acl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `acl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`acl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7028 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_allocatelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_allocatelist_temp`;
CREATE TABLE `t_stock_allocatelist_temp`  (
  `acl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `acl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `acl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `acl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `acl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `acl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `acl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `acl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `acl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `acl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `acl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`acl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5018 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_batch
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_batch`;
CREATE TABLE `t_stock_batch`  (
  `ba_id` int NOT NULL AUTO_INCREMENT,
  `ba_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点申请批号',
  `ba_date` datetime NULL DEFAULT NULL COMMENT '盘点批号申请日期',
  `ba_dp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '盘点仓库',
  `ba_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人',
  `ba_scope` tinyint(1) NULL DEFAULT 0 COMMENT '盘点范围0全场盘点1类别盘点2品牌盘点3单品盘点4年份盘点5季节盘点',
  `ba_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ba_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态:0：未审核；1：已审核 录入;2：已退回;3：处理',
  `ba_isexec` tinyint(1) NULL DEFAULT 0 COMMENT '是否处理，审批完成后才能处理 0未处理1全部处理2部分处理',
  `ba_execdate` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `ba_amount` int NULL DEFAULT 0,
  `ba_stockamount` int NULL DEFAULT 0,
  `ba_costmoney` decimal(11, 2) NULL DEFAULT 0.00,
  `ba_retailmoney` decimal(11, 2) NULL DEFAULT 0.00,
  `ba_us_id` int NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`ba_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 182 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_batchscope
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_batchscope`;
CREATE TABLE `t_stock_batchscope`  (
  `bs_id` int NOT NULL AUTO_INCREMENT,
  `bs_ba_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点申请批号',
  `bs_scope` tinyint(1) NULL DEFAULT 0 COMMENT '盘点范围0全场盘点1类别盘点2品牌盘点3单品盘点4年份盘点5季节盘点',
  `bs_scope_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '具体项目编号',
  `bs_scope_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '具体项目名称',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`bs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_check
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_check`;
CREATE TABLE `t_stock_check`  (
  `ck_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ck_ba_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点申请批号',
  `ck_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点单编号',
  `ck_date` datetime NULL DEFAULT NULL COMMENT '盘点日期',
  `ck_dp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点仓库ID',
  `ck_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人',
  `ck_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ck_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `ck_isexec` tinyint(1) NULL DEFAULT 0 COMMENT '是否处理，审批完成后才能处理 0未处理1全部处理2部分处理',
  `ck_execdate` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `ck_amount` int NULL DEFAULT 0 COMMENT '总计数量',
  `ck_stockamount` int NULL DEFAULT 0 COMMENT '库存',
  `ck_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总计金额',
  `ck_retailmoney` decimal(11, 2) NULL DEFAULT 0.00,
  `ck_us_id` int NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`ck_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 233 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_checklist
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_checklist`;
CREATE TABLE `t_stock_checklist`  (
  `ckl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ckl_ba_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点批次编号',
  `ckl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `ckl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ckl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ckl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ckl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ckl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ckl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ckl_stock_amount` int NULL DEFAULT 0 COMMENT '实际库存数量',
  `ckl_amount` int NULL DEFAULT 0 COMMENT '实际盘点数量',
  `ckl_unitprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ckl_retailprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `ckl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ckl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78956 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_checklist_all
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_checklist_all`;
CREATE TABLE `t_stock_checklist_all`  (
  `ckl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ckl_ba_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '盘点批次编号',
  `ckl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ckl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ckl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ckl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ckl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ckl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ckl_stock_amount` int NULL DEFAULT 0 COMMENT '实际库存数量',
  `ckl_amount` int NULL DEFAULT 0 COMMENT '实际盘点数量',
  `ckl_unitprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ckl_retailprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `ckl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ckl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 83240 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_checklist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_checklist_temp`;
CREATE TABLE `t_stock_checklist_temp`  (
  `ckl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ckl_ba_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '盘点批次编号',
  `ckl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `ckl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ckl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ckl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ckl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ckl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ckl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ckl_stock_amount` int NULL DEFAULT 0 COMMENT '实际库存数量',
  `ckl_amount` int NULL DEFAULT 0 COMMENT '实际盘点数量',
  `ckl_unitprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ckl_retailprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `ckl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ckl_isrepair` tinyint(1) NULL DEFAULT 0 COMMENT '补盘:1是0否',
  `ckl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ckl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72170 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_data
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_data`;
CREATE TABLE `t_stock_data`  (
  `sd_id` bigint NOT NULL AUTO_INCREMENT,
  `sd_pd_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品编号',
  `sd_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商品子码',
  `sd_cr_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '颜色编号',
  `sd_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '尺码编号',
  `sd_br_code` char(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型编号',
  `sd_dp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '仓库编号',
  `sd_amount` int NOT NULL DEFAULT 1 COMMENT '实际库存',
  `sd_init` int NULL DEFAULT 0 COMMENT '初始库存',
  `sd_date` datetime NOT NULL COMMENT '创建日期，方便查询客户的操作库存是否正确',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`sd_id`) USING BTREE,
  INDEX `IDX_SD_CO_DP`(`companyid` ASC, `sd_dp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124427 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_data_copy
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_data_copy`;
CREATE TABLE `t_stock_data_copy`  (
  `sd_id` bigint NOT NULL AUTO_INCREMENT,
  `sd_pd_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `sd_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品子码',
  `sd_cr_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色编号',
  `sd_sz_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码编号',
  `sd_br_code` char(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型编号',
  `sd_dp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '仓库编号',
  `sd_amount` int NULL DEFAULT 1 COMMENT '实际库存',
  `sd_init` int NULL DEFAULT NULL COMMENT '初始库存',
  `sd_date` datetime NULL DEFAULT NULL COMMENT '创建日期，方便查询客户的操作库存是否正确',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`sd_id`) USING BTREE,
  INDEX `IDX_SD_CO_DP`(`companyid` ASC, `sd_dp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38234 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_loss
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_loss`;
CREATE TABLE `t_stock_loss`  (
  `lo_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `lo_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `lo_date` date NULL DEFAULT NULL COMMENT '日期',
  `lo_dp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '仓库code',
  `lo_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人ID',
  `lo_amount` int NULL DEFAULT 0 COMMENT '总计数量',
  `lo_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总计金额',
  `lo_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `lo_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态:0：未审核；1：已审核2：已退回',
  `lo_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `lo_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `lo_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `lo_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `lo_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`lo_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_losslist
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_losslist`;
CREATE TABLE `t_stock_losslist`  (
  `lol_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `lol_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `lol_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `lol_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `lol_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `lol_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `lol_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `lol_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `lol_amount` int NULL DEFAULT 0 COMMENT '数量',
  `lol_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `lol_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`lol_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29854 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_losslist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_losslist_temp`;
CREATE TABLE `t_stock_losslist_temp`  (
  `lol_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `lol_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `lol_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `lol_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `lol_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `lol_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `lol_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `lol_amount` int NULL DEFAULT 0 COMMENT '数量',
  `lol_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `lol_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `lol_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`lol_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_overflow
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_overflow`;
CREATE TABLE `t_stock_overflow`  (
  `of_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `of_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `of_date` date NULL DEFAULT NULL COMMENT '日期',
  `of_dp_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '仓库code',
  `of_manager` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人ID',
  `of_amount` int NULL DEFAULT 0 COMMENT '总计数量',
  `of_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '总计金额',
  `of_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `of_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态:0：未审核；1：已审核2：已退回',
  `of_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `of_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `of_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `of_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `of_maker` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '制单人',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`of_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 153 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_overflowlist
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_overflowlist`;
CREATE TABLE `t_stock_overflowlist`  (
  `ofl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ofl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '单据编号',
  `ofl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ofl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ofl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ofl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ofl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ofl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ofl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `ofl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ofl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ofl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18365 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_overflowlist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_overflowlist_temp`;
CREATE TABLE `t_stock_overflowlist_temp`  (
  `ofl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ofl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `ofl_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '子码',
  `ofl_szg_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码组ID',
  `ofl_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '尺码ID',
  `ofl_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '颜色ID',
  `ofl_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣编号',
  `ofl_amount` int NULL DEFAULT 0 COMMENT '数量',
  `ofl_unitprice` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '单价',
  `ofl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `ofl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`ofl_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_price
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_price`;
CREATE TABLE `t_stock_price`  (
  `pc_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pc_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '调价单号',
  `pc_date` date NULL DEFAULT NULL COMMENT '调价日期',
  `pc_manager` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人ID',
  `pc_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注',
  `pc_ar_state` tinyint(1) NULL DEFAULT 0 COMMENT '审核状态:0：未审核；1：已审核2：已退回',
  `pc_ar_date` datetime NULL DEFAULT NULL COMMENT '审核日期',
  `pc_isdraft` tinyint(1) NULL DEFAULT 0 COMMENT '草稿 0 不是 1 是',
  `pc_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `pc_us_id` int NULL DEFAULT NULL COMMENT '用户ID',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号ID',
  PRIMARY KEY (`pc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_pricelist
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_pricelist`;
CREATE TABLE `t_stock_pricelist`  (
  `pcl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pcl_number` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '调整单据',
  `pcl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `pcl_oldprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '调整前价格',
  `pcl_newprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '调整后价格',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`pcl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 199 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_pricelist_temp
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_pricelist_temp`;
CREATE TABLE `t_stock_pricelist_temp`  (
  `pcl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pcl_pd_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品编号',
  `pcl_oldprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '调整前价格',
  `pcl_newprice` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '调整后价格',
  `pcl_us_id` int NULL DEFAULT 0 COMMENT '用户id',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`pcl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 197 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_stock_useable
-- ----------------------------
DROP TABLE IF EXISTS `t_stock_useable`;
CREATE TABLE `t_stock_useable`  (
  `ua_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ua_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '模块编号',
  `ua_join` tinyint(1) NULL DEFAULT 0 COMMENT '参与计算0:否 1:是',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`ua_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_company
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_company`;
CREATE TABLE `t_sys_company`  (
  `co_id` int NOT NULL AUTO_INCREMENT,
  `co_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商家编号',
  `co_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '商家名称',
  `co_state` tinyint(1) NOT NULL COMMENT '状态0正常1停用',
  `co_sysdate` date NOT NULL COMMENT '注册时间',
  `co_man` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `co_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电话',
  `co_shops` smallint NOT NULL COMMENT '店铺数',
  `co_province` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `co_city` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `co_town` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `co_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '地址',
  `co_qq` varchar(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'qq号',
  `co_type` tinyint(1) NOT NULL COMMENT '用户类型0_体验用户  1_正式用户 ',
  `co_ver` tinyint(1) NOT NULL COMMENT '版本',
  `co_users` tinyint NOT NULL COMMENT '免费用户数',
  `co_wechat` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '微信号',
  `co_salesman` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `co_unit` int NULL DEFAULT NULL,
  PRIMARY KEY (`co_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_kpi
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_kpi`;
CREATE TABLE `t_sys_kpi`  (
  `ki_id` int NOT NULL AUTO_INCREMENT,
  `ki_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '指标编号',
  `ki_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`ki_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '商家选择的kpi参数' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_kpiscore
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_kpiscore`;
CREATE TABLE `t_sys_kpiscore`  (
  `ks_id` int NOT NULL AUTO_INCREMENT,
  `ks_ki_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT 'KPI编号',
  `ks_min` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '标准下限',
  `ks_max` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '标准上限',
  `ks_score` smallint NOT NULL DEFAULT 0 COMMENT '分数',
  `ks_shop_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `ks_rw_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '获得奖励编号',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`ks_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 94 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '商家自定义的分数标准' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu`  (
  `mn_id` smallint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mn_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '编号',
  `mn_name` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `mn_upcode` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '父类ID',
  `mn_url` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '菜单URL',
  `mn_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态',
  `mn_order` smallint NULL DEFAULT 0 COMMENT '排序',
  `mn_version` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '版本号1:总代理2:连锁店3:自营店',
  `mn_shop_type` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '角色类型1:总公司2:分公司3:自营店4:加盟店5:合伙店',
  `mn_style` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '样式名称',
  `mn_icon` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '图标位置样式',
  `mn_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '总部操作权限',
  `mn_area_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '区域模块功能操作权限条件',
  `mn_join_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '加盟操作权限条件',
  `mn_own_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '自营操作权限条件',
  `mn_team_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `mn_child` char(1) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT 'N' COMMENT '是否有子项',
  `mn_app` tinyint(1) NULL DEFAULT 0 COMMENT '是否应用：0否 1是',
  PRIMARY KEY (`mn_id`) USING BTREE,
  INDEX `IDX_MN_CODE`(`mn_code`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 616 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_print
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_print`;
CREATE TABLE `t_sys_print`  (
  `sp_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sp_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '模板名称',
  `sp_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '模块类型',
  `sp_remark` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `sp_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '店铺编号',
  `sp_sysdate` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `sp_us_id` int NOT NULL DEFAULT 0 COMMENT '用户ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_printdata
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_printdata`;
CREATE TABLE `t_sys_printdata`  (
  `spd_id` int NOT NULL AUTO_INCREMENT,
  `spd_sp_id` int NULL DEFAULT 0 COMMENT '主表关联编号',
  `spd_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '数据库字段名',
  `spd_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '字段名称',
  `spd_namecustom` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '自定义显示名称',
  `spd_order` smallint NULL DEFAULT 0 COMMENT '排序',
  `spd_show` tinyint(1) NULL DEFAULT 1 COMMENT '是否显示0 否 1是',
  `spd_width` int NULL DEFAULT 80 COMMENT '字段宽度',
  `spd_align` tinyint(1) NULL DEFAULT 0 COMMENT '对齐方式0:居左 1:居中 2:居右',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`spd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 813 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_printfield
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_printfield`;
CREATE TABLE `t_sys_printfield`  (
  `spf_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `spf_sp_id` int NULL DEFAULT 0 COMMENT '关联主表ID',
  `spf_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '数据库字段名',
  `spf_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `spf_namecustom` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '自定义显示名称',
  `spf_colspan` tinyint(1) NULL DEFAULT NULL COMMENT '默认占列数',
  `spf_show` tinyint(1) NULL DEFAULT NULL COMMENT '默认是否显示',
  `spf_line` tinyint(1) NULL DEFAULT NULL,
  `spf_row` tinyint(1) NULL DEFAULT NULL,
  `spf_position` tinyint(1) NULL DEFAULT 0 COMMENT '位置 0:表头 1:表尾',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`spf_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 763 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_printset
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_printset`;
CREATE TABLE `t_sys_printset`  (
  `sps_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sps_sp_id` int NOT NULL DEFAULT 0 COMMENT '模板ID',
  `sps_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '字段编号',
  `sps_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '中文名称',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sps_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1151 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_reward
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_reward`;
CREATE TABLE `t_sys_reward`  (
  `rw_id` int NOT NULL AUTO_INCREMENT,
  `rw_code` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '奖励编号',
  `rw_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '奖励名称',
  `rw_score` int NULL DEFAULT 0 COMMENT '价格',
  `rw_icon` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '图标',
  `rw_style` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '样式',
  `rw_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '摘要',
  `rw_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级店铺编号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`rw_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`  (
  `ro_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ro_code` char(4) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '角色编号',
  `ro_name` char(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '角色名称',
  `ro_shop_type` tinyint(1) NOT NULL COMMENT '店铺类型ID',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  `ro_shop_code` char(4) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL DEFAULT '' COMMENT '所属店铺的编号',
  `ro_date` date NULL DEFAULT NULL,
  `ro_default` char(1) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '0' COMMENT '是否是默认角色0否1是',
  PRIMARY KEY (`ro_id`) USING BTREE,
  INDEX `IDX_RO_CO`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 257 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_rolemenu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_rolemenu`;
CREATE TABLE `t_sys_rolemenu`  (
  `rm_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rm_ro_code` char(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '角色编号',
  `rm_state` tinyint(1) NULL DEFAULT 0 COMMENT '0 启用 1 停用',
  `rm_limit` char(12) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '操作权限设置',
  `rm_mn_code` char(15) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '菜单编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`rm_id`) USING BTREE,
  INDEX `IDX_RM_COM_RO_MN`(`companyid` ASC, `rm_ro_code` ASC, `rm_mn_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26895 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_set
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_set`;
CREATE TABLE `t_sys_set`  (
  `st_id` int NOT NULL AUTO_INCREMENT,
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家id',
  `st_buy_calc_costprice` tinyint(1) NULL DEFAULT 1 COMMENT '进货使用加权平均算法修改成本价 1：启用 0：不启用',
  `st_buy_os` tinyint(1) NULL DEFAULT 0 COMMENT '进货到自营店(综合与高级版):0否1是',
  `st_batch_os` tinyint(1) NULL DEFAULT 0 COMMENT '批发是否选择自营店仓库:0否1是',
  `st_batch_showrebates` tinyint(1) NULL DEFAULT 0 COMMENT '批发显示返点/扣点:1-显示；0-不显示',
  `st_ar_print` tinyint(1) NULL DEFAULT 0 COMMENT '是否审批后打印1:是 0：否',
  `st_blank` tinyint(1) NULL DEFAULT 3 COMMENT '盘点机分隔符：0-逗号，1-一个空格类推23，4-分号，5-tab',
  `st_subcode_isrule` tinyint(1) NULL DEFAULT 0 COMMENT '条码规则：0:系统配置 1：自定义规则',
  `st_subcode_rule` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '编码+颜色+尺码+杯型' COMMENT '条码生成规则',
  `st_allocate_unitprice` tinyint(1) NULL DEFAULT 0 COMMENT '仓库调拨单价格按: 0零售价(默认),1为成本价',
  `st_check_showstock` tinyint(1) NULL DEFAULT 1 COMMENT '库存盘点录入，0不显示库存1显示',
  `st_check_showdiffer` tinyint(1) NULL DEFAULT 1 COMMENT '盘点录入差异，0不显示1显示',
  `st_force_checkstock` tinyint(1) NULL DEFAULT 0 COMMENT '强制验证库存:1-是；0-否',
  `st_useable` tinyint(1) NULL DEFAULT 0 COMMENT '可用库存设置0不启用1启用',
  `st_use_upmoney` tinyint(1) NULL DEFAULT 0 COMMENT '使用上级店铺余额',
  PRIMARY KEY (`st_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_sqb
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_sqb`;
CREATE TABLE `t_sys_sqb`  (
  `sqb_id` int NOT NULL AUTO_INCREMENT,
  `sqb_device_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `sqb_device_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '设备名称',
  `sqb_terminal_sn` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '终端号',
  `sqb_terminal_key` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '终端密钥',
  `sqb_activate_time` datetime NULL DEFAULT NULL COMMENT '激活时间',
  `sqb_checkin_time` datetime NULL DEFAULT NULL COMMENT '上次签到时间',
  `sqb_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `sqb_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态：0-正常，1-停用',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sqb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_sqb_copy
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_sqb_copy`;
CREATE TABLE `t_sys_sqb_copy`  (
  `sqb_id` int NOT NULL AUTO_INCREMENT,
  `sqb_device_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `sqb_device_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '设备名称',
  `sqb_terminal_sn` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '终端号',
  `sqb_terminal_key` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '终端密钥',
  `sqb_activate_time` datetime NULL DEFAULT NULL COMMENT '激活时间',
  `sqb_checkin_time` datetime NULL DEFAULT NULL COMMENT '上次签到时间',
  `sqb_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '店铺编号',
  `sqb_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态：0-正常，1-停用',
  `companyid` int NOT NULL,
  PRIMARY KEY (`sqb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`  (
  `us_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `us_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '',
  `us_account` char(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `us_name` char(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '用户姓名',
  `us_pass` char(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `us_state` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '用户帐号状态0正常1停用',
  `us_date` datetime NULL DEFAULT NULL COMMENT '用户生成日期',
  `us_ro_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '角色编号',
  `us_shop_code` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '门店编号',
  `us_limit` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '价格权限：【C:成本价 S:零售价 D:配送价 V:会员价 W:批发价 E:进货价】',
  `us_last` datetime NULL DEFAULT NULL COMMENT '最终使用时间',
  `us_lock` tinyint(1) NULL DEFAULT 0 COMMENT '启用加密狗0未启用，1启用',
  `us_end` date NULL DEFAULT NULL COMMENT '到期时间',
  `us_default` tinyint(1) NULL DEFAULT 0 COMMENT '默认帐户0否1是',
  `us_pay` tinyint(1) NULL DEFAULT 0 COMMENT '付费用户0：否 1：是',
  `companyid` int NULL DEFAULT NULL COMMENT '商家ID',
  PRIMARY KEY (`us_id`) USING BTREE,
  INDEX `IDX_US_COM_ST_RO`(`companyid` ASC, `us_shop_code` ASC, `us_ro_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1208 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_weather
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_weather`;
CREATE TABLE `t_sys_weather`  (
  `we_id` int NOT NULL AUTO_INCREMENT,
  `we_max_tmp` smallint NULL DEFAULT NULL,
  `we_min_tmp` smallint NULL DEFAULT NULL,
  `we_we_id` tinyint NULL DEFAULT NULL,
  `we_date` date NULL DEFAULT NULL,
  `we_city` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `we_wind` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `we_tomorrow` tinyint NULL DEFAULT NULL COMMENT '明天天气,防止7点之前取的数据不正确使用',
  PRIMARY KEY (`we_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6076 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_workbench
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_workbench`;
CREATE TABLE `t_sys_workbench`  (
  `wb_id` int NOT NULL AUTO_INCREMENT,
  `wb_us_id` int NULL DEFAULT 0 COMMENT '用户编号',
  `wb_mn_id` int NULL DEFAULT 0 COMMENT '菜单编号',
  `wb_oper_usid` int NULL DEFAULT 0 COMMENT '操作人id',
  `wb_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `wb_weight` int NULL DEFAULT 0 COMMENT '权重，排序使用',
  `wb_type` tinyint(1) NULL DEFAULT NULL COMMENT '类型：0-日常工作，1-日常报表，2-待办事项',
  `companyid` int NOT NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`wb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 148 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_agegroupsetup
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_agegroupsetup`;
CREATE TABLE `t_vip_agegroupsetup`  (
  `ags_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ags_beg_age` int NULL DEFAULT NULL COMMENT '开始年龄',
  `ags_end_age` int NULL DEFAULT NULL COMMENT '结束年龄',
  `ags_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号',
  PRIMARY KEY (`ags_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_birthdaysms
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_birthdaysms`;
CREATE TABLE `t_vip_birthdaysms`  (
  `vb_id` int NOT NULL AUTO_INCREMENT,
  `vb_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `vb_state` tinyint(1) NULL DEFAULT 0 COMMENT '发送状态 0：手动发送 1：自动发送',
  `vb_agowarn_day` int NULL DEFAULT 0 COMMENT '提前提醒天数',
  `vb_sms_content` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '短信内容',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号',
  PRIMARY KEY (`vb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_brand_rate
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_brand_rate`;
CREATE TABLE `t_vip_brand_rate`  (
  `br_id` int NOT NULL AUTO_INCREMENT,
  `br_bd_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `br_mt_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `br_rate` decimal(5, 2) NOT NULL,
  `companyid` int NOT NULL,
  PRIMARY KEY (`br_id`) USING BTREE,
  INDEX `IDX_BR_CO_MT_BD`(`companyid` ASC, `br_mt_code` ASC, `br_bd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 576 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_grade
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_grade`;
CREATE TABLE `t_vip_grade`  (
  `gd_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gd_lower` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '周期消费下限',
  `gd_upper` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '周期消费上限',
  `gd_color` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '颜色代码',
  `gd_name` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '等级',
  `gd_remark` char(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `gd_code` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '等级编号',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`gd_id`) USING BTREE,
  UNIQUE INDEX `PK_Gd_Id`(`gd_id` ASC) USING BTREE,
  INDEX `Index_Companyid`(`companyid` ASC) USING BTREE,
  INDEX `Index_Gd_Code`(`gd_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_member
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_member`;
CREATE TABLE `t_vip_member`  (
  `vm_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vm_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '编号',
  `vm_mobile` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `vm_cardcode` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '会员卡号',
  `vm_mt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '会员类型ID',
  `vm_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '发卡店铺编号',
  `vm_name` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '会员姓名',
  `vm_sex` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '性别',
  `vm_password` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '密码',
  `vm_birthday_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '提醒方式阴历生日:0公历生日1为农历生日',
  `vm_birthday` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '阳历生日',
  `vm_lunar_birth` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '阴历生日',
  `vm_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0:正常 1:无效 2:挂失',
  `vm_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '办卡日期',
  `vm_enddate` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '有效日期',
  `vm_manager_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人编号',
  `vm_old_manager` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '原经办人,存中文',
  `vm_total_point` decimal(9, 0) NOT NULL DEFAULT 0 COMMENT '累计总积分',
  `vm_points` decimal(9, 0) NOT NULL DEFAULT 0 COMMENT '会员积分',
  `vm_used_points` decimal(11, 0) NOT NULL DEFAULT 0 COMMENT '已用积分',
  `vm_init_points` decimal(11, 0) NULL DEFAULT 0 COMMENT '初始积分',
  `vm_times` int NOT NULL DEFAULT 0 COMMENT '消费次数',
  `vm_total_money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '累计消费额',
  `vm_lastbuy_date` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '最后购买时间：XXXX－XX－XX方便统计',
  `vm_lastbuy_money` decimal(11, 2) NULL DEFAULT 0.00 COMMENT '最后消费金额',
  `vm_sysdate` varchar(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '系统时间',
  `vm_img_path` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '头像路径',
  `companyid` int NOT NULL,
  PRIMARY KEY (`vm_id`) USING BTREE,
  INDEX `IDX_VM_CO_VM`(`companyid` ASC, `vm_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63911 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_member_info
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_member_info`;
CREATE TABLE `t_vip_member_info`  (
  `vmi_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vmi_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '编号',
  `vmi_anima` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '生肖',
  `vmi_marriage_state` tinyint(1) NULL DEFAULT 0 COMMENT '婚姻状况:0未婚 1已婚 2曾婚',
  `vmi_nation` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '民族',
  `vmi_bloodtype` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '血型',
  `vmi_idcard_type` tinyint(1) NULL DEFAULT 0 COMMENT '证件类型：0身份证 1军官证 2驾照  3护照 4学生证 5其他',
  `vmi_idcard` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '身份证号',
  `vmi_job` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '职业',
  `vmi_qq` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '会员qq',
  `vmi_wechat` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '微信',
  `vmi_email` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '邮箱',
  `vmi_address` varchar(70) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '家庭住址',
  `vmi_work_unit` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '工作单位',
  `vmi_faith` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '宗教信仰',
  `vmi_family_festival` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '家庭节日',
  `vmi_culture` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '文化程度',
  `vmi_family` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '家庭信息',
  `vmi_oral_habit` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '口语习惯',
  `vmi_transport` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '交通工具',
  `vmi_like_topic` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '喜欢话题',
  `vmi_taboo_topic` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '禁忌话题',
  `vmi_smoke` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '抽烟与否',
  `vmi_like_food` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '饮食偏好',
  `vmi_income_level` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '收入水平',
  `vmi_character` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '性格特点',
  `vmi_hobby` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '兴趣',
  `vmi_height` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '身高',
  `vmi_weight` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '体重',
  `vmi_skin` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '肤色',
  `vmi_bust` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '胸围',
  `vmi_waist` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '腰围',
  `vmi_hips` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '臀围',
  `vmi_foottype` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '脚型',
  `vmi_size_top` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '上装尺码',
  `vmi_size_lower` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '下装尺码',
  `vmi_size_shoes` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '鞋子尺码',
  `vmi_size_bra` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '内衣尺寸',
  `vmi_trousers_length` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '裤长',
  `vmi_like_color` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '钟意颜色',
  `vmi_like_brand` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '常穿品牌',
  `vmi_wear_prefer` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '穿着偏好',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`vmi_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38034 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_member_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_member_tag`;
CREATE TABLE `t_vip_member_tag`  (
  `vmt_id` int NOT NULL AUTO_INCREMENT,
  `vmt_vm_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `vmt_vt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '标签编号',
  `vmt_vt_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '标签名字',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`vmt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_membertype
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_membertype`;
CREATE TABLE `t_vip_membertype`  (
  `mt_id` int NOT NULL AUTO_INCREMENT COMMENT '主键字段自增',
  `mt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '会员类别编号',
  `mt_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '会员类别名称',
  `mt_discount` decimal(9, 2) NOT NULL DEFAULT 1.00 COMMENT '折扣',
  `mt_isauto_up` tinyint(1) NOT NULL DEFAULT 0 COMMENT '自动升级0否1是',
  `mt_min_point` int NOT NULL DEFAULT 0 COMMENT '升级下限积分',
  `mt_money` int NOT NULL DEFAULT 1 COMMENT '消费多少元',
  `mt_point` int NOT NULL DEFAULT 1 COMMENT '积多少分',
  `mt_point_to_money` int NOT NULL DEFAULT 0 COMMENT '多少分抵一元',
  `mt_shop_upcode` char(6) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '所属上级店铺编号',
  `mt_mark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `companyid` int NOT NULL,
  PRIMARY KEY (`mt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_pointlist
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_pointlist`;
CREATE TABLE `t_vip_pointlist`  (
  `vpl_id` bigint NOT NULL AUTO_INCREMENT,
  `vpl_vm_code` char(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `vpl_point` double(9, 0) NULL DEFAULT NULL,
  `vpl_type` tinyint NULL DEFAULT NULL COMMENT '类型：对应字典表内容',
  `vpl_remark` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `vpl_date` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `vpl_shop_name` varchar(40) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `vpl_manager` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '操作人中文',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`vpl_id`) USING BTREE,
  INDEX `IDX_VPL_CO_VPL`(`companyid` ASC, `vpl_vm_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13458 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_reachshop
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_reachshop`;
CREATE TABLE `t_vip_reachshop`  (
  `rs_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rs_vm_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '会员编号',
  `rs_vm_name` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '会员姓名',
  `rs_vm_mobile` char(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `rs_content` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '注意内容',
  `rs_manager_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人Code',
  `rs_manager` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '人员',
  `rs_type` tinyint(1) NULL DEFAULT NULL COMMENT '意向来源 1:会员回访 2:生日回访 3:消费回访',
  `rs_arrive_date` date NULL DEFAULT NULL COMMENT '到店日期',
  `rs_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `rs_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '到店状态：0未到，1已到',
  `rs_sysdate` datetime NULL DEFAULT NULL COMMENT '时间',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`rs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_returnsetup
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_returnsetup`;
CREATE TABLE `t_vip_returnsetup`  (
  `rts_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rts_day` int NULL DEFAULT NULL COMMENT '回访天数',
  `rts_second` int NULL DEFAULT NULL COMMENT '回访次数',
  `rts_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '回访' COMMENT '回访名称',
  `rts_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号',
  PRIMARY KEY (`rts_id`) USING BTREE,
  INDEX `Index_rts_shop_code`(`rts_shop_code` ASC) USING BTREE,
  INDEX `Index_rts_companyid`(`companyid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 181 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_setup
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_setup`;
CREATE TABLE `t_vip_setup`  (
  `vs_id` int NOT NULL AUTO_INCREMENT COMMENT '会员设置编号',
  `vs_loss_day` int NULL DEFAULT NULL COMMENT '会员流失设置天数',
  `vs_consume_day` int NULL DEFAULT NULL COMMENT '会员消费周期',
  `vs_loyalvip_times` int NOT NULL DEFAULT 3 COMMENT '忠诚客户消费次数',
  `vs_richvip_money` double(11, 0) NOT NULL DEFAULT 1000 COMMENT '高价客户消费金额',
  `vs_activevip_day` int NOT NULL DEFAULT 30 COMMENT '活跃客户消费天数',
  `vs_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号',
  PRIMARY KEY (`vs_id`) USING BTREE,
  INDEX `Index_Companyid`(`companyid` ASC) USING BTREE,
  INDEX `Index_Vs_Shop_Code`(`vs_shop_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_specialfocus
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_specialfocus`;
CREATE TABLE `t_vip_specialfocus`  (
  `sf_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sf_vm_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '会员编号',
  `sf_vm_name` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '会员姓名',
  `sf_vm_mobile` char(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sf_content` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '关注内容',
  `sf_contact_date` date NULL DEFAULT NULL COMMENT '下次联系时间',
  `sf_manager_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '经办人Code',
  `sf_manager` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '关注人员',
  `sf_type` tinyint(1) NULL DEFAULT NULL COMMENT '来源 1:会员回访 2:生日回访 3:消费回访',
  `sf_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `sf_status` tinyint(1) NULL DEFAULT 0 COMMENT '到店状态：0未到，1已到',
  `sf_sysdate` datetime NULL DEFAULT NULL COMMENT '关注时间',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`sf_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_tag`;
CREATE TABLE `t_vip_tag`  (
  `vt_id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `vt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '编号',
  `vt_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '标签名字',
  `vt_manager_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人编号',
  `vt_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `vt_sysdate` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`vt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_trylist
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_trylist`;
CREATE TABLE `t_vip_trylist`  (
  `tr_id` bigint NOT NULL AUTO_INCREMENT,
  `tr_sysdate` datetime NULL DEFAULT NULL COMMENT '系统时间',
  `tr_pd_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `tr_sub_code` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商品子码',
  `tr_cr_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `tr_sz_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '尺码',
  `tr_br_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '杯型',
  `tr_sell_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `tr_vm_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员编号',
  `tr_em_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '导购编号',
  `tr_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家编号',
  PRIMARY KEY (`tr_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_type_rate
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_type_rate`;
CREATE TABLE `t_vip_type_rate`  (
  `tr_id` int NOT NULL AUTO_INCREMENT,
  `tr_tp_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `tr_mt_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `tr_rate` decimal(5, 2) NULL DEFAULT NULL,
  `companyid` int NULL DEFAULT NULL,
  PRIMARY KEY (`tr_id`) USING BTREE,
  INDEX `IDX_TR_CO_MT_BD`(`companyid` ASC, `tr_mt_code` ASC, `tr_tp_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_vip_visit
-- ----------------------------
DROP TABLE IF EXISTS `t_vip_visit`;
CREATE TABLE `t_vip_visit`  (
  `vi_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `vi_vm_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '会员编号',
  `vi_vm_name` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '会员姓名',
  `vi_consume_money` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '消费金额',
  `vi_consume_date` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '购物日期',
  `vi_visit_date` datetime NULL DEFAULT NULL COMMENT '回访日期',
  `vi_manager` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '回访人员',
  `vi_content` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '内容',
  `vi_satisfaction` tinyint(1) NULL DEFAULT NULL COMMENT '满意度 0非常满意，1基本满意，2一般 ，3不满意，4非常不满意',
  `vi_manager_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '经办人code',
  `vi_shop_code` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺code',
  `vi_us_id` int NULL DEFAULT NULL,
  `vi_sysdate` datetime NULL DEFAULT NULL,
  `vi_type` tinyint(1) NULL DEFAULT 1 COMMENT '回访类型 1:会员回访 2:生日回访 3:消费回访',
  `vi_is_arrive` tinyint(1) NULL DEFAULT 1 COMMENT '1:不确定到店 2:确认到店 3:确认不到店',
  `vi_date` date NULL DEFAULT NULL,
  `vi_remark` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `vi_way` tinyint(1) NULL DEFAULT 1 COMMENT '回访形式 1:电话回访 2:短信回访 3:微信回访 4:推送优惠券',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`vi_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_authorizer
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_authorizer`;
CREATE TABLE `t_wx_authorizer`  (
  `wa_id` int NOT NULL AUTO_INCREMENT,
  `wa_appid` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'appid',
  `wa_access_token` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '令牌',
  `wa_refresh_token` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '刷新令牌',
  `wa_expire_time` datetime NULL DEFAULT NULL COMMENT '令牌过期时间',
  PRIMARY KEY (`wa_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_component
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_component`;
CREATE TABLE `t_wx_component`  (
  `wc_id` int NOT NULL AUTO_INCREMENT,
  `wc_appid` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'appid',
  `wc_verify_ticket` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `wc_createtime` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `wc_access_token` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '令牌',
  `wc_expire_time` datetime NULL DEFAULT NULL COMMENT '令牌过期时间',
  `wc_pre_auth_code` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '预授权码',
  `wc_expire_time_pre_auth` datetime NULL DEFAULT NULL COMMENT '预授权码过期时间',
  PRIMARY KEY (`wc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_product
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_product`;
CREATE TABLE `t_wx_product`  (
  `wp_id` int NOT NULL AUTO_INCREMENT,
  `wp_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `wp_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '店铺编号',
  `wp_pd_code` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '编号',
  `wp_pd_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品名称',
  `wp_sell_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '零售价',
  `wp_rate_price` decimal(9, 2) NULL DEFAULT 0.00 COMMENT '折扣价',
  `wp_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '商品类型：0-正常商品，1-新品，2-折扣商品',
  `wp_state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0: 上架 1： 下架',
  `wp_pt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '类别编号',
  `wp_istry` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否试穿0：否 1：是',
  `wp_try_way` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '试穿方式:0-到店试穿，1-送货上门试穿，多个用逗号分隔',
  `wp_isbuy` tinyint(1) NULL DEFAULT 1 COMMENT '是否购买0：否 1：是',
  `wp_buy_way` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '购买取货方式:0-快递，1-自取，多个用逗号分隔',
  `wp_postfree` tinyint(1) NULL DEFAULT 0 COMMENT '是否包邮0：否 1：是',
  `wp_subtitle` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '副标题',
  `wp_weight` int NULL DEFAULT 0 COMMENT '权重,从高到低排序',
  `wp_sell_amount` int NULL DEFAULT 0 COMMENT '已售数量',
  `wp_virtual_amount` int NULL DEFAULT 0 COMMENT '虚拟销售',
  `wp_buy_limit` int NULL DEFAULT NULL COMMENT '限购数量',
  `wp_browse_count` int NOT NULL DEFAULT 0 COMMENT '商品浏览次数',
  `wp_likecount` int NULL DEFAULT 0 COMMENT '点赞数量',
  `wp_sysdate` datetime NOT NULL COMMENT '创建时间',
  `companyid` int NULL DEFAULT NULL COMMENT '商家id',
  PRIMARY KEY (`wp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_product_list
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_product_list`;
CREATE TABLE `t_wx_product_list`  (
  `pl_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pl_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '关联字段',
  `pl_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '商品属性编号',
  `pl_type` tinyint(1) NULL DEFAULT 0 COMMENT '0-商品属性，1-售后承诺',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`pl_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_product_shop
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_product_shop`;
CREATE TABLE `t_wx_product_shop`  (
  `ps_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ps_number` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '关联字段',
  `ps_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '店铺编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`ps_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_producttype
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_producttype`;
CREATE TABLE `t_wx_producttype`  (
  `pt_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pt_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分类编号',
  `pt_upcode` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '父级类别编号',
  `pt_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `pt_weight` int NULL DEFAULT 0 COMMENT '权重',
  `pt_img_path` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `pt_state` tinyint(1) NULL DEFAULT 0 COMMENT '0-正常，1-停用',
  `pt_shop_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NULL DEFAULT 0 COMMENT '商家ID',
  PRIMARY KEY (`pt_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_set
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_set`;
CREATE TABLE `t_wx_set`  (
  `wx_id` int NOT NULL AUTO_INCREMENT,
  `wx_appid` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '微信头文字',
  `wx_sysdate` datetime NULL DEFAULT NULL,
  `wx_shop_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`wx_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_user
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_user`;
CREATE TABLE `t_wx_user`  (
  `wu_id` int NOT NULL AUTO_INCREMENT,
  `wu_code` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '编号，用于关联',
  `wu_mobile` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `wu_nickname` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `wu_password` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '密码',
  `wu_headimage` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '头像',
  `wu_state` tinyint(1) NULL DEFAULT NULL COMMENT '状态：0-正常，1-停用',
  `wu_sysdate` datetime NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`wu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_vip
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_vip`;
CREATE TABLE `t_wx_vip`  (
  `wx_id` int NOT NULL AUTO_INCREMENT,
  `wx_openid` char(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '微信标识码',
  `wx_vm_code` char(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '实体店用户卡号',
  `wx_mobile` char(11) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '会员手机号',
  `companyid` int NULL DEFAULT NULL COMMENT '商家id',
  `wx_sysdate` datetime NULL DEFAULT NULL COMMENT '注册时间',
  `wx_shop_code` char(20) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL,
  PRIMARY KEY (`wx_id`) USING BTREE,
  INDEX `IDXWX_EI_MI_Mobile`(`companyid` ASC, `wx_mobile` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_wechatinfo
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_wechatinfo`;
CREATE TABLE `t_wx_wechatinfo`  (
  `wx_id` int NOT NULL AUTO_INCREMENT,
  `wx_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '公众号名称 ',
  `wx_originalid` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '原始id',
  `wx_accountchat` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '微信号',
  `wx_accounttype` tinyint(1) NULL DEFAULT NULL COMMENT '账号类型 0订阅号 1服务号 2企业号',
  `wx_appid` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'appId',
  `wx_appsecret` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'appSecret',
  `wx_sysdate` datetime NULL DEFAULT NULL,
  `wx_mt_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '默认会员类型编号',
  `wx_shop_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `wx_enddate` date NULL DEFAULT NULL COMMENT '到期时间',
  `companyid` int NOT NULL COMMENT '商家ID',
  `wx_qrc_ticket` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `wx_mch_id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商户编号',
  `wx_mch_key` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商户key',
  `wx_mch_cer_path` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商户证书地址',
  `wx_mch_pwd` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '商户证书密码',
  PRIMARY KEY (`wx_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_wx_wechatset
-- ----------------------------
DROP TABLE IF EXISTS `t_wx_wechatset`;
CREATE TABLE `t_wx_wechatset`  (
  `ws_id` int NOT NULL AUTO_INCREMENT,
  `ws_title` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '微信头文字',
  `ws_bgimg_path` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '背景图片路径',
  `ws_useable_balance` decimal(11, 2) NULL DEFAULT 0.00,
  `ws_sysdate` datetime NULL DEFAULT NULL,
  `ws_shop_code` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '店铺编号',
  `companyid` int NOT NULL COMMENT '商家ID',
  PRIMARY KEY (`ws_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_zhjr_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_zhjr_menu`;
CREATE TABLE `t_zhjr_menu`  (
  `mn_id` smallint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mn_code` char(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '编号',
  `mn_name` char(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `mn_upcode` varchar(15) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '父类ID',
  `mn_url` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '菜单URL',
  `mn_state` tinyint(1) NULL DEFAULT 0 COMMENT '状态',
  `mn_order` smallint NULL DEFAULT 0 COMMENT '排序',
  `mn_version` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '版本号1:总代理2:连锁店3:自营店',
  `mn_shop_type` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '角色类型1:总公司2:分公司3:自营店4:加盟店5:合伙店',
  `mn_style` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '样式名称',
  `mn_icon` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '图标位置样式',
  `mn_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '总部操作权限',
  `mn_area_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '区域模块功能操作权限条件',
  `mn_own_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '自营操作权限条件',
  `mn_team_limit` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `mn_child` char(1) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT 'N' COMMENT '是否有子项',
  PRIMARY KEY (`mn_id`) USING BTREE,
  INDEX `IDX_MN_CODE`(`mn_code`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 616 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_zhjr_role
-- ----------------------------
DROP TABLE IF EXISTS `t_zhjr_role`;
CREATE TABLE `t_zhjr_role`  (
  `ro_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ro_code` char(4) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '角色编号',
  `ro_name` char(30) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT NULL COMMENT '角色名称',
  `ro_shop_type` tinyint(1) NOT NULL COMMENT '店铺类型ID',
  `ro_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`ro_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_zhjr_rolemenu
-- ----------------------------
DROP TABLE IF EXISTS `t_zhjr_rolemenu`;
CREATE TABLE `t_zhjr_rolemenu`  (
  `rm_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rm_ro_code` char(10) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '角色编号',
  `rm_state` tinyint(1) NULL DEFAULT 0 COMMENT '0 启用 1 停用',
  `rm_limit` char(12) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '操作权限设置',
  `rm_mn_code` char(15) CHARACTER SET gbk COLLATE gbk_chinese_ci NULL DEFAULT '' COMMENT '菜单编号',
  PRIMARY KEY (`rm_id`) USING BTREE,
  INDEX `IDX_RM_COM_RO_MN`(`rm_ro_code` ASC, `rm_mn_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9186 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_zhjr_unit
-- ----------------------------
DROP TABLE IF EXISTS `t_zhjr_unit`;
CREATE TABLE `t_zhjr_unit`  (
  `un_id` int NOT NULL AUTO_INCREMENT,
  `un_name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `un_state` tinyint(1) NULL DEFAULT NULL,
  `un_man` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `un_tel` varchar(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `un_addr` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`un_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_zhjr_user
-- ----------------------------
DROP TABLE IF EXISTS `t_zhjr_user`;
CREATE TABLE `t_zhjr_user`  (
  `us_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `us_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '',
  `us_account` char(18) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `us_name` char(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '用户姓名',
  `us_pass` char(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `us_state` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '用户帐号状态0正常1停用',
  `us_date` datetime NULL DEFAULT NULL COMMENT '用户生成日期',
  `us_ro_code` char(4) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '角色编号',
  `us_limit` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '价格权限：【C:成本价 S:零售价 D:配送价 V:会员价 W:批发价 E:进货价】',
  `us_last` datetime NULL DEFAULT NULL COMMENT '最终使用时间',
  `us_end` date NULL DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`us_id`) USING BTREE,
  INDEX `IDX_US_COM_ST_RO`(`us_ro_code` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1001 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Function structure for f_addnumber
-- ----------------------------
DROP FUNCTION IF EXISTS `f_addnumber`;
delimiter ;;
CREATE FUNCTION `f_addnumber`(maxNo VARCHAR(20))
 RETURNS varchar(4) CHARSET utf8mb4
BEGIN  
  DECLARE serial_num INT DEFAULT 1;
	IF('' <> maxNo) THEN
	SET serial_num = RIGHT(maxNo,4)+1;
	END IF;
	RETURN CONCAT(LPAD(serial_num, 4, '0'));  
END
;;
delimiter ;

-- ----------------------------
-- Function structure for SquareNumber
-- ----------------------------
DROP FUNCTION IF EXISTS `SquareNumber`;
delimiter ;;
CREATE FUNCTION `SquareNumber`(n INT)
 RETURNS int
BEGIN  
  RETURN n * n;  
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
