////app.js
//App({
//  onLaunch: function () {
//    // 展示本地存储能力
//    var logs = wx.getStorageSync('logs') || []
//    logs.unshift(Date.now())
//    wx.setStorageSync('logs', logs)
//
//    // 登录
//    wx.login({
//      success: res => {
//        // 发送 res.code 到后台换取 openId, sessionKey, unionId
//      }
//    })
//    // 获取用户信息
//    wx.getSetting({
//      success: res => {
//        if (res.authSetting['scope.userInfo']) {
//          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
//          wx.getUserInfo({
//            success: res => {
//              // 可以将 res 发送给后台解码出 unionId
//              this.globalData.userInfo = res.userInfo
//
//              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
//              // 所以此处加入 callback 以防止这种情况
//              if (this.userInfoReadyCallback) {
//                this.userInfoReadyCallback(res)
//              }
//            }
//          })
//        }
//      }
//    })
//  },
//  globalData: {
//    userInfo: null,
//    currentCompany:null
//  }
//})


var server = require('./common/server.js');
var common = require('./common/common.js');

App({
  onLaunch: function () {
    console.log('App Launch')
    var self = this;
    var openid = self.globalData.openid;//wx.getStorageSync('openid');
    //console.log('rd_session', rd_session)
    if (!openid) {
      self.login();
    } else {
      wx.checkSession({
        success: function () {
          // 登录态未过期
          console.log('登录态未过期')
          self.globalData.openid = openid;
          self.getUserInfo();
        },
        fail: function () {
          //登录态过期
          console.log('登录态过期')
          self.login();
        }
      })
    }
  },
  globalData: {
    imgdir: 'http://ftp.zhjr100.com/',
    hasLogin: false,
    userInfo: null,
    currentCompany: null,
    session_key:'',
    openid:''
  },
  login: function (opt) {
    var self = this;
    wx.login({
      success: function (res) {

        var that = {
          data:{
            controller: '/api/wx/user/auth',
            params: {
              code:res.code
            }
          }
        };

        common.doMethod({
          that: that,
          succFun: function (data) {
            if (data) {
              var ad = data.data;
              self.globalData.sessionkey = ad.session_key;
              self.globalData.openid = ad.openid;
            }
          }
        })        
      }
    });
  },

  setUserInfo: function (opt) {
    var self = this;
    var that = opt && opt.that;
    var compFun = opt && opt.compFun;
    var succFun = opt && opt.succFun;
    var res = opt && opt.res;

    //console.warn('getUserInfo', res);
    // self.globalData.userInfo = res.userInfo;
    // wx.setStorageSync('userInfo', res.userInfo);
    if (compFun) {
      compFun();
    }
    if (res.userInfo) {
      var sessionKey = self.globalData.sessionkey;
      var nickName = res.userInfo.nickName;
      var avatarUrl = res.userInfo.avatarUrl;
      var encryptedData = encodeURIComponent(res.encryptedData);
      var iv = res.iv;

       var that = {
        data:{
          controller: '/api/wx/user/update',
          params: {
            wu_code: self.globalData.user.wu_code,
            nickname: nickName,
            headimage: avatarUrl,
            encrypteddata: encryptedData,
            sessionkey: sessionKey,
            iv: iv
          }
        }
      };

      common.doMethod({
        that: that,
        succFun: function (data) {
          if (data) {               
            self.globalData.user.wu_nickname = data.user.wu_nickname;
            self.globalData.user.wu_headimage = data.user.wu_headimage;
            self.globalData.user.wu_openid = data.user.wu_openid;
            if (succFun){
              succFun();
            }            
          }
        }
      })    
    }
  },
  updateManager: function () {
    // 获取小程序更新机制兼容
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？',
              success: function (res) {
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
            })
          })
        }
      })
    } else {
      // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  }
  
  
})