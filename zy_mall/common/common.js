var server = require("server.js");
var h2w = require("htmlToWxml.js");
var globalData = require("globaldata.js");
// var WxParse = require('/wxParse/wxParse.js');
var gb = globalData.getGlobalData();
module.exports = {
  gb: gb,
  // 初始化模版数据
  // initTemplateInfo: function(that) {
  //   server.getJSON(gb.CurrentURL + '/company/queryUnit', {
  //     hmid: gb.ID
  //   }, function (res) {
  //     console.log("obj:"+res);
  //     var pdata = res.data.data;
  //     for (var k in pdata){
  //       console.log("info:" + pdata[k].un_name)
  //     }   
  //   });
  // },
  /******接口******** */

  //后台调用接口
  doMethod: function (opt) {
    var that = opt.that;
    var succFun = opt && opt.succFun;
    server.postJSON(gb.CurrentURL + that.data.controller, that.data.params,
      function (json) {
        var data = json.data;
        if (data) {
          if (data.stat == 200) {
            if (succFun) {
              succFun(data);
            }
          }
          else{
            wx.showModal({
              title: '提示',
              content: JSON.stringify(data),
              success: function (e) {

              }
            })
          }
        }
      });
  },
  //页面分页查询
  pagedDetails: function (opt) {
    var that = opt.that;
    var comFun= opt && opt.comFun;
    var succFun = opt && opt.succFun;
    var pagination = that.data.pagination;
    server.postJSON(gb.CurrentURL + that.data.controller,that.data.params, 
      function (json) {
        if (comFun){
          comFun();
        }
        
        var data = json.data;
        if (data.stat == 200) {
          var details = data.data;
          var totalCount =0;

          if (details.data){
            totalCount = details.totalCount || 0;
            details = details.data;
          }
         
          if (details.length <= 0) {
            opt.that.setData({
              noData: true
            });
          }
          var ls = [];
          if (pagination) {
            ls = opt.that.data.details;
            ls = ls ? ls : [];
            ls = ls.concat(details);
          } else {
            ls = details;
          }
          opt.that.setData({
            totalCount: totalCount
          });
          details = ls;
          if (succFun) {
            succFun(details);
          }
        }
        else {
          wx.showModal({
            title: '提示',
            content: JSON.stringify(data),
          })
        }
      })
  },

  //微信支付
  wxPay: function (that) {
    var that = opt.that;
    var comFun = opt && opt.comFun;
    var succFun = opt && opt.succFun;
    var user = app.globalData.user;
    
    server.postJSON(
      gb.CurrentURL + that.data.controller,
      {
        ordernumber: that.data.ordernumber,  //订单号
        totalprice: that.data.paymoney,   //订单金额
        openid: user.wu_openid,//用户openid
        // hmapid: common.gb.ID,//小程序id暂不用到
        prdouctdesc: that.data.productdesc,//支付商品信息
        payhmapflag: that.data.payhmapflag//支付页面区分
      },
      function (json) {
        var data = json.data;
        if (data.stat == 200) {
          var data = JSON.parse(data.Data);
          wx.requestPayment({
            'timeStamp': data.timeStamp,
            'nonceStr': data.nonceStr,
            'package': data.package,
            'signType': 'MD5',
            'paySign': data.paySign,
            'success': function (res) {
              console.warn("success");
              wx.showToast({
                title: '支付成功',
                icon: 'loading',
                duration: 3000,
                success: function () {
                  if (succFun) {
                    succFun();
                  }
                  if (comFun){
                    comFun();
                  }
                }
              })
            },
            'fail': function (res) {
              wx.showModal({
                title: '提示',
                content: JSON.stringify(res),
              })

            }
          })
        } else {
          wx.showModal({
            title: '提示',
            content: JSON.stringify(data.data),
          })
        }
    })

  },
  
};