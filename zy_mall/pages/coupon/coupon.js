var common = require("../../common/common.js");
var server = require("../../common/server.js");
var pageIndex = 1;
var pageSize = 10;
// 获取全局应用程序实例对象
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    hidden: true,
    pagination: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    var that = this;
    var pageamount = e.pageamount||0;
    var voucherId = app.globalData['voucherId'] || '';
    that.setData({
      voucherId: voucherId,
      pageamount: pageamount
    })

    pageIndex = 1;
    that.getCouponList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },  
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    var that = this;
    // that.nousevoucher();
  },
  //以下为自定义点击事件
  onPullDownRefresh() {

    //下拉  
    //console.log("下拉");
    wx.showNavigationBarLoading(); //在标题栏中显示加载
    pageIndex = 1;
    var that = this;
    that.setData({
      pagination: false,
      couponlist:[]
    });

    that.getCouponList();

  },
  //以下为自定义点击事件
  onReachBottom: function () {

    var that = this;
    //上拉  
    //console.log("上拉")
    var that = this;
    that.setData({
      pagination: true
    });
    pageIndex++;
    that.getCouponList();
  },

  // /**
  //  * 用户点击右上角分享
  //  */
  // onShareAppMessage: function () {

  // },
  nousevoucher:function (e) {
   
    var that = this;
    app.globalData['voucherId'] = '';
    app.globalData['voucherAmount'] = '';
    app.globalData['voucherLimitAmount'] = '';
    
    wx.navigateBack({
      delta: 1,
    })

  },
  usevoucher: function (e) {
    var that = this;
    var ecu_id = e.currentTarget.dataset.ecuid;
    var amount = e.currentTarget.dataset.amount;
    var limitamount = e.currentTarget.dataset.limitamount;

    app.globalData['voucherId'] = ecu_id;
    app.globalData['voucherAmount'] = amount;
    app.globalData['voucherLimitAmount'] = limitamount;
    if (that.data.pageurl == 'index') {
      wx.switchTab({
        url: '/pages/index/index',
      })
    } else {
      wx.navigateBack({
        delta: 1,
      })
    }
  },

  getCouponList: function (e) {
    var that = this;
    that.setData({
      controller: '/api/wx/my/getMyCouponList',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        wu_code: app.globalData.user.wu_code,
        shop_code: app.globalData.currentCompany.sp_code,
        wu_mobile: app.globalData.user.wu_mobile,
        pageamount: that.data.pageamount ||0, //如果是0除了自身限制都可用，如果>0 要判断status的状态 
        pageIndex: pageIndex,
        pageSize: pageSize
      }
    })

    common.pagedDetails({
      that: that,
      succFun: function (data) {
        // var data = [{
        //   usetype: 0,//使用条件：无限额
        //   amount: 10,//优惠券面值
        //   limitamount: 0,//满多少
        //   endtime: '2018-05-30',//有效期
        //   maxget: 0,//领取次数状态 0：领取按钮激活
        //   status: 0 //0：正常 1：不可用 2：已过期
        // },
        // {
        //   usetype: 1,//
        //   amount: 10,
        //   limitamount: 100,
        //   endtime: '2018-05-30',
        //   maxget: 1,//领取次数状态 1：领取图标激活
        //   status:1 //0：正常 1：不可用 2：已过期
        // },
        // {
        //   usetype: 1,//
        //   amount: 10,
        //   limitamount: 100,
        //   endtime: '2018-05-30',
        //   maxget: 1,//领取次数状态 1：领取图标激活
        //   status: 2 //0：正常 1：不可用 2：已过期
        // }
        // ];
        that.setData({
          couponlist: data
        })
      }
    })
  }
   
})

