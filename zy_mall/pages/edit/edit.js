var common = require("../../common/common.js");
var server = require("../../common/server.js");
var location = require("../../common/location.js");
var app = getApp();
var pageNo = 1;
var pageSize = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (opt) {
    var that = this;

    var memberId = wx.getStorageSync('rd_session') || '';
    var userInfo = app.globalData.userInfo;
    var lawerId = opt.lawerId || '';
    var label = opt.label || '';
    var field = opt.field || '';
    var value = opt.value || '';
    var place = opt.place ||'';
    var province = opt.province || '';
    var city = opt.city || '';
    var district = opt.district || '';
    var address = opt.address || '';
    that.setData({
      memberId: memberId,
      userInfo: userInfo,
      lawerId: lawerId,
      label: label,
      field: field,
      value: value,
      province: province,
      city: city,
      district: district,
      address: address,
      
      old_value: value,
    });
    wx.setNavigationBarTitle({
      title: label || '',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;

    var memberId = wx.getStorageSync('rd_session') || '';
    var store = wx.getStorageSync('store') || { id: '' };
    var userInfo = wx.getStorageSync('userInfo') || {};
    that.setData({
      userInfo: userInfo,
      memberId: memberId,
      storeId: store.id
    });

    that.initPageData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;

    that.initPageData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //以下为自定义点击事件
  initPageData: function () {
    var that = this;

  },
  onCustom: function (e) {
    var that = this;

    console.warn('e:' + JSON.stringify(e));
    var type = e.currentTarget.dataset.type;
    switch (type) {
      case 'editpassword':
        var that = this;

        that.setData({
          controller: '/api/wx/cart/' + that.data.action,
          params: {
            id: that.data.id,
            amount: that.data.amount
          }
        })

        common.doMethod({
          that: that,
          succFun: function (data) {
            wx.showToast({
              title: '数量更新成功',
              icon: 'none',
              success: function () {
                if (data) {
                  var list = that.data.goodsList.list;
                  for (var i = 0; i < list.length; i++) {
                    var curItem = list[i];
                    if (that.data.id == curItem.id) {
                      curItem.amount = that.data.amount;
                    }
                  }
                  //that.data.goodsList.list = data.data;
                  that.setGoodsList(that.getSaveHide(), that.totalPrice(), that.allSelect(), that.noSelect(), list);
                  that.setData({
                    nogoods: 'false',
                    // cartgoods: cartgoods
                  });
                }

              }
            });
          }
        })
        break;
      default:
        break;
    }
  },
  loadInfo: function () {
    wx.showToast({
      title: '定位中',
      icon: 'loading'
    })

    var page = this
    location.getLocationFun(
      'wgs84',
      function (res) {
        wx.hideToast();
        console.log(res);
        var latitude = res.latitude;
        var longitude = res.longitude;
        var selectcity = page.data.selectcity;
        page.loadCity(longitude, latitude, selectcity);
      },
      null,
      null
    )


  },
  loadCity: function (longitude, latitude, selectcity) {
    var page = this

    var memberid = app.rd_session || wx.getStorageSync('rd_session');
    var hmapid = common.gb.ID;

    //console.warn('selectcity:' + selectcity);
    server.getJSON(common.gb.CurrentURL + '/Interface/HmapInfo/GetDataInfoWithPageFlagForLS',
      {
        action: 'loadlocation',
        memberid: memberid,
        hmapid: hmapid,
        location: latitude + ',' + longitude,
        selectcity: selectcity,
        orgid: common.gb.OrgID,
      }, function (rsp) {
        //console.warn('rsp:' + JSON.stringify(rsp));
        var json = rsp.data;
        if (json.status = "success") {

          page.setData({
            province: json.place.province,
            city: json.place.city,
            district: json.place.district,
            address: json.address
          })
        }
      });
  },
})