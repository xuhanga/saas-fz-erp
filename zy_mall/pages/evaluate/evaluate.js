Page({
  data: {
    windowHeight: 'auto',
    commentList: [
      {
        url: 'http://share.30days-tech.com/daka/pictures/commodity_icon.png',
        id: 1
      },
      {
        url: 'http://share.30days-tech.com/daka/pictures/commodity_icon.png',
        id: 2
      },
      {
        url: 'http://share.30days-tech.com/daka/pictures/commodity_icon.png',
        id: 3
      }
    ]
  },
  onShow: function () {
    // 页面显示
    var vm = this
    // 初始化评论选项为好评
    for (var i = 0, len = commentList.length; i < len; i++) {
      commentList[i].commentType = 'GOOD';
    }
  },
  selectCommentType: function (e) {
    console.log('选中的是第几条评论的哪一种类型', e.currentTarget.dataset);
    var commentList = this.data.commentList;
    var index = parseInt(e.currentTarget.dataset.index);
    commentList[index].commentType = e.currentTarget.dataset.type;
    this.setData({
      'commentList': commentList
    });
  },
  saveContent: function (e) {
    console.log('保存评论到列表', e.detail.value, e.currentTarget.dataset.index);
    var vm = this;
    var commentList = vm.data.commentList;
    var index = e.currentTarget.dataset.index;
    commentList[index].content = e.detail.value;
    vm.setData({
      commentList: commentList
    });
  }
})