var common = require("../../common/common.js");
var server = require("../../common/server.js");
//获取应用实例
const app = getApp()
// common.initTemplateInfo();
Page({
  data: {
    my_shop: '我的商家',
    logo: '../../resources/images/logo.png',
    // userInfo: {},
    // hasUserInfo: false,
    // canIUse: wx.canIUse('button.open-type.getUserInfo'),
    myCompany:[]
  },
  onLoad: function () {
    var that = this;
    var user = app.globalData.user;
    if (!user) {
      wx.redirectTo({
        url: '/pages/login/login'
      });
      return false;
    }else{
      that.loadMyCompany(user);
    }
    // var loginMobile = wx.getStorageSync('loginMobile');
    // var loginPwd = wx.getStorageSync('loginPwd');
    // if (loginMobile == '' || loginPwd == ''){
    //   wx.redirectTo({
    //     url: '/pages/login/login'
    //   });
    //   return;
    // }
    // server.postJSON(common.gb.CurrentURL + '/login/' + loginMobile + '/' + loginPwd,
    //   {}, function (result) {
    //     var res = result.data;
    //     if (res.stat == 200) {
    //       var data = res.data;

    //       if (data == null || data == '') {
    //         wx.redirectTo({
    //           url: '/pages/login/login'
    //         });
    //         return;
    //       }
    //       wx.setStorage({ key: "loginUser", data: JSON.stringify(data) });
    //       that.loadMyCompany(data);
    //     }
    //   });
  },
  loadMyCompany: function (user){
    var that = this;
    var loginMobile = user.wu_mobile;//wx.getStorageSync('loginMobile');
    server.postJSON(common.gb.CurrentURL + '/api/base/shop/listByMobile/' + loginMobile,
      {}, function (result) {
        var res = result.data;
        if (res.stat == 200) {
          var data = res.data;
          that.setData({
            myCompany: data
          })
        }
      });


    
  },
  // getUserInfo: function (e) {
  //   console.log(e)
  //   app.globalData.userInfo = e.detail.userInfo
  //   this.setData({
  //     userInfo: e.detail.userInfo,
  //     hasUserInfo: true
  //   })
  // },
  toShop: function (e) {
    var idx = e.currentTarget.dataset.idx;
    var currentCompany = this.data.myCompany[idx];
    app.globalData.currentCompany = currentCompany;
    wx.switchTab({
      url: '../shop/shop'
    }) 

  }
})
