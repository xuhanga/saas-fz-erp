var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
var app = getApp();

var pageIndex = 1;
var pageSize = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noData: false,
    pagination: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    
    var user = app.globalData.user;
    var company = app.globalData.currentCompany;//店铺信息

    that.setData({
      user: user,
      company: company,
      pagination: false
    })

    pageIndex = 1;
    that.initPageData();
  

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  //以下为自定义点击事件
  onPullDownRefresh() {

      //下拉  
      console.log("下拉");
      wx.showNavigationBarLoading(); //在标题栏中显示加载
      pageIndex = 1;
      var that = this;
      that.setData({
        pagination: false,
      });

      that.initPageData();
      wx.hideNavigationBarLoading();

  },
  //以下为自定义点击事件
  onReachBottom: function () {

      var that = this;
      //上拉  
      console.log("上拉")
      var that = this;
      that.setData({
          pagination: true
      });
      pageIndex++;
      that.initPageData();
  },

  /**
   * 用户点击右上角分享
  onShareAppMessage: function () {

  }
   */

  //以下为自定义点击事件
  initPageData: function () {
    var that = this;

    // wx.showLoading({
    //   title: '加载中',
    //   mask: true
    // })

    that.setData({
      controller: '/api/wx/user/getAccountDetails',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        wu_code: app.globalData.user.wu_code,
        pageSize: pageSize,
        pageIndex: pageIndex
      }
    })

    common.pagedDetails({
      that: that,
      comFun: function () {
        wx.hideToast();
        wx.hideNavigationBarLoading();
      },
      succFun: function (data) {
        wx.hideLoading();
        that.setData({
          details: data
        })
      }
    })
  },
})
