var common = require("../../common/common.js");
var server = require("../../common/server.js");

var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    identity:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    var user = app.globalData.user;
    var company = app.globalData.currentCompany;//店铺信息

    that.setData({
      user: user,
      company: company
    })

    var memberid = app.rd_session || wx.getStorageSync('rd_session'); 

    //获取余额，储值卡，优惠券，代金券，积分
    server.postJSON(common.gb.CurrentURL + '/api/wx/my/getMyValue/' + that.data.user.wu_mobile,
      {
        "companyid": company.companyid,
        "shop_type": company.sp_shop_type,
        "shop_code": company.sp_code,
        "shop_upcode": company.sp_upcode
      }, function (result) {
        var res = result.data;
        if (res.stat == 200) {
          that.setData({
            balance: res.data.balance,
          });
        }
      });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var memberid = app.rd_session || wx.getStorageSync('rd_session');   
    // common.getMember({
    //   app: app,
    //   that: that,
    //   memberid: memberid
    // });
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
  onShareAppMessage: function () {

  }
   */
})