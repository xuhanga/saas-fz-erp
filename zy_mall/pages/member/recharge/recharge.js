var common = require("../../../common/common.js");
var server = require("../../../common/server.js");

var app = getApp();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    rechargeoptions:[
      {
        paymoney:100,
        facemoney:100,
        issendmember:false
      },
      {
        paymoney: 200,
        facemoney: 200,
        issendmember: false
      },
      {
        paymoney: 300,
        facemoney: 300,
        issendmember: false
      },
      {
        paymoney: 500,
        facemoney: 500,
        issendmember: true
      },
      {
        paymoney: 800,
        facemoney: 800,
        issendmember: true
      },
      {
        paymoney: 1000,
        facemoney: 1000,
        issendmember: true
      },
      {
        paymoney: 1500,
        facemoney: 1500,
        issendmember: true
      },
      {
        paymoney: 2000,
        facemoney: 2000,
        issendmember: true
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    var memberid = app.rd_session || wx.getStorageSync('rd_session');
    var store = wx.getStorageSync('store') || { id: '' };

    that.setData({
      memberid: memberid,
      storeid: store.id,
    })   

    // common.getMember({
    //   app: app,
    //   that: that,
    //   memberid: memberid
    // });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.initPageData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    wx.showNavigationBarLoading();
    that.onShow();
    wx.hideNavigationBarLoading();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
  onShareAppMessage: function () {

  },
   */
  wxpay:function(e){

    var that = this;
    var openID = app.rd3_session || wx.getStorageSync('rd3_session');
    if (!openID) {
      openID = that.data.member.open_id;
    }

    var paymoney = e.currentTarget.dataset.paymoney;//该套餐的支付金额
    var facemoney = e.currentTarget.dataset.facemoney;//该套餐的面值金额
    var issendmember = e.currentTarget.dataset.issendmember;//该套餐的面值金额
    
    server.getJSON(common.gb.CurrentURL + '/Interface/HmapInfo/GetDataInfoWithPageFlagForQC', 
      { 
      action: 'saveAccountDetail',  
      memberid: that.data.memberid, 
      facemoney: facemoney,
      paymoney: paymoney,
      issendmember:issendmember,
      hmapid: common.gb.ID }, function (json) {
      var data = json.data;
      if (data) {
        if (data.status == 'success') {
          //微信接口数据
          var paymoney = data.data.paymoney;
          var ordernumber = data.data.ordernumber;
          var productdesc = "储值金充值";
          //支付时间
          var rowcreatedtime = data.data.rowcreatedtime;;

          var paytype = data.data.paytype||'';
          var url = data.data.url||'';
          if (paytype == '2') {
            wx.navigateTo({
              url: '/page/receive/receive?url=' + url + '&orderNo=' + ordernumber + '&payAmount=' + paymoney,
            })
          } else {
            //微信支付接口调用
            that.setData({
              openID: openID || data.data.member.open_id,
              paymoney: paymoney,
              ordernumber: ordernumber,
              productdesc: productdesc,
              rowcreatedtime: rowcreatedtime,
              rechargemode: 'recharge',
              payhmapFlag: 'wxpay_qiche_recharge'
            })
            app.wxpay(that);

          }
          
        }
        else {
          wx.showToast({
            title: "提交订单失败",
            icon: 'loading',
            duration: 1500
          })
        }
      }
    });
  },
  //以下为自定义点击事件
  initPageData: function () {
    var that = this;

    // wx.showLoading({
    //   title: '加载中',
    //   mask: true
    // })
    // common.getRechargeOptions({
    //   that: that
    // });
  },


})