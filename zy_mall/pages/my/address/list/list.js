
//var Bmob = require('../../utils/bmob.js');
//var WxNotificationCenter = require('../../utils/WxNotificationCenter.js');

var common = require("../../../../common/common.js");
var server = require("../../../../common/server.js");
//获取应用实例
const app = getApp()

var that;

Page({
	data: {
		visual: 'hidden',
    //addressList: [{ name: '靳征', gender: 1, mobile: '18012110998', detail:'宿迁中豪'}]
    addressList: []
	},
	onLoad: function (options) {
		that = this;
		if (options.isSwitchAddress) {
			that.setData({
				isSwitchAddress: true
			});
		}
	},
	onShow: function () {
    that.getAddressByUserCode();	
	},
	add: function () {
		wx.navigateTo({
			url: '../add/add'
		});
	},
  getAddressByUserCode: function () {
    server.postJSON(common.gb.CurrentURL + '/api/wx/my/getAddrListByUserCode',
      { companyid: 1, wu_code: app.globalData.user.wu_code }, function (result) {
        console.info(result.data.data);
        var res = result.data.data;
        that.setData({
          addressList: result.data.data
          //visual: results.length ? 'hidden' : 'show'
        });


      });
    //console.info("asd"+addressList);
  }
  ,
	getAddress: function () {
		var query = new Bmob.Query('Address');
		query.equalTo('user', Bmob.User.current());
		query.limit(Number.MAX_VALUE);
		query.find().then(function (results) {
			that.setData({
				addressList: results,
				visual: results.length ? 'hidden' : 'show'
			});
		});
	},
	edit: function (e) {
		var index = e.currentTarget.dataset.index;
		var objectId = that.data.addressList[index].wd_id;
		wx.navigateTo({
			url: '../add/add?objectId=' + objectId
		})
	},
	selectAddress: function (e) {
		if (!that.data.isSwitchAddress) {
			return;
		}
		var index = e.currentTarget.dataset.index;
		WxNotificationCenter.postNotificationName("addressSelectedNotification", that.data.addressList[index].id);
		wx.navigateBack();
	}
})