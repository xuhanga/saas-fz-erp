// pages/my/my.js
var common = require("../../common/common.js");
var server = require("../../common/server.js");
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    balance: 0.00,
    storeCardMoney: 0.00,
    coupon: 0,
    cashCoupon: 0,
    integral: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var user = app.globalData.user;
    var company = app.globalData.currentCompany;//店铺信息

    that.setData({
      user:user,
      company: company
    })

    if (!user || !user.wu_nickname) {
      wx.reLaunch({
        url: '/pages/grant-msg/shouquan'
      })
      return false;
    }


    // server.postJSON(common.gb.CurrentURL + '/api/wx/my/getMyValue/' + user.wu_mobile,
    //   {
    //     "companyid": company.companyid,
    //     "shop_type": company.sp_shop_type,
    //     "shop_code": company.sp_code,
    //     "shop_upcode": company.sp_upcode
    //   }, function (result) {
    //     var res = result.data;
    //     if (res.stat == 200) {
    //       res.data.ecouponCount;
          
    //     }
    //   });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
    var that = this;

    var loginMobile = app.globalData.user.wu_mobile;//wx.getStorageSync('loginMobile');
    var currentCompany = app.globalData.currentCompany;//店铺信息

    if (!loginMobile) {
      wx.redirectTo({
        url: '/pages/login/login'
      });
      return;
    }

    
    //获取余额，储值卡，优惠券，代金券，积分
    server.postJSON(common.gb.CurrentURL + '/api/wx/my/getMyValue/' + loginMobile,
    {
      "companyid": currentCompany.companyid,
      "shop_type": currentCompany.sp_shop_type,
      "shop_code": currentCompany.sp_code,
      "shop_upcode": currentCompany.sp_upcode
    }, function (result) {
      var res = result.data;
      if (res.stat == 200) {
        that.setData({
          balance: res.data.balance,
          storeCardMoney: res.data.cardValue,
          coupon: res.data.ecouponCount,
          cashCoupon: res.data.voucherCount,
          integral: res.data.points
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  to_order:function(){
    // wx.navigateTo({
    //   url: '../product/product'
    // })
    wx.showToast({
      title: '你好',
      icon: 'success',
      duration: 2000
    })
  },
  to_address: function () {
    wx.navigateTo({
      url: '../product/product'
    })
  },
  to_updatePassword: function () {
    wx.navigateTo({
      url: '../my/updatepwd/updatepwd'
    })
  },
  to_exchange: function () {
   wx.showToast({
     title: '敬请期待',
     icon:'none'
   })
  },
  to_member: function () {
    wx.navigateTo({
      url: '/pages/member/member',
    })
  },
  to_bespeak: function () {
    wx.showToast({
      title: '敬请期待',
      icon: 'none'
    })
  },
  to_address: function () {
    wx.navigateTo({
      url: '../my/address/list/list'
    })
  },
  toCoupon:function(){
    wx.navigateTo({
      url: '/pages/coupon/coupon',
    })
  },
  toOrder: function (e) {
    // var status = e.currentTarget.dataset.status;
    var type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '../order/order-list/index?type=' + type
    })
  },
})