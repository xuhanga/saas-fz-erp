var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    old_password: '',
    new_password: '',
    confirm_password: ''
  },
  old_passwordInput: function (e) {
    this.setData({
      old_password: e.detail.value
    })
  },
  new_passwordInput: function (e) {
    this.setData({
      new_password: e.detail.value
    })
  },
  confirm_passwordInput: function (e) {
    this.setData({
      confirm_password: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  to_updatePassword: function () {
    var that = this;
    var old_password = this.data.old_password;
    var new_password = this.data.new_password;
    var confirm_password = this.data.confirm_password;
    if (old_password == '') {
      wx.showModal({
        content: '请输入旧密码',
        success: function (res) {
        }
      });
      return;
    }
    if (new_password == '') {
      wx.showModal({
        content: '请输入新密码',
        success: function (res) {
        }
      });
      return;
    }

    if (confirm_password == '') {
      wx.showModal({
        content: '请输入确认新密码',
        success: function (res) {
        }
      });
      return;
    }
    server.postJSON(common.gb.CurrentURL + '/api/wx/my/updatePassword',
      { companyid: 1, wu_code: '10004', old_password: this.data.old_password, new_password: this.data.new_password, confirm_password: this.data.confirm_password}, function (result) {
        var res = result.data;
        var data = res.data;
        var message = res.message;
        
        if (res.stat == 200) {
          wx.showToast({
            title: message,
            icon: 'success',
            duration: 2000,
            success:function(){
              setTimeout(function () {
                wx.redirectTo({
                  url: '/pages/index/index'
                })
              }, 2000);
            }
          })         
        }
        else{
          wx.showToast({
            title: message,
            icon: 'success',
            duration: 2000
          })
        } 
      });
  }
})