var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    new_password: '',
  },
  new_passwordInput: function (e) {
    this.setData({
      new_password: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var user = app.globalData.user;// || wx.getStorageSync('user');
    var company = app.globalData.currentCompany;
    that.setData({
      user: user,
      company: company
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  to_updatePassword: function () {
    var that = this;
    var new_password = this.data.new_password;
    if (new_password == '') {
      wx.showModal({
        content: '请输入新密码',
        success: function (res) {
        }
      });
      return;
    }

    server.postJSON(common.gb.CurrentURL + '/api/wx/my/updatePassword',
      { 
        companyid: that.data.company.companyid, 
        wu_code: that.data.user.wu_code,
        new_password: this.data.new_password
      }, function (result) {
        var res = result.data;
        var data = res.data;
        var message = res.message;
        
        if (res.stat == 200) {

          wx.showToast({
            title: message,
            icon: 'success',
            duration: 2000,
            success:function(){
              wx.redirectTo({
                url: '/pages/index/index'
              })
            }
          })
        }else{
          wx.showToast({
            title: message,
            icon: 'success',
            duration: 2000
          })
        }
      });
  }
})