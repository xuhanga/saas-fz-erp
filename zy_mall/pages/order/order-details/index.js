var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
//获取应用实例
const app = getApp()
Page({
  data: {
    ordernumber: '',
    orderInfo: '',
    orderLogisticsInfo: '',
    moreLogisticsInfo: false,
    firstAddr: '',
    firstTime: ''
  },

  //跳转
  goReturn: function (e) {
    var sodid = e.target.dataset.sodid;
    var types = e.target.dataset.types;
    var count = e.target.dataset.count;
    var price = e.target.dataset.price;
    var name = e.target.dataset.name;
    var img = e.target.dataset.img;
    var canreturn = e.target.dataset.canreturn;
    var ordernumber = e.target.dataset.ordernumber;
    wx.navigateTo({
      url: '/pages/tuihuo/tuihuo?sodid=' + sodid
      + '&types=' + types + '&count=' + count + '&price=' + price + '&name=' + name + '&img=' + img + '&canreturn=' + canreturn
      + '&ordernumber=' + ordernumber,
    })
  },

  goReturnDetail: function (e) {
    var sodid = e.target.dataset.sodid;
    var count = e.target.dataset.count;
    var price = e.target.dataset.price;
    var name = e.target.dataset.name;
    var img = e.target.dataset.img;
    var ordernumber = e.target.dataset.ordernumber;
    wx.navigateTo({
      url: '/pages/tuihuodetail/tuihuodetail?sodid=' + sodid
      + '&price=' + price + '&name=' + name + '&img=' + img
      + '&ordernumber=' + ordernumber,
    })
  },
  
  onLoad: function (e) {
    var that = this;
    var ordernumber = e.ordernumber;
    that.setData({
      ordernumber: ordernumber
    });


  },
  loadData: function () {
    var that = this;
    wx.showLoading();
    that.setData({
      controller: '/api/wx/order/getOrderList',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        wu_code: app.globalData.user.wu_code,
        number: that.data.ordernumber,
        statustap: 0,
        pageSize: 1,
        pageIndex: 1
      }
    })

    common.pagedDetails({
      that: that,
      succFun: function (data) {
        wx.hideLoading();
        
        that.setData({
          order: data[0].order,
          products: data[0].details
        })
      }
    })
  },

  cancelOrderTap: function (e) {
    var that = this;

    //保存formid 结束
    var ordernumber = e.currentTarget.dataset.number;
    wx.showModal({
      title: '确定要取消该订单吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          that.setData({
            controller: '/api/wx/order/cancleOrder',
            params: {
              number: ordernumber
            }
          })

          common.doMethod({
            that: that,
            succFun: function (data) {
              if(data){
                wx.showToast({
                  title: JSON.stringify(data),
                })
              }
            }
          })
        }
      }
    })
  },
  toReciveTap: function (e) {
    var that = this;
    //保存formid 结束
    var ordernumber = e.currentTarget.dataset.number;
    wx.showModal({
      title: '确认收货吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          that.setData({
            controller: '/api/wx/order/confirmOrder',
            params: {
              number: ordernumber
            }
          })

          common.doMethod({
            that: that,
            succFun: function (data) {
              if (data) {
                wx.showToast({
                  title: JSON.stringify(data),
                })
              }
            }
          })
        }
      }
    })
  },
  toPayTap: function (e) {
    var that = this
    //保存formid 开始
    var formid = e.detail.formId;
    var formid_json = {
      "xcxid": wx.getExtConfigSync().ID,
      "type": "submit",
      "openid": app.globalData.login.openId,
      "formid": formid
    }
    app.appSaveFormId(formid_json, function (res) { })
    //保存formid 结束
    var order = e.currentTarget.dataset.id;
    app.appPay(order, function () {
      that.onShow();
    })
  },

  onShow: function () {
    var that=this;
    that.loadData();
  },

  wuliuDetailsTap: function () {
    var that = this;
    wx.navigateTo({
      url: "/pages/wuliu/index?ordernumber=" + that.data.ordernumber
    })
  }
})