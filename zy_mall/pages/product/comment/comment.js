var common = require("../../../common/common.js");
var server = require("../../../common/server.js");
var pageIndex = 1;
var pageSize = 10;

// 获取全局应用程序实例对象
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noData: true,
    pagination: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      pd_code: options.pd_code,
      pagination: false
    })

    pageIndex = 1;
    that.initPageData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  //以下为自定义点击事件
  onPullDownRefresh() {
    var that = this;
    //下拉  
    console.log("下拉");
    pageIndex = 1;

    that.setData({
      pagination: false
    });
    that.initPageData();
  },
  //以下为自定义点击事件
  onReachBottom: function () {
    var that = this;
    //上拉  
    console.log("上拉");
    pageIndex++;

    that.setData({
      pagination: true
    });
    that.initPageData();
  },
  /**
   * 用户点击右上角分享
   * onShareAppMessage: function () {},
   */

  //以下为自定义点击事件
  initPageData: function () {
    var that = this;

    wx.showLoading({
      title: '加载中',
      mask: true
    })
    
      var params = {
        "companyid": app.globalData.currentCompany.companyid,
        "shop_code": app.globalData.currentCompany.sp_code,
        "pd_code": that.data.pd_code,
        "pageSize": pageSize,
        "pageIndex":pageIndex
      };

      that.setData({
        controller: '/api/wx/product/getComments',
        params: params
      })


      common.pagedDetails({
        that: that,
        succFun: function (data) {
          // console.log(JSON.stringify(data))
          if (data) {
            that.setData({
              details: data
            })
          }
        },
        comFun: function () {
          wx.hideLoading();
          wx.stopPullDownRefresh();
        }

      })
    
  },


})