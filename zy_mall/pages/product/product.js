var common = require("../../common/common.js");
var server = require("../../common/server.js");
var WxParse = require("../../common/wxParse/wxParse.js");
//获取应用实例
const app = getApp()
Page({
  data: {
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 3000, //自动切换时间间隔,3s
    duration: 1000, //  滑动动画时长1s
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 3000, //自动切换时间间隔,3s
    duration: 1000, //  滑动动画时长1s
    colorImg: '',//选中的花色图片
    selectedInfo: "",//选中的规格信息
    selectStockInfo:'',//选中对应的库存信息
    crcode: "",//判断是否选中
    crname: "",//判断是否选中
    szcode: "",//判断是否选中
    szname: "",//判断是否选中
    brcode: "",//判断是否选中
    brname: "",//判断是否选中
    isHidden: "",
    isLike: true,
    animationData: {},//选择动画
    showModalStatus: false,//显示遮罩
    goodNum: 1,//商品数量
    select: 0,//商品详情、参数切换,
    shopNum:0
  },
  /**
 * 生命周期函数--监听页面加载
 */
  onLoad: function (options) {
    var that = this;

    var pd_code = options.pd_code || '';
    that.setData({
      pd_code: pd_code,
      imgdir: app.globalData.imgdir
    })

    that.initPageData();

  },
  /**选择花色 */
  chooseColor: function (data) {
    var that = this;
    var crcode = data.currentTarget.dataset.crcode;
    var crname = data.currentTarget.dataset.crname;
    var img = data.currentTarget.dataset.img;
    that.setData({//把选中值，放入判断值中
      crname: crname,
      crcode: crcode
    })

    that.setSelectData();
    
  },
  chooseSize:function(data){
    var that = this;
    var szcode = data.currentTarget.dataset.szcode;
    var szname = data.currentTarget.dataset.szname;
    that.setData({//把选中值，放入判断值中
      szname: szname,
      szcode: szcode
    })

    that.setSelectData();

  },
  chooseBra: function (data) {
    var that = this;
    var brcode = data.currentTarget.dataset.brcode;
    var brname = data.currentTarget.dataset.brname;
    that.setData({//把选中值，放入判断值中
      brname: brname,
      brcode: brcode
    })
    
    that.setSelectData();

  },
  setSelectData:function(){
    var that = this;
    var select_sub_code = that.data.product.wp_pd_code + '' + that.data.crcode + '' + that.data.szcode + '' + that.data.brcode;
    for (var i = 0; i < that.data.stock.length; i++) {
      var sd = that.data.stock[i];
      var sub_code = sd.sd_code;
      var sd_amount = sd.sd_amount;

      if (sub_code == select_sub_code) {
        if (sd_amount <= 0) {
          that.setData({
            goodNum: 0,
            sd_amount: 0,
            selectStockInfo: '库存不足',
            isenableaddcart: false
          })
        } else {
          that.setData({
            goodNum: 1,
            sd_amount: sd_amount,
            selectStockInfo: ' 库存：' + sd_amount,
            isenableaddcart: true
          })
        }
        break;
      }
    }

    var select_sub_name = that.data.crname + "-" + that.data.szname + "-" + that.data.brname;
    var laststr = select_sub_name.substring(select_sub_name.length - 1);
    if (laststr == '-') {
      select_sub_name = select_sub_name.substring(0, select_sub_name.length - 1);
    }
    that.setData({
      select_sub_code: select_sub_code,
      select_sub_name: select_sub_name,
      selectedInfo: select_sub_name + " " + that.data.goodNum + "个"
    })
  },
  chooseSku:function(){
    var that = this;
    if (!that.data.select_sub_code) {
      wx.showToast({
        title: '您还没有选择规格~',
        icon: 'none'
      })
      return false;
    }

    if (!that.data.isenableaddcart) {
      wx.showToast({
        title: '选择的商品没有库存~',
        icon: 'none'
      })
      return false;
    }

    if (that.data.color.length >0 && !that.data.crcode){
      wx.showToast({
        title: '您还没有选择颜色~',
        icon: 'none'
      })
      return false;
    }
    if (that.data.size.length > 0 && !that.data.szcode) {
      wx.showToast({
        title: '您还没有选择尺码~',
        icon: 'none'
      })
      return false;
    }
    if (that.data.bra.length > 0 && !that.data.brcode) {
      wx.showToast({
        title: '您还没有选择杯型~',
        icon: 'none'
      })
      return false;
    }

    that.setData({//把选中值，放入判断值中
      showModalStatus: false,//显示遮罩       
      isHidden: 0,
    })

  },
  /**点击选择花色按钮、显示页面 */
  viewFlowerArea: function (data) {
    var that = this;
    var isswitchtab = data.currentTarget.dataset.isswitchtab;
    that.setData({
      isenableaddcart: true,
      isswitchtab: isswitchtab
    })
    var animation = wx.createAnimation({//动画
      duration: 500,//动画持续时间
      timingFunction: 'linear',//动画的效果 动画从头到尾的速度是相同的
    })
    animation.translateY(0).step()//在Y轴偏移tx，单位px
    this.animation = animation
    that.setData({
      showModalStatus: true,//显示遮罩       
      animationData: animation.export()
    })
    that.setData({//把选中值，放入判断值中
      isHidden: 1,
    })
  },
  /**隐藏选择花色区块 */
  hideModal: function (data) {
    var that = this;

    // if (!that.data.sd_amount){      
      that.setData({
        crcode:'',
        szcode:'',
        brcode:'',
        crname: '',
        szname: '',
        brname: '',
        select_sub_code: '',
        select_sub_name: '',
        selectedInfo: ''
      })
    // }
    that.setData({//把选中值，放入判断值中
      showModalStatus: false,//显示遮罩       
      isHidden: 0,
    })
  },
  goodAdd: function (data) {
    var that = this;
    
    var goodCount = that.data.goodNum + 1;
    if (goodCount > that.data.sd_amount){
      goodCount = that.data.goodNum;
    }
    that.setData({//商品数量+1
      goodNum: goodCount
    })

    that.setData({
      selectedInfo: that.data.crname + "-" + that.data.szname + "-" + that.data.brname + " " + that.data.goodNum + "个"
    })
  },
  goodReduce: function (data) {
    var that = this;
    var goodCount = that.data.goodNum - 1;
    if (goodCount <1 ) {
      goodCount = that.data.goodNum;
    }
    that.setData({//商品数量-1
      goodNum: goodCount
    })
    that.setData({
      selectedInfo: that.data.crname + "-" + that.data.szname + "-" + that.data.brname+ " " + that.data.goodNum + "个"
    })
  },
  /**商品详情、参数切换 */
  changeArea: function (data) {
    var that = this;
    var area = data.currentTarget.dataset.area;
    that.setData({ "select": area });
  },
  /**
   * 加入购物车:不跳转，购物车数字图标变化
   * 立即购买：直接跳转到购物车页面处理
   */
  addCart: function (data) {
    var that = this;
    var isswitchtab = that.data.isswitchtab;//data.currentTarget.dataset.isswitchtab;


    if (!that.data.select_sub_code){
      wx.showToast({
        title: '您还没有选择规格~',
        icon:'none'
      })
      return false;
    }else{
      if (!data.currentTarget.dataset.showpopup){
        isswitchtab = data.currentTarget.dataset.isswitchtab;
      }      
    }

    if (!that.data.isenableaddcart) {
      wx.showToast({
        title: '选择的商品没有库存~',
        icon: 'none'
      })
      return false;
    }
    if (that.data.color.length > 0 && !that.data.crcode) {
      wx.showToast({
        title: '您还没有选择颜色~',
        icon: 'none'
      })
      return false;
    }
    if (that.data.size.length > 0 && !that.data.szcode) {
      wx.showToast({
        title: '您还没有选择尺码~',
        icon: 'none'
      })
      return false;
    }
    if (that.data.bra.length > 0 && !that.data.brcode) {
      wx.showToast({
        title: '您还没有选择杯型~',
        icon: 'none'
      })
      return false;
    }

   
    //抽取商品图片对应的名称
    var pd_pic = that.data.imgs[0].split(that.data.imgdir)[1];
    //直接跳转购物车
    that.setData({
      controller: '/api/wx/cart/addCart',
      params: {
        companyid: app.globalData.currentCompany.companyid,//公司id
        // shop_code: app.globalData.currentCompany.sp_code,//店铺编码
        pd_code: that.data.product.wp_pd_code ,//商品编码
        wu_code: app.globalData.user.wu_code,//用户编码
        pd_name: that.data.product.wp_pd_name,//商品名称
        pd_pic: pd_pic,//商品主图
        cr_code: that.data.crcode,//颜色编码
        sz_code: that.data.szcode,//尺寸编码
        br_code: that.data.brcode,//杯型编码
        cr_name: that.data.crname,//颜色名称
        sz_name: that.data.szname,//尺寸名称
        br_name: that.data.brname,//杯型名称
        sub_code: that.data.select_sub_code,//选中的规格组合编码
        sub_name: that.data.select_sub_name,//选中的规格串 红色-XL-B
        amount: that.data.goodNum,//数量
        price: that.data.product.wp_rate_price,//价格     
        sell_price: that.data.product.wp_sell_price,//原价   
      }
    })

    common.doMethod({
      that: that,
      succFun: function (data) {
        if (isswitchtab == '1') {
          wx.switchTab({
            url: '/pages/cart/cart',
          })
        }
        else if (isswitchtab == '2') {
          wx.showToast({
            title: '成功加入购物车',
            icon: 'none',
            success: function () {
              that.hideModal();
              //查询购物车商品总数
              that.getShopNum(); 
            }
          })
        }

        if (isswitchtab == '1') {
          that.hideModal();
        }

      }
    })
    
    
  },
  /**
   * 查看轮播图片
   */
  seeSwiperAll: function (e) {
    this.seePreviewImg(0, e.currentTarget.dataset.img)
  },
  /**
* 查看花色图片 
* */
  seeFlowersAll: function (e) {
    this.seePreviewImg(1, e.currentTarget.dataset.img)
  },
  /**
   * 预览图片
   * @pd 0表示轮播图 、 1表示花色
   */
  seePreviewImg: function (pd, showImg) {
    var array = [];
    var that = this;
    if (pd == 0) {
      // var imgArray = that.data.imgUrls;
      // for (var i = 0; i < imgArray.length; i++) {
      //   array.push(imgArray[i].flower_image)
      // }
    } else if (pd == 1) {
      var imgArray = that.data.imgArray;
      for (var i = 0; i < imgArray.length; i++) {
        array.push(imgArray[i].url)
      }
    }

    wx.previewImage({
      current: showImg, // 当前显示图片的http链接
      urls: array // 需要预览的图片http链接列表
    })
  },
  toCartPage: function () {
    wx.switchTab({
      url: "/pages/cart/cart"
    });
  },
  toIndexPage: function () {
    wx.switchTab({
      url: "/pages/shop/shop"
    });
  },

  //商品详情页加载
  initPageData: function (e) {
    var that = this;
    var currentCompany = app.globalData.currentCompany;
    that.setData({
      controller: '/api/wx/product/load',         
      params: {
        companyid: currentCompany.companyid,
        shop_code: currentCompany.sp_code,
        pd_code: that.data.pd_code
      }
    })

    common.doMethod({
      that: that,
      succFun: function (data) {
        var data = data.data;
        that.setData({
          imgs:data.imgs || [],
          product: data.product,
          color:data.color,
          size:data.size,
          stock:data.stock,
          bra:data.bra
        })
        WxParse.wxParse('desc', 'html', data.product.wp_desc, that, 5);
        WxParse.wxParse('property', 'html', data.product.wp_property_name, that, 5);

        //查询购物车商品总数
        that.getShopNum(); 

        //获取商品评价
        that.getComments();
      }
    })
  },
  getShopNum:function(){
    var that = this;
    that.setData({
      controller: '/api/wx/cart/getCartInfo',
      params: {
        wu_code: app.globalData.user.wu_code,
        companyid: app.globalData.currentCompany.companyid
      }
    })

    common.doMethod({
      that: that,
      succFun: function (data) {
        if (data) {
          var cartgoods = data.data;
          if (cartgoods) {
            var shopNum = 0;
            for (var i = 0; i < cartgoods.length; i++) {
              var cartgood = cartgoods[i];
              shopNum += cartgood.amount;
            }
            that.setData({
              shopNum: shopNum
            })
          }
        }
      }
    })
  },
  getComments: function () {
    var that = this;
    var params = {
      "companyid": app.globalData.currentCompany.companyid,
      "shop_code": app.globalData.currentCompany.sp_code,
      "pd_code": that.data.pd_code,
      "pageSize": 5,//pageSize,
      "pageIndex": 1//pageIndex
    };

    that.setData({
      controller: '/api/wx/product/getComments',
      params: params
    })


    common.pagedDetails({
      that: that,
      succFun: function (data) {
        // console.log(JSON.stringify(data))
        if (data) {
          that.setData({
            comments: data
          })
        }
      },
      comFun: function () {
        wx.hideLoading();
        wx.stopPullDownRefresh();
      }

    })
  },

  haslike: function () {
    var login = app.globalData.login;
    var that = this;

    that.setData({
      controller: '/api/wx/product/like',
      params: {
        companyid: app.globalData.currentCompany.companyid,
        shop_code: app.globalData.currentCompany.sp_code,
        pd_code: that.data.pd_code
      }
    })

    common.doMethod({
      that: that,
      succFun: function (data) {
        var data = data.data;
        if (data== "false") {
          that.data.product.wp_likecount = that.data.product.wp_likecount-1;
          that.setData({
            islike: false,
            product: that.data.product
          })
        }
        if (data == "true") {
          that.data.product.wp_likecount = that.data.product.wp_likecount + 1;
          that.setData({
            islike: true,
            product: that.data.product
          })
        }
      }
    })
  }
})