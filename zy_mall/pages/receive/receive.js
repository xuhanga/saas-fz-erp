var common = require("../../common/common.js");
var server = require("../../common/server.js");
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (opt) {
    var that = this;

    var memberId = wx.getStorageSync('rd_session') || '';
    var userInfo = app.globalData.userInfo;
    var url = opt.url || '';
    var id = opt.id || '';
    var orderNo = opt.orderNo || '';
    var payAmount = opt.payAmount || '0';
    var pageurl = opt.pageurl ||'';
    console.warn('url:' + url);
    that.setData({
      memberId: memberId,
      userInfo: userInfo,
      url: url,
      id: id,
      orderNo: orderNo,
      payAmount: payAmount,
      payCard: '',
      pageurl: pageurl
    });

    that.initPageData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;

    that.initPageData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //以下为自定义点击事件
  initPageData: function () {
    var that = this;

        //console.warn('order:' + JSON.stringify(order));
        var orderNo = that.data.orderNo;
        var price = that.data.payAmount;
        var qrcode = that.data.url;
        wx.showToast({
          title: "加载中",
          icon: 'loading',
          duration: 15000,
          success: function (e) {
            console.warn(11111111111111111);
            var url = common.gb.CurrentURL + '/Interface/HmapInfo/GetDataInfoWithPageFlagForMM';
            server.getJSON(url, {
              hmapID: common.gb.ID,
              action: 'createPayCard',
              orderNo: orderNo,
              price: price,
              qrcode: qrcode
            }, function (fileRes) {
              console.log(JSON.stringify(fileRes));
              that.setData({
                payCard: common.gb.CurrentURL + "/" + fileRes.data.data
              });
              console.log("url：" + common.gb.CurrentURL + "/" + fileRes.data.data);
              wx.hideLoading(); 
            });
          }
        })


    common.renderCustomData({
      that: that,
      events: 'shareTitle',
      pageName: 'index'
    });
  },
  // 图片点击事件
  previewImage: function (event) {
    var src = event.currentTarget.dataset.src;//获取data-src
    var urls = [];
    urls.push(src);
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: urls // 需要预览的图片http链接列表
    })
  },
  scanCode: function () {
    wx.scanCode({
      success: (res) => {
        var path = res.path;
        wx.showToast({
          title: '扫码成功',
        })
      },
      fail: (res) => {
        wx.showToast({
          title: '支付失败',
        })
      }
    })
  },
  saveImg: function (e) {
    var that = this;
    
    var src = e.currentTarget.dataset.src;
    // console.log("src:" + this.data.payCard)
    wx.downloadFile({
      url: this.data.payCard,
      success: function (res) {
        let path = res.tempFilePath;
        console.log("path:" + path)
        wx.saveImageToPhotosAlbum({
          filePath: path,
          success: function(res) {
            wx.showToast({
              title: '图片保存成功',
              icon: 'success'
            })
            console.log("success:" + JSON.stringify(res));
          },
          fail: function(res) {
            console.log("fail:" + JSON.stringify(res));
            if (res.errMsg&&res.errMsg.indexOf("den")>=0) {
              console.log("用户一开始拒绝了，我们想再次发起授权")
              console.log('打开设置窗口')
              wx.openSetting({
                success: function(settingdata) {
                  console.log(settingdata)
                  if (settingdata.authSetting['scope.writePhotosAlbum']) {
                    console.log('获取权限成功，给出再次点击图片保存到相册的提示。')
                  } else {
                    console.log('获取权限失败，给出不给权限就无法正常使用的提示')
                  }
                }
              })
            }
          },
          complete: function(res) {
            console.log("complete:" + JSON.stringify(res));
          }
        })
      }, 
      fail: function (res) {
        console.log(res)
      }
    })
  }
})